$(document).ready(function()
{
    uploadify(0);
});

function uploadify(album_id)
{
    album_id = album_id || 0;
    now = new Date();
	$('#fileInput').uploadify({

                'swf'               : '/vendor/uploadify/uploadify.swf',
                'uploader'          : '<?if(isset($_GET['path'])) echo $_GET['path']; else echo '..';?>/<?php if(isset($_GET['module_id']) && !empty($_GET['module_id']) ) echo $_GET['module_id'].'/';?>images_upload',
                'checkExisting' 	: '<?if(isset($_GET['path'])) echo $_GET['path']; else echo '..';?>/<?php if(isset($_GET['module_id']) && !empty($_GET['module_id']) ) echo $_GET['module_id'].'/';?>images_check',

                'buttonImage'		: ($.browser.opera ? '/static/admin/images/btn_upload_ru.png' : ''),
                'cancelImage' 		: '/vendor/uploadify/uploadify-cancel.png',
                'uploaderType'    	: 'flash',
                'fileSizeLimit'         : 2048000,
                'fileTypeDesc'		: 'Files (*.jpg,*.jpeg,*.png,*.gif)',
                'fileTypeExts'		: '*.jpg;*.JPG;*.jpeg;*.JPEG;*.png;*.PNG;*.gif;*.GIF',
                
                postData                : {
                                            'user'          : '<?php echo $_GET["u"];?>',
                                            'session_id'    : '<?php echo $_GET["n"];?>',
                                            'album_id'      : album_id
                                        },
                                        
                onUploadError           : function (a, b, c, d){

							if (d.status == 404)
                                                            alert('Could not find upload script.');
							else if (d.type === "HTTP")
                                                            alert('error '+d.type+": "+d.status);
							else if (d.type ==="File Size")
                                                            alert(c.name+' '+d.type+' Limit: '+Math.round(d.sizeLimit/1024)+'KB');
							else
                                                            alert('error '+d.type+": "+d.info);
						},
                                                
                'onInit'		: function(event, data) {        
                                        showImagesPreview(album_id);
                                  },
                'onQueueComplete'       : function(event, data) {
                                                showImagesPreview(album_id);
                                        },
                'buttonClass'           : 'btn blue',                                        
                'buttonText'            : 'Загрузить',                                        
                'height'                : 19,                                        
                'width'                 : 100,                                        
                'successTimeout'        : 5,                                        
                'multi'                 : true,
                'auto'                  : true
                                        
	});    
}