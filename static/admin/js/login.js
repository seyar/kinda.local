/* @version jQueryId: v 0.1 08.01.2010 - 15:35:55 Exp jQuery
 *
 * Project:     chimera
 * File:        login.js *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @copyright (C) 2009
 *
 * @author Yuriy Klinkov <klinkov@gmail.com>
 */

jQuery(document).ready(function()
{
    jQuery('.loginWrap').css('marginTop', -jQuery('.loginWrap').height() / 2 + 'px' );

    jQuery('a.submitForm').bind("click", function(){
        jQuery('.' + jQuery(this).attr('rel') + ' form:first').submit();
        return false;
        
    });

    jQuery('a.clearForm').bind("click", function(){

        jQuery('.' + jQuery(this).attr('rel') + ' input').each(function(){
            jQuery(this).val('').attr('checked','');
        });
        return false;

    });
   
    jQuery('input[name=username]').focus();
    jQuery('input[name=email]').focus();
    
});
