function nextPage(page_id)
{
    current_page = parseInt($(page_id).val());

    if(current_page < $('#total_pages').val())
        $(page_id).val( current_page+1 );

    return current_page;
}

function prevPage(page_id)
{
    current_page = parseInt($(page_id).val());

    if(current_page > 1)
        $(page_id).val( current_page-1 );

    return current_page;
}