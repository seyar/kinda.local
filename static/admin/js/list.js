function formfilter(thisObj)
{
    jQuery(thisObj).parents('form')
    .attr( 'method', 'POST')
    .submit();
    return false;
}

jQuery(document).ready(function()
{
    jQuery('a img.del').parent('a').click(
        function()
        {
            if( !jQuery(this).hasClass('ajax') )
                return confirm(Chimera.lang.are_you_sure);
        });
            
    // Form check/uncheck
    jQuery('input.listCheckboxAll').click(function()
    {
        var obj = jQuery(this);
        jQuery('input.listCheckbox').each(
            function()
            {
                jQuery(this).attr('checked',obj.attr('checked'));
                var id = jQuery(this).attr('id');
                if(obj.attr('checked'))
                    jQuery("#l"+id).addClass('checked');
                else jQuery("#l"+id).removeClass('checked');
            }
            );
    });

    jQuery('a.ajaxSwitchColumn').click(function()
    {
        var obj = this;
        jQuery.get(jQuery(this).attr('href'), function(data){
            if (data=='OK')
                jQuery(obj).children('img').toggle();
        })
        return false;
    });

    // Table Sort
    jQuery('table.sortableContentTab th').each(function()
    {        
        jQuery(this).click(function(){
            if( jQuery(this).children('input[type=checkbox]').length > 0 || jQuery(this).hasClass('unsortable') ) return;
            document.location = jQuery(this).attr('order_url');
        });
    });

    // Form Filter
    jQuery('#row_per_page').live('change', function()
    {
        var obj = this;
        alert( jQuery('#row_per_page').val());
//        jQuery(obj).attr('disabled', 'true');
//        formfilter(jQuery(obj));
//        jQuery(obj).attr('disabled', 'false');

        return false;
    });

    jQuery('button.formFilter').click(function()
    {
        var obj = this;
        jQuery(obj).attr('disabled', 'true');
        formfilter(jQuery(obj));
        jQuery(obj).attr('disabled', 'false');

        return false;
    });

    jQuery('input.has_default_value').each(function(){
		obj = this;
		$(obj).focus(function(){ if(this.value == $(this).attr('rel')) this.value = ''});
		$(obj).blur(function(){ if(this.value == '') this.value = $(this).attr('rel')});
	})
});