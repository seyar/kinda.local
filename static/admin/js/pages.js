jQuery(document).ready(function(){
    /* open cur tab on reload */
    var currentUrl = document.URL;
    var anchor = (currentUrl.indexOf('#') != -1) ? currentUrl.replace(/.*#/, '') : '';
    currentUrl = (currentUrl.indexOf('#') != -1) ? currentUrl.replace(/#.*/, '') : currentUrl;


    if( anchor != '')
    {
        anchor = anchor.replace(/\//, ''); /* replace all slashes */
        if( jQuery('#'+anchor).html() != undefined && jQuery('.'+anchor).html() != undefined )
        {
            jQuery('.tabs li').removeClass('active');
            jQuery('#'+anchor).addClass('active');
            jQuery('.tabContent').removeClass('openedTab');
            jQuery('.'+anchor).addClass('openedTab');

            if(anchor == 'domains') loadDomains();
            if(anchor == 'languages') langLoad();

        }
    }
    /*  */

    // Show Page Preview
    jQuery('.tabs li a').click(function(){

        var actId = jQuery(this).parent().attr('id');
        if( actId == 'showPreview') return false;

        if (document.location.hash != '#'+actId) {
            document.location.hash = '#'+actId;
        }


        jQuery('.tabs li').removeClass('active');
        jQuery(this).parent().addClass('active');
        jQuery('.openedTab')
        .slideUp( 'fast',
            //	    .hide( 'fast',
            function(){
                jQuery('.openedTab').removeClass('openedTab');

                jQuery('.'+actId).addClass('openedTab').show();

                jQuery('#actTab').val(actId);

            });
        if( actId == 'showPageBlocks')
        {

        }
        return false;
    });


    jQuery("input[name=parent_url]").val( jQuery('#parent_id').attr('title') );

});

function hideAddSelect()
{
    jQuery('.innerGray table tr td.need_hide span').addClass('hidden');
    jQuery('#ckeditor_div').removeClass('hidden');
}

function showAddSelect()
{
    jQuery('.innerGray table tr td.need_hide span').removeClass('hidden');
    jQuery('#ckeditor_div').addClass('hidden');
}

function copyPageFrom( id )
{
    jQuery.getJSON(
        '/admin/pages/'+id+'/copy_from',
        function(data)
        {
            jQuery('input[name=page_title]').val(data.page_title);
            jQuery('input[name=page_name]').val(data.page_name);
            jQuery('input[name=page_url]').val(data.page_url);
            jQuery('select[name=parent_id]').val(data.parent_id);

        }
        );
}
function changeParent()
{
    jQuery('[name=parent_url]').val( jQuery('#parent_id').attr('title') );
}
