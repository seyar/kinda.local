$(document).ready(function()
{
    $('.addShortcut').click(function()
    {
        params = {name: document.title, link: document.location.href};
        
        $.post('/admin/shortcuts/add', params, function(data)
        {
            if (data.result == '1')
            {
                $('.addShortcut').hide();
                $('ul.subMenu_ li:last').after('<li><a href="'+ document.location.href +'">'+ data.name +'</a></li>');
            }
            else
            {
                alert('Error!');
            }

        }, 'json');

        return false;
        
    });
})
