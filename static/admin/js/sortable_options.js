 function color_sortable(){
        a = $("#test-list").find('li');
        a.removeClass('grey_color');
        a.removeClass('white_color');
        a.removeClass('border_radius_first');
        a.removeClass('border_radius_last');
        $("#test-list").find('li').each(function(i){
            if (i % 2) {$(a[i]).addClass('grey_color')} else {$(a[i]).addClass('white_color')};
        });
    $('#test-list .border_round').each(function(i){
        $(this).find('li').first().addClass('border_radius_first');
        $(this).find('li').last().addClass('border_radius_last');
        if ($(this).find('li').size() == 1){
            $(this).find('li').removeClass('border_radius_first');
            $(this).find('li').removeClass('border_radius_last');
            $(this).find('li').addClass('border_radius_one');
        }

    });
    $('#test-list .items_no_group').each(function(i){
        $(this).find('li').first().addClass('border_radius_first');
        $(this).find('li').last().addClass('border_radius_last');

            if ($(this).find('li').size() == 1){
            $(this).find('li').removeClass('border_radius_first');
            $(this).find('li').removeClass('border_radius_last');
            $(this).find('li').addClass('border_radius_one');
        }
    });
 
    }
	
	function serialize_sort()
	{
	    var str='';
		jQuery('#test-list li[id^="id"]').each(function(i){
		var a = $(this).attr('id');
		a =  a.substr(3);
		str = str+'&id[]='+a;
		});
		return str;
	}
	
    function clicked(e)
    {
        var par = e.parent('li');
        par.find('.click_active').each(function(i){

            par.find('.group_settings').hide();
            par.find('.text_edit_options').hide();
            par.find('.input_edit').show();

            var row_text = ($(this).find('span').text());

            if ($(this).hasClass('input_add')){
                
                var y = $("<input>", {
                    type: 'text',
                    value: row_text,
                    className: "input_edit"
                });
                var block = $(document.createElement('div')).addClass('temp_item').html(y);
                $(this).append(block);
            }

            if (i==0) {$(this).find('input').focus();}
            
            
            if ($(this).hasClass('column_options_free')){

                var sel = $(' <select id="jSelect" class="input_edit selected_style">'+

                    '<option value="int">Int</option>'+
                    '<option value="float">Float</option>'+
                    '<option value="date">Дата</option>'+
                    '<option value="text">Text</option>'+
                    '<option value="varchar">Varchar</option>'+
                    '<option value="boolean">Boolean</option>'+
                    '<option value="select">Select</option>'+
                    '</select>').val($(this).find('span').html());

                var block = $(document.createElement('div')).addClass('temp_item').html(sel);
                $(this).append(block);
            }

        });
        par.find('.column_options_checkbox').hide();
        par.find('.column_options_delete').hide();
        par.find('.clear').before('<div class="flRight temp_item"><button type="button" class="btn blue formUpdate but_save"><span><span>'+Chimera.lang.save+'</span></span></button>&nbsp;&nbsp;&nbsp;<button type="button" class="btn blue formUpdate but_save but_cancel"><span><span>'+Chimera.lang.cancel+'</span></span></button></div>');

        par.find('.input_textarea').append('<div class="temp_item"><textarea class="textarea_edit">'+par.find('.input_textarea span').html()+'</textarea></div>');
        jQuery('.click_active').addClass('click_deactive');
        jQuery('.click_active').removeClass('click_active');

    }

    $(document).ready(function()
    {
	//alert('1');
        color_sortable();
        if( $('#test-list li').length != 0 )
        {
            $("#test-list").sortable({
                revert: true,
                items   : '.group_link',
                handle  : '.handle',
                //nested: '#test-list',
                //containment: 'parent',

                update  : function () {

                    //$(this).sortable('cancel');
                    //var serialize_vars = ($('#test-list').sortable('serialize'));
                    var serialize_vars = serialize_sort();
                    jQuery.post("/admin/categories/SortInUpdate", {serialize:serialize_vars} ,function(data){
                        if(data != 1 ) alert('Server error');
                    });
                    color_sortable();
                }
            });
        }
        if( $('.border_style_item').html().length != 0 )
        {
            $(".border_style_item").sortable({
                revert: true,
                items   : '.items_links',
                handle  : '.handle',
                nested: '#test-list',
                //containment: 'parent',

                update  : function () {

                    //$(this).sortable('cancel');
                    //var serialize_vars = ($('#test-list').sortable('serialize'));
                    var serialize_vars = serialize_sort();
                    jQuery.post("/admin/categories/SortInUpdate", {serialize:serialize_vars} ,function(data){
                        if(data != 1 ) alert('Server error');
                    });
                    color_sortable();
                }
            });
        }

        if( $('#test-list li').length != 0 )
        {
            $("#test-list li").sortable({
                revert: true,
                items   : 'li',
                handle  : '.handle',
                nested: '#test-list',
                containment: 'parent',

                update  : function () {
                    //var serialize_vars = ($('#test-list').sortable('serialize'));
                    var serialize_vars = serialize_sort();
                    jQuery.post("/admin/categories/SortInUpdate", {serialize:serialize_vars} ,function(data){
                        if(data != 1 ) alert('Server error');
                    });
                    color_sortable();
                }
            });
        }

        jQuery('#test-list li .click_active').live('click', function(){clicked($(this));} );
        jQuery('.but_cancel').live('click', function(){
            $('.cat_optionts').find('.deactive_item_add').addClass('active_item_add');
            $('.cat_optionts').find('.deactive_item_add').removeClass('deactive_item_add');
            var par = jQuery(this).parent().parent();
            par.find('.text_edit_options').show();
            par.find('.group_settings').show();
            par.find('.temp_item').remove();
            par.find('.column_options_checkbox').show();
            par.find('.column_options_delete').show();
            jQuery('.click_deactive').addClass('click_active');
            jQuery('.click_deactive').removeClass('click_deactive');
            var f = 0;
            par.find('.click_active').each(function(i){
                if (i != 2)
                {if ($(this).find('span').html() != '') {f=1;}}
            });
            if (f==0) {par.remove()}
            return false;
        });

        jQuery('.but_save').live('click', function(){

            $('.cat_optionts').find('.deactive_item_add').addClass('active_item_add');
            $('.cat_optionts').find('.deactive_item_add').removeClass('deactive_item_add');

            var par = jQuery(this).parent().parent();

            var old_name_group = par.children('div:first').children('span').html();

            var massiv = [];
                var name_group = par.attr('rel');
            par.find('.input_edit').each(function(i)
            {
                if (i==0) {massiv[i] = name_group+$(this).val();}
                else{
                massiv[i] = $(this).val();}
            });

            massiv[4] = (par.find('.textarea_edit').val());

            var id_cat = $('#test-list').attr('rel');
             
            var id = (par.attr('id'));

            jQuery.getJSON(
                "/admin/categories/edit_options_update",
                {id_cat:id_cat,id:id,mas:massiv,old_name_group:old_name_group},
                function(data){

                    if (data == 'rebut')
                    {
                        window.location.reload();
                        return false;
                    }

                    if (data[0]>1) par.attr('id','id_'+data[0]);
                    if(data[1] != 1 ) alert('Server error');

                    par.find('.text_edit_options').show();
                    par.find('.temp_item').remove();
                    par.find('.column_options_checkbox').show();
                    par.find('.column_options_delete').show();
                    par.find('.group_settings').show();

                    jQuery('.click_deactive').addClass('click_active');
                    jQuery('.click_deactive').removeClass('click_deactive');
                    par.find('.click_active').each(function(i){
                        if (i==0)
                        {
                            var name_alias = massiv[i].replace(name_group, '');
                            $(this).find('span').html(name_alias);
                        }
                        else
                        {$(this).find('span').html(massiv[i]);}
                    });
                    var str='';
                    jQuery('.ui-sortable li[id^="id"]').each(function(i){
                        var a = $(this).attr('id');
                        a =  a.substr(3);
                        str = str+'&id[]='+a;
                    });
                   jQuery.post("/admin/categories/SortInUpdate", {serialize:str} ,function(data){
                            if(data != 1 ) alert('Server error');
                        });
            });

            return false;
            
        });

        jQuery('.rol_del').live('click',function(){
            var par = jQuery(this).parent();
            var id = (par.attr('id'));

            jQuery.post("/admin/categories/options_delete", {id:id} ,function(data){
                if(data != 1 ) alert('Server error');
            });

            par.hide(3000);
            par.remove();
            color_sortable();
            return false;

        });

        jQuery('input[name=is_searchable]').live('click',function(){
            var id = jQuery(this).parent().parent().parent().attr('id');
            jQuery.post("/admin/categories/is_searchable_update", {id:id} ,function(data){
                if(data != 1 ) alert('Server error');
            });
        });
        jQuery('input[name=is_filterable]').live('click',function(){
            var id = jQuery(this).parent().parent().parent().attr('id');
            jQuery.post("/admin/categories/is_filterable_update", {id:id} ,function(data){
                if(data != 1 ) alert('Server error');
            });
        });

        jQuery('#add_field.active_item_add').live('click',function(){
            $('.cat_optionts').find('.active_item_add').addClass('deactive_item_add');
            $('.cat_optionts').find('.active_item_add').removeClass('active_item_add');
            $('.items_no_group').show();
            var par = $('#test-list').find('.border_style_item');

            if (par == null) {alert('1')}
            
            //console.log(par);
            var a = '<li rel=""><div class="flLeft column_options_first"><img src="/static/admin/images/pull_over.png" class="handle" alt="move" /></div><div class="flLeft column_options click_active input_add"><span class="text_edit_options"></span></div><div class="flLeft column_options click_active input_add"><span class="text_edit_options"></span></div><div class="flLeft column_options_free click_active"><span class="text_edit_options"></span></div><div class="flLeft column_options click_active input_add"><span class="text_edit_options"></span></div><div class="flLeft column_options click_active input_textarea"><span class="text_edit_options"></span></div><div class="flLeft column_options_checkbox"><span class="text_edit_options"><input type="checkbox" name="is_searchable"></span></div><div class="flLeft column_options_checkbox2"><span class="text_edit_options"><input type="checkbox" name="is_filterable"></span></div> <div class="flRight rol_del"><a class="flLeft column_options_delete" href="#"></a></div><div class="clear"></div></li>';
            par.append(a);
            color_sortable();
             var z = par.find('li:last div');
            clicked(z);
            return false;
        });
        
        jQuery('#add_group.active_item_add').live('click',function(){
            $('.cat_optionts').find('.active_item_add').addClass('deactive_item_add');
            $('.cat_optionts').find('.active_item_add').removeClass('active_item_add');
            var par = $('#test-list').find('.border_style_item');
            var a = '<li rel=""><div class="flLeft column_options_first"></div><div class="flLeft column_options click_active input_add"><span class="text_edit_options"></span></div><div class="flLeft column_options click_active input_add"><span class="text_edit_options"></span></div><div class="flLeft column_options click_active input_add"><span class="text_edit_options"></span></div><div class="flRight rol_del"><a class="flLeft column_options_delete" href="#"></a></div><div class="clear"></div></li>';
            par.append(a);
            color_sortable();
             var z = par.find('li:last div');
            clicked(z);
            return false;
        });



        jQuery('.delete_group').live('click',function(){
            var par = ($(this).parent().parent().parent().parent());
            var del_list = '';
            par.find('li[id^="id"]').each(function(i){
                del_list = del_list + $(this).attr('id');
            });

            jQuery.getJSON(
                "/admin/categories/edit_options_delete",
                {del_list:del_list},function(data){
                   if(data[1] != 1 ) alert('Server error');
                   par.parent().hide("slow");
                   par.parent().remove();
                }
                );
            return false;
        });
    });