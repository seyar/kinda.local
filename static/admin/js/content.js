var simpleTreeCollection;

$(document).ready(function(){
    simpleTreeCollection = $('.simpleTree').simpleTree({
            autoclose: true,
            afterClick:function(node){
                    //alert("text-"+$('span:first',node).text());
            },
            afterDblClick:function(node){
                    document.location = $(node).attr('id')+'/edit';
            },
            afterMove:function(destination, source, pos){
				if( confirm('Are you sure?') )
				{
					$.ajax(
						{
							cache: false,
							type: "POST",
							url: source.attr('id')+'/ajax_reorder',
							data: 'new_parent_id='+destination.attr('id')+'&new_order='+pos,
							success: function(msg){
							   if(msg)
								alert( "Error: " + msg );
							}
						}
					);
				}
				else
					document.location.reload();
            },
            afterAjax:function()
            {
//                    alert('Loaded');
            },
            animate:true
            //,docToFolderConvert:true
    });

});