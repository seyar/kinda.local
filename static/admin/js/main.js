/* @version jQueryId: v 0.1 04.01.2010 - 15:35:55 Exp jQuery
 *
 * Project:     chimera
 * File:        main.js *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @copyright (C) 2009
 *
 * @author Seyar Chapuh <sc@bestitsolutions.biz>
 */
Chimera = {};
jQuery(document).ready(function(){
    //    обработка стрелки выдв. менюхи

    jQuery("#button-in-out").click(function(){
        jQuery("#button-in-out").hide();
        jQuery("#id_panel").slideToggle("fast");
        jQuery('.bottomshadow').css({
            marginLeft:249
        }, 500);
        jQuery('#id_content').css({
            marginLeft:249
        }, 500);
        jQuery.cookie('stat', 'yes');
        return false;
    });


    //    обработка стрелки выдв. менюхи
    jQuery("#button-out").click(function(){
        jQuery("#id_panel").slideToggle("fast");
        jQuery('.bottomshadow').css({
            marginLeft:0
        }, 500);
        jQuery('#id_content').css({
            marginLeft:0
        }, 500);
        jQuery("#button-in-out").show();
        jQuery.cookie('stat', 'no');
        return false;
    });
	
    var topPosition = jQuery('#contentNavBtns').offset();

    var widthRightCol = jQuery('.rightCol').css('width');
    if( widthRightCol) widthRightCol = widthRightCol.substr(0, widthRightCol.length-2) - 1;

    jQuery(document).scroll(function(){
        if( jQuery('#contentNavBtns').html() )
        {
            var wrapwidth = parseInt(jQuery('.wrap').css('width'));
            //determine top of #contentNavBtns
            if( window.pageYOffset > topPosition.top )
            {
                jQuery('#contentNavBtns').children('div.floatingInner').addClass('fixed');
                
                jQuery('#contentNavBtns').children('div.fixed').width( (wrapwidth)+'px' );
                jQuery('#contentNavBtns').children('div.fixed').css('marginLeft', (-(wrapwidth+2)/2)+'px' );
            }
            else
            {
                jQuery('#contentNavBtns').children('div').removeClass('fixed');
                jQuery('#contentNavBtns').children('div.floatingInner').width( '100%' );
                jQuery('#contentNavBtns').children('div').css('marginLeft', '0' );

            }
        }
    });

    /* menu */
    jQuery('#mainMenu li').hover(
        function(){
            jQuery(this).children('.submenuAct').show();
            jQuery(this).children('.submenu').show();
			
			
            var p = jQuery(this).parent();
            var offset = p.offset();
            //            p.html( "left: " + offset.left );
            //            alert("ширина родителя: "+(p.width()));
            //            alert('ширина суб меню'+jQuery(this).children('.submenu').width());
            //            alert(jQuery(this).offset().left);

            jQuery('.has_submenu .submenu').css('left','0');
            jQuery('.has_submenu .submenu .subsubmenu').css('left','200');

            var width_parent = p.width();
            var width_sub_menu = jQuery(this).children('.submenu').width();
            var offset_sub_menu = jQuery(this).offset().left;

            if ((width_sub_menu+offset_sub_menu)>width_parent){
                var off_left = (width_sub_menu+offset_sub_menu)-width_parent;
                jQuery(this).find('.submenu').css('left',-off_left+0);

            }
								

        },
        function(){
            jQuery(this).children('.submenuAct').hide();
            jQuery(this).children('.submenu').hide();
			
        }
        );

    jQuery('#mainMenu li .submenuItem').hover(
        function(){
            jQuery(this).children('.subsubmenu').show();
        },
        function(){
            jQuery(this).children('.subsubmenu').hide();
        }
        );

    /* menu */

    // Module links process

    //    jQuery('a.close').click( function(){
    //	jQuery('#addBox').slideUp();jQuery('.showHelp').show();
    //	jQuery.get('/admin/ajax/switch_desc/');
    //	return false;
    //    });
    //
    //    jQuery('a.showDescrFull').click(function() {
    //	jQuery('.module_descr_short').slideUp(function(){
    //	    jQuery('.module_descr_full').slideDown();
    //	});
    //	return false;
    //    });
    //
    //    jQuery('a.showDescrShort').click(function() {
    //	jQuery('.module_descr_full').slideUp(function(){
    //	    jQuery('.module_descr_short').slideDown();
    //	});
    //	return false;
    //    });
    //
    //    jQuery('.showHelp').click(function() {
    //	jQuery('#addBox').slideDown();
    //	jQuery(this).hide();
    //	jQuery.get('/admin/ajax/switch_desc/');
    //	return false;
    //    });
    //

    jQuery('a.addToShortcats').click(function()
    {
        params = {
            name: document.title,
            link: document.location.href
        };
        jQuery.post('/admin/shortcuts/add', params, function(data)
        {
            if (data.result == '1')
            {
                jQuery('a.addToShortcats').hide();
                jQuery('ul.subMenu_ li:last').after('<li><a href="'+ document.location.href +'">'+ data.name +'</a></li>');
            }
            else
            {
                if(data.result != '2')
                    alert('Error!');
            }

        }, 'json');

        return false;
    });

    jQuery('a.close').click( function(){
        jQuery('#addBox').slideUp();
        jQuery('.showHelp').show();
        jQuery.get('/admin/ajax/switch_desc/');
        return false;
    });

    jQuery('a.showDescrFull').click(function() {
        jQuery('.module_descr_short').slideUp(function(){
            jQuery('.module_descr_full').slideDown();
        });
        return false;
    });

    jQuery('a.showDescrShort').click(function() {
        jQuery('.module_descr_full').slideUp(function(){
            jQuery('.module_descr_short').slideDown();
        });
        return false;
    });

    jQuery('.showHelp').click(function() {
        jQuery('#addBox').slideDown();
        jQuery(this).hide();
        jQuery.get('/admin/ajax/switch_desc/');
        return false;
    });

    //
    //    jQuery('tr.even,tr.odd').live('mouseover',
    //        function(){jQuery(this).addClass('hover');}
    //
    //    );
    //
    //    jQuery('tr.even,tr.odd').live('mouseout',
    //        function(){jQuery(this).removeClass('hover');}
    //    );


    jQuery("select").each(
        function(){
            var id = jQuery(this).attr('id');
            if(!id)
            {
                jQuery(this).attr('id', jQuery(this).attr('name'));
                id = jQuery(this).attr('id');
            }

            if( !jQuery(this).attr('multiple') && !jQuery(this).hasClass('uncustomized') ) 
				/* 
					if this select 
					not multiple
					not has class ucustomized
					not in ie7 and on page with ckeditor
				*/
                if( jQuery.browser.version == '7.0' )
                {
                    if( jQuery('textarea#ckeditor').length == 0 ) 
                        jQuery(this).SelectCustomizer();
                }
                else            
                    jQuery(this).SelectCustomizer();
        }
        );



    // вставка для того что бы каждому селекту ставить зет индекс по уменьшению что бы
    // следующий не накладывался на предыдущий
    // 22.01.2011

    var c_zIndex = 9999;
    jQuery('.customSelectWrap').each(function() {
        if(c_zIndex > 0)
        {
            jQuery(this).css('zIndex', c_zIndex)
            c_zIndex--;
        }
    });
    // конец

    jQuery('a img[src$=del.png], a img[src$=del.gif]').parent('a').click(
        function()
        {
            if( !jQuery(this).hasClass('ajax') )
                return confirm(Chimera.lang.are_you_sure);
        }
        );

    //jQuery("input:checkbox").customCheckboxes();


    // Form Save&Update
    jQuery('button.formSave, button.formUpdate').click(function()
    {
        var obj = this;
        jQuery(obj).attr('disabled', 'true');
        formsaveupdate(jQuery(obj));
        jQuery(obj).attr('disabled', 'false');

        return false;
    });


    


    jQuery("a[name='is_bestseller']").click(function(){
        console.log(jQuery(this).children('img').attr('src'));

        if (jQuery(this).children('img').attr('src')==='/static/admin/images/plus.png')
        {
            jQuery(this).children('img').attr('src','/static/admin/images/minus.png');
        }
        else
        if (jQuery(this).children('img').attr('src')==='/static/admin/images/minus.png')
        {
            jQuery(this).children('img').attr('src','/static/admin/images/plus.png');
        }


        jQuery.post("/admin/goods/"+jQuery(this).attr('rel')+"/statusUpdateBest", null ,function(data){
            if(data != 1 ) alert('Server error');
        });
        return false;
    });
    jQuery("a[name='is_new']").click(function(){
        console.log(jQuery(this).children('img').attr('src'));

        if (jQuery(this).children('img').attr('src')==='/static/admin/images/plus.png')
        {
            jQuery(this).children('img').attr('src','/static/admin/images/minus.png');
        }
        else
        if (jQuery(this).children('img').attr('src')==='/static/admin/images/minus.png')
        {
            jQuery(this).children('img').attr('src','/static/admin/images/plus.png');
        }


        jQuery.post("/admin/goods/"+jQuery(this).attr('rel')+"/statusUpdateNews", null ,function(data){
            if(data != 1 ) alert('Server error');
        });
        return false;
    });
    jQuery("a[name='show_on_startpage']").click(function(){
        console.log(jQuery(this).children('img').attr('src'));

        if (jQuery(this).children('img').attr('src')==='/static/admin/images/plus.png')
        {
            jQuery(this).children('img').attr('src','/static/admin/images/minus.png');
        }
        else
        if (jQuery(this).children('img').attr('src')==='/static/admin/images/minus.png')
        {
            jQuery(this).children('img').attr('src','/static/admin/images/plus.png');
        }


        jQuery.post("/admin/goods/"+jQuery(this).attr('rel')+"/statusUpdatePage", null ,function(data){
            if(data != 1 ) alert('Server error');
        });
        return false;
    });


//	Change left panel height so it fits right panel
	jQuery(document).ready(function() {
		h = jQuery('#idpanel2').css('height');
		h = parseInt(h) + 120 + 'px';
		jQuery('#id_panel').css('max-height', h)
		jQuery('#id_panel .pubTypes').css('min-height', h).css('max-height', h);
	});


});

function formsaveupdate(thisObj)
{
    var act = '';
    act += jQuery('#list_action').val();
    if( act == '')
        act = jQuery(thisObj).attr('rel');
    else
        act += '/';

    thisObj.parents('form')
    .attr( 'action', act.replace('//','/') )
    .attr( 'method', 'POST')
    .submit();
    return false;
}

function chkWidget(id, status, del)
{
    jQuery.post('/admin/widgetsmanager/update', {
        id: id,
        status: status
    });
    if(del)
    {
        jQuery('#widget'+id).remove();
        var num = 0;
        jQuery('.widget').each(function(){
            num++
        });
        if(!num) jQuery("#cmsInfo").show();
    }
}

jQuery.fn.customCheckboxes = function(){
    jQuery(this).each(
        function()
        {
            var id,checked;
            if( !(id = jQuery(this).attr('id')) )
            {
                id = (jQuery(this).attr('name') + '_' + Math.random(1000)).replace(/[\[\].]/ig, '');
                jQuery(this).attr('id', id);
            }
            checked = this.checked ? ' checked' : '';
            jQuery(this).hide();
            jQuery(this).after('<a id="l' + id + '" href="#' + id + '" class="customcheckbox' + checked + '"></a>');
        }
        );

    jQuery('.customcheckbox').click(
        function()
        {
            jQuery(this.hash).trigger('click');
            return false;
        }
        );

    jQuery('input:checkbox').click(
        function()
        {
            var id = jQuery(this).attr('id');
            jQuery(this).attr('checked', jQuery(this).attr('checked') ? '' : 'checked');
            jQuery("#l"+id).toggleClass('checked');
            return false;
        }
        );


}

//var proxy_url = '/proxy.php';
//var ajax_url = 'http://www.otherdomain.com/ajax.php';
//
//jQuery(document).ready(function() {
//  var data = new Object();
//  data.field = 'value';
//  data.field1 = 'value1';
//  data.ajax_url = ajax_url;
//  jQuery.ajax({
//    type: 'POST', url: proxy_url, dataType: 'html', data: data,
//    success: function(res) {
//      flag = res.substring(0, 2);
//      if (flag == '0|') alert('Ошибка: ' + res.substring(2, res.length));
//      else alert('Удачно: ' + res);
//    },
//    error: function(xhr, er_type) { alert('Ошибка: ' + er_type); }
//  });
//});

//function setStatusClient ( a )
//    {
//            jQuery.post("/admin/currencies/"+a+"/statusUpdate", null ,function(data){
//                if(data != 1 ) alert('Server error');
//            });
//
//
//    }
function setStatusProduct ( a )
    {
            //console.log(a);
            var vv = jQuery("#good_id_"+a);

            //console.log(jQuery(vv).val());
            var bb = jQuery(vv).val().toString();
            console.log(bb);
            jQuery.post("/admin/goods/"+a+"/statusUpdate", {status:bb} ,function(data){
                if(data != 1 ) alert('Server error');
            });


    }
function setFilterCat( cat_id )
{
    jQuery('input[name=cat]').val( cat_id );
    jQuery('.btn.formFilter').click();
}


