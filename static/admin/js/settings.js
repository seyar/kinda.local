/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function loadDomains()
{
    $.post('/admin/settings/ajax', {action: 'domains_list'}, function(data)
    {
        i = 0;
        $('#DomainsTab table tr').each(function(){
            if (i>0)
                $(this).remove();
            i++;
        });

        var j=0, cssClass;

        for (i in data)
        {
            cssClass = ++j%2  ? 'odd' : 'even';
            tr =  '<tr class="' + cssClass + '">\
                    <td align="center">&nbsp;</td>\
                    <td align="">'+ data[i].http_host +'</td>\
                    <td align="">'+ data[i].name +'</td>\
                    <td align="">'+ data[i].domain_title +'</td>\
                    <td align="">'+ data[i].domain_template +'</td>\
                    <td nowrap><a href="#" onclick="return DomainEdit('+ data[i].id +', this)"><img '
                    +'src="/static/admin/images/edi.png" alt="edit" title="Edit"/></a>'
                    +'<a href="#" onclick="return DomainDelete('+ data[i].id +', this)" style="margin-left: 10px;"><img '
                    +'src="/static/admin/images/del.png" alt="delete" title="Delete"/></a></td>\
                  </tr>';

            $('#DomainsTab table tr:last').after(tr);
        }

        $('#DomainsTab input[name=domain_id]').val('0');
        DomainCancel();
        $('#DomainsTab').slideDown();
    }, 'json');
}

function DomainEdit(domain_id, obj)
{
    $('#domainFormDiv').slideUp();
    $('#DomainsTab tr').css('background-color', '');
    $(obj).parents('tr').css('background-color', '#CCCCCC');
    $.post('/admin/settings/ajax', {action: 'domains_edit', id: domain_id}, function(data)
    {
        var lang = '';
        $('#dom_language_options .selectitems').each(function(){            
            if( $(this).attr('title') == data[0].languages_id ) lang = $(this).children('span').text();
        });

        $('#DomainsTab input[name=dom_id]').val(domain_id);
        $('#DomainsTab input[name=dom_http_host]').val(data[0].http_host);
        $('#DomainsTab [name=dom_language]').val(data[0].languages_id);$('#DomainsTab #dom_language_iconselect').text(lang);
        $('#DomainsTab input[name=dom_title]').val(data[0].domain_title);
        $('#DomainsTab textarea[name=dom_keywords]').val(data[0].domain_keywords);
        $('#DomainsTab textarea[name=dom_description]').val(data[0].domain_description);
        $('#DomainsTab [name=dom_template]').val(data[0].domain_template);$('#DomainsTab #dom_template_iconselect').text(data[0].domain_template);
//        $('#domainFormDiv').slideDown();
    }, 'json');

    return false;
}

function DomainSave()
{
    $.post('/admin/settings/ajax', 'action=domains_save&' + $('#DomainsTab').serializeAnything(), function(data)
    {
        if (data=='1')
        {
            loadDomains();
            // $('a.showDomains').click();
        }
        else
            alert('Oopos, some error!');
    });

    return false;
}

function DomainDelete(domain_id, obj)
{
    $.post('/admin/settings/ajax', {action: 'domains_delete', id: domain_id}, function(data)
    {
        if (data=='1')
        {
            $(obj).parents('tr').remove();
            DomainCancel();
        }
        else
            alert('Oopos, some error!');
    });

    return false;
}

function DomainCancel()
{
    $('#DomainsTab tr').css('background-color', '');
    $('#DomainsTab :input[type=text],#DomainsTab textarea').val('');
//    $('#domainFormDiv').slideUp();
    return false;
}

// #############################
function langLoad()
{
    $.post('/admin/settings/ajax', {action: 'languages_list'}, function(data)
    {
        i = 0;
        $('#LanguagesTab table tr').each(function(){
            if (i>0)
                $(this).remove();
            i++;
        });

        var j=0, cssClass;

        for (i in data)
        {
            cssClass = ++j%2  ? 'odd' : 'even';
            tr =  '<tr class="' + cssClass + '">\
                    <td>&nbsp;</td>\
                    <td>'+ data[i].name +'</td>\
                    <td>'+ data[i].prefix +'</td>\
                    <td>'+ data[i].shortname+'</td>\
                    <td nowrap><a href="#" onclick="return LangEdit('+ data[i].id +', this)"><img '
                    +'src="/static/admin/images/edi.png" alt="edit" title="Edit"/></a>'
                    +' <a href="#" onclick="return LangDelete('+ data[i].id +', this)" style="margin-left: 10px;"><img '
                    +'src="/static/admin/images/del.png" alt="delete" title="Delete"/></a></td>\
                </tr>';

            $('#LanguagesTab tr:last').after(tr);
        }
        $('#LanguagesTab input[name=lang_id]').val('0');
        LangCancel();
//        $('#LanguagesTab').slideDown();
    }, 'json');
}


function LangShow()
{
    if( $('#langFormDiv').is(':visible') ) return false;
    LangCancel();
    $('#langFormDiv').slideDown();
    return false;
}

function LangEdit(domain_id, obj)
{
    $('#langFormDiv').slideUp();
    $('#LanguagesTab tr').css('background-color', '');
    $(obj).parents('tr').css('background-color', '#CCCCCC');
    $.post('/admin/settings/ajax', {action: 'languages_edit', id: domain_id}, function(data)
    {
        $('#LanguagesTab input[name=lang_id]').val(domain_id);
        $('#LanguagesTab input[name=lang_name]').val(data[0].name);
        $('#LanguagesTab input[name=lang_prefix]').val(data[0].prefix);
        $('#LanguagesTab input[name=lang_shortname]').val(data[0].shortname);

    $('#langFormDiv').slideDown();
    }, 'json');

    return false;
}

function LangSave()
{
    $.post('/admin/settings/ajax', 'action=languages_save&' + $('#LanguagesTab').serializeAnything(), function(data)
    {
        if (data=='1')
        {
            langLoad();
        }
        else
            alert('Oopos, some error!');
    });

    return false;
}

function LangDelete(domain_id, obj)
{
    $.post('/admin/settings/ajax', {action: 'languages_delete', id: domain_id}, function(data)
    {
        if (data=='1')
        {
            $(obj).parents('tr').remove();
            LangCancel();
        }
        else
            alert('Oopos, some error!');
    });

    return false;
}

function LangCancel()
{
    $('#LanguagesTab tr').css('background-color', '');
    $('#LanguagesTab :input').val('');
    $('#langFormDiv').slideUp();
    return false;
}
