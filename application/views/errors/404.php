<h1>The page "<?php echo Request::instance()->uri ?>" does not exist.</h1>
<p>Check to make sure you typed the address correctly or use the menu to navigate the site.</p>