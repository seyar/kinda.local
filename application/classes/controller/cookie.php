<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cookie extends Controller {
 
	public function action_get()
	{
		echo cookie::get(arr::get($_POST, 'name', ''));
	}
 
	public function action_set()
	{
		cookie::$path = arr::get($_POST, 'path', cookie::$path);
		cookie::$domain = arr::get($_POST, 'domain', cookie::$domain);
		cookie::$secure = (bool)arr::get($_POST, 'secure', cookie::$secure);
		cookie::set(arr::get($_POST, 'name', ''), arr::get($_POST, 'value', ''), arr::get($_POST, 'expiration', NULL));
	}
 
	public function action_delete()
	{
		cookie::delete(arr::get($_POST, 'name', ''));
	}
}
