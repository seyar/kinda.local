<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Content extends Controller_Quicky
{

    public $auto_render = FALSE;
    public $template = 'index.tpl';
    public $request_uri = '';
    public $page_info = array();
    public $blocks = array();

    public function before()
    {
        if (Kohana::config('settings')->site_stop)
        {
//            Model_Content::error();
            echo Kohana::config('settings')->site_stop_message;
            die();
        }
        parent::before();

        $this->model = new Model_Content();
        $this->request_uri = $this->model->parse_uri($this->request->uri());

        $this->view->assign('languages', Model_Content::get_languages_array());
        $this->view->assign('current_url', Request::instance()->uri());
        $domain_info = Model_Content::load_domain_info();
        $this->view->template_root = str_replace('//', '/', $domain_info['domain_template'] . '/');
    }

    public function after()
    {
        $this->view->assign('inter_info', Kohana_Controller_Quicky::$intermediate_vars);

        $this->view->assign('lang', $this->request->param('lang'));
        $lang_url = LANGUAGE_ROUTE_DEFAULT == $this->request->param('lang') ? '' : $this->request->param('lang') . '/';
        $this->view->assign('lang_url', $lang_url);

        $this->view->assign('controller', $this->request->controller);

        $this->view->assign('action', $this->request->action);
        $this->view->assign('path', $this->request->param('path'));
        $this->view->assign('id', ( @$this->request->param('id') ? $this->request->param('id') : NULL));

        $domain_info = Model_Content::load_domain_info();
        $template_name = str_replace('//', '/', $domain_info['domain_template'] . '/');

        $this->view->template_dir = TEMPLATEPATH . $template_name;
        $this->view->template_root = $template_name;

        $this->view->assign('template_root', '/templates/' . $template_name);                
        $this->view->assign('page', isset($this->page_info) && count($this->page_info) > 0 ? $this->page_info : $this->model->page_info);
        
        if( isset($this->model->page_info['page_template']) )
            $this->template = $this->model->page_info['page_template'];        
        $this->view->assign('blocks', Model_AdminGlobalBlocks::gb_frontend_load(CURRENT_LANG_ID));
        $this->view->assign('domain_info', $domain_info);

        $this->request->response .= $this->view->fetch($this->template);

        parent::after();
    }

    public function action_index()
    {   
        if (!$this->model->find_static_page(  Request::detect_uri() ))
        {
            Model_Content::error();
//            echo Request::factory('error/404')->execute();
            die();
        }
    }

    static public function sitemap()
    {
        return Model_Content::sitemap();
    }

    static public function sitemapXML()
    {
        return array();
    }

}