<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Search extends Controller_Quicky {

    public $auto_render = FALSE;
    public $template = 'search.tpl';

    public function after() {

        parent::after();

        $this->template = $this->template;

        $this->view->assign('controller', $this->request->controller);
        $this->view->assign('action', $this->request->action );
        $this->view->assign('path', $this->request->param('path') );
        $this->view->assign('lang', $this->request->param('lang') );

        $this->request->response .= $this->view->fetch($this->template);

    }

    public function action_index() {

        $this->view->assign('info', 'Search Quicky world!');

    }

}