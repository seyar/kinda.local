<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Errors extends Controller_Content
{
    public $template = 'errors/404.tpl';

    public function action_404()
    {
      $this->request->status = 404;
      $this->request->title = '404 : Page not found';
//      $this->template = 
//      $this->request->response = View::factory('errors/404');
    }
}