<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Content extends Model {

    static public $request;

    static public function error()
    {
        echo Request::factory('/error/404')->execute();
        exit();
        Model_Content::$request = Request::instance();
        Model_Content::$request->status = 404;
        Model_Content::$request->title = '404 : Page not found';
        Model_Content::$request->response = View::factory('errors/404');
        
        if (!IN_PRODUCTION)
        {
            Model_Content::$request->response .= "<br>Current routes: <br><br>";

            $uri = Request::instance()->uri();

            foreach (Route::all() as $rn=>$r)
            {
              Model_Content::$request->response .=  $rn . Kohana::debug($r->matches($uri));
            }
        }
        
        echo Model_Content::$request->send_headers()->response;
        exit();
    }

    static public function get_languages_array()
    {
	$query = DB::select('id','prefix', 'shortname')
		->from('languages')
		->order_by('id')
		->as_assoc()
		->cached(120)
		;

	$result = $query->execute();

	if ($result->count() == 0) return array();
	else
	{
	    return $result->as_array('id');
	}
    }

    static public function parse_uri($request_uri)
    {
	$lang_id = 0;
	
	$uri_parts = explode('/', $request_uri);

	$lang_array = Model_Content::get_languages_array();

	foreach ($lang_array as $value)
	{
	    if($value['prefix'] == Request::instance()->param('lang') )
		$lang_id = $value['id'];
	}
	
	unset($uri_parts[0]);
	
	$uri_rest = implode('/', $uri_parts);

	return (string)$uri_rest;
    }

    public function find_static_page($uri)
    {        
        $uri = $uri == '/' ? '' : '/'.rtrim($uri,'/');
        $uri = str_replace('//','/',$uri);
        if (!defined('CURRENT_LANG_ID'))
            Model_Content::error();
        
            $query = DB::query(Database::SELECT, 
            'SELECT content.* FROM content '
            .' WHERE status > 0 '
            .($uri ? " AND REPLACE( CONCAT(parent_url,'/',page_url), '//', '/') = '$uri'" : ' and is_default = 1' )
            ." AND languages_id = " . CURRENT_LANG_ID
            .' LIMIT 1')
            ->cached(10)
            ;
            
        $result = $query->execute();

        if ($result->count() == 0) self::error();

        $page_info = $result->current();

        $this->template = $page_info["page_template"];

        $this->page_info = $page_info;        
        $this->blocks = Model_Content::load_global_blocks();

        return true;
    }

    static public function load_domain_info()
    {
	$query = DB::select()
		->from('domains')
		->where('http_host', '=', $_SERVER['HTTP_HOST'])
		->limit(1)
        ->cached(7200)
		;
	$result = $query->execute();

	if ($result->count() == 0) return array(
						    'http_host' => $_SERVER['HTTP_HOST'],
						    'languages_id' => CURRENT_LANG_ID,
						    'domain_title' => 'Default Template',
						    'domain_keywords' => 'Keywords',
						    'domain_description' => 'Description',
						    'domain_template' => 'default',
						);
	else
	{           
	    return $result->current();
	}
    }

    static public function load_global_blocks()
    {
        $query = DB::select('id','name','content')
			->from('globalblocks')
			->cached(60)
			;

	$query->where('languages_id', '=', CURRENT_LANG_ID);

	$result = $query->execute();
	if ($result->count() == 0) return array();
	else
	{
	    $return = array();
	    foreach($result->as_array() as $value)
		    $return[$value['name']] = $value['content'];
	    return $return;
	}
    }
    
    static public function sitemap($lang_id = 1)
    {
        $res = DB::select(
                                'id',
                                'parent_id',
                                array(DB::expr('0.8'), 'priority'),
                                array('page_name', 'name'),
                                array(DB::expr("CONCAT(parent_url, page_url)"), 'page_url')
                            )
			    ->from('content')
			    ->where('status', '=', 1)
			    ->and_where('languages_id', '=', $lang_id)
			    ->and_where('visible_to', '=', 'all')
			    ->and_where('hide_from_map', '!=', '1')
                            ->execute()
                            ->as_array('id');
        return $res;
    }

    /**
     * function generate html select from custom keys, fork of Form::select
     *
     * @param string            $name
     * @param array             $options
     * @param int,string        $selected
     * @param array             $attributes
     * @param int,string        $valuekey
     * @param int,string        $titleKey
     * @return htmlselect
     */
    static public function customSelect($name, array $options = NULL, $selected = NULL, array $attributes = NULL, $valuekey = NULL, $titleKey = NULL)
    {        
        if( $valuekey || $titleKey)
        {
            $newoptions = array();
            foreach($options as $key=>$item)
            {                
                if( is_array($titleKey) && count($titleKey) > 0 )
                {
                    $val = '';
                    foreach( $titleKey as $i ) $val .= array_key_exists($i, $item) ? $item[$i] : $i;
                    $newoptions[$item[$valuekey]] = $val;
                }
                elseif(isset($valuekey) &&  !empty($valuekey) )
                    $newoptions[$item[$valuekey]] = $item[$titleKey];
                else
                    $newoptions[$key] = $item[$titleKey];
            }
        }
        else
        {
            $newoptions = $options;
        }
        return Form::select($name, $newoptions, $selected, $attributes);
    }

    /**
     * forked from Arr::get, add !empty value
     * 
	 * Retrieve a single key from an array. If the key does not exist in the
	 * array, the default value will be returned instead.
	 *
	 *     // Get the value "username" from $_POST, if it exists
	 *     $username = Arr::get($_POST, 'username');
	 *
	 *     // Get the value "sorting" from $_GET, if it exists
	 *     $sorting = Arr::get($_GET, 'sorting');
	 *
	 * @param   array   array to extract from
	 * @param   string  key name
	 * @param   mixed   default value
	 * @return  mixed
	 */
	public static function arrGet($array, $key, $default = NULL)
	{        
		return isset($array[$key]) && !empty($array[$key]) ? $array[$key] : $default;
	}
}
