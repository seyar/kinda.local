<?php  defined('SYSPATH') or die('No direct script access.');
/* @version $Id: v 0.1 09.11.2010 - 12:18:03 Exp $
 *
 * Project:     bnn.local
 * File:        string.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */

class Helper_String extends Kohana_Text
{
    static $sklon_url = 'http://export.yandex.ru/inflect.xml?format=json&name=';
    /**
     * function is sklonyaet words
     * Вася Пупкин
     * ->Вася Пупкин
     * ->Васи Пупкина
     * ->Васе Пупкину
     * ->Васю Пупкина
     * ->Васей Пупкиным
     * ->Васе Пупкине
     *
     * @param   string        $word
     * @param   int           nomer padezha v rus yaz
     * @return  array words
     * @return  word
     */
    public static function yandex_sklon($word, $padezh = NULL)
    {        
        $cache = Cache_Sqlite::instance();
        $data = $cache->get( 'text_'.$word );

        if( !$data )
        {
            $data = file_get_contents( urldecode(self::$sklon_url.$word) );
            $cache->set('text_'.$word, $data);
        }
        
        $data = json_decode($data, TRUE);
        /* надо предусмотреть если не удалось построить склонение  */
        if( isset($data['inflection']) && !empty($data['inflection']) ) return $word;

        if( isset($padezh) && !empty($padezh) ) return $data[$padezh];
        return $data;
    }
}
?>