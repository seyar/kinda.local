<?php defined('SYSPATH') OR die('No direct access allowed.');

/*
 * Usage examples:
 *     Helper_Num::$words = array('год', 'года', 'лет'); - указываем перед применением на странице, три формы склонения слова
 *     echo Helper_Num::decl(1);  // return 1 год
 *     echo Helper_Num::decl(2);  // return 2 года
 *     echo Helper_Num::decl(10);  // return 10 лет

 */
class Helper_Num extends Kohana_Num {

    public static $words = array('комментарий', 'комментария', 'комментариев');

    public static $N0 = 'ноль';
    public static $Ne0 = array(
                0 => array('','один','два','три','четыре','пять','шесть',
                           'семь','восемь','девять','десять','одиннадцать',
                           'двенадцать','тринадцать','четырнадцать','пятнадцать',
                           'шестнадцать','семнадцать','восемнадцать','девятнадцать'),
                1 => array('','одна','две','три','четыре','пять','шесть',
                           'семь','восемь','девять','десять','одиннадцать',
                           'двенадцать','тринадцать','четырнадцать','пятнадцать',
                           'шестнадцать','семнадцать','восемнадцать','девятнадцать')
                );
    public static $Ne1 = array('','десять','двадцать','тридцать','сорок','пятьдесят',
                'шестьдесят','семьдесят','восемьдесят','девяносто');
    public static $Ne2 = array('','сто','двести','триста','четыреста','пятьсот',
                'шестьсот','семьсот','восемьсот','девятьсот');
    public static $Ne3 = array(1 => 'тысяча', 2 => 'тысячи', 5 => 'тысяч');
    public static $Ne6 = array(1 => 'миллион', 2 => 'миллиона', 5 => 'миллионов');
    /* end to word */

    public static function decl($number , $array = FALSE, $only_word = FALSE )
    {
        if($array == FALSE)
            $array = self::$words;

        $cases = array (2, 0, 1, 1, 1, 2);

        if($only_word)
        {
            if ($number % 100 > 4 AND $number % 100 < 20)
            {
                return $array[2];
            }
            else
            {
                return $array[$cases[min($number % 10, 5)]];
            }
        }else{
            if ($number % 100 > 4 AND $number % 100 < 20)
            {
                return $number.' '.$array[2];
            }
            else
            {
                return $number.' '.$array[$cases[min($number % 10, 5)]];
            }
        }
    }

    // start - Числа в строку

    public static function written_number($i, $female=false)
    {
     if ( ($i<0) || ($i>=1e9) || !is_int($i) ) {
       return false; // Аргумент должен быть неотрицательным целым числом, не превышающим 1 миллион
     }
     if($i==0) {
       return self::$N0;
     }
     else {
       return preg_replace( array('/s+/','/\s$/'),
                            array(' ',''),
                            self::num1e9($i, $female));
       return self::num1e9($i, $female);
     }
    }

    static function num_125($n) {
     /* форма склонения слова, существительное с числительным склоняется
      одним из трех способов: 1 миллион, 2 миллиона, 5 миллионов */
     $n100 = $n % 100;
     $n10 = $n % 10;
     if( ($n100 > 10) && ($n100 < 20) ) {
       return 5;
     }
     elseif( $n10 == 1) {
       return 1;
     }
     elseif( ($n10 >= 2) && ($n10 <= 4) ) {
       return 2;
     }
     else {
       return 5;
     }
    }

    static function num1e9($i, $female) {
     if($i<1e6) {
       return self::num1e6($i, $female);
     }
     else {
       return self::num1000(intval($i/1e6), false) . ' ' .
         self::$Ne6[num_125(intval($i/1e6))] . ' ' . self::num1e6($i%1e6, $female);
     }
    }

    static function num1e6($i, $female) {
     if($i<1000) {
       return self::num1000($i, $female);
     }
     else {
       return self::num1000(intval($i/1000), true) . ' ' .
         self::$Ne3[self::num_125(intval($i/1000))] . ' ' . self::num1000($i%1000, $female);
     }
    }

    static function num1000($i, $female) {
     if( $i<100) {
       return self::num100($i, $female);
     }
     else {
       return self::$Ne2[intval($i/100)] . (($i%100)?(' '. self::num100($i%100, $female)):'');
     }
    }

    static function num100($i, $female)
    {
     
     $gender = $female?1:0;
     if ($i<20) {
       return self::$Ne0[$gender][$i];
     }
     else {
       return self::$Ne1[intval($i/10)] . (($i%10)?(' ' . self::$Ne0[$gender][$i%10]):'');
     }
    }

    

    // end - Числа в строку
}
