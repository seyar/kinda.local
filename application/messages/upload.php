<?php

return array
(
	'picture' => array(
	'Upload::valid'    	=> 'Upload Error',
    	'Upload::not_empty'    	=> 'Empty upload',
    	'Upload::type'    	=> 'Incorrect file type',
    	'Upload::size'    	=> 'Too big file',
    	'default'  		=> 'Error',
        ),
);