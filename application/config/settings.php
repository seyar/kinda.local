<?php defined('SYSPATH') OR die('No direct access allowed.');

return array (
  'site_name' => 'Сайт Kinda',
  'site_owner' => 'Kinda',
  'admin_email' => 'seyar@inbox.ru',
  'url_autofix' => true,
  'session_timeout' => 43200,
  'session_ip_lock' => true,
  'mail_from_name' => 'Kinda',
  'mail_from_address' => 'seyar@inbox.ru',
  'cache_lifetime' => 60,
  'cached_objects' => 
  array (
    'Images' => false,
    'JavaScript' => true,
    'CSS' => true,
    'HTML' => false,
    'SQL' => false,
  ),
  'site_stop' => FALSE,
  'site_stop_message' => 'Cайт находится в стадии разработки',
);
