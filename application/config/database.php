<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	'default' => array
	(
		'type'       => 'mysql',
		'connection' => array(
			/**
			 * The following options are available for MySQL:
			 *
			 * string   hostname
			 * integer  port
			 * string   socket
			 * string   username
			 * string   password
			 * boolean  persistent
			 * string   database
			 */
			'hostname'   => 'localhost',
			'username'   => 'root',
			'password'   => '', 
			'persistent' => FALSE,
			'database'   => 'kinda',
		),
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => IN_PRODUCTION,
		'profiling'    => TRUE,
	),
	'default_prod' => array
	(
		'type'       => 'mysql',
		'connection' => array(
			/**
			 * The following options are available for MySQL:
			 *
			 * string   hostname
			 * integer  port
			 * string   socket
			 * string   username
			 * string   password
			 * boolean  persistent
			 * string   database
			 */
			'hostname'   => 'localhost',
			'username'   => 'kinda507_db',
			'password'   => 'yLr2XTqg', 
			'persistent' => FALSE,
			'database'   => 'kinda507_db',
		),
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => IN_PRODUCTION,
		'profiling'    => TRUE,
	),
);
