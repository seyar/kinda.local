<?php defined('SYSPATH') OR die('No direct access allowed.');

return array (
  'registration' => MODPATH.'registration',
  'publications' => MODPATH.'publications',
  'photogallery' => MODPATH.'photogallery',
//  'comments' => MODPATH.'comments',
  'questions' => MODPATH.'questions',
  'search' => MODPATH.'search',
  'votes' => MODPATH.'votes',
  'rss' => MODPATH.'rss',
  'feedback' => MODPATH.'feedback',
  'templater' => MODPATH.'templater',
  'shop' => MODPATH.'shop',
  'shopimport' => MODPATH.'shop_import',
  'shopexport' => MODPATH.'shop_export',
  'soap1c' => MODPATH.'soap1c',
  'parser' => MODPATH.'parsergood',
  'sitemap' => MODPATH.'sitemap',
  'downloads' => MODPATH.'downloads',
);