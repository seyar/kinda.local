<?php defined('SYSPATH') or die('No direct script access.');

//-- Environment setup --------------------------------------------------------

/**
 * Set the default time zone.
 *
 * @see  http://docs.kohanaphp.com/about.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('Europe/Kiev');

/**
 * Set the default locale.
 *
 * @see  http://docs.kohanaphp.com/about.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'ru_RU.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://docs.kohanaphp.com/about.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

/**
 * Set the production status by the domain.
 */

define('IN_PRODUCTION', !strpos($_SERVER['SERVER_NAME'],'.loc') );

//-- Configuration and initialization -----------------------------------------

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(
        array(
        'base_url' => '',
        'index_file' => '',
        'cache_dir' => DOCROOT.'cache/internal',
        'profiling' => ! IN_PRODUCTION,
        'caching' => IN_PRODUCTION,
        )
);

//Kohana::$caching_merged['status'] = TRUE;

Kohana::$log_errors = IN_PRODUCTION;

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Kohana_Log_File(DOCROOT.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Kohana_Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */

$defaultModules = array(
    'sprig-aacl' => MODPATH . 'sprig-aacl', // Sprig AACL
    'sprig-auth' => MODPATH . 'sprig-auth', // Sprig Auth
    'sprig' => MODPATH . 'sprig', // Sprig ORM
    'auth' => MODPATH . 'auth', // Basic authentication
    'cache' => MODPATH . 'cache', // Cache via SQLite, Memcached, Memcached-tags, eAccelerator, APC
    'database' => MODPATH . 'database', // Database access
    'image' => MODPATH . 'image', // Image manipulation
    'quicky' => MODPATH . 'quicky', // Quicky template engine
    'pagination' => MODPATH . 'pagination', // Paging of results
    'event' => MODPATH . 'event', // Event tool
    'debug_toolbar' => MODPATH . 'debug_toolbar', // Debug tool
    'admin' => MODPATH . 'admin', // Admin
    'sitemap' => MODPATH . 'sitemap', // Sitemap
    'captcha' => MODPATH . 'captcha', // Images form protection
    'email' => MODPATH . 'email', // Kohana Email
    'messages' => MODPATH . 'messages', // Kohana mess
    
);

$userConfigFiles = Kohana::find_file('config', 'userModules');

$userModules = require $userConfigFiles[0];

Kohana::modules(array_merge($defaultModules, $userModules));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */

Event::run('system.ready');

// Turn On Route cache

if( ! Route::cache() )
{
    Route::set(
                'cookie_ajax'
                , 'cookie(/<action>)'
                , array(
                    'action'    => '(?:set|get|delete)',
                )
            )
            ->defaults(
                array(
                    'controller'    => 'cookie'
                    ,'action'        => 'get'
                )
            )
    ;

    Route::set(
		'images'
		, 'image/<file>'
		, array('file' => '.+.(?:jpe?g|png|gif)')
	    )
	    ->defaults
	    (
		array(
		    'controller' => 'images',
		    'action' => 'index',
		)
	    )
    ;

    Route::set(
                'search'
                , 'search(/<path>)'
                , array('path' => '.+')
            )
            ->defaults(
                array(
                    'controller'    => 'search'
                    ,'action'        => 'index'
                )
            )
    ;
    
    Route::set(
                'errorPage'
                , '(<lang>/)error(/<action>)'
                , array('action' => '[0-9]+','lang' => '(?:'. LANGUAGE_ROUTE .')', )
            )
            ->defaults(
                array(
                    'controller'    => 'errors'
                    ,'action'        => '404'
                    ,'lang'         => LANGUAGE_ROUTE_DEFAULT
                )
            )
    ;

    Route::set(
                'content_index'
                , '<lang>'
                , array('lang' => '(?:'. LANGUAGE_ROUTE .')', )
            )
            ->defaults(
                array(
                    'controller'    => 'content'
                    ,'action'       => 'index'
                    ,'lang'         => LANGUAGE_ROUTE_DEFAULT
                )
            )
    ;

    Route::set(
                'default'
//                , '(<lang>/)(/)content(/<path>)'
                , '(<lang>/)(<path>)'
                , array('path' => '.+', 'lang' => '(?:'. LANGUAGE_ROUTE .')', )
            )
            ->defaults(
                array(
                    'controller'    => 'content'
                    ,'action'       => 'index'
                    ,'lang'         => LANGUAGE_ROUTE_DEFAULT
                )
            )
    ;

    // Cache the routes
    if(IN_PRODUCTION) Route::cache(TRUE);

}

/*
$uri = '';
$uri = Request::instance()->uri();

foreach (Route::all() as $r)
{
  echo Kohana::debug($r->matches($uri));
}

echo Kohana::debug(Route::all());
exit;
*/

Event::run('system.routing');

/**
 * Execute the main request. A source of the URI can be passed, eg: $_SERVER['PATH_INFO'].
 * If no source is specified, the URI will be automatically detected.
 */

$request = Request::instance($_SERVER['PATH_INFO']);

try {
    $request->execute();    
}
catch (Kohana_Exception404 $e) {    
    $request = Request::factory('error/404')->execute();    
}
catch (Kohana_Exception403 $e) {
    $request = Request::factory('error/403')->execute();
}
catch (ReflectionException $e) {
    $request = Request::factory('error/404')->execute();
}
catch (Exception $e) {

    if ( ! IN_PRODUCTION) {
        // Just re-throw the exception
        throw $e;
    }

    // Log the error
    Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));

    // Create a 404 response
    $request->status = 404;
    $request->response = View::factory('error/404');
}

/**
* Display the request response.
*/

Event::run('system.execute');

echo $request->send_headers()->response;

Event::run('system.response');

if ( (!IN_PRODUCTION || $_SERVER['REMOTE_ADDR'] == '178.137.177.34') && class_exists('DebugToolbar') )
    echo DebugToolbar::render();
