{include file="header.tpl"}
                
<!-- header -->

<div class="centerColInner">
    <div class="rightColInner rel newContent">
        <h1 class="colTitle tLeft">Поздравляем.</h1>
        <br />
        <div class="text">
            <p><b>Ваш заявка была передана в обработку</b></p>
            <p><b>Наш менеджер свяжется с вами, для уточнения деталей заказа</b></p>
        </div>                    
    </div>                    
</div>

<div class="leftColInner">
    {include file="shop_menu.tpl"}
</div>
<div class="clear"></div>

<br />

{include file="menu.tpl"}

<!-- footer -->
				
{include file="footer.tpl"}