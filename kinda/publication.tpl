{include file="header.tpl"}
                
<!-- header -->

<div class="centerColInner">
    <div class="rightColInner rel newContent">
        <h1 class="colTitle tLeft">{$obj.pub_name}</h1>
        <p class="date">&ndash; {$obj.pub_date|date_format:'%d. %m. %Y'}</p>
        <br />
        <div class="text">
            {if $obj.pub_image}
            <a class="fancy" href="{$obj.images.webpath}/{$obj.pub_image}">
                           <img align="left" src="{$images_folder}{$obj.id}/prev_{$obj.pub_image}" alt="{$obj.pub_name}" />
                       </a>
            {/}
            {$obj.pub_fulltext}
        </div>                    
    </div>                    
</div>

<div class="leftColInner">
    <p class="colTitle"><a href="/news/">Новости</a></p>
    <div class="emptyBlock"></div>
    
    {if $lastNews}
    <ul class="leftNews">
        {foreach from=$lastNews item=value}
        <li>
            <a href="/news/{$value['pub_url']}">{$value.pub_name}</a>
            <p class="date">&mdash;{$value.pub_date|date_format:'%d. %m. %Y'}</p>
            <div class="bottomTaxiBorder"><div></div></div>
        </li>
        {/}
    </ul>
    {else}
    Новости не найдены.
    {/}
</div>
<div class="clear"></div>

<br />

{include file="menu.tpl"}

<!-- footer -->
				
{include file="footer.tpl"}