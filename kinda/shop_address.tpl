{include file="header.tpl"}

<!-- header -->
            <form action="/shop/order/{$customer['default_address_id']}/addresssave" method="post" id="regForm">
                <input type="hidden" value="{$customer['id']}" name="customerId"/>
                <div class="regRows cols flLeft">
                    <p class="colTitle">Мой профиль</p>
                        <div>
                            <p>Электронная почта</p>
                            <!--<input type="text" name="email" value="{$customer.email|default:$quicky.post.email}" class="email required" readonly="true" />-->
                            {$customer.email|default:$quicky.post.email}
                        </div>
                        <div>
                            <p>Полное имя</p>
                            <input type="text" name="name" class="required" value="{$customer.name|default:$quicky.post.name}" rel="Введите полное имя"/>
                        </div>
                        <div>
                            <p>Мобильный телефон</p>
                            <input type="text" name="phone" value="{$customer_address.phone|default:$quicky.post.phone}" class="required" rel="Введите мобильный телефон"/>
                        </div>
                        <div>
                            <p>Страна</p>
                            <input type="text" name="country" value="{$customer_address.country|default:$quicky.post.country}" class="required" rel="Введите странy"/>
                        </div>
                        <div>
                            <p>Область</p>
                            <input type="text" name="state" value="{$customer_address.state|default:$quicky.post.district}" class="required" rel="Введите область"/>
                        </div>
                        <div>
                            <p>Город</p>
                            <input type="text" name="city" value="{$customer_address.city|default:$quicky.post.city}" class="required" rel="Введите город"/>
                        </div>
                        <div>
                            <p>Адрес</p>
                            <input type="text" name="address" value="{$customer_address.address_line1|default:$quicky.post.address}"/>
                        </div>
                        <div class="rel">
                            <a href="#" class="addBtn absBlocks tCenter" style="left: 206px;" onclick="$('#regForm').submit();" >Сохранить</a>
                        </div>
                </div>
            </form>
            <form action="/profile/passupdate" method="post" id="passForm" onsubmit="if( !checkPasses($('#passForm')) ) return false;">
                <div class="cols regRows flRight">
                    <p class="colTitle">&nbsp;</p>
                    <div>
                        <p>Пароль</p>
                        <input type="password" name="current_pass" value="" class="required" rel="Введите Пароль"/>
                    </div>
                    <div>
                        <p>Новый пароль</p>
                        <input type="password" name="new_pass" class="required" value="" rel="Введите Новый пароль"/>
                    </div>
                    <div>
                        <p>Повторите новый пароль</p>
                        <input type="password" name="new_pass2" value="" class="required" rel="Введите Повтор нового пароля"/>
                    </div>
                    <div class="rel">
                        <a href="#" class="addBtn absBlocks tCenter" style="left:206px;" onclick="if( checkPasses($('#passForm') )) $('#passForm').submit();return false;">Сохранить</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </form>
                
                
				<br />
                {literal}
                <script type="text/javascript">
                    function checkPasses( obj )
                    {
                        var err = [];
                        var i = 0;
                        
                        if( $('[name=new_pass]').val() != $('[name=new_pass2]').val() ){ err[i] = 'Пароли не совпадают';i++;}
                            
                        $(obj).find('.required').each(
                            function()
                            {
                                if( !$(this).val() )
                                {
                                    err[i] = $(this).attr('rel');
                                    i++;
                                }
                            }
                        );

                        if( err.length > 0 )
                        {
                            myAlert( err.join(',')  ,'err');
                            return false;
                        }else
                            return true;
                    }
                </script>
                {/literal}
{include file="menu.tpl"}
{include file="footer.tpl"}
