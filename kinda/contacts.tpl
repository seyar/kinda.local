{include file="header.tpl"}
                <!-- header -->
            <div class="contactsPage">
                <h1 class="colTitle tLeft">Контактная информация</h1>
                <br />
                {$page.page_content}
<!--                <div class="cols flLeft">
                    <p class="addressCities">Офис в симферополе</p>
                    <br />
                    
                    <table class="citiesTab">
                        <tr>
                            <td>Адрес:</td>
                            <td>Украина, АР Крым г.Симферополь ул.Луговая 6, строение 18</td>
                        </tr>
                        <tr>
                            <td>Телефоны:</td>
                            <td>
                                (0652) 69-25-25<br />
                                (050) 808-04-08<br />
                                (067) 654-37-37
                            </td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><a href="javascript:void(0)">kinda@home.cris.net</a></td>
                        </tr>
                    </table>
                </div>
                <div class="cols flRight">
                    <p class="addressCities">Офис в киеве</p>
                    <br />
                    <table class="citiesTab">
                        <tr>
                            <td>Адрес:</td>
                            <td>Украина, г.Киев ул.Вербовая 17</td>
                        </tr>
                        <tr>
                            <td>Телефоны:</td>
                            <td>
                                (050) 910-66-66
                            </td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><a href="javascript:void(0)">kinda_ko@mail.ru</a></td>
                        </tr>
                    </table>
                </div>-->
                <div class="clear"></div>
                <br />
                <br />
                <div class="cols flLeft">
                    
                    <p class="colTitle">Карта проезда</p>
                    <br />
                    <br />
                    <div class="map">{$blocks.karta_na_kontaktax}</div>
                </div>
                
                <form action="/feedback/send" method="post" id="contForm" onsubmit="if( !checkLogin($('#contForm')) ) return false;">
                <input type="hidden" name="mail_template" value="mail_ru"/>
                <input type="checkbox" name="agree" value="1" style="position:absolute;top:0;left:-10000px;"/>
                <input type="hidden" name="referer" value="contacts.html"/>
                
                <div class="cols flRight contactsPage contForm">
					{if $quicky.GET.success|isset}
					Ваше сообщение отправлено.
					{else}
                    <p class="colTitle" style="">Напишите нам</p>
                    <br />
                        <div>
                            <p>Полное имя <span class="red">*</span></p>
                            <input type="text" name="feedback_fio" value="{$quicky.post.name}" rel="Введите имя" class="required"/>
                        </div>
                        <div>
                            <p>Электронная почта <span class="red">*</span></p>
                            <input type="text" name="feedback_email" value="{$quicky.post.email}" rel="Введите email" class="required email"/>
                        </div>
                        <div>
                            <p>Cообщение <span class="red">*</span><span class="grayText">(Предложение или пожелание)</span></p>
                            <textarea name="feedback_body"  rel="Введите сообщение" class="required">{$quicky.post.comment}</textarea>
                        </div>
                    <a href="#" class="addBtn flLeft" id="sendFeedbackMessage"> &nbsp;Отправить сообщение</a>
                    <div class="clear"></div>
                    {/}
                </div>
                </form>
                <div class="clear"></div>
            </div>
				
				{include file="menu.tpl"}
				
                <!-- footer -->
                <script src="{$template_root}js/feedback.js"></script>
{include file="footer.tpl"}
