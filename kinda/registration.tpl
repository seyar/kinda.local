{include file="header.tpl"}
            <!-- header -->
            <form action="/registration/add" method="post" id="regForm">
                <div class="regRows cols flLeft">
                    <p class="colTitle">Форма регистрации</p>
                        <div>
                            <p>Электронная почта <span class="red">*</span></p>
                            <input type="text" name="email" value="{$quicky.post.email}" class="email required" rel="Введите Email"/>
                        </div>
                        <div>
                            <p>Полное имя <span class="red">*</span></p>
                            <input type="text" name="name" class="required" value="{$quicky.post.name}" rel="Введите полное имя"/>
                        </div>
                        <div>
                            <p>Мобильный телефон <span class="red">*</span></p>
                            <input type="text" name="phone" value="{$quicky.post.phone}" class="required" rel="Введите мобильный телефон"/>
                        </div>
                        <div>
                            <p>Страна <span class="red">*</span></p>
                            <input type="text" name="country" value="{$quicky.post.country}" class="required" rel="Введите странy"/>
                        </div>
                        <div>
                            <p>Область <span class="red">*</span></p>
                            <input type="text" name="district" value="{$quicky.post.district}" class="required" rel="Введите область"/>
                        </div>
                        <div>
                            <p>Город <span class="red">*</span></p>
                            <input type="text" name="city" value="{$quicky.post.city}" class="required" rel="Введите город"/>
                        </div>
                        <div>
                            <p>Адрес</p>
                            <input type="text" name="address" value="{$quicky.post.address}"/>
                        </div>
                        <div>
                            <p>Cообщение <span class="grayText">(Предложение или пожелание)</span></p>
                            <textarea name="comment">{$quicky.post.comment}</textarea>
                        </div>
                        <div class="rel">
                            <span class="red">*</span> - <span class="grayText">Обязательные поля</span>
                            <a href="#" class="sendBtn absBlocks" id="register">Зарегистрироваться</a>
                            <div class="clear"></div>
                        </div>
                </div>
            </form>
                
                <div class="cols textRows flRight">
                    <p class="colTitle">Преимущества регистрации</p>
                    <div class="num1">
						{$blocks.reg_preimywestva_1}                        
                    </div>
                    <div class="num2">
						{$blocks.reg_preimywestva_2}                        
                    </div>
                    <div class="num3">
						{$blocks.reg_preimywestva_3}                        
                    </div>
                </div>
                
                
                
				<br />
				
<!--				<div class="absBlocks teaser">
					<div class="teaserInner">
						<img src="{$template_root}images/image1.jpg" alt=""/>
						<img src="{$template_root}images/image1.jpg" alt=""/>
					</div>
				</div>-->

                {include file="menu.tpl"}
				
                <!-- footer -->
                <script src="{$template_root}js/registration.js"></script>
{include file="footer.tpl"}
