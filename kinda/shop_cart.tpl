{include file="header.tpl"}
{php}$this->assign('first', Model_Frontend_Categories::getFirst(1));{/}
                <!-- header -->
                <h2><span style="background-color: #F1F8FE;">Список товаров, выбранных вами</span></h2>
                <table class="cartTab">
                    <tr>
                        <th colspan="3">&nbsp;</th>
                        <th>Цена 1 товара</th>
                        <th>Количество</th>
                        <th>Общая стоимость</th>
                    </tr>
                {if $rows}
                    {foreach from=$rows item=item key=key name="bla"}
                    <tr class="{if $quicky.foreach.bla.iteration % 2 == 0}even{/}">
                        <td class="cellDel tCenter">
                            <a href="#" onclick="if(confirm('Удалить товар из корзины?')) deleteFromCart('{$item.id}', this); return false;">
                                <img src="{$template_root}images/del.gif" alt="delete"/>
                            </a>
                        </td>
                        <td class="imageCell"><p><a href="/shop/category/{$item.category_url|escape:'url'}_{$item.categories_id}/{$item.seo_url|escape:'url'}_{$item.id}.html"><img src="{if $item.default_photo}/static/media/shop/{$item.id}/prev_{$item.default_photo}{else}/static/media/no_photo.jpg{/if}" alt=""/></a></p></td>
                        <td><a href="/shop/category/{$item.category_url|escape:'url'}_{$item.categories_id}/{$item.seo_url|escape:'url'}_{$item.id}.html">{$item.name|default:$item.alter_name}</a> 
                            <p class="scuCart">Артикул - <span>{$item['scu']}</span></p>
                        </td>
                        <td class="priceCell tRight">{$item.prices[1][1]} <p>грн/шт.</p></td>
                        <td class="amountCell tRight">
                            <input type="text" name="amount[{$item.id}]" class="unblur amount tCenter" value="{$item.amount}" 
                                                           onkeyup="if(this.value == 0) this.value=1; if(! change_amount('{$item.id}', this)) this.value = this.value.replace(/[\D]*/g, ''); return false;"/>
                            <p>штук</p>
                        </td>
                        <td class="sumCell tRight"><span class="good_price">{($item.prices[1][1]*$item.amount)|string_format:"%.2F"}</span> <p>грн.</p></td>
                    </tr>
                    {/}
                {else}
                    <tr>
                        <td colspan="6">
                            Товары не добавлены в корзину.
                        </td>
                    </tr>
                {/}
                <tr>
                    <td colspan="5" class="total tRight">Итого:</td>
                    <td class="totalSumCell tRight"><span id="cart_global_sum">{$total_sum|string_format:"%.2F"}</span><p>грн.</p></td>
                </tr>
                </table>
<!--                    <tr class="">
                        <td class="cellDel tCenter"><a href="#"><img src="{$template_root}images/del.gif" alt=""/></a></td>
                        <td class="imageCell"><p><a href="#"><img src="{$template_root}images/image1.jpg" alt=""/></a></p></td>
                        <td><a href="#">Открытка к торжеству ручной работы</a> 
                            <p class="scuCart">Артикул - <span>170122</span></p>
                        </td>
                        <td class="priceCell tRight">19 <p>грн/шт.</p></td>
                        <td class="amountCell tRight">
                            <input type="text" name="" value="222"/>
                            <p>штук</p>
                        </td>
                        <td class="sumCell tRight">3931 <p>грн.</p></td>
                    </tr>
                    <tr class="even">
                        <td class="cellDel tCenter"><a href="#"><img src="{$template_root}images/del.gif" alt=""/></a></td>
                        <td class="imageCell"><p><a href="#"><img src="{$template_root}images/image1.jpg" alt=""/></a></p></td>
                        <td><a href="#">Открытка к торжеству ручной работы</a> 
                            <p class="scuCart">Артикул - <span>170122</span></p>
                        </td>
                        <td class="priceCell tRight">19 <p>грн/шт.</p></td>
                        <td class="amountCell tRight">
                            <input type="text" name="" value="222"/>
                            <p>штук</p>
                        </td>
                        <td class="sumCell tRight">3931 <p>грн.</p></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="total tRight">Итого:</td>
                        <td class="totalSumCell tRight">3944<p>грн.</p></td>
                    </tr>
                </table>-->
                <br />
                <br />
                <h2><span>Форма заявки</span></h2>
                <form action="/shop/order/submit" method="post" id="orderForm" onsubmit="if(!checkLogin($('#orderForm'))) return false;">
                    {if $rows}
                    {foreach from=$rows item=item key=key name="bla"}
                    <input type="hidden" name="addParam[{$item.id}]" value="" id="id_{$item.id}"/>
                    {/}
                    {/}
                <table width="100%" class="leaveMess">
                    <tr>
                        <td class="leftTabCol">
                            <p>Представьтесь, пожалуйста <span class="red">*</span></p>
                            <input type="text" name="name" class="required" value="{$authorized.name|default:$quicky.post.name}" rel="Введите имя"/>
                            <br />
                            <br />
                            <p>Связь с вами <span class="grayText">(Телефон или электронная почта)</span> <span class="red">*</span></p>
                            <input type="text" name="mobphone" class="required" value="{$user['phone']|default:$quicky.post.mobphone}" rel="Введите телефон или email"/>
                        </td>
                        <td>
                            <p>Cообщение <span class="grayText">(Предложение или пожелание)</span></p>
                            <textarea name="comment">{$quicky.post.comment}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td> <span class="red">*</span> - <span class="grayText">Обязательные поля</span></td>
                        <td>
                            <img src="{$template_root}images/search_ico.png" alt="" class="vMiddle"/> <a href="/shop/{$first['seo_url']}_{$first['id']}" class="simpleLink">Продолжить покупки</a>
                            <a href="#" class="sendBtn flRight" onclick="if(checkLogin($('#orderForm'))) $('#orderForm').submit();return false;"><img src="{$template_root}images/orderIco.png" alt=""/> Оформить заявку</a>
                            <input type="submit" value='d' style="position: absolute;left:-10000px;"/>
                            <div class="clear"></div>
                        </td>
                    </tr>
                </table>
                </form>
				<br />
				
{include file="menu.tpl"}
				
                <!-- footer -->
<script type="text/javascript" src="{$template_root}js/quickorder.js"></script>
<script type="text/javascript" src="{$template_root}js/cart.js"></script>				
{include file="footer.tpl"}