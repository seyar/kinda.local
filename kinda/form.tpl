{include file="header.tpl"}
<!-- header -->
<form action="/feedback/send" method="post" id="contForm" onsubmit="if( !checkLogin($('#contForm')) ) return false;">
<input type="hidden" name="mail_template" value="form"/>
<input type="hidden" name="referer" value="form"/>
<div class="cols flLeft">
    <h1 class="colTitle tLeft">Заказ имиджевого пакета</h1>
    <br />
    <p class="colTitle2 blueText">Размер</p>
    <div class="formRow">
        <div class="titleCell flLeft">Ширина</div>
        <input type="text" name="width" value="{$quicky.post.width}"/>
    </div>
    <div class="formRow">
        <div class="titleCell flLeft">Высота</div>
        <input type="text" name="height" value="{$quicky.post.height}"/>
    </div>
    <div class="formRow">
        <div class="titleCell flLeft">Глубина</div>
        <input type="text" name="depth" value="{$quicky.post.depth}"/>
    </div>
    <div class="clear"></div>

    <p class="checkboxRow"><label><input type="radio" name="exact" value="точно по размеру" {if $quicky.post.exact == 'точно по размеру' || !$quicky.post}checked="true"{/}/> &mdash;Изготовить по размеру</label></p>
    <p class="checkboxRow"><label><input type="radio" name="exact" value="готовый штамп близкий по размеру" {if $quicky.post.exact == 'готовый штамп близкий по размеру'}checked="true"{/}/> &mdash;Изготовить готовый штамп близкий по размеру</label></p>
    <br />
    <br />
    <p class="colTitle2 blueText">Материал</p>

    <div class="formRow">
        <div class="titleCell flLeft">Вид бумаги</div>
        <div class="borderedBorder flLeft">
            <select id="vid_bumagi"  name="vid_bumagi">
            <option value="Мелованая бумага" {if $quicky.post.vid_bumagi == 'Мелованая бумага'}selected="selected"{/}>Мелованая бумага</option>
            <option value="Ламинированый картон" {if $quicky.post.vid_bumagi == 'Ламинированый картон'}selected="selected"{/}>Ламинированый картон</option>
            <option value="Дизайнерская бумага" {if $quicky.post.vid_bumagi == 'Дизайнерская бумага'}selected="selected"{/}>Дизайнерская бумага</option>
            </select>
        </div>
        <div class="clear"></div>
    </div>
    <div class="formRow">
        <div class="titleCell flLeft">Плотность бумаги</div>
        <div class="borderedBorder flLeft">
            <select name="plot">
            <option value="б - 170 гр/м" {if $quicky.post.plot == 'б - 170 гр/м'}selected="selected"{/}>б - 170 гр/м</option>
            <option value="б,к - 200 гр/м" {if $quicky.post.plot == 'б,к - 200 гр/м'}selected="selected"{/}>б,к - 200 гр/м</option>
            <option value="к - 230 гр/м" {if $quicky.post.plot == 'к - 230 гр/м'}selected="selected"{/}>к - 230 гр/м</option>
            <option value="к - 270 гр/м" {if $quicky.post.plot == 'к - 270 гр/м'}selected="selected"{/}>к - 270 гр/м</option>
            <option value="к - 290 гр/м" {if $quicky.post.plot == 'к - 290 гр/м'}selected="selected"{/}>к - 290 гр/м</option>    
            </select>
        </div>
        <div class="clear"></div>
    </div>
    <div class="formRow">
        <div class="titleCell flLeft">Ламинация</div>
        <div class="borderedBorder flLeft">
            <select name="laminat">
            <option value="Глянцевая"  {if $quicky.post.laminat == 'Глянцевая'}selected="selected"{/}>Глянцевая</option>
            <option value="Матовая" {if $quicky.post.laminat == 'Матовая'}selected="selected"{/}>Матовая</option>
            <option value="Без ламинации" {if $quicky.post.laminat == 'Без ламинации'}selected="selected"{/}>Без ламинации</option>    
            </select>
        </div>
        <div class="clear"></div>
    </div>
    <div class="formRow">
        <div class="titleCell flLeft">Цвет шнура</div>
<!--        <div class="borderedBorder flLeft">-->
            <input type="text" name="shnur_color" value="{$quicky.post.shnur_color}" style="width:153px;"/>
<!--        </div>-->
        <div class="clear"></div>
    </div>
    <p class="checkboxRow"><label><input type="checkbox" name="monoversi" value="Да" checked="true"/> &mdash;Наличие моноверсов</label></p>
    <br />
    <br />
    <p class="colTitle2 blueText">Печать</p>
    <div class="formRow">
        <div class="titleCell flLeft">Количество цветов</div>
        <div class="borderedBorder flLeft">
            <select name="count_colors">
            <option value="1 + 0"  {if $quicky.post.count_colors == '1 + 0'}selected="selected"{/}>1 + 0</option>
            <option value="2 + 0" {if $quicky.post.count_colors == '2 + 0'}selected="selected"{/}>2 + 0</option>
            <option value="3 + 0" {if $quicky.post.count_colors == '3 + 0'}selected="selected"{/}>3 + 0</option>
            <option value="4 + 0" {if $quicky.post.count_colors == '4 + 0'}selected="selected"{/}>4 + 0</option>
            <option value="5 + 0" {if $quicky.post.count_colors == '5 + 0'}selected="selected"{/}>5 + 0</option>
            <option value="6 + 0" {if $quicky.post.count_colors == '6 + 0'}selected="selected"{/}>6 + 0</option>    

            </select>
        </div>
        <div class="clear"></div>
    </div>
    <div class="formRow">
        <div class="titleCell flLeft">Вид печати</div> 
        <div class="borderedBorder flLeft">
            <select name="vid_pechati">
            <option value="Офсетная" {if $quicky.post.vid_pechati == 'Офсетная'}selected="selected"{/}>Офсетная</option>
            <option value="Шелкотрафарет" {if $quicky.post.vid_pechati == 'Шелкотрафарет'}selected="selected"{/}>Шелкотрафарет</option>    
            </select>
        </div>
        <div class="clear"></div>
    </div>
    <div class="formRow">
        <div class="titleCell flLeft">Тираж</div> 
<!--        <div class="borderedBorder flLeft">-->
            <input type="text" name="tirazh" value="{$quicky.post.tirazh}"/>
<!--        </div>-->
        <div class="clear"></div>
    </div>
</div>

<div class="cols flRight">
    <div class="contactData">
        <p class="colTitle2">Cвязь с вами</p>
        <div>
            <p>Электронная почта <span class="red">*</span></p>
            <input type="text" name="email" value="{$quicky.post.email}"  class="required email" rel="Введите email"/>
        </div>
        <div>
            <p>Полное имя <span class="red">*</span></p>
            <input type="text" name="name" value="{$quicky.post.name}"  class="required" rel="Введите полное имя"/>
        </div>
        <div>
            <p>Мобильный телефон <span class="red">*</span></p>
            <input type="text" name="mobphone" value="{$quicky.post.mobphone}" class="required" rel="Введите мобильный телефон"/>
        </div>
        <div>
            <p>Компания <span class="red">*</span></p>
            <input type="text" name="company" value="{$quicky.post.company}" class="required" rel="Введите компанию"/>
        </div>
        <div>
            <p>Cообщение <span class="grayText">(Предложение или пожелание)</span></p>
            <textarea name="comment">{$quicky.post.comment}</textarea>
        </div>
    </div>
</div>
<div class="clear"></div>
<br />
<div class="tCenter taxiRow"><a class="addBtn orderBtn" href="#" onclick="if( checkLogin($('#contForm')) ) $('#contForm').submit();return false;">Оформить заказ</a></div>
</form>
<br />

{include file="menu.tpl"}

<!-- footer -->

{include file="footer.tpl"}