<div id="message">
{php}$this->assign('messages', Messages::get());{/php}
{php}$this->assign('messtype', Messages::getType());{/php}
{if $messages}
<div class="message {$messtype}" >
    {foreach from=$messages value=message}
    {if is_array($message)}
        {$message.0}<br/>
    {else}
    {$message}<br/>
    {/}
    {/foreach}
</div>
{literal}<script type='text/javascript'>setTimeout( function(){$("#message .message").slideUp();$("#message").html(""); } , 10000 );</script>{/literal}
{php}Messages::delete();{/php}
{/}
</div>