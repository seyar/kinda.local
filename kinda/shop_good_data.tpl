<a onclick="$('.jqmWindowGood').jqmHide();" class="closeLoginForm" title="закрыть" href="javascript:void(0);"></a>
<div class="jqmWindowGoodInner">
    <div class="imagePrev">
        <img src="/static/media/{if $obj_images.default_img}shop/{$obj.id}/prev_{$obj_images.default_img}{else}no_photo.jpg{/}" alt=""/>
    </div>
    <Div class="characters">

        {if $authorized }
        <div class="priceMain goodPage flRight oldpricegood"><span>{$obj.prices[1][1]|string_format:"%.2F"}</span> грн</div>{/}

        {if $authorized && $obj.oldprices[1][1] > 0}
        <div class="priceMain goodPage flRight haveoldpricegood"><span>{$obj.oldprices[1][1]|string_format:"%.2F"}</span> грн
        </div>{/}

        <p class="goodTitle">{$obj.name|default:$obj.alter_name} </p>



        <br/>
        <div class="bottomTaxiBorder">
            <div></div>
        </div>
        <br/>

    </Div>
    <div class="clear"></div>
    <div class="borderedBorder flLeft quantityStatus">
        Наличие на складе:
    {if $obj.quantity==0}
    {$obj['store_approximate_date']}
    {else}
        <span>есть в наличии</span>
    {/}
    </div>
    <a href="#" onclick="document.location.href='/shop/cart/{$obj.id}/add?count='+$(this).next().val();return false;" class="addBtn" id="addBtn"><img src="{$template_root}images/plustocart.png" alt=""/> Добавить в
        корзину</a>
    <input type="text" name="count" value="1" class="countGoodInner" id="countGoodInner"
           onkeyup="if(this.value == 0) this.value=1; this.value = this.value.replace(/[\D]*/g, '');"/>
    <div class="clear"></div>
    <br/>
    <br/>
    <div class="goodDescription">
    {$obj.full_description}
    </div>
</div>