<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Abstract controller class for automatic templating with smarty.
 *
 * @package    Controller
 * @author     Kohana Team
 * @copyright  (c) 2008-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */

abstract class Kohana_Controller_Quicky extends Controller {

    /**
     * @var  string  page template
     */
    public $template = null;

    /**
     * @var  object Quicky object
     */
    public $view = null;

    /**
     * @var  boolean  auto render template
     **/
    public $auto_render = TRUE;

    static public $intermediate_vars = array();
    /**
     * Loads the Quicky object.
     *
     * @return  void
     */
    public function before() {
        $this->view = new Quicky();
        $base = Kohana::config('quicky')->get('base');

        //if a default header is specified show it
        if(!empty($base['header']))
            $this->request->response = $this->view->fetch($base['header']);
        else
            $this->request->response = '';
    }

    /**
     * Assigns the template as the request response if auto_render is true.
     *
     * @return  void
     */
    public function after() {        
        if ($this->auto_render === TRUE) {
            // Assign the template as the request response and render it
            if($this->template != null)
                $this->request->response .= $this->view->fetch($this->template);
        }

        //if a default footer is specified show it
        if(!empty($base['footer']))
            $this->request->response .= $this->view->fetch($base['footer']);
    }

} // End Kohana_Controller_Quicky