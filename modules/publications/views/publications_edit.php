<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 22.11.2010 - 16:28:23 Exp $
 *
 * Project:     Chimera2.local
 * File:        publications_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */

include MODPATH.'/admin/views/system/wysiwyg.php';
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/vendor/jquery/instant_translit_min.js"></script>
<script src="/static/admin/js/pubs.js" type="text/javascript"></script>

<!--floating block-->
<form method="post" action="" onsubmit="checkFilterValues();">
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
    <input type="hidden" name="actTab" id="actTab" value="main" />
    <div class="absBlocks floatingInner">
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td><button class="btn blue formSave" type="button" rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                <span><span><?= I18n::get('Save') ?></span></span></button></td>
                        <td style="width:37%">
                            <? if ($obj['id']): ?>
                                <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">
                                    <span><span><?= I18n::get('Apply') ?></span></span></button>
                            <? else: ?>
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/apply">
                                        <span><span><?= I18n::get('Apply') ?></span></span></button>
                            <? endif; ?>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>/';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
                </div>
                <div class="absBlocks floatingPlaCorner L"></div>
                <div class="absBlocks floatingPlaCorner R"></div>
            </div>
            <div class="absBlocks side L"></div>
            <div class="absBlocks side R"></div>
        </div>
        <!--floating block-->

        <div class="rel" >

            <div class="whiteblueBg padding20px">
                <ul class="tabs font14px">
                    <li class="active" id="main"><a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= (isset($id) ? I18n::get('Edit') : I18n::get('New')) ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="options">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Options'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="additional">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Additional'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <?if(isset($obj['id'])):?>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="comments">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Comments'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <?endif;?>
                </ul>
                <div class="clear"></div>
                <br />
                <div class="dottedBottomBorder"></div>                
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>

        <div id="pageEditMain" class="tabContent main openedTab"><? include 'pubs_edit_main.php'; ?></div>
        <div id="pageEditOptions" class="tabContent options "><? include 'pubs_edit_optional.php'; ?></div>
        <div id="pageEditAdditional" class="tabContent additional "><? include 'pubs_edit_additional.php'; ?></div>
        <?if(isset($obj['id'])):?><div id="pageEditcomments" class="tabContent comments "><? include 'comments.php'; ?></div><?endif;?>
    </div>
            
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>
</div>
</form>