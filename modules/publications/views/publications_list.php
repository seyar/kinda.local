<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 22.11.2010 - 10:11:50 Exp $
 *
 * Project:     Chimera2.local
 * File:        publications_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>
<script src="/static/admin/js/list.js" type="text/javascript"></script>
<script src="/static/admin/js/pubs.js" type="text/javascript"></script>
<form method="post" action="<?=$admin_path.$controller?>" onsubmit="checkFilterValues();">
<!--floating block-->
<div class="flLeft outerLeftCol">

        <ul class="leftSubmenu">
            <li><a href="<?=$admin_path.$controller?>types_edit/"><?= I18n::get('Add') ?></a></li>
            <li><a href="<?=$admin_path.$controller?><?=$filter['pub_id']?>/types_delete/" onclick="<? if($filter['pub_id'] != 0):?>if(!confirm('Удалить?')) <?endif;?>return false;"><?= I18n::get('Delete');?></a></li>
            <li style="border:none"><a href="<?=$admin_path.$controller?><?=$filter['pub_id']?>/types_edit/"><?= I18n::get('Correct') ?></a></li>
        </ul>

    <div class="whiteBg pubTypes">
        <?if($publ_types):?>
        <ul class="pubTypesMenu">
            <li <? if($filter['pub_id'] == 0):?>class="opened" <?endif;?>style="padding-left:5px;">
                <a href="<?=$admin_path.$controller?>" onclick="setFilter('0');return false;"><span><?=i18n::get('Root');?></span></a>
            </li>
            <? //die(Kohana::debug($publ_types));?>
            <?// Kohana::debug($publ_types) ?>
            <? foreach($publ_types[0] as $item):?>
            <li>
                <a href="#" class="listCollapse" <?if(!isset($publ_types[$item['id']])):?>style="visibility:hidden;"<?endif;?>></a>
                <a href="#" onclick="setFilter(<?=$item['id']?>);return false;"><span><?=$item['name']?> (<?=$item['count']?>)</span></a>


                <?if(isset($publ_types[$item['id']]) && is_array($publ_types[$item['id']])):?>
                    <? $menu = array($item['id'] => $publ_types[$item['id']]);?>
                    <?  include 'pubtypemenu_recurse.php';?>
                
                <?endif;?>


            </li>
            <?endforeach;?>
        </ul>
        <?endif;?>
    </div>
    <div class="leftColBottom"></div>
</div>

<div class="outerRightCol">
    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner" style="padding-top: 15px;">
            <div class="padding20px">
                <table width="100%">
                    <tr>
                        <td style="width:23%"><button class="btn blue" type="button" onclick="document.location.href='/admin/publications/edit<?=(isset($filter['pub_id'])?'/?pub_type='.$filter['pub_id']:'')?>';">
                                    <span><span><?= I18n::get('Create') ?></span></span></button></td>
                        <td class="vMiddle"><?= I18n::get('With selected') ?> </td>
                        <td style="padding-top:2px"><select id="list_action">
                                <option value="delete"><?echo I18n::get('Delete')?></option>
                                <option value="copy"><?echo I18n::get('Copy')?></option>
                            </select></td>
                        <td><button class="btn formUpdate" type="button">
                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>
                        <td class="vMiddle tRight" style="width:20%"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>
                        <td style="*width:17%">
                            <?=Form::select('rows_per_page', array(
                                '10' => '10 ' . I18n::get('lines'),
                                '20' => '20 ' . I18n::get('lines'),
                                '30' => '30 ' . I18n::get('lines'),
                        ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange'=>"formfilter(jQuery('#rows_per_page'))"))?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>
    <!--floating block-->

    <div class="rel">
        <div class="whiteblueBg padding20px" style="border-top:1px solid #fff;">
            <br />
            <fieldset class="filter filter_input_titles">
                <legend style="font-weight:bold; color: black"><?= i18n::get('Publications filter');?></legend>
                <table width="100%" cellpadding="0" cellspacing="0" class="borderNone">
                    <tr>
                        <td width="100"><input type="text" name="filter[pub_title]" value="<?= (!empty($filter['pub_title']) ? Arr::get($filter,'pub_title') : 'Название');?>" rel="Название" style="width:auto;" onfocus="if(this.value == 'Название') this.value = ''" onblur="if(this.value == '') this.value = 'Название'" /></td>
                        <td width="100"><input type="text" name="filter[pub_date]" value="<?= (!empty($filter['pub_date']) ? Arr::get($filter,'pub_date') : 'Дата добавления');?>" rel="Дата добавления" class="date" style="width:auto;" onfocus="if(this.value == 'Дата добавления') this.value = ''" onblur="if(this.value == '') this.value = 'Дата добавления'" /></td>

                        <? array_unshift($publ_types_unsorted, array('id'=>'', 'name'=>'---','url'=>'---', 'preTitle'=>'') );?>

                        <td><?= Model_Content::customSelect('publ_types_id', $publ_types_unsorted, $filter['pub_id'], NULL, 'id', array('preTitle','name'))?> </td>

                        <td width="150" style="padding-top:3px; *padding-top:0px">
						<div style="width:150px;height:30px; overflow:hidden; " class="padtop3px"><button class="btn formFilter" type="button"><span><span style="padding: 3px 40px;"><?= I18n::get('Apply') ?></span></span></button></div></td>
						<!--<button class="btn formFilter" type="button"><span><span style="padding: 3px 3px;"><?= I18n::get('Apply') ?></span></span></button></td> -->
                    </tr>
                </table>
            </fieldset>
            <br />
        </div>
        <div class="whiteBg">
            <!-- content module-->
            <table width="100%" class="sortableContentTab" cellpadding="0" cellspacing="0">
                <thead>
                          <tr class="first">
                              <th width="2%"><input type="checkbox" name="" value="" style="width:auto;" class="listCheckboxAll"/></th>
                              <th width="17%" class="<?=Admin::table_sort_header('pub_url')?>"><?echo I18n::get('Page')?></th>
                              <th class="<?=Admin::table_sort_header('pub_name')?>"><? echo I18n::get('Name')?></th>
                              <th class="<?=Admin::table_sort_header('pub_date')?>" width="12%"><?echo I18n::get('Date')?></th>
                              <th class="tCenter"><?echo I18n::get('Section')?></th>
                              <th class="last" width="5%">&nbsp;</th>
                          </tr>
                </thead>
                <tbody>
                    <? $i=0; ?>
                        <?foreach( $rows as $key=>$value ):?>
                          <tr class="<?if($i % 2 == 0):?>odd<?else:?>even<?endif;$i++;?>">
                              <td class="tCenter" style="padding-left:8px;"><input type="checkbox" name="chk[<?=$value['id']?>]" value="<?=$value['id']?>" class="listCheckbox"/></td>
                              <td><a href="<?=$admin_path.$controller?><?=$value['id']?>/edit/"><?=  Text::limit_chars($value['pub_url'], 30, '...')?></a></td>
                              <td><?=$value['pub_name']?></td>
                              <td class="nowrap"><?=$value['pub_date']?></td>
                              <td align="center"><?=$value['pub_type_name']?></td>
                              <td width="5%" nowrap>
                                  <a href="<?=$admin_path.$controller?><?=$value['id']?>/active/" ><img src="/static/admin/images/act_<?if($value['status'] == 1):?>green<?else:?>red<?endif;?>.png" alt="copy" title="<?echo I18n::get('Active');?>"/></a>&nbsp;&nbsp;&nbsp;
                                  <a target="_blank" href="<?=$publ_types_forlist[$value['publicationstypes_id']]['parent_url']?><?=$publ_types_forlist[$value['publicationstypes_id']]['url']?>/<?=$value['pub_url']?>" ><img src="/static/admin/images/ico_preview.gif" alt="to the site" title="<? echo I18n::get('View on site')?>"/></a>&nbsp;
<!--                                  <a href="<?=$admin_path.$controller?>/<?=$module_name?>/<?=$value['id']?>/edit/" ><img src="/static/admin/images/edi.png" alt="edit" title="{php}echo I18n::get('Edit'){/}"/></a>&nbsp;
                                  <a href="<?=$admin_path.$controller?>/<?=$module_name?>/<?=$value['id']?>/delete/" ><img src="/static/admin/images/del.png" alt="delete" title="{php}echo I18n::get('Delete'){/}"/></a>&nbsp;-->
                              </td>
                        </tr>
                        <?endforeach;?>
                        </tbody>
              </table>
            <? if ($pagination): ?>
            <?=$pagination?>
            <?endif;?>
            <!--                        content module-->
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
    <div class="bottomshadow_narrow"></div>
</div>
<div class="clear"></div>
<style>.bottomshadow{ display: none; }</style>
<input type="submit" name="ss" value="ss" class="hidden"/>
</form>