<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 23.11.2010 - 10:57:01 Exp $
 *
 * Project:     Chimera2.local
 * File:        pubs_edit_optional.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<br />
<div class="flLeft dottedRightBorder" style="width: 45%;">
    <table width="100%" class="tableTop italic borderNone grayText2">
        <tr>
            <td>
                <? echo I18n::get('Keywords, separated by commas')?><br/>
                <textarea rows="5" cols="44" name="pub_keywords"><?=(isset($obj['pub_keywords'])?$obj['pub_keywords']:$_POST['pub_keywords'])?></textarea>
            </td>
        </tr>
    </table>
</div>
<div class="flLeft italic grayText2" style="width: 45%; padding: 5px 0pt 0pt 40px;">
    <br />
    <label style="padding-right: 2em;" for="status">
    <input type="checkbox" name="status" value="1" style="width:auto;" class="vMiddle"
	       <?if($obj['status'] || $_POST['status'] || !$obj['id']):?> checked="checked"<?endif;?>/>&nbsp;<? echo I18n::get('Publication active');?></label>
    <br />
    <br />
    <label style="padding-right: 2em;">
        <input type="checkbox" name="cache_page" value="1" style="width:auto;" class="vMiddle" <?if($obj['cache_page'] || $_POST['cache_page'] || !$obj['id']):?> checked="checked"<?  endif;?>/>&nbsp;<? echo I18n::get('To cache')?></label>
        
    <?if($stdParam['pub_expiredate']):?>
        <label style="padding-right: 2em;" for="pub_expiredate"><? echo I18n::get('Publication Expires');?>
        <input type="text" name="pub_expiredate" value="<?=(isset($obj['pub_expiredate'])?$obj['pub_expiredate']:$_POST['pub_expiredate']);?>"  style="width:auto;" /></label>
    <?endif;?>
    <?if($stdParam['pub_expiredate']):?>
        <label style="padding-right: 2em;" for="pub_source"><? echo I18n::get('Source')?>
        <input type="text" name="pub_source" value="<?=(isset($obj['pub_source'])?$obj['pub_source']:$_POST['pub_source'])?>"  style="width:auto;" /></label>
    <?endif;?>
</div>
<div class="clear"></div>
<br />