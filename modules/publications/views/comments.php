<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 29.12.2010 - 15:08:35 Exp $
 *
 * Project:     kontext
 * File:        comments.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
$type = 'comments/';

if( in_array('comments',  Kohana::modules() ))
{

    $comm = Request::factory('admin/' . $type . $obj['id'] . '/index')->execute();

    echo str_replace($controller, $type , $comm->response);
    
}

?>