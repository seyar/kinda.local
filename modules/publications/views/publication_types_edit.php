<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 23.11.2010 - 17:11:37 Exp $
 *
 * Project:     Chimera2.local
 * File:        publication_types_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript">
    $(document).ready(function()
    {
	$('a.delete').click(function()
	{
	    $(this).parents('tr').remove();
	    return false;
	});

	changeParent();
    });

    function troggleAddsFld( status )
    {
	if (status)
	{
	    $('#optionalInfo').show();
	    $('#optionalInfo input, #optionalInfo textarea, #optionalInfo select').attr('disabled', '');
	}
	else
	{
	    $('#optionalInfo').hide();
	    $('#optionalInfo input, #optionalInfo textarea, #optionalInfo select').attr('disabled', 'disabled');
	}
    }

    function addNewField()
    {
	//newHtml = $("#proto").html();

	newLine = $('#addFld_table tr:last').clone(true).insertAfter('#addFld_table tr:last');

	$('#addFld_table tr:last')
	    .find('input[type=text],select')
	    .val('')
	;

	$('#addFld_table tr:last').find('input[alt=name]').attr('name','newAddFldName[]').attr('disabled','');
	$('#addFld_table tr:last').find('select[alt=type]').attr('name','newAddFldType[]').attr('disabled','');
	$('#addFld_table tr:last').find('input[alt=desc]').attr('name','newAddFldDesc[]').attr('disabled','');

	$('#addFld_table tr:last').attr('id','').removeClass('hidden');

	return false;
    }

    function changeParent()
    {
	parent_url = $('input[name=parent_id]').attr('title');
	$('input[name=parent_url]').val( parent_url );
        var text = $('#upper_level').text();
	$('#parent_path').text( text+parent_url+'/' );

	return false;
    }

    $(document).ready(function(){changeParent();});

    </script>
<!--floating block-->
<form method="post" action="">
    <div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
        <input type="hidden" name="actTab" id="actTab" value="main" />
        <div class="absBlocks floatingInner">
            <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        
<? if ($obj['id']): ?>
                        <td><button class="btn blue formSave" type="button" rel="<?= $admin_path . $controller ?>/<?= (isset($id) ? $id : 0) ?>/types_save">
                            <span><span><?= I18n::get('Save') ?></span></span></button></td>
                        <td>
                            <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?>/<?= (isset($id) ? $id : 0) ?>/types_update">
                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

<? else: ?>
                        <td>
                            <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?>/<?= (isset($id) ? $id : 0) ?>/types_save">
                                <span><span><?= I18n::get('Save and exit') ?></span></span></button>
                        </td>
<? endif; ?>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?= $admin_path . $controller ?>/';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel" >

        <div class="whiteblueBg padding20px">
            <? if ($errors): ?><div class="errors"><? foreach ($errors as $message) echo $message; ?></div><? endif; ?>
            <br />
            <div class="flLeft dottedRightBorder" style="width:48%;">
                <b><?php echo I18n::get('Properties Categories')?> <?php //echo I18n::get('Name')?></b>	<br /><br />
				<span class="italic grayText2"><?php echo I18n::get('Category name')?></span>
                <div class="winput" style=" margin-bottom: 8px;"><input style="width: 93%;" type="text" name="name" value="<?=(isset($obj['name'])?$obj['name']:(isset($_POST['name'])?$_POST['name']:'Имя категории'))?>" title="<?php echo I18n::get('Name')?>" onfocus="if(this.value=='Имя категории') this.value='';" onblur="if(this.value=='') this.value='Имя категории';"></div>
				<span class="italic grayText2"><?php echo I18n::get('Category title')?></span>
                <div class="winput" style=" margin-bottom: 8px;"><input style="width: 93%;" type="text" name="pubtype_title" value="<?=(isset($obj['pubtype_title'])?$obj['pubtype_title']:(isset($_POST['pubtype_title'])?$_POST['pubtype_title']:'Title категории'))?>" title="<?php echo I18n::get('Name')?>" onfocus="if(this.value=='Title категории') this.value='';" onblur="if(this.value=='') this.value='Title категории';"></div>
                <?php $upper_level = I18n::get('Parent category');?>
                <table class="borderNone italic grayText2" style=" margin-bottom: 8px;">
                    <tr style="position: relative;z-index: 8;">
                        <td style="width: 185px;"><?php //echo I18n::get('Parent category')?>
							<span class="italic grayText2"><?php echo I18n::get('Section')?></span>
                            <input type="hidden" name="old_parent" value="<?=$obj['parent_id']?>"/>
                            <select name="parent_id"  onchange="changeParent();">
                                <option value="0" title="/"><?=$upper_level;?></option>
                                
                                <?foreach($publications_list as $key => $value):?>
                                    <?if($value['id'] != $obj['id']):?>
                                            <? $level=str_repeat('-', $value['depth']);?>
                                    
                                        <option
                                            <?if($value['id'] == $obj['parent_id'] || $value['id'] == $_POST['parent_id'] ):?> selected="selected" <?endif;?>
                                            <?if(!isset($obj['parent_id']) && !isset($_POST['parent_id']) && $value['id'] == $filter_pub_id ):?> selected="selected" <?endif;?>
                                            value="<?=$value['id']?>"
                                            title="<?=  htmlentities( $value['parent_url'].$value['url'], ENT_COMPAT,'UTF-8');?>"><?=$level.$value['name']?></option>
                                    <?endif;?>
                                <?endforeach;?>
                            </select>
                        </td>
                        <td><? $url = Arr::get($obj, 'url', $_POST['url'])?>
							<span class="italic grayText2">URL</span>
                            <input type="text" name="url" value="<?=$url?>" onfocus="if(this.value == '<?=$url?>') this.value=''" onblur="if(this.value == '') this.value='<?=$url?>'" style="width: 221px;"/>
                        </td>
                    </tr>
                    <tr style="position: relative;z-index: 7;">
                        <td style="width: 185px;">
                            <?= i18n::get('Template');?>
                            <?=Form::select('template', $pubtemplates, Arr::get($obj, 'template', $_POST['template']), array('name'=>'template') );?>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <div class="italic grayText2">
                    <label><input type="checkbox" name="have_addFields" value="1" onclick="troggleAddsFld(this.checked);"
                              <?if($obj['have_addFields'] || $_POST['have_addFields']):?> checked="checked"<?endif;?> style="width: auto;"/>
                        <?php echo I18n::get('Use the additional fields')?></label>
                    <br/>
                    <br/>
                    <label><input type="checkbox" name="status" value="1" <?if($obj['status'] || $_POST['status'] || !$obj['id']):?> checked="checked"<?endif;?> style="width: auto;"/>
                        <?php echo I18n::get('Category active')?></label>
                </div>
            </div>
            <div class="flLeft" style="width:40%; padding: 0 0 0 5%;">
                <p class="tBold"><?php echo I18n::get('Options Uncategorized')?></p>
                <br />
                <div class="italic grayText2">
                    <?foreach($priFieldsName as $key => $value):?>
                        <div style="margin-bottom:7px"><label><input type="checkbox" name="priFields[<?=$key?>]" value="<?=$key?>" <?if($obj['ser_priFields'][$key]):?>checked="checked"<?endif;?> /> <?=$value?></label></div>
                    <?endforeach;?>

                    <div style="border-top:1px dotted #878787" id="optionalInfo" class="<?if($obj['have_addFields'] || $_POST['have_addFields']):?><?else:?>hidden<?endif;?>">
                        <?include "publication_types_edit_optional.php";?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <br />

            <p class="hidden"><img src="/static/admin/images/arrowRight.gif" alt="" class="vMiddle"/> <a href="#"><?php echo I18n::get('Copy the content in other languages')?></a></p>
			<div class="grayBlock hidden">
			    <table width="100%">
				<tr>
                    <?foreach($langs as $item):?>
				    <td >
					<label><input type="checkbox" name="copy_content_for_lang[<?=$item['id']?>]" value="1" style="width:auto;" class="vMiddle"/> <?=$item['name']?></label>
				    </td>
                    <?endforeach;?>
				    <td width="55%">
					<button class="btn" type="button" onclick="return false;">
					    <span><span><?php echo I18n::get('Copy')?></span></span></button>
				    </td>
				</tr>
			    </table>
			</div>
            <br />
            <p class="hidden"><img src="/static/admin/images/arrowRight.gif" alt="" class="vMiddle"/> <a href="#"><?php echo I18n::get('Create a copy in other languages')?></a></p>
			<div class="grayBlock hidden">
			    <table width="100%">
				<tr>
                    <?foreach($langs as $item):?>
				    <td >
					<label><input type="checkbox" name="copy_content_for_lang[<?=$item['id']?>]" value="1" style="width:auto;" class="vMiddle"/> <?=$item['name']?></label>
				    </td>
                    <?endforeach;?>
				    <td width="55%">
					<button class="btn" type="button" onclick="return false;">
					    <span><span><?php echo I18n::get('Create')?></span></span></button>
				    </td>
				</tr>
			    </table>
			</div>

            <input type="hidden" name="parent_url" value="<?=(isset($obj['parent_url'])?$obj['parent_url']:(isset($_POST['parent_url'])?$_POST['parent_url']:'/'))?>" />
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>