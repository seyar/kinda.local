<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 22.11.2010 - 17:07:21 Exp $
 *
 * Project:     Chimera2.local
 * File:        pubs_edit_main.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="flLeft dottedRightBorder" style="width: 45%;">
	<div style="font-weight: bold; margin: 15px 0pt 10px 3px;"><?php echo I18n::get('Placement options')?></div>
    <table width="100%" class="tableTop italic borderNone grayText2">
        <tr>
            <td>
                <?php echo I18n::get('Headline news')?>:<br />
                <div class="winput" style="margin-top: 8px;"><input type="text" name="pub_name" id="input_pub_name" value="<?=Arr::get($obj, 'pub_name',$_POST['pub_name'])?>" /></div>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo I18n::get('Publication Start Date')?>:<br />
                <div class="winput" style="margin-top: 8px;"><input type="text" class="date" name="pub_date2" value="<?=(isset($obj['pub_date2'])?$obj['pub_date2']:(isset($_POST['pub_date2'])?$_POST['pub_date2']:date('Y-m-d H:i:s')));?>"/></div>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo I18n::get('Publication Date')?>:<br />
                <div class="winput" style="margin-top: 8px;"><input type="text" class="date" name="pub_date" value="<?=(isset($obj['pub_date'])?$obj['pub_date']:(isset($_POST['pub_date'])?$_POST['pub_date']:date('Y-m-d H:i:s')));?>"/></div>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo I18n::get('Short description of publication')?>:<br />
                <textarea rows="5" cols="44" style="width:98%; margin-top: 8px;" name="pub_shorttext"
                  ><?=(isset($obj['pub_shorttext'])?$obj['pub_shorttext']:$_POST['pub_shorttext'])?></textarea>
            </td>
        </tr>
    </table>
</div>
<div class="flLeft" style="width: 45%; padding: 0pt 0pt 0pt 40px;">
	<div style="font-weight: bold; margin: 15px 0pt 10px 3px;"><?php echo I18n::get('Publishing options')?></div>
    <table width="100%" class="tableTop italic borderNone grayText2">
        <tr>
            <td>
                <?php echo I18n::get('Page title')?>:<br />
                <div class="winput" style="margin-top: 8px;"><input type="text" name="pub_title" value="<?=(isset($obj['pub_title'])?$obj['pub_title']:$_POST['pub_title'])?>" style="width:98%;"/></div>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <input type="checkbox" name="main_new" value="1" <?=($obj['main_new'] == 1 ? 'checked="true"' :'')?> />
                &nbsp;<?php echo I18n::get('Main new')?><br />
                <br />
            </td>
        </tr>
        <tr>
            <td style="padding: 4px 8px 0 0;"><?php echo I18n::get('Path to the page')?> (*.<span style="color: #C0C0C0">html</span>):<br />
				<table class="borderNone">
					<tbody>
						<tr>
							<td style="width:1%; white-space:nowrap; vertical-align:middle">

							<?if($obj['publicationstypes_id']):?>
							<input type="hidden" name="pub_types_id" value="<?=$obj['publicationstypes_id']?>" />
							<?=$obj['publ_type_url']?>
							/&nbsp;

							</td>
							<td><input style="width:100%" type="text" name="pub_url" id="input_pub_url" value="<?=(isset($obj['pub_url'])?$obj['pub_url']:$_POST['pub_url'])?>" style="margin-right: 0; padding-right: 0; width: 120px;"/>
							<?else:?>
								<? array_unshift($publ_types, array('id'=>'', 'name'=>'---','url'=>'---', 'preTitle'=>'') );?>

								<div class="flLeft" style="width:200px;"><?= Model_Content::customSelect('pub_types_id', $publ_types, Arr::get($_GET,'pub_type', $_POST['pub_types_id']), NULL, 'id', array('preTitle','name'))?></div>

								<input type="text" name="pub_url" id="input_pub_url" value="<?=(isset($obj['pub_url'])?$obj['pub_url']:$_POST['pub_url'])?>" style="margin:0 0 0 210px; display: block;" />
							<?endif;?>
							<div class="clear"></div>
							</td>
						</tr>
					</tbody>
				</table>
            </td>
        </tr>
        <tr>
            <td>
                <?echo I18n::get('Description')?>:<br />
                <textarea rows="5" cols="44" name="pub_description" style="width:98%; margin-top: 8px;"><?=(isset($obj['pub_description'])?$obj['pub_description']:$_POST['pub_description'])?></textarea>
            </td>
        </tr>
    </table>
</div>
<div class="clear"></div>
<br />
<div class="dottedBottomBorder"></div>

<br/>
<br/>
<span class="grayText2 italic"><?php echo I18n::get('Contents of this publication')?>:</span>
<br/>

<textarea rows="15" cols="44" style="width:98%;" name="pub_fulltext" id="ckeditor"
          class="jquery_ckeditor"><?=(isset($obj['pub_fulltext'])?$obj['pub_fulltext']:$_POST['pub_fulltext'])?></textarea>
<br/>
<p class="grayText2 italic">Добавить изображение в новость:</p>

<?if($obj['id'] && $obj['ser_priFields']['pub_image']):?>
    <? require MODPATH.'/admin/views/system/uploadify.php';?>
<?endif;?>

<br/>
<p class="hidden"><img src="/static/admin/images/arrowRight.gif" alt="" class="vMiddle"/> <a href="#" onclick="$(this).parent().next().slideToggle(); return false;"><?=i18n::get('Создать копию на других языках')?></a></p>
<div class="grayBlock hidden">
    <table width="100%">
        <tr>
            {foreach from=$langs item=item}
            {if $item.id != $current_lang}
            <td >
                <label><input type="checkbox" name="copy_for_lang[{$item.id}]" value="1" style="width:auto;" class="vMiddle"/> {$item.name}</label><br />
                {select class="uncustomized" name="toPubtype" value="" }
                {foreach from=$publ_types item=iitem key=key}
                {if $obj.publicationstypes_id != $iitem.id}
                {option value=$iitem.id text=$iitem.name rel=$key}
                {/}
                {/}
                {/}
            </td>
            {/}
            {/foreach}
            <td width="55%">
                <button class="btn formSave" type="button" onclick="$('#list_action').val('{$admin_path}{$controller}/{$module_name}/{$id}/copy2otherlang');return false;">
                    <span><span>{i18n('Создать')}</span></span></button>
            </td>
        </tr>
    </table>
</div>
<p class="hidden"><img src="/static/admin/images/arrowRight.gif" alt="" class="vMiddle"/> <a href="#"><?=i18n::get('Copy the content in other languages')?></a></p>
<div class="grayBlock hidden">
    <table width="100%">
        <tr>
            {foreach from=$langs item=item}
            <td >
                <label><input type="checkbox" name="copy_content_for_lang[{$item.id}]" value="1" style="width:auto;" class="vMiddle"/> {$item.name}</label>
            </td>
            {/foreach}
            <td width="55%">
                <button class="btn" type="button" onclick="return false;">
                    <span><span><?=i18n::get('Copy')?></span></span></button>
            </td>
        </tr>
    </table>
</div>
<br />
