<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 24.11.2010 - 17:22:50 Exp $
 *
 * Project:     Chimera2.local
 * File:        pubtypemenu_recurse.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
reset($menu);
$menukey = current($menu);
$menukey = $menukey[key($menukey)]['parent_id'];
?>
<ul class="subPubTypesMenu" >
    <?foreach($menu[$menukey] as $key => $item):?>
            <li<?if($item['id'] == $filter['pub_id']):?> class="opened"<?endif;?>>
                <?if(isset($publ_types[$item['id']]) && is_array($publ_types[$item['id']])):?><a href="#" class="listCollapse" style="padding-left: <?echo 3 + ($item['depth']*10)?>px; background-position: <?echo 3+ ($item['depth']*10)?>px 0">&nbsp;</a><?endif;?>
                <a href="#" onclick="setFilter(<?=$item['id']?>);return false;" style="padding-left: <?echo 15+($item['depth']*10)?>px"><span><?=$item['name']?> (<?=$item['count']?>)</span></a>
                <?if(isset($publ_types[$item['id']]) && is_array($publ_types[$item['id']])):?>
                    <? $menu = array($item['id'] => $publ_types[$item['id']]);?>
                    <? include 'pubtypemenu_recurse.php';?>                    
                <?endif;?>
            </li>
    <?endforeach;?>
</ul>
