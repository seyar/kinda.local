<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 23.11.2010 - 18:43:55 Exp $
 *
 * Project:     Chimera2.local
 * File:        publication_types_edit_optional.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div>
    <br />
    <p style="font-style:normal;color: #000; margin-bottom:10px"><b><?php echo I18n::get('Additional fields')?></b></p>
    <table width="77%" id="addFld_table" class="borderNone">
<!--	<tr>
	    <th width="5%"><?php echo I18n::get('Field name')?></th>
	    <th width="10%"><?php echo I18n::get('Type')?></th>
	    <th width="30%"><?php echo I18n::get('Description')?></th>
	    <th width="5%"> </th>
	</tr>-->
	<tr id="proto" class="hidden">
	    <td>            
    		<div style="padding: 0 0 5px 0; width: 350px;">
                <input style="width:225px" type="text" name="newAddFld_name[]" value="" disabled="disabled" alt="name" />
            <?= Form::select('newAddFld_type[]', $add_fields_types, NULL, array('disabled'=>'disabled', 'alt'=>"type", 'class'=>"uncustomized"))?></div>
            <div class="winput" style="border-bottom:1px dotted #878787; padding-bottom: 18px;"><input style=" width: 82%;" type="text" name="newAddFld_desc[]" value="" disabled="disabled" alt="desc" /></div><br />
	    </td>
<!--	    <td>
	    </td>-->
<!--	    <th>
		<a href="#" rel="" class="delete"><img title="Удалить" alt="delete" src="/static/admin/images/del.gif"></a>
	    </th>-->
	</tr>
    
	<? foreach($obj['ser_addFields'] as $a_key => $a_value):?>
	<tr>
	    <td>
		<input style="width:140px" type="text" name="addFld_name[<?=$a_value['id']?>]" value="<?=$a_value['f_name']?>" alt="name"  />
	    </td>
	    <td>
            <?= Form::Select('addFld_type['.$a_value['id'].']', $add_fields_types, $a_value['f_type'], array('alt'=>"type", 'class'=>"uncustomized"));?>

	    </td>
	    <td>
		<input style="width:140px" type="text" name="addFld_desc[<?=$a_value['id']?>]" value="<?=$a_value['f_desc']?>" alt="desc"  />
	    </td>
	    <td>
		<a href="#" rel="<?=$key?>"
		   class="delete"><img title="Удалить" alt="delete" src="/static/admin/images/del.gif"></a>
	    </td>
	</tr>
	<?endforeach;?>
    </table>
    <a class="plushref" href="#" onclick="addNewField(); return false;"><?php echo I18n::get('Add new field')?></a>
    <br>
</div>