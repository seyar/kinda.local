<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 23.11.2010 - 11:57:16 Exp $
 *
 * Project:     Chimera2.local
 * File:        pubs_edit_additional.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<? $current_date = date('Y-m-d');?>
<div style="padding:10px 0; width: 99%; color:#636466">
<? $count = ceil(count($obj['add_fields']) / 2); ?>
    <table width="100%">
        <tr><td style="width:48%;">
    <table  class="borderNone">
<?foreach($obj['add_fields'] as $key => $value):?>
        <tr>
            <td valign="top">
                <input name="add_fields_id[<?=$value['f_id']?>]" value="<?=$value['id']?>" type="hidden" />
                <? $add_fields_id = "add_fields[{$value['f_id']}]";?>
                <?=$value['f_desc']?>                
            </td>
            <td valign="top">
                <?if($value['f_type'] == 'int'):?>
                    <input name="add_fields[<?=$value['f_id']?>]" value="<?=htmlentities((isset($value['value_int'])?$value['value_int']:(isset($_POST[$add_fields_id])?$_POST[$add_fields_id]:0)), ENT_COMPAT, 'UTF-8')?>" type="text" class="" />
                <?elseif($value['f_type'] == 'float'):?>
                    <input name="add_fields[<?=$value['f_id']?>]" value="<?=htmlentities((isset($value['value_float'])?$value['value_float']:(isset($_POST[$add_fields_id])?$_POST[$add_fields_id]:0)), ENT_COMPAT, 'UTF-8')?>" type="text" class="" />
                <?elseif($value['f_type'] == 'date'):?>
                    <input name="add_fields[<?=$value['f_id']?>]" value="<?=htmlentities((isset($value['value_date'])?$value['value_date']:(isset($_POST[$add_fields_id])?$_POST[$add_fields_id]:$current_date)), ENT_COMPAT, 'UTF-8')?>" type="text" class="date" />
                <?elseif($value['f_type'] == 'string'):?>
                    <input name="add_fields[<?=$value['f_id']?>]" value="<?=htmlentities((isset($value['value_string'])?$value['value_string']:(isset($_POST[$add_fields_id])?$_POST[$add_fields_id]:'')), ENT_COMPAT, 'UTF-8')?>" type="text" class="" />
                <?elseif($value['f_type'] == 'text'):?>
                    <textarea name="add_fields[<?=$value['f_id']?>]" rows="5" cols="44" style="width: 90%;">{$value.value_text|default:$quicky.post['$add_fields_id']}</textarea>
                <?elseif($value['f_type'] == 'logic'):?>
                    <input name="add_fields[<?=$value['f_id']?>]" value="1" type="checkbox" <?=(isset($value['value_int']) || $_POST[$add_fields_id]) ? 'checked' : ''?>/>
                <?endif;?>
                <br/>
            </td>
        </tr>
        
        <? if($key == $count-1 ):?> 
            </table>    </td><td style="width:48%;"><table class="borderNone">
    <? endif;?>
<? endforeach;?>
    </table>
            </td></tr>
    </table>
    
</div>
