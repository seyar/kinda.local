<?php defined('SYSPATH') or die('No direct script access.');

Route::set
        (
	    'publications',
	    'admin/publications((/<id>)/<action>)(/<path>)',
	    array( 'id' => '\d{1,10}', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'publications',
		    'id'	    => NULL,
		    'action'        => 'index',
                )
	)
;

// Frontned Routes
$root = Kohana::config('publications')->pubRootUrl;
$types = '';
$pub_types = Model_Publications::publ_types();
foreach($pub_types as $item) $types .= ltrim($item['parent_url'],'/').$item['url'].'|';
$types = rtrim($types,'|');

/**
 * if we use select from much categories, we may to define tpl file.
 * for select from one category we use publications_front_narrow_widget route
 */

Route::set // widget
        (
	    'publications_front_widget',
	    '(<lang>/)'.$root.'/widget(/<type_uri>(/<amount>(/<type_tpl>)))',
	    array
            (
            'lang' => '(?:'.LANGUAGE_ROUTE.')',
            'type_uri' => '[^/]*',
            'amount' => '[0-9]*',
            'type_tpl' => '(?:main|sec|narrow|mainnarrow)',
            )
        )
        ->defaults
        (
            array(
            'controller'    => 'frontend_publications',
            'action'        => 'widget',
            'type_uri'	    => NULL,
            'lang'          => LANGUAGE_ROUTE_DEFAULT,
            'amount'	    => 2,
            'type_tpl'	    => 'main',
            )
	)
;

Route::set // narrow-widget
        (
	    'publications_front_narrow_widget',
	    '(<lang>/)'.$root.'/narrow-widget(/<type_uri>(/<amount>))',
	    array
            (
            'lang' => '(?:'.LANGUAGE_ROUTE.')',
            'type_uri' => '[^/]*',
            'amount' => '[0-9]*',
            )
        )
        ->defaults
        (
            array(
            'controller'    => 'frontend_publications',
            'action'        => 'narrowwidget',
            'type_uri'	    => NULL,
            'lang'          => LANGUAGE_ROUTE_DEFAULT,
            'amount'	    => 2,
            )
	)
;

Route::set
        (
	    'publications_front_view',
	    '(<lang>/)<type_uri>/<id>', //(:<id>)(/<action>)
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'type_uri' => '(?:'.$types.')'
		    , 'id' => '[^/]+\.html'
            )
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_publications',
		    'action'        => 'view',
		    'type_uri'	    => NULL,
		    'id'	    => NULL,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

Route::set
        (
	    'publications_front_filter_view',
	    '(<lang>/)<type_uri>/f_<filter>/<value>(/<addvalue>)', //(:<id>)(/<action>)
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'type_uri' => '(?:'.$types.')'
		    , 'filter' => '[^/]+'
		    , 'value' => '[^/]*'
		    , 'addvalue' => '.*'
            )
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_publications',
		    'action'        => 'index',
		    'type_uri'	    => NULL,
		    'filter'        => NULL,
		    'lang'          => LANGUAGE_ROUTE_DEFAULT,
            )
	)
;


Route::set
        (
	    'publications_front',
	    '(<lang>/)<type_uri>',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'type_uri' => '(?:'.$types.')'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_publications',
		    'action'        => 'index',
		    'type_uri'	    => NULL,
		    'id'	    => NULL,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

/* ratings */
Route::set
        (
	    'publications_ratings',
	    '(<lang>/)ratings/<action>/<id>',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'action' => '[^/]+'
//		    , 'value' => '[\-0-9]+'
		    , 'id' => '[0-9]+'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_ratings',
		    'action'        => 'rate',
//		    'value'         => '1',
		    'id'         => '1',
		    'lang'          => LANGUAGE_ROUTE_DEFAULT,
            )
	)
;

if ( strpos($_SERVER["PATH_INFO"], ADMIN_PATH) !== 0)
    Event::add('system.execute', array('Model_Frontend_Publications', 'init'));
