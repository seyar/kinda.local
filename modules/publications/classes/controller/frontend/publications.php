<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Frontend_Publications extends Controller_Content
{
    public $template = 'publication_list.tpl';
    static public $newIDs = array();
    const images_folder = 'publications';
    const root_url = 'publications';

    public $top_count = 5;

    public function before()
    {
        parent::before();

        $this->model->images_folder = MEDIAPATH . $this->images_folder . '/';
//        Kohana_I18n::lang(LANGUAGE_ROUTE_DEFAULT);

        self::$newIDs = Kohana::config('publications')->newIDs;
        $this->view->assign('newIDs', self::$newIDs);

        $this->view->assign('root_folder', Controller_Frontend_Publications::root_url);
        $this->view->assign('images_folder', MEDIAWEBPATH . Controller_Frontend_Publications::images_folder . "/");

        $this->view->assign('module_id', Model_AdminModulesControl::find('publications'));
        $this->view->assign('publRootUrl', Kohana::config('publications')->pubRootUrl );
        $this->view->assign('rating_values', Kohana::config('publications')->rating_values );
    }

    public function action_index()
    {
        $publ_types = Model_Frontend_Publications::publication_types(CURRENT_LANG_ID);

        $where = array('publications.status' => array( 'op'=> '=', 'value'=> 1 ) );        
        if (!$this->request->param('type_uri'))
        {
            // show all types top messages
            $list = Model_Frontend_Publications::publication_top(CURRENT_LANG_ID);
        }
        else
        {
            /* filters */
            if( $this->request->param('filter') && $this->request->param('filter') == 'to_date' )       $where['pub_date'] = array( 'op'=> '<', 'value'=> $this->request->param('value') );
            if( $this->request->param('filter') && $this->request->param('filter') == 'from_date' )     $where['pub_date'] = array( 'op'=> '>', 'value'=> $this->request->param('value') );
            if( $this->request->param('filter') && $this->request->param('filter') == 'exact_date' )    $where['pub_date'] = array( 'op'=> '=', 'value'=> DB::expr( 'TO_DAYS(\''. $this->request->param('value') .'\')') );
            if( $this->request->param('filter') && $this->request->param('filter') == 'main_new' )      $where['main_new'] = array( 'op'=> '=', 'value'=> 1 );
            /* end filters */

            
           
                foreach ($publ_types as $value)
                {
                    $url = str_replace('//', '/', '/' . $value['parent_url'] . $value['url']);
                    if ($url == '/' . $this->request->param('type_uri'))
                    {
                        // show current publ_type list
                        $list = Model_Frontend_Publications::publication_index(CURRENT_LANG_ID, $value['id'], NULL, $where);
                        $publ_typeId = $value['id'];
                        $this->view->assign('publ_id', $publ_typeId);
                    }
                }

                $publ_type = Model_Publications::publ_types_load($publ_typeId);
                // TODO need delete
                
                $this->view->assign('publ_type', $publ_type );                                
                $this->template = $publ_type['template'].'_list.tpl';
           

        }
        
        if($this->request->param('value'))
            $this->view->assign('filter', $this->request->param('value'));

		$page_title = isset($publ_type['pubtype_title']) ? $publ_type['pubtype_title'] : Kohana::config('publications.titles.main');
        $this->page_info = array('page_title' => $page_title );

        $this->view->assign('rows', $list[0]);
        $this->view->assign('page_line', $list[1]);
        $this->view->assign('total_pages', $list[2]);
    }

    public function action_comment()
    {
        die();
        $publ_types = Model_Frontend_Publications::publication_types(CURRENT_LANG_ID);

        if (!$this->request->param('type_uri'))
            echo Kohana::debug($publ_types);
        else
        {
            foreach ($publ_types as $value)
            {
                $url = str_replace('//', '/', '/' . $value['parent_url'] . $value['url']);
                echo Kohana::debug("$url=/" . $this->request->param('type_uri'));
                if ($url == '/' . $this->request->param('type_uri'))
                {
                    echo Kohana::debug($value);
                }
            }
        }
        echo Kohana::debug($this->request);
        die();
    }

    public function action_view()
    {
        $url = '/'.$this->request->param('type_uri').'/'. $this->request->param('id');

        $obj = Model_Publications::publ_load_by_url($url, CURRENT_LANG_ID);
        /* ratings */
        $rating = Model_Frontend_Ratings::getItem($obj['id']);
        $obj['rating'] = $rating['rating'];
        /* ratings */

        $publ_type = Model_Publications::publ_types_load($obj['publicationstypes_id']);

        $this->view->assign('publ_type', $publ_type );
        $this->template = $publ_type['template'].'.tpl';

        if ($obj['id'])
            $obj['images'] = Model_Publications::publ_images_show(
                            Controller_Frontend_Publications::images_folder . "/",
                            $path = '',
                            MEDIAWEBPATH . Controller_Frontend_Publications::images_folder . "/{$obj['id']}",
                            $obj['id']
            );

        $this->page_info = array('page_title' => $obj['pub_title'], 'page_keywords' => $obj['pub_keywords'],
            'page_keywords' => $obj['pub_description'],);

        $obj['user'] = Model_Registration::get_items($obj['add_fields']['user_id']['value_int']);
        $this->view->assign('obj', $obj);
        $obj_datetime = strtotime($obj['pub_date']);
        
        $lastNews = Model_Frontend_Publications::publication_top( CURRENT_LANG_ID,1,3 );
        
        $this->view->assign('obj_date', $obj_datetime );
        $this->view->assign('lastNews', $lastNews );

        $this->view->assign('month_names', I18n::get(strtolower(date('M', $obj_datetime))));

        $this->view->assign('publ_id', $this->request->param('id'));
    }

    public function action_widget()
    {
        $pubtypeId = $this->request->param('type_uri');

        if( strstr($pubtypeId, '_') )
        {
            $pubtypeId = explode('_',$this->request->param('type_uri'));
            $this->template = 'publication_'.$this->request->param('type_tpl').'_widget.tpl';
        }
        else
        {
            $publ_type = Model_Publications::publ_types_load($pubtypeId);
            $this->view->assign('publ_type', $publ_type );
            $this->template = $publ_type['template'].'_widget.tpl';
        }
        $where = array();

        if( isset($_GET['filter']) && !empty($_GET['filter']['to_date']) ) $where['pub_date'] = array( 'op'=> '<', 'value'=> $_GET['filter']['to_date'] );
        if( isset($_GET['filter']) && !empty($_GET['filter']['from_date']) ) $where['pub_date'] = array( 'op'=> '>', 'value'=> $_GET['filter']['from_date'] );
        if( isset($_GET['filter']) && !empty($_GET['filter']['main_new']) ) $where['main_new'] = array( 'op'=> '=', 'value'=> $_GET['filter']['main_new'] );

        $list = Model_Frontend_Publications::publication_index(CURRENT_LANG_ID, $pubtypeId, $this->request->param('amount'), $where );

           if( $this->request->param('type_uri') == Kohana::config('publications')->anounceID || $this->request->param('type_uri') == Kohana::config('publications')->opinionsID)
           {
                // for announcements
                $cats = self::action_getCats( $this->request->param('type_uri') );

                $where_cats = array();
                foreach( $cats as $item )
                    $where_cats[] = $item['id'];

                $publ_type = Model_Publications::publ_types_load($this->request->param('type_uri'));
                $this->view->assign('publ_type', $publ_type );
                $this->template = $publ_type['template'].'_widget.tpl';

                $list = Model_Frontend_Publications::publication_index(CURRENT_LANG_ID,$where_cats , $this->request->param('amount'), $where );
           }

        $this->view->assign('rows', $list);

    }


    public function action_narrowwidget()
    {
        $pubtypeId = $this->request->param('type_uri');

        $publ_type = Model_Publications::publ_types_load($pubtypeId);
        $this->view->assign('publ_type', $publ_type );
        
        $this->template = $publ_type['template'].'_narrowwidget.tpl';
        
        $where = array();
        
        if( isset($_GET['notID']) && !empty($_GET['notID']) ) $where['publications.id'] = array('op' => '!=', 'value' => $_GET['notID']);

        if( $pubtypeId == Kohana::config('profile')->blog_publicationstypes_id )
        {
            // for opinioins
            $cats = self::action_getCats( Kohana::config('profile')->blog_publicationstypes_id );
            $where_cats = array();
            foreach( $cats as $item )
                $where_cats[] = $item['id'];

            $list = Model_Frontend_Publications::publication_index(CURRENT_LANG_ID,$where_cats , NULL );
        }
        elseif( $this->request->param('type_uri') == Kohana::config('publications')->anounceID )
        {
                // for announcements
                $cats = self::action_getCats( Kohana::config('publications')->anounceID );

                $where_cats = array();
                foreach( $cats as $item )
                    $where_cats[] = $item['id'];

                $publ_type = Model_Publications::publ_types_load(Kohana::config('publications')->anounceID);
                $this->view->assign('publ_type', $publ_type );
                $this->template = $publ_type['template'].'_narrowwidget.tpl';

                $list = Model_Frontend_Publications::publication_index(CURRENT_LANG_ID,$where_cats , $this->request->param('amount'), $where );
        }else
            $list = Model_Frontend_Publications::publication_index(CURRENT_LANG_ID, $pubtypeId, $this->request->param('amount'), $where );


        $this->view->assign('rows', $list);

    }

    static public function action_getDayNew( $thisObj )
    {
        $pub = Model_Frontend_Publications::getDayNew();
        $type = Model_Publications::publ_types_load($pub['publicationstypes_id']);

        $thisObj->assign('pub_type', $type);
        $thisObj->assign('obj', $pub);

        return $thisObj->fetch('pub_day_new.tpl');
    }

    static public function action_getCats( $pubtype_id = NULL )
    {
        return Model_Publications::getCats( $pubtype_id );
    }

    static public function action_getAutors( $pubtype_id = NULL )
    {
        return Model_Publications::getAutors( $pubtype_id );
    }

}