<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 24.12.2010 - 10:02:27 Exp $
 *
 * Project:     kontext
 * File:        ratings.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */

class Controller_Frontend_Ratings extends Controller_Content
{
    public function before()
    {

    }

    public function action_rate()
    {
        $res = Model_Frontend_Ratings::rate( $this->request->param('id'),$_GET['value'] );
        die(print($res));
    }
}
?>