<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Publications extends Controller_AdminModule
{

    public $template = 'publications_list';
    public $module_name = 'publications';
    public $module_title = 'Publications';
    public $module_desc_short = 'Publications Descr Short';
    public $module_desc_full = 'Publications Descr Full';
    public $images_folder = 'publications';
    public $image_width;
    public $image_height;
    public $image_prev_width;
    public $image_prev_height;
    public $max_images_count = 6;

    static public $pri_fields_names = array(
        'pub_expiredate' => 'Актуальна до',
        'pub_source' => 'Источник информации',
        'pub_image' => 'Фотографии',
    );

    public function before()
    {
        parent::before();
        $this->model = new Model_Publications();
        $this->model->images_folder = MEDIAPATH . $this->images_folder . '/';

        $this->image_width = Kohana::config('publications')->image_width;
        $this->image_height = Kohana::config('publications')->image_height;
        $this->image_prev_width = Kohana::config('publications')->image_prev_width;
        $this->image_prev_height = Kohana::config('publications')->image_prev_width;

        $this->template->current_lang = $this->lang;

    }

    public function action_index()
    {
        $publ_types = $this->model->publ_types(TRUE, $this->lang);


//        echo(Kohana::debug($publ_types));
        $this->template->publ_types_forlist = $publ_types;
//        echo'----------------';
        $publ_types_sorted = Model_Publications::getTypesRecurse($publ_types);
        //echo(Kohana::debug($publ_types_sorted));

        //die(Kohana::debug($publ_types));
        $publ_types = Model_Publications::getTypesRecurse($publ_types);
        //echo(Kohana::debug($publ_types));
        //echo'----------------';
        $publ_types = Model_Publications::getSortedTypesForSelect($publ_types);

        $this->template->publ_types_unsorted = $publ_types;
        $this->template->publ_types = $publ_types_sorted;


        //die(Kohana::debug($this->template->publ_types));

        $rows = array();
        $page_links = '';
        $total_pages = 0;

        /* define filter */
        $where = array();        
        //echo(Kohana::debug($where));
        list($where, $this->template->filter) = Model_Publications::getFilter();

        //die(Kohana::debug($where));

        //die(Kohana::debug($this->template->filter));
        /**/
        
        
        list($this->template->pagination, $this->template->rows) = Admin::model_pagination($this->lang, 'publications_list',
                array('id', 'pub_url', 'pub_name', 'pub_date','status', 'pub_type_name','publicationstypes_id',),
                $where
            );
        
    }

    public function action_active()
    {
        if ($this->request->param('id'))
            $this->model->publ_active($this->request->param('id'));

        $this->redirect_to_controller($this->request->controller);
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            $this->model->publ_delete($this->request->param('id'));

        elseif (count($_POST['chk']))
            $this->model->publ_delete_list($_POST['chk']);

        $this->redirect_to_controller($this->request->controller);
    }

    public function action_edit()
    {
        $this->template = View::factory('publications_edit');
        $publ_types = $this->model->publ_types(FALSE, $this->lang);
        $publ_types = Model_Publications::getTypesRecurse($publ_types);
        $publ_types = Model_Publications::getSortedTypesForSelect($publ_types);
        
        $this->template->publ_types = $publ_types;




        $this->template->langs = $this->get_languages_array();

        $this->template->obj = $this->model->publ_load($this->request->param('id'));

        $img_array = $this->model->publ_images_show(
                        $this->images_folder, '','/',$this->request->param('id')
        );

        $this->template->max_img_count = max(0, $this->max_images_count - count($img_array['files']));
        $this->template->image_prev_height = Kohana::config('publications')->image_prev_height;
        $this->template->image_prev_width = Kohana::config('publications')->image_prev_width;

    }

    public function action_apply()
    {   
        if ( $id = $this->model->publ_save($this->lang) )
        {            
            $this->redirect_to_controller($this->request->controller . '/' . $id . '/edit');
        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_save()
    {

        if ($this->model->publ_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller);

        }
        else
        {            
            $this->action_edit();
        }
    }

    public function action_update()
    {
        if ($this->model->publ_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->action_edit();
        }
    }

    // images view actions

    public function action_image_preview()
    {
        $this->show_image_preview(MEDIAPATH . $this->images_folder . '/' . $this->request->param('id') . '/' . basename($_GET['name']));
    }

    public function action_images_default()
    {
        $this->model->publ_images_default(MEDIAPATH . $this->images_folder . '/' . $this->request->param('id') . '/' . basename($_GET['name']), $this->request->param('id'));
        die();
    }
    
    public function action_cancel_default_image()
    {
        $result = $this->model->publ_cancel_default_image( $this->request->param('id'));
        die(print($result));
    }

    public function action_images_delete()
    {
        if( is_array($_GET['name']) && count($_GET['name']) > 0 )
        {
            $res = 0;
            foreach($_GET['name'] as $item)
                $res |= $this->model->publ_images_delete($this->images_folder, $this->request->param('id'), $item);
        }else
            $res = $this->model->publ_images_delete($this->images_folder, $this->request->param('id'));

        die(print($res));
    }

    public function action_images_show()
    {
        echo json_encode(
                $this->model->publ_images_show(
                        $this->images_folder
                        , $this->admin_path . Request::instance()->controller . "/" . $this->module_name . "/" . $this->request->param('id')
                        , MEDIAWEBPATH . $this->module_name . "/" . $this->request->param('id') .'/'
                        , $this->request->param('id')
                )
        );
        die();
    }

    // images upload actions

    public function action_images_upload()
    {
		if( Kohana::config('publications.watermark') ) $watermark = Kohana::config('publications')->watermark;

        $this->model->publ_images_upload($this->images_folder, $this->image_width, $this->image_height,
                $this->image_prev_width, $this->image_prev_height, $this->request->param('id'), $watermark);
    }

    public function action_images_check()
    {
        $this->model->publ_images_check($this->images_folder, $this->request->param('id'));
    }

    // publication types actions

    public function action_types()
    {
        $this->template = View::factory('publication_types_list');
        $this->template->rows = $this->model->publ_types_show();
    }

    public function action_types_active()
    {
        if ($this->request->param('id'))
            $this->model->publ_types_active($this->request->param('id'));

        $this->redirect_to_controller($this->request->controller . '/types');
    }

    public function action_types_delete()
    {
        if ($this->request->param('id'))
            $this->model->publ_types_delete($this->request->param('id'));

        elseif (count($_POST['chk']))
            $this->model->publ_types_delete_list($_POST['chk']);

        $this->redirect_to_controller($this->request->controller );
    }

    public function action_types_edit()
    {
        $this->template = View::factory('publication_types_edit');
        $this->template->langs = $this->get_languages_array();
        $this->template->priFieldsName = Controller_Publications::$pri_fields_names;
        $this->template->pubtemplates = Model_Publications::get_pub_templates();

        $this->template->obj = $this->model->publ_types_load($this->request->param('id'));
//        $this->template->publications_list = $this->model->publ_types_show();
        $publ_types = $this->model->publ_types(TRUE, $this->lang);        
        
        $publ_types = Model_Publications::getTypesRecurse($publ_types);
        $publ_types = Model_Publications::getSortedTypesForSelect($publ_types);
        $this->template->publications_list = $publ_types;
        $this->template->filter_pub_id = Cookie::get('filter_publ_types_id');
        
        $this->template->add_fields_types = $this->model->publ_add_fields_types();

    }

    public function action_types_save()
    {
        if ($this->model->publ_types_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller );
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_types_edit();
        }
    }

    public function action_types_update()
    {
        if ($this->model->publ_types_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/types_edit');
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_types_edit();
        }
    }

    public function action_panel()
    {
        Db::update('modules')->set(
                        array(
                            'panel_serialized' => serialize(
                                    array(
                                        'info' => array('name' => 'Publications',
                                            'link' => ADMIN_PATH . 'modules/publications',
                                            'image' => ADMIN_PATH . 'modules/publications/logo'
                                        ),
                                        'links' => array(
                                            array('name' => 'Create', 'value' => '/0/edit'),
                                            array('name' => 'List', 'value' => '/'),
                                        ),
                                    )
                            ), // serialize
                        ) // array
                )
                ->where('fs_name', '=', MODULE_NAME)
                ->execute();
        exit();
    }

    public function action_logo()
    {
        // Create the filename
        $file = Kohana::find_file('media', 'publications', 'png');

        if (!is_file($file))
        {
            throw new Kohana_Exception('Image does not exist');
        }

        // Set the mime type
        $this->request->headers['Content-Type'] = File::mime($file);
        $this->request->headers['Content-length'] = filesize($file);

        // Send the set headers to the browser
        $this->request->send_headers();

        // Send the file
        $img = @ fopen($file, 'rb');
        if ($img)
        {
            fpassthru($img);
            exit;
        }
    }

    static public function sitemap()
    {
        return Model_Publications::sitemap();
    }

    static public function sitemapXML()
    {
        return Model_Publications::sitemapXML();
    }

    static public function getUrlById($id)
    {
        $m = new Model_Publications();
        $res = $m->publ_load($id);
        return "/publications/{$res['publ_type_url']}/{$res['pub_url']}";
    }

    public function action_copy2otherlang()
    {
        if ($this->model->copy2otherlang($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

    public function action_types_copy()
    {
        if ($this->model->types_copy($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/types_edit');
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

}