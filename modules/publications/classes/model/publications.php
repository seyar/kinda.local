<?php
defined( 'SYSPATH' ) OR die( 'No direct access allowed.' );

class Model_Publications extends Model
{

    public $images_folder = '';
    static public $allPubTypes = array( );
    static public $sortedPubTypes = array( );
    static public $usedIDs = array( );

    static public function publ_types( $count_pubs = FALSE, $languages_id = NULL )
    {
        $query = DB::select( 'publicationstypes.id', 'publicationstypes.name', 'publicationstypes.parents_path', 'publicationstypes.parent_id', 'publicationstypes.parent_url', 'publicationstypes.url'
                        )
                        ->from( 'publicationstypes' )
                        ->order_by( 'publicationstypes.name' )
                        
        ;

        if( $count_pubs )
        {
            $query = DB::select( 'publicationstypes.id', 'publicationstypes.name', 'publicationstypes.parents_path', 'publicationstypes.parent_id', 'publicationstypes.parent_url', 'publicationstypes.url', Db::expr( 'COUNT(publications.id) AS count' )
                            )
                            ->from( 'publicationstypes' )
                            ->join( 'publications', 'left' )
                            ->on( 'publicationstypes.id', '=', Db::expr( 'publications.publicationstypes_id AND publications.languages_id = ' . $languages_id )
                            )
                            ->order_by( 'publicationstypes.name' )
                            ->group_by( 'publicationstypes.id' )
                            
            ;
        }

        $result = $query->cached(30)->execute();
        
        if( $result->count() == 0 )
            return array( );
        else
            return $result->as_array( 'id' );
    }

    public function publ_show( $languages_id = NULL )
    {
        if( is_numeric( $languages_id ) )
        {
            // Set and save rows_per_page value
            $rows_per_page = (isset( $_POST['rows_num'] ) && (int) $_POST['rows_num']) ? ((int) $_POST['rows_num']) : (int) cookie::get( 'rows_per_page', 10 );

            cookie::set( 'rows_per_page', $rows_per_page );
            $_POST['rows_num'] = $rows_per_page;

            $rows_per_page = 30;
            // Set and save current pulcationTypeId
            $publ_id = (isset( $_POST['publ_types_id'] ) ? self::getTypesFromID( (int) $_POST['publ_types_id'] ) : (int) Session::instance()->get( 'publ_type', 0 ) );

            Session::instance()->set( 'publ_type', $publ_id );
//            $_POST['publ_types_id'] = $publ_id;

            $query_c = DB::select( DB::expr( 'COUNT(*) AS mycount' ) )
                            ->from( 'publications' )
                            ->where( 'publications.languages_id', '=', $languages_id );

            $query = DB::select( 'publications.*', array( 'publicationstypes.name', 'pub_type_name' ) )
                            ->from( 'publications' )
                            ->join( 'publicationstypes' )
                            ->on( 'publicationstypes.id', '=', 'publications.publicationstypes_id' )
                            ->where( 'publications.languages_id', '=', $languages_id );

            if( $publ_id )
            {
                if( is_array( $publ_id ) && count( $publ_id ) > 0 )
                {
                    $query_c->and_where( 'publications.id', 'IN', DB::expr( '(' . implode( ',', $publ_id ) . ')' ) );
                    $query->and_where( 'publications.publicationstypes_id', 'IN', DB::expr( '(' . implode( ',', $publ_id ) . ')' ) );
                }else
                {
                    $query_c->and_where( 'publications.id', '=', $publ_id );
                    $query->and_where( 'publications.publicationstypes_id', '=', $publ_id );
                }
            }

            $count = $query_c
                            ->execute()
                            ->get( 'mycount' );

            if( isset( $_POST['page'] ) )
                $_GET['page'] = $_POST['page'];

            $pagination = Pagination::factory( array(
                        'total_items' => $count,
                        'items_per_page' => $rows_per_page,
                    ) );

            $result = $query
                            ->order_by( 'date_create', 'DESC' )
                            ->limit( $pagination->items_per_page )
                            ->offset( $pagination->offset )
                            ->execute();

            $page_links_str = '';
//	    $page_links_str = $pagination->render();
            if( $result->count() == 0 )
                return array( array( ), '', 1 );
            else
                return array( $result->as_array(), $page_links_str, $pagination->total_pages );
        }

        else
            return array( array( ), '', 1 );
    }

    public function publ_load_by_url( $url = NULL, $lang_id = 1 )
    {
        $query = DB::select( 'publications.*'
                                , array( 'publicationstypes.url', 'publ_type_url' )
                                , array( 'publicationstypes.parent_url', 'publ_type_parent_url' )
                                , array( 'publicationstypes.ser_priFields', 'ser_priFields' )
                                , array( 'publicationstypes.ser_addFields', 'ser_addFields' )
                        )
                        ->from( 'publications' )
                        ->join( 'publicationstypes' )
                        ->on( 'publicationstypes.id', '=', 'publications.publicationstypes_id' )
                        ->limit( 1 );

        if( $url )
        {
            $query->where( DB::expr( 'REPLACE( CONCAT(publicationstypes.parent_url,publicationstypes.url,"/",publications.pub_url),"//","/")' ), '=', $url );
            $query->and_where( 'publications.languages_id', '=', $lang_id );

            $result = $query->execute();

            if( $result->count() == 0 )
                return array( );
            else
            {
                $return = $result->current();

                $return['ser_priFields'] = unserialize( $return['ser_priFields'] );
                $return['ser_addFields'] = unserialize( $return['ser_addFields'] );

                $id = $return['id'];

                $return['add_fields'] = DB::query( Database::SELECT, "SELECT `f`.`id` AS `f_id`, `f`.`f_name`, `f`.`f_type`, `f`.`f_desc`, v.* "
                                        . " FROM `publicationsaddfields` AS `f` "
                                        . " LEFT OUTER JOIN "
                                        . "(SELECT * FROM `publicationsaddvalues` AS v1 WHERE v1.publications_id = {$id} ) "
                                        . " AS `v` ON (`f`.`id`=`v`.`publicationsAddFields_id`) "
                                        . " WHERE f.publicationsTypes_id = {$return['publicationstypes_id']}"
                                )
                                ->execute()
                                ->as_array( 'f_name' )
                ;
                return $return;
            }
        }else
        {
            return array( );
        }
    }

    static public function publ_load( $id = NULL )
    {
        $query = DB::select( 'publications.*'
                                , array( 'publicationstypes.url', 'publ_type_url' )
                                , array( 'publicationstypes.ser_priFields', 'ser_priFields' )
                                , array( 'publicationstypes.ser_addFields', 'ser_addFields' )
                        )
                        ->from( 'publications' )
                        ->join( 'publicationstypes' )
                        ->on( 'publicationstypes.id', '=', 'publications.publicationstypes_id' )
                        ->limit( 1 );

        if( (int) $id )
        {
            $query->where( 'publications.id', '=', $id );

            $result = $query->execute();
            if( $result->count() == 0 )
                return array( );
            else
            {
                $return = $result->current();

                $return['ser_priFields'] = unserialize( $return['ser_priFields'] );
                $return['ser_addFields'] = unserialize( $return['ser_addFields'] );

                $return['add_fields'] = DB::query( Database::SELECT, "SELECT `f`.`id` AS `f_id`, `f`.`f_name`, `f`.`f_type`, `f`.`f_desc`, v.* "
                                        . " FROM `publicationsaddfields` AS `f` "
                                        . " LEFT OUTER JOIN "
                                        . "(SELECT * FROM `publicationsaddvalues` AS v1 WHERE v1.publications_id = {$id} ) "
                                        . " AS `v` ON (`f`.`id`=`v`.`publicationsAddFields_id`) "
                                        . " WHERE f.publicationsTypes_id = {$return['publicationstypes_id']}"
                                )
                                ->execute()
                                ->as_array()
                ;
// TODO: Fix PublictionType `addFields` via current publicationsAddFields_id rows
                return $return;
            }
        }else
        {
            return array( );
        }
    }

    public function publ_save( $lang, $id = NULL )
    {
        if( $post = Model_Publications::publ_validate() )
        {
            if( isset( $id ) && !empty( $id ) && (int) $id != 0 )
            { // update
                $id = (int) $id;

                DB::update( 'publications' )
                        ->set(
                                array(
                                    'publicationstypes_id' => $post['pub_types_id'],
                                    'languages_id' => $lang,
                                    'pub_name' => $post['pub_name'],
                                    'pub_url' => Controller_Admin::to_url( trim( $post['pub_url'] ) ),
                                    'pub_title' => isset( $post['pub_title'] ) && !empty( $post['pub_title'] ) ? $post['pub_title'] : $post['pub_name'],
                                    'pub_date' => $post['pub_date'],
                                    'pub_date2' => $post['pub_date2'],
                                    'pub_expiredate' => $post['pub_expiredate'],
                                    'pub_shorttext' => $post['pub_shorttext'],
                                    'pub_fulltext' => $post['pub_fulltext'],
                                    'pub_keywords' => $post['pub_keywords'],
                                    'pub_description' => $post['pub_description'],
                                    'pub_source' => $post['pub_source'],
                                    'is_cached' => $post['is_cached'],
                                    'status' => $post['status'],
                                    'main_new' => $post['main_new'], // TODO need delete
                                )
                        )
                        ->where( 'id', '=', $id )
                        ->execute()
                ;
            }else
            { // create
                $id = $total_rows = 0;

                if( self::validatePubUrlExists( $post['pub_url'], $post['pub_types_id'] ) )
                {
                    Messages::add( array( i18n::get( 'URL already exists' ) ) );
                    return false;
                }

                list($id, $total_rows) = DB::insert( 'publications', array(
                                    'publicationstypes_id', 'languages_id',
                                    'pub_name', 'pub_url',
                                    'pub_title', 'pub_date', 'pub_date2',
                                    'pub_expiredate', 'pub_shorttext',
                                    'pub_fulltext', 'pub_keywords',
                                    'pub_description', 'pub_source',
                                    'is_cached', 'status',
                                    'date_create', 'main_new',
                                        )
                                )
                                ->values(
                                        array(
                                            $post['pub_types_id'],
                                            $lang,
                                            $post['pub_name'],
                                            Controller_Admin::to_url( trim( $post['pub_url'] ) ),
                                            isset( $post['pub_title'] ) && !empty( $post['pub_title'] ) ? $post['pub_title'] : $post['pub_name'],
                                            $post['pub_date'],
                                            $post['pub_date2'],
                                            $post['pub_expiredate'],
                                            $post['pub_shorttext'],
                                            $post['pub_fulltext'],
                                            $post['pub_keywords'],
                                            $post['pub_description'],
                                            $post['pub_source'],
                                            $post['is_cached'],
                                            $post['status'],
                                            date( 'Y-m-d H:i:s' ),
                                            $post['main_new'],
                                        )
                                )
                                ->execute()
                ;
            }

            Model_Publications::publ_save_add_filelds( $lang, $id, $post );
            Model_Publications::publ_images_default( NULL, $id );

            return $id;
        }else
        {
            return false;
        }
    }

    public function publ_save_add_filelds( $lang = NULL, $id = NULL, $post = array( ) )
    {
        if( !$lang OR !$id )
        {
            Messages::add( array( 'publications' => 'ID not set' ) );
            return false;
        }

        $result = array( );

        $rows = DB::select( 'id', 'f_name', 'f_type', 'f_desc' )
                        ->from( 'publicationsaddfields' )
                        ->where( 'publicationsTypes_id', '=', $post['pub_types_id'] )
                        ->cached(10)
                        ->execute()
                        ->as_array()
        ;
        $qa_array = array( );

        foreach( $rows as $value )
        {
            if( isset( $_POST['add_fields'] ) )
            {
                if( isset( $_POST['add_fields'][$value['id']] ) )
                {
                    $result[$value['id']] = array( 'name' => $value['f_name'], 'type' => $value['f_type'], 'desc' => $value['f_desc'], );
                    $qa_array[$value['f_name']] = array( 'name' => $value['f_name'], 'type' => $value['f_type'], 'desc' => $value['f_desc'], );
                }else
                {
                    $query = Db::delete( 'publicationsaddvalues' )
                                    ->where( 'publications_id', '=', $id )
                                    ->and_where( 'id', '=', (int) $_POST['add_fields_id'][$value['id']] )
                                    ->execute()
                    ;
                }
            }
        }



        foreach( $result as $key => $value )
        {

            if( !$_POST['add_fields_id'][$key] )
                @$query = Db::query( Database::INSERT
                                        , " INSERT INTO publicationsaddvalues "
                                        . " SET "
                                        . "   publications_id = $id "
                                        . " , publicationsAddFields_id = :publ_add_fld_id "
                                        . " , languages_id = $lang "
                                        . " , value_int = :value_int "
                                        . " , value_float = :value_float "
                                        . " , value_string = :value_string "
                                        . " , value_text = :value_text "
                                        . " , value_date = :value_date "
                                )
                                ->bind( ':publ_add_fld_id', $key )
                                ->bind( ':value_int', DB::expr( 'NULL' ) )
                                ->bind( ':value_float', DB::expr( 'NULL' ) )
                                ->bind( ':value_string', DB::expr( 'NULL' ) )
                                ->bind( ':value_text', DB::expr( 'NULL' ) )
                                ->bind( ':value_date', DB::expr( 'NULL' ) )
                ;
            else
                @$query = Db::query( Database::UPDATE
                                        , " UPDATE publicationsaddvalues "
                                        . " SET "
                                        . "   publications_id = $id "
                                        . " , publicationsAddFields_id = :publ_add_fld_id "
                                        . " , languages_id = $lang "
                                        . " , value_int = :value_int "
                                        . " , value_float = :value_float "
                                        . " , value_string = :value_string "
                                        . " , value_text = :value_text "
                                        . " , value_date = :value_date "
                                        . " WHERE id = " . $_POST['add_fields_id'][$key]
                                )
                                ->bind( ':publ_add_fld_id', $key )
                                ->bind( ':value_int', DB::expr( 'NULL' ) )
                                ->bind( ':value_float', DB::expr( 'NULL' ) )
                                ->bind( ':value_string', DB::expr( 'NULL' ) )
                                ->bind( ':value_text', DB::expr( 'NULL' ) )
                                ->bind( ':value_date', DB::expr( 'NULL' ) )
                ;

            $qa_array[$result[$key]['name']]['value'] = $_POST['add_fields'][$key];

            switch( $value['type'] )
            {
                case 'int':
                case 'logic':
                    $query->bind( ':value_int', $_POST['add_fields'][$key] );
                    break;

                case 'float':
                    $query->bind( ':value_float', $_POST['add_fields'][$key] );
                    break;

                case 'string':
                    $query->bind( ':value_string', $_POST['add_fields'][$key] );
                    break;

                case 'text':
                    $query->bind( ':value_text', $_POST['add_fields'][$key] );
                    break;

                case 'date':
                    $query->bind( ':value_date', $_POST['add_fields'][$key] );
                    break;

                default:
                    break;
            }
            $query->execute();
        }

        /* save quick access serialize array  */
        DB::update( 'publications' )
                ->set( array(
                    'qa_ser_fields' => serialize( $qa_array )
                ) )
                ->where( 'id', '=', $id )
                ->execute()
        ;
        /**/

        return true;
    }

    public function publ_delete( $id = NULL )
    {
        $query = DB::delete( 'publications' );

        if( is_numeric( $id ) )
        {
            $query->where( 'id', '=', $id ); // ->limit(1)

            $total_rows = $query->execute();

            Controller_Admin::delete_directory( $this->images_folder . $id );
        }

        return true;
    }

    public function publ_delete_list( $array )
    {
        $query = DB::delete( 'publications' );
        if( count( $array ) )
        {
            $list = array_flip( $array );
            $query->where( 'id', 'in', $list ); // ->limit(1)
            $total_rows = $query->execute();

            foreach( $list as $id )
            {
                Controller_Admin::delete_directory( $this->images_folder . $id );
            }
        }
        return true;
    }

    public function publ_active( $id = NULL )
    {
        if( is_numeric( $id ) )
        {
            DB::query( Database::UPDATE, "UPDATE publications SET status = !status WHERE id =$id LIMIT 1" )
                    ->execute();
        }
        return true;
    }

    public function publ_validate()
    {
        $keys = array(
            'pub_name', 'pub_types_id',
            'pub_url', 'pub_title', 'pub_date', 'pub_date2',
            'pub_shorttext', 'pub_fulltext', 'status',
            'pub_description', 'pub_keywords', 'is_cached',
            'pub_source', 'pub_expiredate', 'pub_image', 'main_new',
            'show_default_image_on_pub', 'not_show_default_image_on_pub'
        );

        $params = Arr::extract( $_POST, $keys, '' );
        $params['pub_url'] = strstr( $params['pub_url'], '.html' ) != FALSE ? $params['pub_url'] : $params['pub_url'] . '.html';
        $params['pub_date'] = empty( $params['pub_date'] ) || !isset( $params['pub_date'] ) ? date( 'Y-m-d H:i:s' ) : $params['pub_date'];

        $post = Validate::factory( $params )
                        ->rule( 'pub_types_id', 'not_empty' )
                        ->rule( 'pub_types_id', 'digit' )
                        ->rule( 'pub_date', 'date' )
                        ->rule( 'pub_name', 'not_empty' )
                        ->rule( 'pub_shorttext', 'not_empty' )
                        ->rule( 'pub_fulltext', 'not_empty' )
                        ->rule( 'pub_url', 'not_empty' )
                        ->rule( 'pub_url', 'max_length', array( 255 ) )
//                        ->callback( $params['pub_types_id'].$params['pub_url'], array('Model_Publications', 'validatePubUrlExists') )
                        ->rule( 'pub_keywords', 'max_length', array( 255 ) )
                        ->rule( 'pub_description', 'max_length', array( 255 ) )
//					->rule('pub_template', 'not_empty')
        ;

        if( $post->check() )
        {
            return $params;
        }else
        {
            Messages::add( $post->errors( 'publications' ) );
        }
    }

    static function validatePubUrlExists( $field, $pubType )
    {

        $exists = (bool) DB::select( 'id' )
                        ->from( 'publications' )
                        ->where( DB::expr( 'CONCAT(publicationstypes_id, pub_url)' ), '=', $pubType . $field )
                        ->execute()
                        ->get( 'id' )
        ;

        if( $exists )
//            $array->error('pub_url', 'url_exist', array($field));
            return true;
        return false;
    }

    // Publiction Images functions

    static public function publ_images_default( $file, $id = NULL )
    {
        if( is_file( $file ) )
        {
            $query = DB::update( 'publications' )
                            ->set(
                                    array(
                                        'pub_image' => basename( $_GET['name'] ),
                                    )
                            )
                            ->where( 'id', '=', $id );
            $query->execute();
            echo '1';
        }else
        { // set FIRST image as default
            Model_Publications::publ_images_show( Kohana::config( 'publications' )->images_folder . '/', '', '/', $id );
        }
    }

    static public function publ_cancel_default_image( $id = NULL )
    {
        $query = DB::update( 'publications' )
                        ->set( array(
                            'pub_image' => '',
                        ) )
                        ->where( 'id', '=', $id )->execute();
        return '1';
    }

    public function publ_images_delete( $folder, $id = NULL, $name = NULL )
    {
        $name = (isset( $name ) && !empty( $name )) ? $name : $_GET['name'];

        $obj_image = MEDIAPATH . "$folder/" . $id . '/' . basename( $name );

        $return = '';

        if( is_file( $obj_image ) )
        {
            if( unlink( $obj_image ) )
            {

                $obj_image = MEDIAPATH . "$folder/" . $id . '/prev_' . basename( $name );

                if( is_file( $obj_image ) )
                {
                    if( unlink( $obj_image ) )
                        $return = '1';
                    else
                        $this->errors = 'Unable to delete preview';
                }
                else
                    $return = '1';
            }
            else
                $this->errors = 'Unable to delete file';
        }
        return $return;
    }

    static public function publ_images_show( $folder, $path='', $webpath='/', $id = NULL )
    {
        $obj = array( 'pub_image' => '' );
        $pub_id = defined( 'MODULE_ID' ) ? MODULE_ID : $id;

        $query = DB::select( 'pub_image' )
                        ->from( 'publications' )
                        ->limit( 1 );

        $obj = $query->where( 'id', '=', $pub_id )->execute()->current();

        $filesArray = array( 'path' => $path
            , 'webpath' => $webpath
            , 'files' => array( )
            , 'default_img' => $obj['pub_image'] );

        $obj_images_dir = @dir( MEDIAPATH . "$folder/" . $pub_id );

        if( $obj_images_dir )
        {
            while( false !== ($entry = $obj_images_dir->read()) )
            {
                if( $entry != '.' and $entry != '..' and !strstr( $entry, 'prev_' ) )
                    $filesArray['files'][] = $entry;
            }
        }

        // set default image if it empty and there is images

        if( (!isset( $obj['pub_image'] ) || empty( $obj['pub_image'] ) ) AND count( $filesArray['files'] ) )
        {
            DB::update( 'publications' )
                    ->set( array( 'pub_image' => $filesArray['files'][0] ) )
                    ->where( 'id', '=', $pub_id )
//                    ->execute()
            ;
//            $filesArray['default_img'] = $filesArray['files'][0];
        }

        return $filesArray;
    }

    public function publ_images_upload( $folder, $width = NULL, $height = NULL, $t_width = NULL, $t_height = NULL, $id = NULL, $watermark = NULL )
    {
        if( !empty( $_FILES ) )
        {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = MEDIAPATH . "$folder/$id/";
            $targetFile = str_replace( '//', '/', $targetPath ) . strtolower( Controller_Admin::to_url( $_FILES['Filedata']['name'] ) );
            if( !file_exists( MEDIAPATH . "$folder/$id" ) )
                @mkdir( str_replace( '//', '/', $targetPath ), 0755, true ); move_uploaded_file( $tempFile, $targetFile );
            $this->image = Image::factory( $targetFile );
            // Resize Image             
            if( $width || $height )
            {
                if( $width > $this->image->width || $height > $this->image->height )
                {
                    $this->image->resize($width, $height, Image::AUTO)
                    //->crop($width, $height)                     
                    ;                 
                } $this->image->save( $targetFile, 90 );
            }
            // Generate Thumbnail             
            if( $width || $height )
            {
                $this->image->resize( $t_width, $t_height, Image::AUTO )->crop( $t_width, $t_height );
                $this->image->save( dirname( $targetFile ) . '/prev_' . basename( $targetFile ), 90 );
            }
 
	    if( $watermark !== null && file_exists( $watermark ) )
            {
                $this->image = Image::factory( $targetFile );
                $this->image->watermark( Image::factory( $watermark ), true, true )->save( $targetFile, 90 );
            } 
		echo "1";
        } die();
    }

    public function publ_images_check( $folder, $id = NULL )
    {
        $fileArray = array( );

        foreach( $_POST as $key => $value )
        {
            if( $key != 'folder' && $key != '_' )
            {
                if( file_exists( MEDIAPATH . "$folder/$id/$value" ) )
                {
                    $fileArray[$key] = $value;
                }
            }
        }

        echo json_encode( $fileArray );
        die();
    }

    // PublictionsTypes functions

    public function publ_types_show()
    {
        $query = DB::select()
                        ->from( 'publicationstypes' )
                        ->order_by( 'name' );

        $result = $query->execute();

        if( $result->count() == 0 )
            return array( );
        else
            return $result->as_array();
    }

    static public function publ_types_load( $id = NULL, $where = array( ) )
    {
        $query = DB::select( 'publicationstypes.*' )
                        ->from( 'publicationstypes' )
                        
                        ->limit( 1 );

        if( (int) $id || (is_array( $where ) && count( $where ) > 0) )
        {
            if( is_array( $where ) && count( $where ) > 0 )
            {
                foreach( $where as $key => $item )
                    $query->where( 'publicationstypes.' . $key, '=', $item );
            }else
                $query->where( 'publicationstypes.id', '=', $id );


            $result = $query->cached(60)->execute();
            if( $result->count() == 0 )
                return array( );
            else
            {
                $return = $result->current();

                $return['ser_priFields'] = unserialize( $return['ser_priFields'] );

                $query = DB::select( 'id', 'f_name', 'f_type', 'f_desc' )
                                ->from( 'publicationsaddfields' )
                                ->where( 'publicationsTypes_id', '=', $id );

                $return['ser_addFields'] = $query->cached(60)->execute()->as_array();

                return $return;
            }
        }else
        {
            return array( );
        }
    }

    public function publ_types_save( $lang, $id = NULL )
    {
        if( $post = Model_Publications::publ_types_validate() )
        {
            // add / to end of 'url' if it missed
            $post['url'] = basename( $post['url'] );
            $post['parent_url'] = str_replace( '//', '/', "{$post['parent_url']}/" );

            if( (int) $id )
            { // update
                $id = (int) $id;

                DB::update( 'publicationstypes' )
                        ->set(
                                array(
                                    'parent_id' => $post['parent_id'],
                                    'parent_url' => $post['parent_url'],
                                    'name' => $post['name'],
                                    'url' => $post['url'],
                                    'have_addFields' => $post['have_addFields'],
                                    'ser_priFields' => $post['ser_priFields'],
                                    'ser_addFields' => $post['ser_addFields'],
                                    'status' => $post['status'],
                                    'template' => $post['template'],
                                    'pubtype_title' => $post['pubtype_title'],
                                )
                        )
                        ->where( 'id', '=', $id )
                        ->execute()
                ;
            }else
            { // create
                $id = $total_rows = 0;

                list($id, $total_rows) = DB::insert( 'publicationstypes', array(
                                    'parent_id', 'parent_url',
                                    'name',
                                    'url', 'have_addFields',
                                    'ser_priFields', 'ser_addFields',
                                    'status', 'template', 'pubtype_title'
                                        )
                                )
                                ->values(
                                        array(
                                            $post['parent_id'],
                                            $post['parent_url'],
                                            $post['name'],
                                            $post['url'],
                                            $post['have_addFields'],
                                            $post['ser_priFields'],
                                            $post['ser_addFields'],
                                            $post['status'],
                                            $post['template'],
                                            $post['pubtype_title'],
                                        )
                                )
                                ->execute();
            }
            self::updateParentPath( $id, (int) $post['parent_id'] );

            Model_Publications::publ_types_fields_update( $lang, $id, $post );

            return true;
        }else
        {
            return false;
        }
    }

    public function publ_types_fields_update( $lang = NULL, $type_id = NULL, $post = array( ) )
    {

        if( !$lang OR !$type_id )
            return false;

        // Clean Up incative Publictions Standart Fileds for current type_id

        $priFields = unserialize( $post['ser_priFields'] );

        foreach( Controller_Publications::$pri_fields_names as $key => $value )
        {
            if( !isset( $priFields[$key] ) && empty( $priFields[$key] ) )
                DB::update( 'publications' )
                        ->set(
                                array(
                                    $key => '',
                                )
                        )
                        ->where( 'id', '=', $type_id )
                        ->and_where( 'languages_id', '=', $lang )
                        ->execute()
                ;
        }

        // Clean Up inactive Publictions Additional Fileds for current type_id

        $query = DB::select()
                        ->from( 'publicationsaddfields' )
                        ->where( 'publicationsTypes_id', '=', $type_id );

        $result = $query->execute();

        if( $result->count() != 0 )
        {
            foreach( $result->as_array() as $row )
            {
                if( !isset( $_POST['addFld_name'][$row['id']] ) )
                {
                    DB::delete( 'publicationsaddfields' )
                            ->where( 'publicationsTypes_id', '=', $type_id )
                            ->and_where( 'id', '=', $row['id'] )->execute();
                }else
                {
                    DB::update( 'publicationsaddfields' )
                            ->set(
                                    array(
                                        'f_name' => $_POST['addFld_name'][$row['id']],
                                        'f_type' => $_POST['addFld_type'][$row['id']],
                                        'f_desc' => $_POST['addFld_desc'][$row['id']],
                                    )
                            )
                            ->where( 'publicationsTypes_id', '=', $type_id )
                            ->and_where( 'id', '=', $row['id'] )->execute();
                }
            }
        }

        // Add new Publictions Additional Fileds for current type_id

        if( isset( $_POST['newAddFields'] ) )
        {
            foreach( $_POST['newAddFields'] as $key => $value )
            {
                DB::insert( 'publicationsaddfields', array(
                            'publicationsTypes_id',
                            'f_name',
                            'f_type',
                            'f_desc',
                                )
                        )
                        ->values(
                                array(
                                    $type_id,
                                    $value['name'],
                                    $value['type'],
                                    $value['desc'],
                                )
                        )
                        ->execute()
                ;
            }
        }

        // Update

        $query = DB::select()
                        ->from( 'publicationsaddfields' )
                        ->where( 'publicationsTypes_id', '=', $type_id );

        $result = $query->execute()->as_array();

        $ser_addFields = array( );

        foreach( $result as $value )
        {
            $ser_addFields[$value['id']] = array(
                'f_name' => $value['f_name'],
                'f_type' => $value['f_type'],
                'f_desc' => $value['f_desc'],
            );
        }

        DB::update( 'publicationstypes' )
                ->set(
                        array(
                            'ser_addFields' => $post['ser_addFields'],
                        )
                )
                ->where( 'id', '=', $type_id )
                ->execute()
        ;
    }

    public function publ_types_delete( $id = NULL )
    {

        if( is_numeric( $id ) )
        {
            $query = DB::select( 'publications.id' )
                            ->from( 'publications' )
                            ->join( 'publicationstypes' )
                            ->on( 'publicationstypes.id', '=', 'publications.publicationstypes_id' )
                            ->where( 'publications.publicationstypes_id', '=', $id )
            ;
            foreach( $query->execute()->as_array() as $value )
                Model_Publications::publ_delete( $value['id'] );


            $query = DB::delete( 'publicationstypes' );
            $query->where( 'id', '=', $id );
            $total_rows = $query->execute();
        }
        return true;
    }

    public function publ_types_delete_list( $array )
    {
        if( count( $array ) )
        {
            foreach( $array as $value )
            {
                Model_Publications::publ_types_delete( $value );
            }
        }
        return true;
    }

    public function publ_types_active( $id = NULL )
    {
        if( is_numeric( $id ) )
        {
            DB::query( Database::UPDATE, "UPDATE publicationstypes SET status = !status WHERE id =$id LIMIT 1" )
                    ->execute();
        }
        return true;
    }

    public function publ_types_validate()
    {
        $keys = array(
            'parent_url', 'parent_id', 'name',
            'url', 'have_addFields',
            'ser_priFields', 'ser_addFields',
            'status', 'template', 'pubtype_title'
        );

        $addFields = $newAddFields = array( );

        if( isset( $_POST['addFld_name'] ) && count( $_POST['addFld_name'] ) )
        {
            foreach( $_POST['addFld_name'] as $key => $value )
            {
                $addFields[$key] = array( 'name' => $value, 'type' => $_POST['addFld_type'][$key], 'desc' => $_POST['addFld_desc'][$key], );
            }
        }

        if( isset( $_POST['newAddFldName'] ) && count( $_POST['newAddFldName'] ) )
        {
            foreach( $_POST['newAddFldName'] as $key => $value )
            {
                $newAddFields[$key] = array(
                    'name' => $value,
                    'type' => $_POST['newAddFldType'][$key],
                    'desc' => $_POST['newAddFldDesc'][$key],
                );
                $addFields[] = $newAddFields[$key];
            }
            $_POST['newAddFields'] = $newAddFields;
        }

        @$_POST['ser_priFields'] = serialize( $_POST['priFields'] );
        @$_POST['ser_addFields'] = serialize( $addFields );
        $params = Arr::extract( $_POST, $keys, '' );

        $post = Validate::factory( $params )
                        ->rule( 'parent_url', 'not_empty' )
                        ->rule( 'parent_id', 'digit' )
                        ->rule( 'name', 'not_empty' )
                        ->rule( 'pubtype_title', 'not_empty' )
                        ->rule( 'name', 'max_length', array( 255 ) )
                        ->rule( 'url', 'not_empty' )
                        ->rule( 'url', 'max_length', array( 255 ) )
                        ->rule( 'have_addFields', 'digit' )
        ;

        if( $post->check() )
        {
            return $params;
        }else
        {
            $this->errors = $post->errors( 'validate' );
        }
    }

    public function publ_add_fields_types()
    {
        $return = $enum_type = array( );
        $query = DB::query( Database::SELECT, "SHOW COLUMNS FROM publicationsaddfields LIKE 'f_type'" );

        $result = $query->cached(10)->execute(); 
        

        if( $result->count() == 0 )
            return $enum_type;
        else
        {
            $columns = $result->current();
            preg_match_all( "|'([^']+)'|i", $columns['Type'], $enum_type );

            foreach( $enum_type[1] as $value )
            {
                $return[$value] = I18n::get( ucfirst( $value ) );
            }

            return $return;
        }
    }

    static public function sitemap()
    {
        $res = DB::select(
                                'id', 'parent_id', 'name', array( DB::expr( '0.5' ), 'priority' ), array( DB::expr( "CONCAT('publications', parent_url, '/' ,url)" ), 'page_url' )
                        )
                        ->from( 'publicationstypes' )
                        ->where( 'status', '=', 1 )
                        ->execute()
                        ->as_array( 'id' );
        return $res;
    }

    static public function sitemapXML()
    {
        $res = DB::select(
                                'publications.id', array( DB::expr( '0.8' ), 'priority' ), array( DB::expr( "CONCAT('publications', parent_url, '/' ,url, '/', pub_url)" ), 'page_url' )
                        )
                        ->from( 'publications' )
                        ->join( 'publicationstypes' )
                        ->on( 'publications.publicationstypes_id', '=', 'publicationstypes.id' )
                        ->where( 'publications.status', '=', 1 )
                        ->execute()
                        ->as_array();
        return $res;
    }

    function copy2otherlang( $lang_id = 1, $id )
    {
        if( !$_POST['toPubtype'] || !$_POST['copy_for_lang'] )
            return false;

        $pub = $this->publ_load( $id );
        unset( $pub['id'] );
        foreach( $_POST['copy_for_lang'] as $key => $item )
        {
            if( $item == 1 )
            {
                $pub['languages_id'] = $key;
                $pub['publicationstypes_id'] = $_POST['toPubtype'];

                unset( $pub['ser_priFields'] );
                unset( $pub['ser_addFields'] );
                $pubaddfields = $pub['add_fields'];
                unset( $pub['add_fields'] );
                unset( $pub['publ_type_url'] );

                $temppub = self::publ_load_by_url( $pub['pub_url'], $key );
                if( count( $temppub ) > 0 )
                    return array( );

                /* insert publication */
                list($insert_id, $total_rows) = DB::insert( 'publications', array_keys( $pub ) )
                                ->values( $pub )
                                ->execute()
                ;
                /* get add values of pub */
                $result = DB::select()
                                ->from( 'publicationsaddvalues' )
                                ->where( 'publications_id', '=', $id )
                                ->execute()
                ;
                /**/
                /* копирование картинок */
                $source = MEDIAPATH . "publications/" . $id . '/';
                $dest = MEDIAPATH . "publications/" . $insert_id . '/';
                if( !file_exists( $dest ) )
                    @mkdir( $dest, 0755, TRUE );


                if( $objDir = @dir( $source ) )
                {
                    while( false !== ($entry = $objDir->read()) )
                    {
                        if( $entry != '.' && $entry != '..' )
                            @copy( $source . $entry, $dest . $entry );
                    }
                }
                /**/

                if( $result->count() != 0 )
                {
                    $result = $result->as_array();
                    /* копирование дополнительных полей */
                    foreach( $pubaddfields as $item )
                    {
                        $newCatFields = self::getAddFieldsId( array( 'publicationsTypes_id' => $_POST['toPubtype'], 'f_name' => $item['f_name'] ) );
                        if( array_search( $item['f_id'], array_keys( $newCatFields ) ) === FALSE )
                            DB::insert( 'publicationsaddvalues', array(
                                        'publications_id',
                                        'publicationsAddFields_id',
                                        'languages_id',
                                        'value_int',
                                        'value_float',
                                        'value_string',
                                        'value_text',
                                        'value_date',
                                    ) )
                                    ->values( array(
                                        $insert_id,
                                        $newCatFields['publicationsTypes_id'],
                                        $key,
                                        $item['value_int'],
                                        $item['value_float'],
                                        $item['value_string'],
                                        $item['value_text'],
                                        $item['value_date'],
                                    ) )
                                    ->execute()
                            ;
                    }
                    /* энд копирование дополнительных полей */
                }
            }
        }
        return 1;
    }

    static function getAddFieldsId( $where = NULL, $ascurrent = TRUE )
    {
        $query = DB::select()
                        ->from( 'publicationsaddfields' )
        ;
        if( $where )
        {
            foreach( $where as $key => $item )
                $query->where( $key, '=', $item );
        }
        $result = $query->cached(60)->execute();

        if( $result->count() == 0 )
            return array( );
        else
        {
            if( $result->count() == 1 && $ascurrent )
                return $result->current();
            else
                return $result->as_array( 'id' );
        }
    }

    public function types_copy( $lang_id = 1, $id )
    {
        $pubtype = $this->publ_types_load( $id );
        unset( $pubtype['id'] );
        $pubtype['ser_priFields'] = serialize( $pubtype['ser_priFields'] );
        $pubtype['ser_addFields'] = serialize( $pubtype['ser_addFields'] );
        $pubtype['name'] .= '_copy';
        $pubtype['url'] .= '_copy';

        $addFields = self::getAddFieldsId( array( 'publicationsTypes_id' => $id ), FALSE );

//        $temppubtype = $this->publ_types_load($id); уникальности нет.
        list($insert_id, $affected_rows) = DB::insert( 'publicationstypes', array_keys( $pubtype ) )
                        ->values( $pubtype )
                        ->execute()
        ;

        foreach( $addFields as $key => $item )
        {
            $item['publicationsTypes_id'] = $insert_id;
            unset( $item['id'] );
            list($insert_id, $affected_rows) = DB::insert( 'publicationsaddfields', array_keys( $item ) )
                            ->values( $item )
                            ->execute()
            ;
        }

        return 1;
    }

    /**
     * function return all child of defined types, multilevel
     *
     * @param array         array of types
     * @return array        multilevel array of cats
     */
    static public function getTypesRecurse( array $types = array( ) )
    {
        $return = array( );
        //echo(Kohana::debug($types));
        foreach( $types as $item )
        {
            $item['depth'] = substr_count( $item['parents_path'], ',' ) - 1;
            $item['preTitle'] = str_repeat( '&nbsp;', $item['depth'] ) . ( $item['depth'] != 0 ? str_repeat( '-', $item['depth'] ) : '');

            $return[$item['parent_id']][$item['id']] = $item;
//        echo(Kohana::debug($types));
//        echo'------------------------';
        }
        return $return;
    }

    /**
     * function return children of child_id or false
     *
     * @param array         all pub types
     * @param int           $child_id parent_id of children
     * @return array        children array
     * @return bool         FALSE
     */
    static function hasChildTypes( $data = array( ), $child_id = NULL )
    {
        $children = array( );
        foreach( self::$allPubTypes as $item )
        {
            if( $item['parent_id'] == $child_id )
                $children[$item['id']] = $item;
        }

        if( count( $children ) > 0 )
            return $children;
        else
            return false;
    }

    static function getTypesFromID( $pubId = NULL )
    {
        $result = DB::select()
                        ->from( 'publicationstypes' )
                        ->where( DB::expr( ' FIND_IN_SET(' ), DB::expr( '' ), DB::expr( (int) $pubId . ',parents_path)' ) )
                        ->cached(30)
                ->execute()
        ;

        if( $result->count() == 0 )
            return array( );
        return $result->as_array( 'id' );
    }

    static function getCats( $pubtype_id = NULL )
    {
        $result = DB::select()
                        ->from( 'publicationstypes' )
                        ->where( 'publicationstypes.parent_id', '=', $pubtype_id )
                        ->and_where( 'publicationstypes.status', '=', 1 )
                        ->order_by( 'orderBy' )
                        ->cached(30)
                        ->execute()
        ;

        if( $result->count() == 0 )
            return array( );
        return $result->as_array( 'id' );
    }

    static function getSearch( $queryOrig, $notSearchInPubTypeID = NULL, $searchInPubTypeID = NULL )
    {
        $queryOrig = mb_strtolower( $queryOrig, 'UTF-8' );
        $table = array( 'pub_name', 'pub_url', 'pub_title', 'pub_fulltext', 'pub_shorttext', 'pub_tags' );
        $query = explode( ' ', $queryOrig );

        $found = array( );

        $str = "SELECT publications.*, publicationstypes.url, publicationstypes.parent_url AS pub_typeparenturl, publicationstypes.url AS pub_typeurl FROM publications
                LEFT JOIN publicationstypes ON publicationstypes.id = publications.publicationstypes_id
                WHERE ";
        $like = '';
        foreach( $table as $item )
        {
            foreach( $query as $word )
            {
                $like .= " OR LCASE(publications.$item) LIKE '%" . mysql_escape_string( $word ) . "%' ";
            }
        }

        $where = (isset( $notSearchInPubTypeID ) && !empty( $notSearchInPubTypeID ) ) ?
                'publicationstypes.id IN (' . implode( ',', $notSearchInPubTypeID ) . ') AND ' :
                'publicationstypes.id != ' . implode( ' AND publicationstypes.id != ', $searchInPubTypeID ) . ' AND ';

        $select = $str . $where . "publications.status = 1 AND publications.languages_id = " . CURRENT_LANG_ID . " AND (" . ltrim( $like, ' OR' ) . ")";

        $queryDB = DB::query( Database::SELECT, $select . ' ORDER BY pub_date DESC' );
        $result = $queryDB->cached(60)->execute();

        if( $result->count() == 0 )
            return array( );

        $found = $result->as_array();
        $return = array( );
        $langs = explode( '|', '|' . LANGUAGE_ROUTE );
        $lang = ( $langs[CURRENT_LANG_ID] == LANGUAGE_ROUTE_DEFAULT ) ? '' : '/' . $langs[CURRENT_LANG_ID];

        foreach( $found as $field )
        {
            $text = '';
            foreach( $table as $item )
            {
                foreach( $query as $word )
                    if( strstr( mb_strtolower( strip_tags( $field[$item] ), 'UTF-8' ), $word ) )
                        $text .= $field[$item] . ' ';
            }
            $return[] = array(
                'text' => $text,
                'url' => $lang . $field['pub_typeparenturl'] . $field['pub_typeurl'] . '/' . $field['pub_url'],
                'date' => $field['pub_date'],
                'pub_shorttext' => $field['pub_shorttext'],
                'pub_image' => $field['pub_image'],
            );
        }

        return $return;
    }

    /**
     * будет работать и когда всего 1 шаблон публикаций!
     * @return type 
     */
    static public function get_pub_templates()
    {
        $template_list = array( );
        $template_info = Model_AdminTemplates::get_info( Model_AdminDomains::domain_template( $_SERVER['HTTP_HOST'] ) );
        $template_list = (array) $template_info->pubtemplates;        
        $template_list = array_combine( (array) $template_list['filename'], (array) $template_list['filename'] );
        return $template_list;
    }

    static function getAutors( $pubtype_id = NULL )
    {
        $return = array( );

        $user_IDs = DB::select( 'publicationsaddvalues.*', array( DB::expr( 'COUNT(publicationsaddvalues.id)' ), 'count' ), 'pt.url' )
                        ->from( 'publicationsaddvalues' )
                        ->join( 'publications' )
                        ->on( 'publicationsaddvalues.publications_id', '=', 'publications.id' )
                        ->join( array( 'publicationstypes', 'pt' ), 'left' )
                        ->on( 'publications.publicationstypes_id', '=', 'pt.id' )
                        ->where( 'publicationsAddFields_id', 'IN', DB::expr( '(
                SELECT id
                FROM publicationsaddfields
                WHERE f_name = \'user_id\')' ) )
                        ->and_where( 'publications.status', '=', 1 )
                        ->group_by( 'value_int' )
                        ->cached(60)
                        ->execute()
        ;


        if( $user_IDs->count() == 0 )
            return array( );
        $count = $user_IDs->as_array( 'value_int' );

        $user_IDs = $user_IDs->as_array( 'id', 'value_int' );
        $user_IDs = '(' . implode( ',', $user_IDs ) . ')';

        $return = DB::select()
                        ->from( 'site_users' )
                        ->where( 'id', 'IN', DB::expr( $user_IDs ) )
                        ->cached(60)
                        ->execute()
        ;
        $return = $return->as_array( 'id' );
        $r = array( );
        foreach( $return as $key => $item )
        {
            $item['count'] = $count[$key]['count'];
            $item['pubaddfieldid'] = $count[$key]['publicationsAddFields_id'];
            $item['url'] = $count[$key]['url'];
            $r[] = $item;
        }

        return $r;
    }

    static function updateParentPath( $id = NULL, $parent_id = NULL )
    {
        if( isset( $id ) )
        {
            $parentsparents_path = DB::select( 'parents_path' )
                            ->from( 'publicationstypes' )
                            ->where( 'id', '=', $parent_id )
                            ->execute()
                            ->get( 'parents_path' );

            DB::update( 'publicationstypes' )
                    ->set( array(
                        'parents_path' => $parentsparents_path . $id . ','
                    ) )
                    ->where( 'id', '=', $id )
                    ->execute()
            ;


            if( isset( $_POST['old_parent'] ) && !empty( $_POST['old_parent'] ) && $_POST['old_parent'] != $parent_id )
            {
                DB::update( 'publicationstypes' )
                        ->set( array(
                            'parents_path' => DB::expr( "REPLACE(parents_path, '{$_POST['old_parent']},', '$parent_id,' )" )
                        ) )
                        ->where( DB::expr( 'FIND_IN_SET(' ), '', DB::expr( $id . ',parents_path)' ) )
                        ->execute()
                ;
            }
        }
    }

    static public function getCommentURL( $publ_id = NULL )
    {
        return Db::query( Database::SELECT, "SELECT CONCAT(CONCAT(pt.parent_url, pt.url), '/', p.pub_url) AS url "
                        . " FROM publications AS p LEFT JOIN publicationstypes AS pt ON p.publicationstypes_id = pt.id "
                        . " WHERE p.id = " . (int) $publ_id
                )
                ->cached(30)->execute()->get( 'url', '' );
    }

    /**
     * function sort pub types for select. 
     * 
     * @param   array   $pub_types all sorted pub_types
     * @return  array    odnomerniu
     */
    static function getSortedTypesForSelect( $pub_types, $id = 0 )
    {
        //die(Kohana::debug($pub_types));
        $return = array( );
        if( isset( $pub_types[$id] ) ):
            foreach( $pub_types[$id] as $item )
            {
                //echo (Kohana::debug($item));

                $item['depth'] = substr_count( $item['parents_path'], ',' ) - 1;

                //echo('parents_path = '.$item['parents_path']);
                //echo('depth = '.$item['depth']);
                
                $item['name'] = ( $item['depth'] == 0 ? "<b>{$item['name']}</b>" : $item['name']);
                $item['preTitle'] = str_repeat( '&nbsp;', $item['depth'] ) . ( $item['depth'] != 0 ? str_repeat( '-', $item['depth'] ) : '');

                $return[$item['id']] = $item;
                //echo'---';
                //echo (Kohana::debug($item));
                //echo (Kohana::debug($item['id']));


                $child = self::getSortedTypesForSelect( $pub_types, $item['id'] );
//                echo '-------------';
//                echo (Kohana::debug($child));
                $return = array_merge( $return, $child );
            }

        endif;
        return $return;
    }

    static function getFilter()
    {
        //die(Kohana::debug($_POST));
        $filter = (array) json_decode( Cookie::get( 'filter_' . MODULE_NAME ), TRUE );

        //die(Kohana::debug($_POST));
//
        if( isset( $_POST['publ_types_id'] ) )
            $filter['pub_id'] = $_POST['publ_types_id'];


        if( isset( $_POST['filter'] ) )
            //$filter = array_merge( $filter, $_POST['filter'] );



        if( isset( $_POST['filter'] ) || isset( $_POST['publ_types_id'] ) )
            Cookie::set( 'filter_' . MODULE_NAME, json_encode( $filter ) );

        $where = array( );
        if( count( $filter ) > 0 ):
            foreach( $filter as $key => $item )
            {
                if( !empty( $item ) )
                {
                    switch( $key )
                    {
                        case 'pub_id':
                            $ids = Model_Publications::getTypesFromID( $filter['pub_id'] );

                            $ids = array_keys( $ids );
                            /* define ids */
                            if( count( $ids ) > 0 )
                                $where[] = array( 'publicationstypes_id', 'IN', DB::expr( '(' . implode( ',', $ids ) . ')' ) );
                            /**/
                            break;

                        case 'pub_title':
                            $where[] = array( $key, 'LIKE', "%$item%" );
                            break;

                        case 'pub_date':
                            $where[] = array( DB::expr( 'TO_DAYS(' . mysql_escape_string( $key ) . ')' ), '=', DB::expr( 'TO_DAYS("' . mysql_escape_string( $item ) . '")' ) );
                            break;

                        default:
                            $where[] = array( $key, '=', $item );
                    }
                }
            }
        endif;
   

     
        return array( $where, $filter );
    }

}
