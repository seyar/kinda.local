<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 24.12.2010 - 10:03:38 Exp $
 *
 * Project:     kontext
 * File:        ratings.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */

class Model_Frontend_Ratings extends Model
{
    static function getItem( $id = NULL )
    {
        if( !isset($id) ) return false;

        $return = DB::select('publications.*', array( DB::expr('publications.ratings_sum / publications.ratings_count'), 'rating') )
            ->from('publications')
            ->where('id','=',$id)
            ->limit(1)
            ->execute()
            ;
        if( $return->count() == 0 ) return 0;
        else
            return $return->current();
    }

    static function rate( $id = NULL, $value = NULL )
    {
        if( !isset($id) ) return false;
        
        DB::update('publications')->set(array(
                    'ratings_sum' => DB::expr('ratings_sum + '.(int)$value),
                    'ratings_count' => DB::expr('ratings_count + 1')
                ))
            ->where('id', '=',$id)
            ->execute()
            ;

        $rated_pubs = (array)Cookie::get('rated_pubs', array() );        
        array_push( $rated_pubs, $id);

        Cookie::set('rated_pubs', implode(',',$rated_pubs));
//        setcookie('rated_pubs', implode(',',$rated_pubs), 60*60*24*31, '/');

        $obj = self::getItem($id);
        
        return floor($obj['rating']);
    }
}
?>