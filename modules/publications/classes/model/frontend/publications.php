<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Frontend_Publications extends Model
{

    public static function init()
    {
        $query = DB::select()
                        ->from('publicationstypes')
                        ->order_by('id')
                        ->where('status', '=', 1)
		->cached(180)
        ;

        $result = $query->execute();

        if ($result->count())
        {
            $rows = array();
            $results = $result->as_array();

            foreach ($results as $value)
            {
                $rows[$value['id']] = array(
                    'id' => $value['id'],
                    'name' => $value['name'],
                    'parent_url' => $value['parent_url'],
                    'url' => $value['url'],
                );
            }

            Kohana_Controller_Quicky::$intermediate_vars['publications_types'] = $rows;

            Kohana_Controller_Quicky::$intermediate_vars['publRootUrl'] = Kohana::config('publications')->pubRootUrl;

//            $tops = Model_Frontend_Publications::publication_top(CURRENT_LANG_ID);

            if (isset($tops) && count($tops) > 0)
                Kohana_Controller_Quicky::$intermediate_vars['publications_tops'] = $tops[0];
        }
    }

    public static function publication_top($lang_id = NULL, $publ_type_id = NULL, $limit = 4)
    {
		self::init();

		// Load top from all categories
        $query_parts = $results = array();

        if ($publ_type_id === null) // Show top for all publications type
        {
            foreach (Kohana_Controller_Quicky::$intermediate_vars['publications_types'] as $key => $value)
            {
                $query_parts[] = "( SELECT p$key.id, p$key.publicationstypes_id, p$key.pub_url, p$key.pub_name,p$key.pub_shorttext, p$key.pub_date, p$key.pub_image "
                        . " FROM publications AS p$key "
                        . " WHERE p$key.status " . ((int)$lang_id ? "AND p$key.languages_id = $lang_id" : "") . " AND p$key.publicationstypes_id = " . $value['id']
                        . " ORDER BY p$key.pub_date DESC LIMIT " . Kohana::config('publications')->top_count . " )";
            }

            $results = Db::query(Database::SELECT, implode("\n UNION ALL \n", $query_parts)." LIMIT $limit")
							->cached(300)
                            ->execute()
                            ->as_array()
            ;

            $rows = array();
            foreach ($results as $value)
            {
//                $rows[$value['publicationstypes_id']][] = $value;
                $rows[] = $value;
            }

            return array($rows, '', 0);
        }

        $results = DB::select('id', 'publicationstypes_id', 'pub_url', 'pub_name', 'pub_date', 'pub_image', 'pub_shorttext')
                        ->from('publications')
                        ->where('status', '=', 1)
                        ->and_where('languages_id', '=', $lang_id)
        ;

        if (!is_numeric($publ_type_id))
        {
            $res = DB::select('id')
                            ->from('publicationstypes')
                            ->where('url', '=', $publ_type_id)
                            ->and_where('status', '=', 1)
                            ->cached(30)
                            ->execute()
                            ->as_array();
            $results->where('publicationstypes_id', '=', $res[0]['id']);
        }
        $limit = $limit ? $limit : Kohana::config('publications')->top_count;
        $results = $results->limit($limit)
                        ->order_by('pub_date', 'DESC')
                        ->cached(60)
                        ->execute()
                        ->as_array();

        $rows = array();
        foreach ($results as $value)
        {
            $rows[] = $value;
        }

        return $rows;
    }

    public static function publication_types($lang_id = NULL)
    {
        if (!is_numeric($lang_id))
            return array();

        $query = DB::select('id', 'name', 'parent_id', 'parent_url', 'url')
                        ->from('publicationstypes')
                        ->order_by('name')
		->cached(60)
        ;

        $result = $query->execute();

        if ($result->count() == 0)
            return array();
        else
            return $result->as_array('id');
    }

    public static function publication_index($lang_id = NULL, $publ_type_id = NULL, $rows_per_page = NULL, $where = NULL )
    {
        $rows_per_page = isset($rows_per_page) && !empty($rows_per_page) ? $rows_per_page : Kohana::config('publications')->rows_per_page;

        if (!is_numeric($lang_id))
            return array();

        $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
                        ->from('publications')
                        ->order_by('pub_date', 'DESC')
                    ->cached(60)
        ;

        $query = DB::select('publications.*', array('publicationstypes.url', 'pubtype_url'), array('publicationstypes.parent_url', 'pubtype_parent_url'), array('publicationstypes.name', 'pub_type_name'), array( DB::expr('publications.ratings_sum / publications.ratings_count'), 'rating') )
                ->from('publications')
                ->join('publicationstypes', 'left')
                    ->on('publicationstypes.id','=','publications.publicationstypes_id')
                ->order_by('pub_date', 'DESC')
                ->cached(60)
        ;

        if( is_array($publ_type_id) && count($publ_type_id) > 0 )
        {
            $in = '('.implode(',',$publ_type_id).')';
            $query->where('publicationstypes_id', 'IN', DB::expr($in) );
            $query_c->where('publicationstypes_id', 'IN', DB::expr($in) );
        }
        elseif(isset($publ_type_id) && !empty($publ_type_id) )
        {
                $query->where('publicationstypes_id', '=', $publ_type_id);
                $query_c->where('publicationstypes_id', '=', $publ_type_id);
        }

        if( is_array($where) && count($where) > 0 )
        {
            foreach($where as $key => $item)
            {
                if( strstr($item['value'], 'TO_DAYS') )
                {
                    $query->where( DB::expr('TO_DAYS('.$key.')'), $item['op'], $item['value'] );
                    $query_c->where( DB::expr('TO_DAYS('.$key.')'), $item['op'], $item['value'] );
                }
                else
                {
                    $query->where( $key, $item['op'], $item['value'] );
                    $query_c->where( $key, $item['op'], $item['value'] );
                }
            }
        }


        $count = $query_c
                        ->execute()
                        ->get('mycount');

        if (isset($_POST['page']))
            $_GET['page'] = $_POST['page'];

        $pagination = Pagination::factory(array(
                    'previous_page' => '<img src="/templates/kinda/images/pagination_prev.png" alt="" class="vMiddle"/>',
                    'next_page' => '<img src="/templates/kinda/images/pagination_next.png" alt="" class="vMiddle"/>',
                    'total_items' => $count,
                    'items_per_page' => $rows_per_page,
                ));

        $result = $query->limit($pagination->items_per_page)
                        ->offset($pagination->offset)
                        ->execute()
        ;
        
        $page_links_str = $pagination->render('pagination/digg');

        if ($result->count() == 0)
            return array('', array());
        else
        {
            $return = array();
            /* count comments */
            /*
            if( method_exists( 'Model_Frontend_Comments', 'countcomments' ) )
            {
                $oComments = new Model_Frontend_Comments();
                $comments = $oComments->countcomments(array());
            }
            * */
            /**/            
            foreach($result->as_array() as $item)
            {
                if(strstr($item['pub_shorttext'], 'youtube.com')) // TODO need delete
                {
                    $image = self::getyoutubeimage($item['pub_shorttext']);
                    $item['youtube_imgcode'] = $image['youtube_imgcode'];
                    $item['youtube_preview'] = $image['youtube_preview'];
                }
                $item['qa_ser_fields'] = unserialize($item['qa_ser_fields']);

                if( isset($comments[$item['id']]) && !empty($comments[$item['id']]) ) $item['comments'] = $comments[$item['id']];

                $return[] = $item;
            }            
            return array($return, $page_links_str, $pagination->total_pages);
        }
    }

    static function getyoutubeimage( $str = NULL )
    {
        $return = array();
        if($str)
        {
            preg_match('|youtube\.com[^>].+[^>](.+)\?|Uis', $str, $matches);
            $return['youtube_imgcode'] = $matches[1];
            $return['youtube_preview'] = 'http://img.youtube.com/vi/'.$matches[1].'/1.jpg';
        }
        return $return;
    }

    static function getDayNew()
    {
        $return = array();
        $return = DB::select('p.*')
            ->from( array('publicationsaddvalues', 'afv'))
            ->join( array('publications','p'), 'left' )
                ->on('p.id','=', 'afv.publications_id')
            ->where('afv.value_int','=', '1')
            ->and_where('afv.publicationsAddFields_id', '=', DB::expr('(SELECT id FROM publicationsaddfields WHERE publicationsaddfields.f_name = "new_of_day" LIMIT 1)') )
            ->order_by('pub_date','desc')
            ->limit(1)
            ->cached(30)
            ->execute()
            ->current()
        ;

        return $return;
    }

}