<?php defined('SYSPATH') or die('No direct script access.');

return array
(
    'Publications Descr Short'	=> 'Module "Publications" is designed to accommodate and manage online news, articles and other useful information. This module allows you to print publications, various graphic images and other information on your site. ',
    'Publications Descr Full'	=> 'To add news or articles, click "Modules" item "Publications". <br />
	You can break all your publications in several sections. For example, such as: news, articles, announcements, etc. To do this, go to the item "Publications Management" (in the upper right corner). Here you can edit an existing partition, or add new ones.<br />
                                    Once you have made the necessary surgery, return to the item "Content-> Publications. This page lists all your pages on all sections. To see all the generated pages in a section, you can apply a filter, selecting the desired section in the drop-down list in the upper right corner and clicking "Show".<br />
                                    To create a new page, click "Create". In the window on the tab "Main" in the field "Name" enter the name of the page. The drop-down list of "Path to the page, select the desired section, and in the box to the right enter the URL of the page in the format *. html, where *- a short page name in Latin letters (for example, news_priglashenie.html). In the "Page Title" you enter a title page, which will be displayed at the top of the browser, through which the created page will open. ',


    'Создать копию на других языках' => 'Create a copy in other languages',
);

