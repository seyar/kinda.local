<?php

defined('SYSPATH') OR die('No direct access allowed.');

return array
    (
    'lang' => 'ru', // Default the  language.
    'path' => dirname(__FILE__) . '/../views/', // Admin templates folder.

    'pubRootUrl' => 'publications',
    'images_folder' => 'publications',
    'rows_per_page' => 15,
    'rowsperpage_front_news' => 15,
    

    'top_count' => 4,
    'image_width' => 800,
    'image_height' => 600,
    'image_prev_width' => 280,
    'image_prev_height' => 187,
    'top_count' => 6,
    'max_images_count_back' => 6,

    'rating_values' => array(-3,-2,-1,0,'+1','+2','+3'),
    'watermark' => DOCROOT.'static/watermark.png',
    
    'titles' => array(
        'main' => 'Новости',        
    ),
);
