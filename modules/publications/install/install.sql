# Dumping structure for table crimea.publicationstypes
DROP TABLE IF EXISTS `publicationstypes`;
CREATE TABLE IF NOT EXISTS `publicationstypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `parent_url` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `have_addFields` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ser_priFields` text,
  `ser_addFields` text,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `publications`;
CREATE TABLE IF NOT EXISTS `publications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publicationstypes_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `pub_url` char(255) NOT NULL,
  `pub_title` char(255) NOT NULL,
  `pub_date` datetime NOT NULL,
  `pub_expiredate` datetime NOT NULL,
  `pub_name` char(255) NOT NULL,
  `pub_shorttext` text NOT NULL,
  `pub_fulltext` text NOT NULL,
  `pub_keywords` char(255) NOT NULL,
  `pub_description` char(255) NOT NULL,
  `pub_image` char(255) NOT NULL,
  `pub_source` varchar(255) NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_create` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_cached` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `publicationsTypes_id` (`publicationstypes_id`),
  KEY `languages_id` (`languages_id`),
  CONSTRAINT `FK_language_id` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_publicationsTypes_id` FOREIGN KEY (`publicationstypes_id`) REFERENCES `publicationstypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table crimea.publicationsaddfields
DROP TABLE IF EXISTS `publicationsaddfields`;
CREATE TABLE IF NOT EXISTS `publicationsaddfields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publicationsTypes_id` int(10) unsigned NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `f_type` enum('int','float','string','date','text','logic') DEFAULT NULL,
  `f_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `publicationsTypes_id` (`publicationsTypes_id`),
  CONSTRAINT `publicationsTypes_id` FOREIGN KEY (`publicationsTypes_id`) REFERENCES `publicationstypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table crimea.publicationsaddvalues
DROP TABLE IF EXISTS `publicationsaddvalues`;
CREATE TABLE IF NOT EXISTS `publicationsaddvalues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publications_id` int(10) unsigned NOT NULL,
  `publicationsAddFields_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `value_int` bigint(20) DEFAULT NULL,
  `value_float` float DEFAULT NULL,
  `value_string` varchar(255) DEFAULT NULL,
  `value_text` text,
  `value_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `publicationsAddFields_id` (`publicationsAddFields_id`),
  KEY `languages_id` (`languages_id`),
  CONSTRAINT `FK_languages_id` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_publicationsAddFields_id` FOREIGN KEY (`publicationsAddFields_id`) REFERENCES `publicationsaddfields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `publicationphotos` (
  `pub_id` int(10) unsigned default NULL,
  `filename` varchar(255) default NULL,
  `comment` varchar(255) default NULL,
  UNIQUE KEY `pub_id_filename` (`pub_id`,`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;