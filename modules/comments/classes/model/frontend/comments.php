<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Frontend_Comments extends Model {

    protected $_name = 'comments'; // table name
    protected $_moduleId = null;
    protected $_objectId = null;

    public function setModule($moduleId)
    {
        $this->_moduleId = $moduleId;
    }

    public function setObject($objectId)
    {
        $this->_objectId = $objectId;
    }

    public function getModule()
    {
        return $this->_moduleId;
    }

    public function getObject()
    {
        return $this->_objectId;
    }

    public function find($where = null, $limit = null, $offset = null)
    {        
        $q = $this->_prepareFind($where);

        if($limit !== null)
        {
            $q->limit($limit);
            if( $offset ) $q->offset($offset);
        }

        $res = $q->execute()->as_array('id');
        $ret = array();
        $depth = 0;
        foreach ($res as $comment)
        {
            $comment['user'] = Model_Registration::get_items($comment['user_id']);
            $comment['depth'] = substr_count($comment['parents_path'],',')-1;
            if($comment['depth'] > Kohana::config('comments')->max_increment) $comment['depth'] = Kohana::config('comments')->max_increment;
            $ret[$comment['parent_id']][] = $comment;
        }
        return $ret;
    }

    public function count($where)
    {
        $q = $this->_prepareFind($where, array(DB::expr('count(*)'), 'count' ) );
        $res = $q->execute()->as_array();

        return $res[0]['count'];
    }

    public function countcomments($where)
    {

        $q = $this->_prepareFind($where, array(DB::expr('count(*)'),'count'), array('object_id') );

        $res = $q->group_by('object_id')->execute()->as_array('object_id','count');        
        return $res;
    }

    protected function _prepareFind($where, $columns = NULL, $columns_str = NULL)
    {        
        if($columns === null) $columns = DB::expr('*');
        if( $columns_str !== null )
        {
            $columns_str = implode(',',$columns_str);
            $q = DB::select($columns_str, $columns);
        }
        else
            $q = DB::select( $columns);

        $q->from($this->_name);

        if($this->_moduleId !== null)
            $q->where('module_id', '=', $this->_moduleId)
              ->where('object_id', '=', $this->_objectId);

        if( $where !== null && !empty($where))
        {
            if(is_array($where))
            {
                if(is_array($where[0]))
                    foreach($where as $cond)
                    {
                        $q->where($cond[0], $cond[1], $cond[2]);
                    }
                else $q->where($where[0], $where[1], $where[2]);
            }
            else $q->where('id', '=', $where);
        }
        return $q;
    }

    public function update(array $data, $id)
    {
        DB::update($this->_name)
                ->set($data)
                ->where('id', '=', $id)
                ->execute();
    }

    public function insert(array $data)
    {
        $data['created_at'] = DB::expr("NOW()");
        list($lastInsertId, $insertedRows) = DB::insert($this->_name, array_keys($data))
                        ->values(array_values($data))
                        ->execute();
        return array('lastInsertId' => $lastInsertId, '' => $insertedRows);
    }

    public function delete( $id )
    {
        $query = DB::delete($this->_name);
        if(is_array($id))
            $query->where('id', 'in', DB::expr("(".implode($id).")"));
        else $query->where('id', '=', $id);
        return $query->execute();
    }

    public function getFilterWhere($data)
    {
        $where = array();
        if($data['dateFrom']) $where[] = array('created_at', '>=', $data['dateFrom']);
        if($data['dateTo']) $where[] = array('created_at', '<=', $data['dateTo']);
        if( isset($data['status']) &&
            $data['status'] !== '' &&
            $data['status'] !== '-1'
          )
            $where[] = array('status', '=', $data['status']);
        return $where;
    }

}