<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Comments extends Model {
    protected $_name = null;
//
    public function find(array $where = array(), $limit = null, $offset = null)
    {
        $q = DB::select()->from($this->_name);
        foreach($where as $cond)
        {
            $q->where($cond[0], $cond[1], $cond[2]);
        }

        if($limit !== null)
        {
            $q->limit($limit);
            if( $offset ) $q->offset($offset);
        }

        $res = $q->execute()->as_array('id');
        return $res;
    }
//
    public function update($data, array $where)
    {
        $q = DB::update($this->_name)
                ->set($data);
        if(is_array($where[0]))
        {
            foreach($where as $cond)
            {
                $q->where($cond[0], $cond[1], $cond[2]);
            }
        }
        else
        {
            $q->where('id', '');
        }

        $q->execute();
    }
//
    public function insert($data)
    {
        list($lastInsertId, $insertedRows) = DB::insert($this->_name, array_keys($data))
                        ->values(array_values($data))
                        ->execute();
        return array('lastInsertId' => $lastInsertId, '' => $insertedRows);
    }

    public function delete($id)
    {
        $query = DB::delete($this->_name);

        if(is_array($where[0]))
        {
            foreach($where as $cond)
            {
                $q->where($cond[0], $cond[1], $cond[2]);
            }
        }
        else
        {
            $q->where($where[0], $where[1], $where[2]);
        }

        if (is_numeric($id))
	{
            $query->where('id', '=', $id);
	    $total_rows = $query->execute();
	}
        elseif( is_array($id) )
        {
            $query->where('id', 'in', DB::expr('('.implode(",", $id).')')) ;
        }

        $affectedRows = $query->execute();
    }
}
