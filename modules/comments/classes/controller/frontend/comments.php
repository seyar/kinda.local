<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Frontend_Comments extends Controller_Content {

    public $template		= 'comments/view.tpl';

    public $images_folder	= 'comments';
    protected $_user_id = null;
    protected $_user_name = null;
    protected $_model = null;

    public function before()
    {
        if ($this->request == Request::instance() && !Request::$is_ajax)
        {
            //throw new Exception('jhg');
        }
        
        parent::before();
        $this->_model = new Model_Frontend_Comments();

        $moduleId = intval($this->request->param('moduleId'));
        $objectId = intval($this->request->param('objectId'));

        if(!$moduleId && !$objectId) throw new Exception('Intvalid arguments');
        
        $this->_model->setModule($moduleId);
        $this->_model->setObject($objectId);

        $auth = Session::instance()->get('authorized', array());        
        if( !empty($auth) )
        {
            $this->_user_id = Session::instance()->get('user_id', null);
            $this->_user_name = $auth['name'];
        }

        if($this->request->action != 'view')
        {
            $this->auto_render = false;
//            if($this->_user_id === null)
//                throw new Exception();
        }
        $this->view->assign('moduleId', $moduleId);
        $this->view->assign('objectId', $objectId);
    }

    public function action_view()
    {        
        $this->view->assign('user_id',  $this->_user_id);
        $this->view->assign('comments', $this->_model->find(array('status', '=', '1')));
    }

    public function action_add()
    {
        if( ($post = $this->valid()) )
        {
            $moderate = Kohana::config('comments.moderate');
            $data = array(
                'module_id'  => $this->_model->getModule(),
                'object_id'  => $this->_model->getObject(),
                'user_id'    => $this->_user_id,
                'parent_id'  => $post['parent_id'],
                'parents_path'  => $post['parents_path'],
                'comment'    => Security::xss_clean($post['comment']),
                'rating'     => 0,
                'created_at' => date("Y:m:d H:i:s")
            );
            
            if($moderate) $data['status'] = 0;
            $res = $this->_model->insert($data);
            $this->_model->update(array('parents_path' => $post['parents_path'].$res['lastInsertId'].','), $res['lastInsertId']);
            if(!$moderate)
            {
                $data['id'] = $res['lastInsertId'];
                $data['user']['name'] = $this->_user_name;
                $data['depth'] =  substr_count($data['parents_path'],',');
                $this->view->assign('user_id', $this->_user_id);
                $this->view->assign('comments', array($data['parent_id']=>array($data)));
                $this->view->assign('id', $data['parent_id']);
                $response = $this->view->fetch($this->view->template_root.'/comments/recursion.tpl');
            }
//            $response .= $this->view->fetch($this->view->template_root.'/comments/add.tpl');
        }
        else
        {
            $response = $this->_errors;
            echo Kohana::debug($response);
        }
        exit($response);
    }

    public function action_rating()
    {
        if( !($id = intval($_POST['id'])) )
        {
            return;
        }

        $val = isset($_POST['val']) && $_POST['val'] >=0 ? "+1" : "-1";

        $data = array('rating' => DB::expr('rating'.$val));
        $this->_model->update($data, $id);
        die;
    }

    public function valid()
    {
        $keys = array(
                    'comment',
                    'parent_id',
                    'parents_path',
                );
        
        $params = Arr::extract($_POST, $keys, '');
        $post = Validate::factory($params)
                            ->rule('comment', 'not_empty')
                            ->rule('parent_id', 'not_empty')
                            ->rule('parent_id', 'digit');

        if ($post->check())
	{
	    return $params;
	}
	else
	{
            $this->_errors = $post->errors('validate');
	    //$this->view->assign('errors', $post->errors('validate'));
            return false;
	}
    }

    public function action_showComplaint()
    {        
        $responce = $this->view->fetch($this->view->template_root.'comments/complaint.tpl');
        die(print($responce));
    }

    public function action_sendComplaint()
    {
        if( !$_POST['comment'] || !$_POST['complain'] ) return false;

        $obj = $this->_model->find(array( array('id','=',$_POST['comment'])), 1);
        reset($obj);
        $obj = current($obj);
        reset($obj);
        $obj = current($obj);
        
        $message = include dirname(__FILE__).'/../../../views/message.php';
        
        if( !Email::send(
                Kohana::config('comments')->send_to,
                Kohana::config('comments')->send_from,
                Kohana::config('comments')->send_subject,
                $message
                ) )
                Kohana_log::instance()->add('email send error',
                        Kohana::debug(Kohana::config('comments')->send_to,
                        Kohana::config('comments')->send_from,
                        Kohana::config('comments')->send_subject,
                        $message)
                        );
        
        $this->request->redirect( Arr::get($_POST, 'referer', '/') );

    }

}