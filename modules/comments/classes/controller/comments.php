<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Comments extends Controller_AdminModule {

    public $template		= 'comments_list.tpl';

    public $module_title	= 'Comments';
    public $module_desc_short   = 'Comments Descr Short';
    public $module_desc_full	= 'Comments Descr Full';

    public function before()
    {
        if ($this->request == Request::instance()) {
            throw new Exception();
        }

        parent::before();
	$this->_model = new Model_Frontend_Comments();
    }

    public function action_index()
    {
        $where = array();
        if(Request::$method == 'POST')
        {
            $itemsPerPage = intval($_POST['rows_num']) > 10 ? intval($_POST['rows_num']) : 10;
            setcookie('bcommentsRowsPerPage', $itemsPerPage);
            $filter = $this->validData($_POST);
            if($filter !== false)
            {
                setcookie('commentsFilterData', json_encode($filter), time()+604800);
            }
        }else
        {

            $filter = $_COOKIE['commentsFilterData'];
            $filter = json_decode($filter, true);

            if( !is_array($filter) || !$this->validData($filter))
            {
                $filter = array();
            }

            $itemsPerPage = intval($_COOKIE['bcommentsRowsPerPage']);

        }

        $this->view->assign('filter', $filter);

        $where = $this->_model->getFilterWhere($filter);

        $totalRows = $this->_model->count($where);
        $itemsPerPage = $itemsPerPage > 10 ? $itemsPerPage : 10;

        if(isset($_POST['page']) && intval($_POST['page']) > 0)
        {
            $currentPage = intval($_POST['page']);
        }else{
            if(isset($_GET['page']) && intval($_GET['page']) > 0)
                $currentPage = intval($_GET['page']);
            else $currentPage = 1;
        }


        $this->view->assign('itemsPerPage', $itemsPerPage);
        $this->view->assign('currentPage', $currentPage);

        $paginator = Pagination::factory(
                array(
                    'current_page'   => $currentPage,
                    'total_items'    => $totalRows,
		    'items_per_page' => $itemsPerPage
                ))
                ;

        $this->view->assign('total_pages', $paginator->total_pages);
        $this->view->assign('items_per_page', $paginator->items_per_page);

        $comments = $this->_model->find($where, $paginator->items_per_page, $paginator->offset);
        $this->view->assign('rows', $comments);
    }

    public function action_active()
    {
	$id = intval($this->request->param('id'));
        if( !$id ) return;

        $data = array(
                'status' => DB::expr("!status")
            );

        $this->_model->update($data, $id);

        $this->redirect_to_controller($this->request->controller);
    }

    public function action_delete()
    {
        if(Request::$method == 'POST')
        {
            $id = $_POST['chk'];
            array_map('intval', $id);
        }else $id = intval($this->request->param('id'));

        if( !$id ) return;

	$this->_model->delete( $id );
        $this->redirect_to_controller($this->request->controller);
    }

    public function validData($data)
    {
        $keys = array('dateFrom', 'dateTo', 'status');

        $data = Arr::extract($data, $keys);

        $validator = Validate::factory($data)
                            ->rule('dateFrom', 'date')
                            ->rule('dateTo', 'date')
                            ->rule('status', 'range', array(-1, 1));
        if( $validator->check() )
        {
            return $data;
        }
        else
        {
            return false;
        }
    }
}
