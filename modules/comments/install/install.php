<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
            'name'             => 'Comments',
            'fs_name'          => 'comments',
            'panel_serialized' => NULL,
            'useDatabase'      => true
        );