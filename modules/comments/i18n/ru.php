<?php

defined('SYSPATH') or die('No direct script access.');

return array
    (
    'Comments' => 'Комментарии',
    'Comments Descr Short' => 'Описание комментов',
    'Comments Descr Full' => 'Подробное описание модуля',
    'Inactive' => 'Неактивный',
    'Comment' => 'Комментарий',
    'From ' => 'От',
    'To ' => 'До',
    'Status ' => 'Статус',
    'Rating ' => 'Рейтинг',
);

