<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    'moderate'    => false,
    'path'		=> dirname(__FILE__) . '/../views/', // Admin templates folder.

    'max_increment' => 4,

    'send_to' => array('to' =>'seyar@inbox.ru', 'cc' =>'tester@bestitsolutions.biz'),
    'send_from' => array( 'noreply@'.$_SERVER['REQUEST_URI'], $_SERVER['REQUEST_URI'] ),
    'send_subject' => 'Сообщение с сайта '.$_SERVER['REQUEST_URI'],
);
