<?php defined('SYSPATH') or die('No direct script access.');

Route::set
        (
	    'comments',
	    'admin/comments((/<id>)/<action>)(/<path>)',
	    array( 'id' => '\d{1,10}', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'comments',
		    'id'	    => NULL,
		    'action'        => 'index',
                )
	)
;

// Frontned Routes
Route::set
        (
	    'comments_front',
	    '(<lang>/)comments/<moduleId>/<objectId>(/<action>)',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_comments',
		    'action'        => 'view',
		    'moduleId'	    => 0,
		    'objectId'	    => 0,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;