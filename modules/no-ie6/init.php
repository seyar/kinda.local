<?php defined('SYSPATH') or die('No direct script access.');
if (!Arr::get($_COOKIE, 'skip_no_ie6') && $file = Kohana::find_file('vendor', 'Browscap'))
{
    require $file;
    $bc = new Browscap(dirname(__FILE__).'/cache/');
    $current_browser = $bc->getBrowser();
    if ($current_browser->Browser == 'IE' && $current_browser->MajorVer < 7)
    {
        require('views/noie6.php');
        die();
    }
    else
    {
        cookie::$expiration = 60*60*24*365;
        cookie::$path = '/';
        cookie::set('skip_no_ie6', TRUE);
    }
}