<!DOCTYPE html>
<html>
    <head>
        <title>Используемый Вами браузер устарел!</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo MEDIAWEBPATH;?>/noie6/noie6.css" />
        <script type="text/javascript"  src="/vendor/jquery/jquery.js" ></script>
        <script type="text/javascript" language="javascript" src="/vendor/jquery/jquery.cookie.js"></script>
        <script type="text/javascript" language="javascript">
            $(document).ready(function(){
                    $(".skip").click(function(){
                        $.cookie('skip_no_ie6', 1, {expiration: 60*60*24*365*1000, 'path': '/'});
                    });
                }
            );
        </script>
    </head>
    <body>
        <div id="old">
            <div id="header">
                <h1>Используемый Вами браузер устарел</h1>
                <font>По данным, предоставляемым сервисом <a href="http://www.liveinternet.ru/stat/ru/browsers.html?id=54;relgraph=yes">LiveInternet</a>, браузером Internet Explorer 6 пользуется менее 16% пользователей и их число постоянно уменьшается. В связи с этим на Ваш суд представляются более современные браузеры. Все они бесплатны, легко устанавливаются и просты в использовании. Если слова «браузер» и «Internet Explorer» для вас не знакомы, загрузите Internet Explorer 8</font>

            </div>

            <ul id="modern-browser">
                <li class="ie8">
                    <h2>Internet Explorer 8</h2>
                    <div class="descriptiontext">
						Современный браузер от компании Microsoft (разработчиков операционных систем семейства Windows). Бесплатно предоставляется всем желающим и свободен для распространения.
                        <ul class="file">
                            <li class="microsoft"><a href="http://www.microsoft.com/rus/windows/internet-explorer/">Internet Explorer 8, 16.2 Mb</a></li>
                            <li class="yandex"><a href="http://ie.yandex.ru/?kold">Internet Explorer 8 с сервисами yandex.ru, 17.8 Mb</a></li>
                            <li class="mail"><a href="http://ie8.mail.ru/">Internet Explorer 8 с сервисами mail.ru, 16.8 Mb</a></li>
                            <li class="rambler"><a href="http://ie.rambler.ru/">Internet Explorer 8 с сервисами rambler.ru, 17.0 Mb</a></li>
                        </ul>
                    </div>
                </li>

                <hr class="delim" />

                <li class="firefox">
                    <h2>FireFox 3.5</h2>
                    <div class="descriptiontext">
						Один из самых распространенных браузеров, благодаря мощной поддержке плагинов. Огромное число дополнений к браузеру на все случаи жизни и огромное число тем оформления для него вы можете найти на <a href="https://addons.mozilla.org/ru/firefox/">официальном сайте дополнений</a>. Разделы не стоят на месте и постоянно обновляются. Браузер может быть настроен под себя на любой вкус.
                        <ul class="file">
                            <li><a href="http://ru.www.mozilla.com/ru/">Mozilla FireFox 3.5.2, 8 Mb</a></li>
                        </ul>
                    </div>
                </li>

                <hr class="delim" />

                <li class="chrome">
                    <h2>Google Chrome</h2>
                    <div class="descriptiontext">
						Новый, но уже достаточно популярный браузер от гиганта поисковой индустрии, компании Google. Очень приятный и обладает многими особенностями и новшествами.
                        <ul class="file">
                            <li><a href="http://www.google.com/chrome">Google Chrome 556 Kb (установщик требует подключения к сети, будет загружена последняя стабильная версия. Во время установки будет загружено еще 9 MB)</a></li>
                            <li><a href="http://www.google.com/chrome/thankyou.html?standalone=1">Google Chrome 9.7 MB (установщик не требудет подключения. Вы можете использовать версию для установки на компьютерах, на которых отсутствует подключение к сети)</a></li>
                        </ul>
                    </div>
                </li>

                <hr class="delim" />

                <li class="opera">
                    <h2>Opera 9, 10</h2>
                    <div class="descriptiontext">
						Браузер Opera всегда позиционировался, как очень удобный и быстрый. Имеет внутренние утилиты для ускорения загрузки страниц, особенно актуально для пользователей с медленным интернетом. Хотя отлично подойдет и любым другим пользователям. Очень красивый и удобный.
                        <ul class="file">
                            <li class="v9"><a href="http://www.opera.com/">Opera 9, 8.5 Mb</a></li>
                            <li class="v10"><a href="http://www.opera.com/browser/next/">Opera 10 beta 2, 6.7 Mb</a></li>
                        </ul>
                    </div>
                </li>

                <hr class="delim" />

                <li class="skip">
                    <h2><a class="skip" href="/" >Я все равно хочу посмотреть на этот сайт</a></h2>
                    <div class="descriptiontext">
                        Сайт может отображатся неверно и часть функциональности может быть недоступна!
                    </div>
                </li>

            </ul>
        </div>

    </body>

</html>
