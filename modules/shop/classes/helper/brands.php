<?php
    class Helper_Brands {
        static public function init( $categryId = null )
        {
            if( $categryId === null )
            $categryId = Request::instance()->controller == 'frontend_goods' ? 0 : Request::instance()->param('id');
            $brands = new Model_Brands();
            Kohana_Controller_Quicky::$intermediate_vars['shop_brands'] =
                            $brands->findInCategoryF(intval($categryId));
        }
}
?>
