<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */

class Model_Paymethods_Nal extends Model_PayMethods
{
    private $_method = 'Nal';

    function add( $data )
    {
        list($insert_id, $total_rows) = DB::insert('paymethod_'.strtolower($this->_method),
            array (
//                    'cost',
//                    'percent',
//                    'plusToOrder',
//                    'invoice',
                    'ordersendmail',
                    'letter_description',
//                    'forShipment',
                    'postavshik',
                )
            )
        ->values(
                array (
//                    $data['cost'],
//                    $data['percent'],
//                    $data['plusToOrder'],
//                    $data['invoice'],
                    $data['ordersendmail'],
                    $data['letter_description'],
//                    implode(',',$data['forShipment']),
                    $data['postavshik'],
                )
            )
            ->execute()
        ;
        return $total_rows;
    }

    function save( $id, $data )
    {
        DB::update('paymethod_'.strtolower($this->_method))
            ->set(
                array (
//                    'cost' => $data['cost'],
//                    'percent' => $data['percent'],
//                    'plusToOrder' => $data['plusToOrder'],
//                    'invoice' => $data['invoice'],
                    'ordersendmail' => $data['ordersendmail'],
                    'letter_description' => $data['letter_description'],
//                    implode(',',$data['forShipment']),
                    'postavshik' => $data['postavshik'],
                )
            )
            ->where('id','=',$id)
            ->execute()
        ;
        return true;
    }

    function getItem( $where = NULL, $as_array = TRUE )
    {
        $query = DB::select()
            ->from('paymethod_'.strtolower($this->_method))
        ;

        if($where)
        {
            foreach($where as $key => $item)
                $query->where($key,'=', $item);
        }
        $result = $query->execute();
        if( $result->count() == 0 ) return array();
        else{
            if( $as_array )                return $result->as_array();
            else                           return $result->current();
        }

    }

    public function delete($id = NULL)
    {
        $query = DB::delete('paymethod_'.strtolower($this->_method));

        if (is_numeric($id))
	{
            $query->where('id', '=', $id); // ->limit(1)

	    $total_rows = $query->execute();
	}
    }

    public function delete_list($array)
    {
        $query = DB::delete('paymethod_'.strtolower($this->_method));

        if (count($array))
	{
            $query->where('id', 'IN', DB::expr('('.implode(',', $array).')') ); // ->limit(1)

	    $total_rows = $query->execute();
	}
        return true;
    }

    public function getCost($obj,$sum = 0,$weight = 0 )
    {
        return (float)$obj['cost'] + (float)$sum*(float)$obj['percent'] / 100;
    }
}

?>
