<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 05.08.2010 - 15:29:49 Exp $
 *
 * Project:     stream-of-time
 * File:        ukrpost.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_Shipmethods_Selfdelivery extends Model_ShipMethods
{
    public $method = 'Selfdelivery';
    public $sum = 0;
    public $weight = 0;

    public function show()
    {
        die('func show');
    }

    public function getItem($id)
    {        
        $parent = parent::getMainItem(array('id'=>$id, 'fs_name' => $this->method) );

        return $parent;
    }

    public function save($id, $lang_id = 1)
    {
        if ($post = $this->validate_save())
        {
                    $parent = parent::getMainItem(array('id'=>$id, 'fs_name' => $this->method) );

                    DB::update('shop_shipment_methods')
			    ->set(
				    array(
					'name' => $post['name'],
					'description' => $post['description'],
					'text' => $post['text'],
					'letter_description' => $post['letter_description'],
					'orderid' => $post['orderid'],
					)
				)
			    ->where('id', '=', $parent['id'])
			    ->execute()
                        ;

	    return $id;
        }else {
            return false;
        }
    }
//
//    public function validate_save()
//    {
//        $keys = array (
//                'cost' ,
//                'percent' ,
//            );
//
//	$params = Arr::extract($_POST, $keys, '');
//
//	$post = Validate::factory($params)
//
//                        ->rule('cost', 'not_empty')
////                        ->rule('cost', 'digit')
////                        ->rule('percent', 'not_empty')
////                        ->rule('percent', 'digit')
//            ;
//
//        if ($post->check())
//	{
//	    return $params;
//	}
//	else
//	{
//	    $this->errors = $post->errors('validate');
//	}
//    }

    public function addItem($lang_id = 1)
    {
        if ($post = $this->validate_save())
        {
            
            list($id, $total_rows) =
                            DB::insert('shop_shipment_methods', array('settings_id','fs_name','name','description','text','orderid') )
			    ->values(
				    array(
                                        1,
                                        $post['basic_fs_name'],
					$post['name'],
					$post['description'],
					$post['text'],
					$post['orderid'],
					)
				)
//			    ->where('id', '=', $id)
			    ->execute()
                        ;

            return $id;
        }else {
            return false;
        }
    }

    public function getCost($weight)
    {
//        $method = $this->getItem();
        return 0;
    }

    public function validate_save()
    {
        $keys = array (
                'cost' ,
                'percent' ,
                'name' ,
                'description' ,
                'text' ,
                'basic_fs_name',
                'letter_description',
                'orderid',
            );
	$params = Arr::extract($_POST, $keys, '');

	$post = Validate::factory($params)

//                        ->rule('cost', 'not_empty')
                        ->rule('name', 'not_empty')
//                        ->rule('percent', 'not_empty')
                        ->rule('basic_fs_name', 'not_empty')
//                        ->rule('percent', 'digit')
            ;

        if ($post->check())
	{
	    return $params;
	}
	else
	{
	    $this->errors = $post->errors('validate');
	}
    }
}
?>