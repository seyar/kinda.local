<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */
class Model_Shipmethods_Newpost extends Model_ShipMethods
{
    public $method = 'Newpost';
    public $sum = 0;
    public $weight = 0;

    public $border = 0;

    public function show()
    {
        die('func show');
    }

    public function getItem($id)
    {
        $parent = parent::getMainItem( array('id' => $id, 'fs_name' => $this->method) );

        $result =
            DB::select()
            ->from('shipmethod_'.strtolower($this->method))
            ->where('id','=',  $parent['settings_id'] )
            ->execute()
            ;

        if( $result->count() == 0 ) return $parent;
        else{
            $return = $result->current();
            $return['borders'] = unserialize($return['borders']);
            
            return array_merge($return, $parent);
        }
    }

    public function addItem($lang_id = 1)
    {
        if ($post = $this->validate_save())
        {
            $borders = array();
            if($post['border_sum'] && $post['border_value'])
            {
                foreach( $post['border_value'] as $key => $item )
                    $borders[$post['border_sum'][$key]] = preg_replace ('/[^\d][^\%]/', '', $post['border_value']);
            }
            list($id, $total_rows) =
                            DB::insert('shipmethod_'.strtolower($this->method), array('borders') )
			    ->values(
				    array(serialize($borders) )
				)
//			    ->where('id', '=', $id)
			    ->execute()
                        ;

            list($id, $total_rows) =
                            DB::insert('shop_shipment_methods', array('settings_id','fs_name','name','description','text', 'letter_description', 'orderid') )
			    ->values(
				    array(
                                        $id,
                                        $post['basic_fs_name'],
					$post['name'],
					$post['description'],
					$post['text'],
					$post['letter_description'],
					$post['orderid']
					)
				)
//			    ->where('id', '=', $id)
			    ->execute()
                        ;

            return $id;
        }else {
            return false;
        }
    }

    public function save($id, $lang_id = 1)
    {
        if ($post = $this->validate_save())
        {            
            $parent = parent::getMainItem(array('id'=>$id, 'fs_name' => $this->method) );
            $borders = array();
            if($post['border_sum'] && $post['border_value'])
            {
                foreach( $post['border_value'] as $key => $item )
                {
                    $item = str_replace(',','.',$item);
                    $item = preg_replace('/[^\d\.,%]/', '', $item);
                    if( $item )
                        $borders[$post['border_sum'][$key]] = $item;
                }
            }
            
            DB::update('shipmethod_'.strtolower($this->method))
                    ->set(
                            array(
                                'borders' => serialize($borders),
                                )
                        )
                    ->where('id', '=', $parent['settings_id'])
                    ->execute()
                ;
            
            DB::update('shop_shipment_methods')
                    ->set(
                            array(
                                'name' => $post['name'],
                                'description' => $post['description'],
                                'text' => $post['text'],
                                'letter_description' => $post['letter_description'],
                                'orderid' => $post['orderid'],
                                )
                        )
                    ->where('id', '=', $parent['id'])
                    ->execute()
                ;

	    return $id;
        }else {
            return false;
        }
    }

    public function validate_save()
    {
        $keys = array (
                'name' ,
                'description' ,
                'text' ,
                'orderid' ,
                'basic_fs_name',
                'letter_description',
                'border_sum',
                'border_value',
            );
	$params = Arr::extract($_POST, $keys, '');

	$post = Validate::factory($params)

                        ->rule('name', 'not_empty')
//                        ->rule('border_sum', 'digit')
//                        ->rule('border_value', 'regex', array("/^[0-9]+((\.|,)[0-9]{1,2})?$/"))
//                        ->rule('percent', 'not_empty')
                        ->rule('basic_fs_name', 'not_empty')
//                        ->rule('percent', 'digit')
            ;

        if ($post->check())
	{
	    return $params;
	}
	else
	{
	    $this->errors = $post->errors('validate');
	}
    }

    public function getCost($method_id)
    {
        if($method_id)
        {
            $method = $this->getItem($method_id);
            krsort($method['borders']);
            $border = array_walk($method['borders'], array('Model_Shipmethods_Newpost', 'foundborder'));
            
            return (float)$method['borders'][$this->border];
        }
    }

    function foundborder($item, $key = 0)
    {
        if( $this->sum < $key )
        {            
            $this->border = $key; //            return $key;
        }        
    }
}
?>
