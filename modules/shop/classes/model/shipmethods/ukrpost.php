<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 05.08.2010 - 15:29:49 Exp $
 *
 * Project:     stream-of-time
 * File:        ukrpost.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_Shipmethods_Ukrpost extends Model_ShipMethods
{
    public $method = 'Ukrpost';
    public $sum = 0;
    public $weight = 0;

    public function show()
    {
        die('func show');
    }

    public function getItem($id)
    {
        $parent = parent::getMainItem(array('id'=>$id, 'fs_name' => $this->method) );
        
        $result =
            DB::select()
            ->from('shipmethod_'.strtolower($this->method))
            ->where('id','=',  $parent['settings_id'])
            ->execute()
            ;
        
        if( $result->count() == 0 ) return array();
        else{
            $return = $result->current();            
            
            return array_merge($return, $parent);
        }
    }
    
    public function addItem($lang_id = 1)
    {
        if ($post = $this->validate_save())
        {
            list($id, $total_rows) =
                            DB::insert('shipmethod_'.strtolower($this->method), array('cost','percent') )
			    ->values(
				    array(
					number_format($post['cost'],2,'.',''),
					number_format($post['percent'],2,'.',''),
					)
				)
//			    ->where('id', '=', $id)
			    ->execute()
                        ;

            list($id, $total_rows) =
                            DB::insert('shop_shipment_methods', array('settings_id','fs_name','name','description','text', 'letter_description','orderid') )
			    ->values(
				    array(
                                        $id,
                                        $post['basic_fs_name'],
					$post['name'],
					$post['description'],
					$post['text'],
					$post['letter_description'],
					$post['orderid'],
					)
				)
//			    ->where('id', '=', $id)
			    ->execute()
                        ;

            return $id;
        }else {
            return false;
        }
    }

    public function save($id, $lang_id = 1)
    {
        if ($post = $this->validate_save())
        {            
                    $parent = parent::getMainItem(array('id'=>$id, 'fs_name' => $this->method) );
                    DB::update('shipmethod_'.strtolower($this->method))
			    ->set(
				    array(
					'cost' => number_format($post['cost'],2,'.',''),
					'percent' => number_format($post['percent'],2,'.',''),
					)
				)
			    ->where('id', '=', $parent['settings_id'])
			    ->execute()
                        ;
                    
                    DB::update('shop_shipment_methods')
			    ->set(
				    array(
					'name' => $post['name'],
					'description' => $post['description'],
					'text' => $post['text'],
					'letter_description' => $post['letter_description'],
					'orderid' => $post['orderid'],
					)
				)
			    ->where('id', '=', $parent['id'])
			    ->execute()
                        ;
                    
	    return $id;
        }else {
            return false;
        }
    }

    public function validate_save()
    {
        $keys = array (
                'cost' ,
                'percent' ,
                'name' ,
                'description' ,
                'text' ,
                'basic_fs_name',
                'letter_description',
                'orderid',
            );
	$params = Arr::extract($_POST, $keys, '');

	$post = Validate::factory($params)

                        ->rule('cost', 'not_empty')
                        ->rule('name', 'not_empty')
                        ->rule('percent', 'not_empty')
                        ->rule('basic_fs_name', 'not_empty')
//                        ->rule('percent', 'digit')
            ;

        if ($post->check())
	{
	    return $params;
	}
	else
        Messages::add($post->errors('validate'));
    }

    public function getCost($method_id = NULL)
    {
        if($method_id)
        {
            $method = $this->getItem($method_id);

            return (float)$method['cost']+$this->weight*(float)$method['percent'];
        }
    }
}
?>