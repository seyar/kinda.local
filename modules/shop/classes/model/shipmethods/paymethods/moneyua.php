<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */
class Model_Paymethods_moneyua extends Model_Paymethods
{
    private $_method = 'Moneyua';

    function getItem( $where = NULL, $as_array = TRUE )
    {
        $query = DB::select()
            ->from('paymethod_'.strtolower($this->_method))
        ;

        if($where)
        {
            foreach($where as $key => $item)
                $query->where($key,'=', $item);
        }
        $result = $query->execute();
        if( $result->count() == 0 ) return array();
        else{
            if( $as_array )                return $result->as_array();
            else{
                $return = $result->current();
                $return['fields'] = array(
                    'PAYMENT_AMOUNT' => '',
                    'PAYMENT_INFO' => '',
                    'PAYMENT_DELIVER' => '',
                    'PAYMENT_ORDER' => '',
                    'PAYMENT_TYPE' => $return['payment_type'],
                    'MERCHANT_INFO' => $return['merchant_info'],
                    'SECRETCODE' => $return['secretcode'],
                    'PAYMENT_ADDVALUE' => str_replace(' ', '_', $return['payment_addvalue']),
                    'PAYMENT_RULE' => $return['payment_rule'],
                    'PAYMENT_RETURNRES' => Kohana::config('shop')->PAYMENT_RETURNRES,
                    'PAYMENT_RETURN' => Kohana::config('shop')->PAYMENT_RETURN,
                    'PAYMENT_RETURNFAIL' => Kohana::config('shop')->PAYMENT_RETURNFAIL,
                    'PAYMENT_RETURNMET' => 1,
                    'PAYMENT_TESTMODE' => $return['payment_testmode'],
                    'PAYMENT_HASH' => '',
                );
                return $return;
            }
        }

    }

    public function delete($id = NULL)
    {
        $query = DB::delete('paymethod_'.strtolower($this->_method));

        if (is_numeric($id))
	{
            $query->where('id', '=', $id); // ->limit(1)

	    $total_rows = $query->execute();
	}
    }

    public function delete_list($array)
    {
        $query = DB::delete('paymethod_'.strtolower($this->_method));

        if (count($array))
	{
            $query->where('id', 'IN', DB::expr('('.implode(',', $array).')') ); // ->limit(1)

	    $total_rows = $query->execute();
	}
        return true;
    }

    public function getCost($obj,$sum = 0,$weight = 0 )
    {   
        return (float)$sum * (float)$obj['percent'] / 100;
    }

    function add( $data )
    {
        list($insert_id, $total_rows) = DB::insert('paymethod_'.strtolower($this->_method),
            array (
                    'merchant_info',
                    'payment_testmode',
                    'percent',
                    'payment_rule',
                    'secretcode',
                    'payment_addvalue',
                    'invoice',
                    'ordersendmail',
                    'letter_description',
                    'action_url',
                    'payment_type',
                    'postavshik',
                )
            )
        ->values(
                array (
                    $data['merchant_info'],
                    (isset($data['payment_testmode']) && !empty($data['payment_testmode']) ? 1 : 0 ),
                    $data['percent'],
                    (isset($data['plusToOrder']) && !empty($data['plusToOrder']) ? 1 : 2 ),
                    $data['secretcode'],
                    $data['payment_addvalue'],
                    $data['invoice'],
                    $data['ordersendmail'],
                    $data['letter_description'],
                    $data['action_url'],
                    $data['payment_type'],
                    $data['postavshik'],
                )
            )
            ->execute()
        ;
        return $insert_id;
    }

    function save( $id, $data )
    {        
        DB::update('paymethod_'.strtolower($this->_method))
            ->set(
                array (
                    'merchant_info' => $data['merchant_info'],
                    'payment_testmode' => (isset($data['payment_testmode']) && !empty($data['payment_testmode']) ? 1 : 0 ),
                    'percent' => $data['percent'],
                    'payment_rule' => (isset($data['payment_rule']) && !empty($data['payment_rule']) ? 1 : 2 ),
                    'invoice' => $data['invoice'],
                    'secretcode' => $data['secretcode'],
                    'payment_addvalue' => $data['payment_addvalue'],
                    'ordersendmail' => $data['ordersendmail'],
                    'letter_description' => $data['letter_description'],
                    'action_url' => $data['action_url'],
                    'payment_type' => $data['payment_type'],
                    'postavshik' => $data['postavshik'],
                )
            )
            ->where('id','=',$id)
            ->execute()
        ;
        return true;
    }
}
?>