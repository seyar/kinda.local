<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Brands extends Model {

    protected $_name = 'shop_brands';
    
    static public function getItems( $limit = NULL, $where = NULL )
    {
        $query = DB::select()
            ->from('shop_brands')
        ;
        
        if( isset($where) && count($where) > 0 )
        {
            foreach($where as $item)
            {
                $query->and_where($item[0],$item[1],$item[2]);
            }
        }
        
        if( isset($limit) ) $query->limit($limit);
        $result = $query->execute();
        
        if( $result->count() == 0 ) return array();
        else
            return $result->as_array('id');
    }

    public function find($id = null, $limit = null, $offset = 0){
        $q = DB::select()->from($this->_name);

        if( $id !== null )
        {
            if (is_numeric($id))
            {
                $q->where('id', '=', $id);
            }
            elseif(is_array($id))
            {
                $q->where('id', 'in', DB::expr('('.implode(",", $id).')'));
            }
        }

        if($limit !== null)
        {
            $q->limit($limit);
            if( $offset ) $q->offset($offset);
        }

        $res = $q->execute()->as_array('id');
        if( !is_numeric($id) ){
                 return $res;
        }

        foreach ($res as $val) {
            return $val;
        }
    }

    public function findAll($limit = null, $offset = 0)
    {
        return $this->find(null, $limit, $offset);
    }

    public function update($data, array $where)
    {
        DB::update($this->_name)
                ->set($data)
                ->where($where[0], $where[1], $where[2])
                ->execute();
    }

    public function insert($data)
    {
        list($lastInsertId, $insertedRows) = DB::insert($this->_name, array_keys($data))
                        ->values(array_values($data))
                        ->execute();
        return array('lastInsertId' => $lastInsertId, '' => $insertedRows);
    }

    public function delete($id)
    {
       $query = DB::delete($this->_name);

        if (is_numeric($id))
	{
            $query->where('id', '=', $id);
	    $total_rows = $query->execute();
	}
        elseif( is_array($id) )
        {
            $query->where('id', 'in', DB::expr('('.implode(",", $id).')')) ;
        }

        $affectedRows = $query->execute();
    }

    public function count()
    {
        return DB::select(DB::expr('count(*) as cnt'))
                    ->from($this->_name)
                    ->execute()
                    ->cnt;
    }

    public function findInCategory($categoryId)
    {        
        $sql = 'SELECT b.*
                    FROM '.$this->_name.' b
                    WHERE b.id in (
                        SELECT brand_id
                            FROM shop_goods sg
                            JOIN `shop_categories` AS sc ON sg.categories_id = sc.id
                            WHERE
                                brand_id <>0 and
                                sc.id IN (
                                        SELECT id
                                        FROM `shop_categories`
                                        WHERE `parents_serialized` LIKE \'%:"'.$categoryId.'"%\'
                                )
                            GROUP BY brand_id
                    )';
        
        $res = DB::query(Database::SELECT, $sql)
                    ->execute()
                    ->as_array();
        return $res;
    }


    
    public function findInCategoryF($categoryId)
    {
        $sql = 'SELECT b.*
                    FROM '.$this->_name.' b
                    WHERE b.id in (
                        SELECT brand_id
                            FROM shop_goods sg ';
        if($categoryId)
            $sql .=
                            'JOIN `shop_categories` AS sc ON sg.categories_id = sc.id
                            WHERE
                                brand_id <>0 and
                                sc.id IN (
                                        SELECT id
                                        FROM `shop_categories`
                                        WHERE `parents_serialized` LIKE \'%:"'.$categoryId.'"%\' or id='.$categoryId.'
                                )
                            GROUP BY brand_id';
         $sql .= ')';
        $res = DB::query(Database::SELECT, $sql)
                    ->execute()
                    ->as_array('id');
        return $res;
    }

    public function goods($brandId, $rows_per_page = 12)
    {
        $count = DB::select(DB::expr('COUNT(*) AS mycount'))
                        ->from(array('shop_goods', 'g'))
                        ->where('brand_id', '=', $brandId)
                        ->execute()
                        ->get('mycount');
                    ;
        $query = DB::select()
                    ->from('shop_goods')
                    ->where('brand_id', '=', $brandId)
                ;
        
        $pagination = Pagination::factory(array(
                        'total_items'    => $count,
                        'items_per_page' => $rows_per_page,
                ));

        $res = $query
                  ->limit($pagination->items_per_page)
                  ->offset($pagination->offset)
                  ->execute()
                  ->as_array();

        $page_links_str = $pagination->render('pagination/numbers_and_arrows');

        return array( $res, $page_links_str, $pagination->total_pages );
    }

    public function load($id = NULL)
    {
        if ((int) $id)
        {
            $query = DB::select()
                            ->from('shop_brands')
                            ->where('id', '=', $id)
                            ->limit(1);

            $result = $query->execute();

            if ($result->count() == 0)
                return array();
            else
            {

                $return = $result->current();
                return $return;
            }
        }
        else
        {
            return array();
        }
    }

}