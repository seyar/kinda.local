<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Frontend_Order extends Model
{
    static public function create(  $goods_list = array(),
                                    $shipment_method_id = NULL, $shipment_address_id = NULL,
                                    $payment_method_id = NULL, $payment_address_id = NULL,
                                    $total_goods_count = NULL, $total_goods_sum = NULL,
                                    $customer_id = NULL,
                                    $discount = 0,
                                    $addParam = array()
                                    
                                )
    {
        $goods_info = array();
        $total_sum = 0;
        $total_weight = 0;
        
        $user_data = Session::instance()->get( 'authorized' );

        if( $shipment_address_id == -1 && !$user_data)
        {
            $user_data = Model_Customers::load( Kohana::config('shop.quickorderUserId') );
            $shipment_address_id = $user_data['default_address_id'];
        }
        
        if( !empty($user_data) )
        {
            // reg exist user but not logged
            $user_data = Model_Customers::load( $customer_id );
            $shipment_address_id = $user_data['default_address_id'];            
        }
//        die(Kohana::debug($goods_list));
        if( !$goods_list )
            return false;
        
        $payment_method = Model_PayMethods::instance()->load( $payment_method_id );
        Model_Frontend_Currencies::$rate = $payment_method['rateMethod'];
        
        foreach( $goods_list as $key => $val )
        {
            $info = Model_Goods::load( $key );
            $goods_info[] = array(
                'id' => $info['id'],
                'scu' => $info['scu'],
                'name' => $info['name'] ? $info['name'] : $info['alter_name'],
                'count' => $val,
                'addParam' => isset($addParam[$info['id']]) ? $addParam[$info['id']] : '',
                'price' => $info['prices'][Kohana::config( 'shop' )->default_category][Kohana::config( 'shop' )->frontend_currency],
                'discount' => 0,
                'sum' => $val * $info['prices'][Kohana::config( 'shop' )->default_category][Kohana::config( 'shop' )->frontend_currency],
            );

            $total_sum += $val * $info['prices'][Kohana::config( 'shop' )->default_category][Kohana::config( 'shop' )->frontend_currency];
            $total_weight += $info['weight'];
        }
        
//        echo(Kohana::debug($goods_info,
//                $shipment_address_id ,$shipment_method_id ,
//                $payment_address_id , $payment_method_id ,
//                $total_goods_count ,$total_goods_sum ,
//                $customer_id ,number_format($total_sum,2,'.','' ), number_format($total_goods_sum,2,'.','')));
        
        if( $goods_info == array() ||
//                $shipment_address_id == NULL || $shipment_method_id == NULL ||
//                $payment_address_id == NULL || $payment_method_id == NULL ||
                $total_goods_count == NULL || $total_goods_sum == NULL ||
                $customer_id == NULL || number_format($total_sum,2,'.','' ) != number_format($total_goods_sum,2,'.','')
        )
        {
            return false;
        }
        $shipment_methods = Model_Shop::shipment_methods();
        
        $address_list = Model_Customers::shipment_address_list( $user_data['id'] );
        
        $shipment_name = $address_list[$shipment_address_id]['name'];
        $payment_name = $address_list[$payment_address_id]['name'];
//        $shipment_cost = str_replace( ',', '.', Controller_ShipMethods::getCost( $shipment_method_id, $total_weight, $total_sum ) );

//        unset(  $address_list[$shipment_address_id]['name'], $address_list[$payment_address_id]['name'],
//                $address_list[$shipment_address_id]['phone'], $address_list[$payment_address_id]['phone']
//            );
        unset( $address_list[$shipment_address_id]['name'], $address_list[$payment_address_id]['name'] );
        
        $customer_data = Model_Customers::load( $customer_id );
        DB::update( 'shop_customers' )->set( array( 'totalOrdersSum' => ($customer_data['totalOrdersSum'] + $total_goods_sum) ) )->where( 'id', '=', $customer_id )->execute();

        $total_goods_sum = $total_goods_sum - $discount;
//        $payment_cost = Controller_PayMethods::getCost( $payment_method_id, str_replace( ',', '.', $total_goods_sum ) );
        $sum = $total_goods_sum + $shipment_cost;
//        $sum += $payment_method['include_tax'] ? $tax : 0;
//        $sum += $payment_method['plusToOrder'] ? $payment_cost : 0;
//        $payment_cost = $payment_method['plusToOrder'] ? $payment_cost : 0;
        $sum = number_format( $sum, 2, '.', '' );

        $query = Db::insert( 'shop_orders' )
                        ->columns(
                                array(
                                    'customer_id',
                                    'shipment_method_id',
                                    'payment_method_id',
                                    'date_create',
                                    'currency_id',
                                    'subtotal',
                                    'discount',
                                    'shipment_cost',
                                    'payment_cost',
                                    'sum',
                                    'weight',
                                    'comments',
                                    'shipment_name',
                                    'shipment_address',
                                    'billing_name',
//                            'billing_address',
                                    'ser_details',
                                    'ser_status_history',
                                )
                        )
                        ->values(
                                array(
                                    $customer_id,
                                    1,//$shipment_method_id,
                                    1,//$payment_method_id,
                                    Db::expr( 'NOW()' ),
                                    Kohana::config( 'shop' )->frontend_currency,
                                    $total_goods_sum,
                                    str_replace( ',', '.', $discount ),
                                    0,//$shipment_cost,
                                    0,//str_replace( ',', '.', $payment_cost ),
                                    $sum,
                                    $total_weight,
                                    Arr::get($_POST, 'comment', ''),
                                    $shipment_name,
                                    implode( ', ', $address_list[$shipment_address_id] ),
                                    "$payment_name",
//                            implode(', ', $address_list[$payment_address_id] ),
                                    serialize( $goods_info ),
                                    serialize(
                                            array(
                                                array(
                                                    'date' => time(),
                                                    'status' => 0,
                                                    'alert' => 1,
                                                    'comment' => $_POST['comment'],
                                                )
                                            )
                                    ),
                                )
                        )
        ;
        list($order_id, $total_rows) = $query->execute();

        if( $order_id )
            return $order_id;

        return false;

    }

    static public function orders( $status_list = array( ) )
    {
        $user_data = Session::instance()->get( 'authorized' );
        $query = Db::select()
                        ->from( 'shop_orders' )
                        ->where( 'customer_id', '=', $user_data['id'] )
        ;

        if( $status_list )
        {
            $status_code = array_flip( Controller_Orders::$order_statuses );
            $query->where_open();
            foreach( $status_list as $status )
                $query->or_where( 'status', '=', $status_code[$status] );

            $query->where_close();
            
        }

        $result = $query->execute();

        if( $result->count() == 0 )
            return array( );
        else
        {
            $return = array( );

            foreach( $result->as_array( 'id' ) as $key => $value )
            {
                $value['ser_details'] = unserialize( $value['ser_details'] );
                $return[$key] = $value;
            }
            return $return;
        }
        
    }

    static public function load_address( $customer_id = NULL, $id = NULL )
    {
        if( $customer_id && (int) $id )
        {
            $query = DB::select()
                            ->from( 'shop_shipment_addresses' )
                            ->where( 'id', '=', $id )
                            ->and_where( 'customer_id', '=', $customer_id )
            ;

            $result = $query->execute();

            if( $result->count() == 0 )
                return array( );
            else
            {
                return $result->current();
            }
        }
    }

    public function save_address( $lang_id = 1, $id = NULL )
    {

        if( !$post = Model_Frontend_Order::validate_save() )
            return false;
        
        $user_data = Session::instance()->get( 'authorized' );
        if( (int) $id )
        { // update
            $id = (int) $id;
            $ins = array(
                'name' => $post['name'],
                'country' => $post['country'],
                'state' => $post['state'],
                'city' => $post['city'],
                'zip' => $post['zip'],
                'address_line1' => $post['address'],
                'address_line2' => $post['address_line2'],
                'phone' => $post['phone'],
            );

            if( $post['is_company'] == 1 )
            {
                $ins['is_company'] = $post['is_company'];
                $ins['company_name'] = $post['company_name'];
                $ins['company_bank'] = $post['company_bank'];
                $ins['company_mfo'] = $post['company_mfo'];
                $ins['company_okpo'] = $post['company_okpo'];
                $ins['company_phone'] = $post['company_phone'];
                $ins['company_fax'] = $post['company_fax'];
            }else
            {
                $ins['is_company'] = 0;
                $ins['company_name'] = '';
                $ins['company_bank'] = '';
                $ins['company_mfo'] = '';
                $ins['company_okpo'] = '';
                $ins['company_phone'] = '';
                $ins['company_fax'] = '';
            }

            DB::update( 'shop_shipment_addresses' )
                    ->set( $ins )
                    ->where( 'id', '=', $id )
                    ->and_where( 'customer_id', '=', $user_data['id'] )
                    ->execute()
            ;
        }else
        { // create
            $id = $total_rows = 0;
            $user_data = Session::instance()->get( 'authorized' );
            $keys = array(
                'name',
                'country',
                'state',
                'city',
                'zip',
                'address_line1',
                'address_line2',
                'phone',
                'customer_id',
            );
            $ins = array(
                $post['name'],
                $post['country'],
                $post['state'],
                $post['city'],
                $post['zip'],
                $post['address_line1'],
                $post['address_line2'],
                $post['phone'],
                $user_data['id'],
            );

            if( $post['is_company'] == 1 )
            {
                $keys[] = 'is_company';
                $keys[] = 'company_name';
                $keys[] = 'company_bank';
                $keys[] = 'company_mfo';
                $keys[] = 'company_okpo';
                $keys[] = 'company_phone';
                $keys[] = 'company_fax';

                $ins[] = $post['is_company'];
                $ins[] = $post['company_name'];
                $ins[] = $post['company_bank'];
                $ins[] = $post['company_mfo'];
                $ins[] = $post['company_okpo'];
                $ins[] = $post['company_phone'];
                $ins[] = $post['company_fax'];
            }

            list($id, $total_rows) = DB::insert( 'shop_shipment_addresses', $keys )
                            ->values( $ins )
                            ->execute();
        }
        return true;
    }

    static public function address_delete( $id = NULL )
    {
        $user_data = Session::instance()->get( 'authorized' );
        DB::delete( 'shop_shipment_addresses' )
                ->where( 'id', '=', $id )
                ->and_where( 'customer_id', '=', $user_data['id'] )
                ->execute()
        ;
    }

    public function validate_save()
    {

        $keys = array(
            'name',
            'phone',
            'country',
            'state',
            'city',
            'zip',
            'address',
            'address_line1',
            'address_line2',
            'is_company',
            'company_name',
            'company_bank',
            'company_mfo',
            'company_okpo',
            'company_phone',
            'company_fax',
        );

        $params = Arr::extract( $_POST, $keys, '' );

        $post = Validate::factory( $params )
                        ->rule( 'name', 'not_empty' )
                        ->rule( 'phone', 'not_empty' )
//                        ->rule('phone', 'phone', array(array(6,7,10,12,13)) )
                        ->rule( 'country', 'not_empty' )
                        ->rule( 'state', 'not_empty' )
                        ->rule( 'city', 'not_empty' )
//                        ->rule('zip', 'not_empty')
//                        ->rule( 'address_line1', 'not_empty' )
        ;

        if( $post->check() )
        {
            return $params;
        }else
        {
            Messages::add($post->errors( 'validate' ),'err');
        }
    }

    static function save_default()
    {
        $user_data = Session::instance()->get( 'authorized' );
        
        DB::update( 'shop_customers' )
                ->set( array(
                    'shipment_method_id' => $_POST['shipment'],
                    'default_address_id' => $_POST['address'],
                    'payment_method_id' => $_POST['payment'],
                    'news_subscribe' => $_POST['getnews'],
                ) )
                ->where( 'id', '=', $user_data['id'] )
                ->execute()
        ;
        return true;
    }

    static public function ordersendmail( $order, $siteTitle = '', $payment_method = NULL, $shipment_method = NULL, $email = NULL )
    {
        $user = Session::instance()->get( 'authorized' );
        $site = 'http://' . $_SERVER['HTTP_HOST'];
        $siteTitle = Kohana::config( 'shop' )->siteTitle;
        $order_id = $order['id'];

        $shipment_method['text'] = $shipment_method['letter_description'] == 1 ?
                '<p>' . str_replace('/static/media/', 'http://'.$_SERVER['HTTP_HOST'].'/static/media/', $shipment_method['text']) . '</p>' :
                '';
        $payment_method['text'] = $payment_method['letter_description'] == 1 ?
                '<p>' . str_replace('/static/media/', 'http://'.$_SERVER['HTTP_HOST'].'/static/media/',  $payment_method['text']) . '</p>' :
                '';
        
        $discount = $order['discount'];
        $otherComments = '';
        if( $order['customer_id'] == Kohana::config( 'shop.quickorderUserId') )
        {
            $otherComments = $order['comments'];
        }

        $to = array( 'to' => isset($email) ? $email : $user['email'] );
        
        if( $payment_method['ordersendmail'] )
            $to['bcc'] = array(Kohana::config( 'shop' )->send_to );
        if( Kohana::config( 'shop' )->sendEmailToDeveloper == 1 )
            $to['bcc'][] = Kohana::config( 'shop' )->EmailDeveloper;
        
        if( empty($user) && empty($to['to']) )
        {
            $to['to'] = $to['bcc'];
        }

        $from = array( Kohana::config( 'shop' )->send_from, 'Заказ №' . $order_id );
        $subject = I18n::get( Kohana::config( 'shop' )->subject ) . ' ' . $_SERVER['HTTP_HOST'];
        $message = include(dirname( __FILE__ ) . Kohana::config( 'shop' )->mail_template);

        if( empty($to) || empty($to['to']) ) return 1;
        if( !Email::send( $to, $from, $subject, $message, TRUE ) )
        {
            Kohana_Log::instance()->add( 'email send error. Model_Frontend_Cart->add item()', Kohana::debug( $to, $from, $subject, $message )
            );
        }
        return 1; //or use booleans here
    }

    function check_status()
    {
        $obj = 'Model_Frontend_Registration';
        $method = 'check_status';
        if( method_exists( $obj, $method ) )
        {
            call_user_func( array($obj, $method) );
        }
        return;
    }

}
