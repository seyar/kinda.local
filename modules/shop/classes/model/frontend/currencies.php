<?php

class Model_Frontend_Currencies extends Model
{

    static protected $_currencies = null;
    static protected $_defaultCurrency = null;
    static protected $_frontendCurrency = null;
    static public $rate = 'rate';

    public function __construct()
    {
        if( self::$_currencies === null )
        {
            self::$_currencies = DB::select()
                            ->from( 'shop_currencies' )
                            ->cached('120')
                            ->execute()
                            ->as_array( 'id' )
                    ;
            self::$_defaultCurrency = Kohana::config( 'shop' )->default_currency;
            self::$_frontendCurrency = Kohana::config( 'shop' )->frontend_currency;
        }
    }
    
    /**
     * Function return current currencies
     * 
     * @return array currencies 
     */
    function getCurrencies()
    {
        return self::$_currencies;
    }

    public function getDefaultCurrency()
    {
        return self::$_defaultCurrency;
    }

    public function getPrice( $prices, $key = 'price' )
    {
        $ret = array( );
        foreach( $prices as $price )
        {
            foreach( self::$_currencies as $currency )
            {
                if( $price['currencies_id'] !== $currency['id'] )
                {
                    $ret[$price['price_categories_id']][$currency['id']] = $currency[self::$rate] == 0 ? 0 : 
                            $price[$key] /
                            (
                                self::$_currencies[self::$_defaultCurrency][self::$rate] / $currency[self::$rate]
                            );
                }
                else
                    $ret[$price['price_categories_id']][$currency['id']] = $price[$key];
            }
        }
        
        if( Kohana::config('shop.priceRoundingBorder') ) return self::roundPrices($ret);
        return $ret;
    }
    
    /**
     * Function round prices which > than some value
     * 
     * @param array $data - input prices
     * @return array rounded prices 
     */
    static function roundPrices( $data = array() )
    {
        if( empty($data) ) return false;
        $return = array();
        foreach($data as $key => $item)
        {
            foreach($item as $kkey => $iitem)
            {
                // if front currency (grn)
                if( $kkey == self::$_frontendCurrency && $iitem > Kohana::config('shop.priceRoundingBorder') )
                    $return[$key][$kkey] = ceil($iitem);
                else
                    $return[$key][$kkey] = $iitem;
            }
        }
        return $return;
    }
    
    /**
     * Only for frontend
     * 
     * @param type $prices
     * @param type $key
     * @return type 
     */
    function getPriceManual( $prices, $key = 'price' )
    {
        return 'Задача перестала быть актуальной';
        $ret = array( );
        $oldprice = 0;
        $replacemtns = Kohana::config('shop.price_cat_replacement');
        
        foreach( $prices as $price )
            if( $price['price'] != 0) 
                $oldprice = $price['price'];

            
        foreach( $prices as $price )
        {
            foreach( self::$_currencies as $currency )
            {
                if( $price[$key] != 0 )
                    /* пересчет если цена не заполнена */
                    $ret[$price['price_categories_id']][$currency['id']] = $price[$key];
                else
                    $ret[$price['price_categories_id']][$currency['id']] = $oldprice / self::$_currencies[self::$_defaultCurrency]['rate'];
            }
        }
        
        $return = array();
        
        foreach($ret as $key => $item)
        {            
            if( array_search($key, $replacemtns) === FALSE )
            {
               $ret[$key][self::$_frontendCurrency] = isset($ret[$replacemtns[$key]][self::$_defaultCurrency]) ? $ret[$replacemtns[$key]][self::$_defaultCurrency] : 10000;
            }
        }
        return $ret;
    }

}

?>