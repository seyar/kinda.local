<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );

/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
class Model_Frontend_Recgoods extends Model
{

    static protected $_table = 'shop_recommended_goods';

    static function getItem( $goodId = NULL, $catId = NULL )
    {
        if( isset($goodId) && !empty($goodId) )
            $good = Model_Goods::load($goodId);
        
        $cat = Model_Categories::categories_load( isset($catId) && !empty($catId) ? $catId : $good['categories_id'] );
        if( !isset($cat) || count($cat) == 0 ) return array();
        if( empty($cat['parents_path']) || !isset($cat['parents_path']) ) return array();
        $parents = $cat['parents_path'];
        $parents = explode( ',', rtrim($parents,',') );
        $length  = count($parents)-1;
        
        $mysqlExpr = 'IF(catId='.$parents[$length].', 2,';
        $mysqlExpr .= isset($parents[$length-1]) && isset($parents[$length-2]) ? 'IF(catId = '.$parents[$length-1].','.$parents[$length-2].', ' : '';
        $mysqlExpr .= ' IF(catId = 1,1, 0))';
        $mysqlExpr .= isset($parents[$length-1]) && isset($parents[$length-2]) ? ')' : '';
        
        /*
         * IF(catId='.$parents[$length].', 2, IF(catId = '.$parents[$length-1].','.$parents[$length-2].', IF(catId = 1,1, 0)))
         */
        
        $result = DB::select( self::$_table . '.*', array( DB::expr($mysqlExpr), 'cat_order' ) )
                        ->from( self::$_table )
                        ->where( 'goodId', '=', $goodId )
                        ->or_where( 'catId', 'IN', DB::expr('('.rtrim($cat['parents_path'],',').')') )
                        ->cached( 60 )
                        ->order_by('cat_order','ASC')
                        ->limit(1)
                        ->execute()
        ;
        
        if( $result->count() == 0 )
            return array();
        else
        {
            $price_categoty = Kohana::config( 'shop' )->default_category;
            $curr = new Model_Frontend_Currencies();

            $resultArr = $result->current();            
            
            $resultArr = DB::select()
                ->from('shop_goods')
                ->where('id', 'IN', DB::expr('('.$resultArr['goodIds'].')') )
                ->and_where('quantity','>',0)
                ->execute()
                ;
            $resultArr = $resultArr->as_array('id');
            
            $randomize = Kohana::config( 'shop' )->recGoodsRandomize;

            foreach( $resultArr as $key => $val )
            {
                $query = Db::select( 'p.price_categories_id', 'p.currencies_id', 'p.price' )
                                ->from( array( 'shop_prices', 'p' ) )
                                ->where( 'p.good_id', '=', $val['id'] )
                                ->order_by( 'p.currencies_id' )
                                ->cached( 30 )
                ;

                if( is_numeric( $price_categoty ) )
                    $query->where( 'p.price_categories_id', '=', (int) $price_categoty );

                $prices = array( );                
                $rows = $query->execute()->as_array();

                $val['prices'] = $curr->getPrice( $rows );
                $val['seo_url'] = $val['seo_url'] ? str_replace( Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, urlencode($val['seo_url']) ) : str_replace( Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, Controller_Admin::to_url( $val['name'] ));
                $val['seo_url'] = str_replace( '__', '_', $val['seo_url'] );
                $val['seo_url'] = str_replace( '__', '_', $val['seo_url'] );
                $val['category_url'] = $val['category_url'] ? str_replace( Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, urlencode( $val['category_url'] ) ) : str_replace( Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, Controller_Admin::to_url( $val['category_name'] ));
                $val['category_url'] = str_replace( '__', '_', $val['category_url'] );
                $val['category_url'] = str_replace( '__', '_', $val['category_url'] );

                if( $randomize == 1 )
                    $return[$val['categories_id']][] = $val;
                else
                    $return[$val['id']] = $val;
            }

            if( $randomize != 1 )
                return $return;

            $res = array();
            if( is_array($return) && count($return) ):
            foreach( $return as $item )
            {
                if( count( $item ) > 1 )
                {
                    $key = array_rand( $item );
                    $res[] = $item[$key];
                }
                else
                    $res[] = $item[0];
            }
            endif;
            
            return $res;
        }
    }

}

?>