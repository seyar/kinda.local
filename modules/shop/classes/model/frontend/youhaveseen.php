<?php

defined('SYSPATH') OR die('No direct access allowed.');

/**
 * @version $Id: v 0.1 Jul 21, 2011 - 11:01:22 AM Exp $
 *
 * Project:     aristokrat
 * File:        youhaveseen.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://seyar.org.ua * 
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
class Model_Frontend_Youhaveseen
{
    private $_cookieKey = 'youhaveseen';
    
    private static $_instance;

	/**
	 * Get the singleton instance of this class and enable writing at shutdown.
	 *
	 *     $log = Kohana_Log::instance();
	 *
	 * @return  Kohana_Log
	 */
	public static function instance()
	{
		if (self::$_instance === NULL)
		{
			// Create a new instance
			self::$_instance = new self;
		}

		return self::$_instance;
	}
    
    static public function saveItem( $id = NULL )
    {
        if( !$id ) return false;
        $items = self::getCookieItems();
        array_push($items, $id );
        Cookie::set( self::instance()->_cookieKey, json_encode(array_unique($items)) );
    }
    
    static public function getCookieItems()
    {
        $cookieKey = self::instance()->_cookieKey;
        return (array)json_decode(Cookie::get( $cookieKey, '' ), TRUE);
    }
    
    static public function getGoods()
    {
        $ids = self::getCookieItems();
        if( is_array($ids) && count($ids) == 0 ) return array();
        
        $curr = new Model_Frontend_Currencies();
        $price_categories_id = 1;
        $query = Db::select( 'g.*', array( 'c.name', 'category_name' ), array( 'c.seo_url', 'category_url' ), array( 'p.price', 'price' ), array(DB::expr('IF(g.quantity > 0, 1, 0)'), 'exist') )
                            ->from( array( 'shop_goods', 'g' ) )
                            ->join( array( 'shop_categories', 'c' ) )
                            ->on( 'g.categories_id', '=', 'c.id' )
                            ->join( array( DB::expr( '(select price,good_id  from `shop_prices` where currencies_id = ' . $curr->getDefaultCurrency() . ' and price_categories_id = ' . $price_categories_id . ' group by good_id)' ), 'p' ), 'left outer' )
                            ->on( 'p.good_id', '=', 'g.id' )

                            ->where( 'p.price', '>', '0' )
                            ->order_by( 'exist', 'desc' )
        ;
        $query->and_where_open();
        foreach( Kohana::config('shop.frontendStatuses') as $key => $item )
        {
            $query->or_where( "g.status", '=', $item );
        }
        $query->and_where_close();

        if( is_array($ids) && count($ids) > 0)
        {
            $query->and_where( 'g.id', 'in', DB::expr( "(" . implode( ",", $ids ) . ")" ) );
        }
        
        $result = $query->cached(180)->execute();
        if( $result->count() == 0 )
            return array( );
        else
        {
            return $result->as_array( 'id' );
        }
    }
}

?>