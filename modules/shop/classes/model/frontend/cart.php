<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 26.04.2010 - 15:14:38 Exp $
 *
 * Project:     golden
 * File:        cart.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
class Model_Frontend_Cart extends Model
{
    static public function init()
    {
        $num = 0;
        $cart = self::get_cookie();
        foreach($cart as $q)
        {
            $num += $q;
        }
        Kohana_Controller_Quicky::$intermediate_vars['countCart'] = $num;
    }

    function add_item($id, $count=1)
    {
        $cart = self::get_cookie();
        $good = Model_Goods::load( $id );

        $cart[$id] = (int)$cart[$id] + $count;

        Cookie::set( 'cart', serialize($cart) );
        
        Cookie::set( 'cart_global_amount', Cookie::get('cart_global_amount')+$count );
        Cookie::set( 'cart_global_weight', Cookie::get('cart_global_weight')+$good['weight']*$count );

        Cookie::set( 'cart_global_sum', str_replace(',', '.',Cookie::get('cart_global_sum')+number_format($good['prices'][Kohana::config('shop')->get('default_category')][Kohana::config('shop')->get('frontend_currency')]*$count,2,'.','') ));

        return 1;
    }

    static function get_items()
    {
        if(! $cart = self::get_cookie() ) return array();
        $items = array();
        $total_sum = 0;
        $total_amount = 0;
        $total_weight = 0;
        foreach ($cart as $key => $item)
        {
            $res = Model_Frontend_Goods::info($key);
            $res['amount'] = $item;
            $total_sum += $res['amount'] * $res['prices'][Kohana::config('shop')->get('default_category')][Kohana::config('shop')->get('frontend_currency')];
            $total_amount += $res['amount'];
            $total_weight += $res['weight'];
            $items[$res['id']] = $res;
        }
        return array($items,$total_sum, $total_amount,$total_weight);
    }

    function delete_item($id)
    {
        $cart = self::get_cookie();

        unset( $cart[$id] );

        Cookie::set( 'cart', serialize($cart) );

        $total_sum = 0;
        $total_amount = 0;
        $items = $this->get_items();

        foreach($items['0'] as $item)
        {
            if($id != $item['id'])
            {
                $total_sum += $item['amount'] * $item['prices'][Kohana::config('shop')->get('default_category')][Kohana::config('shop')->get('frontend_currency')];
                $total_amount += $item['amount'];
                $total_weight += $item['weight'];
            }
        }

        Cookie::set( 'cart_global_amount', $total_amount );
        Cookie::set( 'cart_global_sum', str_replace(',', '.',$total_sum) );
        Cookie::set( 'cart_global_weight', str_replace(',', '.',$total_weight) );
        $total_amount = Helper_Num::decl($total_amount, array('товар', 'товара', 'товаров') );

        return array('total_sum' => $total_sum, 'count' => $total_amount );
    }

    function change_amount($id, $amount, $shipmentMethodId = NULL, $discount = 0)
    {
        $cart = self::get_cookie();

        $cart[$id] = $amount;

        Cookie::set( 'cart', serialize($cart) );
        $total_sum = 0;
        $total_amount = 0;
        $total_weight = 0;
        $items = $this->get_items();

        foreach($items['0'] as $item)
        {
            if($id != $item['id'])
            {
                $total_sum += $item['amount'] * $item['prices'][Kohana::config('shop')->get('default_category')][Kohana::config('shop')->get('frontend_currency')];
                $total_weight += $item['weight'] * $item['amount'];
                $total_amount += $item['amount'];
            }else{
                $total_amount += $amount;
                $total_weight += $item['weight']*$amount;
                $total_sum += $amount * $item['prices'][Kohana::config('shop')->get('default_category')][Kohana::config('shop')->get('frontend_currency')];
            }
        }

        $shipmentCost = Controller_ShipMethods::getCost( $shipmentMethodId, str_replace(',', '.', $total_weight), ($total_sum - $discount ));
        $good_sum = $amount * $items['0'][$id]['prices'][Kohana::config('shop')->get('default_category')][Kohana::config('shop')->get('frontend_currency')];

        $total_sum += $shipmentCost;

        Cookie::set( 'cart_global_amount', $total_amount );
        Cookie::set( 'cart_global_sum', str_replace(',', '.', $total_sum + $shipmentCost ));
        Cookie::set( 'cart_global_weight', str_replace(',', '.', $total_weight ));

        $total_amount = Helper_Num::decl($total_amount, array('товар', 'товара', 'товаров') );
        return array('errors' => 0, 'good_sum' => $good_sum, 'total_sum' => $total_sum, 'total_amount'=> $total_amount,'shipmentCost' => $shipmentCost );
    }

    static function get_cookie()
    {
        $cart = Cookie::get('cart');
        if( !$cart )
            $cart = array();
        else
            $cart = unserialize($cart);

        return $cart;
    }

    static function get_default_shipment(  )
    {
        if( $user_data = Session::instance()->get('authorized') )
        {
            $user = Model_Customers::load($user_data['id']);
            return $user['shipment_method_id'];
        }else{
            return Cookie::get( 'shipment');
        }
    }
    static function get_default_payment(  )
    {
        if( $user_data = Session::instance()->get('authorized') )
        {
            $user = Model_Customers::load($user_data['id']);
            return $user['payment_method_id'];
        }else{
            return Cookie::get( 'payment');
        }
    }

    function changeShipment( $shipmentMethodId = NULL, $cartGoods = array() )
    {
        if( !isset($cartGoods) || !isset($shipmentMethodId) ) return false;
        if( count($cartGoods) == 0 ) return false;
        if( Cookie::get('cart_active_payment') )
        {
            $oPaymethods = new Model_PayMethods();
            $paymentMethod = $oPaymethods->load( Cookie::get('cart_active_payment') );
            Model_Frontend_Currencies::$rate = $paymentMethod['rateMethod'];
        }


        foreach ($cartGoods as $key => $item)
        {
            $res = Model_Frontend_Goods::info($key);
            $res['amount'] = $item;
            $total_sum += $res['amount'] * $res['prices'][Kohana::config('shop')->get('default_category')][Kohana::config('shop')->get('frontend_currency')];
            $total_amount += $res['amount'];
            $total_weight += $res['weight'];
            $items[$res['id']] = $res;
        }

        $shipment_cost = Controller_ShipMethods::getCost( $shipmentMethodId, str_replace(',', '.', $total_weight), ($total_sum - $discount ));

        return array(
            'shipmentCost' => $shipment_cost,
            'total_sum' => $total_sum+$shipment_cost,
            'total_amount' => $total_amount,
            'total_weight' => $total_weight,
        );
    }

    function quickOrderSubmit()
    {
        if( !$post = $this->validateQuickOrder() ) return false;
        
        $shipment_address_id = $payment_address_id = -1;
        $user_id = Kohana::config('shop.quickorderUserId');
        
        if( $post['newUser'] == 1 ) // reg new user if need
        {
            $userData = Model_Registration::get_items( NULL, $post['email'], NULL, NULL, NULL );

            if( !isset($userData) || empty($userData) )
            { // user not exist
                $user_id = $post['id'] = Model_Frontend_Registration::save_item(FALSE); // add regular site user
                if( !$post['id'] )
                    return false;

                // add customer
                $data = array(
                    'name' => $post['name'],
                    'email' => $post['email'],
                    'date_create' => DB::expr('NOW()'),
                    'status' => 1,
                    'id' => $post['id'],
                    'price_category_id' => 1,
                );

                // Childs modules proccessing (Shop, ...etc)
                $keys = array_keys($data);
                DB::insert('shop_customers', $keys)->values($data)->execute();

                if( $post['city'] || $post['street'] || $post['house'] || $post['flat'] || $post['id'] )
                {
                    $post['address'] = $post['city'].'<br />'.$post['street'].'<br/>'.$post['house'].'<br/>'.$post['flat'].'<br/>'.$post['addInfo'];
                    $post['country'] = 'Ukraine';
                    $post['state'] = $post['address_add'] = $post['zip'] = '--';

                    $shipment_address_id = Model_Customers::addShipmentAddress($post);
                    Model_Customers::update(
                                array('default_address_id' => $shipment_address_id),
                                $post['id']
                            );
                }
            }
        }

        $_POST['comment'] = $post['comment'];
        if( $post['newUser'] != 1 || isset($userData) )
        {
            $post['address'] = $post['city'].'<br />'.$post['street'].'<br/>'.$post['house'].'<br/>'.$post['flat'].'<br/>'.$post['addInfo'];
            $post['country'] = 'Ukraine';
            $_POST['comment'] = $post['name'].' '.$post['phone'].' '.$post['email'].' '.$post['comment'].' '.$post['country'].' '.$post['address'];
        }

        // save new order
        if( !$res = Model_Frontend_Order::create(
                $post['amount'],
                $post['shipment'], // $shipment_method_id,
                $shipment_address_id,
                $post['payment'], // $payment_method_id,
                $payment_address_id,
                Cookie::get( 'cart_global_amount' ), // $total_goods_count,
                Cookie::get( 'cart_global_sum' ), // $total_goods_sum,
                $user_id,
                0, // $discount
                $post['addParam']
        ))
        {
            $this->errors = 'Извините, ваш заказ не прошел. попробуйте еще раз';
            return false;
        }

        $order = Model_orders::load($res);
        $shipment_methods = Model_Shop::shipment_methods();
        $payment_method = Model_PayMethods::instance()->load( $post['payment'] );
        $email = strstr($post['mobphone'],'@') ? $post['mobphone'] : NULL;
        Model_Frontend_Order::ordersendmail($order, '', $payment_method, $shipment_methods[$order['shipment_method_id']], $email);

        return $order;
    }

    function validateQuickOrder()
    {
        $keys = array(
            "amount",
            "name",
            "phone",
            "mobphone",
            "city",
            "street",
            "house",
            "flat",
            "addInfo",
            "email",
            "newUser",
            "news_subscribe",
            "payment",
            "shipment",
            "comment",
            "addParam",
        );

        $params = Arr::extract($_POST, $keys, '');

        $post = Validate::factory($params)
//                        ->rule('email', 'not_empty')
                        ->rule('name', 'not_empty')
//                        ->rule('phone', 'not_empty')
                        ->rule('mobphone', 'not_empty')
//                        ->rule('phone', 'phone', array(array(6,7,10,12,13)) )
//                        ->rule('payment', 'not_empty')
//                        ->rule('shipment', 'not_empty')
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {            
            Messages::add( $post->errors('validate') );
//            $this->errors = $post->errors('validate');
        }
    }
}
?>
