<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Frontend_Goods extends Model
{

    static public function images($id = NULL)
    {
        
    }

    static public function properties($id = NULL)
    {
        
    }

    static public function review($id = NULL)
    {
        
    }

    static public function info($id = NULL, $price_categoty = NULL)
    {
        $query = Db::select('g.*'
                        , array('c.name', 'category_name')
                        , array('c.seo_url', 'category_url')
                )
                ->from(array('shop_goods', 'g'))
                ->join(array('shop_categories', 'c'))
                ->on('g.categories_id', '=', 'c.id')
                ->where('g.id', '=', $id)
                ->and_where_open()
                ->where('c.status', '=', 'verified')
                ->or_where('c.id', '=', Kohana::config('shop.archiveID'))
                ->and_where_close()
                ->limit(1)
        ;
        $query->and_where_open();
        foreach ( Kohana::config('shop.frontendStatuses') as $key => $item )
        {
            $query->or_where("g.status", '=', $item);
        }
        $query->or_where('g.status', '=', 'out_of_store');
        $query->or_where('g.status', '=', 'archive');
        $query->and_where_close();

        //Kohana_log::instance()->add('d', print_r($query->__toString(),1) );
        $result = $query->cached(60)->execute();

        if( $result->count() == 0 )
        {
            echo Request::factory('error/404')->execute();
            die();
        }
        else
        {
            $return = array();

            $return = $result->current();
            if( $return['rating'] > 0 )
                $return['rating'] = intval($return['stars'] / $return['rating']);

            $query = Db::select('p.price_categories_id', 'p.currencies_id', 'p.price', 'p.oldprice')
                    ->from(array('shop_prices', 'p'))
                    ->where('p.good_id', '=', $return['id'])
                    ->order_by('p.currencies_id')
            ;

            if( is_numeric($price_categoty) )
                $query->where('p.price_categories_id', '=', (int) $price_categoty);

            $prices = array();
            $rows = $query->cached(60)->execute()->as_array();

            $curr = new Model_Frontend_Currencies();
            $return['prices'] = $curr->getPrice($rows);
            $return['oldprices'] = $curr->getPrice($rows,'oldprice');

            $return['seo_url'] = $return['seo_url'] ? str_replace(Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, urlencode($return['seo_url'])) : str_replace(Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, Controller_Admin::to_url($return['name']));
            $return['seo_url'] = str_replace('__', '_', $return['seo_url']);
            $return['seo_url'] = str_replace('__', '_', $return['seo_url']);
            $return['category_url'] = $return['category_url'] ? str_replace(Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, urlencode($return['category_url'])) : str_replace(Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, Controller_Admin::to_url($return['category_name']));
            $return['category_url'] = str_replace('__', '_', $return['category_url']);
            $return['category_url'] = str_replace('__', '_', $return['category_url']);

            $add_fields = DB::select('ser_addFields')
                    ->from('shop_categories')
                    ->where('id', '=', $return['categories_id'])
                    ->execute()
                    ->current()
            ;

            $return['ser_addFields'] = unserialize($add_fields['ser_addFields']);
            // если из груп то не эксплодим. в противном| случае эксплод с влож-ю 2.
            $category_info = Model_Categories::categories_load($return['categories_id']);   
            
            $return['add_fields'] = DB::query(Database::SELECT, "SELECT `f`.`id` AS `f_id`, `f`.`alias` AS `f_alias`, `f`.`is_group` AS `f_group`, `f`.`name`, `f`.`type`, `f`.`desc`, `f`.`defaults`, v.* "
                            . " FROM `shop_goods_properties` AS `f` "
                            . " LEFT OUTER JOIN "
                            . "(SELECT * FROM `shop_goods_properties_values` AS v1 WHERE v1.good_id = {$id} ) "
                            . " AS `v` ON (`f`.`id`=`v`.`properties_category_id`) "
                            . " WHERE f.category_id = {$return['categories_id']} OR f.category_id = {$category_info['parent_id']}"
                            . " ORDER BY f.category_id DESC, order_id"
                    )
                    ->cached(60)
            ;
            //Kohana_log::instance()->add('d', print_r($return['add_fields']->__toString(),1));
            $return['add_fields'] = $return['add_fields']->execute()
                    ->as_array()
            ;
            
//            die(Kohana::debug( $return['add_fields'] ));
            foreach ( $return['add_fields'] as $key => $value )
            {
                if( $value['f_group'] == '1' )
                {
                    foreach ( $return['add_fields'] as $ikey => $item )
                    {
                        if( !$item['f_group'] && strstr($item['f_alias'], $value['f_alias']) )
                        {
                            $return['add_fields'][$key]['childs'][$ikey] = $item;
                            unset($return['add_fields'][$ikey]);
                        }
                    }
                }
            }
            
            $return['groups'] = self::getGroupInfo($id);

            return $return;
        }
    }

    static function getPriceCategories($arr = array(), $price_category = NULL)
    {
        $query = Db::select('p.price_categories_id', 'p.currencies_id', 'p.price', 'p.good_id')
                ->from(array('shop_prices', 'p'))
                ->where('p.good_id', 'IN', DB::expr('(' . implode(',', $arr) . ')'))
                ->order_by('p.currencies_id')
                ->cached(120)
        ;
        if( is_numeric($price_category) )
            $query->where('p.price_categories_id', '=', (int) $price_category);

        $return = array();
        $rows = $query->execute()->as_array();
        foreach ( $rows as $item )
        {
            $return[$item['good_id']][] = $item;
        }
        return $return;
    }

    static public function thisCategoryOtherGoods($goodId, $catid, $price_category = NULL, $limit = NULL, $rand = FALSE)
    {
        $query = Db::select('g.*'
                        , array('c.name', 'category_name')
                        , array('c.seo_url', 'category_url')
                )
                ->from(array('shop_goods', 'g'))
                ->join(array('shop_categories', 'c'))
                ->on('g.categories_id', '=', 'c.id')
                ->where('c.status', '=', 'verified')
                ->and_where('c.id', '=', $catid)
                ->and_where('g.id', '<>', $goodId)
                ->order_by('id', 'desc')
        ;
        $query->and_where_open();
        foreach ( Kohana::config('shop.frontendStatuses') as $key => $item )
        {
            $query->or_where("g.status", '=', $item);
        }
        $query->and_where_close();

//        if( $limit )
//                    $query->limit($limit);

        $result = $query->cached(30)->execute();

        if( $result->count() == 0 )
            return array();
        else
        {
            $return = array();

            $curr = new Model_Frontend_Currencies();
            $tmpkeys = $result->as_array('id');

            $rows = self::getPriceCategories(array_keys($tmpkeys));

            foreach ( $tmpkeys as $key => $val )
            {
                $ids[] = $key;

//                if (is_numeric($price_categoty))
//                    $query->where('p.price_categories_id', '=', (int)$price_categoty);

                $prices = array();
                $val['prices'] = $curr->getPrice($rows[$key]);
                $val['seo_url'] = $val['seo_url'] ? $val['seo_url'] : Controller_Admin::to_url($val['name']);
                $val['category_url'] = $val['category_url'] ? $val['category_url'] : Controller_Admin::to_url($val['category_name']);
                $return[$key] = $val;
            }

            if( $rand && $limit && $result->count() > $limit )
            {
                $ids = array_rand($return, $limit);
                foreach ( $ids as $item )
                    $return2[] = $return[$item];

                $return = $return2;
            }

            return $return;
        }
    }

    static public function vote($goodId, $stars, $userId)
    {
        if( self::isVoted($goodId, $userId) )
            throw new Exception('You have already voted');

        DB::update('shop_goods')
                ->set(
                        array(
                            'rating' => DB::expr("rating+1"),
                            'stars' => DB::expr("stars+" . $stars),
                        )
                )
                ->where('id', '=', $goodId)
                ->execute();
        self::setVoted($goodId, $userId);
    }

    static public function isVoted($goodId, $userId)
    {
        $res = DB::select()
                ->from('shop_goods_votes')
                ->where('good_id', '=', $goodId)
                ->and_where('user_id', '=', $userId)
                ->execute()
                ->as_array('id')
        ;
        return!empty($res);
    }

    static public function setVoted($goodId, $userId)
    {
        DB::insert('shop_goods_votes', array('good_id', 'user_id'))
                ->values(array($goodId, $userId))
                ->execute();
    }

    static function getKeywords($id = NULL, $catid = NULL, $type = 'keywords')
    {
        if( !isset($id) && !isset($catid) )
            return '';
        $return = $type == 'keywords' ? Kohana::config('shopkeywords.keywordSource') : Kohana::config('shopkeywords.descriptionSource');

        // get good
        $goodInfo = DB::select()
                ->from('shop_goods')
                ->where('id', '=', $id)
                ->cached(120)
                ->limit(1)
                ->execute()
                ->current()
        ;
        if( count($goodInfo) == 0 && isset($id) )
            return '';
        $catid = isset($id) ? $goodInfo['categories_id'] : $catid;
        //get cat
        $catInfo = DB::select()
                ->from('shop_categories')
                ->where('id', '=', $catid)
                ->cached(120)
                ->limit(1)
                ->execute()
                ->current()
        ;

        $goodname = isset($goodInfo['name']) && !empty($goodInfo['name']) ? $goodInfo['name'] : $goodInfo['alter_name'];
        $catname = isset($catInfo['page_title']) && !empty($catInfo['page_title']) ? $catInfo['page_title'] : $catInfo['name'];

//        $configKeys = Kohana::config('shopkeywords.keywords');
//        $keywords = array();
//        foreach($configKeys as $k => $value)
//            $keywords[strtolower($k)] = $value;
//        $goodname = str_replace( array_keys($keywords),$keywords, strtolower($goodname) );
//        $catname = str_replace( array_keys($keywords),$keywords, strtolower($catInfo['name']) );

        $search = array('%category%', '%good%');
        $replace = array($catname, $goodname);

        $return = ucwords(str_replace($search, $replace, $return));

        return $return;
    }

    static function getApproximateDate($storeDate = NULL)
    {
        $return = i18n::get('Expected') . ' ';
        $dateTexts = Kohana::config('shop.stpreApproximateText');
        $seconds = 60 * 60 * 24;
        $dateDiff = (int) (strtotime($storeDate, time()) / $seconds) - (int) (time() / $seconds) + 1;

        if( $dateDiff < 0 || !$storeDate )
            return $return . 'доставка';

        $dateText = '';
        foreach ( $dateTexts as $key => $item )
        {
            if( $dateDiff == $key )
                $dateText .= i18n::get($item);
        }

        if( !empty($dateText) )
            return $return . $dateText;
        return $return . date('d.m.Y', strtotime($storeDate));
    }

    static public function getGroupInfo($id = NULL)
    {
        if( !$id )
            return array();

        $query = DB::select('goodId')
                ->from('shop_goodgroups')
                ->where('mainGoodId', '=', $id)
                ->execute()
        ;
        if( $query->count() == 0 )
            return array();

        $goods = $query->as_array(NULL, 'goodId');

        $goods = DB::select('id', 'scu', 'format')
                ->from('shop_goods')
                ->where('id', 'IN', $goods)
                ->execute()
        ;
        if( $goods->count() == 0 )
            return array();
        return $goods->as_array('id');
    }

    static public function loadGood($id)
    {
        if( !$id )
            return array();

        $goods = DB::select('id', 'good_id', 'price')
                ->from('shop_prices')
                ->where('good_id', '=', $id)
                ->execute()
        ;
        if( $goods->count() == 0 )
            return array();
        return $goods->current('good_id', 'price');
    }

}