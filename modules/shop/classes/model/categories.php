<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Categories extends Model_Admin
{

    static public $children = array();
    static public $cats = array();

    public function catinfo($id)
    {
        
    }


    public function categories_list($languages_id = NULL)
    {
        if (is_numeric($languages_id))
        {
			
            @$rows_per_page = ((int) $_POST['rows_num']) ? ((int) $_POST['rows_num']) : (int) cookie::get('rows_per_page_' . MODULE_NAME . '_' . MODULE_SUBCONTROLLER, 10);

            cookie::set('rows_per_page_' . MODULE_NAME . '_' . MODULE_SUBCONTROLLER, $rows_per_page);
            $_POST['rows_num'] = $rows_per_page;

            if (@$_POST['search_cat_name'])
            {
                $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
                                ->from('shop_categories')
                                ->where('name', 'LIKE', "%{$_POST['search_cat_name']}%");

                $query = DB::select('*')
                                ->from('shop_categories')
                                ->where('name', 'LIKE', "%{$_POST['search_cat_name']}%");
            }
            else
            {
                // Set and save current Category
                $cat_id = (isset($_GET['shop_cat_id']) ? (int) $_GET['shop_cat_id'] : (int) Session::instance()->get('shop_cat_id', Kohana::config('shop')->root_category_id) );

                // get current Category info
                $current_info = DB::select('id', 'parent_id', 'parents_serialized')
                                ->from('shop_categories')
                                ->where('id', '=', $cat_id)->execute()->current();

                Session::instance()->set('shop_cat_id', $cat_id);
                $_POST['shop_cat_id'] = $cat_id;
                Session::instance()->set('shop_parent_id', $current_info['parent_id']);
                $_POST['shop_parent_id'] = $current_info['parent_id'];

                $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
                                ->from('shop_categories')
                                ->where('languages_id', '=', $languages_id);

                $query = DB::select('*')
                                ->from('shop_categories')
                                ->where('languages_id', '=', $languages_id);

                $query_c->and_where('parent_id', '=', $cat_id);
                $query->and_where('parent_id', '=', $cat_id);
            }

            $count = $query_c
                            ->execute()
                            ->get('mycount');

            if (isset($_POST['page']))
                $_GET['page'] = $_POST['page'];

            $pagination = Pagination::factory(array(
                        'total_items' => $count,
                        'items_per_page' => $rows_per_page,
                    ));

            $result = $query
                            ->order_by('name', 'ASC')
                            ->limit($pagination->items_per_page)
                            ->offset($pagination->offset)
                            ->execute();

            $page_links_str = '';
            if ($result->count() == 0)
                return array(array(), '', array());
            else
            {
                $result = $result->as_array();

//                foreach ($result as $key=>$value)
//                {
//                    $result[$key]['is_category'] = TRUE;
//                }
                return array($result, $page_links_str, $pagination->total_pages);
            }
        }
    }

    static public function categories_load($id = NULL)
    {
        if ((int) $id)
        {
            $query = DB::select()
                            ->from('shop_categories')
                            ->limit(1)
                    ;

            $query->where('id', '=', $id);

            $result = $query->execute();
            if ($result->count() == 0)
                return array();
            else
            {
                $return = $result->current();

                $query = DB::select()
                                ->from('shop_goods_properties')
                                ->where('category_id', '=', $id)
                                ->order_by('order_id');
                //->order_by('alias');

                $return['ser_addFields'] = $query->execute()->as_array();

                return $return;
            }
        }
        else
        {
            return array('parent_id' => Session::instance()->get('shop_cat_id'));
        }
    }

    static function categories_folders($languages_id = NULL, $add_to_depth = 0)
    {

        function find_childrens($param_id, &$param_rows, $depth=0, $parent_name = '')
        {
            $p_return = array();

            foreach ($param_rows as $p_k => $p_v)
            {
                if ($p_v['parent_id'] == $param_id)
                {
                    $p_v['depth'] = $depth; // save object depth
                    $p_v['last_parse'] = $p_v['last_parse'];
//                    $p_v['name'] = str_repeat('&nbsp;', $depth) . $p_v['name'];
                    $p_v['name'] = (($parent_name) ? '&nbsp;&nbsp;&nbsp;' . $parent_name .' &raquo; ':''). $p_v['name'] ;
                    $p_return[] = $p_v;

                    $p_current_key = count($p_return) - 1;
                    foreach (find_childrens($p_v['id'], $param_rows, $depth + 1, $p_v['name']) AS $value)
                    {
                        $p_return[] = $value;
                    }
                }
            }

            return $p_return;
        }

// function

        $return = array(Kohana::config('shop')->root_category_id => array('name' => I18n::get('Root Category'), 'id' => Kohana::config('shop')->root_category_id));

        //die(Kohana::debug($return));

        $query = DB::select('id', 'name', 'parent_id', 'last_parse')
                        ->from('shop_categories')
                        ->where('languages_id', '=', $languages_id)
                        ->order_by('name')
                        ->cached(10);

        $result = $query->execute(); //->cached(10)
        //die(Kohana::debug($result));

        if ($result->count() == 0)
            return $return;
        else
        {
            $rows = $result->as_array();

            $rows = find_childrens(Kohana::config('shop')->root_category_id, $rows, $add_to_depth);

            foreach ($rows as $key => $value)
            {
                $value['name'] = $value['depth'] == 1 ? "{$value['name']}" : "<b>{$value['name']}</b>";
                $return[$value['id']] = array('name' => $value['name']
                    , 'id' => $value['id']
                    , 'last_parse' => $value['last_parse']
                    , 'depth' => $value['depth']
                );
            }

            return $return;
        }
    }

// ToDo: rebuild single category parents by "category_parents_rebuild"
    public function categories_save($lang, $id = NULL)
    {
        if ($post = Model_Categories::page_validate())
        {
            if ((int) $id)
            { // update
                $id = (int) $id;


                // Load current page info
                $result = DB::select()->from('shop_categories')
                                ->limit(1)
                                ->where('id', '=', $id)
                                ->execute()
                                ->current()
                ;
                DB::update('shop_categories')
                        ->set(
                                array(
                                    'name' => $post['name'],
                                    'page_title' => $post['page_title'] ? $post['page_title'] : $post['name'],
                                    'seo_url' => ($post['seo_url'] ? $post['seo_url'] : Controller_Admin::to_url($post['name'])),
                                    'seo_keywords' => $post['seo_keywords'],
                                    'seo_description' => $post['seo_description'],
                                    'description' => $post['full_description'],
                                    'parent_id' => $post['parent_id'],
                                    'order_id' => $post['order_id'],
                                    'status' => $post['status'],
                                )
                        )
                        ->where('id', '=', $id)
                        ->execute()
                ;
            }
            else
            { // create
                $id = 0;
                
                list($id, $total_rows) = DB::insert('shop_categories',
                                        array(
                                            'languages_id',
                                            'name',
                                            'page_title',
                                            'seo_url',
                                            'seo_keywords',
                                            'seo_description',
                                            'description',
                                            'parent_id',
                                            'order_id',
                                            'status'
                                        )
                                )
                                ->values(
                                        array(
                                            $lang,
                                            $post['name'],
                                            ($post['page_title'] ? $post['page_title'] : $post['name']),
                                            ($post['seo_url'] ? $post['seo_url'] : Controller_Admin::to_url($post['name'])),
                                            $post['seo_keywords'],
                                            $post['seo_description'],
                                            $post['full_description'],
                                            $post['parent_id'],
                                            $post['order_id'],
                                            $post['status'],
                                        )
                                )
                                ->execute();

                // new parents_path from new id
                $q1 = DB::select('parents_path')->from('shop_categories')
                        ->where('id', '=', $post['parent_id'])
                        ->execute();

                $q1 = $q1->as_array();

                $new_parent = $q1[0]['parents_path'].$id.',';

                DB::update('shop_categories')
                    ->set(array('parents_path' => $new_parent))
                    ->where('id', '=', $id)
                    ->execute();

//                /die( Kohana::debug($new_parent));
                
            }

//            Model_Categories::save_add_filelds($lang, $id, $post);
//            if ($post['copyParamsFrom'] != 1)
//            {
//                Model_Categories::copyParamsFrom($post['copyParamsFrom'], $id, $post);
//            }
            // save rec goods
            $addGoodsModel = Kohana::config('shop')->addGoodsModel;
            $addGoodsMethod = Kohana::config('shop')->addGoodsMethSave;
            if (isset($addGoodsModel) && !empty($addGoodsModel))
                call_user_func(array($addGoodsModel, $addGoodsMethod), $id, $post);
            
            $this->saveImage($id);
            
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function deleteCatImg($id)
    {
        @unlink(MEDIAPATH.'shop/categories/thumb_'.$id.'.jpg');
        return 1;
    }
    
    function saveImage($id)
    {
        if( !$_FILES['catImage']['name'] ) return false;
        $path = MEDIAPATH.'shop/categories/';
        if( !file_exists($path) ) mkdir( $path, 0755, true );
        $destination = MEDIAPATH.'shop/categories/thumb_'.$id.'.jpg';
        move_uploaded_file($_FILES['catImage']['tmp_name'], $destination);
        
//        parent::updateItem( array('ima'), $where);
    }

    public function save_add_filelds($lang = NULL, $id = NULL, $post = array())
    {
        if (!$lang OR !$id)
            return false;
// Зачем в эту функцию передавать $post ???
        // Clean Up inactive Additional Fileds for current type_id

        $query = DB::select()
                        ->from('shop_goods_properties')
                        ->where('category_id', '=', $id)
                        ->order_by('alias');

        $result = $query->execute();

        if ($result->count() != 0)
        {
            foreach ($result->as_array() as $row)
            {
                if (!$_POST['addFld_name'][$row['id']])
                {
                    $query = DB::delete('shop_goods_properties')
                                    ->where('category_id', '=', $id)
                                    ->and_where('id', '=', $row['id']);

                    $query->execute();
                }
                else
                {
                    $query = DB::update('shop_goods_properties')
                                    ->set(
                                            array(
                                                'alias' => $_POST['addFld_alias'][$row['id']],
                                                'name' => $_POST['addFld_name'][$row['id']],
                                                'type' => $_POST['addFld_type'][$row['id']],
                                                'desc' => $_POST['addFld_desc'][$row['id']],
                                                'defaults' => $_POST['addFld_defaults'][$row['id']],
                                                'is_searchable' => $_POST['addFld_searchable'][$row['id']],
                                                'is_filterable' => $_POST['addFld_filterable'][$row['id']],
                                                'is_group' => $_POST['addFld_group'][$row['id']],
                                            )
                                    )
                                    ->where('category_id', '=', $id)
                                    ->and_where('id', '=', $row['id']);

                    $query->execute();
                }
            }
        }

        // Add new Additional Fileds for current type_id

        if ($_POST['newAddFields'])
        {
            foreach ($_POST['newAddFields'] as $key => $value)
            {
                $query = DB::insert('shop_goods_properties',
                                        array(
                                            'category_id',
                                            'alias',
                                            'name',
                                            'type',
                                            'desc',
                                            'defaults',
                                            'is_searchable',
                                            'is_filterable',
                                            'is_group',
                                        )
                                )
                                ->values(
                                        array(
                                            $id,
                                            $value['alias'],
                                            $value['name'],
                                            $value['type'],
                                            $value['desc'],
                                            $value['defaults'],
                                            $value['searchable'],
                                            $value['filterable'],
                                            $value['group'],
                                        )
                                )
                ;

                $query->execute();
            }
        }

        // Update

        $query = DB::select()
                        ->from('shop_goods_properties')
                        ->where('category_id', '=', $id);

        $result = $query->execute()->as_array();

        $ser_addFields = array();

        foreach ($result as $value)
        {
            $ser_addFields[$value['id']] = array(
                'alias' => $value['name'],
                'name' => $value['name'],
                'type' => $value['type'],
                'desc' => $value['desc'],
                'defaults' => $value['defaults'],
                'is_searchable' => $value['is_searchable'],
                'is_filterable' => $value['is_filterable'],
                'is_group' => $value['is_group'],
            );
        }

        $query = DB::update('shop_categories')
                        ->set(
                                array(
                                    'ser_addFields' => serialize($ser_addFields),
                                )
                        )
                        ->where('id', '=', $id)
        ;

        $query->execute();
    }

    public function page_validate()
    {
        $keys = array(
            'name',
            'page_title',
            'seo_url',
            'seo_keywords',
            'seo_description',
            'full_description',
            'parent_id',
            'status',
            'order_id',
            'ser_addFields',
            'copyParamsFrom',
            'overwriteParams',
            'recgoodid',
            'recgoodname',
        );

        $addFields = $newAddFields = array();

//        if (count($_POST['newAddFldName']))
//        {
//            foreach ($_POST['newAddFldName'] as $key => $value)
//            {
//                $newAddFields[$key] = array(
//                    'name' => $value,
//                    'alias' => $_POST['newAddFldAlias'][$key],
//                    'type' => $_POST['newAddFldType'][$key],
//                    'desc' => $_POST['newAddFldDesc'][$key],
//                    'defaults' => $_POST['newAddFldDefaults'][$key],
//                    'searchable' => isset($_POST['newAddFldSearchable'][$key]),
//                    'filterable' => isset($_POST['newAddFldFilterable'][$key]),
//                    'group' => $_POST['newAddFldGroup'][$key],
//                );
//                $addFields[] = $newAddFields[$key];
//            }
//            $_POST['newAddFields'] = $newAddFields;
//        }

        $_POST['ser_addFields'] = serialize($addFields);

        $params = Arr::extract($_POST, $keys, '');

        $post = Validate::factory($params)
                        ->rule('name', 'not_empty')
                        ->rule('parent_id', 'not_empty')
                        ->rule('parent_id', 'digit')
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            $this->errors = $post->errors('validate');
        }
    }

    static public function categories_delete($id = NULL)
    {
        //$query = DB::delete('shop_categories');

        $q = DB::select('id')->from('shop_categories')
                        ->where('', Db::expr('FIND_IN_SET('), Db::expr(intval($id) . ", parents_path)"))
                        ->execute()
                        ->as_array()
                ;
        
        DB::delete('shop_categories')
                ->where('id', '=', $id)
                ->execute()
                ;

        if (count($q))
        {
            foreach($q as $key => $value)
            {
                DB::delete('shop_categories')
                        ->where('id', '=', $value['id'])
                        ->execute()
                        ;
            }
        }
        return TRUE;
    }

    public function categories_delete_list(array $array)
    {
        $query = DB::delete('shop_categories');

        if (count($array))
        {
            $query->where('id', 'in', array_flip($array));

            $total_rows = $query->execute();
        }
    }

    public function categories_add_fields_types()
    {
        $return = $enum_type = array();
        $query = DB::query(Database::SELECT, "SHOW COLUMNS FROM shop_goods_properties LIKE 'type'");

        $result = $query->execute(); //->cached(10)

        if ($result->count() == 0)
            return $enum_type;
        else
        {
            $columns = $result->current();
            preg_match_all("|'([^']+)'|i", $columns['Type'], $enum_type);

            foreach ($enum_type[1] as $value)
            {
                $return[$value] = I18n::get(ucfirst($value));
            }
            return $return;
        }
    }

    public function copy_add_fields($fromCategoryId, $toCategoryId)
    {                
        DB::query(Database::INSERT, 
                "INSERT INTO shop_goods_properties
             (category_id, alias, name, type, `desc`, defaults, is_searchable, is_group, is_filterable, order_id)"
                ." SELECT $toCategoryId, alias, name, type, `desc`, defaults, is_searchable, is_group, is_filterable, order_id " 
                ." FROM shop_goods_properties WHERE category_id = $fromCategoryId")
                ->execute();             
   }
    public function categories_verify($id = NULL)
    {
        if (is_numeric($id))
        {
            $result = DB::query(Database::SELECT, "SHOW COLUMNS FROM shop_categories LIKE 'status%'")
                            ->execute()
                            ->current();

            $options = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $result['Type']));

            $current = DB::select('status')->from('shop_categories')->where('id', '=', $id)
                            ->execute()
                            ->current();
            $current_status = $current['status'];
            $status = '';

            while (list($key, $val) = each($options))
            {
                if ($current_status == $val)
                {
                    $status = current($options);

                    if ($status === false)
                    {
                        reset($options);

                        $status = current($options);
                    }

                    break;
                }
            }

            DB::update('shop_categories')->set(array('status' => $status))
                    ->where('id', '=', $id)
                    ->execute();
        }
        return true;
    }

    static function category_parents_rebuild($languages_id, $category_id = NULL)
    {

        function update_parents_path($branch_array = array())
        {
            foreach ($branch_array as $value)
            {

                if (isset($value['childs']) and count($value['childs']) > 0)
                {
                    update_parents_path($value['childs'][0]);
                }

                $query = Db::update('shop_categories')
                                ->set(array('parents_serialized' => serialize($value['parents_info'])))
                                ->where('id', '=', $value['id'])
                                ->execute();
            }
        }

        function find_childrens($param_id, &$param_rows, $parents_info_array=array())
        {
            $p_return = array();

            foreach ($param_rows as $p_k => $p_v)
            {
                if ($p_v['parent_id'] == $param_id)
                {
                    $parents_info_array[] = array('id' => $p_v['id'], 'name' => $p_v['name'], 'seo_url' => $p_v['seo_url']); // save object depth
                    $p_return[$param_id][$p_v['id']] = $p_v;

//		    $p_current_key = count($p_return)-1;

                    foreach (find_childrens($p_v['id'], $param_rows, $parents_info_array) AS $value)
                    {
                        $p_return[$param_id][$p_v['id']]['childs'][] = $value;
                    }

                    $p_return[$param_id][$p_v['id']]['parents_info'] = $parents_info_array;
                    array_pop($parents_info_array);
                }
            }

            return $p_return;
        }

// function

        $query = DB::select('id', 'name', 'parent_id','seo_url')
                        ->from('shop_categories')
                        ->where('languages_id', '=', $languages_id)
                        ->order_by('parent_id', 'ASC')
                        ->order_by('name', 'ASC')
                        ->cached(10);

        $result = $query->execute(); //->cached(10)

        if ($result->count() == 0)
            return false;
        else
        {
            $rows = $result->as_array('id');

            $rows_2 = find_childrens(Kohana::config('shop')->root_category_id, $rows,
//                                        array('id'=>Kohana::config('shop')->root_category_id, 'name'=>I18n::get('Root Category'))
                            array()
            );

            update_parents_path($rows_2[Kohana::config('shop')->root_category_id]);


            return true;
        }
    }

    static function copyParamsFrom($fromId, $toId, $post)
    {
        $result = DB::select()
                        ->from('shop_goods_properties')
                        ->where('category_id', '=', $fromId)
                        ->order_by('alias')
                        ->execute()
        ;

        $newParams = $result->as_array('alias');
        
        $result = DB::select()
                        ->from('shop_goods_properties')
                        ->where('category_id', '=', $toId)
                        ->order_by('alias')
                        ->execute()
        ;

        $currentParams = $result->as_array('alias');

        $insertquery = DB::insert('shop_goods_properties', array(
                    'category_id',
                    'alias',
                    'name',
                    'type',
                    'desc',
                    'defaults',
                    'is_searchable',
                    'is_filterable',
                    'is_group',
                ))
        ;

        foreach ($newParams as $key => $item)
        {
            $alias = $item['alias'];
            $name = $item['name'];
            if ($post['overwriteParams'] == 1)
            {
                DB::delete('shop_goods_properties')
                        ->where('category_id', '=', $toId)
                        ->and_where('alias', '=', $item['alias'])
                        ->and_where('type', '=', $item['type'])
                        ->execute()
                ;
            }
            else
            {
                $alias .= $currentParams[$key]['alias'] == $item['alias'] ? '_copy' : ''; // если такой уже есть
                $name .= $currentParams[$key]['name'] == $item['name'] ? '_copy' : ''; // если такой уже есть
            }


            $insertquery->values(array(
                $toId,
                $alias,
                $name,
                $item['type'],
                $item['desc'],
                $item['defaults'],
                $item['is_searchable'],
                $item['is_filterable'],
                $item['is_group'],
            ));
        }
        $insertquery->execute();

        return 1;
    }

    /**
     * function find child categories
     * 
     * @param int       languages_id = 1
     * @param int       parentId = cat_id or NULL
     * @param array     cats
     * @return = array(45,55,66,77) independ child or sub child
     */
    static function findChildrenRecurse($languages_id = NULL, $parentId = NULL)
    {
        $parentId = (isset($parentId) && !empty($parentId)) ? $parentId : Kohana::config('shop')->root_category_id;
        $query = DB::select('id')
                        ->from('shop_categories')
                        ->where('', Db::expr('FIND_IN_SET('), Db::expr(mysql_escape_string($parentId) . ", parents_path)"))
                        ->execute()
        ;

        if ($query->count() == 0)
            return array();

        return array_keys($query->as_array('id'));
    }

    /**
     * function return children if it has
     *
     * @param   int       $parentId
     * @param   array     $data
     * @return  false     on no child
     * @return  array
     */
    static function recurse($parentId, $data = array())
    {
        $return = array();
        foreach ($data as $item)
        {
            if ($parentId == $item['parent_id'])
                $return[$item['id']] = $item;
        }


        if (count($return) == 0)
            return false;
        else
            return $return;
    }

    /**
     * return all cats from cur ID
     *
     * @param INT $lang_id
     * @param INT $parentId
     * @return array(id => array(id,name,..))
     */
    static function getAllCats($lang_id = NULL, $parentId = NULL)
    {
        $query = DB::select('id', 'name', 'parent_id', 'last_parse')
                        ->from('shop_categories')
                        ->where('languages_id', '=', $lang_id)
                        ->where('parent_id', '=', $parentId)
                        ->order_by('id')
                        ->cached(10)
        ;

        $result = $query->execute(); //->cached(10)
        if ($result->count() == 0)
            return array();
        return $result->as_array('id');
        die(Kohana::debug($result));
    }

    static function getAllGoods()
    {
        $query = DB::select('shop_categories.id','shop_categories.name','shop_categories.parents_path','shop_categories.parent_id', array(DB::expr('count(shop_goods.id)'), 'count'))
                ->from('shop_categories')
                ->join('shop_goods', 'LEFT OUTER')->on('shop_categories.id','=','shop_goods.categories_id')
                ->group_by('shop_categories.id')
                ->order_by('order_id')
                //->cached(180)
                //этот кэш не нужен иначе когда редактируешь название категории,она появиться только через 3 минуты
                ->execute()
                ;
        
        $return = array();
        foreach( $query->as_array() as $key => $value )
        {
            $value['depth'] = substr_count( $value['parents_path'], ',' );
            $return[$value['parent_id']][] = $value;
        }
        
        return $return;
    }

    static function SortInUpdate()
    {

        $mas = $_POST['serialize']; //это из поста строка с переменными который отсортировыны юзером
        $mas = '&' . $mas; //это разбиваю на переменные
        $rez = explode("&id[]=", $mas);
        foreach ($rez as $key => $value)
        {
            // это обновление всех типов категорий по порядку как нужно
            if ($key == 0)
                continue;
            DB::update('shop_goods_properties')
                    ->set(array('order_id' => $key,))
                    ->where('id', '=', $value)
                    ->execute();
        }
        die('1');
    }

    static function edit_options_update()
    {
        $item = $_GET['old_name_group'];
        $id_cat = $_GET['id_cat'];
        $id = $_GET['id'];
        $vars_edit = $_GET['mas'];
        if ($id)
        {
            $id = substr($id, 3);

            if (count($vars_edit) == 3)
            {
                DB::update('shop_goods_properties')
                        ->set(array(
                            'alias' => $vars_edit[0],
                            'name' => $vars_edit[1],
                            'desc' => $vars_edit[2],
                        ))
                        ->where('id', '=', $id)
                        ->execute();
                $group_name_up =
                                DB::select()->from('shop_goods_properties')
                                ->where('category_id', '=', $id_cat)
                                ->where('is_group', '=', null)
                                ->where('alias', 'LIKE', "%$item%")
                                ->execute()->as_array();

                $lenght_old_name = strlen($item);
                $count = 1;
                foreach ($group_name_up as $key => $value)
                {
                    $v = substr($value['alias'], 0, $lenght_old_name);
                    if ($v == $item)
                    {
                        $str1 = str_replace($item, $vars_edit[0], $value['alias'], $count);
                        DB::update('shop_goods_properties')
                                ->set(array('alias' => $str1))
                                ->where('id', '=', $value['id'])
                                ->execute();
                    }
                }
                die(print(json_encode(array('1', '1'))));
            }
            else
            {

                DB::update('shop_goods_properties')
                        ->set(array(
                            'alias' => $vars_edit[0],
                            'name' => $vars_edit[1],
                            'type' => $vars_edit[2],
                            'desc' => $vars_edit[3],
                            'defaults' => $vars_edit[4]
                        ))
                        ->where('id', '=', $id)
                        ->execute();

                die(print(json_encode(array('1', '1'))));
            }
        }
        else
        {
            if (count($vars_edit) == 3)
            {

                $new_id = DB::insert('shop_goods_properties', array(
                                    'category_id', 'alias', 'name', 'desc', 'is_group'))
                                ->values(array(
                                    $_GET['id_cat'],
                                    $vars_edit[0],
                                    $vars_edit[1],
                                    $vars_edit[2],
                                    '1'
                                ))
                                ->execute();
                $name_gr = $vars_edit[0];

                $sorted_group = DB::select(DB::expr("max(order_id)"))->from('shop_goods_properties')
                                ->where('is_group', '=', '1')
                                ->where('category_id', '=', $id_cat)
                                ->execute()
                                ->as_array();
                // выбираем максимальный order_id для групп

                if ($sorted_group[0]['max(order_id)'] === null)
                {

                    $query1 = DB::select('id', 'order_id')->from('shop_goods_properties')
                                    ->where('category_id', '=', $id_cat)
                                    ->execute()
                                    ->as_array();

                    foreach ($query1 as $key => $value)
                    {
                        DB::update('shop_goods_properties')
                                ->set(array('order_id' => $value['order_id'] + 1))
                                ->where('id', '=', $value['id'])
                                ->execute();
                    }

                    $str = $name_gr;

                    $query2 = DB::select('id', 'order_id', 'alias')->from('shop_goods_properties')
                                    ->where('category_id', '=', $id_cat)
                                    ->where('is_group', '=', null)
                                    //->where('alias', 'LIKE', "%$str%")
                                    ->execute()
                                    ->as_array();

                    $max_order_id = 2;

                    $len_group = strlen($str);

                    foreach ($query2 as $key => $value)
                    {
                        $v = substr($value['alias'], 0, $len_group);
                        if ($v == $str)
                        {
                            DB::update('shop_goods_properties')
                                    ->set(array('order_id' => $max_order_id))
                                    ->where('id', '=', $value['id'])
                                    ->execute();
                            $max_order_id++;
                        }
                    }

                    foreach ($query2 as $key => $value)
                    {
                        $v = substr($value['alias'], 0, $len_group);
                        if ($v <> $str)
                        {
                            DB::update('shop_goods_properties')
                                    ->set(array('order_id' => $max_order_id))
                                    ->where('id', '=', $value['id'])
                                    ->execute();
                            $max_order_id++;
                        }
                    }


                    die(print(json_encode(array('rebut'))));
                }

                $step2 = DB::select('alias')->from('shop_goods_properties')
                                ->where('category_id', '=', $id_cat)
                                ->where('order_id', '=', $sorted_group[0]['max(order_id)'])
                                ->execute()
                                ->as_array();
                // имя последней группе

                $str = $step2[0]['alias'];

                // имя последней группы

                $step3 = DB::select(DB::expr("max(order_id)"))->from('shop_goods_properties')
                                //->where('is_group', '=', 'null')
                                ->where('category_id', '=', $id_cat)
                                ->where('alias', 'LIKE', "%$str%")
                                ->execute()
                                ->as_array();
                //последнее поле в найденной категории

                $step4 = DB::select()->from('shop_goods_properties')
                                ->where('category_id', '=', $id_cat)
                                ->where('order_id', '>', $step3[0]['max(order_id)'])
                                //->order_by('order_id')
                                ->execute()
                                ->as_array();
                //выбираем все что после последнего элемента в группе

                $step5 = DB::select()->from('shop_goods_properties')
                                ->where('category_id', '=', $id_cat)
                                ->where('id', '=', $new_id[0])
                                ->execute()
                                ->as_array();

                // новая группа

                array_unshift($step4, $step5[0]);
                if (isset($step4[1]['order_id']))
                {
                    $max_order_id = $step4[1]['order_id'];
                }
                else
                {
                    $max_order_id = DB::select(DB::expr("max(order_id)"))->from('shop_goods_properties')
                                    ->where('category_id', '=', $id_cat)
                                    ->execute()
                                    ->as_array();

                    $max_order_id = $max_order_id[0]['max(order_id)'];
                    $max_order_id++;
                }

                $len_group = strlen($step4[0]['alias']);

                foreach ($step4 as $key => $value)
                {
                    $v = substr($value['alias'], 0, $len_group);
                    if ($v == $step4[0]['alias'])
                    {
                        DB::update('shop_goods_properties')
                                ->set(array('order_id' => $max_order_id))
                                ->where('id', '=', $value['id'])
                                ->execute();
                        $max_order_id++;
                    }
                }

                foreach ($step4 as $key => $value)
                {
                    $v = substr($value['alias'], 0, $len_group);
                    if ($v <> $step4[0]['alias'])
                    {
                        DB::update('shop_goods_properties')
                                ->set(array('order_id' => $max_order_id))
                                ->where('id', '=', $value['id'])
                                ->execute();
                        $max_order_id++;
                    }
                }

                die(print(json_encode(array('rebut'))));
            }
            else
            {

                $new_id = DB::insert('shop_goods_properties', array(
                                    'category_id', 'alias', 'name', 'type', 'desc', 'defaults'))
                                ->values(array(
                                    $_GET['id_cat'],
                                    $vars_edit[0],
                                    $vars_edit[1],
                                    $vars_edit[2],
                                    $vars_edit[3],
                                    $vars_edit[4]
                                ))
                                ->execute();
                die(print(json_encode($new_id)));
            }
        }
    }

    static function options_delete()
    {
        $id = $_POST['id'];
        $id = substr($id, 3);
        DB::delete('shop_goods_properties')
                ->where('id', '=', $id)
                ->execute();
        die('1');
    }

    static function is_searchable_update()
    {
        $id = $_POST['id'];
        $id = substr($id, 3);
        DB::update('shop_goods_properties')
                ->set(array('is_searchable' => DB::expr('! is_searchable')))
                ->where('id', '=', $id)
                ->execute();
        die('1');
    }

    static function is_filterable_update()
    {
        $id = $_POST['id'];
        $id = substr($id, 3);
        DB::update('shop_goods_properties')
                ->set(array('is_filterable' => DB::expr('! is_filterable')))
                ->where('id', '=', $id)
                ->execute();
        die('1');
    }

    static function edit_options_delete()
    {
        $mas = $_GET['del_list'];
        $rez = explode("id_", $mas);

        foreach ($rez as $key => $value)
        {
            DB::delete('shop_goods_properties')
                    ->where('id', '=', $value)
                    ->execute();
        }
        die(print(json_encode(array('1', '1'))));
    }
    
    public function getBrokenid()
    {
        $str = array();
        $result = DB::select('id', 'parent_id')
                ->from('shop_categories')
                ->execute()
                ->as_array('id')
                ;
        
        foreach( $result as $key => $item )
        {
            if( !isset($result[$item['parent_id']]) ) $str[] = $item['id'];
        }
        
        return implode(',',$str);
    }
    
    
}