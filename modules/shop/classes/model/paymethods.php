<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_PayMethods extends Model
{

    // Singleton static instance
    private static $_instance;

    public function setMethod($class)
    {
        $class = "Model_Paymethods_$class";        
        return new $class;
    }

    public static function instance()
    {
        if (self::$_instance === NULL)
        {
            // Create a new instance
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    public function show()
    {
        $post = $_POST;
        if (!isset($_POST['rows_num']))
        {
            $post['rows_num'] = 0;
        }
        $rows_per_page = ((int) $post['rows_num']) ? ((int) $post['rows_num']) : (int) cookie::get('rows_per_page', 10);

        if (isset($_POST['rows_per_page']))
        {
            $rows_per_page = $_POST['rows_per_page'];
        }

        cookie::set('rows_per_page', $rows_per_page);
        $_POST['rows_num'] = $rows_per_page;

//
        $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
                        ->from('shop_payment_methods')
        ;


        $count = $query_c
                        ->execute()
                        ->get('mycount');
        
        $pagination = Pagination::factory(array(
                    'total_items' => $count,
                    'items_per_page' => $rows_per_page,
                ));

        $query = DB::select('*')
                        ->from('shop_payment_methods')
        ;

        $result = $query
                        ->order_by('name', 'ASC')
                        ->limit($pagination->items_per_page)
                        ->offset($pagination->offset)
                        ->execute();

        $result = $result->as_array();


        return array($result, $pagination, $result);
    }

    public function delete($id = NULL)
    {
        $query = DB::delete('shop_payment_methods');

        if (is_numeric($id))
        {
            $query->where('id', '=', $id); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function delete_list($array = array(), $fs_name = NULL)
    {
        //die(Kohana::debug($array));
        $query = DB::delete('shop_payment_methods');

        if (count($array))
        {
            $query->where('id', 'in', array_flip($array)); // ->limit(1)

            $ids = self::getMainItem($array);
            $total_rows = $query->execute();

            $newids = array();
            foreach ($ids as $item)
                $newids[] = $item['settings_id'];

            //$this->setMethod($fs_name)->delete_list($newids);
        }
    }

    public function load($id = NULL)
    {
        if( (int)$id )
        {
            $query = DB::select()
                            ->from('shop_payment_methods')
                            ->where('id', '=', $id)
                            ->limit(1)
                    ;

            $result = $query->execute();

            //die(Kohana::debug($result));
            if ($result->count() == 0) return array();
            else
            {
                $return = $result->current();                
                $return2 = $this->setMethod($return['fs_name'])->getItem( array('id' => $return['settings_id']), FALSE );
                $return['forShipment'] = explode(',', $return['forShipment']);

                return array_merge($return, $return2);
            }
        }
        else
        {
            return array();
        }
    }

    public function add($lang_id = 1)
    {
        if ($post = $this->validate_save())
        {
            $paytype = DB::select('type')->from('shop_payment_methods')->where('fs_name', '=', $post['fs_name'])->limit(1)->execute()->get('type');

            list($id, $total_rows) = DB::insert('shop_payment_methods',
                                    array(
                                        'name', 'fs_name', 'text',  'forShipment', 'include_tax', 'type', 'orderid','rateMethod'
                                    )
                            )
                            ->values(
                                    array(
                                        $post['name'],
                                        $post['fs_name'],
                                        $post['text'],
//                                        $post['description'],
                                        implode(',', $post['forShipment']),
                                        $post['include_tax'],
                                        $paytype,
                                        $post['orderid'],
                                        $post['rateMethod'],
                                    )
                            )
                            ->execute()
            ;
            $insert_id = $this->setMethod($post['fs_name'])->add($post);
            self::update($id, array('settings_id' => $insert_id));
            return $id;
        }
        else
        {
            return false;
        }
    }

    public function save($lang_id = 1, $id = NULL)
    {
        if( $post = $this->validate_save() )
		{   
			if ((int)$id)
			{ // update
			DB::update('shop_payment_methods')
					->set(
						array(
						'name' => $post['name'],
	//					'cost' => $post['cost'],
//						'description' => $post['description'],
						'text' => $post['text'],
						'orderid' => $post['orderid'],
						'include_tax' => $post['include_tax'],
						'rateMethod' => $post['rateMethod'],
						'forShipment' => implode(',',$post['forShipment']),
						)
					)
					->where('id', '=', $id)
					->execute()
					;

					$this->setMethod($post['fs_name'])->save( $post['settings_id'],$post );
			}

			return $id;
		}
		else
		{
			return false;
		}
    }
    
    
    static public function update( $id = NULL, $data)
    {
        if ( $id && $data )
        {
            DB::update('shop_payment_methods')
                    ->set(
                        $data
                    )
                    ->where('id', '=', $id)
                    ->execute()
                    ;
            return true;
        }
        else
            return false;
        
    }


    function validate_save()
    {
        $keys = array(
            "fs_name",
            "name",
            "cost",
//            "margin",
            "settings_id",
            "percent",
            "plusToOrder",
            "invoice",
            "ordersendmail",
            "letter_description",
            "include_tax",
            "forShipment",
            "text",
            "postavshik",
            "merchant_info",
            "payment_testmode",
            "payment_rule",
            "action_url",
            "payment_type",
            "payment_addvalue",
            "secretcode",
            "orderid",
            "rateMethod",
        );

        $params = Arr::extract($_POST, $keys, '');

        $post = Validate::factory($params)
                        ->rule('name', 'not_empty')

//                        ->rule('cost', 'not_empty')
//                        ->rule('cost', 'digit')
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            $this->errors = $post->errors('validate');
        }
    }

    static function getPaymentGroups($where, $as_array = TRUE)
    {
        $query = DB::select()
                        ->from('shop_payment_methods')
                        ->where('protected', '=', '1');

        if ($where)
        {
            foreach ($where as $key => $item)
                $query->and_where($key, '=', $item);
        }
        $result = $query->execute();
        if ($result->count() == 0)
            return array();
        else
        {
            if (!$as_array)
                return $result->current();
            return $result->as_array('id');
        }
    }

    static public function getMainItem($where = NULL, $as_array = TRUE)
    {
        $query = DB::select()
                        ->from('shop_payment_methods');

        $query->where('id', 'IN', array_flip($where));

//                       ->limit(1);

        $result = $query->execute();

        if ($result->count() == 0)
            return array();
        else
        {
            if ($as_array == TRUE)
                $return = $result->as_array();
            else
                $return = $result->current();

//                $return2 = $this->setMethod($return['fs_name'])->getItem( array('id' => $return['settings_id']), FALSE );
//                $return['forShipment'] = explode(',', $return['forShipment']);
            return $return;
        }
    }

    public function status($id = NULL)
    {
        $query = DB::update('shop_payment_methods')->set(array(
                    'status' => DB::expr('!status')
                ));

        if (is_numeric($id))
        {
            $query->where('id', '=', $id); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function updateOrderId( $data = array() )
    {
        if( !empty($data) ):
        foreach( $data as $key => $item )
        {
            if( !self::update( $item, array( 'orderid'=> $key )) )
                    return false;
        }
        endif;
        return TRUE;
    }
}