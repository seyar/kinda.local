<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Customers extends Model
{

    /**
     * Function changes customer status.
     *
     * @return bool 
     */
    static public function changeStatus( $id )
    {
        DB::update('shop_customers')
            ->set(array('status' => DB::expr('! status')))
            ->where('id', '=', $id)
            ->execute();
        return TRUE;
    }
    
    static public function statuses()
    {
        $return = array();

        foreach (Controller_Customers::$customer_statuses as $key => $value)
        {
            $return[$key] = I18n::get($value);
        }

        return $return;
    }

    public function show($per_page = NULL)
    {
//            $rows_per_page = ((int)$_POST['rows_num'])
//				? ((int)$_POST['rows_num'])
//				: (int)cookie::get('rows_per_page_'.MODULE_NAME.'_'.MODULE_SUBCONTROLLER, 10);
//            $rows_per_page = $per_page ? $per_page : $rows_per_page;
//	    cookie::set('rows_per_page_'.MODULE_NAME.'_'.MODULE_SUBCONTROLLER, $rows_per_page); $_POST['rows_num'] = $rows_per_page;

        $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
                        ->from(array('shop_customers', 'customers'))
                        ->where(Db::expr('1'), '=', 1)
        ;

        $query = DB::select('customers.*', array('categories.name', 'category_name'))
                        ->from(array('shop_customers', 'customers'))
                        ->join(array('shop_price_categories', 'categories'))
                        ->on('categories.id', '=', 'customers.price_category_id')
                        ->where(Db::expr('1'), '=', 1)
        ;

        ////////////////////////////
        // begin filters
        ////////////////////////////

        $customer_id = (isset($_POST['search_customer_id']) ? $_POST['search_customer_id'] : Session::instance()->get('search_customer_id', '') );

        Session::instance()->set('search_customer_id', $customer_id);
        $_POST['search_customer_id'] = $customer_id;

        if ($_POST['search_customer_id'])
        {
            $query_c->and_where('customers.id', '=', (int) $_POST['search_customer_id']);
            $query->and_where('customers.id', '=', (int) $_POST['search_customer_id']);
        }


        $good_name = (isset($_POST['search_customer_name']) ? $_POST['search_customer_name'] : Session::instance()->get('search_customer_name', '') );

        Session::instance()->set('search_customer_name', $good_name);
        $_POST['search_customer_name'] = $good_name;

        if ($_POST['search_customer_name'])
        {
            $query_c->where(Db::expr('LCASE(customers.name)'), 'LIKE', "%" . mb_strtolower($_POST['search_customer_name'], 'UTF-8') . "%")
            ;
            $query->where(Db::expr('LCASE(customers.name)'), 'LIKE', "%" . mb_strtolower($_POST['search_customer_name'], 'UTF-8') . "%")
            ;
        }


        $good_email = (isset($_POST['search_customer_email']) ? $_POST['search_customer_email'] : Session::instance()->get('search_customer_email', '') );

        Session::instance()->set('search_customer_email', $good_email);
        $_POST['search_customer_email'] = $good_email;

        if ($_POST['search_customer_email'])
        {
            $query_c->and_where(Db::expr('LCASE(customers.email)'), 'LIKE', "%" . mb_strtolower($_POST['search_customer_email'], 'UTF-8') . "%");
            $query->and_where(Db::expr('LCASE(customers.email)'), 'LIKE', "%" . mb_strtolower($_POST['search_customer_email'], 'UTF-8') . "%");
        }


        $customer_cat = (isset($_POST['search_customer_cat']) ? (int) $_POST['search_customer_cat'] : Session::instance()->get('search_customer_cat', '') );

        Session::instance()->set('search_customer_cat', $customer_cat);
        $_POST['search_customer_cat'] = $customer_cat;

        if ($_POST['search_customer_cat'])
        {
            $query_c->and_where('customers.price_category_id', '=', $_POST['search_customer_cat']);
            $query->and_where('customers.price_category_id', '=', $_POST['search_customer_cat']);
        }


        $customer_phone = (isset($_POST['search_customer_phone']) ? $_POST['search_customer_phone'] : Session::instance()->get('search_customer_phone', '') );

        Session::instance()->set('search_customer_phone', $customer_phone);
        $_POST['search_customer_phone'] = $customer_phone;

        if ($_POST['search_customer_phone'])
        {
            $query_c->and_where(Db::expr('LCASE(customers.phone)'), 'LIKE', "%" . mb_strtolower($_POST['search_customer_phone'], 'UTF-8') . "%");
            $query->and_where(Db::expr('LCASE(customers.phone)'), 'LIKE', "%" . mb_strtolower($_POST['search_customer_phone'], 'UTF-8') . "%");
        }


        $news_subscribe = (isset($_POST['search_news_subscribe']) ? $_POST['search_news_subscribe'] : Session::instance()->get('search_news_subscribe', '') );

        Session::instance()->set('search_news_subscribe', $news_subscribe);
        $_POST['search_news_subscribe'] = $news_subscribe;

        if ($_POST['search_news_subscribe'])
        {
            $query_c->and_where('customers.news_subscribe', '=', $_POST['search_news_subscribe']);
            $query->and_where('customers.news_subscribe', '=', $_POST['search_news_subscribe']);
        }


        $customer_discount = (isset($_POST['search_customer_discount']) ? $_POST['search_customer_discount'] : Session::instance()->get('search_customer_discount', '') );

        Session::instance()->set('search_customer_discount', $customer_discount);
        $_POST['search_customer_discount'] = $customer_discount;

        if ($_POST['search_customer_discount'])
        {
            $query_c->and_where('customers.discount', '=', (float) $_POST['search_customer_discount']);
            $query->and_where('customers.discount', '=', (float) $_POST['search_customer_discount']);
        }


        $customer_turnover_from = (isset($_POST['search_customer_turnover_from']) ? $_POST['search_customer_turnover_from'] : Session::instance()->get('search_customer_turnover_from', '') );

        Session::instance()->set('search_customer_turnover_from', $customer_turnover_from);
        $_POST['search_customer_turnover_from'] = $customer_turnover_from;

        if ($_POST['search_customer_turnover_from'])
        {
            $query_c->and_where('customers.turnover', '>=', (float) $_POST['search_customer_turnover_from']);
            $query->and_where('customers.turnover', '>=', (float) $_POST['search_customer_turnover_from']);
        }


        $customer_turnover_to = (isset($_POST['search_customer_turnover_to']) ? $_POST['search_customer_turnover_to'] : Session::instance()->get('search_customer_turnover_to', '') );

        Session::instance()->set('search_customer_turnover_to', $customer_turnover_to);
        $_POST['search_customer_turnover_to'] = $customer_turnover_to;

        if ($_POST['search_customer_turnover_to'])
        {
            $query_c->and_where('customers.turnover', '<=', (float) $_POST['search_customer_turnover_to']);
            $query->and_where('customers.turnover', '<=', (float) $_POST['search_customer_turnover_to']);
        }


        $customer_created_from = (isset($_POST['search_customer_created_from']) ? $_POST['search_customer_created_from'] : Session::instance()->get('search_customer_created_from', '') );

        $customer_created_from = $customer_created_from ? $customer_created_from : '';

        Session::instance()->set('search_customer_created_from', $customer_created_from);
        $_POST['search_customer_created_from'] = $customer_created_from;
        if ($_POST['search_customer_created_from'])
        {
            $query_c->and_where('customers.date_create', '>=', $_POST['search_customer_created_from']);
            $query->and_where('customers.date_create', '>=', $_POST['search_customer_created_from']);
        }


        $customer_created_to = (isset($_POST['search_customer_created_to']) ? $_POST['search_customer_created_to'] : Session::instance()->get('search_customer_created_to', '') );

        $customer_created_to = $customer_created_to ? $customer_created_to : '';

        Session::instance()->set('search_customer_created_to', $customer_created_to);
        $_POST['search_customer_created_to'] = $customer_created_to;
        if ($_POST['search_customer_created_to'])
        {
            $query_c->and_where('customers.date_create', '<=', $_POST['search_customer_created_to']);
            $query->and_where('customers.date_create', '<=', $_POST['search_customer_created_to']);
        }


        $customer_status = (isset($_POST['search_customer_status']) ? (int) $_POST['search_customer_status'] : Session::instance()->get('search_customer_status', '') );

        Session::instance()->set('search_customer_status', $customer_status);
        $_POST['search_customer_status'] = $customer_status;

        if ($_POST['search_customer_status'])
        {
            $query_c->and_where('customers.status', '=', $_POST['search_customer_status']);
            $query->and_where('customers.status', '=', $_POST['search_customer_status']);
        }

        ////////////////////////////
        // end filters
        ////////////////////////////

        $count = $query_c
                        ->execute()
                        ->get('mycount');

        if (isset($_POST['page']))
            $_GET['page'] = $_POST['page'];

//	    $pagination = Pagination::factory(array(
//		    'total_items'    => $count,
//		    'items_per_page' => $rows_per_page,
//	    ));
//	    $result = $query
//			    ->order_by('customers.name','ASC')
//			    ->limit($pagination->items_per_page)
//			    ->offset($pagination->offset)
//			    ->execute();
//            $page_links_str = '';
//	    if ($result->count() == 0) return array(array(), '', array());
//	    else
//	    {
//                $result = $result->as_array();
//
//
//		return array( $result, $page_links_str, $pagination->total_pages );
//	    }
    }

    public function active($id = NULL)
    {
        if (is_numeric($id))
        {
            DB::query(Database::UPDATE, "UPDATE shop_customers SET status = !status WHERE id = $id LIMIT 1")
                    ->execute();
        }
        return true;
    }

    public function delete($id = NULL)
    {
        $query = DB::delete('shop_customers');

        if (is_numeric($id))
        {
            $query->where('id', '=', $id); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function delete_list($array = array())
    {
        $query = DB::delete('shop_customers');

        if (count($array))
        {
            $query->where('id', 'in', array_flip($array)); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    static public function load($id = NULL)
    {
        if ((int) $id)
        {
            $query = DB::select()
                            ->from('shop_customers')
                            ->limit(1);

            $query->where('id', '=', $id);

            $result = $query->execute();


            if($result->count() == 0)
                return array();
            else
            {
                $return = $result->current();

                $customer_addresses = Model_Customers::shipment_address_list($id);                
                $return = array_merge($return ,array('customer_addresses'=>$customer_addresses));
                
                return $return;
            }
        }
    }

    static public function update($data, $id)
    {

        DB::update('shop_customers')->set($data)->where('id', '=', $id)->execute();
    }

    static public function create($data)
    {
        list($id, $total_rows) = DB::insert('shop_customers',
                                array(
                                    'price_category_id',
                                    'name', 'email', 'phone',
                                )
                        )
                        ->values(
                                $data
                        )
                        ->execute()
        ;

        die(Kohana::debug($id));
        //echo($id);
        
        return $id;
    }

    static public function save($data = array())
    {
        unset($_POST['referer']);

        $keys = array_keys($data);

        list($id, $total_rows) = DB::insert('shop_customers', $keys)->values($data)->execute();
        if( !$post = self::validatePostSaveCustomer($_POST) ) return false;
        /**/

//        list($billing_id, $billing_total_rows) = DB::insert('shop_billing_addresses', array(
//            'name',
//            'country',
//            'state',
//            'city',
//            'zip',
//            'address_line1',
//            'address_line2',
//            'phone',
//            'email',
//            'customer_id',
//        ))->values(array(
//                $_POST['name'],
//                $_POST['country'],
//                $_POST['state'],
//                $_POST['city'],
//                $_POST['zip'],
//                $_POST['address_line1'],
//                $_POST['address_line2'],
//                $_POST['phone'],
//                $_POST['email'],
//                $data['id']
//        ))->execute();
        $billing_id = 1;
        list($shipment_id, $shipment_total_rows) = DB::insert('shop_shipment_addresses', array(
                    'name',
                    'country',
                    'state',
                    'city',
//                    'zip',
                    'address_line1',
//                    'address_line2',
                    'phone',
                    'customer_id',
                ))->values(array(
                    $post['name'],
                    $post['country'],
                    $post['district'],
                    $post['city'],
//                    $post['zip'],
                    $post['address'],
//                    $post['address_add'],
                    $post['phone'],
                    $data['id'],
                ))->execute();
        /**/
        DB::update('shop_customers')->set(array('default_address_id' => $shipment_id, 'default_pay_address_id' => $billing_id))->where('id', '=', $data['id'])->execute();
        return 1;
    }
    
    static function validatePostSaveCustomer( $arrpost )
    {
        $keys = array(
            'email', 'name', 'phone','country', 'district', 'city', 'address', 'comment'
        );

        
        $params = Arr::extract($arrpost, $keys, '');

        $post = Validate::factory($params)
                        ->rule('email', 'not_empty')
                        ->rule('email', 'email')
                        ->rule('name', 'not_empty')
                        ->rule('phone', 'not_empty')
                        ->rule('country', 'not_empty')
                        ->rule('district', 'not_empty')
                        ->rule('city', 'not_empty')
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            Messages::add( $post->errors('validate') );            
            return false;
        }
    }
    
    static function addShipmentAddress( $post = array() )
    {
        if( count($post) == 0 ) return array();
        list($shipment_id, $shipment_total_rows) = DB::insert('shop_shipment_addresses', array(
                    'name',
                    'country',
                    'state',
                    'city',
                    'zip',
                    'address_line1',
                    'address_line2',
                    'phone',
                    'news_subscribe',
                    'customer_id',
                ))->values(array(
                    $post['name'],
                    $post['country'],
                    $post['state'],
                    $post['city'],
                    $post['zip'],
                    $post['address'],
                    $post['address_add'],
                    isset($post['mobphone']) ? $post['mobphone'] : $post['phone'],
                    $post['news_subscribe'],
                    $post['id'],
                ))->execute();
        /**/
        return $shipment_id;
    }

    static public function billing_address_list($id=NULL)
    {
//        if ((int)$id)
//	{
//           $query = DB::select()
//                       ->from('shop_billing_addresses')
//                       ->where('customer_id', '=', $id)
//                    ;
//
//	    $result = $query->execute();
//
//	    if ($result->count() == 0) return array();
//	    else
//	    {
//		$return = $result->as_array('id');
//
//                foreach ($return as $key=>$value)
//                {
//                    unset($value['id'], $value['customer_id']);
//
//                    foreach ($value as $k=>$v)
//                        if (!$v) unset($value[$k]);
//
////                    $return[$key] = implode(',', $value);
//                    $return[$key] = $value;
//                }
//
//		return $return;
//	    }
//	}
//	else
//	{
        return array();
//	}
    }

    static public function shipment_address_list($id=NULL)
    {
        if ((int) $id)
        {
            $query = DB::select()
                            ->from('shop_shipment_addresses')
                            ->where('customer_id', '=', $id)
            ;

            $result = $query->execute();

            if ($result->count() == 0)
                return array();
            else
            {
                $return = $result->as_array('id');

                foreach ($return as $key => $value)
                {
                    unset($value['id'], $value['customer_id']);

                    foreach ($value as $k => $v)
                        if (!$v)
                            unset($value[$k]);

//                    $return[$key] = implode(',', $value);
                    $return[$key] = $value;
                }

                return $return;
            }
        }
        else
        {
            return array();
        }
    }
    
    static function getFilter()
    {        
        $filter = (array)json_decode( Cookie::get( 'filter_customers', '' ), TRUE );

        if( isset($_POST['status']) )
            $_POST['filter']['status'] = $_POST['status']; // because custom select not working with []
        
        if( isset($_POST['filter']) )
        {
            $filter = $_POST['filter'];
            Cookie::set( 'filter_' . MODULE_NAME, json_encode( $_POST['filter'] ) );
        }

        $where = array( );
        if( count( $filter ) > 0 ):
            foreach( $filter as $key => $item )
            {
                if( $item != '' )
                {
                    switch( $key )
                    {
                        case 'oborot-do':
                            $where[] = array('turnover', '>=', $item);
                            break;
                        case 'oborot-ot':
                            $where[] = array('turnover', '<=', $item);
                            break;
                        case 'name':
                            $where[] = array('name', 'LIKE', "%$item%");
                            break;
                        case 'email':
                            $where[] = array('email', 'LIKE', "%$item%");
                            break;
                        case 'phone':
                            $where[] = array('phone', 'LIKE', "%$item%");
                            break;
                        default:
                                $where[] = array( $key, '=', $item );
                            break;
                    }
                }
            }
        endif;
        
        return array($where, $filter);
    }

}