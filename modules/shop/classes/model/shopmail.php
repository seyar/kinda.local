<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 03.08.2010 - 10:32:27 Exp $
 *
 * Project:     rozymnik
 * File:        shopmail.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_ShopMail extends Model
{
    public  function getList($id = NULL, $per_page = 100)
    {
        $_POST['search_news_subscribe'] = 1;
        $oCustomers = new Model_Customers();
        if(! $id )
            $customers = $oCustomers->show($per_page);
        else
            $customers = $oCustomers->load($id);
        
        return $customers;
    }

    function send( $array, $lang_id = 2 )
    {
        $list = array_flip($array);

        $langs = Model_Content::get_languages_array();
        I18n::lang($langs[$lang_id]['prefix']);
        $lang = $langs[$lang_id]['prefix'] == 'en' ? '/'.$langs[$lang_id]['prefix'] : '';
        $prefix = $langs[$lang_id]['prefix'] == 'en' ? $langs[$lang_id]['prefix'] : '';
        
        foreach($list as $item)
        {

            $text = $_POST['text'];
            $row = self::getList($item);
            if( $row['unsubscribe'] )
                $text .= '<br/><br/>...................................<br/>'.i18n::get('To refuse the news subscription, please switch to link:').' <a href="http://'.$_SERVER['HTTP_HOST'].$lang.'/shop/mail/'.$row['unsubscribe'].'">http://'.$_SERVER['HTTP_HOST'].$lang.'/shop/mail/'.$row['unsubscribe'].'</a>';

            if(! Email::send(
			trim($row['email']),
			array(Kohana::config('shop')->send_from,Kohana::config('shop')->send_from_title),
			I18n::get(Kohana::config('shop')->subject).' '.$_SERVER['HTTP_HOST'],
			$text,
                        TRUE
		)){
                    Kohana_Log::instance()->add('email send error',
                            Kohana::config('shop')->send_from.'<br/>'.
                            trim($row['email']).'<br/>'.
                            I18n::get(Kohana::config('shop')->subject).' '.$_SERVER['HTTP_HOST'].'<br/>'.
                            $text
                        );
                }

        }

	return true;
    }


    static function unsubscribe( $code, $lang_id = 1 )
    {
        if($code)
        {
            DB::update('shop_customers')
                ->set( array('news_subscribe' => 0) )
                ->where('unsubscribe','=', $code)
                ->execute()
            ;
        }
        
        return 1;
    }
}
?>