<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Shop extends Model
{

    static public function product_create()
    {
        $post = $_POST;

        if (!isset($_POST['is_bestseller']))
        {
            $post['is_bestseller'] = 0;
        }
        if (!isset($_POST['is_new']))
        {
            $post['is_new'] = 0;
        }
        if (!isset($_POST['show_on_startpage']))
        {
            $post['show_on_startpage'] = 0;
        }

        //echo (Kohana::debug($post));
              list($id, $total_rows) =   DB::insert('shop_goods',
                                        array(
                                            'categories_id',
                                            'name', 'alter_name',
                                            'scu', 'short_description',
                                            'full_description', 'rating','weight',
                                            'is_bestseller', 'is_new',
                                            'status', 'show_on_startpage'
                                        )
                                )
                                ->values(
                                        array(
                                            $post['categories_id'],
                                            $post['name'],$post['alter_name'],
                                            $post['scu'],$post['short_description'],
                                            $post['full_description'],$post['rating'],
                                            $post['weight'],$post['is_bestseller'],
                                            $post['is_new'],$post['status'],
                                            $post['show_on_startpage']
          
                                        )
                                )
                                ->execute()
                        ;

              //echo($id);
                        return $id;
    }

    static public function product_load($id = NULL)
    {

        $query = DB::select('id', 'alter_name', 'name', 'short_description',
                                'full_description', 'is_bestseller', 'is_new', 'show_on_startpage',
                                'scu', 'weight', 'rating', 'status','brand_id','categories_id'
                        )
                        ->where('id', '=', $id)
                        ->from('shop_goods');

        $result = $query->execute()->as_array();

        $query_b = DB::select('id','name'
                )
                ->from('shop_brands');
        $result_b = $query_b
                ->execute()
                ->as_array();

        $query_c = DB::select('id' ,'name') ->from('shop_categories');

        $result_c = $query_c
                ->execute()
                ->as_array();
                
        return array($result,$result_b,$result_c);
    }

    static public function product_save($id = NULL)
    {
        $post = $_POST;

        if (!isset($_POST['is_bestseller']))
        {
            $post['is_bestseller'] = 0;
        }
        if (!isset($_POST['is_new']))
        {
            $post['is_new'] = 0;
        }
        if (!isset($_POST['show_on_startpage']))
        {
            $post['show_on_startpage'] = 0;
        }



        //die(Kohana::debug($post));
        DB::update('shop_goods')
                ->set(
                        array(
                            'name' => $post['name'],
                            'alter_name' => $post['alter_name'],
                            'scu' => $post['scu'],
                            'short_description' => $post['short_description'],
                            'full_description' => $post['full_description'],
                            'full_description' => $post['full_description'],
                            'rating' => $post['rating'],
                            'weight' => $post['weight'],
                            'is_bestseller' => $post['is_bestseller'],
                            'is_new' => $post['is_new'],
                            'status' => $post['status'],
                            'show_on_startpage' => $post['show_on_startpage'],
                            'brand_id' => $post['brand_id'],
                            'categories_id' => $post['categories_id'],
                        )
                )
                ->where('id', '=', $id)
                ->execute()
        ;
        return true;
    }

    static public function shop_currencies()
    {
        //if( Kohana::config('shop.rateMode') )
        $query = DB::select('id', 'name', 'rate')
                        ->from('shop_currencies')
                        ->order_by('code', 'ASC');

        $result = $query->execute();
        if ($result->count() == 0)
            return array();
        else
            return $result->as_array('id');
    }

    static public function shop_price_categories()
    {
        $query = DB::select('id', 'name', 'cost')
                        ->from('shop_price_categories')
                        ->order_by('name', 'ASC');

        $result = $query->execute();
        if ($result->count() == 0)
            return array();
        else
            return $result->as_array('id');
    }

    static public function payment_methods($where = NULL)
    {
        $query = DB::select('id', 'name', 'text')
                        ->from('shop_payment_methods')
                        ->order_by('orderid', 'ASC');
        if ($where)
        {
            foreach ($where as $key => $item)
            {
                $query->where($key, '=', $item);
            }
        }

        $result = $query->execute();
        if ($result->count() == 0)
            return array();
        else
            return $result->as_array('id');
    }

    static public function shipment_methods($where = NULL)
    {
        $query = DB::select()
                        ->from('shop_shipment_methods')
                        ->order_by('orderid', 'ASC');
        ;
        if ($where)
        {
            foreach ($where as $key => $item)
            {
                $query->where($key, '=', $item);
            }
        }

        $result = $query->execute();
        if ($result->count() == 0)
            return array();
        else
            return $result->as_array('id');
    }

    static public function price_categories()
    {
        $query = DB::select('id', 'name')
                        ->from('shop_price_categories')
                        ->order_by('name', 'ASC');

        $result = $query->execute();
        if ($result->count() == 0)
            return array();
        else
            return $result->as_array('id');
    }

}