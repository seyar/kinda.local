<?php
defined( 'SYSPATH' ) OR die( 'No direct access allowed.' );

class Model_Goods extends Model
{

    public $images_folder = '';

    static function statuses()
    {
        $statuses = array( '0' => I18n::get( 'Any' ) );

        foreach( Controller_Goods::$goods_status_list as $value )
            $statuses[$value] = I18n::get( ucfirst( str_replace( '_', ' ', $value ) ) );

        return $statuses;
    }

    static function getFilter( $lang_id = 1 )
    {
        $filter = (array) json_decode( Cookie::get( 'filter_' . MODULE_NAME, '' ), TRUE );

        if( isset( $_POST['cat'] ) )
            $_POST['filter']['cat'] = $_POST['cat']; // because custom select not working with []
        if( isset( $_POST['filter_price_categories_id']) )
            $_POST['filter']['price_categories_id'] = $_POST['filter_price_categories_id']; // because custom select not working with []
        if( isset( $_POST['status']) )
            $_POST['filter']['status'] = $_POST['status']; // because custom select not working with []

        if( isset( $_POST['filter'] ) )
        {
            $filter = $_POST['filter'];
            Cookie::set( 'filter_' . MODULE_NAME, json_encode( $_POST['filter'] ) );
        }

        $where = array( );
        if( count( $filter ) > 0 ):
            foreach( $filter as $key => $item )
            {
                if( !empty( $item ) )
                {
                    switch( $key )
                    {
                        case 'cat':
                            $cat = (int) $filter['cat'];
                            $cats = Model_Categories::findChildrenRecurse( $lang_id, $cat );
                            if( count( $cats ) > 0 )
                            {
                                $where[] = array( 'categories_id', 'IN', DB::expr( '(' . implode( ', ', $cats ) . ')' ) );
                            }else
                            {
                                $where[] = array( 'categories_id', '=', $cat );
                            }
                            /**/
                            break;
                        case 'shop_articul':
                            $where[] = array( 'scu', 'LIKE', "%$item%" );
                            break;
                        case 'name':
                            $where[] = array( 'shop_goods.name', 'LIKE', "%$item%" );
                            break;
                        case 'shop_col_ot':
                            $where[] = array( 'quantity', '>=', $item );
                            break;
                        case 'shop_col_do':
                            $where[] = array( 'shop_prices', '<=', $item );
                            break;
                        case 'shop_price_ot':
                            $where[] = array( 'shop_prices', '>=', $item );
                            break;
                        case 'shop_price_do':
                            $where[] = array( 'shop_prices', '<=', $item );
                            break;
                        case 'default_photo':
                            $where[] = array( 'default_photo', '=', '' );
//                            $where[] = array( 'default_photo', 'IS', DB::expr('NULL') );
                            break;
                        case 'id':
                            $where[] = array( 'shop_goods.id', '=', $item );
							break;
                        case 'status':
                            $where[] = array( 'shop_goods.status', '=', $item );
							break;
                        default:
                            $where[] = array( $key, '=', $item );
                    }
                }
            }
        endif;

        return array( $where, $filter );
    }

    static public function product_create()
    {
        $post = $_POST;

        if( !isset( $_POST['is_bestseller'] ) )
        {
            $post['is_bestseller'] = 0;
        }
        if( !isset( $_POST['is_new'] ) )
        {
            $post['is_new'] = 0;
        }
        if( !isset( $_POST['show_on_startpage'] ) )
        {
            $post['show_on_startpage'] = 0;
        }

        //echo (Kohana::debug($post));
        list($id, $total_rows) = DB::insert( 'shop_goods', array(
                            'categories_id',
                            'name', 'alter_name',
                            'scu', 'short_description',
                            'full_description', 'rating', 'weight',
                            'is_bestseller', 'is_new',
                            'status', 'show_on_startpage'
                                )
                        )
                        ->values(
                                array(
                                    $post['categories_id'],
                                    $post['name'], $post['alter_name'],
                                    $post['scu'], $post['short_description'],
                                    $post['full_description'], $post['rating'],
                                    $post['weight'], $post['is_bestseller'],
                                    $post['is_new'], $post['status'],
                                    $post['show_on_startpage']
                                )
                        )
                        ->execute()
        ;

        //echo($id);
        return $id;
    }

    static public function product_load( $id = NULL )
    {

        $query = DB::select( 'id', 'alter_name', 'name', 'short_description', 'full_description', 'is_bestseller', 'is_new', 'show_on_startpage', 'scu', 'weight', 'rating', 'status', 'brand_id', 'categories_id', 'seo_url', 'seo_description', 'seo_keywords'
                        )
                        ->where( 'id', '=', $id )
                        ->from( 'shop_goods' );

        $result = $query->execute()->as_array();

        $query_b = DB::select( 'id', 'name' )
                        ->from( 'shop_brands' );
        $result_b = $query_b
                        ->execute()
                        ->as_array();

        $query_c = DB::select( 'id', 'name' )->from( 'shop_categories' );

        $result_c = $query_c
                        ->execute()
                        ->as_array();

        return array( $result, $result_b, $result_c );
    }

    public function show( $languages_id = NULL )
    {
        if( is_numeric( $languages_id ) )
        {
            // Set and save rows_per_page value
            $rows_per_page = ((int) $_POST['rows_num']) ? ((int) $_POST['rows_num']) : (int) cookie::get( 'rows_per_page_' . MODULE_NAME . '_' . MODULE_SUBCONTROLLER, 10 );

            cookie::set( 'rows_per_page_' . MODULE_NAME . '_' . MODULE_SUBCONTROLLER, $rows_per_page );
            $_POST['rows_num'] = $rows_per_page;

            $query_c = DB::select( DB::expr( 'COUNT(*) AS mycount' ) )
                            ->from( array( 'shop_goods', 'goods' ) )
                            ->join( array( 'shop_prices', 'prices' ), 'LEFT' )
                            ->on( 'goods.id', '=', 'prices.good_id' )
                            ->join( array( 'shop_categories', 'c' ) )
                            ->on( 'goods.categories_id', '=', 'c.id' )
                            ->where( 'prices.price_categories_id', '=', Kohana_Config::instance()->load( 'shop' )->default_category )
                            ->and_where( 'prices.currencies_id', '=', Kohana_Config::instance()->load( 'shop' )->default_currency )
                            ->and_where( 'c.languages_id', '=', $languages_id )
            ;

            $query = DB::select( 'goods.*', array( 'categories.name', 'category_name' ), array( 'prices.price', 'price' ) )
                            ->from( array( 'shop_goods', 'goods' ) )
                            ->join( array( 'shop_categories', 'categories' ), 'LEFT' )
                            ->on( 'categories.id', '=', 'goods.categories_id' )
                            ->join( array( 'shop_prices', 'prices' ), 'LEFT' )
                            ->on( 'goods.id', '=', 'prices.good_id' )
                            ->where( 'prices.price_categories_id', '=', Kohana_Config::instance()->load( 'shop' )->default_category )
                            ->and_where( 'prices.currencies_id', '=', Kohana_Config::instance()->load( 'shop' )->default_currency )
                            ->and_where( 'categories.languages_id', '=', $languages_id )

            ;


            ////////////////////////////
            // begin filters
            ////////////////////////////
            
            // Set and save SCU
            $good_scu = (isset( $_POST['search_good_scu'] ) ? $_POST['search_good_scu'] : Session::instance()->get( 'search_good_scu', '' ) );

            Session::instance()->set( 'search_good_scu', $good_scu );
            
            $_POST['search_good_scu'] = $good_scu;
            if( $_POST['search_good_scu'] )
            {
                $query_c->and_where( 'goods.scu', '=', $_POST['search_good_scu'] );
                $query->and_where( 'goods.scu', '=', $_POST['search_good_scu'] );
            }

            // Set and save Name
            $good_name = (isset( $_POST['search_good_name'] ) ? $_POST['search_good_name'] : Session::instance()->get( 'search_good_name', '' ) );

            Session::instance()->set( 'search_good_name', $good_name );
            $_POST['search_good_name'] = $good_name;
            if( $_POST['search_good_name'] )
            {
                $query_c->where_open()
                        ->where( Db::expr( 'LCASE(goods.name)' ), 'LIKE', "%" . mb_strtolower( $_POST['search_good_name'], 'UTF-8' ) . "%" )
                        ->or_where( Db::expr( 'LCASE(goods.alter_name)' ), 'LIKE', "%" . mb_strtolower( $_POST['search_good_name'], 'UTF-8' ) . "%" )
                        ->where_close();
                $query->where_open()
                        ->where( Db::expr( 'LCASE(goods.name)' ), 'LIKE', "%" . mb_strtolower( $_POST['search_good_name'], 'UTF-8' ) . "%" )
                        ->or_where( Db::expr( 'LCASE(goods.alter_name)' ), 'LIKE', "%" . mb_strtolower( $_POST['search_good_name'], 'UTF-8' ) . "%" )
                        ->where_close();
            }

            // Set and save filter for cat ID

            $good_cat = (isset( $_POST['search_good_cat'] ) ? $_POST['search_good_cat'] : Session::instance()->get( 'search_good_cat', Kohana::config( 'shop' )->root_category_id ) );

            Session::instance()->set( 'search_good_cat', $good_cat );
            $_POST['search_good_cat'] = $good_cat;

            if( $_POST['search_good_cat'] && $_POST['search_good_cat'] != Kohana::config( 'shop' )->root_category_id )
            {

                // Set and save filter for cat ID
                $all_cats = Model_Categories::findChildrenRecurse( $languages_id, $_POST['search_good_cat'], $result );
                $all_cats = count( $all_cats ) != 0 ? $all_cats : array( $_POST['search_good_cat'] );

                $in = '(' . implode( ',', $all_cats ) . ')';
                $query_c->and_where( 'categories_id', 'IN', DB::expr( $in ) );
                $query->and_where( 'goods.categories_id', 'IN', DB::expr( $in ) );
            }

            // Set and save Price filters
            $good_price_from = (isset( $_POST['search_good_price_from'] ) ? (float) $_POST['search_good_price_from'] : Session::instance()->get( 'search_good_price_from', '' ) );

            $good_price_from = $good_price_from ? $good_price_from : '';

            Session::instance()->set( 'search_good_price_from', $good_price_from );
            $_POST['search_good_price_from'] = $good_price_from;
            if( $_POST['search_good_price_from'] )
            {
                $query_c->and_where( 'prices.price', '>=', $_POST['search_good_price_from'] );
                $query->and_where( 'prices.price', '>=', $_POST['search_good_price_from'] );
            }

            $good_price_to = (isset( $_POST['search_good_price_to'] ) ? (float) $_POST['search_good_price_to'] : Session::instance()->get( 'search_good_price_to', '' ) );

            $good_price_to = $good_price_to ? $good_price_to : '';

            Session::instance()->set( 'search_good_price_to', $good_price_to );
            $_POST['search_good_price_to'] = $good_price_to;
            if( $_POST['search_good_price_to'] )
            {
                $query_c->and_where( 'prices.price', '<=', $_POST['search_good_price_to'] );
                $query->and_where( 'prices.price', '<=', $_POST['search_good_price_to'] );
            }

            $good_price_currency = (isset( $_POST['search_good_price_currency'] ) ? (int) $_POST['search_good_price_currency'] : Session::instance()->get( 'search_good_price_currency', Controller_Shop::$default_currency ) );

            Session::instance()->set( 'search_good_price_currency', $good_price_currency );
            $_POST['search_good_price_currency'] = $good_price_currency;
            if( $_POST['search_good_price_currency'] && ($good_price_from || $good_price_to) )
            {
                $query_c->and_where( 'prices.currencies_id', '=', $_POST['search_good_price_currency'] );
                $query->and_where( 'prices.currencies_id', '=', $_POST['search_good_price_currency'] );
            }else if( $good_price_from || $good_price_to )
            {
                $query_c->where( 'prices.currencies_id', '=', Kohana::config( 'shop' )->default_currency );
                $query->where( 'prices.currencies_id', '=', Kohana::config( 'shop' )->default_currency );
            }

            // Set and save Quantity
            $good_quantity_from = (isset( $_POST['search_good_quantity_from'] ) ? (int) $_POST['search_good_quantity_from'] : Session::instance()->get( 'search_good_quantity_from', '' ) );

            $good_quantity_from = $good_quantity_from ? $good_quantity_from : '';

            Session::instance()->set( 'search_good_quantity_from', $good_quantity_from );
            $_POST['search_good_quantity_from'] = $good_quantity_from;
            if( $_POST['search_good_quantity_from'] )
            {
                $query_c->and_where( 'goods.quantity', '>=', $_POST['search_good_quantity_from'] );
                $query->and_where( 'goods.quantity', '>=', $_POST['search_good_quantity_from'] );
            }

            $good_quantity_to = (isset( $_POST['search_good_quantity_to'] ) ? (int) $_POST['search_good_quantity_to'] : Session::instance()->get( 'search_good_quantity_to', '' ) );

            $good_quantity_to = $good_quantity_to ? $good_quantity_to : '';

            Session::instance()->set( 'search_good_quantity_to', $good_quantity_to );
            $_POST['search_good_quantity_to'] = $good_quantity_to;
            if( $_POST['search_good_quantity_to'] )
            {
                $query_c->and_where( 'goods.quantity', '<=', $_POST['search_good_quantity_to'] );
                $query->and_where( 'goods.quantity', '<=', $_POST['search_good_quantity_to'] );
            }

            // Set and save Status
            $good_status = (isset( $_POST['search_good_status'] ) ? $_POST['search_good_status'] : Session::instance()->get( 'search_good_status', '' ) );

            Session::instance()->set( 'search_good_status', $good_status );
            $_POST['search_good_status'] = $good_status;
            if( $_POST['search_good_status'] )
            {
                $query_c->and_where( 'goods.status', '=', $_POST['search_good_status'] );
                $query->and_where( 'goods.status', '=', $_POST['search_good_status'] );
            }

            // Set and save default photo filter
            $good_default_photo = (isset( $_POST['search_default_photo'] ) ? $_POST['search_default_photo'] : Session::instance()->get( 'search_default_photo', '' ) );

            Session::instance()->set( 'search_default_photo', $good_default_photo );
            $_POST['search_default_photo'] = $good_default_photo;
            if( $_POST['search_default_photo'] == 1 )
            {
                $query_c->and_where( DB::expr( 'TRIM(goods.default_photo)' ), '!=', '' );
                $query->and_where( DB::expr( 'TRIM(goods.default_photo)' ), '!=', '' );
            }elseif( $_POST['search_default_photo'] == 0 )
            {
                $query_c->and_where( DB::expr( 'TRIM(goods.default_photo)' ), '=', '' );
                $query->and_where( DB::expr( 'TRIM(goods.default_photo)' ), '=', '' );
            }

            ////////////////////////////
            // end filters
            ////////////////////////////
            $count = $query_c
                            ->execute()
                            ->get( 'mycount' );

            if( isset( $_POST['page'] ) )
                $_GET['page'] = $_POST['page'];

            $pagination = Pagination::factory( array(
                        'total_items' => $count,
                        'items_per_page' => $rows_per_page,
                    ) );

            $result = $query
                            ->order_by( 'id', 'ASC' )
                            ->limit( $pagination->items_per_page )
                            ->offset( $pagination->offset )
                            ->execute()
            ;

            $page_links_str = '';
            if( $result->count() == 0 )
                return array( array( ), '', array( ) );
            else
            {
                $result = $result->as_array( 'id' );

                return array( $result, $page_links_str, $pagination->total_pages );
            }
        }
    }

    static public function load( $id = NULL )
    {
        if( (int) $id )
        {
            $query = DB::select()
                            ->from( 'shop_goods' )
                            ->limit( 1 );

            $query->where( 'id', '=', $id );

            $result = $query->execute();
            if( $result->count() == 0 )
                return array( );
            else
            {
                $return = $result->current();

                $prices = DB::select()
                                ->from( array( 'shop_prices', 'prices' ) )
                                ->where( 'good_id', '=', $return['id'] )
                                ->order_by( 'currencies_id', 'ASC' )
                                ->cached( 10 )
                                ->execute()
//                            ->as_array('currencies_id')
                                ->as_array()
                ;

                $curr = new Model_Frontend_Currencies();
                $return['prices'] = $curr->getPrice( $prices );
                $return['oldprices'] = $curr->getPrice( $prices, 'oldprice' );

                $add_fields = DB::select( 'ser_addFields' )
                                ->from( 'shop_categories' )
                                ->where( 'id', '=', $return['categories_id'] )
                                ->cached( 30 )
                                ->execute()
                                ->current()
                ;

                $return['ser_addFields'] = unserialize( $add_fields['ser_addFields'] );

                $return['add_fields'] = DB::query( Database::SELECT, "SELECT `f`.`id` AS `f_id`, `f`.`alias` AS `f_alias`, `f`.`is_group` AS `f_group`, `f`.`name`, `f`.`type`, `f`.`desc`, `f`.`defaults`, v.* "
                                        . " FROM `shop_goods_properties` AS `f` "
                                        . " LEFT OUTER JOIN "
                                        . "(SELECT * FROM `shop_goods_properties_values` AS v1 WHERE v1.good_id = {$return['id']} ) "
                                        . " AS `v` ON (`f`.`id`=`v`.`properties_category_id`) "
                                        . " WHERE f.category_id = {$return['categories_id']}"
                                        . " ORDER BY `f_alias` "
                                )
//                                ->cached( 30 )
                                ->execute()
                                ->as_array()
                ;

                return $return;
            }
        }else
        {
            return array( 'parent_id' => Session::instance()->get( 'search_good_cat' ) );
        }
    }

    public function save( $lang, $id = NULL )
    {
        if( $post = Model_Goods::validate() )
        {
            $search = array( '%23', '%3F', '%2F', '%26', '.', ',', 'quot','amp', '/','"',"'",'%27',';','%2C','%28','%29','%3B','&','%2B','(',')', );
            //        $replace = array('#','?','/','&', "'",'/', '"');
            $replace = array( '', '', '', '', '', '','_','_','_', '', '','','','','(',')','','','','','' );
            $post['seo_url'] = $post['seo_url'] ? str_replace( $search, $replace, urlencode($post['seo_url']) ) : str_replace( $search, $replace, Controller_Admin::to_url( $post['name'] ));
            $post['seo_url'] = str_replace( '__', '_', $post['seo_url'] );
            $seourl = strtolower( str_replace( '__', '_', $post['seo_url'] ));
            
            if( (int)$id )
            { 
                // update
                $id = (int)$id;
                
                DB::update( 'shop_goods' )
                        ->set(
                                array(
                                    'categories_id' => $post['categories_id'],
                                    'scu' => $post['scu'],
                                    'name' => $post['name'],
                                    'brand_id' => $post['brand_id'],
                                    'alter_name' => $post['alter_name'],
                                    'short_description' => $post['short_description'],
                                    'full_description' => $post['full_description'],
                                    'is_bestseller' => $post['is_bestseller'],
                                    'is_new' => $post['is_new'],
                                    'rating' => $post['rating'],
                                    'show_on_startpage' => $post['show_on_startpage'],
                                    'cost' => $post['cost'],
                                    'weight' => $post['weight'],
                                    'quantity' => $post['quantity'],
                                    'guaranties_period' => $post['guaranties_period'],
                                    'store_approximate_date' => (strtotime( $post['store_approximate_date'] ) ? $post['store_approximate_date'] : NULL),
                                    'seo_keywords' => $post['seo_keywords'],
                                    'seo_description' => $post['seo_description'],
                                    'seo_url' => $seourl,
                                    'status' => $post['status'],
                                    'parsed' => $post['parsed'],
                                    'format' => $post['format'],
                                )
                        )
                        ->where( 'id', '=', $id )
                        ->execute()
                ;
            }else
            { // create
                $id = $total_rows = 0;

                list($id, $total_rows) = DB::insert( 'shop_goods', array(
                                    'categories_id',
                                    'scu',
                                    'name',
                                    'brand_id',
                                    'alter_name',
                                    'short_description',
                                    'full_description',
                                    'is_bestseller',
                                    'is_new',
                                    'rating',
                                    'show_on_startpage',
                                    'cost',
                                    'weight',
                                    'quantity',
                                    'guaranties_period',
                                    'store_approximate_date',
                                    'seo_keywords',
                                    'seo_description',
                                    'seo_url',
                                    'status',
                                    'format',
                                        )
                                )
                                ->values(
                                        array(
                                            $post['categories_id'],
                                            $post['scu'],
                                            $post['name'],
                                            $post['brand_id'],
                                            Model_Content::arrGet($post, 'alter_name', $post['name']),
                                            $post['short_description'],
                                            $post['full_description'],
                                            $post['is_bestseller'],
                                            $post['is_new'],
                                            $post['rating'],
                                            $post['show_on_startpage'],
                                            $post['cost'],
                                            $post['weight'],
                                            $post['quantity'],
                                            $post['guaranties_period'],
                                            (strtotime( $post['store_approximate_date'] ) ? $post['store_approximate_date'] : NULL),
                                            $post['seo_keywords'],
                                            $post['seo_description'],
                                            $seourl,
                                            $post['status'],
                                            $post['format'],
                                        )
                                )
                                ->execute();
            }

            // Prices update
            if( method_exists( 'Model_Currencies', 'prices_update' ) )
            {
                Model_Currencies::prices_update( $id, 'price', $post );
                Model_Currencies::prices_update( $id, 'oldprice', $post );
            }

            Model_Goods::save_add_filelds( $lang, $id, $post );

            // save rec goods
            $addGoodsModel = Kohana::config( 'shop' )->addGoodsModel;
            $addGoodsMethod = Kohana::config( 'shop' )->addGoodsMethSave;

            if( isset( $addGoodsModel ) && !empty( $addGoodsModel ) && method_exists( $addGoodsModel, $addGoodsMethod ) )
                call_user_func( array( $addGoodsModel, $addGoodsMethod ), $id, $post, 'goodId' );

            if( isset($post['goodgroups_scu']) && count($post['goodgroups_scu']) > 0 )
            {
                $this->saveGoodGroups($post, $id);
            }
            
            return $id;
        }
        else
        {
            return false;
        }
    }

    static public function save_add_filelds( $lang = NULL, $id = NULL, $post = array( ) )
    {
        if( !$lang OR !$id )
            return false;
        
        $result = array( );

        $rows = DB::select()
                        ->from( 'shop_goods_properties' )
                        ->where( 'category_id', '=', $post['categories_id'] )
                        ->execute()
                        ->as_array()
        ;

        foreach( $rows as $value )
        {
            if( isset($_POST['add_fields'][$value['id']]) )
            {
                $result[$value['id']] = array(
                    'name' => $value['name'],
                    'type' => $value['type'],
                );
            }else
            {
                $query = Db::delete( 'shop_goods_properties_values' )
                                ->where( 'good_id', '=', $id )
                                ->and_where( 'properties_category_id', '=', (int)@$_POST['add_fields_id'][$value['id']] )
                ;
                $query->execute();
            }
        }


        foreach( $result as $key => $value )
        {

            $insert = array( );

            $null = DB::expr( 'NULL' );
            if( !$_POST['add_fields_id'][$key] )
            {
                $query = Db::query( Database::INSERT
                                        , " INSERT INTO shop_goods_properties_values "
                                        . " SET "
                                        . "   good_id = $id "
                                        . " , properties_category_id = :add_fld_id "
                                        . " , value_int = :value_int "
                                        . " , value_float = :value_float "
                                        . " , value_varchar = :value_varchar "
                                        . " , value_text = :value_text "
                                        . " , value_date = :value_date "
                                )
                                ->bind( ':add_fld_id', $key )
                                ->bind( ':value_int', $null )
                                ->bind( ':value_float', $null )
                                ->bind( ':value_varchar', $null )
                                ->bind( ':value_text', $null )
                                ->bind( ':value_date', $null )
                ;
            }
            else
                $query = Db::query( Database::UPDATE
                                        , " UPDATE shop_goods_properties_values "
                                        . " SET "
                                        . "   good_id = $id "
                                        . " , properties_category_id = :add_fld_id "
                                        . " , value_int = :value_int "
                                        . " , value_float = :value_float "
                                        . " , value_varchar = :value_varchar "
                                        . " , value_text = :value_text "
                                        . " , value_date = :value_date "
                                        . " WHERE id = " . $_POST['add_fields_id'][$key]
                                )
                                ->bind( ':add_fld_id', $key )
                                ->bind( ':value_int', $null )
                                ->bind( ':value_float', $null )
                                ->bind( ':value_varchar', $null )
                                ->bind( ':value_text', $null )
                                ->bind( ':value_date', $null )
                ;
            switch( $value['type'] )
            {
                case 'int':
                    $query->bind( ':value_int', $_POST['add_fields'][$key] );
                    break;
                
                case 'boolean':
                    if(is_array($_POST['add_fields'][$key]))
                        $query->bind( ':value_text', implode('|',$_POST['add_fields'][$key]) );
                    break;
                
                case 'float':
                    $query->bind( ':value_float', $_POST['add_fields'][$key] );
                    break;

                case 'varchar':
                    $query->bind( ':value_varchar', $_POST['add_fields'][$key] );
                    break;

                case 'text':
                case 'select':
                    $query->bind( ':value_text', $_POST['add_fields'][$key] );
                    break;

                case 'date':
                    $query->bind( ':value_date', $_POST['add_fields'][$key] );
                    break;

                default:
                    break;
            }
            $query->execute();
        }
        return true;
    }

    public function saveGoodGroups( $post = array(), $id = NULL )
    {
        if( !isset($post['goodgroups_scu']) || count($post['goodgroups_scu']) == 0 && !$id ) return array();
        if($post['goodgroups_scu']):
        foreach($post['goodgroups_scu'] as $key => $item)
        {
            if( !empty($item) )
            {
                if( !isset($post['goodgroups'][$key]) )
                {// товар новый
                    list($insertId, $totalRows) = $this->copyGood($id);
                    DB::insert('shop_goodgroups', array('goodId','mainGoodId') )->values(array($insertId, $id))->execute();
                    // обновление табл гудс другим артикулом. 
                    // и цен 
                    DB::update('shop_goods')->set( array('scu' => $item, 'format' => $post['goodgroups_format'][$key]) )->where('id','=', $insertId)->execute();
                    DB::insert('shop_prices', array('price','good_id',
                        'price_categories_id','currencies_id') )->values( 
                                array(number_format($post['goodgroups_price'][$key],2,'.',''),$insertId,1,1) )
                            ->execute();
                }
                else
                {
                    $this->updateHiddenGood( $id, $key );
                    DB::update('shop_goods')->set( array('scu' => $item,'format' => $post['goodgroups_format'][$key],) )->where('id','=', $key)->execute();
                    DB::update('shop_prices')->set(
                            array('price' => number_format($post['goodgroups_price'][$key],2,'.',''),
                                'price_categories_id' => 1,
                                'currencies_id' => 1))
                            ->where('good_id','=',$key)
                            ->execute();
                }
            }
        }
        endif;
    }
    
    static function getGoodsGroups( $id = NULL )
    {
        if( !$id ) return false;
        
        $result = DB::select('goodId',array('g.scu','scu'),array('g.format','format'), array('p.price','price') )
                ->from(array('shop_goodgroups','gg'))
                ->join(array('shop_goods','g'))
                    ->on('gg.goodId','=','g.id')
                ->join(array('shop_prices','p'))
                    ->on('gg.goodId','=','p.good_id')
                ->where('mainGoodId','=',$id)
                ->execute()
                ;
        if( $result->count() == 0 ) return array();
        else
            return $result->as_array('goodId');
    }
    
    public function copyGood( $from = NULL )
    {
        $columns = array('scu','format', 'categories_id', 'brand_id','alter_name','name',
                'short_description','full_description','is_bestseller',
                'is_new','rating','show_on_startpage','default_photo',
                'start_date','edit_date','date','weight',
                'quantity','guaranties_period','store_approximate_date',
                'seo_title','seo_keywords','seo_description','seo_url',
                'parsed');

        $select = DB::select( DB::expr( implode(',',$columns).', "archive"' ) )
                ->from('shop_goods')
                ->where('id','=',$from)
                ;

        $sql = "INSERT INTO shop_goods(".implode(',',$columns).", status) ".$select->__toString();
        $data = DB::query(Database::INSERT, $sql)->execute();
        
        $folder = MEDIAPATH.'shop/';
        if( !file_exists($folder.$data[0].'/') ) mkdir( $folder.$data[0].'/', 0755,true );
        Helper_Directory::fullCopy( $folder.$from.'/', $folder.$data[0].'/');
        
        return $data;
    }
    
    public function updateHiddenGood( $from = NULL, $to = NULL )
    {
        if( !$from || !$to ) return false;
            
        $columns = array('categories_id', 'brand_id','alter_name','name',
                'short_description','full_description','is_bestseller',
                'is_new','rating','show_on_startpage','default_photo',
                'start_date','edit_date','date','weight',
                'quantity','guaranties_period','store_approximate_date',
                'seo_title','seo_keywords','seo_description','seo_url',
                'parsed');
        
        $select = DB::select( DB::expr( implode(',',$columns)) )
                ->from('shop_goods')
                ->where('id','=',$from)
                ->execute()
                ;
        if( $select->count() == 0 ) return false;
        
        $data = array_combine($columns, $select->current() );
        
        DB::update('shop_goods')
                ->set($data)
                ->where('id','=', $to)
                ->execute()
                ;
        return  array($from, $to);
    }
    
    public function validate()
    {
        $keys = array(
            'categories_id',
            'scu',
            'name',
            'brand_id',
            'alter_name',
            'short_description',
            'full_description',
            'is_bestseller',
            'is_new',
            'show_on_startpage',
            'rating',
            'cost',
            'weight',
            'quantity',
            'guaranties_period',
            'store_approximate_date',
            'seo_keywords',
            'seo_description',
            'seo_url',
            'status',
            'price',
            'oldprice',
            'add_fields_id',
            'add_fields',
            'recgoodid',
            'parsed',
            'goodgroups',
            'goodgroups_scu',
            'goodgroups_price',
            'goodgroups_format',
            'format',
        );
        $params = Arr::extract( $_POST, $keys, 0 );
        $params['weight'] = str_replace( ",", ".", $params['weight'] );
        $params['cost'] = str_replace( ",", ".", $params['cost'] );
        $post = Validate::factory( $params )
                        ->rule( 'categories_id', 'not_empty' )
                        ->rule( 'categories_id', 'digit' )
//                      ->rule('scu', 'not_empty')
                        ->rule( 'name', 'not_empty' )
                        ->rule( 'brand_id', 'digit' )
//                      ->rule( 'alter_name', 'not_empty' )
//                      ->rule('short_description', 'not_empty')
                        ->rule( 'full_description', 'not_empty' )
                        ->rule( 'is_bestseller', 'digit' )
                        ->rule( 'is_new', 'digit' )
                        ->rule( 'show_on_startpage', 'digit' )
//                      ->rule('rating', 'decimal', array(2))
//                      ->rule('cost', 'numeric')
                        ->rule( 'cost', 'regex', array( "/^[0-9]+((\.|,)[0-9]{1,2})?$/" ) )
                        ->rule( 'weight', 'regex', array( "/^[0-9]+((\.|,)[0-9]{1,3})?$/" ) )
                        ->rule( 'quantity', 'digit' )
                        ->rule( 'guaranties_period', 'digit' )
//                      ->rule( 'price', 'numeric' )
                        ->rule( 'store_approximate_date', 'date' )
                        ->rule( 'status', 'not_empty' )
        ;
        if( $post->check() )
        {
            return $params;
        }else
        {
            Messages::add( $post->errors( 'validate' ) );
        }
    }

    public function delete( $id = NULL )
    {
        $query = DB::delete( 'shop_goods' );

        if( is_numeric( $id ) )
        {
            $query->where( 'id', '=', $id ); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function delete_list( array $array )
    {
        $query = DB::delete( 'shop_goods' );

        if( count( $array ) )
        {
            $query->where( 'id', 'in', array_flip( $array ) );

            $total_rows = $query->execute();
        }
    }

    // Images functions

    public function images_default( $file, $id = NULL )
    {
        if( is_file( $file ) )
        {
            $id = defined( 'MODULE_ID' ) ? MODULE_ID : $id;

            $query = DB::update( 'shop_goods' )
                            ->set(
                                    array(
                                        'default_photo' => basename( $_GET['name'] ),
                                    )
                            )
                            ->where( 'id', '=', $id );
            $query->execute();
            echo '1';
        }
    }

    public function images_delete( $folder, $id = NULL )
    {
        if( $id == NULL ) return false;
        
        if( isset($_GET['name']) && is_array($_GET['name']) )
        {
            foreach( $_GET['name'] as $item )
            {                
                $obj_image = MEDIAPATH . "$folder/" . $id . '/' . basename( $item );

                if( is_file( $obj_image ) )
                    if( unlink( $obj_image ) )
                    {
                        $obj_image = MEDIAPATH . "$folder/" . $id . '/prev_' . basename( $item );

                        if( is_file( $obj_image ) )
                        {
                            if( unlink( $obj_image ) )
                                echo '1';
                        }
                        else
                            echo '1';
                    }
            }
        }
        else
        {
            $obj_image = MEDIAPATH . "$folder/" . $id . '/' . basename( $_GET['name'] );

            if( is_file( $obj_image ) )
                if( unlink( $obj_image ) )
                {

                    $obj_image = MEDIAPATH . "$folder/" . $id . '/prev_' . basename( $_GET['name'] );

                    if( is_file( $obj_image ) )
                    {
                        if( unlink( $obj_image ) )
                            echo '1';
                    }
                    else
                        echo '1';
                }
        }
    }

    static public function images_show( $folder, $path='', $webpath='/', $id = NULL )
    {
        $obj = array( 'image' => '' );

        $id = defined( 'MODULE_ID' ) ? MODULE_ID : $id;

        $obj = DB::select( 'default_photo' )
                        ->from( 'shop_goods' )
                        ->where( 'id', '=', $id )
                        ->limit( 1 )
                        ->execute()
                        ->current()
        ;

        $filesArray = array( 'path' => $path
            , 'webpath' => $webpath
            , 'files' => array( )
            , 'default_img' => $obj['default_photo'] );

        if( !file_exists( MEDIAPATH . "$folder/$id/" ) )
            mkdir( MEDIAPATH . "$folder/$id/", 0755, true );
        $obj_images_dir = dir( MEDIAPATH . "$folder/$id/" );

        if( $obj_images_dir )
        {
            while( false !== ($entry = $obj_images_dir->read()) )
            {
                if( $entry != '.' and $entry != '..' and !strstr( $entry, 'prev_' ) )
                    $filesArray['files'][] = $entry;
            }
        }

        // set default image if it empty and there is images
        if( !$obj['default_photo'] AND count( $filesArray['files'] ) )
        {
            DB::update( 'shop_goods' )
                    ->set( array( 'default_photo' => $filesArray['files'][0] ) )
                    ->where( 'id', '=', $id )
                    ->execute()
            ;
            $filesArray['default_img'] = $filesArray['files'][0];
        }
        return $filesArray;
    }

    public function images_upload( $folder, $width = NULL, $height = NULL, $t_width = NULL, $t_height = NULL, $id = NULL, $watermark = NULL )
    {

        if( !empty( $_FILES ) )
        {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = MEDIAPATH . "$folder/$id/";
            $targetFile = str_replace( '//', '/', $targetPath ) . strtolower( Controller_Admin::to_url( $_FILES['Filedata']['name'] ) );
            if( !file_exists( MEDIAPATH . "$folder/$id" ) )
                @mkdir( str_replace( '//', '/', $targetPath ), 0755, true ); move_uploaded_file( $tempFile, $targetFile );
            $this->image = Image::factory( $targetFile );
            // Resize Image             
            if( $width || $height )
            {
                if( $width < $this->image->width || $height < $this->image->height )
                {
                    $this->image->resize( $width, $height, Image::AUTO )
                    //->crop($width, $height)                     
                    ;
                } $this->image->save( $targetFile, 90 );
            }
            // Generate Thumbnail             
            if( $width || $height )
            {
                $this->image->resize( $t_width, $t_height, Image::AUTO )->crop( $t_width, $t_height );
                $this->image->save( dirname( $targetFile ) . '/prev_' . basename( $targetFile ), 90 );
            }

            if( $watermark !== null && file_exists( $watermark ) )
            {
                $this->image = Image::factory( $targetFile );
                $this->image->watermark( Image::factory( $watermark ), true, true )->save( $targetFile, 90 );
            }
            echo "1";
        } die();
    }

    public function images_check( $folder, $id = NULL )
    {
        $fileArray = array( );
        if( !file_exists( MEDIAPATH . "$folder/$id/" ) )
            return array( );
        foreach( $_POST as $key => $value )
        {
            if( $key != 'folder' && $key != '_' )
            {
                if( file_exists( MEDIAPATH . "$folder/" . $id . "/$value" ) )
                {
                    $fileArray[$key] = $value;
                }
            }
        }
        echo json_encode( $fileArray );
        die();
    }

    function reprice()
    {
        $goods = DB::select( 'id' )
                        ->from( 'shop_goods' )
//                ->limit(100)
                        ->execute()
        ;
        $goods = $goods->as_array( 'id' );
        $prices = DB::select( array( 'good_id', 'id' ) )
                        ->from( 'shop_prices' )
//                ->limit(100)
                        ->execute()
        ;
        $prices = $prices->as_array( 'id' );

        $query = DB::insert( 'shop_prices', array( 'price_categories_id', 'currencies_id', 'price', 'good_id' ) );
        $arr = array_diff_assoc( $goods, $prices );
        if( count( $arr ) == 0 )
            die( 'нетУ' );
        foreach( $arr as $item )
        {
            $query->values( array( 1, 1, 0, $item['id'] ) );
            $query->values( array( 2, 1, 0, $item['id'] ) );
        }
        $query->execute();
        die();
    }

    static public function publ_types( $count_pubs = FALSE, $languages_id = NULL )
    {
        $query = DB::select( 'publicationstypes.id', 'publicationstypes.name', 'publicationstypes.parents_path', 'publicationstypes.parent_id', 'publicationstypes.parent_url', 'publicationstypes.url'
                        )
                        ->from( 'publicationstypes' )
                        ->order_by( 'publicationstypes.name' )
                        ->cached( 30 )
        ;

        if( $count_pubs )
        {
            $query = DB::select( 'publicationstypes.id', 'publicationstypes.name', 'publicationstypes.parents_path', 'publicationstypes.parent_id', 'publicationstypes.parent_url', 'publicationstypes.url', Db::expr( 'COUNT(publications.id) AS count' )
                            )
                            ->from( 'publicationstypes' )
                            ->join( 'publications', 'left' )
                            ->on( 'publicationstypes.id', '=', Db::expr( 'publications.publicationstypes_id AND publications.languages_id = ' . $languages_id )
                            )
                            ->order_by( 'publicationstypes.name' )
                            ->group_by( 'publicationstypes.id' )
                            ->cached( 30 )
            ;
        }

        $result = $query->execute();

        if( $result->count() == 0 )
            return array( );
        else
            return $result->as_array( 'id' );
    }

    static public function cat_types( $count_pubs = FALSE, $languages_id = NULL )
    {
        $query = DB::select( 'id', 'name', 'parent_id' )
                        ->from( 'shop_categories' )
        //->order_by( 'publicationstypes.name' )
        ;

//        if( $count_pubs )
//        {
//            $query = DB::select( 'publicationstypes.id', 'publicationstypes.name', 'publicationstypes.parents_path', 'publicationstypes.parent_id', 'publicationstypes.parent_url', 'publicationstypes.url', Db::expr( 'COUNT(publications.id) AS count' )
//                            )
//                            ->from( 'publicationstypes' )
//                            ->join( 'publications', 'left' )
//                            ->on( 'publicationstypes.id', '=', Db::expr( 'publications.publicationstypes_id AND publications.languages_id = ' . $languages_id )
//                            )
//                            ->order_by( 'publicationstypes.name' )
//                            ->group_by( 'publicationstypes.id' )
//
//            ;
//        }

        $result = $query->cached( 30 )->execute();

        if( $result->count() == 0 )
            return array( );
        else
            return $result->as_array( 'id' );
    }

    static public function toParentsMode( $lang_id = 1 )
    {
        $query = DB::select( 'id', 'parents_serialized' )
                        ->from( array( 'shop_categories', 'categories' ) )
                        ->where( 'parents_path', '=', '1,' )
                        ->execute()
        ;
        $return = array( );
        foreach( $query->as_array() as $item )
        {
//            $parents = Kohana::config( 'shop.root_category_id' ) . ',';
//            $parents_serialized = unserialize( $item['parents_serialized'] );
//            if( is_array( $parents_serialized ) && count( $parents_serialized ) )
//            {
//                foreach( $parents_serialized as $iitem )
//                {
//                    $parents .= $iitem['id'] . ',';
//                }
//            }
            $query = DB::select( 'id', 'parents_path' )
                            ->from( array( 'shop_categories', 'categories' ) )
                            ->where( 'id', '=', $item['parent_id'] )
                            ->execute()
                            ->current()
            ;
            DB::update( 'shop_categories' )->set( array( 'parents_path' => $query['parents_path'] . $item['id'] . ',' ) )->where( 'id', '=', $item['id'] )->execute();
//            $item['parents_path'] = $parents;
//            $return[] = $item;
        }

        return true;
    }
    
    /**
     * function update table shop_good from current id
     * 
     * @param int $id
     * @param array $array array update data
     */
    static function update( $id, $update = array() )
    {
        $totalrows = 0;
        $totalrows = DB::update('shop_goods')
        ->set($update)
        ->where('id','=',$id)
        ->execute()
        ;
        return $totalrows;
    }
    
    static public function itemExists( $id = NULL )
    {
        if( (int) $id )
        {
            $query = DB::select('mainGoodId')
                            ->from( 'shop_goodgroups' )
                            ->where('goodId','=',$id)
                            ->limit( 1 )
                            ->execute()
                    ;
            if( $query->count() > 0 ) return $query->get('mainGoodId');
            return false;
        }
        return false;
    }
}