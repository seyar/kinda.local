<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */
class Model_Recgoods extends Model
{
    static protected $_table = 'shop_recommended_goods';
    
  static function getAllGoods( $where = NULL, $as_array = TRUE )
  {
    $query = DB::select()
        ->from('shop_goods')
//            ->order_by('id')
//            ->cached(60)
        ;
    
    if( !empty($where) )
    {
        foreach($where as $key => $item)
            $query->and_where($key, '=', $item);
    }
    $result = $query->execute();

    if( $result->count() == 0 ) return array();
    else
    {
        if( $as_array ) return $result->as_array('id');
        else return $result->current();
    }
  }

  static function saveFromCat( $id, $data, $where = 'catId' )
  {        
      
      if( !is_array($data) ) return false;
        
      if( empty($data['recgoodid']) )
      {
          DB::delete(self::$_table)->where($where,'=', $id )->execute();
          return;
      }
      
      $goodIds = implode(',',  array_unique($data['recgoodid']));
      DB::delete(self::$_table)->where($where,'=', $id )->execute();
      list($insert_id, $total_rows) = DB::insert(self::$_table, array($where, 'goodIds') )
          ->values( array($id, $goodIds) )
          ->execute()
          ;

      return $insert_id;
  }

  static function getItem( $where = NULL, $frontendCall = FALSE )
  {
      if( !$where ) return array();
      $result = DB::select('shop_goods.*', array('c.name','category_name'), array('c.seo_url','category_url'), array('c.id','categories_id') )
        ->from( 'shop_goods')
        ->join(array('shop_categories', 'c'))
                    ->on('shop_goods.categories_id', '=', 'c.id')
          ->where( DB::expr('FIND_IN_SET(shop_goods.id, '), '', DB::expr('(SELECT goodIds FROM `'.self::$_table.'` WHERE '.$where.' LIMIT 1)) ') )
//          ->cached(60)
          ->execute()
      ;
      
/*
 * SELECT shop_recgoods.*, IF(catId=5234, 2, IF(catId = 5233,1, IF(catId = 1,1, 0))) AS catd_order FROM `shop_recgoods`WHERE `goodId` = 105914 OR `catId`IN (1,5233,5234)
ORDER BY goodId DESC, catd_order DESC
LIMIT 1
 */

      if( $result->count() == 0 ) return array();
      else
      {
          $price_categoty = Kohana::config('shop')->default_category;
          $curr = new Model_Frontend_Currencies();
          
          $resultArr = $result->as_array('id');
          $search = array('%23','%3F','%2F', '%26','.');
//        $replace = array('#','?','/','&');
          $replace = array('','','','','');
          $randomize = Kohana::config('shop')->recGoodsRandomize;

            foreach ($resultArr as $key=>$val)
            {
                $query = Db::select('p.price_categories_id', 'p.currencies_id', 'p.price')
                            ->from(array('shop_prices', 'p'))
                            ->where('p.good_id' , '=',  $val['id'])
                            ->order_by('p.currencies_id')
                            ->cached(30)
                    ;

                if (is_numeric($price_categoty) )

                    $query->where('p.price_categories_id', '=', (int)$price_categoty);

                $prices = array();
                $rows = $query->execute()->as_array();

                $val['prices'] =$curr->getPrice($rows);
                $val['seo_url'] = $val['seo_url'] ? str_replace($search,$replace,urlencode($val['seo_url'])) : Controller_Admin::to_url($val['name']);
                $val['category_url'] = $val['category_url'] ? str_replace($search,$replace,urlencode($val['category_url'])) : Controller_Admin::to_url($val['category_name']);
                
                if($randomize == 1 && $frontendCall )
                    $return[$val['categories_id']][] = $val;
                else
                    $return[ $val['id'] ] = $val;
            }

            if($randomize != 1 || !$frontendCall)
                return $return;
            
            $res = array();
            foreach( $return as $item )
            {
                if( count($item) > 1 )
                {
                    $key = array_rand( $item );
                    $res[] = $item[$key];
                }
                else
                    $res[] = $item[0];
            }
            
            return $res;
      }
  }

  static function getItemFront( $goodId = NULL, $catId = NULL )
  {      
    $cache = Cache_Sqlite::instance();
    $lifetime = 60; // in seconds
    $returnGood = array();
    // товар
    //может есть в sqlite кеше?
    $cachedArr = $cache->get('recGoods_good_' . $goodId);
    if( ! $cachedArr )
    {
        //находим товары у товара. и ретерним али есть
        if( !empty($goodId) && isset($goodId) )
        {
            $res = Model_Recgoods::getItem( 'goodId = '.$goodId, TRUE );

            if( count($res) != 0 ){
                // кладем в sqlite  кеш
                $cache->set(  'recGoods_good_' . $goodId
                            , $res
                            , $lifetime
                        );

                return $res;
            }
        }
    }else
        $returnGood = $cachedArr;

    // категория
    //может есть в sqlite кеше?
    unset($cachedArr);
    $cachedArr = $cache->get('recGoods_cat_' . $catId);
    if( ! $cachedArr )
    {
        if( isset($catId) && !empty($catId) )
        {
            // находим рек товары у категории.
            $res = Model_Recgoods::getItem( 'categories_id = '.$catId, TRUE );

            // кладем в кеш
            $cache->set( 'recGoods_cat_' . $catId
                        , $res
                        , $lifetime
                    );
            return $res;
        }
    }else
        $returnCat = $cachedArr;
    
    
    return count($returnGood) > 0 ? $returnGood : $returnCat;
  }
}
?>