<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_ShipMethods extends Model
{
    // Singleton static instance
    private static $_instance;

    public function setMethod($class){
        $class = "Model_Shipmethods_$class";
        return new $class;
    }

    public static function instance()
    {
            if (self::$_instance === NULL)
            {
                    // Create a new instance
                    self::$_instance = new self;
            }

            return self::$_instance;
    }
    
    public function show()
    {        
            $rows_per_page = ( isset($_POST['rows_num']) )
				? ((int)$_POST['rows_num'])
				: (int)cookie::get('rows_per_page', 10);

	    cookie::set('rows_per_page', $rows_per_page); $_POST['rows_num'] = $rows_per_page;

            $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
                        ->from('shop_shipment_methods')
                        ;

            $query = DB::select('*')
                        ->from('shop_shipment_methods')
                        ;

            $count = $query_c
		    ->execute()
		    ->get('mycount');

	    if (isset($_POST['page'])) $_GET['page'] = $_POST['page'];

	    $pagination = Pagination::factory(array(
		    'total_items'    => $count,
		    'items_per_page' => $rows_per_page,
	    ));

	    $result = $query
			    ->order_by('name','ASC')
			    ->limit($pagination->items_per_page)
			    ->offset($pagination->offset)
			    ->execute();

	    $page_links_str = '';
            
	    if ($result->count() == 0) return array(array(), '', array());
	    else
	    {
                $result = $result->as_array('id');

		return array( $result, $page_links_str, $pagination->total_pages );
	    }

    }

    public function delete($id = NULL)
    {
        $query = DB::delete('shop_shipment_methods');

        if (is_numeric($id))
	{
            $query->where('id', '=', $id); // ->limit(1)

	    $total_rows = $query->execute();
	}
    }

    public function delete_list($array = array())
    {
        $query = DB::delete('shop_shipment_methods');

        if (count($array))
	{
            $query->where('id', 'in', array_flip($array) ); // ->limit(1)

	    $total_rows = $query->execute();
	}
    }
    public function status($id = NULL)
    {
        $query = DB::update('shop_shipment_methods')->set(array(
            'status' => DB::expr('!status')
        ));

        if (is_numeric($id))
	{
            $query->where('id', '=', $id); // ->limit(1)

	    $total_rows = $query->execute();
	}
    }

    public function status_list($array = array())
    {
        $query = DB::update('shop_shipment_methods')->set(array(
            'status' => DB::expr('!status')
        ));

        if (count($array))
	{
            $query->where('id', 'in', array_flip($array) ); // ->limit(1)

	    $total_rows = $query->execute();
	}
    }

    static function getAllAvailableShipment( $listmain = NULL)
    {
        if($listmain)
        {
            $query = DB::select()
                ->from('shop_shipment_methods')
                ->where('fs_name','IN', DB::expr('("'.implode('","',$listmain).'")') )
                ->and_where('protected','=',1)
                ->execute()
                ;
            
            if($query->count() == 0 ) return array();
            $result = $query->as_array();
            return $result;
        }
        return false;
    }


    static public function getMainItem($where = NULL, $cond = '=', $as_array = FALSE)
    {        
        $query =
            DB::select()
            ->from('shop_shipment_methods')
//            ->execute()
            ;
        if($where)
        {
            foreach($where as $key => $item)
            {
                $query->where($key,$cond, $item);
            }
        }        
        $result = $query->execute();

        if( $result->count() == 0 ) return array();
        else{
            if( $as_array )
                $return = $result->as_array();
            else
                $return = $result->current();
            return $return;
        }
    }
    
    static public function getList( $where = array(), $as_array = TRUE )
    {
        $query = DB::select()
                ->from('shop_shipment_methods')
            ;
        
        if( !empty($where) )
        {
            foreach($where as $item)
                $query->where( $item[0], $item[1], $item[2] );
        }
        $result = $query->execute();

        $return = array();
        if ($result->count() == 0) return $return;
        else
        {
            if( $as_array )
                $return = $result->as_array();
            else
            $return = $result->current();
        }
        return $return;
    }
    
    static public function update( $id = NULL, $data)
    {
        if ( $id && $data )
        {
            DB::update('shop_shipment_methods')
                    ->set(
                        $data
                    )
                    ->where('id', '=', $id)
                    ->execute()
                    ;
            return true;
        }
        else
            return false;
        
    }
    
    public function updateOrderId( $data = array() )
    {
        if( !empty($data) ):
        foreach( $data as $key => $item )
        {
            if( !self::update( $item, array( 'orderid'=> $key )) )
                    return false;
        }
        endif;
        return TRUE;
    }
}