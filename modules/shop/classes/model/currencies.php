<?php defined( 'SYSPATH' ) OR die('No direct access allowed.');

class Model_Currencies extends Model
{

    static function init()
    {
        $id = isset( $id ) && !empty( $id ) ? $id : 1;

        Kohana_Controller_Quicky::$intermediate_vars['shop_currencies'] = Model_Currencies::load( $id );
    }

    public function show()
    {
        $rows_per_page = ((int) $_POST['rows_num']) ? ((int) $_POST['rows_num']) : (int) cookie::get( 'rows_per_page_' . MODULE_NAME . '_' . MODULE_SUBCONTROLLER, 10 );

        cookie::set( 'rows_per_page_' . MODULE_NAME . '_' . MODULE_SUBCONTROLLER, $rows_per_page );
        $_POST['rows_num'] = $rows_per_page;

        $query_c = DB::select( DB::expr( 'COUNT(*) AS mycount' ) )
                        ->from( 'shop_currencies' )
        ;

        $query = DB::select( '*' )
                        ->from( 'shop_currencies' )
        ;

        $count = $query_c
                        ->execute()
                        ->get( 'mycount' );

        if( isset( $_POST['page'] ) )
            $_GET['page'] = $_POST['page'];

        $pagination = Pagination::factory( array(
                    'total_items' => $count,
                    'items_per_page' => $rows_per_page,
                ) );

        $result = $query
                        ->order_by( 'name', 'ASC' )
                        ->limit( $pagination->items_per_page )
                        ->offset( $pagination->offset )
                        ->execute();

        $page_links_str = '';

        if( $result->count() == 0 )
            return array( array( ), '', array( ) );
        else
        {
            $result = $result->as_array();

            return array( $result, $page_links_str, $pagination->total_pages );
        }
    }

    public function delete( $id = NULL )
    {
        $query = DB::delete( 'shop_currencies' );

        if( is_numeric( $id ) )
        {
            $query->where( 'id', '=', $id ); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function delete_list( $array = array( ) )
    {
        $query = DB::delete( 'shop_currencies' );

        if( count( $array ) )
        {
            $query->where( 'id', 'in', array_flip( $array ) ); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    static public function load( $id = NULL )
    {
        if( (int) $id )
        {
            $query = DB::select()
                            ->from( 'shop_currencies' )
                            ->limit( 1 )
                            ->cached(3600)
                    ;
            
            $query->where( 'id', '=', $id );

            $result = $query->execute();

            if( $result->count() == 0 )
                return array( );
            else
            {
                $return = $result->current();

                return $return;
            }
        }else
        {
            return array( );
        }
    }

    public function save( $lang_id = 1, $id = NULL )
    {
        if( $post = $this->validate_save() )
        {
            if( (int) $id )
            { // update
                DB::update( 'shop_currencies' )
                        ->set(
                                array(
                                    'name' => $post['name'],
                                    'code' => $post['code'],
                                    'iso_code' => $post['iso_code'],
                                    'rate' => $post['rate'],
                                    'rate_w_vat' => $post['rate_w_vat'],
                                )
                        )
                        ->where( 'id', '=', $id )
                        ->execute();
            }else
            { // create
                DB::insert( 'shop_currencies', array(
                            'name', 'code', 'iso_code', 'rate', 'rate_w_vat'
                                )
                        )
                        ->values(
                                array(
                                    $post['name'],
                                    $post['code'],
                                    $post['iso_code'],
                                    $post['rate'],
                                    $post['rate_w_vat'],
                                )
                        )
                        ->execute();
            }
            /* clean cache*/
            DB::select()
                ->from( 'shop_currencies' )
                ->limit( 1 )
                ->cached(-1)
                ->execute()
            ;

            return true;
        }else
        {
            return false;
        }
    }

    public function validate_save()
    {
        $keys = array(
            'name',
            'code',
            'iso_code',
            'rate',
            'rate_w_vat',
        );

        $params = Arr::extract( $_POST, $keys, '' );
        $params['rate'] = str_replace( ',', '.', $params['rate'] );

        $post = Validate::factory( $params )
                        ->rule( 'name', 'not_empty' )
                        ->rule( 'code', 'not_empty' )
                        ->rule( 'iso_code', 'digit' )
                        ->rule( 'rate', 'not_empty' )
                        ->rule( 'rate', 'regex', array( "/^[0-9]+((\.|,)[0-9]{1,10})?$/" ) )
        ;

        if( $post->check() )
        {
            return $params;
        }else
        {
            $this->errors = $post->errors( 'validate' );
        }
    }

    /**
     * function update prices table for good
     * 
     * @param int $id
     * @param array $keys prices, oldprices
     * @param array $post $_POST data
     */
    static function prices_update( $good_id = 0, $key = 'price', $post = array( ) )
    {
        
        if( $good_id == 0 )
            return false;
        if( count( $post ) == 0 )
            return false;

        if( isset( $post[$key] ) && count( $post[$key] ) > 0 )
        {
            foreach( $post[$key] as $kkey => $value ) // for each price value
            {
                foreach( $value as $p_key => $p_value )
                {
                    // load current price
                    $result = DB::select()
                                    ->from( 'shop_prices' )
                                    ->limit( 1 )
                                    ->where( 'good_id', '=', $good_id )
                                    ->and_where( 'price_categories_id', '=', $kkey )
                                    ->and_where( 'currencies_id', '=', $p_key )
                                    ->execute()
                                    ->current()
                    ;

                    if( $result )
                    {
                        Db::update( 'shop_prices' )
                                ->set( array( $key => $p_value ) )
                                ->where( 'id', '=', $result['id'] )
                                ->execute()
                        ;
                    }else
                    {
                        Db::insert( 'shop_prices' )
                                ->columns(
                                        array(
                                            'good_id'
                                            , 'price_categories_id'
                                            , 'currencies_id'
                                            , $key
                                        )
                                )
                                ->values(
                                        array(
                                            $good_id
                                            , $kkey
                                            , $p_key
                                            , $post[$key][$kkey][$p_key] // price value
                                        )
                                )
                                ->execute()
                        ;
                    }
                }
            }
        }
    }

    /**
     * function get price categories
     * 
     * @return array price categories
     */
    static function getPriceCategories( $where = array( ), $asArray = TRUE )
    {

        $query = DB::select()->from( 'shop_price_categories' )
        ;

        if( isset( $where ) && count( $where ) > 0 )
        {
            foreach( $where as $item )
            {
                $query->where( $item[0], $item[1], $item[2] );
            }
        }
        $return = array( );
        $result = $query->execute();
        if( $result->count() == 0 )
            $return = array( );
        else
        {
            if( $asArray === TRUE )
                $return = $result->as_array();
            else
                $return = $result->current();
        }
        return $return;
    }

    public function getPrice( $prices, $key = 'price' )
    {
        $defValue = 0;
        /**/
        $defaultCurrency = Kohana::config('shop.default_currency');
        $currencies = DB::select()
                            ->from( 'shop_currencies' )
                            //->cached('120')
                            ->execute()
                            ->as_array( 'id' )
                    ;
        /**/
        
        foreach( $prices as $price )
        {
            if( $price['currencies_id'] === $defaultCurrency )
                $defValue = $price[$key];
        }

        reset( $prices );
        $ret = array( );

        foreach( $prices as $price )
        {
            foreach( $currencies as $currency )
            {
                if( $price['currencies_id'] !== $currency['id'] )
                {
                    $ret[$price['price_categories_id']][$currency['id']] =
                            $price[$key] /
                            (
                            $currencies[$defaultCurrency]['rate'] /
                            $currency['rate']
                            );
                }
                else
                    $ret[$price['price_categories_id']][$currency['id']] = $price[$key];
            }
        }
        return $ret;
    }
}