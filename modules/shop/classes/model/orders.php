<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orders extends Model
{

    static public function statuses()
    {
        $return = array();

        foreach (Controller_Orders::$order_statuses as $key => $value)
        {
            $return[$key] = I18n::get($value);
        }

        return $return;
    }

    public static  function load_medthods()
    {
        $query_a = DB::select('id','name')->from('shop_payment_methods');
        $query_a = $query_a->execute()->as_array('id','name');

        $query_b = DB::select('id','name')->from('shop_shipment_methods');
        $query_b = $query_b->execute()->as_array('id','name');

        //return(array($query_a,$query_b));
        return array($query_a,$query_b);

    }

    public function show()
    {
        $rows_per_page = ((int) $_POST['rows_num']) ? ((int) $_POST['rows_num']) : (int) cookie::get('rows_per_page_' . MODULE_NAME . '_' . MODULE_SUBCONTROLLER, 10);

        cookie::set('rows_per_page_' . MODULE_NAME . '_' . MODULE_SUBCONTROLLER, $rows_per_page);
        $_POST['rows_num'] = $rows_per_page;

        $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
                        ->from(array('shop_orders', 'orders'))
                        ->join(array('shop_customers', 'customers'))
                        ->on('customers.id', '=', 'orders.customer_id')
                        ->where(Db::expr('1'), '=', 1)
        ;

        $query = DB::select('*')
                        ->from(array('shop_orders', 'orders'))
                        ->where(Db::expr('1'), '=', 1)
        ;

        ////////////////////////////
        // begin filters
        ////////////////////////////

        $param = (isset($_POST['search_order_id']) ? $_POST['search_order_id'] : Session::instance()->get('search_order_id', '') );

        Session::instance()->set('search_order_id', $param);
        $_POST['search_order_id'] = $param;

        if ($_POST['search_order_id'])
        {
            $query_c->and_where('orders.id', '=', (int) $_POST['search_order_id']);
            $query->and_where('orders.id', '=', (int) $_POST['search_order_id']);
        }


        $param = (isset($_POST['search_order_customer_id']) ? $_POST['search_order_customer_id'] : Session::instance()->get('search_order_customer_id', '') );

        Session::instance()->set('search_order_customer_id', $param);
        $_POST['search_order_customer_id'] = $param;

        if ($_POST['search_order_customer_id'])
        {
            $query_c->and_where('orders.customer_id', '=', (int) $_POST['search_order_customer_id']);
            $query->and_where('orders.customer_id', '=', (int) $_POST['search_order_customer_id']);
        }


        $param = (isset($_POST['search_order_customer_name']) ? $_POST['search_order_customer_name'] : Session::instance()->get('search_order_customer_name', '') );

        Session::instance()->set('search_order_customer_name', $param);
        $_POST['search_order_customer_name'] = $param;

        if ($_POST['search_order_customer_name'])
        {
            $query_c->where(Db::expr('LCASE(orders.shipment_name)'), 'LIKE', "%" . mb_strtolower($_POST['search_order_customer_name'], 'UTF-8') . "%")
            ;
            $query->where(Db::expr('LCASE(orders.shipment_name)'), 'LIKE', "%" . mb_strtolower($_POST['search_order_customer_name'], 'UTF-8') . "%")
            ;
        }


        $param = (isset($_POST['search_order_date_create']) ? $_POST['search_order_date_create'] : Session::instance()->get('search_order_date_create', '') );

        Session::instance()->set('search_order_date_create', $param);
        $_POST['search_order_date_create'] = $param;

        if ($_POST['search_order_date_create'])
        {
            $query_c->and_where(Db::expr('DATE(orders.date_create)'), '=', $_POST['search_order_date_create']);
            $query->and_where(Db::expr('DATE(orders.date_create)'), '=', $_POST['search_order_date_create']);
        }


        $param = (isset($_POST['search_order_sum_from']) ? $_POST['search_order_sum_from'] : Session::instance()->get('search_order_sum_from', '') );

        Session::instance()->set('search_order_sum_from', $param);
        $_POST['search_order_sum_from'] = $param;

        if ($_POST['search_order_sum_from'])
        {
            $query_c->and_where('orders.sum', '>=', (float) $_POST['search_order_sum_from']);
            $query->and_where('orders.sum', '>=', (float) $_POST['search_order_sum_from']);
        }


        $param = (isset($_POST['search_order_sum_to']) ? $_POST['search_order_sum_to'] : Session::instance()->get('search_order_sum_to', '') );

        Session::instance()->set('search_order_sum_to', $param);
        $_POST['search_order_sum_to'] = $param;

        if ($_POST['search_order_sum_to'])
        {
            $query_c->and_where('orders.sum', '<=', (float) $_POST['search_order_sum_to']);
            $query->and_where('orders.sum', '<=', (float) $_POST['search_order_sum_to']);
        }


        $param = (isset($_POST['search_order_pay_method']) ? (int) $_POST['search_order_pay_method'] : Session::instance()->get('search_order_pay_method', '') );

        Session::instance()->set('search_order_pay_method', $param);
        $_POST['search_order_pay_method'] = $param;

        if ($_POST['search_order_pay_method'])
        {
            $query_c->and_where('orders.payment_method_id', '=', (int) $_POST['search_order_pay_method']);
            $query->and_where('orders.payment_method_id', '=', (int) $_POST['search_order_pay_method']);
        }


        $param = (isset($_POST['search_order_ship_method']) ? (int) $_POST['search_order_ship_method'] : Session::instance()->get('search_order_ship_method', '') );

        Session::instance()->set('search_order_ship_method', $param);
        $_POST['search_order_ship_method'] = $param;

        if ($_POST['search_order_ship_method'])
        {
            $query_c->and_where('orders.shipment_method_id', '=', (int) $_POST['search_order_ship_method']);
            $query->and_where('orders.shipment_method_id', '=', (int) $_POST['search_order_ship_method']);
        }


        $param = (isset($_POST['search_order_status']) ? (int) $_POST['search_order_status'] : Session::instance()->get('search_order_status', '') );

        Session::instance()->set('search_order_status', $param);
        $_POST['search_order_status'] = $param;

        if ($_POST['search_order_status'])
        {
            $query_c->and_where('orders.status', '=', (int) $_POST['search_order_status']);
            $query->and_where('orders.status', '=', (int) $_POST['search_order_status']);
        }

        ////////////////////////////
        // end filters
        ////////////////////////////

        $count = $query_c
                        ->execute()
                        ->get('mycount');

        if (isset($_POST['page']))
            $_GET['page'] = $_POST['page'];

        $pagination = Pagination::factory(array(
                    'total_items' => $count,
                    'items_per_page' => $rows_per_page,
                ));

        $result = $query
                        ->order_by('id', 'DESC')
                        ->limit($pagination->items_per_page)
                        ->offset($pagination->offset)
                        ->execute();

        $page_links_str = '';

        if ($result->count() == 0)
            return array(array(), '', array());
        else
        {
            $result = $result->as_array();

            return array($result, $page_links_str, $pagination->total_pages);
        }
    }

    public function delete($id = NULL)
    {
        $query = DB::delete('shop_orders');

        if (is_numeric($id))
        {
            $query->where('id', '=', $id); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function delete_list( $array = array() )
    {
        $query = DB::delete('shop_orders');

        if (count($array))
        {
            $query->where('id', 'in', array_flip($array)); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    static public function load($id = NULL)
    {
        if ((int) $id)
        {
            $query = DB::select()
                            ->from('shop_orders')
                            ->limit(1);

            $query->where('id', '=', $id);

            $result = $query->execute();

            if ($result->count() == 0)
                return array();
            else
            {
                $return = $result->current();

                $return['sum_text'] =
                        Helper_Num::written_number((int) floor($return['sum']), TRUE) . ' ' .
                        Helper_Num::decl(floor($return['sum']), array('гривна', 'гривны', 'гривен'), TRUE);

                $kop = (float) $return['sum'] - floor($return['sum']);
                if ($kop > 0)
                {
                    $return['sum_text'] .= ' ' . number_format($kop * 100, 0, '.', '') . ' коп.';
                }

                $return['ser_status_history'] = unserialize($return['ser_status_history']);
                $return['ser_details'] = unserialize($return['ser_details']);
                
                return $return;
            }
        }
        else
        {
            return array();
        }
    }

    public function save($lang_id = 1, $id)
    {
        if (!$post = Model_Orders::validate_save())
            return false;

        $order = $this->load($id);

        $shipment_address_db = Model_Customers::shipment_address_list($order['customer_id']);
        
        $shipment_address = $shipment_address_db[$post['shipment_address_id']]['country'] . ', '
                . $shipment_address_db[$post['shipment_address_id']]['city'] . ', '
                . $shipment_address_db[$post['shipment_address_id']]['zip'] . ', '
                . $shipment_address_db[$post['shipment_address_id']]['address_line1'];

        $shipment_name = $shipment_address_db[$post['shipment_address_id']]['name'];
        $billing_address_db = Model_Customers::billing_address_list($order['customer_id']);

        $billing_address = $shipment_address;
        $billing_name = $shipment_name;
//        $billing_address = $billing_address_db[$post['billing_address_id']]['country'] . ', '
//                . $billing_address_db[$post['billing_address_id']]['city'] . ', '
//                . $billing_address_db[$post['billing_address_id']]['zip'] . ', '
//                . $billing_address_db[$post['billing_address_id']]['address_line1'];

//        $billing_name = $billing_address_db[$post['billing_address_id']]['name'];

        $update = array(
            'shipment_method_id' => $post['payment_method_id'],
            'payment_method_id' => $post['payment_method_id']
        );
        if ($post['shipment_address_id'])
        {
            $update['shipment_name'] = $shipment_name;
            $update['shipment_address'] = $shipment_address;
        }
        if ($post['billing_address_id'])
        {
            $update['billing_name'] = $billing_name;
            $update['billing_address'] = $billing_address;
        }

        DB::update('shop_orders')
                ->set($update)
                ->where('id', '=', $id)
                ->execute();

        return 1;
    }

    static function validate_save()
    {
        $keys = array(
            'shipment_address_id',
            'billing_address_id',
            'payment_method_id',
            'shipment_method_id',
            'status',
            'alert'
        );

        $params = Arr::extract($_POST, $keys, '');

        $post = Validate::factory($params)
                        ->rule('shipment_address_id', 'not_empty')
                        ->rule('billing_address_id', 'not_empty')
                        ->rule('payment_method_id', 'digit')
                        ->rule('shipment_method_id', 'digit')
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            $this->errors = $post->errors('validate');
        }
    }

    public function delete_good($id = NULL)
    {
        if (is_numeric($id) && isset($_GET['good_id']))
        {
            $query = DB::select()
                            ->from('shop_orders')
                            ->where('id', '=', $id)
                            ->limit(1)
            ;

            $result = $query->execute();

            if ($result->count() == 0)
            {
                echo 0;
            }
            else
            {
                $return = $result->current();

                $details = unserialize($return['ser_details']);

                unset($details[$_GET['good_id']]);

                $i = 1;
                $details_new = array();
                foreach ($details as $value)
                {
                    $details_new[$i] = $value;
                    $i++;
                }

                $query = DB::update('shop_orders')
                                ->set(
                                        array(
                                            'ser_details' => serialize($details),
                                        )
                                )
                                ->where('id', '=', $id)
                                ->execute()
                ;

                echo 1;
            }
        }

        exit();
    }

    public function find_goods($field = NULL, $search = NULL)
    {
        if (!$field || !$search || !in_array($field, array('scu', 'name')))
            return false;

        $query = DB::select('scu', 'name', 'alter_name', 'id')
                        ->from('shop_goods')
                        ->limit(10)
        ;
        $query->where($field, 'LIKE', DB::expr("'$search%'"));

        $result = $query->execute();

        if ($result->count() == 0)
        {
            echo 'No result';
        }
        else
        {
            foreach ($result->as_array() as $value)
            {
                echo $value[$field] . "|{$value['id']}\n";
            }
        }
        return true;
    }

    public function update_good($id = NULL)
    {
        if ((int) $id)
        {
            $query = DB::select()
                            ->from('shop_orders')
                            ->limit(1);

            $query->where('id', '=', $id);

            $result = $query->execute();

            if ($result->count())
            {
                $order = $result->current();

                $order['ser_details'] = unserialize($order['ser_details']);

                foreach ($_POST['count'] as $key => $val)
                {
                    $order['ser_details'][$key]['count'] = $val;
                    $order['ser_details'][$key]['price'] = $_POST['price'][$key];
                    $order['ser_details'][$key]['discount'] = $_POST['discount'][$key];
                    $order['ser_details'][$key]['sum'] =
                            $order['ser_details'][$key]['count'] *
                            ($order['ser_details'][$key]['price'] - $order['ser_details'][$key]['discount']);
                }

                if (isset($_POST['new_scu']))
                {
                    foreach ($_POST['new_scu'] as $key => $val)
                    {
                        $order['ser_details'][] = array(
                            'scu' => $_POST['new_scu'][$key],
                            'name' => $_POST['new_name'][$key],
                            'count' => (int) $_POST['new_count'][$key],
                            'price' => (float) $_POST['new_price'][$key],
                            'discount' => (float) $_POST['new_discount'][$key],
                            'sum' => ((float) $_POST['new_price'][$key] - (float) $_POST['new_discount'][$key]) * (int) $_POST['new_count'][$key],
                        );
                    }
                }

                $subtotal = $discount = $sum = 0;

                foreach ($order['ser_details'] as $value)
                {
                    $subtotal += $value['count'] * $value['price'];
                    $discount += $value['count'] * $value['discount'];
                    $sum += $value['count'] * ( $value['price'] - $value['discount']);
                }

                Db::update("shop_orders")
                        ->set(
                                array(
//                                    'total_cost' => (float)$total_cost, // TODO: recalc total order cost
                                    'subtotal' => (float) $subtotal,
                                    'discount' => (float) $discount,
                                    'sum' => (float) $sum,
                                    'ser_details' => serialize($order['ser_details']),
                                )
                        )
                        ->where('id', '=', $id)
                        ->execute();
            }
        }

        echo '1';
    }

    public function change_status($id = NULL, $language = NULL)
    {
        if( is_numeric($id) && isset($_POST['status']) )
        {
            $query = DB::select()
                            ->from('shop_orders')
                            ->where('id', '=', $id)
                            ->limit(1)
            ;

            $result = $query->execute();

            if ($result->count() == 0)
            {
                echo 0;
            }
            else
            {
                $return = $result->current();

                $status_history = unserialize($return['ser_status_history']);
                
                $status_history[] = array(
                    'date' => time(),
                    'status' => Arr::get($_POST, 'status'),
                    'alert' => Arr::get($_POST, 'status'),
                    'comment' => Arr::get($_POST, 'comment'),
                );
                
                if ($_POST['status']) // send message
                {
                   $customer = DB::select()
                                ->from('shop_customers')
                                ->where('id', '=', $return['customer_id'])
                                ->limit(1)
                                ->execute()
                                ->current()
                            ;


                    $result = $query->execute();
                    
                    $langs = Model_Content::get_languages_array();
                    $order = Model_Orders::load($id);
                    $payment_method = Model_PayMethods::instance()->load( $order['payment_method_id'] );
                    $discount = 0;
                    
                    $message = include(Kohana::find_file('views', 'status_change_' . $langs[$language]['prefix']));
                    
                    $customer['email'] = Kohana::config('shop.quickorderUserId') == $return['customer_id'] ? self::getEmailFromComment( $status_history[0]['comment'] ) : trim($customer['email']);
                    $to = Kohana::config('shop.sendEmailToDeveloper') == 1 ? array( 'to'=> $customer['email'], 'bcc'=>Kohana::config('shop.EmailDeveloper') ) : $customer['email'];
                    
                    if(! Email::send(
                            $to,	// To
                            array( Kohana::config('shop')->send_from, Kohana::config('shop')->send_from_title ),	// From
							i18n::get('Order # ') .' '. $id. ' / '.I18n::get('Order Status Changed'),
                            $message,	// Message
                            TRUE
                        )
                    ){
                                Kohana_Log::instance()->add('email send error',
                                        'From '.Kohana::config('shop')->send_from .' <'. Kohana::config('shop')->send_from_title.'>'.
                                        '. To '.trim($customer['email']).
                                        '. Subject '.Kohana::config('shop')->subject . $id. ' / '.I18n::get('Order Status Changed').
                                        '. Message '.$message
                                    );
                    }
				}

                $query = DB::update('shop_orders')
                                ->set(
                                        array(
                                            'ser_status_history' => serialize($status_history),
                                            'status' => $_POST['status'],
                                            'date_close' => Db::expr('NOW()'),
                                        )
                                )
                                ->where('id', '=', $id)
                                ->execute()
                ;

                echo 1;
            }
        }
        exit();
    }
    
    /**
     * Function parse string and return email
     * 
     * @param string input $str
     * @return string email 
     */
    static function getEmailFromComment( $string = '' )
    {
        preg_match('|([^\s]+)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)|Uis', $string, $match);
        if( isset($match[1]) && isset($match[2]) && isset($match[3]) )
            return $match[1].'@'.$match[2].'.'.$match[3]; 
        return '';
    }
    
    static public function getFilter()
    {
        $where = array();
        
        $filter = (array) json_decode( Cookie::get( 'filter_' . 'orders', '' ), TRUE );

        if( isset($_POST['status']) && $_POST['status'] <> '-1' )
            $_POST['filter']['status'] = $_POST['status']; // because custom select not working with []
        
        if( isset($_POST['payment_methods']) and $_POST['payment_methods'] <> '-1' )
            $_POST['filter']['payment_methods'] = $_POST['payment_methods']; // because custom select not working with []
        
        if( isset($_POST['shipment_methods']) and $_POST['shipment_methods'] <> '-1' )
            $_POST['filter']['shipment_methods'] = $_POST['shipment_methods']; // because custom select not working with []

        if( isset( $_POST['filter'] ) )
        {
            $filter = $_POST['filter'];
            Cookie::set( 'filter_' . MODULE_NAME, json_encode( $_POST['filter'] ) );
        }

        $where = array( );
        if( count( $filter ) > 0 ):
            foreach( $filter as $key => $item )
            {
                if( !empty( $item ) )
                {
                    switch( $key )
                    {
                        case 'sum_do':
                            $where[] = array('sum', '<=', $item );
                            break;
                        case 'sum_ot':
                            $where[] = array('sum', '>=', $item );
                            break;

                        case 'shipment_name':
                            $where[] = array( 'shipment_name', 'LIKE', "%$item%" );
                            break;
                        case 'name':
                            $where[] = array( 'name', 'LIKE', "%$item%" );
                            break;
                        case 'shop_col_ot':
                            $where[] = array( 'quantity', '>=', $item );
                            break;
                        case 'shop_col_do':
                            $where[] = array( 'shop_prices', '<=', $item );
                            break;
                        case 'shop_price_ot':
                            $where[] = array( 'shop_prices', '>=', $item );
                            break;
                        case 'shop_price_do':
                            $where[] = array( 'shop_prices', '<=', $item );
                            break;

                        default:
                            $where[] = array( $key, '=', $item );
                    }
                }
            }
        endif;

        return array( $where, $filter );
    }

}
