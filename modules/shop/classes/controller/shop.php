<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Shop extends Controller_AdminModule
{

//    public $template		= 'shop_front.tpl'; //было
    public $template = 'shop';
    public $module_title = 'Shop';
    public $module_name = 'Shop';
    public $module_desc_short = 'Shop Descr Short';
    public $module_desc_full = 'Shop Descr Full';
    static $default_currency;
    static $active_currencies;
    static $root_category_id;

    public function before()
    {        
        parent::before();
        $this->model = new Model_Shop();
        Controller_Shop::$default_currency = Kohana::config('shop')->default_currency;
        Controller_Shop::$active_currencies = Kohana::config('shop')->active_currencies;
        Controller_Shop::$root_category_id = Kohana::config('shop')->root_category_id;
    }

    public function action_index()
    {
        //die('get tops');
    }

    public function action_panel()
    {
        Db::update('modules')->set(
                        array(
                            'panel_serialized' => serialize(
                                    array(
                                        'info' => array('name' => 'Shop',
                                            'link' => ADMIN_PATH . 'modules/shop',
                                            'image' => ADMIN_PATH . 'modules/shop/logo'
                                        ),
                                        'links' => array(
                                            array('name' => 'Goods', 'value' => ':goods'),
                                            array('name' => 'Customers', 'value' => ':customers'),
                                            array('name' => 'Orders', 'value' => ':orders'),
                                            array('name' => 'Categories', 'value' => ':categories'),
                                            array('name' => 'Brands', 'value' => ':brands'),
                                            array('name' => 'Currencies', 'value' => ':currencies'),
                                            array('name' => 'Payment Methods', 'value' => ':paymethods'),
                                            array('name' => 'Shipment Methods', 'value' => ':shipmethods'),
                                            array('name' => 'Shop Mail', 'value' => ':shopmail'),
                                        ),
                                    )
                            ), // serialize
                        ) // array
                )
                ->where('fs_name', '=', MODULE_NAME)
                ->execute();
        exit();
    }

    public function action_logo()
    {
        // Create the filename
        $file = Kohana::find_file('media', 'shop', 'png');

        if (!is_file($file))
        {
            throw new Kohana_Exception('Image does not exist');
        }

        //
        // Check your permissions here
        //
        // Set the mime type
        $this->request->headers['Content-Type'] = File::mime($file);
        $this->request->headers['Content-length'] = filesize($file);

        // Send the set headers to the browser
        $this->request->send_headers();

        // Send the file
        $img = @ fopen($file, 'rb');
        if ($img)
        {
            fpassthru($img);
            exit;
        }
    }

    static public function getUrlById($id)
    {
        return "/shop/_{$id}.html";
    }

}