<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Customers extends Controller_Shop {

  //  public $template		= 'customers_list.tpl';
    public $template		= 'shop_clients';


    public $module_title	= 'Customers';
    public $module_desc_short   = 'Shop Descr Short';
    public $module_desc_full	= 'Shop Descr Full';

    public static $customer_statuses   = array('0'=>'Blocked', '1'=>'Active');

    public function before()
    {

        parent::before();
	$this->model = new Model_Customers();
        //$this->request->controller .= ':'.MODULE_SUBCONTROLLER;

    }

    public function action_index()
    {
        $this->template = View::factory('shop_clients');
        $rows = $current_category = array(); $page_links = ''; $total_pages = 0;

        $rows = array();
        $page_links = '';
        $total_pages = 0;

        list( $where, $filter ) = Model_Customers::getFilter();
        
        list($this->template->pagination, $this->template->rows) = Admin::model_pagination('', 'shop_customers_list',
                        array('id','name','email' ,'phone','date_create','discount','status','turnover','`group`'),
                        $where
        );
    }

    public function action_active()
    {
	if ($this->request->param('id'))
	    $this->model->active( $this->request->param('id') );

        $this->redirect_to_controller($this->request->controller);
    }

    public function action_delete()
    {
        $reg = new Model_Registration();
        
        if ($this->request->param('id'))
        {
            $this->model->delete( $this->request->param('id') );
            if( method_exists( $reg, 'delete' ) )
                $reg->delete($this->request->param('id'));
        }

        elseif (isset($_POST['chk']) && count($_POST['chk']))
        {
            $this->model->delete_list( $_POST['chk'] );
            if( method_exists( $reg, 'delete' ) )
                $reg->delete( $_POST['chk'] );
        }

        $this->redirect_to_controller($this->request->controller);
    }

    public function action_edit()
    {
        $this->template = View::factory('shop_clients_edit');
        
        $this->template->obj = $this->model->load( $this->request->param('id') );
        
        $this->template->customer_shipment_addresses = Model_Customers::shipment_address_list($this->request->param('id'));
//        $this->template = View::factory('shop_paymentoptions_edit');
//        $this->template->obj = $this->model->load( $this->request->param('id') );

    }

    public function update()
    {
//$this->request->param('id')
//die(Kohana::debug($_POST));
        $id =  $this->request->param('id');
        //die(Kohana::debug($id));
        if ($id>0){
        $data = array(
                    
                    'name'      => ($_POST['name']),
                    'email'     => ($_POST['email']),
                    'phone'     => ($_POST['phone']),
                    'discount'  => floatval($_POST['discount']),
                    //'is_taxpayer' => intval($_POST['is_taxpayer']),
                    //'news_subscribe' => intval($_POST['news_subscribe']),
                );
        //$data['is_taxpayer']=1;
        //die(Kohana::debug($data));
        if (!isset($_POST['is_taxpayer'])){
            //die('222');
            $data['is_taxpayer']=0;
        }
        else {
            $data['is_taxpayer']=1;
        }
        if (!isset($_POST['news_subscribe'])){
            $data['news_subscribe']=0;
        }
        else {
            $data['news_subscribe']=1;
        }

                Model_Customers::update($data, $id);
//        $id = intval($_POST['id']);
//        if($data['name'] == '' || $data['email'] == '' || $id == 0) return;
//        $childModule = Kohana::config('registration')->get('child_module');

//        $res = call_user_func( array($childModule[0], 'update') , $data, $id);
//        Model_RegistrationFrontend::update1(
//                                        array(
//                                            'name' => $data['name'],
//                                            'email' => $data['email']
//                                        ),
//                                        $id
//                                    );
        //return $data;
        }
        else {
            $this->create();
        }


    }
    public  function create(){
        $data = array(
                    'price_category_id' => '1',
                    'name'      => ($_POST['name']),
                    'email'     => ($_POST['email']),
                    'phone'     => ($_POST['phone']),
                );

     //die(Kohana::debug($data));
     $id =  Model_Customers::create($data);
        
    }

    public function action_apply()
    {
        $this->update();
        $this->redirect_to_controller($this->request->controller.'/'.$this->request->param('id').'/edit');
    }

    public function action_save()
    {
        $this->update();
        $this->redirect_to_controller($this->request->controller);
    }

    public function action_statusUpdate()
    {
        $id = $this->request->param('id');

        Model_Customers::changeStatus($id);
        
        // Childs modules proccessing (Shop, ...etc)
        if(Kohana::config('shop')->get('parent_module'))
        {
            $res = call_user_func( array(Kohana::config('shop.parent_module'), Kohana::config('shop.parent_method') ), $id);
        }

        die('1');
    }

}