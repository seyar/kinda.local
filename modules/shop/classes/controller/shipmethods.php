<?php
defined( 'SYSPATH' ) OR die( 'No direct access allowed.' );

class Controller_ShipMethods extends Controller_Shop
{

    public $template = 'shipmethods_list';
    public $module_title = 'Shipment Methods';
    public $module_desc_short = 'Shop Descr Short';
    public $module_desc_full = 'Shop Descr Full';
    public $shipMethods = array( );
    public $main_shipments = array( );

    public function before()
    {
        parent::before();
        $this->model = new Model_ShipMethods;
        //$this->request->controller .= ':' . MODULE_SUBCONTROLLER;
        list($this->shipMethods) = $this->model->show();
        $this->main_shipments = Kohana::config( 'shop' )->main_shipments;
    }

    public function action_index()
    {
        $rows = $current_category = array();
        $page_links = '';
        $total_pages = 0;
        list($rows, $page_links, $total_pages) = $this->model->show();

        $this->template->rows = $rows;
        $this->template->pages_list = $page_links;
        $this->template->total_pages = $total_pages;
    }

    public function action_delete()
    {
        if( $this->request->param( 'id' ) )
            $this->model->delete( $this->request->param( 'id' ) );

        elseif( count( $_POST['chk'] ) )
            $this->model->delete_list( $_POST['chk'] );

        $this->redirect_to_controller( $this->request->controller );
    }

    public function action_status()
    {
        if( $this->request->param( 'id' ) )
            $this->model->status( $this->request->param( 'id' ) );

        elseif( count( $_POST['chk'] ) )
            $this->model->status_list( $_POST['chk'] );

        $this->redirect_to_controller( $this->request->controller );
    }

    public function action_add()
    {        
        $main_shipments = Model_ShipMethods::getAllAvailableShipment( $this->main_shipments );        
        $this->template = View::factory('shipmethods_add');

        $this->template->available_shipment = $main_shipments;
    }

    public function action_edit()
    {
        $fs_name = $this->shipMethods[$this->request->param( 'id' )]['fs_name'];
        $this->template = View::factory('shipmethods_edit_' . strtolower( $fs_name ) );

        $obj = $this->model->setMethod( $fs_name )->getItem( $this->request->param( 'id' ) );
        $this->template->obj = $obj;
    }

    public function action_save()
    {
        $fs_name = $_POST['basic_fs_name'];        
        if( $this->request->param( 'id' ) )
            $res = $this->model->setMethod( $fs_name )->save( $this->request->param( 'id' ), $this->lang );
        else
            $res = $this->model->setMethod( $fs_name )->addItem( $this->lang );

        if(!Messages::has() && $res )
        {
            $this->redirect_to_controller( $this->request->controller );
        }else
        {            
            $this->action_edit();
        }
    }

    public function action_update()
    {        
        $fs_name = $_POST['basic_fs_name'];        
        
        if( $this->request->param( 'id' ) )
                $res = $this->model->setMethod( $fs_name )->save( $this->request->param( 'id' ), $this->lang );
        else
                $res = $this->model->setMethod( $fs_name )->addItem( $this->request->param( 'id' ), $this->lang );
        
        if( $res )
        {
            $this->redirect_to_controller( $this->request->controller . '/' . $res . '/edit' );
        }else
        {            
            $this->action_edit();
        }
    }

    static public function getCost( $method, $weight = 0, $sum = 0 )
    {
        if( $method == 0 ) return 0;
        $method = Model_ShipMethods::getMainItem( array( 'id' => $method ) );
        $oMethod = Model_ShipMethods::instance()->setMethod( $method['fs_name'] );
        $oMethod->weight = $weight;
        $oMethod->sum = $sum;

        return $oMethod->getCost( $method['id'] );
    }

    public function action_next()
    {
        $this->template = View::factory('shipmethods_edit_' . strtolower( $_POST['fs_name'] ) );
        $obj['basic_fs_name'] = $_POST['fs_name'];
//        $obj = $this->model->setMethod($_POST['fs_name'])->getItem();
        $this->template->obj = $obj;
    }

    public function action_updateOrderId()
    {
        if( $this->model->updateOrderId( $_GET['ids'] ))
                die('1');
        else
                die('error');
    }
}