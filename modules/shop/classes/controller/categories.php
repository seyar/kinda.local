<?php defined( 'SYSPATH' ) OR die( 'No direct access allowed.' );

class Controller_Categories extends Controller_Shop
{

    //public $template = 'categories_list.tpl';
    public $module_title = 'Categories';
    public $module_desc_short = 'Shop Descr Short';
    public $module_desc_full = 'Shop Descr Full';

    public function before()
    {
        parent::before();
        $this->model = new Model_Categories();
        //$this->request->controller .= ':'.MODULE_SUBCONTROLLER;
    }

    public function action_index()
    {
        $rows = $current_category = array( );
        $page_links = '';
        $total_pages = 0;

        list($rows, $page_links, $total_pages) = $this->model->categories_list( $this->lang );

        $this->view->root_category_id = Controller_Shop::$root_category_id;

        if( Session::instance()->get( 'shop_cat_id' ) && Session::instance()->get( 'shop_cat_id' ) != Controller_Shop::$root_category_id )
        {
            $parent_category = $this->model->categories_load( Session::instance()->get( 'shop_cat_id' ) );
            $this->view->assign( 'parent_name', $parent_category['name'] );
        }

        $this->view->rows = $rows;
        $this->view->pages_list = $page_links;
        $this->view->total_pages = $total_pages;
    }

    public function action_verify()
    {
        if( $this->request->param( 'id' ) )
            $this->model->categories_verify( $this->request->param( 'id' ) );

        $this->redirect_to_controller( $this->request->controller );
    }

    public function action_delete()
    {
        if( $this->request->param( 'id' ) )
            $this->model->categories_delete( $this->request->param( 'id' ) );

        elseif( count( $_POST['chk'] ) )
            $this->model->categories_delete_list( $_POST['chk'] );
        
        /* Change filter value to default */
        $filter = (array)json_decode( Cookie::get( 'filter_' . MODULE_NAME, '' ), TRUE );
        $filter['cat'] = Kohana::config('shop.root_category_id');
        Cookie::set( 'filter_' . MODULE_NAME, json_encode( $filter ) );

        $this->redirect_to_controller('/goods');
    }

    public function action_edit()
    {
        $this->template = View::factory('shop_categories_edit');
        
        $this->template->categories = $this->model->categories_folders($this->lang);        
        $this->template->obj = $this->model->categories_load($this->request->param('id'));
        $this->template->add_fields_types = $this->model->categories_add_fields_types($this->request->param('id'));
        $addGoodsModel = Kohana::config('shop')->addGoodsModel;
        $addGoodsMethod = Kohana::config('shop')->addGoodsMethGet;
        if( class_exists($addGoodsModel) && $this->request->param('id'))
            $this->template->recGoods = call_user_func(array($addGoodsModel, $addGoodsMethod),'catId = '.$this->request->param('id'));
        
    }

    public function action_save()
    {
        if( $this->model->categories_save( $this->lang, $this->request->param( 'id' ) ) )
        {
            $this->action_rebuild();
            $this->redirect_to_controller('/goods');
        }else
        {
            $this->view->assign( 'errors', $this->model->errors );
            $this->action_edit();
        }
    }

    public function action_update()
    {
        if( $this->model->categories_save( $this->lang, $this->request->param( 'id' ) ) )
        {
            //$this->action_rebuild();
            $this->redirect_to_controller( $this->request->controller . '/' . $this->request->param( 'id' ) . '/edit' );
        }else
        {
            $this->view->assign( 'errors', $this->model->errors );
            $this->action_edit();
        }
    }

    public function action_rebuild()
    {
        $this->model->category_parents_rebuild( $this->lang );
    }
    
    public function action_getBrokenid()
    {
        $res = $this->model->getBrokenid();
        die($res);
    }
    
    public function action_deleteCatImg()
    {
        $res = $this->model->deleteCatImg( $this->request->param('id') );
        die(print($res));
    }

    public function action_copy()
    {
        if( $this->model->copy( $this->request->param( 'id' ) ) )
        {
            $this->redirect_to_controller( $this->request->controller );
        }else
        {
            $this->view->assign( 'errors', $this->model->errors );
            $this->action_edit();
        }
    }
    

    public function action_SortInUpdate()
    {
        $this->model->SortInUpdate();
    }

    public function action_edit_options_update()
    {
        $this->model->edit_options_update();
    }

    public function action_options_delete()
    {
        $this->model->options_delete();
    }

    public function action_is_searchable_update()
    {
        $this->model->is_searchable_update();
    }

    public function action_is_filterable_update()
    {
        $this->model->is_filterable_update();
    }

    public function action_edit_options_delete()
    {
        $this->model->edit_options_delete();
    }
    
    public function action_catsTreewview()
    {                                 
        $this->template = View::factory('shop_categories_treewview');
        $this->template->publ_types = Model_Categories::getAllGoods();
    }
    
    public function action_copy_add_fields()
    {
        if ( intval((Arr::get($_GET, 'source_id', NULL))) )
        {
            $this->model->copy_add_fields( (int)Arr::get($_GET, 'source_id'), $this->request->param( 'id' ) );
            die('1');
}
        else
        {
            die('0');
        }
    } 
    
}