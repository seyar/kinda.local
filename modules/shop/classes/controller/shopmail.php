<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 03.08.2010 - 10:30:48 Exp $
 *
 * Project:     rozymnik
 * File:        shopmail.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_ShopMail extends Controller_Shop
{

    public $template		= 'shopmail.tpl';

    public $module_title	= 'Shop Mail';
    public $module_desc_short   = 'Shop Descr Short';
    public $module_desc_full	= 'Shop Descr Full';

    public function before()
    {
        parent::before();
	$this->model = new Model_ShopMail();
        $this->request->controller .= ':'.MODULE_SUBCONTROLLER;
    }

    public function action_index()
    {
	$rows = $current_category = array(); $page_links = ''; $total_pages = 0;

	list($rows, $page_links, $total_pages) = $this->model->getList(NULL, 100000);

	$this->view->assign('rows', $rows );
	$this->view->assign('pages_list', $page_links );
	$this->view->assign('total_pages', $total_pages );
    }

    public function action_send()
    {

        if(count($_POST['chk']))
            $this->model->send( $_POST['chk'], $this->lang );

        $this->redirect_to_controller($this->request->controller);
    }
    
    public function action_un()
    {        
        $res = Model_ShopMail::unsubscribe( $this->request->param('code'), CURRENT_LANG_ID );
        $this->page_info = array(
            'page_title' => i18n::get('You have successfully unsubscribed'),
            'page_keywords' => i18n::get('You have successfully unsubscribed'),
            'page_description' => i18n::get('You have successfully unsubscribed'),
            'page_content' => i18n::get('You have successfully unsubscribed'),
        );

        $this->template = 'inner.tpl';
    }
}
?>