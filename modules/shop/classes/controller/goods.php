<?php
defined( 'SYSPATH' ) OR die( 'No direct access allowed.' );

class Controller_Goods extends Controller_Shop
{

    public $template		= 'shop_products';

    public $module_title = 'Goods';
//    public $module_name = 'goods';
    public $module_desc_short = 'Shop Descr Short';
    public $module_desc_full = 'Shop Descr Full';
    static $goods_status_list = array( 'active', 'archive', 'out_of_store', 'unverified', 'from1c' );
    static $images_folder;
    static $image_width;
    static $image_height;
    static $image_prev_width;
    static $image_prev_height;
    static $max_images_count;

    public function before()
    {
        parent::before();
        $this->model = new Model_Goods();
    
//        $this->request->controller .= ':'.MODULE_SUBCONTROLLER;

        Controller_Goods::$images_folder = self::$images_folder = Kohana_Config::instance()->load('shop')->images_folder;

        Controller_Goods::$image_width = self::$image_width = Kohana_Config::instance()->load('shop')->image_width;
        Controller_Goods::$image_height = self::$image_height = Kohana_Config::instance()->load('shop')->image_height;
        Controller_Goods::$image_prev_width = self::$image_prev_width = Kohana_Config::instance()->load('shop')->image_prev_width;
        Controller_Goods::$image_prev_height = self::$image_prev_height = Kohana_Config::instance()->load('shop')->image_prev_height;

        Controller_Goods::$max_images_count = self::$max_images_count = Kohana_Config::instance()->load('shop')->max_images_count;

        $this->template->current_lang = $this->lang;
        
    }

    public function action_index()
    {
        
        $where = array();        
//        $this->template = View::factory( 'shop_products' );
                    
        list($where, $filter) = Model_Goods::getFilter($this->lang);        
        $rows = array();
        
        $page_links = '';
        $total_pages = 0;

        $this->template->publ_types = Model_Categories::getAllGoods();

        $this->template->filter = $filter;
        
        $cookie = Arr::get($_COOKIE,'stat', 'yes');
        
        if( $cookie == 'yes' )
        {
            $this->template->style_panel = 'display:block';
            $this->template->style_content='margin-left: 249px;';
            $this->template->style_but='display:none;';
        }else
        {
            $this->template->style_panel = 'display:none';
            $this->template->style_content='margin-left: 0px;';
            $this->template->style_but='display:block;';
        }
        
        $priceCategories = array(-1 => array('id' => '', 'name'=> ''))+Model_Currencies::getPriceCategories(); 
        
        $this->template->priceCategories = $priceCategories;

        //die(Kohana::debug($this->template->filter));
        $shop_goods_prices = DB::select( 'shop_goods.id', 'shop_goods.name', 'alter_name', 'quantity', 'shop_goods.status', 'default_photo', 'is_bestseller', 'is_new', 'show_on_startpage', array('shop_prices.price', 'shop_prices'), 'shop_goods.seo_url', 'shop_goods.seo_description', 'shop_goods.seo_keywords', 'shop_goods.categories_id' )
                ->from('shop_goods')
                ->join('shop_categories','left')
                    ->on( 'shop_categories.id', '=','shop_goods.categories_id')
                ->join('shop_prices','left')
                    ->on( 'shop_prices.good_id', '=','shop_goods.id')
                ;
        
        list($this->template->pagination, $this->template->rows) = Admin::highload_model_pagination( 
                $this->lang, 
                $shop_goods_prices,
                $where
        );
    }

    public function action_apply()
    {
        die('Hello world');
        //die(Kohana::debug($_POST));
        if( $id = $this->model->save( $this->lang, $this->request->param( 'id' ) ) )
        {
            $this->redirect_to_controller( $this->request->controller . '/' . $id . '/edit' . '#' . $_POST['actTab'] );
        }else
        {
//            $this->view->assign( 'errors', $this->model->errors );
            $this->action_edit();
        }
    }

    public function action_edit()
    {
        $this->model = new Model_Goods();
        
        if( $good = Model_Goods::itemExists($this->request->param('id')) )            $this->redirect_to_controller( $this->request->controller . '/' . $good . '/edit' );
        
        $this->template = View::factory( 'shop_products_edit' );
        
        $this->template->edit_categories_list = Model_Categories::categories_folders( $this->lang, 1 );        
        $this->template->edit_price_categories = Model_Shop::shop_price_categories();
        $this->template->edit_currencies_list = Model_Shop::shop_currencies();
        $this->template->defaultCurrency = Kohana::config('shop')->default_currency;
        
        //die(Kohana::debug($this->template->edit_categories_list));
        $this->template->obj = $this->model->load( $this->request->param('id') );
        
        $this->template->brands = array(-1 => array('id' => '', 'name'=> ''))+Model_Brands::getItems();
        $this->template->goodGroups = $this->model->getGoodsGroups($this->request->param('id'));
        
        $addGoodsModel = Kohana::config('shop')->addGoodsModel;
        $addGoodsMethod = Kohana::config('shop')->addGoodsMethGet;
        if( class_exists($addGoodsModel) && $this->request->param('id') )
            $this->template->recGoods = call_user_func(array($addGoodsModel, $addGoodsMethod),'goodId = '.$this->request->param('id'));
    }

    public function action_update()
    {
//        if( $this->request->param( 'id' ) == 0 )
//        {
//            $id = $this->model->product_create();
//            $this->redirect_to_controller( $this->request->controller . '/' . $id . '/edit' );
//        }
        
        if( $res = $this->model->save( $this->lang, $this->request->param( 'id' ) ) )
        {            
            $this->redirect_to_controller( $this->request->controller . '/' . $res . '/edit' . '#' . $_POST['actTab'] );
        }else
        {
//            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

    public function action_save()
    {
//        if( $this->request->param( 'id' ) == 0 )
//        {
//            $id = $this->model->product_create();
//
//            $this->redirect_to_controller( $this->request->controller . '/products' );
//        }
        if( $this->model->save( $this->lang, $this->request->param( 'id' ) ) )
        {
            $this->redirect_to_controller( $this->request->controller  );
        }
        else
        {
//            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

    public function action_delete()
    {
        if( $this->request->param( 'id' ) )
            $this->model->delete( $this->request->param( 'id' ) );

        elseif( count( $_POST['chk'] ) )
            $this->model->delete_list( $_POST['chk'] );
        
        $this->redirect_to_controller( $this->request->controller );
    }

    // Images Actions

    public function action_image_preview()
    {
        $this->show_image_preview( MEDIAPATH . self::$images_folder . '/' . $this->request->param('id') . '/' . basename( $_GET['name'] ),$this->request->param('id') );
    }

    public function action_images_default()
    {
        $this->model->images_default( MEDIAPATH . self::$images_folder . '/' . $this->request->param('id') . '/' . basename( $_GET['name'] ),$this->request->param('id') );
        die();
    }

    public function action_images_delete()
    {
        $this->model->images_delete( self::$images_folder, $this->request->param('id') );
        die();
    }

    public function action_images_show()
    {
        echo json_encode(
                $this->model->images_show(
                        self::$images_folder
                        , strtolower($this->admin_path . Request::instance()->controller . "/" . MODULE_NAME . ':' . MODULE_SUBCONTROLLER . "/" . $this->request->param('id'))
                        , strtolower(MEDIAWEBPATH . MODULE_NAME . "/" . $this->request->param('id').'/')
                        , $this->request->param('id')
                )
        );
        die();
    }

    // Images Upload Actions

    public function action_images_upload()
    {
        $watermark = Kohana::config( 'shop.watermark' );        
//        Kohana_log::instance()->add('d', Kohana::debug(self::$images_folder, self::$image_width, self::$image_height, self::$image_prev_width, self::$image_prev_height, $this->request->param('id')));
        $this->model->images_upload( self::$images_folder, self::$image_width, self::$image_height, self::$image_prev_width, self::$image_prev_height, $this->request->param('id'), NULL );
    }

    public function action_images_check()
    {
        $this->model->images_check( self::$images_folder, $this->request->param('id') );
    }

    public function action_copy2otherlang()
    {
        if( $this->model->copy2otherlang( $_POST['copy_for_lang'], $this->request->param( 'id' ), $this->lang ) )
        {
            $this->redirect_to_controller( $this->request->controller . '/' . $this->request->param( 'id' ) . '/edit' );
        }else
        {
//            $this->view->assign( 'errors', $this->model->errors );
            $this->action_edit();
        }
    }

    public function action_reprice()
    {
        $this->model->reprice();
    }

    public function action_statusUpdate()
    {

//        print_r($_POST);
        DB::update( 'shop_goods' )
                ->set(
                        array(
                            'status' => $_POST['status']
                        )
                )
                ->where( 'id', '=', $this->request->param( 'id' ) )
                ->execute();
        die( '1' );
    }

    public function action_statusUpdateBest()
    {


        $var = $this->request->param( 'id' );

        ;
        DB::update( 'shop_goods' )
                ->set( array( 'is_bestseller' => DB::expr( '! is_bestseller' ) ) )
                ->where( 'id', '=', $var )
                ->execute();

        //die(Kohana::debug($this));
        die( '1' );
    }

    public function action_statusUpdateNews()
    {


        $var = $this->request->param( 'id' );

        ;
        DB::update( 'shop_goods' )
                ->set( array( 'is_new' => DB::expr( '! is_new' ) ) )
                ->where( 'id', '=', $var )
                ->execute();

        //die(Kohana::debug($this));
        die( '1' );
    }

    public function action_statusUpdatePage()
    {


        $var = $this->request->param( 'id' );

        ;
        DB::update( 'shop_goods' )
                ->set( array( 'show_on_startpage' => DB::expr( '! show_on_startpage' ) ) )
                ->where( 'id', '=', $var )
                ->execute()
        ;

        //die(Kohana::debug($this));
        die( '1' );
    }

    public function action_toparentsMode()
    {
        $res = $this->model->toParentsMode( 1 );
        die( Kohana::debug( $res ) );
    }
    
    public function action_cancel_default_image()
    {
        $res = Model_Goods::update( $this->request->param('id'), array('default_photo' => '') );
        die( print($res) );
    }

}