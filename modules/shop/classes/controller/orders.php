<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Orders extends Controller_Shop
{

//    public $template		= 'orders_list.tpl';
    public $template = 'shop_orders';
    public $module_title = 'Orders';
    public $module_desc_short = 'Shop Descr Short';
    public $module_desc_full = 'Shop Descr Full';
    public static $order_statuses = array(
        '0' => 'Accepted',
        '1' => 'Payed',
        '2' => 'Packed',
        '3' => 'Shipped',
        '4' => 'Executed',
        '5' => 'Archive',
    );

    public function before()
    {
        parent::before();
        $this->model = new Model_Orders();
        //$this->request->controller .= ':'.MODULE_SUBCONTROLLER;
        foreach( self::$order_statuses as $key => $item )
        {
            self::$order_statuses[$key] = I18n::get($item);
        }
    }

    public function action_index()
    {
        $this->template = View::factory('shop_orders');

        $rows = array();
        $page_links = '';
        $total_pages = 0;

        list($where, $filter) = Model_Orders::getFilter();        
        $this->template->filter = $filter;
        $this->template->order_statuses = self::$order_statuses;
        
        list($this->template->payment_methods,$this->template->shipment_methods) = Model_Orders::load_medthods();

        list($this->template->pagination, $this->template->rows) = Admin::model_pagination('', 'shop_orders_list',
                        array('id',  'shipment_name', 'date_create', 'sum', 'status','shipment_methods','payment_methods'),
                        $where, 'id'
        );
        
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            $this->model->delete($this->request->param('id'));

        elseif (count($_POST['chk']))
            $this->model->delete_list($_POST['chk']);

        $this->redirect_to_controller($this->request->controller);
    }

    public function action_edit()
    {
        $this->template = View::factory('orders_edit');

        $obj = $this->model->load($this->request->param('id'));
        $this->template->obj = $obj;
        $this->template->customer = Model_Customers::load($obj['customer_id']);

        $this->template->price_categories = Model_Shop::price_categories();
        $this->template->order_statuses = Model_Orders::statuses();
        $this->template->payment_methods = Model_Shop::payment_methods();
        $this->template->shipment_methods = Model_Shop::shipment_methods();

        $this->template->customer_billing_addresses = Model_Customers::billing_address_list($obj['customer_id']);
        $this->template->customer_shipment_addresses = Model_Customers::shipment_address_list($obj['customer_id']);
    }

    public function action_ajax()
    {
        $this->auto_wrapper = false;

        switch ($_GET['task'])
        {
            case 'good_name':
                $good = Model_Goods::load($_GET['id']);

                echo json_encode(
                        array(
                            'name' => ($good['name'] ? $good['name'] : $good['alter_name']),
                            'price' => str_replace(',', '.', $good['prices'][Kohana::config('shop')->default_category][Kohana::config('shop')->frontend_currency]),
                        )
                );
                die();
                break;

            case 'autocomlete':
                $this->model->find_goods($_GET['field'], $_GET['q']);
                die();
                break;

            case 'change_status':
                $this->model->change_status($this->request->param('id'), $this->lang);
                die();
                break;

            case 'update_good':
                $this->model->update_good($this->request->param('id'));
                die();
                break;

            case 'delete_good':
                $this->model->delete_good($this->request->param('id'));
                die();
                break;

            case 'status':
                $this->template = View::factory('order_status');
                break;

            case 'details':
                $this->template = View::factory('order_details');
                break;
            default:
                die();
        }

        $obj = $this->model->load($this->request->param('id'));
        $this->template->obj = $obj;
        $this->template->customer = Model_Customers::load($obj['customer_id']);

        $this->template->price_categories = Model_Shop::price_categories();
        $this->template->order_statuses = Model_Orders::statuses();

        $this->auto_wrapper = FALSE;
//        $this->ajax_echo();
//        die();
    }

    public function action_save()
    {
        if ($this->model->save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller);
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

    public function action_update()
    {
        if ($this->model->save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

    public function action_statusUpdateOrders()
    {
        $orderId = $this->request->param('id');
        
        $res = $this->model->change_status( $orderId, $this->lang );
        
        die('1');
    }

}