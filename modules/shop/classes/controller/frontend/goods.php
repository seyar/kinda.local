<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Frontend_Goods extends Controller_Content {

    public $template		= 'shop_good.tpl';

    static $images_folder;
    static $image_width;
    static $image_height;
    static $image_prev_width;
    static $image_prev_height;


    public function before()
    {
        parent::before();

        Controller_Frontend_Goods::$images_folder = $this->images_folder = Kohana_Config::instance()->load('shop')->images_folder;

        Controller_Frontend_Goods::$image_width = $this->image_width = Kohana_Config::instance()->load('shop')->image_width;
        Controller_Frontend_Goods::$image_height = $this->image_height = Kohana_Config::instance()->load('shop')->image_height;
        Controller_Frontend_Goods::$image_prev_width = $this->image_prev_width = Kohana_Config::instance()->load('shop')->image_prev_width;
        Controller_Frontend_Goods::$image_prev_height = $this->image_prev_height = Kohana_Config::instance()->load('shop')->image_prev_height;

        $this->view->assign('frontendCurrency', Kohana::config('shop.frontend_currency') );
    }

    public function action_index()
    {
        if( $good = Model_Goods::itemExists($this->request->param('id')) )
        {
            $obj = Model_Frontend_Goods::info($good);
            $this->request->redirect( "/shop/{$obj['category_url']}_{$obj['categories_id']}/{$obj['seo_url']}_{$obj['id']}.html" );
        }
        $obj = $this->_objBaseicInfo();

        $obj['other_goods'] = Model_Frontend_Goods::thisCategoryOtherGoods( $obj['id'], $obj['categories_id'], NULL, 5, true );
//        $userId = Session::instance()->get('user_id', null);
//        if($userId)
//        {
//            $this->view->assign('userVoted', Model_Frontend_Goods::isVoted($obj['id'], $userId));
//        }
        $keywords = Model_Frontend_Goods::getKeywords( $obj['id'] );
        $description = Model_Frontend_Goods::getKeywords( $obj['id'], NULL, 'description' );
        
//        $this->page_info = array(
//                        'page_title'=> $keywords, // (isset($obj['name']) && !empty($obj['name'])) ? $obj['name'] : $obj['alter_name'],
//                        'page_keywords'=> $keywords,
//                        'page_description'=> $description, // $obj['seo_description'],
//                );
        $obj['store_approximate_date'] = Model_Frontend_Goods::getApproximateDate($obj['store_approximate_date']);
        $this->view->assign('obj', $obj );
        $this->view->assign('obj_properties', Model_Frontend_Goods::properties($this->request->param('id')) );
        
        $this->view->assign('category_info', Model_Frontend_Categories::info($obj['categories_id']) );
        $this->view->assign('search_currencies_list', Model_Shop::shop_currencies());
        Helper_Brands::init($obj['categories_id']);
        $filterableFields = Model_Frontend_Categories::getFilterableFields($obj['categories_id']);
        $this->view->assign('filterableFields', $filterableFields);

        $filter = $_COOKIE['shop_f_filter'];
        $filter = json_decode($filter, true);

        if( !is_array($filter) )
        {
            $filter = array();
        }
        $sortby = !empty($_COOKIE['shop_f_sortby']) ? $_COOKIE['shop_f_sortby'] : null;
        $sortasc = !empty($_COOKIE['shop_f_sortasc']) ? $_COOKIE['shop_f_sortasc'] : null;

        // get add goods
        $addGoodsModel = Kohana::config('shop')->addGoodsModelFront;
        $addGoodsMethod = Kohana::config('shop')->addGoodsMethGet;        
        if( method_exists( $addGoodsModel, $addGoodsMethod ) )
            $this->view->assign('recGoods', call_user_func(array($addGoodsModel, $addGoodsMethod), $obj['id'], $obj['categories_id'] ) );

        $this->view->assign('filter', $filter);
        $this->view->assign('sortby', $sortby);
        $this->view->assign('sortasc', $sortasc);
        
        $this->view->assign('youhaveseen', Model_Frontend_Youhaveseen::getGoods());
        Model_Frontend_Youhaveseen::saveItem( $obj['id'] );
        
    }

    public function action_comments()
    {
        $this->template = 'shop_list_reviews.tpl';
        $this->_objBaseicInfo();
        
    }

    private function _objBaseicInfo()
    {
        $this->view->assign('module_id', Model_AdminModulesControl::find('shop') );
        $obj = Model_Frontend_Goods::info($this->request->param('id'));
        $this->page_info = array(
                        'page_title'=>$obj['name'],
                        'page_keywords'=>$obj['seo_keywords'],
                        'page_description'=>$obj['seo_description'],
                );
        
        $this->view->assign('obj_images',
                        Model_Goods::images_show
                            (
                                $this->images_folder
                                ,''
                                ,MEDIAWEBPATH.Controller_Frontend_Goods::$images_folder."/".$this->request->param('id')
                                ,$this->request->param('id')
                            )
                );

        return $obj;
    }

    public function action_vote()
    {
        $goodId = intval($_GET['g']);
        $stars = intval($_GET['s']);
        $m = new Model_Frontend_Goods();
        $userId = Session::instance()->get('user_id', null);
        if($userId)
        {
            $m->vote($goodId, $stars, $userId);
        }
        exit;
    }
    
    public function action_loadGood()
    {
       $res = Model_Frontend_Goods::loadGood( $_GET['id'] );
       die( print(json_encode($res)) );
    }

    public function action_loadGoodData()
    {
        $obj = Model_Frontend_Goods::info( $_GET['id'] );
        $obj['store_approximate_date'] = Model_Frontend_Goods::getApproximateDate($obj['store_approximate_date']);
//die(Kohana::debug($res));
        $this->template = 'shop_good_data.tpl';
        $this->view->assign('obj',$obj);
        $this->view->assign('authorized',Session::instance()->get('authorized'));
    }
}