<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 03.08.2010 - 10:30:48 Exp $
 *
 * Project:     rozymnik
 * File:        shopmail.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_Frontend_ShopMail extends Controller_Content
{

    public $template		= 'shopmail.tpl';

    public function before()
    {
        parent::before();
    }


    public function action_un()
    {
        $res = Model_ShopMail::unsubscribe( $this->request->param('code'), CURRENT_LANG_ID );
        $this->page_info = array(
            'page_title' => i18n::get('Congratulations'),
            'page_keywords' => i18n::get('You have successfully unsubscribed'),
            'page_description' => i18n::get('You have successfully unsubscribed'),
            'page_content' => i18n::get('You have successfully unsubscribed'),
        );

        $this->template = 'inner.tpl';
    }
}
?>