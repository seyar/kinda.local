<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 26.04.2010 - 15:14:31 Exp $
 *
 * Project:     golden
 * File:        cart.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_Frontend_Cart extends Controller_Content
{
    public $template		= 'shop_cart.tpl';
    static protected $_res = null;

    public function before()
    {
        parent::before();
        $this->model = new Model_Frontend_Cart();
    }

    public function action_index()
    {
        if( Cookie::get('cart_active_payment') )
        {
            $oPaymethods = new Model_PayMethods();
            $paymentMethod = $oPaymethods->load( Cookie::get('cart_active_payment') );
            Model_Frontend_Currencies::$rate = $paymentMethod['rateMethod'];
        }
        
        $user = Session::instance()->get('authorized');
        if( !$user )
            $this->action_quickOrder();
        elseif(class_exists('Model_Customers'))
        {
            $user = Model_Customers::shipment_address_list($user['id']);
            $this->view->assign('user', current($user) );
        }
        
        $res = $this->model->get_items();
        
        $this->page_info = array(
            'page_title'=> i18n::get('Cart'),
            'page_keywords'=> i18n::get('Cart'),
            'page_description'=> i18n::get('Cart'), 
        );

        $this->view->assign('payment_methods', Model_Shop::payment_methods(array('status' => 1)));
        $this->view->assign('shipment_methods', Model_Shop::shipment_methods(array('status' => 1)));

        $this->view->assign('payment', Model_Frontend_Cart::get_default_payment() );
        $this->view->assign('shipment', Model_Frontend_Cart::get_default_shipment() );

        $this->view->assign('rows', $res['0']);
        $this->view->assign('total_sum', $res['1']);
        $this->view->assign('total_amount', $res['2']);
    }

    public function action_add()
    {
        if( $this->model->add_item($this->request->param('id'), (int)Arr::get($_GET, 'count', 1) ) )
        {
//            $this->view->assign('message', Model_RegistrationFrontend::message(i18n::get('This product was successfully added to your cart'), 'ok', TRUE));
            Messages::add(i18n::get('Продукт успешно добавлен в корзину'));
            if( $_SERVER['HTTP_REFERER'] )
                $this->request->redirect( $_SERVER['HTTP_REFERER'] );
            else
//                $this->request->redirect( $this->request_uri );
                $this->request->redirect( '/' );
        }else{
            $this->view->assign('message', Model_RegistrationFrontend::message('Ошибка добавления', 'err', TRUE));
            $this->request->redirect( '/' );
        }
    }

    public function action_delete_item()
    {
        $res = $this->model->delete_item( $this->request->param('id') );
        die(print(json_encode($res)));
    }

    public function action_change_amount()
    {
        $res = $this->model->change_amount( $this->request->param('id'), (int)$_GET['amount'], $_GET['shipmentMethodId'] );
        die(print(json_encode($res)));
    }
    
    public function action_getShipmentCost()
    {
        $discount = 0;
        $res = array('total_sum' => 0);
        if(isset($_GET['goodsCount']))
            $res = Model_Frontend_Cart::changeShipment( $this->request->param('id'), $_GET['goodsCount']);
        $return = json_encode(array('errors' => 0) + $res);
        
        die(print($return));
    }
    
    public function action_quickOrder()
    {
        $this->template = 'shop_quickorder.tpl';
        $discount = 0;
        
        $cartItems = Model_Frontend_Cart::get_items();
        
        $this->view->assign('cartItems', $cartItems[0] );
        
        $this->view->assign('payment_methods', Model_Shop::payment_methods(array('status' => 1)));
        $this->view->assign('shipment_methods', Model_Shop::shipment_methods(array('status' => 1)));

        $this->view->assign('payment', Model_Frontend_Cart::get_default_payment() );
        $this->view->assign('shipment', Model_Frontend_Cart::get_default_shipment() );
        $this->view->assign('shipment_cost', $shipment_cost );        
    }
    
    public function action_popupCart()
    {
        $this->template = 'shop_popupCart.tpl';
        $discount = 0;
        
        $cartItems = Model_Frontend_Cart::get_items();
        
        $this->view->assign('cartItems', $cartItems[0] );
        $this->view->assign('total_sum', $cartItems[1] );
        
//        $this->view->assign('payment_methods', Model_Shop::payment_methods(array('status' => 1)));
//        $this->view->assign('shipment_methods', Model_Shop::shipment_methods(array('status' => 1)));
//
//        $this->view->assign('payment', Model_Frontend_Cart::get_default_payment() );
//        $this->view->assign('shipment', Model_Frontend_Cart::get_default_shipment() );
//        $this->view->assign('shipment_cost', $shipment_cost );        
    }
    
    public function action_quicksubmit()
    {
        $res = $this->model->quickOrderSubmit();
        
        if($res)
        {
            $payment_method = Model_PayMethods::instance()->load( $_POST['payment'] );
            
            if( $payment_method['type'] == 2 )
            {
                $_POST['quick_orderid'] = $res['id'];
                $_POST['quick_payment_method'] = $payment_method;
                $_POST['quick_shipment_method'] = $_POST['shipment'];
                                
                $this->action_redirecttopayment();
            }
            else
            {
                Messages::add('Cпасибо!Ваш заказ передан в обработку');
                Cookie::delete('cart');
                Cookie::delete('cart_global_sum');
                Cookie::delete('cart_global_amount');
                Cookie::delete('cart_global_weight');
                $this->request->redirect('/');
            }
            
        }else
        {            
            Messages::add(  $this->model->errors );
            $this->action_quickOrder();
            //$this->request->redirect( Route::get('shop_cart_front')->uri( array('lang'=>'')) );
        }
        
    }
    
    public function action_getShipments()
    {
        $oPaymethods = new Model_PayMethods();
        $res = $oPaymethods->load( $this->request->param('id') );
        $res = json_encode(Model_ShipMethods::getList( array(array('id','IN', DB::expr('('.implode(',',$res['forShipment']).')') )) ));
        die(print($res));
    }
    
    public function action_getInstructions()
    {
        $oPaymethods = new Model_PayMethods();
        $paymethods = $oPaymethods->load( $_GET['paymentId'] );
        $return['paymethods'] = $paymethods['text'];
        if(isset($_GET['shipmentId']))
        {
            $shipmethods = Model_Shop::shipment_methods();
            $return['shipmethods'] = $shipmethods[$_GET['shipmentId']]['text'];
        }
        
        $res = json_encode($return);
        die(print($res));
    }
    
    /**
     * redirect to money.ua with folowing params
     * 
     */
    public function action_redirecttopayment()
    {   
        /* for redirect from quick order */
        $order_id = Arr::get($_POST,'quick_orderid', $order_id);
        $payment_method = Arr::get($_POST,'quick_payment_method', $payment_method);
        $shipment_method = Arr::get($_POST,'quick_shipment_method', $shipment_method);
                
        $order = Model_Orders::load($order_id);        
        $shipment_methods = Model_Shop::shipment_methods();

        $payment_method['fields']['PAYMENT_AMOUNT'] = $order['sum']*100;
        $payment_method['fields']['PAYMENT_DELIVER'] = $shipment_methods[$shipment_method]['name'];
        $payment_method['fields']['PAYMENT_ORDER'] = $order['id'];
        $payment_method['fields']['PAYMENT_ADDVALUE'] = str_replace(' ', '_', $payment_method['fields']['PAYMENT_ADDVALUE']);
        $payment_method['fields']['PAYMENT_INFO'] = addslashes($payment_method['fields']['PAYMENT_INFO']);

        $payment_method['fields']['PAYMENT_INFO'] = '';
        foreach($order['ser_details'] as $item) $payment_method['fields']['PAYMENT_INFO'] .= isset($item['name']) && !empty($item['name']) ? $item['name'].', ' : $item['alter_name'].', ';
        $payment_method['fields']['PAYMENT_INFO'] = rtrim(str_replace("'", '', $payment_method['fields']['PAYMENT_INFO']),', ');
        
        array_walk($payment_method['fields'], array('Controller_Frontend_Cart', 'changeEncoding') );
        $SECRETCODE = mb_convert_encoding($payment_method['fields']['SECRETCODE'],'UTF-8');
        $hash =          $payment_method['fields']['MERCHANT_INFO']
                    .':'.$payment_method['fields']['PAYMENT_TYPE']
                    .':'.$payment_method['fields']['PAYMENT_RULE']
                    .':'.$payment_method['fields']['PAYMENT_AMOUNT']
                    .':'.$payment_method['fields']['PAYMENT_ADDVALUE']
                    .':'.$payment_method['fields']['PAYMENT_INFO']
                    .':'.$payment_method['fields']['PAYMENT_DELIVER']
                    .':'.$payment_method['fields']['PAYMENT_ORDER']
                    .':'.$payment_method['fields']['PAYMENT_VISA']
                    .':'.$payment_method['fields']['PAYMENT_TESTMODE']
                    .':'.$payment_method['fields']['PAYMENT_RETURNRES']
                    .':'.$payment_method['fields']['PAYMENT_RETURN']
                    .':'.$payment_method['fields']['PAYMENT_RETURNMET']
                    .':'.$SECRETCODE;
        
        $payment_method['fields']['PAYMENT_HASH'] = md5( $hash );
        header('content-Type: text/html; charset=windows-1251');
//        $this->view->assign('payment_method', $payment_method);
//        $this->view->assign('shipment_method', $shipment_method);
//        не поставил в шаблон потому что теряется кодировку ср1251
        
        echo 
        '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="content-Type" content="text/html; charset=windows-1251" />
                    <title>submit</title>                   
                    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
                </head>
                <body onload="document.getElementById(\'sForm\').submit();">

                    <form action="'. $payment_method['action_url'].'" id="sForm" method="post">
            ';
        
            foreach( $payment_method['fields'] as $key => $item )
            {
                  echo '<input type="hidden" name="'.$key.'" value="'.htmlentities($item,ENT_COMPAT,'CP1251').'" />';
            }
        echo '
                </form>
            </body>
        </html>';
        die();

    }
    
    static function changeEncoding(&$item, $key)
    {
        $item = mb_convert_encoding($item,'CP1251','UTF-8');
    }

    public function action_rebuildPrices()
    {
        if( !isset($_GET['paymentId']) ) return array();
            
        $oPaymethods = new Model_PayMethods();
        $paymentMethod = $oPaymethods->load( $_GET['paymentId'] );
        Model_Frontend_Currencies::$rate = $paymentMethod['rateMethod'];
        
        $return = array();
        $total_amount = 0;
        $total_weight = 0;
        $cartItems = Model_Frontend_Cart::get_items();
        if( count($cartItems[0]) )
        {
            foreach($cartItems[0] as $item)
            {
                $return[$item['id']] = array( number_format($item['prices'][1][2], 2, '.', ''), $item['amount'] );
                $total_amount += $item['amount'];
                $total_weight += $item['weight'];
            }
        }
        else
            return die( print(json_encode(array())));
        
       $discount = 0;
       $shipment_cost = Controller_ShipMethods::getCost( $_GET['shipmentId'], str_replace(',', '.', $total_weight), ($cartItems[1] - $discount ));
       
       $return = array('items' => $return, 'total_sum' => number_format($shipment_cost + $cartItems[1], 2, '.', '') );
       
       Cookie::set( 'cart_global_amount', $total_amount );
       Cookie::set( 'cart_global_sum', number_format($cartItems[1], 2, '.', '') );
       Cookie::set( 'cart_global_weight', str_replace(',', '.',$total_weight) );
       Cookie::set( 'cart_active_payment', (int)$_GET['paymentId'] );
       
       die( print(json_encode($return)));
    }

}
?>