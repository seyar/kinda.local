<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Frontend_Brands extends Controller_Content {

    public $template		= 'shop_brand.tpl';

    public function action_goods()
    {
        $brandId = intval($this->request->param('brandId'));
        if(!$brandId) throw new Exception('Brand is not specified');

        $model = new Model_Brands();
        $brandInfo = $model->find($brandId);
	$this->view->assign('brandInfo', $brandInfo );
        $this->view->assign('brandId', $brandId);
        $this->view->assign('brandSubmit', true);

	$this->page_info = array(
                        'page_title' => arr::get($brandInfo, 'name', $this->page_info['page_title'])
                );

	$rows = array(); $page_links = ''; $total_pages = 0;

        list($rows, $page_links, $total_pages) = $model->goods($brandId);

	$this->view->assign('goods', $rows );
	$this->view->assign('pages_list', $page_links );
	$this->view->assign('total_pages', $total_pages );
        
    }

}