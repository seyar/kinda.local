<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Frontend_Categories extends Controller_Content {

	public $template = 'shop_category.tpl';
	private $_search = false;

	public function before() {
		parent::before();
	}

	public function action_index() 
    {
		$category_info = Model_Frontend_Categories::info($this->request->param('id'));

		$this->view->assign('category_info', $category_info);

		$this->page_info = array(
			'page_title' => arr::get($category_info, 'name', $this->page_info['page_title']),
			'page_keywords' => arr::get($category_info, 'seo_keywords', $this->page_info['page_keywords']),
			'page_description' => arr::get($category_info, 'seo_description', $this->page_info['page_description']),
		);

		$filterableFields = Model_Frontend_Categories::getFilterableFields($this->request->param('id'));
		$this->view->assign('filterableFields', $filterableFields);

		$filter = array();
		if (Request::$method == 'POST') {
			if (isset($_POST['filter'])) {
				if (isset($_POST['filter']['price'])) {
					$filter['price'] = array_map('floatval', $_POST['filter']['price']);
				}

				foreach ($filterableFields as $field) {
					if (isset($_POST['filter'][$field['alias']]) && !empty($_POST['filter'][$field['alias']])) {
						switch ($field['type']) {
							case 'int' :
								if (is_array($_POST['filter'][$field['alias']]))
									$data = array_map('intval', $_POST['filter'][$field['alias']]);
								else
									$data = intval($_POST['filter'][$field['alias']]);
							case 'float' :
								if (is_array($_POST['filter'][$field['alias']]))
									$data = array_map('floatval', $_POST['filter'][$field['alias']]);
								else
									$data = floatval($_POST['filter'][$field['alias']]);

							default :
								$data = $_POST['filter'][$field['alias']];
						}

						$filter['add_fields'][$field['id']] = array(
							'alias' => $field['alias'],
							'type' => $field['type'],
							'data' => $data
						);
					}
				}

				setcookie('shop_f_filter', json_encode($filter), time() + 604800, '/shop/');
				$_COOKIE['shop_f_filter'] = json_encode($filter);
			}

			if (isset($_POST['sortby']) || isset($_POST['sort_asc'])) {
				$sortby = $_POST['sortby'];
				$sortasc = $_POST['sort_asc'];
				setcookie('shop_f_sortby', $sortby, time() + 604800, "/");
				setcookie('shop_f_sortasc', $sortasc, time() + 604800, "/");
				$_COOKIE['shop_f_sortby'] = $sortby;
				$_COOKIE['shop_f_sortasc'] = $sortasc;
			}

			if (isset($_POST['rowsPerPage'])) {
				$rowsPerPage = $_POST['rowsPerPage'];
				setcookie('shop_f_rowsPerPage', $rowsPerPage, time() + 604800, "/");
				$_COOKIE['shop_f_rowsPerPage'] = $rowsPerPage;
			}

			if (isset($_POST['brandId'])) {
				$brandId = $_POST['brandId'];
				setcookie('shop_f_brandId', $brandId, time() + 604800);
				$_COOKIE['shop_f_brandId'] = $brandId;
			}
		}


		$filter = $_COOKIE['shop_f_filter'];
		$filter = json_decode($filter, true);

		if (!is_array($filter)) {
			$filter = array();
		}
		$sortby = !empty($_COOKIE['shop_f_sortby']) ? $_COOKIE['shop_f_sortby'] : null;
		$sortasc = !empty($_COOKIE['shop_f_sortasc']) ? $_COOKIE['shop_f_sortasc'] : null;
		$rowsPerPage = !empty($_COOKIE['shop_f_rowsPerPage']) ? intval($_COOKIE['shop_f_rowsPerPage']) : Kohana::config('shop')->rows_frontend;
		$brandId = !empty($_COOKIE['shop_f_brandId']) ? intval($_COOKIE['shop_f_brandId']) : null;

		$this->view->assign('filter', $filter);
		$this->view->assign('sortby', $sortby);
		$this->view->assign('sortasc', $sortasc);
		$this->view->assign('rowsPerPage', $rowsPerPage);
		$this->view->assign('brandId', $brandId);

		$rows = array();
		$page_links = '';
		$total_pages = 0;

		list($rows, $page_links, $total_pages) = Model_Frontend_Categories::goods($this->request->param('id'), $rowsPerPage, Kohana::config('shop')->default_category, $filter, $sortby, $sortasc, $brandId);

		$this->view->assign('goods', $rows);
		$this->view->assign('pages_list', $page_links);
		$this->view->assign('total_pages', $total_pages);
	}

	public function action_search() {

		$this->page_info = array(
			'page_title' => 'Результаты поиска',
		);
        
        if( mb_strlen(trim($_GET['browser']), 'UTF-8') < 3 ) return false;
        
		$srchWhat = isset($_GET['browser']) ? trim($_GET['browser']) : '';
		$this->view->assign('srchWhat', $srchWhat);
		$this->view->assign('search', true);
		$this->_search = true;

		$filterableFields = Model_Frontend_Categories::getFilterableFields(0);
		$this->view->assign('filterableFields', $filterableFields);

		$filter = array();
		if (Request::$method == 'POST') {
			if (isset($_POST['filter'])) {
				if (isset($_POST['filter']['price'])) {
					$filter['price'] = array_map('floatval', $_POST['filter']['price']);
				}

				foreach ($filterableFields as $field) {
					if (isset($_POST['filter'][$field['alias']]) && !empty($_POST['filter'][$field['alias']])) {
						switch ($field['type']) {
							case 'int' :
								if (is_array($_POST['filter'][$field['alias']]))
									$data = array_map('intval', $_POST['filter'][$field['alias']]);
								else
									$data = intval($_POST['filter'][$field['alias']]);
							case 'float' :
								if (is_array($_POST['filter'][$field['alias']]))
									$data = array_map('floatval', $_POST['filter'][$field['alias']]);
								else
									$data = floatval($_POST['filter'][$field['alias']]);

							default :
								$data = $_POST['filter'][$field['alias']];
						}

						$filter['add_fields'][$field['id']] = array(
							'alias' => $field['alias'],
							'type' => $field['type'],
							'data' => $data
						);
					}
				}
				setcookie('shop_f_filter', json_encode($filter), time() + 604800);
				$_COOKIE['shop_f_filter'] = json_encode($filter);
			}

			if (isset($_POST['sortby'])) {
				$sortby = $_POST['sortby'];
				$sortasc = $_POST['sort_asc'];
				setcookie('shop_f_sortby', $sortby, time() + 604800, "/");
				setcookie('shop_f_sortasc', $sortasc, time() + 604800, "/");
				$_COOKIE['shop_f_sortby'] = $sortby;
				$_COOKIE['shop_f_sortasc'] = $sortasc;
			}

			if (isset($_POST['rowsPerPage'])) {
				$rowsPerPage = $_POST['rowsPerPage'];
				setcookie('shop_f_rowsPerPage', $rowsPerPage, time() + 604800, "/");
				$_COOKIE['shop_f_rowsPerPage'] = $rowsPerPage;
			}

			if (isset($_POST['brandId'])) {
				$brandId = $_POST['brandId'];
				setcookie('shop_f_brandId', $brandId, time() + 604800);
				$_COOKIE['shop_f_brandId'] = $brandId;
			}
		}

		$filter = $_COOKIE['shop_f_filter'];
		$filter = json_decode($filter, true);

		if (!is_array($filter)) {
			$filter = array();
		}

		$sortby = !empty($_COOKIE['shop_f_sortby']) ? $_COOKIE['shop_f_sortby'] : null;
		$sortasc = !empty($_COOKIE['shop_f_sortasc']) ? $_COOKIE['shop_f_sortasc'] : null;
		$rowsPerPage = !empty($_COOKIE['shop_f_rowsPerPage']) ? intval($_COOKIE['shop_f_rowsPerPage']) : 12;
		$brandId = !empty($_COOKIE['shop_f_brandId']) ? intval($_COOKIE['shop_f_brandId']) : null;

		$this->view->assign('filter', $filter);
		$this->view->assign('sortby', $sortby);
		$this->view->assign('sortasc', $sortasc);
		$this->view->assign('rowsPerPage', $rowsPerPage);
		$this->view->assign('brandId', $brandId);
		$rows = array();
		$page_links = '';
		$total_pages = 0;

		list($rows, $page_links, $total_pages) = Model_Frontend_Categories::search($rowsPerPage, $srchWhat, $sortby, $sortasc, $brandId, $filter, CURRENT_LANG_ID);
		$this->view->assign('goods', $rows);
		$this->view->assign('pages_list', $page_links);
		$this->view->assign('total_pages', $total_pages);
	}

	public function action_childs() {
		echo json_encode(Model_Frontend_Categories::show_categories($this->request->param('id')));
		exit();
	}

	public function action_tree() {
		echo json_encode(Model_Frontend_Categories::show_categories($this->request->param('id')));
		exit();
	}

	private function filters() {

	}

    public function action_getLastNewGoods()
    {
        $res = $this->model->getLastNewGoods();
        die(Kohana::debug($res));
    }
}
