<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */
class Controller_Frontend_Paymethods extends Controller_Content
{
    public $secretcode = 0;
    
    public function action_acceptmoney()
    {
        // Kohana_log::instance()->add('хеши', Kohana::debug( $_POST) );
        if( $_POST['RETURN_RESULT'] != 20 ) return false;

        $id = IN_PRODUCTION ? $_POST['RETURN_CLIENTORDER'] : 79;
        $order = Model_Orders::load( $id );
        
        $payment = Model_PayMethods::instance()->load( $order['payment_method_id'] );
        
        /* находим и сравниваем кеши. */
        $returnHash =
            $_POST['RETURN_MERCHANT']
            .':'.$_POST['RETURN_ADDVALUE']
            .':'.$_POST['RETURN_CLIENTORDER']
            .':'.$_POST['RETURN_AMOUNT']
            .':'.$_POST['RETURN_COMISSION']
            .':'.$_POST['RETURN_UNIQ_ID']
            .':'.$_POST['TEST_MODE']
            .':'.$_POST['PAYMENT_DATE']
            .':'.$payment['secretcode']
            .':'.$_POST['RETURN_RESULT']
        ;

        if( $_POST['RETURN_HASH'] != md5($returnHash) ){ Kohana_log::instance()->add('хеши не совпадают', Kohana::debug( $_POST['RETURN_HASH'] != md5($returnHash) ) ); return false;}
                
        /* Обновляем заказ как оплачен. */
        $status_history = unserialize($order['ser_status_history']);
                
        $status_history[] = array(
            'date' => time(),
            'status' => 1,
            'comment' => $order['comments'].' Оплачен через money.ua. UNIQ_ID - '.$_POST['RETURN_UNIQ_ID'],
        );
        
        DB::update('shop_orders')
            ->set(array(
                'status' => 1,
                'comments' => $order['comments'].' Оплачен через money.ua. UNIQ_ID - '.$_POST['RETURN_UNIQ_ID'],
                'ser_status_history' => serialize($status_history)
            ))
            ->where('id', '=', $order['id'] )
            ->execute()
        ;
        
        Kohana_log::instance()->add('log izmeneniya statysa', Kohana::debug(array(
                'status' => 1,
                'comments' => $order['comments'].' Оплачен через money.ua. UNIQ_ID - '.$_POST['RETURN_UNIQ_ID'],
                'ser_status_history serialized' => serialize($status_history),
                'ser_status_history' => $status_history
            )));
        /* и отправим письмецо админу пусть узнает */

        /* и переменные для письма */
//        $user = Session::instance()->get('authorized');
        $site = 'http://'.$_SERVER['HTTP_HOST'];
        $siteTitle = Kohana::config('shop')->siteTitle;
        $order_id = $order['id'];

//die('123');

//        $shipment_method['text'] = $shipment_method['letter_description'] == 1 ?
//            '<br />'.$shipment_method['text'].'<br />'
//            :
//            '';
        $payment_method['text'] = $payment_method['letter_description'] == 1 ?
            '<br />'.$payment_method['text'].'<br />'
            :
            '';
        $discount = $order['discount'];
        /**/
        $to = array(Kohana::config('shop')->send_to);
        if( Kohana::config('shop')->sendEmailToDeveloper == 1) $to['cc'] = array(Kohana::config('shop')->EmailDeveloper, 'Chimera CMS 2');
        
        $from = array( Kohana::config('shop')->send_from, Kohana::config('shop')->send_from_title );
        $subject = 'Оплачен заказ №'.$order['id'].' на сайте '.$site;
        $message = include(dirname(__FILE__).Kohana::config('shop')->mail_template);
        $message = '<h3>Оплачено</h3><br />Номер транзакции - '.$_POST['RETURN_UNIQ_ID'].'<br />'
            .$message;


        if( !Email::send($to, $from, $subject, $message, TRUE) ) Kohana_log::instance()->add('email send error in '.__FILE__, Kohana::debug( $to, $from, $subject, $message ) );
        die('OK');
    }

    public function action_deny()
    {
        Kohana_log::instance()->add('post deny', Kohana::debug( $_POST ) );
        $this->template = 'shop_deny.tpl';
    }

}
?>