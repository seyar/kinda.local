<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Frontend_Order extends Controller_Content {

    public $template = 'shop_submit.tpl';

    static $images_folder;
    static $image_width;
    static $image_height;
    static $image_prev_width;
    static $image_prev_height;

    public function before()
    {
        parent::before();
        $this->model = new Model_Frontend_Order();        
        $this->check_status();
    }

    public function action_index()
    {
        $this->page_info = array(
                        'page_title'=>I18n::get('Order Confirmation'),
                );

        if ( !Cookie::get('cart_global_amount') || !Cookie::get( 'cart_global_sum'))
            Request::instance()->redirect(  $_SERVER['HTTP_REFERER'] );

        Cookie::set( 'shipment', $shipment = Arr::get($_POST, 'shipment', Cookie::get('shipment')) );
        
        Cookie::set( 'payment', $payment = Arr::get($_POST, 'payment', Cookie::get('payment')) );

        $user_data = Session::instance()->get('authorized');
        $customer = Model_Customers::load($user_data['id']);
        $discount = 0;

        $shipment_cost = Controller_ShipMethods::getCost($shipment, str_replace(',', '.', Cookie::get( 'cart_global_weight')), (Cookie::get( 'cart_global_sum')- $discount ));
        $payment_cost = Controller_PayMethods::getCost($payment, str_replace(',', '.', (Cookie::get( 'cart_global_sum')- $discount )));

        $shipment_methods = Model_Shop::shipment_methods();
        $payment_method = Model_PayMethods::instance()->load($payment);
        
        // проверка возможности доставить заказ при выбранном способе оплаты
        if( !is_array($payment_method['forShipment']) ) 
        {
            Messages::add( i18n::get('For the chosen method of payment, we can not deliver the order' ) );
            Request::instance()->redirect(  $_SERVER['HTTP_REFERER'] );
        }
        elseif( is_array($payment_method['forShipment']) && implode(',',$payment_method['forShipment']) == '' && current($payment_method['forShipment']) == '' )
        {
            Messages::add( i18n::get('For the chosen method of payment, we can not deliver the order' ) );
            Request::instance()->redirect(  $_SERVER['HTTP_REFERER'] );
        }
        elseif( array_search($shipment, $payment_method['forShipment']) === FALSE && implode(',',$payment_method['forShipment']) != '') //проверка можно ли доставить при данном методе оплаты
        {
            $av_shipments = Model_ShipMethods::getMainItem(array('id' => DB::expr('('.implode(',',$payment_method['forShipment']).')') ), 'IN', TRUE);
            
            $av_shipments_str = '';
            foreach($av_shipments as $item) $av_shipments_str .= $item['name'].', ';
            Messages::add( i18n::get('For the chosen method of payment are available: ').rtrim($av_shipments_str,', ') );            
            
            unset($av_shipments);
            Request::instance()->redirect(  $_SERVER['HTTP_REFERER'] );
        }
        
        $this->view->assign('shipment_method', $shipment_methods[$shipment]);
        $this->view->assign('shipment_cost', $shipment_cost);
        $this->view->assign('payment_cost', $payment_cost);
        $this->view->assign('payment_method', $payment_method);
        
        Cookie::set( 'address_id', $default_address = Arr::get($_POST, 'shipment_address_id', $customer['default_address_id']) );
        Cookie::set( 'pay_address_id', $pay_default_address = Arr::get($_POST, 'payment_address_id', 
                                            Arr::get($customer, 'default_pay_address_id', 
                                                    $customer['default_address_id'] )));

        $this->view->assign('address_id', $default_address);
        $this->view->assign('pay_address_id', $pay_default_address);

        $this->view->assign('goods_amount', Cookie::get( 'cart_global_amount'));
        $this->view->assign('goods_sum', Cookie::get( 'cart_global_sum'));

        $total_sum = Cookie::get( 'cart_global_sum') + $shipment_cost - $discount;
        $total_sum += $payment_method['include_tax'] ? $tax : 0;
        $total_sum += $payment_method['plusToOrder'] ? $payment_cost : 0;

        $this->view->assign('total_sum', $total_sum);
        $this->view->assign('discount', $discount);
        $this->view->assign('customer', $customer);
        $this->view->assign('customer_address', Model_Customers::shipment_address_list($user_data['id']));

    }

    public function action_submit()
    {
        $user_data = Session::instance()->get('authorized');
        
        $customer = Model_Customers::load($user_data['id']);        
        $order_id = Model_Frontend_Order::create(
                    unserialize(Cookie::get('cart','')),
                    Cookie::get('shipment', NULL), Cookie::get('address_id', NULL),
                    Cookie::get('payment', NULL), Cookie::get('pay_address_id', NULL),
                    Cookie::get('cart_global_amount', NULL), Cookie::get('cart_global_sum', NULL),
                    $user_data['id'],
                    $customer['discount'],
                    $_POST['addParam']
                );

        $this->page_info = array(
                        'page_title'=>I18n::get('Order Submit'),
                );
        if ($order_id)
        {

            $this->view->assign('order_id',$order_id);
            $payment_method = Model_PayMethods::instance()->load( Cookie::get('payment', NULL) );
            $shipment_methods = Model_Shop::shipment_methods();
			
            /* отравляем письмецо*/
            $oOrders = new Model_Orders();
            $order = $oOrders->load($order_id);
            
            Model_Frontend_Order::ordersendmail($order, '', $payment_method, $shipment_methods[$order['shipment_method_id']]);
            /**/
            $this->template = 'shop_accept.tpl';
            
            if ($payment_method['invoice'])
                $this->template = 'shop_accept_download.tpl';

            $this->page_info = array( 'page_title'=> 'Заказ принят' );
//            $this->view->assign('obj', Cookie::get('cart_global_sum', NULL) );
            
            $this->view->assign('order', $order );

            Cookie::delete('cart');
            Cookie::delete('cart_global_sum');
            Cookie::delete('cart_global_amount');
            Cookie::delete('cart_global_weight');
            
            if( $payment_method['type'] == 2 )
            {
                $_POST['quick_orderid'] = $order_id;
                $_POST['quick_payment_method'] = $payment_method;
                $_POST['quick_shipment_method'] = $order['shipment_method_id'];
                
                echo Request::factory('/shop/cart/1/redirecttopayment')->execute();
                die();
            }

        }
        else
        {
            $this->template = 'shop_deny.tpl';
        }
    }

    public function action_archive()
    {
        $this->view->assign('order_status', Controller_Orders::$order_statuses);
        $this->template = 'shop_archive.tpl';

        $obj = Model_Frontend_Order::orders( array('Archive',) );
	$this->view->assign('obj', $obj );

        $this->page_info = array('page_title' => i18n::get('Completed orders') );
    }

    public function action_active()
    {
        $this->view->assign('order_status', Controller_Orders::$order_statuses);
        $this->template = 'shop_active.tpl';

        $obj = Model_Frontend_Order::orders( array('Accepted', 'Payed', 'Packed', 'Shipped', 'Executed',) );
	$this->view->assign('obj', $obj );

        $this->page_info = array('page_title' => i18n::get('Active orders') );
    }

    public function action_address()
    {

        $this->template = 'shop_address.tpl';
        $user_data = Session::instance()->get('authorized');
        
        $this->view->assign('customer', Model_Customers::load($user_data['id']));
        
        $customer_address = Model_Customers::shipment_address_list($user_data['id']);
        
        $this->view->assign('customer_address', current($customer_address));

        $this->view->assign('payment_methods', Model_Shop::payment_methods(array('status'=>1)));
        $this->view->assign('shipment_methods', Model_Shop::shipment_methods(array('status'=>1)));

        $this->page_info = array('page_title'=>I18n::get('Profile') );
    }

    public function action_address_ajax()
    {
        $this->template = 'shop_address_ajax.tpl';
        $user_data = Session::instance()->get('authorized');
        $this->view->assign('currentAddressId',  isset($_GET['curr']) ? intval($_GET['curr']) : 0);
        $this->view->assign('customer', Model_Customers::load($user_data['id']));
        $this->view->assign('customer_address', Model_Customers::shipment_address_list($user_data['id']));

    }

    public function action_address_edit()
    {
        $this->template = 'shop_address_edit.tpl';
        $user_data = Session::instance()->get('authorized');
        $this->view->assign('obj', Model_Frontend_Order::load_address( $user_data['id'], $this->request->param('id') ) );

        if( $this->request->param('id') )
            $this->page_info = array('page_title'=>I18n::get('Edit address') );
        else
            $this->page_info = array('page_title'=>I18n::get('Add new address') );
    }

    public function action_addresssave()
    {
        $autorized = Session::instance()->get('authorized');
        if( $this->model->save_address( $this->lang, $this->request->param('id') ) )
        {
            DB::update('shop_customers')->set(array('name' => $_POST['name']))->where('id', '=', $_POST['customerId'])->execute();
            Model_Registration::edit_item($_POST['customerId'], array('name' => $_POST['name']) );
            if( $_POST['name'] )
                Session::instance()->set('authorized', array('id'=>$autorized['id'],'name'=>$_POST['name'],'email'=> $autorized['email'])  );
            
            $this->request->redirect( 'shop/order/address' );
        }
        else
        {
            Messages::add($this->model->errors);

            $this->action_address_edit();
        }

    }

    public function action_address_delete()
    {
	$this->model->address_delete( $this->request->param('id') );
        Request::instance()->redirect( 'shop/order/address' );
    }

    public function action_print()
    {        
        $search_currencies_list = Model_Shop::shop_currencies();
        $this->view->assign('search_currencies_list', $search_currencies_list );

        $this->page_info = array('page_title'=>'Квитанция об оплате');
        $order = Model_Orders::load( $this->request->param('id') );
        $payment_method = Model_PayMethods::instance()->load( $order['payment_method_id'] );

        $discount = 0; //$order['discount'];        
        $this->view->assign('obj', $order);
        $this->view->assign('payment_methods', Model_Shop::payment_methods());
        $this->view->assign('postavshik', Kohana::config('shop')->postavshik);
        $this->template = 'shop_print.tpl';
    }

    public function action_save_default()
    {
        if(Model_Frontend_Order::save_default())
        {
            Messages::add(i18n::get('Изменения сохранены'));
            Request::instance()->redirect( 'shop/order/address' );
        }
        else
            Messages::add(i18n::get('Some error'));
    }

    
    public function check_status()
    {        
        $this->model->check_status();        
    }
    
}