<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Brands extends Controller_Shop {

    //public $template		= 'brands_list.tpl';
    public $template		= 'shop_brands';

    public $module_title	= 'Brands';
    public $module_desc_short   = 'Shop Descr Short';
    public $module_desc_full	= 'Shop Descr Full';

    public $brands_folder;
    public $brand_image_width;
    public $brand_image_height;

    public function before()
    {

        parent::before();

        $this->model = Model::factory('Brands');
        //die(Kohana::debug($this->model));
        //$this->request->controller .= ':'.MODULE_SUBCONTROLLER;

        $shopConfig = Kohana::config('shop');

        $this->brand_images_folder = $shopConfig->brand_images_folder;
        $this->brand_image_width = $shopConfig->brand_image_width;
        $this->brand_image_height = $shopConfig->brand_image_height;
//        $this->view->assign('brand_images_folder', $this->brand_images_folder);
    }

    public function action_index()
    {

//        $pagination = Pagination::factory(array(
//		    'total_items'    => $this->model->count(),
//		    'items_per_page' => 10,
//	    ));

//        $rows = $this->model->findAll($pagination->items_per_page, $pagination->offset);
//
//	$this->view->assign('rows', $rows);
//
////	$this->view->assign('pages_list', $page_links );
//	$this->view->assign('total_pages', $pagination->total_pages );


//die('11');
        $this->template = View::factory('shop_brands');

        $rows = $current_category = array(); $page_links = ''; $total_pages = 0;

        $rows = array();
        $page_links = '';
        $total_pages = 0;
        $where = array();

;

        list($this->template->pagination, $this->template->rows) = Admin::model_pagination('', 'shop_brands',
                        array('id', 'name', 'url', 'logo'),
                        $where
        );

    }


    public function action_edit()
    {
          $this->template = View::factory('shop_brands_edit.php');
//        $this->template = 'shop_brands_edit.php';
//
        //die(Kohana::debug($this->model));
        $obj = array();
        if( $this->request->param('id') )
        {
            $obj = $this->model->load($this->request->param('id'));
            //die(Kohana::debug($obj));
        }
        $this->template->obj=$obj;
//
//	$this->view->assign('obj', $obj );
    }

    private function _save()
    {

        $validator = Validate::factory($_POST)
                                    ->rule('name', 'not_empty')
                                    ->filter('name', 'trim');
        if( !$validator->check() )
        {
            $this->view->assign('errors', $validator->errors('validate'));
            return $this->action_edit();
        }
        //die(Kohana::debug($_POST));
        $id = $this->request->param('id');
        $translitName = Controller_Admin::transliterate($_POST['name']);

        if($id) $obj = $this->model->find($id);
        $filename = '';
        //die(Kohana::debug($_FILES));
        if( $_FILES['logo']['tmp_name'] != '')
        {
            if( $id )
            {
                $oldFilename = MEDIAPATH.
                                  $this->brand_images_folder .
                                  "/" .$obj['logo'];

                if(in_array(pathinfo($oldFilename, PATHINFO_EXTENSION), array('jpg', 'gif', 'png')))
                {
                    if(file_exists($oldFilename))
                        unlink($oldFilename);
                }
            }

            if( !file_exists(MEDIAPATH . $this->brand_images_folder) )
            {
                $dir = trim($this->brand_images_folder, '/');
                $dirs = explode('/', $dir);
                chdir(MEDIAPATH);
                for($i = 0, $len = count($dirs); $i<$len; $i++)
                {
                    if(!file_exists($dirs[$i]))
                    {
                        mkdir($dirs[$i]);
                        chmod($dirs[$i], 0755);
                    }
                    chdir($dirs[$i]);
                }
            }

            $ext = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
            if(!in_array($ext, array('jpg', 'gif', 'png', 'jpeg'))) die();
            $filename = $translitName . '.' . $ext;
            $targetFile = MEDIAPATH.
                          $this->brand_images_folder . "/" .
                          $filename;

            move_uploaded_file($_FILES['logo']['tmp_name'], $targetFile);
	    // Resize Image
            $this->image = Image::factory( $targetFile );
            $this->image->resize($this->brand_image_width, $this->brand_image_height);
            $this->image->save($targetFile, 90);
        }

        if( !$id )
        {
            $res = $this->model->insert(
                                array(
                                    'name' => $_POST['name'],
                                    'url'  => $translitName,
                                    'logo' => $filename
                                )
                            );
            $id = $res['lastInsertId'];
        }
        else
        {

            if($obj['name'] != $_POST['name'])
            {
                $filename =  Controller_Admin::transliterate($_POST['name']).
                                            '.' . pathinfo($obj['logo'], PATHINFO_EXTENSION);
                if($obj['logo'] != '' && file_exists(MEDIAPATH.$this->brand_images_folder . "/" .$obj['logo']))
                {

                    rename(
                            MEDIAPATH.$this->brand_images_folder . "/" . $obj['logo'],
                            MEDIAPATH.$this->brand_images_folder . "/" . $filename
                        );

                }

            }
            $data = array('name' => $_POST['name'], 'url'  => $translitName);
            if($filename != '') $data['logo'] = $filename;
            //die(kohana::debug($data));
            $this->model->update($data, array('id', '=', $id));
        }
        return $id;

    }

    public function action_update()
    {
        //die(print_r($_POST));
        $id = $this->_save();
        $this->redirect_to_controller($this->request->controller.'/'.$id.'/edit');
    }

    public function action_save()
    {
        $this->_save();
        $this->redirect_to_controller($this->request->controller);
    }

    public function action_delete()
    {
	if ($this->request->param('id'))
        {
            $id = intval($this->request->param('id'));
            if(!$id) return;
            $obj = $this->model->find($id);
            if($obj['logo'] != '' && file_exists(MEDIAPATH.$this->brand_images_folder . "/" .$obj['logo']))
                unlink(MEDIAPATH.$this->brand_images_folder . "/" .$obj['logo']);
	    $this->model->delete( $this->request->param('id') );
        }
	elseif (count($_POST['chk']))
        {
            $ids = array_map('intval', $_POST['chk']);
            $rows = $this->model->find($ids);
            foreach($rows as $obj)
            {
                if($obj['logo'] != '' && file_exists(MEDIAPATH.$this->brand_images_folder . "/" .$obj['logo']))
                    unlink(MEDIAPATH.$this->brand_images_folder . "/" .$obj['logo']);
            }

	    $this->model->delete( $ids );
        }

        $this->redirect_to_controller($this->request->controller);
    }

    /*TODO
     * создать хелпер ImageUploader
     * ImageUploader::upload(
     *                      $targetFile,       //string
     *                      $cropWidth,        //int
     *                      $cropHeight,       //int
     *                      $prevWidth,        //int
     *                      $prevHeigth,       //int
     *                      $watermarkPath,    //string
     *                  )
     * @return  filename
     */

    // Images Actions

    public function action_removelogo()
    {
        $id = intval($this->request->param('id'));
        if( !$id ) return;

        $obj = $this->model->find($id);

        if(file_exists(MEDIAPATH.$this->brand_images_folder . "/" .$obj['logo']))
                unlink(MEDIAPATH.$this->brand_images_folder . "/" .$obj['logo']);

	$this->model->update(array('logo' => ''), array('id', '=', $id));
	$this->redirect_to_controller($this->request->controller.'/'.$id.'/edit');
    }


}