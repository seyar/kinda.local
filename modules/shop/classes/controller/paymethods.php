<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_PayMethods extends Controller_Shop {

    //public $template		= 'paymethods_list.tpl';
    public $template		= 'shop_paymentoptions';

    public $module_title        = 'Payment Methods';
    public $module_desc_short   = 'Shop Descr Short';
    public $module_desc_full	= 'Shop Descr Full';
    
    public function before()
    {
        parent::before();
        $this->model = new Model_PayMethods();

        //die(Kohana::debug($this->model));

        //die('ok!');
        //$this->request->controller .= ':'.MODULE_SUBCONTROLLER;
    }

    public function action_index()
    {
        $this->template = View::factory('shop_paymentoptions');

        $rows = $current_category = array(); $page_links = ''; $total_pages = 0;

        
//	list($rows, $pagination, $total_pages) = $this->model->show();
//        $this->template->rows = $rows;
//        $this->template->pagination = $pagination;


        $rows = array();
        $page_links = '';
        $total_pages = 0;
        $where = array();

        list($this->template->pagination, $this->template->rows) = Admin::model_pagination('', 'shop_payment_methods',
                        array('id', 'name', 'print_invoice', 'margin', 'status'),
                        $where, array('orderid','ASC')
        );
    }

    public function action_delete()
    {
//	if ($this->request->param('id'))
//        {
//            $obj = $this->model->load($this->request->param('id'));
//	    $this->model->delete( $this->request->param('id') );
//	    $this->model->setMethod($obj['fs_name'])->delete( $obj['settings_id'] );
//        }
//        die(Kohana::debug($_POST));
//	if (count($_POST['chk']))
	if (isset($_POST['chk']))
        {
            //$first = current($_POST['chk']);
            //die(Kohana::debug($first));
            //$obj = $this->model->load($first);
	    $this->model->delete_list( $_POST['chk'], null );
//	    $this->model->setMethod($obj['fs_name'])->delete_list( $_POST['chk'] );
        }

        $this->redirect_to_controller($this->request->controller);
    }

    public function action_edit()
    {
        $obj = $this->model->load( $this->request->param('id') );
        $filename = 'shop_payedit_'.strtolower($obj['fs_name']);                
        $filename = file_exists( dirname( __FILE__).'/../../views/'.$filename.'.php' ) ? 'shop_payedit_'.strtolower($obj['fs_name']) : 'shop_payedit';
        
        $this->template = View::factory($filename);
        
        $this->template->obj = $obj;
        
        $this->template->shipments = Model_Shop::shipment_methods();
    }

    public function action_save()
    {
        $id = $this->request->param('id');
        if( $id == 0)
            $res = $this->model->add( $this->lang );
        else
            $res = $this->model->save( $this->lang, $id );
                
	    $this->redirect_to_controller($this->request->controller);
    }

    public function action_update()
    {
        $id = $this->request->param('id');
        if( $id == 0)
            $res = $this->model->add( $this->lang );
        else
            $res = $this->model->save( $this->lang, $id );
        
        if($res)        
            $this->redirect_to_controller( $this->request->controller.'/'.$res.'/edit' );
        else
            $this->redirect_to_controller( $this->request->controller );
        
	
    }

    public function action_step1()
    {
        $this->template = View::factory('paymethods_step1');
        $this->template->payTypes = Kohana::config('shop')->payTypes;
    }
    
    public function action_step2()
    {        
        $payments = Model_PayMethods::getPaymentGroups( array('type' => $_POST['getFromPayment']) );        
        $this->template = View::factory('paymethods_step2');
        $this->template->payments = $payments;
    }

    public function action_step3()
    {        
        $payment = Model_PayMethods::getPaymentGroups( array('id' => $_POST['getFromPayment']), FALSE );
        $shipments = Model_Shop::shipment_methods();
        $fs_name = $payment['fs_name'];        
        $this->template = View::factory('shop_payedit_'.strtolower($payment['fs_name']) );
        $this->template->fs_name = $fs_name;
        $this->template->payments = $payment;
        $this->template->shipments = $shipments;
    }

    static public function getCost($method, $sum = 0, $weight = 0)
    {
        $oPaymethods = new Model_PayMethods;
        $obj = $oPaymethods->load($method);
        return $oPaymethods->setMethod( $obj['fs_name'] )->getCost( $obj, $sum, $weight );
    }

    public function action_status()
    {
        
        if ($this->request->param('id'))
            $this->model->status( $this->request->param('id') );

        elseif (count($_POST['chk']))
            $this->model->status_list( $_POST['chk'] );

        $this->redirect_to_controller($this->request->controller);
    }
    
    function action_updateOrderId()
    {
        if( $this->model->updateOrderId( $_GET['ids'] ))
                die('1');
        else
                die('error');
    }
}