<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Currencies extends Controller_Shop {

//    public $template		= 'currencies_list.tpl';
    public $template		= 'shop_currency';

    public $module_title	= 'Currencies';    
    public $module_desc_short   = 'Shop Descr Short';
    public $module_desc_full	= 'Shop Descr Full';
    
    public function before()
    {


        parent::before();
        $this->model = new Model_Currencies();
//        $this->request->controller .= ':'.MODULE_SUBCONTROLLER;
    }

    public function action_index()
    {
        $this->template = View::factory('shop_currency');
        $rows = $current_category = array(); $page_links = ''; $total_pages = 0;

        $rows = array();
        $page_links = '';
        $total_pages = 0;

        $where = array();
        list($this->template->pagination, $this->template->rows) = Admin::model_pagination(NULL, 'shop_currencies',
                        array('id','name','code' ,'iso_code','rate'),
                        $where
        );
    }

    public function action_delete()
    {
	if ($this->request->param('id'))
	    $this->model->delete( $this->request->param('id') );

	elseif (count($_POST['chk']))
	    $this->model->delete_list( $_POST['chk'] );

        $this->redirect_to_controller($this->request->controller);
    }

    public function action_edit()
    {
        $this->template = View::factory('shop_currency_edit');
        $this->template->obj = $this->model->load( $this->request->param('id') );

    }

    public function action_save()
    {
	if( $this->model->save( $this->lang, $this->request->param('id') ) )
	{
	    $this->redirect_to_controller($this->request->controller);
	}
	else
	{
	    $this->view->assign('errors', $this->model->errors);
	    $this->action_edit();
	}
    }

    public function action_update()
    {
	if( $this->model->save( $this->lang, $this->request->param('id') ) )
	{
	    $this->redirect_to_controller($this->request->controller.'/'.$this->request->param('id').'/edit');
	}
	else
	{
	    $this->view->assign('errors', $this->model->errors);
	    $this->action_edit();
	}
    }



//    public function action_statusUpdate()
//    {
//
//
//        $var = $this->request->param('id');
//
//             ;
//        DB::update('shop_customers')
//        ->set(array('status' => DB::expr('! status')))
//                       ->where('id', '=', $var)
//                       ->execute();
//
//        //die(Kohana::debug($this));
//        die('1');
//
//    }
}