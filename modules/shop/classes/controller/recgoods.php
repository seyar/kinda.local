<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */
class Controller_Recgoods extends Controller_Admin
{
//    public $template ;

    public function before()
    {
        parent::before();        
        $this->model = new Model_Recgoods();
    }

    public function action_index()
    {
        
    }

    public function action_getGoods()
    {
        list($res) = Model_Frontend_Categories::goods( $this->request->param('id'),10000 );
        die(print( json_encode($res) ));
    }
}
?>