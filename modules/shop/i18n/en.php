<?php defined('SYSPATH') or die('No direct script access.');

return array
(
'Shop' => 'On-line shop',
    'Shop Descr Short' => 'Module «On-line Store is designed for sales through the site, as well as for organizing a systematic catalog of products.',
    'Shop Descr Full' => 'Module «On-line Store is designed for sales through the site, as well as for organizing a systematic catalog of products. <br />
To create and edit categories and subcategories, go to "Categories". <br />
All occupants in the online store products are "Goods". There is the possibility of creating, deleting and editing products. <br />
All customer base is located in the "Clients". <br />
In paragraphs "Payment Methods" and "Delivery Methods" have the opportunity to create and configure appropriate ways. <br />
Category "Brands" allows you to order goods by producers. <br />
Item "Currency" includes the ability to manage currency online store. <br />
In the "Orders" has the ability to view all of past and current orders through the website. ',

);