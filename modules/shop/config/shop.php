<?php
defined( 'SYSPATH' ) OR die( 'No direct access allowed.' );

return array
    (
    'lang' => 'ru', // Default the  language.
    'path' => dirname( __FILE__ ) . '/../views/', // Admin templates folder.

    'brand_images_folder' => 'shop/brands',
    'brand_image_width' => 121,
    'brand_image_height' => 110,
    'images_folder' => 'shop',
    'image_width' => 1440,
    'image_height' => 1024,
    'image_prev_width' => 200,
    'image_prev_height' => 200,
    'max_images_count' => 6,
    'rows_frontend' => 12,
    'root_category_id' => 1,
    'scu_like_id' => true,
    // currencies section
    'default_currency' => '1',
    'default_currency_char' => '&#8372;', // ну это же фронтенд currency char 
    'backend_currency_char' => '$', // по идее default currency char 
    'frontend_currency' => '1',
    'active_currencies' => array
        (
        '1'
    ),
    'currency_rate_source' => 'www.oanda.com',
    'use_autoupdate_currency_rates' => true,
    // price categories section
    'default_category' => '1',
    // templates section
    'start_template' => 'index.tpl',
    'watermark' => dirname( __FILE__ ) . '/../../../test.jpg',
    'send_from' => 'noreply@' . $_SERVER['HTTP_HOST'],
    'subject' => 'Message from',
    'send_from' => 'noreply@' . $_SERVER['HTTP_HOST'],
    'send_from_title' => $_SERVER['HTTP_HOST'],
    'send_to' => 'kinda@home.cris.net',
    'mail_template' => '../../../../views/ordersendmail.tpl',
    'siteTitle' => 'Kinda',
    'main_shipments' => array( 'Ukrpost', 'Courier', 'Selfdelivery', 'Newpost' ),
    'payTypes' => array(
        1 => array(
            'title' => 'credit card',
            'description' => 'Receiving credit card payments through different payment systems or manual processing of payments by credit cards (the user enters the card details when placing your order, and then you have to manually process the payment through any payment system).',
        ),
        2 => array(
            'title' => 'through online payment system',
            'description' => 'Accept payments such as payment systems WebMoney, Yandex.Money, PayPal and others.',
        ),
        3 => array(
            'title' => 'Manual processing of payments',
            'description' => 'Payments that you control it manually. For example, payment by cash, payment on receipt of the account for legal entities, payment and so on. ',
        ),
    ),
    'PAYMENT_RETURNRES' => 'http://' . $_SERVER['HTTP_HOST'] . '/shop/paymethods/acceptmoney',
    'PAYMENT_RETURN' => 'http://' . $_SERVER['HTTP_HOST'] . '/',
    'PAYMENT_RETURNFAIL' => 'http://' . $_SERVER['HTTP_HOST'] . '/shop/paymethods/deny',
    'addGoodsModel' => 'Model_Recgoods',
    'addGoodsModelFront' => 'Model_Frontend_Recgoods',
    'addGoodsMethSave' => 'saveFromCat',
    'addGoodsMethGet' => 'getItem',
    'sendEmailToDeveloper' => 0,
    'EmailDeveloper' => 'seyarchapuh@gmail.com',
    'recGoodsRandomize' => 1,
    
    'quickorderUserId' => 1,
    
    'parent_module' => 'Model_Registration',
    'parent_method' => 'changeStatus',
    
    /* for manual rate mode */
//    'priceRoundingBorder' => 5, 
    'archiveID' => 114380, 
    
    'topCatsToNew' => array(2,3,4),
    'countTopCats' => 4,
    
    'frontendStatuses' => array(
        'active',
        'allow_to_order',
    ),
    
    'stpreApproximateText' => array
    (
        0 => 'today',
        1 => 'tomorrow',
    ),
);
