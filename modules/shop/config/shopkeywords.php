<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */

return array(
    'keywordSource' => 'microline.ua: %category% %good%, купить %category% %good% в Украине, Киеве, Харькове, Днепропетровске, Одессе, Донецке, Запорожье, Симферополь, Севастополь, Крым. Цена, характеристики, описание, продажа.',
    'descriptionSource' => "Купить %category% %good% в интернет-магазине Microline.ua. Доставка по Украине, гарантия, лучшая цена.",
    
    'keywords' => array(
        'Ноутбуки' => 'Ноутбук',
        'asus' => 'asusik',
    ),
);
?>