<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
            'name'             => 'Shop',
            'fs_name'          => 'shop',
            'panel_serialized' => 'a:2:{s:4:"info";a:3:{s:4:"name";s:4:"Shop";s:4:"link";s:19:"/admin/modules/shop";s:5:"image";s:24:"/admin/modules/shop/logo";}s:5:"links";a:8:{i:0;a:2:{s:4:"name";s:5:"Goods";s:5:"value";s:6:":goods";}i:1;a:2:{s:4:"name";s:9:"Customers";s:5:"value";s:10:":customers";}i:2;a:2:{s:4:"name";s:6:"Orders";s:5:"value";s:7:":orders";}i:3;a:2:{s:4:"name";s:10:"Categories";s:5:"value";s:11:":categories";}i:4;a:2:{s:4:"name";s:6:"Brands";s:5:"value";s:7:":brands";}i:5;a:2:{s:4:"name";s:10:"Currencies";s:5:"value";s:11:":currencies";}i:6;a:2:{s:4:"name";s:15:"Payment Methods";s:5:"value";s:11:":paymethods";}i:7;a:2:{s:4:"name";s:16:"Shipment Methods";s:5:"value";s:12:":shipmethods";}}}',
            'useDatabase'      => true
        );