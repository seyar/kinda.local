
DROP TABLE IF EXISTS `shop_brands`;
CREATE TABLE IF NOT EXISTS `shop_brands` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_categories`;
CREATE TABLE IF NOT EXISTS `shop_categories` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parents_serialized` blob NOT NULL,
  `display_mode` enum('subcat','subcat_and_goods','goods') NOT NULL,
  `ser_addFields` blob,
  `description` text,
  `seo_url` varchar(255) NOT NULL,
  `seo_keywords` varchar(255) NOT NULL,
  `seo_description` varchar(255) NOT NULL,
  `order_id` int(10) unsigned NOT NULL default '9998',
  `status` enum('verified','unverified') NOT NULL default 'unverified',
  PRIMARY KEY  (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `shop_cat2languages` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_currencies`;
CREATE TABLE IF NOT EXISTS `shop_currencies` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(45) NOT NULL,
  `code` varchar(5) NOT NULL,
  `iso_code` smallint(5) unsigned NOT NULL default '0',
  `rate` float unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_customers`;
CREATE TABLE IF NOT EXISTS `shop_customers` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `price_category_id` int(255) unsigned NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `fax` varchar(45) NOT NULL,
  `default_address_id` int(10) unsigned NOT NULL,
  `default_pay_address_id` int(10) unsigned NOT NULL,
  `shipment_method_id` int(10) unsigned NOT NULL,
  `payment_method_id` int(10) unsigned NOT NULL,
  `comment` text NOT NULL,
  `news_subscribe` tinyint(1) unsigned NOT NULL default '1',
  `contractor_id` int(10) unsigned NOT NULL,
  `is_taxpayer` tinyint(1) unsigned NOT NULL default '0',
  `discount` tinyint(3) unsigned NOT NULL default '0',
  `date_create` datetime NOT NULL,
  `turnover` int(10) unsigned NOT NULL default '0',
  `status` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `customer2payment` (`payment_method_id`),
  KEY `customer2shipment` (`shipment_method_id`),
  KEY `customer2price_categories` (`price_category_id`),
  KEY `default_address_id` (`default_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_goods`;
CREATE TABLE IF NOT EXISTS `shop_goods` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `categories_id` int(10) unsigned NOT NULL,
  `brand_id` int(10) unsigned NOT NULL default '0',
  `alter_name` varchar(255) NOT NULL default '0',
  `scu` varchar(255) NOT NULL default '0',
  `name` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `full_description` text NOT NULL,
  `is_bestseller` tinyint(1) NOT NULL default '0',
  `is_new` tinyint(1) NOT NULL default '0',
  `rating` decimal(4,2) unsigned NOT NULL default '0.00',
  `show_on_startpage` tinyint(1) NOT NULL default '0',
  `default_photo` varchar(100) NOT NULL,
  `start_date` datetime NOT NULL,
  `edit_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `cost` float unsigned default NULL,
  `weight` float unsigned default NULL,
  `quantity` int(11) NOT NULL,
  `guaranties_period` int(11) NOT NULL,
  `store_approximate_date` date default NULL,
  `seo_keywords` varchar(255) NOT NULL,
  `seo_description` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `status` enum('active','archive','out_of_store','unverified','allow_to_order') NOT NULL default 'unverified',
  `stars` int(10) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `categories2goods` (`categories_id`),
  KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_goods_properties`;
CREATE TABLE IF NOT EXISTS `shop_goods_properties` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `category_id` int(10) unsigned NOT NULL,
  `alias` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('int','float','date','text','varchar','select') NOT NULL default 'varchar',
  `desc` varchar(255) NOT NULL,
  `defaults` text NOT NULL,
  `is_searchable` tinyint(1) unsigned default NULL,
  `is_filterable` tinyint(1) unsigned NOT NULL default '0',
  `is_group` int(1) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `categories2goods_prop` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_goods_properties_values`;
CREATE TABLE IF NOT EXISTS `shop_goods_properties_values` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `properties_category_id` int(10) unsigned NOT NULL,
  `good_id` int(10) unsigned NOT NULL,
  `value_int` int(11) default NULL,
  `value_float` float default NULL,
  `value_date` date default NULL,
  `value_text` text,
  `value_varchar` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `id_goods` (`good_id`),
  KEY `goods_prop2values` (`properties_category_id`),
  KEY `goods2values` (`good_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_goods_reviews`;
CREATE TABLE IF NOT EXISTS `shop_goods_reviews` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `good_id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `rating` mediumint(9) NOT NULL,
  `username` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_goods` (`good_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_goods_votes`;
CREATE TABLE IF NOT EXISTS `shop_goods_votes` (
  `user_id` int(10) unsigned NOT NULL,
  `good_id` int(10) unsigned NOT NULL,
  KEY `user_id` (`good_id`,`user_id`),
  KEY `FK_shop_goods_votes_site_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_ordered_goods`;
CREATE TABLE IF NOT EXISTS `shop_ordered_goods` (
  `order_id` int(10) unsigned NOT NULL,
  `goods_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  KEY `order2order_goods` (`order_id`),
  KEY `goods2order_goods` (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_orders`;
CREATE TABLE IF NOT EXISTS `shop_orders` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `customer_id` int(10) unsigned NOT NULL,
  `shipment_method_id` int(10) unsigned NOT NULL,
  `payment_method_id` int(10) unsigned NOT NULL,
  `date_create` datetime NOT NULL,
  `currency_id` int(10) unsigned NOT NULL,
  `total_cost` float NOT NULL,
  `subtotal` float NOT NULL,
  `discount` float NOT NULL,
  `tax` float NOT NULL,
  `shipment_cost` float NOT NULL,
  `sum` float NOT NULL,
  `date_close` datetime NOT NULL,
  `weight` float NOT NULL,
  `shipment_name` varchar(100) NOT NULL,
  `shipment_address` tinytext NOT NULL,
  `billing_name` varchar(100) NOT NULL,
  `billing_address` tinytext NOT NULL,
  `ser_status_history` blob,
  `ser_details` blob,
  `comments` varchar(100) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `customers2orders` (`customer_id`),
  KEY `orders2shipments` (`shipment_method_id`),
  KEY `orders2payments` (`payment_method_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_payment_methods`;
CREATE TABLE IF NOT EXISTS `shop_payment_methods` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(45) NOT NULL,
  `cost` float default NULL,
  `invoice` tinyint(1) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_prices`;
CREATE TABLE IF NOT EXISTS `shop_prices` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `good_id` int(10) unsigned NOT NULL,
  `price_categories_id` int(10) unsigned NOT NULL,
  `currencies_id` int(10) unsigned NOT NULL,
  `price` float unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `prices_cat2price` (`price_categories_id`),
  KEY `prices2goods` (`good_id`),
  KEY `prices2currencies` (`currencies_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_price_categories`;
CREATE TABLE IF NOT EXISTS `shop_price_categories` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `cost` float NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_shipment_addresses`;
CREATE TABLE IF NOT EXISTS `shop_shipment_addresses` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip` varchar(45) NOT NULL,
  `address_line1` varchar(100) NOT NULL,
  `address_line2` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `is_company` tinyint(1) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `company_bank` varchar(255) NOT NULL,
  `company_mfo` varchar(255) NOT NULL,
  `company_okpo` varchar(255) NOT NULL,
  `company_phone` varchar(100) NOT NULL,
  `company_fax` varchar(100) NOT NULL,
  `news_subscribe` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `customer2address` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shop_shipment_methods`;
CREATE TABLE IF NOT EXISTS `shop_shipment_methods` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(45) NOT NULL,
  `cost` float NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `shop_categories`
  ADD CONSTRAINT `shop_cat2languages` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE;

ALTER TABLE `shop_customers`
  ADD CONSTRAINT `customer2price_categories` FOREIGN KEY (`price_category_id`) REFERENCES `shop_price_categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `shop_goods`
  ADD CONSTRAINT `categories2goods` FOREIGN KEY (`categories_id`) REFERENCES `shop_categories` (`id`) ON DELETE CASCADE;

ALTER TABLE `shop_goods_properties`
  ADD CONSTRAINT `categories2goods_prop` FOREIGN KEY (`category_id`) REFERENCES `shop_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `shop_goods_properties_values`
  ADD CONSTRAINT `goods2values` FOREIGN KEY (`good_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `goods_prop2values` FOREIGN KEY (`properties_category_id`) REFERENCES `shop_goods_properties` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `shop_goods_reviews`
  ADD CONSTRAINT `FK_review2goods` FOREIGN KEY (`good_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE;

ALTER TABLE `shop_goods_votes`
  ADD CONSTRAINT `FK_shop_goods_votes_site_users` FOREIGN KEY (`user_id`) REFERENCES `site_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_shop_goods_votes_shop_goods` FOREIGN KEY (`good_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `shop_ordered_goods`
  ADD CONSTRAINT `goods2order_goods` FOREIGN KEY (`goods_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `order2order_goods` FOREIGN KEY (`order_id`) REFERENCES `shop_orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `shop_orders`
  ADD CONSTRAINT `customers2orders` FOREIGN KEY (`customer_id`) REFERENCES `shop_customers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `orders2payments` FOREIGN KEY (`payment_method_id`) REFERENCES `shop_payment_methods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders2shipments` FOREIGN KEY (`shipment_method_id`) REFERENCES `shop_shipment_methods` (`id`) ON DELETE CASCADE;

ALTER TABLE `shop_prices`
  ADD CONSTRAINT `prices2currencies` FOREIGN KEY (`currencies_id`) REFERENCES `shop_currencies` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `prices2goods` FOREIGN KEY (`good_id`) REFERENCES `shop_goods` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `prices_cat2price` FOREIGN KEY (`price_categories_id`) REFERENCES `shop_price_categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `shop_shipment_addresses`
  ADD CONSTRAINT `customer2address` FOREIGN KEY (`customer_id`) REFERENCES `shop_customers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
