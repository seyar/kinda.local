<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>

<form method="post" action="">
    <!--floating block-->

    <div class="rel whiteBg floatingOuter_shop" id="contentNavBtns" style="height: 135px;">
        <div class="whiteblueBg absBlocks floatingInner" style="height: 135px;">
            <div class="padding:20px;">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>

                        <td class="button_clients" style="width: 40%;">
                            <button onclick="document.location.href='/admin/publications/edit';" type="button" class="btn blue" style="padding-left: 5px; ">
                                <span><span><? echo I18n::get('Customers'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><? echo I18n::get('With marked'); ?></td>

                        <td  style="padding-top:1px"><select id="list_action"><option value="delete"><? echo I18n::get('Delete'); ?></option></select></td>

                        <td style="text-align: right;"><button class="btn formUpdate" type="button">
                                <span><span><? echo I18n::get('Apply'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>

                        <td >
                            <?=
                            Form::select('rows_per_page', array(
                                        '10' => '10 ' . I18n::get('lines'),
                                        '20' => '20 ' . I18n::get('lines'),
                                        '30' => '30 ' . I18n::get('lines'),
                                            ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page'))"))
                            ?>
                        </td>
                    </tr>

                    <tr style="height: 10px;"></tr>
                    <tr >
                        <td colspan="6" style="border-top:1px solid #FFFFFF;">

                            <fieldset class="filter">
                                <legend style="font-weight: bold; color: black;"><?= I18n::get('Filter orders') ?></legend>
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td style="width:25%">
                                                <table>
                                                    <tr>
                                                        <td><input type="text" style="width: auto;" value="ID" name="shop_id"></td>
                                                        <td><input type="text" style="width: auto;" value="ID клиента" name="shop_id_client"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="padding-top: 6px;">
                                                            <?=
                                                            Form::select('rows_per_page_payment', array(
                                                                        '1' => I18n::get('Payment'), //'Заказ №1' . I18n::get('lines'),
                                                                        '2' => I18n::get('Payment'), //'Заказ №2' . I18n::get('lines'),
                                                                        '3' => I18n::get('Payment')//'Заказ №3' . I18n::get('lines'),
                                                                            ), Arr::get($_POST, 'rows_per_page_payment', Cookie::get('rows_per_page_shop'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page_payment'))"))
                                                            ?>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:25%">
                                                <table>
                                                    <tr>
                                                        <td><input type="text" style="width: auto;" value="Выписан на имя" name="shop_id_name"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 6px;">
                                                            <?=
                                                            Form::select('rows_per_page_delivery', array(
                                                                        '1' => I18n::get('Delivery'), //'Заказ №1' . I18n::get('lines'),
                                                                        '2' => I18n::get('Delivery'), //'Заказ №2' . I18n::get('lines'),
                                                                        '3' => I18n::get('Delivery')//'Заказ №3' . I18n::get('lines'),
                                                                            ), Arr::get($_POST, 'rows_per_page_delivery', Cookie::get('rows_per_page_delivery'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page_payment'))"))
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:25%">
                                                <table>
                                                    <tr>
                                                        <td><input type="text" style="width: auto;" value="<?= I18n::get('When placed') ?>" name="shop_id_date"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 6px;">
                                                            <?=
                                                            Form::select('rows_per_page_status', array(
                                                                        '1' => I18n::get('Status'), //'Заказ №1' . I18n::get('lines'),
                                                                        '2' => I18n::get('Status'), //'Заказ №2' . I18n::get('lines'),
                                                                        '3' => I18n::get('Status')//'Заказ №3' . I18n::get('lines'),
                                                                            ), Arr::get($_POST, 'rows_per_page_status', Cookie::get('rows_per_page_delivery'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page_status'))"))
                                                            ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:25%">
                                                <table >
                                                    <tr>
                                                        <td>
                                                            <?= I18n::get('Total') ?>
                                                        </td>
                                                        <td>
                                                            <input type="text" style="width: 52px;"  value="<?= I18n::get('From') ?>" name="shop_id_date_ot">
                                                        </td>
                                                        <td>-</td>
                                                        <td>
                                                            <input type="text" style="width: 52px;"  value="<?= I18n::get('to') ?>" name="shop_id_date_ot">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right;padding-top: 6px;" >
                                                            <button type="button" class="btn formUpdate" style="margin: 0px;">
                                                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </fieldset>

                        </td>
                    </tr>

                </table>

            </div>

        </div>

        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>
    <div class="rel whiteBg">
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
		<!-- **** -->
        это страница магазина на ней должны быть графики
<!--		<table width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab">
			<thead>
				<tr class="shop_head">
					<th style="padding:0 8px 0 5px;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll" /></th>
					<th style="" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>"><?= I18n::get('Order ID') ?></th>

					<th style="" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>"><?= I18n::get('Made out to')?></th>
					<th style="width: 86px;" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>"><?= I18n::get('Posted')?></th>
					<th style="" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>"><?= I18n::get('Total')?></th>

					<th style="width: 86px;" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>"><?= I18n::get('Payment')?></th>
					<th style="width: 100px;" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>"><?= I18n::get('Delivery')?></th>
					<th style="" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>"><?= I18n::get('Status')?></th>
				</tr>

			</thead>

			<tbody class="shop">
                <? for ($i = 1; $i <= 5; $i++): ?>
			<tr class="<?
			if ($i % 2)
				echo'even'; else
				echo 'odd';
?>">
			<td style="padding:0 8px 0 5px;" class="tCenter"><input type="checkbox" name="chk[<?= $item['id'] ?>]" value="<?= $item['id'] ?>"  class="listCheckbox checkBox" /></td>
			<td class="id"><a href="<?= $i ?>"><?= "Заказ №" . $i//$item['email'];                    ?></a></td>
			<td class="id_chet" style="padding:0 0 0 10px;"><a href="#">Валерий Михайлович</a></td>
			<td style="text-align:right">2010.07.16<br><span>18:09</span></td>
			<td>140 <span>грн.</span></td>
			<td class="id_bank">Банк переводом</td>
			<td style="text-align:center">Курьер.служба</td>
			<td style="text-align:center" class="status">
				<select size="2" name="status<?=$i?>">
				<option selected value="1">принят</option>
				<option value="0">не не принят</option>
			   </select>
			</td>
		</tr>


<? endfor; ?>
		</tbody>

	</table>-->

        <? if ($pagination): ?>
        <?= $pagination ?>
        <? endif; ?>
                                                                    <!-- **** -->
        <? //echo Kohana::debug($kohana_view_data);   ?>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>

</form>
