<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>
<div class="absBlocks preloader hidden" style="margin:-5px 0 0 -55px;z-index:4; left:50%; top: 50%;width:110px;height: 16px;"><img src="/static/admin/images/preloader.gif" alt=""/></div>
<form method="post" action="">
    <!--floating block-->

    <div class="rel whiteBg floatingOuter_shop" id="contentNavBtns" style="height: 43px;">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding:20px;">
                <table width="100%">
                    <tr>

                        <td class="button_clients" style="width:46%;text-align: left;">
                            <button onclick="document.location.href='/admin/paymethods/step1';" type="button" class="btn blue" style="padding-left: 5px; ">
                                <span><span><? echo I18n::get('Create New'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><? echo I18n::get('With marked'); ?></td>

                        <td style="padding-top:1px"><select id="list_action"><option value="delete"><? echo I18n::get('Delete'); ?></option></select></td>

                        <td style="text-align: right;"><button class="btn formUpdate" type="button">
                                <span><span><? echo I18n::get('Apply'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>

                        <td nowrap="nowrap">
                            <?=
                            Form::select('rows_per_page', array(
                                        '10' => '10 ' . I18n::get('lines'),
                                        '20' => '20 ' . I18n::get('lines'),
                                        '30' => '30 ' . I18n::get('lines'),
                                            ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page'))"))
                            ?>
                        </td>
                    </tr>
                    <tr >

                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>
    <div class="rel whiteBg">
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
                            <!-- **** -->
                            <table width="100%" cellspacing="0" cellpadding="0" class="sortableContentTab" id="paymentTab">
                                <thead>
                                    <tr class="shop_head_current">
                                        <th style="padding:0 8px 0 6px;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll" /></th>
                                        <th style="width:80%" class="tLeft <?= Admin::table_sort_header('name') ?>"><? echo I18n::get('Title'); ?></th>
                                        <!--<th style="width:8%" class="tLeft <?= Admin::table_sort_header('margin') ?>"><? echo I18n::get('Cost'); ?></th> -->
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody width="100%">
                <? $i=0;?>
                <?// die(Kohana::debug($rows));?>
                <? foreach ($rows as $key => $value): ?>
                            <tr class="<? if ($i % 2) echo 'even'; else echo 'odd';$i++; ?>" rel="<?= $value['id']?>">
                                <td style="padding-left:7px;" class="tCenter"><input type="checkbox" name="chk[<?= $value['id'] ?>]" value="<?= $value['id'] ?>"  class="listCheckbox checkBox" /></td>
                                <td style="text-align:left;"><a href="/admin/paymethods/<?=$value['id']?>/edit"><?=$value['name']?></a></td>
                                <!--<td style="text-align:center;"><?=$value['margin']?></td>-->
                                <td class="tCenter">
                                    <a href="<?=$admin_path?><?=$controller?><?= $value['id']?>/status" ><img src="/static/admin/images/act_<?if(!$value['status']):?>red<?else:?>green<?endif;?>.png" alt="<?echo I18n::get('status')?>" title="<?echo I18n::get('status')?>"/></a>
                                </td>
                            </tr>
                <? endforeach; ?>
                            </tbody>
                        </table>
        <? if ($pagination): ?>
        <?= $pagination ?>
        <? endif; ?>
                                    <!-- **** -->
        <? //echo Kohana::debug($kohana_view_data);   ?>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>

<script type="text/javascript" charset="utf-8" src="/vendor/jquery/jquery-ui-1.8.10.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="/static/admin/css/sortable_options.css"/>
<script type="text/javascript">
    $(document).ready(function(){
        
        $( "#paymentTab tbody" ).sortable({ 
            stop: function(event, ui) {
                updateOrderId();
                setColors();
            }  
        });
        
    });
    
    function setColors()
    {
        var i = 0;
        $( "#paymentTab tr" ).each(
            function()
            {
                $(this).removeClass( 'even' );
                $(this).removeClass( 'odd' );
                $(this).addClass( (i % 2 == 0 ? 'even':'odd') );
                i++;
            }
        );
    }
    
    function getIds()
    {
        var ret = {};
        i=0;
        $( "#paymentTab tr" ).each(
            function()
            {
                ret[i] = $(this).attr('rel');
                i++;
            }
        );
        return ret;
    }
    
    function updateOrderId()
    {
        var ids = getIds();
        $('.preloader').removeClass('hidden');
        $.get(
            '<?= $admin_path?><?= $controller?>updateOrderId',
            {ids:ids},
            function(data)
            {
                $('.preloader').addClass('hidden');
                if(data != 1)
                    alert('Server error');
            }
        );
    }
</script>