<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:59:38 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_common.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>

<script type="text/javascript" charset="utf-8" src="/vendor/jquery/jquery-ui-1.8.10.custom.min.js"></script>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/sortable_options.js"></script>
<link rel="stylesheet" type="text/css" href="/static/admin/css/sortable_options.css"/>

<span class="separator_main_cat_options"></span>
<span class="separator_cat_bg"><? echo I18n::get('To edit a line press it'); ?> </span>
<table width="100%" cellpadding="0" cellspacing="0" class="shop" id="main_table" >
    <thead>
        <tr class="shop_head">
            <th class="column_options_first "></th>
            <th  class="tLeft column_options separator_table col_first"><? echo I18n::get('Alias'); ?></th>
            <th  class="tLeft column_options separator_table col_too"><? echo I18n::get('Title'); ?></th>
            <th  class="tLeft column_options_free separator_table col_free"><? echo I18n::get('Type'); ?></th>
            <th  class="tLeft column_options separator_table col_foo"><? echo I18n::get('Descriptions'); ?></th>
            <th  class="tLeft column_options separator_table"><? echo I18n::get('Default value'); ?></th>
            <th  class="tLeft col_cat_options separator_table"><? echo I18n::get('Search in the field'); ?></th>
            <th  class="tLeft col_cat_options "><? echo I18n::get('Filter by field'); ?></th>
            <th></th>
        </tr>
    </thead>
</table>

<? $div_flag=0;?>
<ul id="test-list" rel="<?=$obj['id']?>">
<? for($k=0;$k<count($obj['ser_addFields']);$k++) : ?>
    <? $v = $obj['ser_addFields'][$k];?>
    <? if (!$v['is_group']) : ?>
    <? if ($div_flag == 0): ?><div class="items_no_group"><div class="border_style_item"><?endif;?>
    <? if ($div_flag == 0) {$div_flag=1;} ?>
      <li rel="" id="<?= 'id_' . $v['id'] ?>" class="items_links <?=($key % 2)?'grey_color':'white_color';?>">
            <div class="flLeft column_options_first"><img src="/static/admin/images/pull_over.png" class="handle" alt="move" /></div>
            <div class="flLeft column_options click_active input_add"><span class="text_edit_options"><?= $v['alias'] ?></span></div>
            <div class="flLeft column_options click_active input_add"><span class="text_edit_options"><?= $v['name'] ?></span></div>
            <div class="flLeft column_options_free click_active"><span class="text_edit_options"><?= $v['type'] ?></span></div>
            <div class="flLeft column_options click_active input_add"><span class="text_edit_options"><?= $v['desc'] ?></span></div>
            <div class="flLeft column_options click_active input_textarea"><span class="text_edit_options"><?= $v['defaults'] ?></span></div>
            <div class="flLeft column_options_checkbox"><span class="text_edit_options"><?= Form::checkbox('is_searchable', null, $value['is_searchable'] ? TRUE : FALSE); ?></span></div>
            <div class="flLeft column_options_checkbox2"><span class="text_edit_options"><?= Form::checkbox('is_filterable', null, $value['is_filterable'] ? TRUE : FALSE); ?></span></div>
            <div class="flRight rol_del "><a class="flLeft column_options_delete" href="#"></a></div>
            <div class="clear"></div>
        </li>
    <? if ($k == count($obj['ser_addFields'])):?></div></div><?endif;?>
    <?else:?>
        <? $tempo = $v['alias'];?>
        <li  class="group_item group_link">
            <div class="options_bg">
                <div class="flLeft column_options_first group_margin"><img src="/static/admin/images/pull_over.png" class="handle" alt="move" /></div>
                <ul class="group_class">
                <li rel="" class="bg1" id="<?= 'id_' . $v['id'] ?>">
                    <div class="flLeft column_options click_active input_add"><span class="text_edit_options"><?= $v['alias'] ?></span></div>
                    <div class="flLeft column_options click_active input_add name_style1"><span class="text_edit_options"><?= $v['name'] ?></span></div>
                    <div class="flLeft column_options click_active input_add descr_style"><span class="text_edit_options"><?= $v['desc'] ?></span></div>
                    <div class="flRight  column_options group_settings"><span class="text_edit_options"><a class="add_item_group active_item_add" href="#"><? echo I18n::get('Add field'); ?></a><a class="column_options_delete delete_group" href="#"></a></span></div>
                    <div class="clear"></div>
                </li>
                <div class="border_round">
                <? for($key=$k+1;$key<count($obj['ser_addFields']);$key++) : ?>
                <? $k=$key; $value = $obj['ser_addFields'][$key];?>
                <? if (strpos($value['alias'], $tempo)===false) {$k=$key-1; break; }?>
                    <li rel="<?= $tempo ?>" id="<?= 'id_' . $value['id'] ?>" class="<?=($key % 2)?'grey_color':'white_color';?>">
                        <div class="flLeft column_options_first"><img src="/static/admin/images/pull_over.png" class="handle" alt="move" /></div>
                        <div class="flLeft column_options click_active input_add"><span class="text_edit_options" ><?= str_replace($tempo, '', $value['alias']) ?></span></div>
                        <div class="flLeft column_options click_active input_add"><span class="text_edit_options"><?= $value['name'] ?></span></div>
                        <div class="flLeft column_options_free click_active"><span class="text_edit_options"><?= $value['type'] ?></span></div>
                        <div class="flLeft column_options click_active input_add"><span class="text_edit_options"><?= $value['desc'] ?></span></div>
                        <div class="flLeft column_options click_active input_textarea"><span class="text_edit_options"><?= $value['defaults'] ?></span></div>
                        <div class="flLeft column_options_checkbox"><span class="text_edit_options"><?= Form::checkbox('is_searchable', null, $value['is_searchable'] ? TRUE : FALSE); ?></span></div>
                        <div class="flLeft column_options_checkbox2 "><span class="text_edit_options"><?= Form::checkbox('is_filterable', null, $value['is_filterable'] ? TRUE : FALSE); ?></span></div>
                        <div class="flRight rol_del"><a class="flLeft column_options_delete" href="#"></a></div>
                        <div class="clear"></div>
                    </li>
                <? endfor;?>
                </div>
                </ul>
            </div>
        </li>
    <? endif;?>
<? endfor; ?>
 <? if ($div_flag == 0): ?><div class="items_no_group" style="display: none;"><div class="border_style_item"></div></div><?endif;?>
        </ul>
        
<div class="flLeft" id="copy_field">
    <?= Model_Content::customSelect('source_id', $categories, 
            Model_Content::arrGet($obj, 'parent_id', $_GET['parent_id']) , NULL, 'id', array('name')
            )?>
    <br/>
    <a class="active_item_add" href="#"><? echo I18n::get('Copy fields from'); ?></a>
</div>
<div class="flRight footer_style">
    <a id="add_field" class="active_item_add" href="#"><? echo I18n::get('Add field'); ?></a>
    <a id="add_group" class="active_item_add" href="#"><? echo I18n::get('Add Category'); ?></a>
</div>

<div class="clear"></div>
<script type="text/javascript">

$("#copy_field").unbind('click').click(function(){
    
    $.get("/admin/categories/<?=$id?>/copy_add_fields?source_id=", 
    { source_id: $('input[name=source_id]').val()}, 
        function(data){
            if (data == '1') window.location.reload(); 
        } 
    );
        
    return false;
});

</script>