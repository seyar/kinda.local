<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:59:38 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_common.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>

<div class="flLeft" style="width: 100%;padding-bottom: 13px;font-style: italic">
    <table width="100%" class="italic grayText2 borderNone shop_seo">
        <tr >
            <td style="width: 49%;vertical-align:top;"><?php echo I18n::get('Product Name') . ':' ?> <br />
                <div class="winput seo_padding_top" style="padding-bottom: 16px">
                    <input type="text" name="name" value="<?= htmlentities(Model_Content::arrGet( $obj, 'name', Model_Content::arrGet($_POST, 'name', $obj['alter_name']) ), ENT_COMPAT, 'utf-8');?>" style="padding-right: 0px;"/>
                </div>

                <?php echo I18n::get('Category') . ':' ?> <br />

                <div class="winput seo_padding_top">
                  <? //echo(Kohana::debug($obj['categories_id']));?>
                    <?// echo (Kohana::debug($categories)); ?>
                    <?// array_unshift($publ_types_unsorted, array('id'=>'', 'name'=>'---','url'=>'---', 'preTitle'=>'') );?>
                    <?//= Model_Content::customSelect('publ_types_id', $publ_types_unsorted, $filter['pub_id'], NULL, 'id', array('preTitle','name'))?>
                    <?//= Model_Content::customSelect('categories_id', $,)?>

                    <?// echo (Kohana::debug($edit_categories_list)); ?>
                    <? // $a = array(); foreach ($edit_categories_list as $value) { $a[$value['id']]=$value['name']; } ?>
                    <?= Model_Content::customSelect('categories_id', $edit_categories_list, Model_Content::arrGet($obj,'categories_id',Model_Content::arrGet($_POST,'categories_id',$_GET['parent_id']) ), NULL, 'id', 'name')?>

                    <? //array_unshift($a, array('id'=>'', 'name'=>'---','url'=>'---', 'preTitle'=>'') );?>
                    <?//=  Model_Content::customSelect($name, $options, $selected, $attributes, $valuekey, $titleKey)?>
                    <?//=Model_Content::customSelect('categories_id',$a,$obj['categories_id'],array(''),null,null)?>

               
                </div>


            </td>
            <td style="padding: 0 20px 0 20px;width: 1px;">
                
            </td>
            <td style="width: 49%;vertical-align:top;"><?php echo I18n::get('Alternative name') . ':' ?> <br />
                <div class="winput seo_padding_top" style="padding-bottom: 16px">
                    <input type="text" name="alter_name" value="<?= htmlentities(Model_Content::arrGet($obj,'alter_name', $_POST['alter_name']), ENT_COMPAT, 'utf-8');?>" style="padding-right: 0px;"/>
                </div>

                <?php echo I18n::get('Brand') . ':' ?> <br />

                <div class="winput seo_padding_top">
                    <?= Model_Content::customSelect('brand_id', $brands, Arr::get($obj, 'brand_id', $_POST['brand_id']),null,'id','name')?>
                </div>


            </td>

        </tr>

        <tr>
            <td colspan="3">
                <?php echo I18n::get('Short description') . ':' ?>
                <div class="winput seo_padding_top"><textarea style="width:99%" name="short_description"><?=Model_Content::arrGet($obj, 'short_description', $_POST['short_description']);?></textarea></div>
            </td>
        </tr></table>
    <table width="100%" class="italic grayText2 borderNone shop_seo">
        <tr>
            <td>
                <?php echo I18n::get('News content') . ':' ?>
<!--                <div class="winput seo_padding_top"></div>-->
            </td>
        </tr>
        <tr>
            <td>
<!-- -->
<script type="text/javascript" src="/vendor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/vendor/ckeditor/adapters/jquery.js"></script>

<script type="text/javascript">
		var config;
        jQuery(document).ready(function()
        {
                config = {
                        toolbar:
                        [
                            ['Styles','Format','Font','FontSize'],
                            ['TextColor','BGColor'],
                            ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
                            ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
                            '/',
                            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
                            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
                            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                            ['Link','Unlink','Anchor'],
                            ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],
                            ['Templates','Maximize','ShowBlocks','-','Source']
                        ]
                        , defaultLanguage: 'ru'
                        , language: 'ru'
                        , filebrowserBrowseUrl : '/vendor/elfinder/elfinder.html'

                };

            var ckeditor = CKEDITOR.replace("ckeditor", config);

        });
</script>

<!-- -->

                <textarea rows="15" cols="44" style="width:98%;" name="full_description" id="ckeditor"
                          class="jquery_ckeditor"><?= Model_Content::arrGet($obj, 'full_description', $_POST['full_description']);?></textarea>
          </td>
          </tr>
</table>
           
<table width="100%" class="italic grayText2 borderNone shop_seo">
        <tr>
            <td style="width: 49%;vertical-align:top;"><?php echo I18n::get('scu') . ':' ?> <br />
                <div class="winput seo_padding_top" style="padding-bottom: 16px">
                    <input type="text" name="scu" value="<?= Model_Content::arrGet($obj, 'scu', $_POST['scu']);?>" style="padding-right: 0px;"/>
                </div>

                <?php echo I18n::get('Warranty period (months)') . ':' ?> <br />

                <div class="winput seo_padding_top"  >
                   <input type="text" name="site_name" value="12" style="padding-right: 0px;"/>
                </div>


            </td>
            <td style="padding: 0 20px 0 20px;width: 1px;"></td>
            <td style="width: 49%;vertical-align:top;"><?php echo I18n::get('Weight (kg)') . ':' ?> <br />
                <div class="winput seo_padding_top" style="padding-bottom: 16px">
                    <input type="text" name="weight" value="<?=Model_Content::arrGet($obj, 'weight',$_POST['weight']);?>" style="padding-right: 0px;"/>
                </div>

                <?php echo I18n::get('Rating') . ':' ?> <br />

                <div class="winput seo_padding_top" >
                    <input type="text" name="rating" value="<?=Model_Content::arrGet($obj, 'rating', $_POST['rating']);?>" style="padding-right: 0px;"/>
                </div>


            </td>

        </tr>
        
        <tr>
            <td colspan ="3"style="border-bottom: 1px dotted #929FA7;width: 100%">
            </td>
        </tr>
        
        <tr>
            <td style=" padding-top: 8px;">
                <input name="is_new" value = "1" type="checkbox" <?=($obj['is_new']=='1' ? 'checked' : '')?>   class="" />
                <span style="margin-left: 6px; margin-right: 15px;"><?= I18n::get('New product')?></span>
                <input name="is_bestseller" value = "1" type="checkbox" <?=($obj['is_bestseller']=='1' ? 'checked' : '')?> class="listCheckbox checkBox" />
                <span style="margin-left: 6px; margin-right: 15px;"><?= I18n::get('Best-seller')?></span>
                <input name="show_on_startpage" value = "1" type="checkbox" <?=($obj['show_on_startpage']=='1' ? 'checked' : '')?> class="listCheckbox checkBox" />
                <span style="margin-left: 6px; margin-right: 15px;"><?= I18n::get('Skip to main')?></span>
            </td>
            <td></td>
            <td style=" padding-top: 8px;">
                <label><input name="parsed" value = "1" type="checkbox" <?=($obj['parsed']=='1' ? 'checked' : '')?> class="listCheckbox checkBox" />
                    <span style="margin-left: 6px; margin-right: 15px;"><?= I18n::get('Don`t parse')?></span></label>
            </td>
        </tr>
        <tr>
            <td>                
                <?php echo I18n::get('Status') . ':' ?>
                <div class="winput seo_padding_top" >
                    <? $options = array(
                        'active' => I18n::get('Active'),
						'archive' => I18n::get('Archive'),
						'unverified' => I18n::get('Unverified'),
						'out_of_store' => I18n::get('Out of store'),
						'allow_to_order' => I18n::get('Allow to order'),
						'from1c' => I18n::get('From1c')
                    ); 
                    echo Form::select('status', $options, Arr::get($obj, 'status', $_POST['status']), array('size'=>4));
                    ?>
                </div>
            </td>
            <td></td>
            <td style=" padding-top: 8px;">
                Формат <br />
                <input type="text" name="format" value="<?=Model_Content::arrGet($obj, 'format', $_POST['format']);?>" style="padding-right: 0px;"/>
            </td>
        </tr>

    </table>
</div>

<div class="clear"></div>
