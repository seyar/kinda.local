<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="padding20px">

    <div class="rows">
        <div class="title">
            <div class="cell flLeft" style="width: 65px;">ID</div>
            <div class="cell flLeft first">Артикул</div>
            <div class="cell flLeft second">Цена</div>
            <div class="cell flLeft second">Формат</div>
        </div>
        <?if($goodGroups):?>
            <? foreach($goodGroups as $key => $item):?>
            <div class="savedrow">
                <input type="text" name="goodgroups[<?=$key?>]" value="<?=$key?>" readonly="true"/>
                <input type="text" name="goodgroups_scu[<?=$key?>]" value="<?=$item['scu']?>"/>
                <input type="text" name="goodgroups_price[<?=$key?>]" value="<?=$item['price']?>"/>
                <input type="text" name="goodgroups_format[<?=$key?>]" value="<?=$item['format']?>"/>
            </div>
            <?endforeach;?>
        <?//else:?>
        <?endif;?>
        <div class="row">            
            <div style="float: left;height: 1px;width: 62px;"></div>
            <input type="text" name="goodgroups_scu[]" value=""/>
            <input type="text" name="goodgroups_price[]" value=""/>
            <input type="text" name="goodgroups_format[]" value=""/>
        </div>
        <div class="btns">
            <button class="btn" type="button" id="btn_plus">
                                    <span><span>+</span></span></button>
            <button class="btn" type="button" id="btn_minus" style="display:none;">
                                    <span><span>-</span></span></button>
        </div>
        <div class="clear"></div>
    </div>
    
<script>
    $(document).ready(
    function(){
        $('#btn_plus').click(function(){
            plus($(this).parent());            
        });
        
        $('#btn_minus').click(function(){
            minus();            
        });
        
    });
    
    function plus( thisObj )
    {
        var block = thisObj.prev().clone();
        block.children('input').val('');
        $('.row:last').after(block);
        checkshowingminus();
    }
    
    function minus( )
    {
        $('.row:last').remove();
        checkshowingminus();
    }
    
    function checkshowingminus()
    {
        if( $('.row').length <= 1 )
            $('#btn_minus').hide();
        else
            $('#btn_minus').show();
    }
</script>
<style>
    .rows{ width: 290px; }
    .row, .savedrow{ clear: both;padding: 0 20px 10px 0px; }
    .row, .btns, .savedrow{ float: left; }
    .cell{ width: 65px; }
    .row input, .savedrow input{ width: 50px; }
</style>
    
</div>