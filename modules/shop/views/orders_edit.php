<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<!--floating block-->

<form method="post" action="">
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">

	<input type="text" name="actTab" id="actTab" value="common" />
	<div class="absBlocks floatingInner">
		<div class="padding20px">
			<input type="hidden" name="list_action" id="list_action" value=""/>
			<table width="100%">
				<tr>
					<td style="width:37%">
						<button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
							<span><span><?= I18n::get('Save and exit') ?></span></span></button>
						<button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">

							<span><span><?= I18n::get('Apply') ?></span></span></button>
					</td>

					<td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?= $admin_path?><?= $controller?>';">
							<span><span><?= I18n::get('Cancel') ?></span></span></button>
					</td>
				</tr>
			</table>
		</div>
		<div class="absBlocks floatingPlaCorner L"></div>
		<div class="absBlocks floatingPlaCorner R"></div>
	</div>
	<div class="absBlocks side L"></div>
	<div class="absBlocks side R"></div>
</div>

    <!--floating block-->

    <div class="rel whiteblueBg" style="width:100%; border-bottom: 1px solid #D0DEDE;" >

		<div id="" class="main padding20px flLeft" style="padding-top:20px; width:37%"><? include 'order_main.php'; ?></div>
		<div class="whiteblueBg  flLeft" style="border-bottom: 1px dotted grey; padding-bottom: 5px; padding-top: 26px; width: 57%;">
            <!-- **** -->
            <ul class="tabs">
                    <li class="active"  id="StatusTab"><a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Order Status')?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li id="DetailsTab"><a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Order Details')?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
            </ul>
            <div class="clear"></div>
        </div>
        <div id="" class="tabContent openedTab StatusTab flLeft" style="width:57%"><? include 'order_status.php'; ?></div>
        <div class="whiteblueBg  flLeft" style="width:57%">
            <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
            <!-- **** -->
            
            <div id="" class="tabContent DetailsTab" style="padding-top:15px"><? include 'order_details.php'; ?></div>
            <!-- **** -->
        </div>
</form>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>
<div class="clear"></div>
</div>
