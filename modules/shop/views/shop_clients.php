<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>
<script type="text/javascript">
    function checkFilterValues()
    {
        $('.filter input').each(function(){
            if( $(this).val() == $(this).attr('rel') ) $(this).val('');
        });
    }
    function setStatusClient ( a )
    {
            jQuery.post("<?=$admin_path?><?=$controller?>"+a+"/statusUpdate", null ,function(data){
                if(data != 1 ) alert('Server error');
            });


    }
</script>
<form method="post" action="" onsubmit="checkFilterValues();">

    <div class="rel whiteBg floatingOuter_shop" id="contentNavBtns" style="height: 174px;">
        <div class="whiteblueBg absBlocks floatingInner" style="height: 174px;" >
            <div class="padding:20px;">
                <table width="100%">
                    <tr>

                        <td class="button_clients" style="width:46%;text-align: left;">
               
                            <button onclick="document.location.href='/admin/orders/';" type="button" class="btn blue" style="padding-left: 5px; ">
                                <span><span><? echo I18n::get('orders'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><? echo I18n::get('With marked'); ?></td>

                        <td style="padding-top:1px"><select id="list_action"><option value="<?= $admin_path?><?=$controller?>delete"><? echo I18n::get('Delete'); ?></option></select></td>

                        <td style="text-align: right;"><button class="btn formUpdate" type="button">
                                <span><span><? echo I18n::get('Apply'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>

                        <td nowrap="nowrap">
                            <?=
                            Form::select('rows_per_page', array(
                                        '10' => '10 ' . I18n::get('lines'),
                                        '20' => '20 ' . I18n::get('lines'),
                                        '30' => '30 ' . I18n::get('lines'),
                                            ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page'))"))
                            ?>
                        </td>
                    </tr>
                    <tr style="height: 10px;"></tr>
                    <tr >
                        <td colspan="6" style="border-top:1px solid #FFFFFF;">

                            <fieldset class="filter">
                                <legend style="font-weight: bold; color: black;"><? echo I18n::get('Clients filter'); ?></legend>
                                <table width="100%">
                                    <tbody>
                                        <tr>
                                            <td style="width:25%;vertical-align:top;">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td><input type="text" style="width: 100%;" rel="ID" value="ID" name="filter[ID]" onfocus="if(this.value == 'ID') this.value = ''" onblur="if(this.value == '') this.value = 'ID'"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 6px; padding-right: 0px; text-align:right;">
                                                            <? echo I18n::get('Turnover'); ?>: 
                                                            <input type="text" style="width: 31%;" value="" name="filter[oborot-ot]" >
                                                            -
                                                            <input type="text" style="width: 30%; " value="" name="filter[oborot-do]">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 6px;"><input type="text" style="width: 100%;" rel="<? echo I18n::get('Discount'); ?>" value="<? echo I18n::get('Discount'); ?>" name="filter[discount]" class="has_default_value" ></td>
                                                    </tr>
                                                    
                                                    
                                                </table>
                                            </td>
                                            <td style="width:25%;vertical-align:top;">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td ><input type="text" style="width: 100%;" rel="<? echo I18n::get('User'); ?>" value="<? echo I18n::get('User'); ?>" name="filter[name]" class="has_default_value" ></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 6px; padding-right:0px; text-align:right;">
                                                            <? echo I18n::get('Reg. Date'); ?>:
                                                            <input type="text" style="width: 28%;" value="" name="">
                                                            -
                                                            <input type="text" style="width: 28%;" value="" name="">
                                                        </td>
                                                    </tr>
                                                    
                                                </table>
                                            </td>
                                            <td style="width:25%; vertical-align:top;">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td ><input type="text" style="width: 100%;" rel="E-mail" value="E-mail" name="filter[email]" class="has_default_value" ></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 6px 0 0 0;">
                                                            <?=
                                                                Form::select('status', array(
                                                                            '' => I18n::get('Select status'),
                                                                            '1' => I18n::get('Active'),
                                                                            '0' => I18n::get('Blocked'),
                                                                                ), Model_Content::arrGet( $filter, 'status', $_POST['status'] ) )
                                                                ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:25%; vertical-align:top;">
                                                <table style="width: 100%;" >
                                                    <tbody style="width: 100%;">
                                                    <tr>
                                                        <td><input type="text" style="width: 100%;" rel="<? echo I18n::get('Phone'); ?>" value="<? echo I18n::get('Phone'); ?>" name="filter[phone]" class="has_default_value"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 6px;padding-right: 0px;">
                                                              <?=
                                                                Form::select('group', array(
                                                                            '' => I18n::get('Select group'),
                                                                            '1' => I18n::get('Retail'),
                                                                            '2' => I18n::get('Special price'),
                                                                                ))
                                                              ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td  style="text-align:right;padding-top: 6px; padding-right: 0px;" >
                                                            <button type="button" class="btn formFilter" >
                                                                <span><span><? echo I18n::get('Apply'); ?></span></span></button>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </fieldset>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>
    <div class="rel whiteBg">
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
		<!-- **** -->
		<table width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab">
			<thead>
				<tr class="shop_head">
					<th style="padding-left:0;width: 5%" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll" /></th>
					<th style="width: 5%" class="tLeft <?= Admin::table_sort_header('id') ?>">ID</th>
					<th style="" class="tLeft <?= Admin::table_sort_header('name') ?>"><? echo I18n::get('User'); ?></th>
					<th style="" class="tLeft <?= Admin::table_sort_header('email') ?>"><? echo I18n::get('E-mail'); ?></th>
					<th style="" class="tLeft <?= Admin::table_sort_header('phone') ?>"><? echo I18n::get('Phone'); ?></th>
					<th style="" class="tLeft <?= Admin::table_sort_header('group') ?>"><? echo I18n::get('Group'); ?></th>
					<th style="width: 7%" class="tLeft <?= Admin::table_sort_header('discount') ?>"><? echo I18n::get('Discount'); ?></th>
					<th style="" class="tLeft <?= Admin::table_sort_header('date_create') ?>"><? echo I18n::get('Reg. Date'); ?></th>
					<th style="" class="tLeft <?= Admin::table_sort_header('turnover') ?>"><? echo I18n::get('Turnover'); ?></th>
					<th style="" class="tLeft unsortable"><? echo I18n::get('Status'); ?></th>
				</tr>
			</thead>
			<tbody width="100%" class="shop">
                <? $i=0;?>
                <? foreach ($rows as $key => $value): ?>
					<tr class="<?
					if ($i % 2) echo'even'; else echo 'odd'; $i++;?>">
					<td style="padding-right:8px;" class="tCenter"><input type="checkbox" name="chk[<?= $value['id'] ?>]" value="<?= $value['id'] ?>"  class="listCheckbox checkBox" /></td>
					<td class="id tLeft"><?=$value['id']?></td>
					<td style="text-align:left;padding:5px 0 0 10px;"><a href="/admin/customers/<?=$value['id']?>/edit"><?=$value['name']?></a></td>
					<td class="id_shop_mail" style="text-align:left;padding:5px 0 0 10px;"><a href="mailto:<?=$value['email']?>"><?=$value['email']?></a></td>
					<td style="text-align:center"><?=$value['phone']?></td>
					<td style="text-align:center"><?=$value['group']?></td>
					<td style="text-align:center"><?=$value['discount']?> <span>%</span></td>
					<td style="text-align:right">
						<?=substr($value['date_create'], 0, 10).'<br/>'
							.substr($value['date_create'], 11, 5)?></td>
					<td style="text-align:right"><?=$value['turnover']?> <span><? echo Kohana::config('shop')->default_currency_char ?></span></td>
					<td style="text-align:right" class="status" >

				 <?=
					Form::select('status'.$value['id'], array(
								'0' => I18n::get('Blocked'),
								'1' => I18n::get('Active'),
									),$value['status'],array('onchange'=> "setStatusClient(".$value['id'].");"))
				  ?>
					</td>
				</tr>

                <? endforeach; ?>
				</tbody>
			</table>
        <? if ($pagination): ?>
        <?= $pagination ?>
        <? endif; ?>
                                                                    <!-- **** -->
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
