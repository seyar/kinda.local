<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:27:18 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/static/admin/js/settings.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.serializeanything.js"></script>

<!--floating block-->

<form method="post" action="">
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
   
    <input type="text" name="actTab" id="actTab" value="common" />
    <div class="absBlocks floatingInner">
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td style="width:37%">
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                        <span><span><?= I18n::get('Save and exit') ?></span></span></button>
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">

                                        <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="absBlocks floatingPlaCorner L"></div>
        <div class="absBlocks floatingPlaCorner R"></div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

        <!--floating block-->

        <div class="rel" >


            <style type="text/css">
                .title_currency
                {
    display:block;
    float:left;
    font-style:italic;
    padding-bottom:7px;
    padding-left:5px;
                }
            </style>
    <div class="whiteblueBg padding20px">
        <table style="border-bottom: 0px;"width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab">
            <tr>
                <td >
                    <span style="padding-bottom: 7px; display: block; padding-top: 3px;"><span class="title_currency"><? echo I18n::get('Title'); ?>:</span></span>
                    <input type="text" value="<?=$obj['name']?>" name="name" style="width:95%">
                </td>
                <td >
                    <span style="padding-bottom: 7px; display: block; padding-top: 3px;"><span class="title_currency"><? echo I18n::get('Code'); ?>:</span></span>
                    <input type="text" value="<?=$obj['code']?>" name="code" style="width:95%">  </td>
                <td >
                    <span style="padding-bottom: 7px; display: block; padding-top: 3px;"><span class="title_currency"><? echo I18n::get('ISO Code'); ?>:</span></span>
                    <input type="text" value="<?=$obj['iso_code']?>" name="iso_code" style="width:95%"></td>
                <td >
                    <span style="padding-bottom: 7px; display: block; padding-top: 3px;"><span class="title_currency"><? echo I18n::get('Exchange'); ?>:</span></span>
                    <input type="text" value="<?=$obj['rate']?>" name="rate" style="width:95%"></td>
            </tr>
            

        </table>
                <div class="clear"></div>
                <br />

        
    </div>
   
</form>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>

</div>
