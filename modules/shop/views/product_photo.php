<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:59:38 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_common.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="padding20px">
    <?if($obj['id']):?>
        <? require MODPATH.'/admin/views/system/uploadify.php';?>
    <?endif;?>
</div>

<?
/*
<div class="flLeft" style="width: 100%;padding-bottom: 13px;">
    <table width="100%" class="italic grayText2 borderNone shop_seo">
        <tr>
            <td colspan="2" style="" nowrap="nowrap"><?php echo I18n::get('Add images in the news') . ':' ?> <br /></td>
        </tr>
        <tr >
            <td colspan="2" style="width: 100%;">
                <? for ($i = 1; $i <= 6; $i++): ?>
                    <div style="float: left; padding-right: 14px;">
                        <img src="/static/admin/images/yan.png" />
                    </div>
                <? endfor; ?>
            </td>
        </tr>
        <tr style="vertical-align: top;">
            <td style="width:23%;border-right: 1px dotted #CACFD3;padding: 0px"><button  class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?>update">
                    <span><span>Выбор изображения</span></span></button>
            </td>
            <td class="style_foto" style="padding:0 0 0 30px;">
                <a href="#"><img src="/static/admin/images/foto_plus.png" style="padding-right: 5px;"/>Добавить изображение</a>
                <a href="#"><img src="/static/admin/images/foto_minus.png" style="padding-right: 5px;"/>Очистить очередь</a>

            </td>
        </tr>



    </table>
</div>

<div class="clear"></div>
 * 
 */
?>
<br />