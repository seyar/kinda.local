<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     micrlo
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/static/admin/js/settings.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.serializeanything.js"></script>

<!--floating block-->

<form method="post" action="">
<input type="hidden" name="fs_name" value="<?= isset($fs_name)?$fs_name:$obj['fs_name']?>" />
<input type="hidden" name="settings_id" value="<?= $obj['settings_id']?>" />
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
   
    <input type="text" name="actTab" id="actTab" value="common" />
    <div class="absBlocks floatingInner">
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
        <? include MODPATH . ADMIN_PATH . '/views/system/wysiwyg.php'; ?>
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td style="width:37%">
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                        <span><span><?= I18n::get('Save and exit') ?></span></span></button>
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">

                                        <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="absBlocks floatingPlaCorner L"></div>
        <div class="absBlocks floatingPlaCorner R"></div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

        <!--floating block-->

        <div class="rel" >

    <div class="whiteblueBg padding20px">
        <table style="border-bottom: 0px;"width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab">
            <tr>
                <td colspan="3">
                    <span style="padding-bottom: 7px; display: block; padding-top: 3px;"><span style="float: left;font-style: italic;display: block;"><?= I18n::get('Title') ?>:</span></span>
                </td>
            </tr>
            <tr>
                <td width="45%">
                    <input type="text" style="float: left;display: block;width:100%;" value="<?=$obj['name']?>" rel="" name="name"  ></td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td width="45%">
                    <div style="float: left;">
                    <input type="checkbox" <? if ($obj['print_invoice']==1){echo'checked';}?> name="print_invoice" value="1"  class="listCheckbox checkBox" /> <span style="font-style: italic;padding-left:5px;"><?= I18n::get('Print invoice') ?></span>
                    </div>
                </td>
            </tr>
            <td width="45%">
                    <div class="tLeft">
                        <input type="text" name="orderid" value="<?= Model_Content::arrGet( $obj, 'orderid', Model_Content::arrGet($_POST,'orderid',9998))?>" style="width:28px;text-align: center;"/>
                              <? echo I18n::get('Sort order'); ?>
                      </div>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td width="45%"></td>
            <tr>
                <td colspan="3">
                    <span style="float: left;font-style: italic;"><?= I18n::get('Percent') ?> <span style="color:gray;">(<?= I18n::get('without %, for example 50') ?>):</span></span>
                </td>
            </tr>
            <tr>
                <td width="45%">
                    <input type="text" name="percent" value="<?= Arr::get($obj, 'percent',$_POST['percent']);?>" style="float:left;width:5em;"/>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td>
                    <div class="flLeft padchek">
                    <label><input type="checkbox" name="plusToOrder" <?if(Arr::get( $obj, 'plusToOrder', $_POST['plusToOrder'])):?>checked="checked"<?endif;?> value="1"/>
                              <?= I18n::get('Include the cost of payment in the amount of the order') ?></label>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span style="float: left;font-style: italic;"><?= I18n::get('Paying URL') ?>:</span>
                </td>
            </tr>
            <tr>
                <td width="45%">
                    <input type="text" name="action_url" value="<?= Arr::get($obj, 'action_url', Arr::get($_POST, 'action_url', 'http://money.ua/sale.php'));?>" style="float:left;width:20em;"/>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td>
                    <div class="flLeft padchek">
                          <label><input type="checkbox" name="ordersendmail" <?if(Arr::get( $obj, 'ordersendmail', $_POST['ordersendmail'])):?>checked="checked"<?endif;?> value="1"/>
                              <? echo I18n::get('Send to emeil copy of the invoice'); ?></label>
                      </div>
                </td>
            </tr>
            <tr>
                <td width="45%">
                    <div class="flLeft padchek">
                          <label><input type="checkbox" name="letter_description" <?if(Arr::get( $obj, 'letter_description', $_POST['letter_description'])):?>checked="checked"<?endif;?> value="1"/>
                              <? echo I18n::get('Include instructions to the email'); ?></label>
                      </div>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td>
                    <div class="flLeft padchek">
                          <label><input type="checkbox" name="include_tax" <?if(Arr::get( $obj, 'include_tax', $_POST['include_tax'])):?>checked="checked"<?endif;?> value="1"/>
                               <? echo I18n::get('Calculate taxes'); ?></label>
                      </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                     <div class="tLeft"><? echo I18n::get('Type of payment system'); ?>: </div>
                </td>
            </tr>
            <tr>
                <td width="45%">
                    <? $options = array(
                       "16" => "VISA/MASTER Card", 
                        "1" => "wmz", 
                        "2" => "wmr", 
                        "3" => "wmu", 
                        "9" => "nsmep", 
                        "15" => "liqpay-USD", 
                        "16" => "liqpay-ГРН", 
                        "17" => "ПРИВАТ24-ГРН", 
                        "18" => "ПРИВАТ24-USD",                         
                    );?>
                    <div class="tLeft"><?= Form::select('payment_type', $options, Arr::get($obj,'payment_type', $_POST['payment_type']) );?></div>
                </td>
                <td width="10%"></td>
                <td></td>
            </tr>
            <tr>
                <td width="45%" class="tLeft">
                  <? echo I18n::get('Information on the point of sale'); ?>: 
                  <input type="text" name="payment_addvalue" value="<?= Arr::get($obj,'payment_addvalue', $_POST['payment_addvalue'] )?>" style="width:20em;"/>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td class="tLeft">
                  Merchant Info<br/>
                  <input type="text" name="merchant_info" value="<?= Arr::get($obj, 'merchant_info', Arr::get($_POST,'merchant_info','105'))?>" style="width:20em;"/>
                </td>
            </tr>
            <tr>
                <td width="45%" class="tLeft">
                  Secretcode<br/>
                  <input type="text" name="secretcode" value="<?= Arr::get($obj,'secretcode', Arr::get($_POST, 'secretcode', 'FDojguEddu54jDujfuyu43ds'));?>" style="width:20em;"/>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td class="tLeft">
                  <label><input type="checkbox" name="payment_testmode" <?if(Arr::get($obj, 'payment_testmode', $_POST['payment_testmode'])):?>checked="checked"<?endif;?> value="1" />
                      <? echo I18n::get('Test mode'); ?></label>
                </td>
            </tr>
            <tr>
                <td width="45%">
                    <div class="flLeft padchek">
                          <? echo I18n::get('Price rate'); ?>
                          <?= Form::select('rateMethod', array('rate' => 'Наличный','rate_w_vat' => 'Безналичный' ), Model_content::arrGet($obj, 'rateMethod', Model_Content::arrGet($_POST,'rateMethod',1)) )?>
                      </div>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td>
                    <div class="flLeft padchek">&nbsp;</div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                      <br/>
                      <div class="tLeft"><? echo I18n::get('Payment is available for the following delivery methods'); ?>:</div>
                      <? foreach($shipments as $item):?>                          
                      <div style="padding: 5px;" class="tLeft padchek">
                          <label><input type="checkbox" name="forShipment[<?=$item['id']?>]" <? if(Arr::binary_search( $item['id'], $obj['forShipment']) !== FALSE || !$obj['name']):?>checked="checked"<?endif;?> value="<?=$item['id']?>"/><?=$item['name']?></label><br/>
                      </div>
                      <?endforeach;?>
                </td>
            </tr>

        </table>
                      
                      
        <div style="padding: 5px; font-style:italic; color:#54595F">
          <? echo I18n::get('Instruction'); ?>:<br /><br />
          <textarea class="jquery_ckeditor" id="ckeditor" name="text" rows="7" style="width:100%;" cols="7"><?= Arr::get($obj, 'text', $_POST['text'])?></textarea>
        </div>
        
        <div style="padding: 5px;font-style:italic; color:#54595F">
           <? echo I18n::get('Supplier'); ?>:<br /><br />
          <textarea name="postavshik" rows="7" style="width:100%;" cols="7"><?= htmlentities(Arr::get($obj, 'postavshik', $_POST['postavshik']),ENT_COMPAT,'UTF-8')?></textarea>
        </div>
        
        <div class="clear"></div>
        <br />
       
    </div>
   
</form>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>

</div>
