<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<br />
<div class="flLeft" style="width: 100%;">
    <table cellspacing="5" style="width: 100%;">
        <tr>
            <td class="greyitalic" style="width:8%; padding-top:7px"><? echo I18n::get('Status')?>:</td>
            <td width="31%">
                <?= Form::select('status', $order_statuses, $obj['status']);?>
            </td>
            <td width="31%" class="greyitalic"><label><input type="checkbox" name="alert" value="1" checked /> <? echo I18n::get('Notice')?></label></td>
            <td width="31%" class="greyitalic"><label><input type="checkbox" name="comment" value="1" onclick="$('textarea[name=comment]').attr('disabled', !this.checked);" /> <? echo I18n::get('Comment')?></label></td>
        </tr>
        <tr>
            <td colspan="4"><textarea cols="20" rows="5" name="comment" style="width: 100%;" disabled="true" onfocus="if($(this).attr('edit')) $(this).val('').attr('edit','');" edit="1"><? echo I18n::get('Order comment')?></textarea></td>
        </tr>
		<tr>
			<td colspan="4" class="tRight" style="padding-right: 0;">
				<button class="btn blue" type="button" onclick="return sendStatusInfo();">
				<span><span><? echo I18n::get('Apply')?></span></span></button>
				
			</td>
		</tr>
    </table>
    <br>
</div>
<div class="flLeft" style="width:100%">
    <strong><? echo I18n::get('Order status')?></strong>
    <br>
    <br>
    <table cellspacing="0" width="100%" class="tLeft" style="border:none">
        <tr>
            <th width="25%"><? echo I18n::get('Order date')?></th>
            <th width="18%"><? echo I18n::get('Status')?></th>
            <th width="21%"><? echo I18n::get('Notice')?></th>
            <th width="36%"><? echo I18n::get('Comment')?></th>
        </tr>
        <?foreach($obj['ser_status_history'] as $key => $value):?>
        <tr>
            <td style="padding-left:10px"><?= date("Y-m-d H:i:s", $value['date'])?></td>
            <td style="padding-left:10px"><?= $order_statuses[$value['status']]?></td>
            <td style="padding-left:10px"><?if($value['alert']):?><? echo I18n::get('Yes')?><?else:?><? echo I18n::get('No')?><?endif;?></td>
            <td style="padding-left:10px"><?= $value['comment']?></td>
        </tr>
        <?endforeach;?>
    </table>
</div>
<div class="clear"></div>