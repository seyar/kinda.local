<?
$return = 'Спасибо! Ваш заказ №'.$order_id.' в интернет-магазине <a target="_blank" href="'.$site.'">'.$siteTitle.'</a> был принят!<br /><br />';
$return .= '<table border="0" cellpadding="2">
            <tr valign="top">
                <td valign=top><b><u>'.i18n::get('Поставщик').'</u></b></td>
                <td>&nbsp;</td>
                <td>
<!--                   {$order.shipment_name}, {$order.shipment_address}-->'.
                                   nl2br($payment_method['postavshik'])
                .'</td>
            </tr>
            <tr valign="top">
                <td valign=top><b><u>'.i18n::get('Получатель').'</u></b></td>
                <td>&nbsp;</td>
                <td>
                    <!--'.$personalInfo['lastname'].' '.$personalInfo['firstname'].' '.$personalInfo['nickname'].',
                    Телефон: '.$personalInfo['phone'].',
                    Город: '.$personalInfo['city'].',
                    Область: '.$personalInfo['district'].',
                    Почтовый индекс: '.$personalInfo['zipCode'].',
                    Адрес: '.$personalInfo['address'].',
                    Email: '.$personalInfo['user_name'].',-->
                    '.$order['shipment_name'].', '.$order['shipment_address'].', '.$personalInfo['phone'].'
                </td>
            </tr>
            <tr>
                <td valign=top><b><u>'.i18n::get('Плательщик').'</u></b></td>
                <td>&nbsp;</td>
                <td>
                   '.$order['shipment_name'].', '.
                     $order['shipment_address'].', '.
					 $personalInfo['phone'].'
                </td>
            </tr>
    </table>';


    $return .= '<center>
        <b>'.i18n::get('Счет-фактура ').'№'.$order['id'].'<br/>
            '.i18n::get('от ').date('d.m.Y',strtotime($order['date_create'])).'<br/>
            '.i18n::get('Действителен в течении 5 дней').'<br/><br/>
        </b>
    </center>
    <table border="1" cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <th>№</th>
                <th>'.i18n::get('Полное наименование товара').'</th>
                <th>'.i18n::get('Ед. изм.').'</th>
                <th>'.i18n::get('Кол-во').'</th>
                <th>'.i18n::get('Артикул').'</th>
                <th>'.i18n::get('Формат').'</th>
                <th>'.i18n::get('Язык').'</th>
                <!--<th>'.i18n::get('Цена').'</th>-->
                <!--<th>'.i18n::get('Сумма').'</th>-->
            </tr>';

            if($order['ser_details']):
            foreach($order['ser_details'] as $key=>$item)
            {
                $return .= '<tr>
                    <td align="center">'.($key+1).'</td>
                    <td>'.$item['name'].'</td>
                    <td align="center">'.i18n::get('шт.').'</td>
                    <td align="center">'.$item['count'].'</td>
                    <td align="center">'.$item['scu'].'</td>
                    <td align="center">'.$item['format'].'</td>
                    <td align="center">'.$item['addParam'].'</td>
                    <!--<td align="center">'.number_format($item['price'],2,'.','').'<br/></td>-->
                    <!--<td align="right">'.number_format($item['sum'],2,'.','').'<br/></td>-->
                </tr>';
            }
            endif;

            if( $order['shipment_cost'])
            {
                $return .= '<tr>
                        <td>&nbsp;</td>
                        <td>'.i18n::get('Доставка').'</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="center">'.$order['weight'].' кг.<br/></td>
                        <td>&nbsp;</td>
                        <td align="right">'.number_format($order['shipment_cost'],2,'.','').'</td>
                    </tr>';
            }
            if( $payment_method['plusToOrder'])
            {
                $return .= '<tr>
                        <td>&nbsp;</td>
                        <td>'.i18n::get('Способ оплаты').'</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
<!--                        <td align="center">'.$order['weight'].' кг.<br/></td>-->
                        <td>&nbsp;</td>
                        <td align="right">'.number_format($order['payment_cost'],2,'.','').'</td>
                    </tr>';
            }
            if( $discount)
            {
                $return .= '<tr>
                        <td>&nbsp;</td>
                        <td>'.i18n::get('Скидка').'</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
<!--                        <td align="center">'.$order['weight'].' кг.<br/></td>-->
                        <td>&nbsp;</td>
                        <td align="right">'.number_format($discount,2,'.','').'</td>
                    </tr>';
            }
            $return .= '<tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <!--<td>&nbsp;</td>-->
                <!--<td>&nbsp;</td>-->
            </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <!--<td align="right"><b>'.i18n::get('Всего на сумму').':</b></td>-->
                    <!--<td align="right">'.number_format($order['sum'],2,'.','').'</td>-->
                </tr>

            </table>
            <br/>
            <!--<table border="0" cellpadding="2" width="100%">

                    <tr>
                        <td colspan="0"><b>'.i18n::get('Всего на сумму').': <br/>'.$order['sum_text'].'</b></td>
                    </tr>
                    <tr>
                        <td><b>'.i18n::get('НДС').': 0,00</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><b>'.i18n::get('Выписал(ла)').'___________________________</b></td>
                    </tr>

            </table>-->
            <br/>
            <br/>
            '.i18n::get('Благодарим Вас за выбор наших услуг').'.
            <br/>
            <br/>
            <table cellspacing="0" cellpadding="2">
                <tr>
                    <td valign=top><b><u>'.i18n::get('Доставка').'</u></b></td>
                    <td>&nbsp;</td>
                    <td>'.$shipment_method['name'].
                        ' '.number_format($order['shipment_cost'],2,'.','').'
                    </td>
                </tr>
                <tr>
                    <td valign=top><b><u>'.i18n::get('Оплата').'</u></b></td>
                    <td>&nbsp;</td>
                    <td>'.$payment_method['name'].
                        ' '.number_format($order['payment_cost'],2,'.','').'
                    </td>
                </tr>
            </table>
            ';
            
            $return .= $shipment_method['text'];
            $return .= $payment_method['text'];
            $return .= $otherComments;

return $return;
?>
