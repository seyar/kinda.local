<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
$options = array();
?>
<script type="text/javascript" src="/vendor/jquery/jquery.serializeanything.js"></script>
<script type="text/javascript">

    function sendStatusInfo()
    {
        url = '<?=$admin_path?><?=$controller?><?=$id?>/';

        $.post(url+'ajax?task=change_status'
            , $('.StatusTab').serializeAnything(), function(result){
                if (result == 1)
                {
                    $('.StatusTab').load(
                    url+'ajax?task=status',
                    function()
                    { 
                        $('.StatusTab select').each(function(){ $(this).SelectCustomizer(); }); 
                        alert(Chimera.lang.saved);
                    }
                    
                );
                    
                }else
                    alert('Server error!');
            });
        return false;
    }

    function saveDetails()
    {
        url = '<?=$admin_path?><?= $controller?><?=$id?>/';

        $.post(url+'ajax?task=update_good'
            , $('.DetailsTab').serializeAnything(), function(result){
                if (result == 1)
                {
                    $('.DetailsTab').load(url+'ajax?task=details');
                    alert(Chimera.lang.saved);
                }
                else
                    alert('Server error!');
            });
        return false;
    }

    function deleteGood(id)
    {
        url = '<?=$admin_path?><?= $controller?><?=$id?>/';

        $.get(url+'ajax?task=delete_good&good_id='+id
            , function(result){
                if (result == 1)
                {
                    $('.DetailsTab').load(url+'ajax?task=details');
                    alert(Chimera.lang.saved);
                }
                else
                    alert('Server error!'); 
            });
        return false;
    }

</script>

<strong style="font-size:14px"><?= I18n::get('Order') ?> N<?= $obj['id']?> <?= I18n::get('from') ?> <?= date("d-m-Y", strtotime($obj['date_create']) )?></strong>

<table cellspacing="10" style="border-right:1px dotted grey; border-bottom:none; width:100%">
	<tr valign="top">
        <td nowrap style="width:30%" class="greyitalic"><?= I18n::get('Customer') ?>:</td>
        <td><?$customer['name']?><?= $customer['default_address']?><?= $obj['shipment_name']?><br><?= $obj['shipment_address']?></td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td align="right">            
            <? foreach($customer_shipment_addresses as $key => $item) $options[$key] = implode(', ', $item); ?>
            <? //$options = array(0 => "Текущий")+$options?>            
            <?= Form::select('shipment_address_id', $options, Arr::get( $obj,'shipment_address_id', $_POST['shipment_address_id']) )?>
            
        </td>
	</tr>
	<tr>
        <td nowrap class="greyitalic"><?= I18n::get('Data to account') ?>:</td>
        <td><?= $obj['billing_name']?><br><?= $obj['billing_address']?></td>
		
	</tr>
	<tr>
		<td>&nbsp;</td>
        <td align="right">
            <? foreach($customer_billing_addresses as $key => $item) $options[$key] = implode(', ', $item); /* $value['is_company'] = NULL */?>
            <? //$options = array(0 => "Текущий")+$options?>
            <?= Form::select('billing_address_id', $options, Arr::get($obj, 'billing_address_id', $_POST['billing_address_id'])  );?>
        </td>
	</tr>
    <tr>
        <td class="greyitalic"><?= I18n::get('Phone') ?>:</td>
        <td><?= $customer['phone']?></td>
    </tr>
    <tr>
        <td class="greyitalic">E-mail:</td>
        <td><a href="mailto:<?= $customer['email']?>"><?= $customer['email']?></a></td>
    </tr>
    <tr>
        <td class="greyitalic"><?= I18n::get('Payment Method') ?>:</td>
        <td>
            <?= Model_Content::customSelect('payment_method_id', $payment_methods, Arr::get($obj, 'payment_method_id', $_POST['payment_method_id']),NULL, NULL, 'name' );?>
                        
        </td>
	</tr>
	<tr>
		<td class="greyitalic"><?= I18n::get('Shipment Method') ?>:</td>
        <td colspan="2">
            <?= Model_Content::customSelect('shipment_method_id', $shipment_methods, Arr::get($obj,'shipment_method_id', $_POST['shipment_method_id']), NULL,NULL, 'name');?>
        </td>
<!--        <td colspan="2" align="right"><a href="#" class="greenBtn flRight"><?= I18n::get('Save')?></a></td>-->
    </tr>

</table>