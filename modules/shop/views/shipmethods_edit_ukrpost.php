<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>

<!--floating block-->

<form method="post" action="">
<input type="hidden" name="basic_fs_name" value="<?= Arr::get($obj, 'basic_fs_name', $obj['fs_name'])?>" />
<input type="hidden" name="settings_id" value="<?= $obj['settings_id']?>" />
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
   
    <input type="text" name="actTab" id="actTab" value="common" />
    <div class="absBlocks floatingInner">
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td style="width:37%">
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                        <span><span><?= I18n::get('Save and exit') ?></span></span></button>
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">

                                        <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="absBlocks floatingPlaCorner L"></div>
        <div class="absBlocks floatingPlaCorner R"></div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

        <!--floating block-->

        <div class="rel" >

    <div class="whiteblueBg padding20px">          
        <? $textareaID = array('ckeditor','ckeditor2') ?>
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>       
        <? include MODPATH . ADMIN_PATH . '/views/system/wysiwyg.php'; ?>
        <div class="innerGray" style="padding:0 25px">
            <div style="padding: 5px;"  class="greyitalic">
              <?= I18n::get('Title') ?>:<br />
              <input type="text" name="name" value="<?= Arr::get($obj,'name', $_POST['name'])?>" style="width:20em;"/>
            </div>
            <div style="padding: 5px;" class="greyitalic">
              <?= I18n::get('Description') ?>:<br />
              <textarea name="description" rows="5" style="width:99%;" id="ckeditor"><?= Arr::get($obj,'description', $_POST['description'])?></textarea>
            </div>
            <label style="padding-left:5px"><input type="checkbox" name="letter_description" value="1" <?if( $obj['letter_description'] || $_POST['letter_description'] ):?>checked="checked"<?endif;?> />
            <?= I18n::get('Include a description in a letter order') ?></label><br /><br />
            <div style="padding: 5px;" class="greyitalic">
              <?= I18n::get('Howto') ?>:<br />
              <textarea name="text" rows="5" id="ckeditor2"><?= Arr::get($obj,'text', $_POST['text']);?></textarea>
            </div>
            <div style="padding: 5px;" class="greyitalic">
              <?= I18n::get('Margin') ?>, <?= Kohana::config('shop.default_currency_char')?>:<br />
              <input type="text" name="cost" value="<?= Arr::get($obj,'cost', $_POST['cost'])?>" style="width:5em;"/>
            </div>
            <div style="padding: 5px;" class="greyitalic">
              <?= I18n::get('Price per 1 kg') ?>.:<br />
              <input type="text" name="percent" value="<?= Arr::get($obj,'percent', $_POST['percent'])?>" style="width:5em;"/>
            </div>
            <div style="padding: 5px;" class="greyitalic">
              <?= I18n::get('Sort order') ?>.:<br />
              <input type="text" name="orderid" value="<?= Model_Content::arrGet( $obj, 'orderid', Model_Content::arrGet($_POST,'orderid',9998))?>" style="width:5em;"/>
            </div>
            <br/>
            <br/>
        </div>
        <div class="clear"></div>
        <br />
       
    </div>
   
</form>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>

</div>
