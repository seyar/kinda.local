<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>

<form method="post" action="">
    <!--floating block-->

    <div class="rel whiteBg floatingOuter_shop" id="contentNavBtns" style="height: 43px;">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding:20px;">
                <table width="100%" >
                    <tr>

                        <td class="button_clients" style="width:46%;text-align: left;">
                            <button onclick="document.location.href='/admin/brands/0/edit';" type="button" class="btn blue" style="padding-left: 5px; ">
                                <span><span><? echo I18n::get('Create brand'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><? echo I18n::get('With marked'); ?></td>

                        <td style="padding-top:1px"><select id="list_action"><option value="delete"><? echo I18n::get('Delete'); ?></option></select></td>

                        <td style="text-align: right;"><button class="btn formUpdate" type="button">
                                <span><span><? echo I18n::get('Apply'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>

                        <td nowrap="nowrap">
                            <?=
                            Form::select('rows_per_page', array(
                                        '10' => '10 ' . I18n::get('lines'),
                                        '20' => '20 ' . I18n::get('lines'),
                                        '30' => '30 ' . I18n::get('lines'),
                                            ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page'))"))
                            ?>
                        </td>
                    </tr>
                    <tr >

                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>
    <div class="rel whiteBg">
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
                            <!-- **** -->
                            <table width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab">
                                <thead>
                                    <tr class="shop_head_current">
                                        <th style="padding-left:0;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll" /></th>
                                        <th style="width:5%" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>">№</th>
                                        <th style="width:20%;text-align: left;" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>"><? echo I18n::get('Logotype'); ?></th>
                                        <th style="width:70%" class="tLeft <?= Admin::table_sort_header('user_last_login') ?>"><?= I18n::get('Title') ?></th>
                                    </tr>
                                </thead>
                                <tbody width="100%" class="shop_current">
                <? foreach ($rows as $key => $value): ?>
                                <tr class="<? if ($key % 2) echo'even'; else echo 'odd'; ?>">
                                <td style="padding-right:8px;" class="tCenter"><input type="checkbox" name="chk[<?= $value['id'] ?>]" value="<?= $value['id'] ?>"  class="listCheckbox checkBox" /></td>
                                <td style="text-align:left"><?=$value['id']?></td>
                                <td style="text-align:left;">
                                    <img src="/static/media/shop/brands/<?=$value['logo']?>">
                                </td>
                                <td style="text-align:left;padding-left: 5px;"><a href="/admin/brands/<?=$value['id']?>/edit"><?=$value['name']?></a></td>
                            </tr>

                <? endforeach; ?>
                            </tbody>
                        </table>
        <? if ($pagination): ?>
        <?= $pagination ?>
        <? endif; ?>
                                    <!-- **** -->
        <? //echo Kohana::debug($kohana_view_data);   ?>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
