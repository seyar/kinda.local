<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:59:38 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_common.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="flLeft" style="width: 100%;padding-bottom: 13px;">
    <table width="100%" class="italic grayText2 borderNone shop_seo">
        <tr >
            <td style="width: 49%;vertical-align:top;"><?php echo I18n::get('Quantity') . ':' ?> <br />
                <div class="winput seo_padding_top" style="padding-bottom: 16px">
                    <input type="text" name="quantity" value="<?= Arr::get($obj,'quantity', Arr::get($_POST,'quantity'));?>" style="width:100%;"/>
                </div>

                <?php echo I18n::get('Date of receipt at the warehouse') . ':' ?> <br />

                <div class="winput seo_padding_top">
                    <input type="text" name="store_approximate_date" class="date_input" value="<?= Arr::get($obj,'store_approximate_date', Model_Content::arrGet($_POST, 'store_approximate_date', date('Y-m-d') ) );?>" style="width:100%;"/>
                </div>
            </td>
            <td style="padding: 0 20px 0 20px;width: 1px;">
                <div style="border-right: 1px dotted #909EA7;height:149px; margin-left:3px;"></div>
            </td>
            <td style="vertical-align: top;width: 49%"><?php echo I18n::get('Cost') . ':' ?> <br />
                <table style="width: 100%; border-top: 1px dotted #909EA7;margin-top: 11px; margin-bottom: 11px;border-bottom:  1px dotted #909EA7;" id="shop_price">
                    <tr>
                        <td style="width: 25%">
                            <span><?= I18n::get('Currency')?>:</span><br><br>
                        </td>
                        <td style="width: 25%">
                            <span><?= I18n::get('Exchange')?>:</span><br><br>
                        </td>
                        <td style="width: 25%">
                            <span><?php echo I18n::get('Price') ?>:</span><br><br>
                        </td>
                        <td style="width: 25%">
                            <span><?php echo I18n::get('Old price') ?>:</span><br><br>
                        </td>
                    </tr>
                    <?if( Kohana::config( 'shop.rateMode') == 'auto')
                        include(dirname(__FILE__).'/product_price_autorate.php');
                    else
                        include(dirname(__FILE__).'/product_price_manualrate.php');
                    ?>
                    
                </table>
                <?php echo I18n::get('first cost') . ':' ?>
                <div style=" height: 6px;"></div>
                <input type="text" name="cost" value="<?= Arr::get($obj,'cost', Arr::get($_POST,'cost'));?>" style="width:100%;" />
            </td>

        </tr>

    </table>
</div>

<div class="clear"></div>