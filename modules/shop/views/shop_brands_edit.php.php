<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:27:18 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/static/admin/js/settings.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.serializeanything.js"></script>

<!--это для буттона который загружает бренд-->
<!--<script type="text/javascript" src="/static/admin/js/jquery.custom-input-file.js"></script>-->
<script type="text/javascript" src="/static/admin/js/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="/static/admin/js/jQuery.fileinput.js"></script>
<script type="text/javascript" src="/static/admin/js/enhance.js"></script>
<link rel="stylesheet" href="/static/admin/css/jquery.custom-input-file.css" type="text/css" />
<!--это конец для буттона который загружает бренд-->
<!--floating block-->


<!---->





<!---->

<form method="post" action="" enctype="multipart/form-data">

    <div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">

        <input type="text" name="actTab" id="actTab" value="common" />
        <div class="absBlocks floatingInner">
            <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td style="width:37%">
                            <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                <span><span><?= I18n::get('Save and exit') ?></span></span></button>
                            <button  class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">

                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?= $admin_path . $controller ?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel" >

        <div class="whiteblueBg padding20px" style="height: 133px;">
            <table style="border-bottom: 0px;"width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab">
                <tr>
                    <td width="50%">
                        <span style="float: left; font-style: italic; display: block; padding-top: 5px; padding-bottom: 7px;"><?= I18n::get('Title') ?>:</span>
                        <input type="text" style="float: left; display: block; width: 98%; padding-bottom: 2px; margin-bottom: 11px;" value="<?= $obj['name'] ?>" name="name"  >



                        <span style="font-style: italic; float: left; display: block; margin-right: 0px; padding-right: 117px; padding-bottom: 7px;"><?= I18n::get('Logotype') ?>:</span>
                        <input id="download-but-button"  value="" type="text" style="float: left; width: 64%; display: block;"    >

                        <div style="position: relative;">
                        <button id="download-but" class="btn blue " type="button"  style="float: right; padding-bottom: 2px;"  >
                            <span><span><?= I18n::get('Download') ?></span></span>
                        </button>
                        <script type="text/javascript">
//                            $(function () {
//                                $("#download-but-hidden1").customInputFile({
//                                    filename: "#download-but-hidden",
//
//                                });
//                            });
                            $(document).ready(function() {
                                $("#download-but-button").val($("#fileupload-1").val());
                                console.log($("#download-but-hidden").val());
                            });
                            function push_value(){
                                $("#download-but-button").val($("#download-but-hidden").val());
                            }
                        </script>
                            <input class="btn blue " id="download-but-hidden" name="logo" type="file" style="margin-top: 84px; height: 18px; left: 375px; position: absolute; opacity: 0;"  onchange="push_value();false;"/>

                        <input type="hidden" autocomplete="off" id="fileupload-1" name="oldFileName" value="<?= $obj['logo'] ?>" />
                        </div>
<!--                        -->
<!--		<fieldset>
			<legend>Profile image</legend>
			<label for="file">Choose photo</label>
			<input type="file" name="file" id="file" />
			<input type="submit" name="upload" id="upload" value="Upload photo" />
		</fieldset>-->


<!--                        -->

                    </td>
                    <td width="50%">
                        <a style="float: left; padding-left: 16px;" href="#"><img src="/static/media/shop/brands/<?= $obj['logo'] ?>" /></a>
                    </td>
                </tr>

            </table>
            <div class="clear"></div>
            <br />


        </div>

</form>




<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>

</div>
