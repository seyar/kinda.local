<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:59:38 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_common.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<input type="hidden" name="parents_path" value="<?=$obj['parents_path'];?>"/>
<div class="flLeft" style="width: 100%;padding-bottom: 13px;font-style: italic">
    <table width="100%" class="italic grayText2 borderNone shop_seo">
        <tr >
            <td style="width: 49%;vertical-align:top;"><?php echo I18n::get('Title') . ':' ?> <br />
                <div class="winput seo_padding_top" style="padding-bottom: 16px">
                    <input type="text" name="name" value="<?= htmlentities($obj['name'],ENT_COMPAT, 'UTF-8'); ?>" style="padding-right: 0px;"/>
                </div>

                <?php echo I18n::get('Keywords, SEO') . ':' ?> <br />

                <div class="winput seo_padding_top"  >
                    <? $a = array();
                    foreach ($edit_categories_list as $value)
                    {
                        $a[$value['id']] = $value['name'];
                    } ?>
                    <textarea name="seo_keywords" style="width: 98%;"><?= $obj['seo_keywords']?></textarea>
                </div>
            </td>
            <td style="padding: 0 20px 0 20px;width: 1px;">

            </td>
            <td style="width: 49%;vertical-align:top;"><?php echo I18n::get('URL,SEO') . ':' ?> <br />
                <div class="winput seo_padding_top" style="padding-bottom: 16px">
                    <input type="text" name="seo_url" value="<?= $obj['seo_url']; ?>" style="padding-right: 0px;"/>
                    </div>

<?php echo I18n::get('Description') . ':' ?> <br />

                <div class="winput seo_padding_top" >
<? $a = array();
                    foreach ($brends as $value)
                    {
                        $a[$value['id']] = $value['name'];
                    } ?>
                    <textarea name="seo_description" style="width: 98%;"><?= $obj['seo_description']?></textarea>
                </div>


            </td>

        </tr>

        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px;">
                    <tr>
                        <td>
<?php echo I18n::get('Number sorting') . ':' ?> <br />

<?= Form::input('order_id', $obj['order_id']); ?>
                        </td>
                        <td style="width:50%;padding-right: 0px;">
<?php echo I18n::get('Parent category') . ':' ?> <br />
<?= Model_Content::customSelect('parent_id', $categories, Model_Content::arrGet($obj, 'parent_id', $_GET['parent_id']) , NULL, 'id', array('name'))?>
                        </td>

                    </tr>
                </table>
            </td>
            <td style="padding: 0 20px 0 20px;width: 1px;">

            </td>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" style="border: 0px;">
                    <tr>
                        <td>
                            <?php echo I18n::get('Page Title').':' ?> <br />

                            <?= Form::input('page_title', Model_content::arrGet($obj, 'page_title'));?>                            
                        </td>
                        <td style="width:50%;padding-right: 0px;">
                                <?php echo I18n::get('Status') . ':' ?> <br />
                                <? $options = array('verified'=>I18n::get('Verified'),'unverified'=>I18n::get('Unverified'));?>
                                <?= Form::select('status', $options, $obj['status'], $attributes); ?>
                            
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        <table width="100%" class="italic grayText2 borderNone shop_seo">
            <tr>
                <td>
<?php echo I18n::get('Text on the category page:')  ?>
                    <!--                <div class="winput seo_padding_top"></div>-->
                </td>
            </tr>
            <tr>
                <td>
                    <!-- -->
                    <script type="text/javascript" src="/vendor/ckeditor/ckeditor.js"></script>

                    <script type="text/javascript">
                        var config;
                        jQuery(document).ready(function()
                        {
                            config = {
                                toolbar:
                                    [
                                    ['Styles','Format','Font','FontSize'],
                                    ['TextColor','BGColor'],
                                    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
                                    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
                                    '/',
                                    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
                                    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
                                    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                                    ['Link','Unlink','Anchor'],
                                    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],
                                    ['Templates','Maximize','ShowBlocks','-','Source']
                                ]
                                , defaultLanguage: 'ru'
                                , language: 'ru'
                                , filebrowserBrowseUrl : '/vendor/elfinder/elfinder.html'

                            };

                            var ckeditor = CKEDITOR.replace("ckeditor", config);

                        });
                        
                        function deleteCatImg( id )
                        {
                            $.ajax({
                                url:'/admin/shop/categories/'+id+'/deleteCatImg',
                                success:function(data)
                                { 
                                    if(data == 1) $('div[rel=img_'+id+']').remove();
                                }
                            });
                        }
                    </script>

                    <!-- -->

                    <textarea rows="15" cols="44" style="width:98%;" name="full_description" id="ckeditor"
                              class="jquery_ckeditor"><?=$obj['description']?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <input type="file" name="catImage" value="Обзор"/>
                <?if(file_exists(MEDIAPATH.'shop/categories/thumb_'.$obj['id'].'.jpg')):?>
                <div class="rel catImage" rel="img_<?=$obj['id']?>">
                    <a href="#" class="ajax" onclick="deleteCatImg(<?=$obj['id']?>);return false;"><img src="/static/admin/images/del.png" alt=""/></a>
                    <img src="/static/media/shop/categories/thumb_<?=$obj['id']?>.jpg" alt=""/>
                </div>
                <?endif;?>
            </td>
        </tr>
    </table>


</div>

<div class="clear"></div>
<style>
    .catImage{ min-height: 120px;width: 120px; border:1px solid #ccc;}
    .catImage img{ max-width: 100%;}
    .catImage a{ position: absolute;width: 12px; height: 12px; top: 3px; right: 3px;}
</style>