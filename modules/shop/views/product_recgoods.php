<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="padding20px">
    <div>    
    <br />
    <?= Model_Content::customSelect('cats', $edit_categories_list, NULL, array('onchange'=>'getGoods( jQuery("#cats").val() )'), 'id', 'name')?>
    <br />
    <br />
    <p id="preloader" class="tCenter hidden"><img src="/static/admin/images/preloader.gif" alt="" width="20"/></p>
    <?= Form::select( 'recgoods', array(), NULL, array('class'=>'hidden uncustomized', 'id'=>'recgoods') )?>
    <span class="hidden"><button class="btn" type="button" onclick="addGood( $('#recgoods_iconselect span').text(),$('#recgoods').val() );return false;"><span><span><? echo I18n::get('Add')?></span></span></button></span>
    <br />
    <div id="addedGoodsProto" class="hidden">
        <p>
            <input type="hidden" class="goodid" name="recgoodid[]" value="" disabled="disabled"/>
            <input type="text" class="goodname" name="recgoodname[]" value=""  disabled="disabled" style="width:30em;"/>
            <a href="#" onclick="$(this).parent().remove();return false;"><img src="/static/admin/images/del.png" alt="" class="vMiddle"/></a>
        </p>
    </div>
    <div class="addedGoods">
        <? foreach($recGoods as $item):?>
        <p>
           <input type="hidden" class="goodid" name="recgoodid[]" value="<?=$item['id']?>" />
           <input type="text" class="goodname" name="recgoodname[]" value="<? if(isset($item['name']) && !empty($item['name']) ) echo Arr::get($item, 'name', $item['alter_name']);else echo $item['alter_name']?>"  style="width:30em;"/>
           <a href="#" onclick="$(this).parent().remove();return false;"><img src="/static/admin/images/del.png" alt="" class="vMiddle"/></a>
        </p>
        <?endforeach;?>
    </div>
</div>

    <script>
    function getGoods(id)
    {
        $('#preloader').removeClass('hidden');
        $('#recgoods').addClass('hidden');
        $('#recgoods_iconselect').parent().remove();
        $.getJSON(
            '<?= $admin_path?><? $controller?>recgoods/'+id+'/getGoods/',
            function(data)
            {
                if( typeof(data) == 'object' && data.length != 0)
                {
                    var options = '';
                    
                    
                        for(i in data)
                        {
                            var goodname = data[i]['name'].length != 0 ? data[i]['name'] : data[i]['alter_name'];
                            options += '<option value="'+data[i]['id']+'">'+goodname+'</option>'
                        }
                    
                    $('#recgoods').html(options);
                    $('#recgoods').removeClass('hidden');
                    $('#recgoods').next().removeClass('hidden');
                    $('#recgoods').SelectCustomizer();
                }else{
                    $('#recgoods').addClass('hidden');
                    $('#recgoods').next().addClass('hidden');
                }
                $('#preloader').addClass('hidden');
            }
        );
    }

    function addGood( val,id )
    {
        var obj = $('#addedGoodsProto').clone();
        obj.removeClass('hidden').children('p').children('.goodname').attr('disabled','').val(val);
        obj.children('p').children('.goodid').attr('disabled','').val(id);
        $('.addedGoods').prepend( obj );
    }
</script>
<style>
    .addedGoods{padding: 8px 0;}
</style>
    
</div>