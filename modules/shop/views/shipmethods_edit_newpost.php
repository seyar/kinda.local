<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/static/admin/js/edit.js"></script>
<style type="text/css">
    #proto{ padding:0 10px 0 0; }
    .row{padding: 5px 0;}
</style>
      <script type="text/javascript">
          function addvalue()
          {
              var div = $('#proto').clone();
              /*$(div).find('input').each(function(){ $(this).attr('name', $(this).attr('rel') ); });*/
              $('#rows').prepend( $(div).removeClass('flLeft').addClass('row').attr('id','') );
              $('#rows .row a.hidden').removeClass('hidden');
              $('#proto input').val('');
          }

          function delValue(thisObj)
          {
              $(thisObj).parent().remove();
          }
      </script>


<!--floating block-->

<form method="post" action="">
<input type="hidden" name="basic_fs_name" value="<?= Arr::get($obj, 'basic_fs_name', $obj['fs_name'])?>" />
<input type="hidden" name="settings_id" value="<?= $obj['settings_id']?>" />
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
   
    <input type="text" name="actTab" id="actTab" value="common" />
    <div class="absBlocks floatingInner">
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td style="width:37%">
							<button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
								<span><span><?= I18n::get('Save and exit') ?></span></span></button>
							<button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">

								<span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="absBlocks floatingPlaCorner L"></div>
        <div class="absBlocks floatingPlaCorner R"></div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

        <!--floating block-->

        <div class="rel" >

    <div class="whiteblueBg">
                      <? $textareaID = array('ckeditor','ckeditor2') ?>
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
        <? include MODPATH . ADMIN_PATH . '/views/system/wysiwyg.php'; ?>
        
        <div class="innerGray" style="padding:0 25px">
          <div style="padding: 5px;">
              Наименование<br/>
              <input type="text" name="name" value="<?=Model_content::arrGet($obj,'name',Arr::get($_POST,'name'))?>"  style="width:20em;"/>
          </div>
          <div style="padding: 5px;">
              Описание<br/>
              <textarea name="description" rows="5" id="ckeditor" style="width:99%;"><?= Model_content::arrGet($obj,'description',Arr::get($_POST,'description'))?></textarea>
          </div>
          <div style="padding: 5px;">
          <label><input type="checkbox" name="letter_description" value="1" <?if( Model_Content::arrGet($obj, 'letter_description', Arr::get($_POST,'letter_description') )):?>checked="checked"<?endif;?> />
          Включить инструкцию в письмо заказа</label>
          </div>
          <div style="padding: 5px;">
              Краткая инструкция<br/>
              <textarea name="text" rows="5" id="ckeditor2"><?=Model_content::arrGet($obj,'text',Arr::get($_POST,'text'))?></textarea>
          </div>    
            <div style="padding: 5px;" class="greyitalic">
              <?= I18n::get('Sort order') ?>.:<br />
              <input type="text" name="orderid" value="<?= Model_Content::arrGet( $obj, 'orderid', Model_Content::arrGet($_POST,'orderid',9998))?>" style="width:5em;"/>
            </div>
          <div style="padding: 5px;">
              Стоимость доставки:<br/>
              <span style="color:gray;font-size: 10px;">
                  В форме определите пары [сумма_заказа, стоимость_доставки]. Каждая пара определяет "стоимость_доставки" для заказов, сумма которых ниже чем "сумма_заказа". Для заказов, сумма которых выше максимальной указанной, стоимость доставки считается нулевой. 
                  Стоимость доставки для заказов, сумма которых менее указанной. Вы можете указать фиксированную стоимость доставки (в основной валюте) или в процентах от суммы заказа. Например, значение "10" обозначает фиксированную стоимость доставки, значение "10%" рассчитает стоимость доставки как 10% от сумма заказа.
              </span><br/><br/>

              <div class="first_row">
                  <div class="flLeft" id="proto">
                      <input type="text" name="border_sum[]" value="<?=Model_content::arrGet($obj,'border_sum',Arr::get($_POST,'border_sum'))?>" style="width:5em;"/>
                      <input type="text" name="border_value[]" value="<?=Model_content::arrGet($obj,'border_value',Arr::get($_POST,'border_value'))?>" style="width:5em;"/>
                      <a href="#" onclick="delValue(this);return false;" class="hidden"><img src="/static/admin/images/del.png" alt=""/></a><br/>
                  </div>
                  <button class="btn flLeft" type="button" onclick="addvalue();return false;"><span><span><?echo I18n::get('Add')?></span></span></button>
                  <div class="clear"></div>
                      <span style="color:gray;font-size: 10px;">Сумма, <?= Kohana::config('shop.default_currency_char')?></span>
                      <span style="padding: 0 0 0 50px;color:gray;font-size: 10px;">Цена, <?= Kohana::config('shop.default_currency_char')?></span>
              </div>                          
              <div id="rows">
                  
                  <? foreach($obj['borders'] as $key => $item):?>
                  <?if( !is_array($item)):?>
                    <div class="row">
                          <input type="text" name="border_sum[]" value="<?=$key ? $key : $_POST['border_sum']?>" style="width:5em;"/>
                          <input type="text" name="border_value[]" value="<?=$item ? $item : $_POST['border_value']?>" style="width:5em;"/>
                          <a href="#" onclick="delValue(this);return false;"><img src="/static/admin/images/del.png" alt=""/></a><br/>
                      </div>
                  <?endif;?>
                  <?endforeach;?>
              </div>
          </div>

          <br/>
          <br/>
      </div>
        <div class="clear"></div>
        <br />
       
    </div>
   
</form>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>

</div>
