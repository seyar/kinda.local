<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:59:38 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_common.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>

<?// die(Kohana::debug($obj));?>
<div class="flLeft" style="width: 100%;padding-bottom: 13px;">
    <table width="100%" class="italic grayText2 borderNone shop_seo">
        <tr ><td colspan="2"><?php echo I18n::get('Product page URL') . ':' ?> <br />
                <div class="winput seo_padding_top"><input type="text" name="seo_url" value="<?=$obj['seo_url'] ?>" style="width:100%;"/></div>
            </td>
        </tr>
        <tr >
            <td style="width:50%;border-right: 1px dotted #909EA7;padding-right: 39px;"><?php echo I18n::get('Keywords') . ':' ?> <br />
                <div class="winput seo_padding_top"><textarea style="width:97%" name="seo_keywords"><?=$obj['seo_keywords'] ?></textarea></div>
            </td>
            <td style="width:50%;padding-left: 39px;"><?php echo I18n::get('Descriptions') . ':' ?> <br />
                <div class="winput seo_padding_top"><textarea style="width:100%" name="seo_description"><?=$obj['seo_description'] ?></textarea></div>
            </td>
        </tr>


    </table>
</div>

<div class="clear"></div>