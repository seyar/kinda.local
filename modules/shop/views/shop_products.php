<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>
<script type="text/javascript" src="/vendor/jquery/jquery.cookie.js"></script>
<link rel="stylesheet" type="text/css" href="/static/admin/css/style-ie.css"/>
<!--fancybox-->
		<link rel="stylesheet" type="text/css" href="/vendor/jquery/fancybox/jquery.fancybox.css" />
		<script type="text/javascript" src="/vendor/jquery/fancybox/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="/vendor/jquery/fancybox/jquery.fancybox-1.2.1.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){

				$("a.gallery").fancybox({ 'hideOnContentClick': true });
			});
		</script>
<!--end fancybox-->

<form method="post" action="" onsubmit="checkFilterValues();" enctype="multipart/form-data">
    <input type="hidden" name="cat" value="<?= Arr::get($filter, 'cat', 1); ?>"/>

    <!--floating block-->
    <div class="flLeft outerLeftCol display-panel" id="id_panel" style="<?= $style_panel; ?>">
		<a href="#" id="button-out"></a>
        <ul class="leftSubmenu">
            <li><a href="/admin/categories/edit/<? if ($filter['cat']): ?>?parent_id=<?= Arr::get($filter, 'cat'); ?><? endif; ?>"><?= I18n::get('Add') ?></a></li>
            <li><a href="<?= $admin_path ?>categories/<?= Arr::get($filter, 'cat', 1); ?>/delete/" onclick="<? if (Arr::get($filter, 'cat', 0) != 0): ?>if(!confirm('<?= I18n::get('Delete?') ?>')) <? endif; ?>return false;"><?= I18n::get('Delete'); ?></a></li>
            <li style="border:none"><a href="/admin/categories/<?= Arr::get($filter, 'cat', 1); ?>/edit/"><?= I18n::get('Correct') ?></a></li>
        </ul>

		<div class="whiteBg pubTypes">
			<? if ($publ_types): ?>
				<ul class="pubTypesMenu" id="pubTypesMenu">
		<!--            <li <? if ($filter['pub_id'] == 0): ?>class="opened" <? endif; ?>style="padding-left:5px;">
						<a href="<?= $admin_path . $controller ?>" onclick="setFilter('0');return false;"><span><?= i18n::get('Root'); ?></span></a>
					</li>-->

					<li <? if($filter['cat'] == 0 || (int)$filter['cat'] == (int)Kohana::config('shop.root_category_id') ): ?>class="opened" <? endif; ?>style="padding-left:5px;" id="1">
						<a href="<?= $admin_path . $controller ?>" onclick="setFilter('1');return false;"><span><?= i18n::get('Root'); ?></span></a>
					</li>
					<? foreach ($publ_types[0] as $item): ?>
						<li rel="<?= $item['name'] ?>" id="<?= $item['id'] ?>" <? if( $item['id'] == $filter['cat'] ): ?>class="opened" <? endif; ?>>
							<? //=  Kohana::debug($item)?>
							<a href="#" class="listCollapse" <? if (!isset($publ_types[$item['id']])): ?>style="visibility:hidden;"<? endif; ?>>&nbsp;</a>
							<? //=$item['id'];?>
							<a href="#" onclick="setFilterCat(<?= $item['id'] ?>);return false;"><span><?= $item['name'] ?> <? if ($item['count'] <> 0)
						echo('(' . $item['count'] . ')') ?></span></a>


							<? if (isset($publ_types[$item['id']]) && is_array($publ_types[$item['id']])): ?>

								<? $menu = array($item['id'] => $publ_types[$item['id']]); ?>

								<? include 'shop_products_recurse.php'; ?>

		<? endif; ?>


						</li>
				<? endforeach; ?>
				</ul>
<? endif; ?>
		</div>
		<div class="clear"></div>
		<div class="leftColBottom" style="">
	<!--        <a href="<?= $admin_path ?>categories/catsTreewview" target="_blank"><?= i18n::get('Categories treeview'); ?></a>-->
		</div>
		<div class="clear"></div>
	</div>



    <!--floating block-->
	<div id="id_content" style="<?= $style_content; ?>">
		<div class="rel whiteBg floatingOuter_shop" id="contentNavBtns" style="height: 175px;">
			<a href="#" id="button-in-out" style="<?= $style_but ?>"></a>
			<div class="whiteblueBg absBlocks floatingInner" style="height: 167px;z-index: 10001;">
				<div class="padding:20px;">
					<table width="100%" class="">
						<tr>

							<td class="button_clients" style="width: 40%;">
								<button onclick="document.location.href='/admin/goods/edit';" type="button" class="btn blue" style="padding-left: 5px;">
									<span><span><? echo I18n::get('Create Good'); ?></span></span></button>
							</td>

							<td class="vMiddle" nowrap="nowrap"><? echo I18n::get('With marked'); ?></td>

							<td style="padding-top:1px"><select id="list_action"><option value="<?= $admin_path ?><?= $controller ?>delete"><? echo I18n::get('Delete'); ?></option></select></td>

							<td style="text-align: right;"><button class="btn formUpdate" type="button" >
									<span><span><? echo I18n::get('Delete'); ?></span></span></button>
							</td>

							<td class="vMiddle" nowrap="nowrap"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>

							<td nowrap="nowrap">
								<?=
								Form::select('rows_per_page', array(
											'10' => '10 ' . I18n::get('lines'),
											'20' => '20 ' . I18n::get('lines'),
											'30' => '30 ' . I18n::get('lines'),
												), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page'))"))
								?>
							</td>
						</tr>

						<tr><td style="height: 10px;" colspan="6"></td></tr>
						<tr>
							<td colspan="6" style="border-top:1px solid #FFFFFF;">
								<fieldset class="filter">
									<legend style="font-weight: bold; color: black;"><? echo I18n::get('Products filter'); ?></legend>
									<table width="100%" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
										<tbody>
											<tr>
												<td style="width:33%;vertical-align:top;">
													<table style="width: 100%;padding-right:18px" cellspacing="0" cellpadding="0" >
														<tr>
															<td colspan="4" style="padding-bottom: 6px;padding-right: 0px;"><input type="text" style="padding-right: 0px; width: 99%;" value="<?= Model_Content::arrGet($filter, 'shop_articul', I18n::get('scu')); ?>" rel="<? echo I18n::get('scu'); ?>" name="filter[shop_articul]" class="has_default_value"/></td>
														</tr>
														<tr>
															<td style="width: 1%">
																<span style="color:#818183;"><? echo I18n::get('Price'); ?>:</span>
															</td>
															<td style="width: 49%">
																<input type="text" style="width: 100%" value="<?= Model_Content::arrGet($filter, 'shop_price_ot', I18n::get('From')) ?>" rel="<? echo I18n::get('From'); ?>"  name="filter[shop_price_ot]" class="has_default_value">
															</td>
															<td style="width: 1%;padding-left: 8px;">
																-
															</td>
															<td style="width: 49%">
																<input type="text" style="width: 100%" value="<?= Model_Content::arrGet($filter, 'shop_price_do', I18n::get('To')); ?>" rel="<? echo I18n::get('To'); ?>" name="filter[shop_price_do]" class="has_default_value">
															</td>
														</tr>
													</table>
												</td>
												<td style="width:33%;vertical-align:top;">
													<table style="width: 100%;padding-right:18px;" cellspacing="0" cellpadding="0" >
														<tr>
															<td>
																<input type="text" style="width: 100%;" value="<?= Model_Content::arrGet($filter, 'name', I18n::get('Title')); ?>" rel="<? echo I18n::get('Title'); ?>" name="filter[name]" class="has_default_value">
															</td>
														</tr>
														<tr>
															<td style="padding-top: 6px;padding-right: 0px;">
																<?=
																Model_Content::customSelect('filter_price_categories_id', $priceCategories, Arr::get($_POST, 'filter_price_categories_id', $filter['filter_price_categories_id']), NULL, 'id', 'name')
																?>
															</td>
														</tr>
													</table>
												</td>


												<td style="width:33%;vertical-align:top;padding-right: 0px;">
													<table style="width: 100%;" cellspacing="0" cellpadding="0" >
														<tbody>
															<tr>
																<td colspan="4" style="padding:0 0 8px;">
																	<?=
																	Form::select('status', array(
																				'' => I18n::get('Select status'),
																				'active' => I18n::get('Active'),
																				'archive' => I18n::get('Archive'),
																				'unverified' => I18n::get('Unverified'),
																				'out_of_store' => I18n::get('Out of store'),
																				'from1c' => I18n::get('From1c')
																					), Model_Content::arrGet($filter, 'status'))
																	?>
																</td>
															</tr>
															<tr>
																<td style="width: 33%;text-align: right;">
																	<span style="color:#818183;"><? echo I18n::get('Quantity'); ?>:</span>
																</td>
																<td style="width: 33%">
																	<input type="text" style="width: 100%;" value="<?= Model_Content::arrGet($filter, 'shop_col_ot', I18n::get('From')); ?>" rel="<? echo I18n::get('From'); ?>"  name="filter[shop_col_ot]" class="has_default_value" >
																</td>
																<td style="padding-left:8px;width: 1%">
																	-
																</td>
																<td style="width: 33%;padding-right: 10px;">
																	<input type="text" style="width: 100%;" value="<?= Model_Content::arrGet($filter, 'shop_col_do', I18n::get('To')); ?>" rel="<? echo I18n::get('To'); ?>" name="filter[shop_col_do]"  class="has_default_value">
																</td>
															</tr>

														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan="2" style="padding: 8px 0 0 0;" nowrap="nowrap">
													<input type="text" style="padding-right: 0px; width: 13%;" value="<?= Model_Content::arrGet($filter, 'id', I18n::get('ID')); ?>" rel="<? echo I18n::get('ID'); ?>" name="filter[id]" class="has_default_value">
                                                    &nbsp;
													<label><input type="checkbox" name="filter[default_photo]" value="1" class="" <? if ($filter['default_photo'] == 1)
																		echo 'checked="checked"'; ?> style="margin-top: -1px;"/>
														<span nowrap="nowrap" style="padding-right: 16px; padding-left: 5px;color:#818183;"><? echo I18n::get('Without photo'); ?></span></label>
													<label><input type="checkbox" name="filter[is_bestseller]" value="1" class="" <? if ($filter['is_bestseller'] == 1)
																		echo 'checked="checked"'; ?> style="margin-top: -1px;"/>
														<span nowrap="nowrap" style="padding-right: 16px; padding-left: 5px;color:#818183;"><? echo I18n::get('Top'); ?></span></label>
													<label><input type="checkbox" name="filter[is_new]" value="1" class="checkBox" style="margin-top: -1px;" <? if ($filter['is_new'] == 1)
																		echo 'checked="checked"'; ?>/>
														<span nowrap="nowrap" style="padding-right: 16px; padding-left: 5px;color:#818183;"><? echo I18n::get('Novelty'); ?></span></label>
													<label><input type="checkbox" name="filter[show_on_startpage]" value="1"  class="checkBox" style="margin-top: -1px;" <? if ($filter['show_on_startpage'] == 1)
																		echo 'checked="checked"'; ?>/>
														<span nowrap="nowrap" style="padding-right: 16px; padding-left: 5px;color:#818183;"><? echo I18n::get('Displayed on the start page'); ?></span></label>

												</td>
												<td   style="text-align:right;padding-top: 6px;padding-right: 0px;" >
													<div style="">
														<button type="button" class="btn formFilter">
															<span><span><? echo I18n::get('Apply'); ?></span></span></button>
													</div>
												</td>
											</tr>

										</tbody>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="absBlocks side L"></div>
			<div class="absBlocks side R"></div>
		</div>
		<div class="rel whiteBg" id="idpanel2">
<? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
			<!-- **** -->
			<div>
				<!-- <div style="clear:both;"></div>-->
				<table width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab" style="">
					<thead>
						<tr class="shop_head">
							<th style="padding:0 8px 0 5px;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll" /></th>
							<th style="" class="tLeft <?= Admin::table_sort_header('id') ?>">ID</th>
							<th style="" class="tLeft"><? echo I18n::get('Photo'); ?></th>
							<th style="" class="tLeft <?= Admin::table_sort_header('alter_name') ?>"><? echo I18n::get('Title'); ?></th>
							<th style="" class="tLeft <?= Admin::table_sort_header('shop_prices') ?>"><? echo I18n::get('Price'); ?></th>
							<th style="" class="tLeft <?= Admin::table_sort_header('quantity') ?>"><? echo I18n::get('Quantity'); ?></th>
							<th style="" class="tLeft <?= Admin::table_sort_header('status') ?>"><? echo I18n::get('Status'); ?></th>
							<th style="" class="tLeft <?= Admin::table_sort_header('is_bestseller') ?>"><? echo I18n::get('T'); ?>.</th>
							<th style="" class="tLeft <?= Admin::table_sort_header('is_new') ?>"><? echo I18n::get('N'); ?>.</th>
							<th style="" class="tLeft <?= Admin::table_sort_header('show_on_startpage') ?>"><? echo I18n::get('SP'); ?>.</th>
						</tr>
					</thead>

					<tbody width="100%" class="shop_products">
<? //die(kohana::debug($rows)); ?>

<? $i = 0;
$y = 200; ?>
<? foreach ($rows as $key => $value): ?>

							<tr class="<? if ($i % 2) echo'even'; else echo 'odd'; $i++; ?>">
								<td style="padding-left:5px;" class="tCenter"><input type="checkbox" name="chk[<?= $value['id'] ?>]" value="<?= $value['id'] ?>"  class="listCheckbox checkBox" /></td>
								<td class="id"><?= $value['id'] ?></td>
								<td style="width:60px;;padding-left:0px;">
								
									<div class="goodlistimg">
									<? if ( isset($value['default_photo']) && !empty($value['default_photo']) ):?>
										<a class="gallery" href="/static/media/shop/<?= $value['id'] ?>/<?= $value['default_photo'] ?>">
											<img src="/static/media/shop/<?= $value['id'] ?>/prev_<?= $value['default_photo'] ?>"/>
										</a>
										<?else:?>
										<a class="gallery" href="/static/media/no_photo.jpg">
											<img src="/static/media/no_photo.jpg" alt=""/>
										</a>
										<? endif; ?>
									</div>
									
								</td>
								<td class="" style="width: 40%; text-align:left">
																
								<a title="<?=$value['name']?>" href="/admin/goods/<?= $value['id'] ?>/edit"><? if (isset($value['name']) && !empty($value['name']))
									echo Text::limit_chars( $value['name'], 80); else
									echo Text::limit_chars(Arr::get($value, 'alter_name', 'noname'), 80); ?></a>
									
									<br />
									
									</td>
								<td class="" style="padding-right:13px;text-align:right;"><?= $value['shop_prices'] ?> <span><? echo Kohana::config('shop')->backend_currency_char ?></span></td>
								<td class="" style="padding-right:31px;text-align:right;"><?= $value['quantity'] ?> <span><? echo I18n::get('pc'); ?></span></td>
								<td style="text-align:center ;z-index:<?= $y;
								$y = $y - 10; ?>" class="status">
	<? //=$value['status'] ?>
	<?=
	Form::select('good_id_' . $value['id'], array(
				'active' => I18n::get('Active'),
				'unverified' => I18n::get('Unverified'),
				'archive' => I18n::get('Archive'),
				'out_of_store' => I18n::get('Out of store'),
				'from1c' => I18n::get('From1c')
					), $value['status'], array('id' => 'good_id_' . $value['id'], 'onchange' => 'setStatusProduct(' . $value['id'] . ')'))
	?>
								</td>
								<td class="" style="text-align:center"><a name="is_bestseller" rel="<?= $value['id'] ?>" href="#"><img src="/static/admin/images/<?= ($value['is_bestseller'] == 0 ? 'minus.png' : 'plus.png') ?>"></a></td>
								<td class="" style="text-align:center"><a name="is_new" rel="<?= $value['id'] ?>" href="#"><img src="/static/admin/images/<?= ($value['is_new'] == 0 ? 'minus.png' : 'plus.png') ?>"></a></td>
								<td class="" style="text-align:center"><a name="show_on_startpage" rel="<?= $value['id'] ?>" href="#"><img src="/static/admin/images/<?= ($value['show_on_startpage'] == 0 ? 'minus.png' : 'plus.png') ?>"></a></td>
							</tr>
<? endforeach; ?>
					</tbody>
				</table>
<? if ($pagination): ?>
	<?= $pagination ?>
<? endif; ?>
			</div>

			<div class="absBlocks side L"></div>
			<div class="absBlocks side R"></div>
			<div class="absBlocks corner L"></div>
			<div class="absBlocks corner R"></div>
		</div>
	</div>
</form>

<script type="text/javascript">
    $(function(){
        $('.pubTypesMenu li.opened').parent().css('display','block');
        
        $('.pubTypesMenu li.opened').parents('li').each(function(){
            $(this).addClass('opened');
        });
    });
    
    jQuery(document).ready(function(){

        var scrollvalue = 0;
        /* for scroll to the current category */
        $('#pubTypesMenu').parent().scrollTop(0);
        $('#pubTypesMenu li.opened').each(function(){

            if( $(this).attr('id') && $(this).attr('id') != '1' )
            {
                topvalue = document.getElementById( $(this).attr('id') ).offsetTop;
                scrollvalue += topvalue;
            }
        });
        
        scrollvalue = scrollvalue - $('#pubTypesMenu').parent().height();
		scrollvalue += $('#pubTypesMenu').parent().height() - 325;

        $('#pubTypesMenu').parent().scrollTop(scrollvalue);
        
        $('.breadcrumbs ul').append( getBreadcrumbs() );

        $('li a.listCollapse').click(function(){
            toggleSubmenu( $(this).parent() );
			//            $('.pubTypesMenu li').removeClass('opened');
			//            $(this).parent('li').addClass('opened');

			return false;
        });
    });

    function getBreadcrumbs()
    {
        var breadcrumbsStr = '';
        
        $('.pubTypesMenu li.opened').each(function(){
            if( $(this).attr('rel') != undefined ) 
                breadcrumbsStr += '<li> &gt; <a href="#" onclick="setFilterCat('+$(this).attr('id')+');return false;">'+$(this).attr('rel')+'</a></li>';
        });
        
        return breadcrumbsStr;
    }

    function checkFilterValues()
    {
        $('.filter input').each(function(){
            if( $(this).val() == $(this).attr('rel') ) $(this).val('');
        });
    }

    function toggleSubmenu(thisObj)
    {
        if( $(thisObj).hasClass('opened') )
        {
            $(thisObj).children('ul').hide();
            $(thisObj).removeClass('opened');
        }
        else
        {
            $(thisObj).children('ul').show();
            $(thisObj).addClass('opened');
        }
    }
        
</script>