<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>
<script type="text/javascript">
    function checkFilterValues()
    {
        $('.filter input').each(function(){
            if( $(this).val() == $(this).attr('rel') ) $(this).val('');
        });
    }
    
    function setStatus ( a )
    {
        var status = $("#orders"+a).val();

        $.post("/admin/orders/"+a+"/statusUpdateOrders", {status:status} ,function(data){
            if(data != 1 ) alert('Server error');
        });
    }
</script>
<form method="post" action="" onsubmit="checkFilterValues();">
    <!--floating block-->

<? $payment_methods['-1'] = I18n::get('Payment Method') ?>
<? $shipment_methods['-1'] = I18n::get('Shipment Method') ?>

    <div class="rel whiteBg floatingOuter_shop" id="contentNavBtns" style="height: 135px;">
        <div class="whiteblueBg absBlocks floatingInner" style="height: 135px;">
            <div class="padding:20px;">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>

                        <td class="button_clients" style="width: 40%;">
                            <button onclick="document.location.href='/admin/customers/';" type="button" class="btn blue" style="padding-left: 5px; ">
                                <span><span><? echo I18n::get('Customers'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap" style="width:7%;"><? echo I18n::get('With marked'); ?></td>

                        <td style="padding-top:1px"><select id="list_action"><option value="delete"><? echo I18n::get('Delete'); ?></option></select></td>

                        <td style="text-align: right;width: 7%;"><button class="btn formUpdate" type="button">
                                <span><span><? echo I18n::get('Apply'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap" style="width:7%;"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>

                        <td>
                            <?=
                            Form::select('rows_per_page', array(
                                        '10' => '10 ' . I18n::get('lines'),
                                        '20' => '20 ' . I18n::get('lines'),
                                        '30' => '30 ' . I18n::get('lines'),
                                            ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page'))"))
                            ?>
                        </td>
                    </tr>

                    <tr style="height: 10px;"></tr>
                    <tr>
                        <td colspan="6" style="border-top:1px solid #FFFFFF;">

                            <fieldset class="filter">
                                <legend style="font-weight: bold; color: black;"><? echo I18n::get('Filter orders'); ?></legend>
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td style="width:25%">
                                                <table >
                                                    <tr>
                                                        <td><input type="text" class="has_default_value" style="width: auto;" rel="ID" value="ID" name="filter[ID]"></td>
                                                        <td><input type="text" class="has_default_value" style="width: auto;" rel="<? echo I18n::get('Customer ID'); ?>" value="<? echo I18n::get('Customer ID'); ?>" name="filter[customer_id]" ></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="padding-top: 6px;">
                                                            <?=Form::select('payment_methods', $payment_methods,'-1');?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:25%">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td><input type="text" class="has_default_value" style="width: 99%;" rel="<? echo I18n::get('Made out to'); ?>" value="<? echo I18n::get('Made out to'); ?>" name="filter[shipment_name]" ></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 6px; padding-right:0px;">
                                                            <?=Form::select('shipment_methods', $shipment_methods,'-1');?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:25%">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td><input type="text" class="has_default_value" style="width: 99%;" rel="<? echo I18n::get('When placed'); ?>" value="<? echo I18n::get('When placed'); ?>" name="filter[date_create]"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 6px; padding-right:0px;">
                                                          <?=
                                                                Form::select('status', array('-1'=>'')+$order_statuses ,
                                                                                '-1');
                                                                ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width:25%">
                                                <table style="width:100%" class="tRight">
                                                    <tr>
                                                        <td style="padding-top:3px;">
                                                            <? echo I18n::get('Total'); ?>
                                                        </td>
                                                        <td>
                                                            <input type="text" style="width: 79%;" class="has_default_value" rel="<? echo I18n::get('From'); ?>"  value="<? echo I18n::get('From'); ?>" name="filter[sum_ot]" >
                                                        </td>
                                                        <td>-</td>
                                                        <td>
                                                            <input type="text" class="has_default_value" style="width: 79%;" rel="<? echo I18n::get('To'); ?>" value="<? echo I18n::get('To'); ?>" name="filter[sum_do]" >
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right;padding-top: 6px; padding-right:4px;" >
                                                            <button type="button" class="btn formFilter" style="margin: 0px;">
                                                                <span><span><? echo I18n::get('Apply'); ?></span></span></button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </fieldset>

                        </td>
                    </tr>

                </table>

            </div>

        </div>

        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>
    <div class="rel whiteBg">
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
			<!-- **** -->
			<table width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab">
				<thead>
					<tr class="shop_head">
						<th style="padding:0 8px 0 4px;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll" /></th>
						<th style="" class="tLeft <?= Admin::table_sort_header('id') ?>"><?= I18n::get('Order ID')?></th>

						<th style="" class="tLeft <?= Admin::table_sort_header('shipment_name') ?>"><?= I18n::get('Made out to')?></th>
						<th style="width: 86px;" class="tLeft <?= Admin::table_sort_header('date_create') ?>"><?= I18n::get('Posted')?></th>
						<th style="" class="tLeft <?= Admin::table_sort_header('sum') ?>"><?= I18n::get('Total')?></th>

						<th style="width: 86px;" class="tLeft <?= Admin::table_sort_header('payment_methods') ?>"><?= I18n::get('Payment')?></th>
						<th style="width: 100px;" class="tLeft <?= Admin::table_sort_header('shipment_methods') ?>"><?= I18n::get('Delivery')?></th>
						<th style="width: 86px;" class="tLeft <?= Admin::table_sort_header('status') ?>"><?= I18n::get('Status')?></th>
					</tr>

				</thead>

				<tbody class="shop">
                <? $i=1;?>
                <? foreach ($rows as $key => $value):?>
					<tr class="<? if ($i % 2) echo'even'; else echo 'odd';$i++; ?>">
					<td style="padding-left:5px;" class="tCenter"><input type="checkbox" name="chk[<?= $value['id'] ?>]" value="<?= $value['id'] ?>"  class="listCheckbox checkBox" /></td>
					<td class="id" style="text-align:left;"><a href="<?=$admin_path?><?=$controller?><?=$value['id'] ?>/edit"><?= "Заказ №" . $value['id'] ?></a></td>
					<td class="id_chet" ><a href="<?=$admin_path?><?=$controller?><?=$value['id'] ?>/edit"><?=$value['shipment_name']?></a></td>
					<td class="tLeft"><?=substr($value['date_create'], 0, 10).'<br/>'
							.substr($value['date_create'], 11, 5)?></td>
					<td class="tLeft"><?=$value['sum']?> <span><? echo Kohana::config('shop')->default_currency_char ?></span></td>
					<td class="id_bank"><?=$value['payment_methods']?></td>
					<td class="tLeft"><?=$value['shipment_methods']?></td>
					<td class="status tLeft">
                        
                    <?= Form::select('orders'.$value['id'], $order_statuses, $value['status'],array('onchange'=> "setStatus(".$value['id'].");"))?>
					</td>
				</tr>


		<? endforeach; ?>
			</tbody>

		</table>

        <? if ($pagination): ?>
        <?= $pagination ?>
        <? endif; ?>
                                                                    <!-- **** -->
        <? //echo Kohana::debug($kohana_view_data);   ?>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>

</form>
