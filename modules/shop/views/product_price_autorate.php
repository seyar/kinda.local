<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>

<? foreach($edit_price_categories as $price_cat):?>
    <? foreach($edit_currencies_list as $key => $value):?>
        <tr>
            <td style="width: 25%">
                <?= $price_cat['name']?> (<?= $value['name']?>)
            </td>
            <td style="width: 25%">
                <?= $value['rate']?>
            </td>
            <td style="width: 25%">
                <?if( $value['id'] == $defaultCurrency ):?>
                    <input type="text" name="price[<?= $price_cat['id']?>][<?= $key;?>]" value="<?= number_format($obj['prices'][$price_cat['id']][$value['id']],2)?>" style="width:90%;"/>
                <? else:?>
                    <?= number_format($obj['prices'][$price_cat['id']][$value['id']],2)?>
                <?endif;?>
            </td>
            <td style="width: 25%">
                <?if( $value['id'] == $defaultCurrency):?>
                    <input type="text" name="oldprice[<?= $price_cat['id']?>][<?= $key;?>]" value="<?= number_format($obj['oldprices'][$price_cat['id']][$value['id']],2)?>" style="width:100%;"/>
                <?else:?>
                    <?= number_format($obj['oldprices'][$price_cat['id']][$value['id']],2)?>
                <?endif;?>
            </td>
        </tr>
    <?endforeach;?>
<?  endforeach;?>