<?php defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */

$rate = $edit_currencies_list[$defaultCurrency]['rate'];

foreach($edit_price_categories as $price_cat):
    $val = empty($obj['prices'][$price_cat['id']][$defaultCurrency]) ? $obj['prices'][$price_cat['id']][$defaultCurrency]  : $obj['prices'][$price_cat['id']][$defaultCurrency];
    $oldval = empty($obj['oldprices'][$price_cat['id']][$defaultCurrency]) ? $obj['oldprices'][$price_cat['id']][$defaultCurrency]  : $obj['oldprices'][$price_cat['id']][$defaultCurrency];
    
    ?>
        <tr>
            <td style="width: 25%">
                <?= $price_cat['name']?>
            </td>
            <td style="width: 25%">
                <?// $value['rate']?>
            </td>
            <td style="width: 25%">
                 <input type="text" name="price[<?= $price_cat['id']?>][<?= $defaultCurrency;?>]" value="<?= number_format($val,2)?>" style="width:90%;"/>
            </td>
            <td style="width: 25%">
                <input type="text" name="oldprice[<?= $price_cat['id']?>][<?= $defaultCurrency;?>]" value="<?= number_format($oldval,2)?>" style="width:100%;"/>
            </td>
        </tr>
<?  endforeach;?>