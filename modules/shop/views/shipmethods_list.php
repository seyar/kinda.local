<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>
<div class="absBlocks preloader hidden" style="margin:-5px 0 0 -55px;z-index:4; left:50%; top: 50%;width:110px;height: 16px;"><img src="/static/admin/images/preloader.gif" alt=""/></div>
<form method="post" action="">
    <!--floating block-->

    <div class="rel whiteBg floatingOuter_shop" id="contentNavBtns" style="height: 43px;">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding:20px;">
                <table width="100%">
                    <tr>

                        <td class="button_clients" style="width:46%;text-align: left;">
                            <button onclick="document.location.href='<?=$admin_path?><?=$controller?>add';" type="button" class="btn blue" style="padding-left: 5px; ">
                                <span><span><? echo I18n::get('Create New'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><? echo I18n::get('With marked'); ?></td>

                        <td style="padding-top:1px"><select id="list_action"><option value="delete"><? echo I18n::get('Delete'); ?></option></select></td>

                        <td style="text-align: right;"><button class="btn formUpdate" type="button">
                                <span><span><? echo I18n::get('Apply'); ?></span></span></button>
                        </td>

                        <td class="vMiddle" nowrap="nowrap"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>

                        <td nowrap="nowrap">
                            <?=
                            Form::select('rows_per_page', array(
                                        '10' => '10 ' . I18n::get('lines'),
                                        '20' => '20 ' . I18n::get('lines'),
                                        '30' => '30 ' . I18n::get('lines'),
                                            ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange' => "formfilter(jQuery('#rows_per_page'))"))
                            ?>
                        </td>
                    </tr>
                    <tr >

                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>
    <div class="rel whiteBg">
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
                            <!-- **** -->
                      <table width="100%" cellspacing="0" cellpadding="0" class="sortableContentTab" id="shipmentTab">
                          <thead>
                          <tr>
                              <th width="6%" class="tCenter"><input type="checkbox" name="" value="" style="width:auto;" class="listCheckboxAll"/></th>
                              <th class="sorting">&nbsp;<?= I18n::get('Title') ?>&nbsp;</th>
                              <!--<th width="10%">&nbsp;Стоимость;</th>-->
                              <!--<th width="10%">&nbsp;Наценка;</th>-->
                              <th width="7%">&nbsp;</th>
                          </tr>
                          </thead>
                          <tbody>
                          <? $i=0;?>
<? foreach($rows as $key => $value):?>
			  <tr class="<?= $i % 2 == 0 ? 'even' : 'odd'?>" rel="<?= $value['id']?>">
                              <td align="center" style="padding-left: 10px;"><input type="checkbox" name="chk[<?= $value['id']?>]" value="<?= $value['id']?>" class="listCheckbox" <?if($value['protected'] == 1):?>disabled="disabled"<?endif;?>/></td>
                              <td><a href="<?=$admin_path?><?=$controller?><?= $value['id']?>/edit" ><?= $value['name']?></a></td>
                              <!--<td align="center">{$value.cost|default:'0'}</td>-->
                              <!--<td align="center">{$value.percent|default:'0'}</td>-->
                              <td align="center" nowrap>
                                <a href="<?=$admin_path?><?=$controller?><?= $value['id']?>/status" ><img src="/static/admin/images/act_<?if(!$value['status']):?>red<?else:?>green<?endif;?>.png" alt="<?echo I18n::get('status')?>" title="<?echo I18n::get('status')?>"/></a>
<!--                                <a <?if($value['protected'] == 1):?>class="ajax" href="javascript:void(0);"<?else:?>href="<?=$admin_path?><?=$controller?><?=$value['id']?>/delete<?endif;?>" ><img src="/static/admin/images/del.png" alt="edit" title="<?echo I18n::get('Edit')?>"/></a>&nbsp;-->
                              </td>                              
                          </tr>
                          <?$i++;?>
<?endforeach;?>
                          </tbody>
                      </table>
        <? if ($pagination): ?>
        <?= $pagination ?>
        <? endif; ?>
                                    <!-- **** -->
        <? //echo Kohana::debug($kohana_view_data);   ?>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>

<script type="text/javascript" charset="utf-8" src="/vendor/jquery/jquery-ui-1.8.10.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="/static/admin/css/sortable_options.css"/>
<script type="text/javascript">
    $(document).ready(function(){
        
        $( "#shipmentTab tbody" ).sortable({ 
            stop: function(event, ui) {
                updateOrderId();
                setColors();
            }  
        });
        
    });
    
    function setColors()
    {
        var i = 0;
        $( "#shipmentTab tr" ).each(
            function()
            {
                $(this).removeClass( 'even' );
                $(this).removeClass( 'odd' );
                $(this).addClass( (i % 2 == 0 ? 'even':'odd') );
                i++;
            }
        );
    }
    
    function getIds()
    {
        var ret = {};
        i=0;
        $( "#shipmentTab tr" ).each(
            function()
            {
                ret[i] = $(this).attr('rel');
                i++;
            }
        );
        return ret;
    }
    
    function updateOrderId()
    {
        var ids = getIds();
        $('.preloader').removeClass('hidden');
        $.get(
            '<?= $admin_path?><?= $controller?>updateOrderId',
            {ids:ids},
            function(data)
            {
                $('.preloader').addClass('hidden');
                if(data != 1)
                    alert('Server error');
            }
        );
    }
</script>