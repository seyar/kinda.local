<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="padding20px">
<? $current_date = date('Y-m-d');?>
<? $i = 0;?>
<table class="formatedTbl textareaheight">    
    <tr>
<? foreach( $obj['add_fields'] as $key => $value):?>
    <? if($value['f_group']):?>
        </tr>
        <tr>
            <td class="descTd" width="24%" nowrap>
                <strong><i><?= $value['name']?></i></strong>
            </td>
            <td colspan="0"><hr height="1" /></td>
        </tr>
        <tr>
    <?else:?>
        
            <td class="descTd" width="12%">
                <input name="add_fields_id[<?=$value['f_id']?>]" value="<?=$value['id']?>" type="hidden" />

    <? $add_fields_id = "add_fields[{$value['id']}]";?>
                <div class="greyitalic" style="padding-bottom:3px"><span style="border-bottom:1px dashed grey; cursor:help" title="<?= $value['desc']?>"><?= $value['name']?>:</style></div>
            
            
            
                <?if($value['type'] == 'int'):?>
                        <input title="<?= $value['desc']?>" name="add_fields[<?=$value['f_id']?>]" value="<?= Arr::get($value, 'value_int', Arr::get($_POST, $add_fields_id, '0'))?>"
                               type="text" style="width: 6em;" />
                <?elseif ($value['type'] == 'float'):?>
                        <input title="<?= $value['desc']?>" name="add_fields[<?=$value['f_id']?>]" value="<?= Arr::get($value, 'value_float', Arr::get($_POST, $add_fields_id, '0'))?>"
                               type="text" style="width: 6em;" />
                <?elseif( $value['type'] == 'date'):?>
                        <input title="<?= $value['desc']?>" name="add_fields[<?=$value['f_id']?>]" value="<?= Arr::get($value, 'value_date', Arr::get($_POST, $add_fields_id, $current_date))?>"
                               type="text" class="date" style="width: 6em;" />
                <?elseif ($value['type'] == 'varchar'):?>
                        <input title="<?= $value['desc']?>" name="add_fields[<?=$value['f_id']?>]" value="<?= Arr::get($value,'value_varchar', $_POST[$add_fields_id])?>" type="text" class="" />
                <?elseif ($value['type'] == 'text'):?>
                        <textarea title="<?= $value['desc']?>" name="add_fields[<?=$value['f_id']?>]" rows="7" cols="1" style="width:98%;"><?= Arr::get($value, 'value_text', $_POST['$add_fields_id']);?></textarea>
                <?elseif ($value['type'] == 'boolean'):?>                        
                        <?if(strstr($value['defaults'],'|')):?>
                        <? $options = explode("|",$value['defaults']);?>
                        <? $values = explode("|",$value['value_text']);?>
                            <?foreach($options as $opt):?>
                                <label><?=$opt?><input title="<?= $value['desc']?>" name="add_fields[<?=$value['f_id']?>][]" value="<?=$opt?>"  type="checkbox" <?if( array_search($opt, $values) !== FALSE ):?>checked<?endif;?>/> &nbsp;</label>
                            <?  endforeach;?>
                        <?else:?>
                            <input title="<?= $value['desc']?>" name="add_fields[<?=$value['f_id']?>]" value="1"  type="checkbox" <?if($value['value_int'] || $_POST[$add_fields_id]):?>checked<?endif;?>/>
                        <?endif;?>
                <?elseif($value['type'] == 'select'):?>
                        <select title="<?= $value['desc']?>" name="add_fields[<?=$value['f_id']?>]" class="uncustomized">
                            <? $options = explode("|",$value['defaults']);?>
                            <?foreach($options as $opt):?>
                            <option <?if($opt == $value['value_text']):?>selected="selected"<?endif;?> value="<?= HTML::entities($opt)?>"><?= HTML::entities($opt)?></option>
                            <?  endforeach;?>
                        </select>
                <?endif;?>
            </td>
            <td width="1%">&nbsp;</td>
            <!--<td width="6%"><?= $value['desc']?></td>-->
      <? if( $i % 2 != 0):?></tr><tr><?endif;?>
    <?endif;?>
                    <?$i++;?>
<?endforeach;?>
    </tr>
</table>
</div>