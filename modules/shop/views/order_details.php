<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<table cellspacing="0" width="100%" id="orderDetails" style="border-bottom:none">
    <tr>
        <th width="5%" class="tCenter">N</th>
        <th width="40%"><?php echo I18n::get('scu') ?> / <?= I18n::get('Title') ?></th>
        <th width="9%"><?php echo I18n::get('Quantity') ?></th>
        <th width="15%"><?php echo I18n::get('Price') ?></th>
        <th width="13%"><?php echo I18n::get('Discount') ?>, <?= Kohana::config('shop.default_currency_char')?></th>
        <th width="14%" style="text-align:right"><?php echo I18n::get('Total') ?></th>
        <th width="4%">&nbsp;</th>
    </tr>
    <? $i = 1;?>
    <? foreach($obj['ser_details'] as $key => $value):?>
    <tr>
        <td align="center"><?= $i?></td>
        <td align="left"><?= $value['name']?><br />
		(<?= $value['scu']?>) - <?= $value['addParam']?></td>
        <td align="center" nowrap class="count">
            <input type="text" name="count[<?= $key?>]" value="<?= $value['count']?>" style="width: 2em;" /></td>
        <td class="price"><input style="width:70px; border:none;" type="text" name="price[<?= $key?>]" value="<?= number_format($value['price'],2,'.','')?>" readonly  /></td>
        <td align="right" class="discount">
            <input type="text" name="discount[<?=$key?>]" value="<?= $value['discount']?>" style="width: 2em;" /></td>
        <td align="right" class="sum"><?= number_format($value['sum'], 2,'.',' ')?></td>
        <td class="delete"><a href="#" class="ajax" onclick="if(confirm('Удалить?')) return deleteGood('<?= $key?>'); return false;"><img src="/static/admin/images/del.gif" alt="Delete"/></a></td>
    </tr>
    <? $i++;?>
    <? endforeach;?>
    <tr class="hidden" id="proto">
        <td align="center" class="number">&nbsp;</td>
        <td align="left" class="scu">
			<input style="width:90%" onfocus="if(this.value == '<?php echo I18n::get('Title') ?>') this.value = '';" class="name" type="text" name="new_name[]" value="<?php echo I18n::get('Title') ?>" disabled /><br />
			(<input onfocus="if(this.value == '<?php echo I18n::get('scu') ?>') this.value = '';" style="width:90%" type="text" name="new_scu[]" value="<?php echo I18n::get('scu') ?>" disabled class="scu" />)
			</td>
        <td align="center" class="count"><input type="text" name="new_count[]" value="" style="width: 2em;" disabled /></td>
        <td class="price"><input type="text" name="new_price[]" value="" disabled readonly style="border: 0px none; width:70px" /></td>
        <td align="right" class="discount"><input type="text" name="new_discount[]" value="" style="width: 2em;" disabled /></td>
        <td align="right" class="sum">&nbsp;</td>
        <td class="hide_row"><a href="#"><img src="/static/admin/images/del.gif" alt="Delete"/></a></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="6"><br><a href="#" onclick="return saveDetails();"><?php echo I18n::get('Save Changes') ?></a><br></td>
    </tr>
    <tr id="insertNew">
        <td colspan="6"><a href="#"><?php echo I18n::get('Add another item') ?></a></td>
    </tr>
	</table>
	<table width="100%" id="orderDetails" style="border-bottom:none">
    <tr>
        <td align="right" colspan="6" class="greyitalic"><?php echo I18n::get('In total') ?>:</td>
        <td align="right" style="width:10%"><span id="subtotal"><?= $obj['subtotal']?></span></td>
        
    </tr>
    <tr>
        <td align="right" colspan="6" class="greyitalic"><?php echo I18n::get('Including VAT') ?>:</td>
        <td align="right"><?= $obj['tax']?></td>
        
    </tr>
    <tr>
        <td align="right" colspan="6" class="greyitalic"><?php echo I18n::get('Delivery') ?>:</td>
        <td align="right"><?= $obj['shipment_cost']?></td>
        
    </tr>
    <tr>
        <td align="right" colspan="6" class="greyitalic"><strong><?php echo I18n::get('Altogether') ?>:</strong></td>
        <td align="right"><strong id="totalSum"><?= $obj['sum']?></strong></td>
        
    </tr>
</table>

    <script type="text/javascript" src="/vendor/jquery/jquery.autocomlete.js"></script>
    <link rel="stylesheet" type="text/css" href="/vendor/jquery/autocomplete/autocomplete.css" />
    <script type="text/javascript">

    $(document).ready(function()
    {
        $('td.hide_row img').click(function()
        {
            $("#proto").hide();
            $("#proto input").attr('disabled','disabled');
            $("#insertNew").show();
            return false;
        });

        $("td.count input").bind('change', function(){ reCalc($(this).parents('tr')); })
        $("td.discount input").bind('change', function(){ reCalc($(this).parents('tr')); })

        function reCalc(obj)
        {
            count = parseInt( $(obj).find('td.count input').val() );
            price = parseFloat( $(obj).find('td.price input').val() );
            discount = parseFloat( $(obj).find('td.discount input').val() );

            // alert(count +' * '+ price +' - '+ discount +'='+ (count*(price-discount)));

            $(obj).find('td.sum').text( (count*(price-discount)).toFixed(2) );
            var total = reCalcAll();
            $('#subtotal').text( total );
            $('#totalSum').text( total );
        }

        $('#insertNew a').click(function(){
            $("#proto").show();
            $("#proto input").attr('disabled','');
            $("#insertNew").hide();

            $("#proto td.scu input").autocomplete(
                        '<?= $admin_path?><?= $controller?>/ajax?task=autocomlete&field=scu'
                        ,
                        {
                            matchSubset:1,
                            matchContains:1,
                            cacheLength:10,
                            selectOnly:1,
                            //mode:"multiple",
                            onItemSelect:selectItem,
                            onFindValue:findValue,
                            formatItem:formatItem,
                            multipleSeparator:"|"
                        });


            return false;

        });

        function findValue(li) {

            if( li == null ) return alert("No match!");

            // if coming from an AJAX call, let's use the CityId as the value
            var sValue = 0;

            if( !!li.extra )
                sValue = li.extra[0];

            // otherwise, let's just display the value in the text box
            else
                sValue = li.selectValue;

            if(sValue) {
                $.getJSON('<?= $admin_path?><?= $controller?>/ajax?task=good_name&id='+sValue,
                    function(data)
                    {
                        $("#proto input.name ").val(data.name);
                        $("#proto td.count input").val('1');
                        $("#proto td.price input").val(data.price);
                        $("#proto td.discount input").val('0');
                        $("#proto td.sum").text(data.price);
                    }
                );
            }
        }

        function selectItem(li) {
            findValue(li);
        }

        function formatItem(row) {
            return row[0];
        }

    });
    
    function reCalcAll()
    {
        var total = 0;
        $('#orderDetails tr').each(function()
        {
            var sum = 0;
            if( $(this).find('td.sum').length > 0 )
            {
                count = parseInt( $(this).find('td.count input').val() );
                price = parseFloat( $(this).find('td.price input').val() );
                discount = parseFloat( $(this).find('td.discount input').val() );
                sum = (count*(price-discount));
                if( !isNaN(sum) )
                {
                    total = parseFloat(total) + count*(price-discount);
                }
            }
        });
        return total.toFixed(2);
    }
    </script>