<?php 

$return = " Ваш заказ N{$id} изменил статус на " . I18n::get(Controller_Orders::$order_statuses[Arr::get($_POST,'status')]) . '.'
		."<br>\nДата изменения: ".date('Y-m-d.')
		.(Arr::get($_POST,'comment')?"<br>\nКомментарий: ".nl2br(Arr::get($_POST, 'comment')):'');

$return .= '<table border="1" cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <th>№</th>
                <th>'.i18n::get('Полное наименование товара').'</th>
                <th>'.i18n::get('Ед. изм.').'</th>
                <th>'.i18n::get('Кол-во').'</th>
                <th>'.i18n::get('Цена').'</th>
                <th>'.i18n::get('Сумма').'</th>
            </tr>';

            if($order['ser_details']):
            foreach($order['ser_details'] as $key=>$item)
            {
                $return .= '<tr>
                    <td align="center">'.($key+1).'</td>
                    <td>'.$item['name'].'</td>
                    <td align="center">'.i18n::get('шт.').'</td>
                    <td align="center">'.$item['count'].'</td>
                    <td align="center">'.number_format($item['price'],2,'.','').'<br/></td>
                    <td align="right">'.number_format($item['sum'],2,'.','').'<br/></td>
                </tr>';
            }
            endif;

            if( $order['shipment_cost'])
            {
                $return .= '<tr>
                        <td>&nbsp;</td>
                        <td>'.i18n::get('Доставка').'</td>
                        <td>&nbsp;</td>
                        <td align="center">'.$order['weight'].' кг.<br/></td>
                        <td>&nbsp;</td>
                        <td align="right">'.number_format($order['shipment_cost'],2,'.','').'</td>
                    </tr>';
            }
            if( isset($payment_method['plusToOrder']) )
            {
                $return .= '<tr>
                        <td>&nbsp;</td>
                        <td>'.i18n::get('Способ оплаты').'</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
<!--                        <td align="center">'.$order['weight'].' кг.<br/></td>-->
                        <td>&nbsp;</td>
                        <td align="right">'.number_format($order['payment_cost'],2,'.','').'</td>
                    </tr>';
            }
            if( $discount)
            {
                $return .= '<tr>
                        <td>&nbsp;</td>
                        <td>'.i18n::get('Скидка').'</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
<!--                        <td align="center">'.$order['weight'].' кг.<br/></td>-->
                        <td>&nbsp;</td>
                        <td align="right">'.number_format($discount,2,'.','').'</td>
                    </tr>';
            }
            $return .= '<tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right"><b>'.i18n::get('Всего на сумму').':</b></td>
                    <td align="right">'.number_format($order['sum'],2,'.','').'</td>
                </tr>

            </table>';
            
return $return;
?>
