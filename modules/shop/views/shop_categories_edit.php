<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:27:18 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/static/admin/js/settings.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.serializeanything.js"></script>
<link rel="stylesheet" type="text/css" href="/static/admin/css/style-ie.css"/>
<!--floating block-->


<form method="post" action="" enctype="multipart/form-data">
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">

	<input type="text" name="actTab" id="actTab" value="common" />
	<div class="absBlocks floatingInner">
		<div class="padding20px">
			<input type="hidden" name="list_action" id="list_action" value=""/>
			<table width="100%">
				<tr>
					<td style="width:37%">
						<button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
							<span><span><?= I18n::get('Save and exit') ?></span></span></button>
                                                <? if ( $id <> null ) :?>
						<button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">

							<span><span><?= I18n::get('Apply') ?></span></span></button>
                                                <? endif;?>
					</td>

					<td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?= $admin_path?>goods';">
							<span><span><?= I18n::get('Cancel') ?></span></span></button>
					</td>
				</tr>
			</table>
		</div>
		<div class="absBlocks floatingPlaCorner L"></div>
		<div class="absBlocks floatingPlaCorner R"></div>
	</div>
	<div class="absBlocks side L"></div>
	<div class="absBlocks side R"></div>
</div>

    <!--floating block-->

    <div class="rel" >

        <div class="whiteblueBg padding20px">
            <ul class="tabs">
                <li class="active" id="common"><a href="#">
                        <span class="linkSide L"></span>
                        <span class="linkSide C"><?= I18n::get('Main') ?></span>
                        <span class="linkSide R"></span>
                    </a>
                </li>

                <li class="divider">&nbsp;</li>
                <li class=""  id="cat_optionts">
                    <a href="#">
                        <span class="linkSide L"></span>
                        <span class="linkSide C"><?= I18n::get('Options2'); ?></span>
                        <span class="linkSide R"></span>
                    </a>
                </li>
				<li class="divider">&nbsp;</li>
                    <li class="" id="recommended">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Recommended goods'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>

            </ul>
            <div class="clear"></div>
            <br />

			<? if ($errors): ?><div class="errors"><? foreach ($errors as $message) echo $message; ?></div><? endif; ?>
        </div>
        <div class="whiteblueBg">
            <!-- **** -->
            <div class="tabContent common openedTab  padding20px"><? include 'shop_main_cat.php'; ?></div>
            <div class="tabContent cat_optionts  "><? include 'shop_main_options.php'; ?></div>
            <div class="tabContent recommended  "><? include 'shop_cat_recgoods.php'; ?></div>

            <!-- **** -->
        </div>
</form>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>

</div>
