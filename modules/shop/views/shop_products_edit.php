<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:27:18 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/static/admin/js/settings.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.serializeanything.js"></script>

<!--floating block-->

<form method="post" action="" enctype="multipart/form-data">
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
   
    <input type="text" name="actTab" id="actTab" value="common" />
    <div class="absBlocks floatingInner">
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td style="width:37%">
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                        <span><span><?= I18n::get('Save and exit') ?></span></span></button>&nbsp;&nbsp;
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">

                                        <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="absBlocks floatingPlaCorner L"></div>
        <div class="absBlocks floatingPlaCorner R"></div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

        <!--floating block-->

        <div class="rel" >

            <div class="whiteblueBg padding20px">
                <ul class="tabs">
                    <li class="active" id="main"><a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Main')?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="prices">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Price'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <?if ($obj['id']):?>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="photos">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Photos'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <?endif;?>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="seo">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('SEO'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <?if($obj['id']):?>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="options">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Options2'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="recommended">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Recommended goods'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="goodgroups">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Good Groups'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <?endif;?>
                </ul>
                <div class="clear"></div>
                <br />
    </div>
    <div class="whiteblueBg">
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
        <!-- **** -->
        <div id="" class="tabContent main openedTab "><? include 'product_main.php'; ?></div>
        <div id="" class="tabContent prices  "><? include 'product_price.php'; ?></div>
        <div id="" class="tabContent photos "><? include 'product_photo.php'; ?></div>
        <div id="" class="tabContent seo "><? include 'product_seo.php'; ?></div>
        <?if($obj['id']):?><div id="" class="tabContent options "><? include 'product_options.php'; ?></div>
        <div id="" class="tabContent recommended "><? include 'product_recgoods.php'; ?></div>
        <div id="" class="tabContent goodgroups "><? include 'product_goodgroups.php'; ?></div><?endif;?>
        
        <!-- **** -->
    </div>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>

</div>
</form>
