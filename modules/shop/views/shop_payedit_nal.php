<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:27:18 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/static/admin/js/settings.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.serializeanything.js"></script>

<!--floating block-->

<form method="post" action="">
    <input type="hidden" name="fs_name" value="<?= isset($fs_name)?$fs_name:$obj['fs_name']?>" />
<input type="hidden" name="settings_id" value="<?= $obj['settings_id']?>" />
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
   
    <input type="text" name="actTab" id="actTab" value="common" />
    <div class="absBlocks floatingInner">
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td style="width:37%">
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                        <span><span><?= I18n::get('Save and exit') ?></span></span></button>
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">

                                        <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="absBlocks floatingPlaCorner L"></div>
        <div class="absBlocks floatingPlaCorner R"></div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

        <!--floating block-->

        <div class="rel" >

    <div class="whiteblueBg padding20px">
        <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
        <? include MODPATH . ADMIN_PATH . '/views/system/wysiwyg.php'; ?>
        <table style="border-bottom: 0px;"width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab">
            <tr>
                <td colspan="3">
                    <span style="padding-bottom: 7px; display: block; padding-top: 3px;"><span style="float: left;font-style: italic;display: block;"><?= I18n::get('Title') ?>:</span></span>
                </td>
            </tr>
            <tr>
                <td width="45%">
                    <input type="text" style="float: left;display: block;width:100%;" value="<?=$obj['name']?>" rel="" name="name"  ></td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td width="45%">
                    <div style="float: left;">
                       <label><input type="checkbox" name="include_tax" <?if(Model_Content::arrGet( $obj, 'include_tax', $_POST['include_tax'])):?>checked="checked"<?endif;?> value="1"/>
                            <span style="font-style: italic;padding-left:5px;"> <? echo I18n::get('Calculate taxes'); ?></span>
                       </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td width="45%">
                    <div class="tLeft">
                        <label><input type="checkbox" name="ordersendmail" <?if(Model_Content::arrGet( $obj, 'ordersendmail', $_POST['ordersendmail'])):?>checked="checked"<?endif;?> value="1"/>
                              <? echo I18n::get('Send to emeil copy of the invoice'); ?></label>
                      </div>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td width="45%">
                    <div class="tLeft"><label><input type="checkbox" name="letter_description" <?if(Model_Content::arrGet( $obj, 'letter_description', $_POST['ordersendmail']) == 1 ):?>checked="checked"<?endif;?> value="1"/>
                              <? echo I18n::get('Include instructions to the email'); ?></label></div>
                </td>
            </tr>
            <tr>
                <td width="45%">
                    <div class="tLeft">
                        <input type="text" name="orderid" value="<?= Model_Content::arrGet( $obj, 'orderid', Model_Content::arrGet($_POST,'orderid',9998))?>" style="width:28px;text-align: center;"/>
                              <? echo I18n::get('Sort order'); ?>
                      </div>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td width="45%"></td>
            </tr>
            <tr>
                <td width="45%">
                    <div class="flLeft padchek">
                          <? echo I18n::get('Price rate'); ?>
                          <?= Form::select('rateMethod', array('rate' => 'Наличный','rate_w_vat' => 'Безналичный' ), Model_content::arrGet($obj, 'rateMethod', Model_Content::arrGet($_POST,'rateMethod',1)) )?>
                      </div>
                </td>
                <td width="10%" style="padding:0 42px 0 0;">
                    <span style="display: block; height: 22px; padding-left: 0px; border-right: 1px dotted rgb(0, 0, 0); padding-right: 0px;"></span>
                </td>
                <td>
                    <div class="flLeft padchek">&nbsp;</div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                      <br />
                      <div class="tLeft "><? echo I18n::get('Payment is available for the following delivery methods'); ?></div>
                      <? foreach($shipments as $item):?>
                      <div style="padding: 5px;" class="tLeft padchek">
                          <label><input type="checkbox" name="forShipment[<?=$item['id']?>]" <? if(Arr::binary_search( $item['id'], $obj['forShipment']) !== FALSE || !$obj['name']):?>checked="checked"<?endif;?> value="<?=$item['id']?>"/><?=$item['name']?></label><br/>
                      </div>
                      <?endforeach;?>
                </td>
            </tr>
        </table>
        <div style="padding: 5px; font-style:italic; color:#54595F">
          <? echo I18n::get('Instruction'); ?>:<br /><br />
          <textarea class="jquery_ckeditor" id="ckeditor" name="text" rows="7" style="width:100%;" cols="7"><?= Model_Content::arrGet($obj, 'text', $_POST['text'])?></textarea>
        </div>
        <div style="padding: 5px;font-style:italic; color:#54595F">
           <? echo I18n::get('Supplier'); ?>:<br /><br />
           <textarea name="postavshik" rows="7" style="width:100%;" cols="7"><?= htmlentities(Arr::get($obj, 'postavshik', $_POST['postavshik']),ENT_COMPAT,'UTF-8')?></textarea>
        </div>
        <div class="clear"></div>
        <br />
    </div>
   
</form>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>

</div>
