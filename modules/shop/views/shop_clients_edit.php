<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:27:18 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/static/admin/js/settings.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.serializeanything.js"></script>


<!--floating block-->


<!---->

<style TYPE="text/css">
  .td-settings{
  vertical-align: top;
}
</style>




<!---->
       
<form method="post" action="" enctype="multipart/form-data">

<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
   
    <input type="text" name="actTab" id="actTab" value="common" />
    <div class="absBlocks floatingInner">
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td style="width:37%">
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                        <span><span><?= I18n::get('Save and exit') ?></span></span></button>&nbsp;&nbsp;
                                    <button  class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/apply">

                                        <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="absBlocks floatingPlaCorner L"></div>
        <div class="absBlocks floatingPlaCorner R"></div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

        <!--floating block-->

        <div class="rel" >
<style type="text/css">
.settings-clients
    {
        text-align: left;
    }
    .settings-clients > span  {
        display: block;
        color: #808080;
        font-style: italic;
    }
    .settings-clients > input {
        display: block;
        margin-top: 7px;
        width: 91%;
}
.settings-clients div span{
    color: #808080;
    font-style: italic;

}
.settings-clients .title-settings{
    color:#3F3F3F;
    font-size:12px;
    font-style:normal;
    font-weight:bold;
    margin-top:15px;
}

</style>
    <div class="whiteblueBg padding20px" style="height: auto;">
        <table style="border-bottom: 0px;"width="100%" cellpadding="0" cellspacing="0" class="shop sortableContentTab">
            <tr>

                 <td width="50%" class="td-settings">

                   <div class="settings-clients">
                       <span class="title-settings"><? echo I18n::get('Personal Information'); ?></span><br>
                       <span><? echo I18n::get('name'); ?>:</span>
                       <input type="text" value="<?=$obj['name']?>" name="name"><br>
                       <span>E-mail:</span>
                       <input type="text" value="<?=$obj['email']?>" name="email"><br>
<!--                       <span><? echo I18n::get('Phone'); ?>:</span>-->
<!--                       <input type="text" value="<?=$obj['phone']?>" name="phone"><br>-->
						<?if($customer_shipment_addresses):?>
                       <span style="float: left;padding-top:5px;"><? echo I18n::get('Delivery Address'); ?>:</span>
                       <?//=  Kohana::debug($obj,$addr) ?>
                       <div style="margin-left: 112px; margin-top: 5px;color: #000;">
                       <?$last_key = end(array_keys($customer_shipment_addresses));?>
                       <? foreach($customer_shipment_addresses as $key => $value):?>
                       <div style="">
                           <? //$text = implode(', ', $value);?>
                           <?= $value['phone']?>
                           <?=$value['country'].','.'г.'.$value['city'].',<br>'.'ул.'.$value['address_line1'].'<br>'?>
                           <? if($key != $last_key) echo '-';?>
                       </div>
                       <? endforeach;?>
                       </div>
                       <? endif;?>
                   </div>
                </td>
                <td width="50%" class="td-settings">
                    <div class="settings-clients" style="padding-left: 37px;border-left: 1px dotted #919598">
                        <span class="title-settings"><? echo I18n::get('Options'); ?></span><br>
                        
                        <span><? echo I18n::get('Discount'); ?></span>
                        <input type="text" name="discount" value="<?=$obj['discount']?>" style="height: 17px;width: 45px;float: left"><span style="    margin-left:61px;
    margin-top:10px;">%</span>
                        <div class="clear"></div>
                        <div style="    border-top:1px dotted #919598;
    margin-top:17px;
    padding-top:15px;
    width:158px;">
                            <input style="margin-right: 7px;" type="checkbox" value="1" name="is_taxpayer" <? if ($obj['is_taxpayer']==1)echo 'checked'?>><span><? echo I18n::get('Tax Payer'); ?></span><br><br>
                            <input style="margin-right: 7px;" type="checkbox" value="1" name="news_subscribe" <? if ($obj['news_subscribe']==1)echo 'checked'?>><span><? echo I18n::get('Signed on news'); ?></span>
                        </div>

                    </div>
                </td>
            </tr>

        </table>
                <div class="clear"></div>
                <br />

        
    </div>
   
</form>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>

</div>
<? //die(Kohana::debug($obj))?>