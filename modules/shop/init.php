<?php defined('SYSPATH') or die('No direct script access.');

Route::set
        (
	    'shop',
	    'admin/shop(/<controller>)((/<id>)/<action>)(/<path>)',
	    array( 'id' => '\d{1,10}', 'controller'    => '(?:shop|categories|goods|customers|orders|currencies|paymethods|shipmethods|brands|shopmail|recgoods)', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'shop',
		    'id'	    => NULL,
		    'action'        => 'index',
                )
	)
     
;

// Frontned Routes
//
// //
// vote
Route::set
        (
	    'shop_vote_front',
	    '(<lang>/)shop/vote',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_goods',
		    'action'        => 'vote',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;
// 
// front brands
Route::set
        (
	    'shop_brands_front',
	    '(<lang>/)shop/brand/<brandId>',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_brands',
		    'action'        => 'goods',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;
//
// search
Route::set
        (
	    'shop_search_front',
	    '(<lang>/)shop/search',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_categories',
		    'action'        => 'search',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

// Categories
Route::set
        (
	    'shop_categories_front',
	    '(<lang>/)shop/(<name>)_<id>(/<action>)',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'name' => '.+'
		    , 'id' => '\d{1,10}'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_categories',
		    'action'        => 'index',
		    'name'	    => NULL,
		    'id'	    => NULL,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

// Goods
Route::set
        (
	    'shop_goods_front',
	    '(<lang>/)shop(/<category_name>/)(<good_name>)_<id>.html(/<action>)', //(<action>)
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'category_name' => '.+'
		    , 'good_name' => '.+'
		    , 'id' => '\d{1,10}'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_goods',
		    'action'        => 'index',
		    'category_name' => NULL,
		    'good_name'     => NULL,
		    'id'	    => NULL,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;
// Shop mail
Route::set
        (
	    'shopmail_front',
	    '(<lang>/)shop/mail/<code>', //(<action>)
            array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
                )
        )
        ->defaults
        (
            array(
                    'lang' => 'ru',
                    'controller' => 'frontend_shopmail',
		    'action' => 'un'
                )
	)
;

// Cart
Route::set
        (
	    'shop_cart_front',
	    '(<lang>/)shop/cart(/<id>(/<action>))',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'id' => '\d+'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_cart',
		    'action'        => 'index',
		    'id'	    => NULL,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

// order
Route::set
        (
	    'shop_order_front',
	    '(<lang>/)shop/order((/<id>)/<action>)',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'id' => '\d{1,10}'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_order',
		    'action'        => 'index',
		    'id'	    => NULL,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

// paymethods
Route::set
        (
	    'shop_paymethods_front',
	    '(<lang>/)shop/paymethods(/<action>)',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'		    
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_paymethods',
		    'action'        => 'index',
		    'id'	    => NULL,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

if ( strpos($_SERVER["PATH_INFO"], ADMIN_PATH) !== 0){
//    Event::add('system.routing', array('Helper_Brands', 'init'));
//    Event::add('system.routing', array('Model_Frontend_Categories', 'init'));
    Event::add('system.routing', array('Model_Frontend_Cart', 'init'));
    Event::add('system.routing', array('Model_Currencies', 'init'));
}