<?php defined('SYSPATH') or die('No direct script access.');
// cURL base set of options. These are the most common, for more see http://www.php.net/manual/en/function.curl-setopt.php

return array(
	'options' => array
				(
					CURLOPT_FAILONERROR        => TRUE,
					CURLOPT_FOLLOWLOCATION     => TRUE,
					CURLOPT_RETURNTRANSFER     => TRUE,
					CURLOPT_TIMEOUT            => 30,
					CURLOPT_FRESH_CONNECT      => TRUE,
					CURLOPT_FORBID_REUSE       => TRUE,
					CURLOPT_POST               => TRUE,
					CURLOPT_URL                => 'http://chimeracms.com',
					CURLOPT_SSL_VERIFYPEER     => FALSE,
					CURLOPT_SSL_VERIFYHOST     => FALSE,
					CURLOPT_AUTOREFERER        => TRUE
				),
	// Allow caching of results. FALSE for no cache, else number of seconds for cache to live
	'cache' => FALSE,
	// The cache tag(s) to use
	'cache_tags' => array('mod_Curl'),
	// The hash method (for caching)
	'hash' => 'sha1',
	// The Curl error codes to ignore
	'ignored_errors' => array(),
);