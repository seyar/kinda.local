<?php defined('SYSPATH') or die('No direct script access.');

Route::set
        (
	    'photogallery',
	    'admin/photogallery((/<id>)/<action>)',
	    array( 'id' => '\d{1,10}', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'photogallery',
		    'id'	    	=> NULL,
		    'action'        => 'index',
                )
	)
;

// Frontend Routes
Route::set
        (
	    'photogallery_front',
	    '(<lang>/)photogallery(/<id>)',
	    array( 'lang' => '(?:'.LANGUAGE_ROUTE.')', 'id' => '\d{1,10}', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_photogallery',
		    'action'        => 'list',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

if ( strpos($_SERVER["PATH_INFO"], ADMIN_PATH) !== 0)
    Event::add('system.routing', array('Model_Frontend_Photogallery', 'init'));