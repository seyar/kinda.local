<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	'lang'		=> 'ru', // Default the  language.
	'path'		=> dirname(__FILE__) . '/../views/', // Admin templates folder.

    'watermark' => '/../../media/watermark.png',

    'image_width' => 980,
    'image_height' => 800,
    'image_prev_width' => 308,
    'image_prev_height' => 205,
);
