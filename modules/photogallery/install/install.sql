DROP TABLE IF EXISTS `photogallery_albums`;
CREATE TABLE IF NOT EXISTS `photogallery_albums` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `language_id` tinyint(4) NOT NULL default '1',
  `order_by` int(11) NOT NULL default '9998',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `photogallery_photos`;
CREATE TABLE IF NOT EXISTS `photogallery_photos` (
  `id` int(11) NOT NULL auto_increment,
  `album_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `album_id` (`album_id`),
  CONSTRAINT `photogallery_photos_ibfk_3` FOREIGN KEY (`album_id`) REFERENCES `photogallery_albums` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
