<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
            'name'             => 'Photo Gallery',
            'fs_name'          => 'photogallery',
            'panel_serialized' => NULL,
            'useDatabase'      => true
        );