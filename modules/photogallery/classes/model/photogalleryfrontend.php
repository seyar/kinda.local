<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 19.02.2010 - 18:24:43 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        photogalleryfrontend.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
class Model_PhotogalleryFrontend extends Model {

    static public function init()
    {
        $query = DB::select('pp.*', array('pa.id', 'albumId'))
		->from(array('photogallery_photos','pp'))
		->order_by('id','desc')
                ->join(array('photogallery_albums','pa'),'left')
                ->on('pp.album_id','=','pa.id')
                ->limit(15)
//		->cached(60)
		;

        $result = $query->execute()->as_array('id');
        Kohana_Controller_Quicky::$intermediate_vars['top_photos'] = $result;
    }


    function getPhotos( $album = 0, $lang_id = 1)
    {
        $query = DB::select('pp.*', array('pa.title', 'albumTitle'), array('pa.order_by', 'album_order_by') )
            ->from(array('photogallery_photos','pp'))
            ->join(array('photogallery_albums','pa'),'left')
                ->on('pp.album_id','=','pa.id')
            ->order_by('pa.order_by','asc')
            ->where('language_id','=',$lang_id)
            ;

        if( $album )
            $query->and_where('album_id','=',$album);

        $result = $query->execute(); //->cached(10)

        if ($result->count() == 0) return array();
        else $photos = $result->as_array();

        return $photos;
    }
}
?>