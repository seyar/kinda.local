<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Photogallery extends Model
{

    static  public function getAlbums($id = 0, $lang_id = 1)
    {
        $query = DB::select()
                        ->from('photogallery_albums')
//                        ->where('language_id', '=', $lang_id)
                        ->order_by('order_by', 'asc')
        ;

        if (isset($id) && !empty($id) )
            $query->and_where('id', '=', $id);

        $result = $query->execute(); //->cached(10)

        if ($result->count() == 0)
            return array();
        else
            return $result->as_array('id');
    }

    function getPhotos($album = 0, $lang_id = 1)
    {
        $query = DB::select('pp.*', array('pa.title', 'albumTitle'))
                        ->from(array('photogallery_photos', 'pp'))
                        ->join(array('photogallery_albums', 'pa'), 'left')
                        ->on('pp.album_id', '=', 'pa.id')
                        ->order_by('order_by', 'asc')
                        ->order_by('id', 'asc')
//                        ->where('language_id', '=', $lang_id)
        ;

        if ($album)
            $query->and_where('album_id', '=', $album);

        $result = $query->execute(); //->cached(10)

        if ($result->count() == 0)
            return array('files'=>array());
        else
            $photos = $result->as_array();

//        if( $album == 0 )
//        {
        $return = array();
        foreach ($photos as $item)
        {
            $return[$item['albumTitle']][] = $item;
        }

        
//        }

        return $return;
    }

    public function publ_images_upload($folder, $album_id = 1, $hash='', $width = NULL, $height = NULL, $t_width = NULL, $t_height = NULL, $equalSize = FALSE, $watermark = NULL)
    {
        if (!empty($_FILES))
        {
            $hash = $hash ? $hash : uniqid() . '.jpg';
            $album_id = $album_id == 0 ? 1 : $album_id;

            $tempFile = $_FILES['Filedata']['tmp_name'];
//            $targetPath = MEDIAPATH . "$folder/" . $album_id.'/';
            $targetPath = MEDIAPATH . "$folder/";
            $targetFile = str_replace('//', '/', $targetPath) . $_FILES['Filedata']['name'];

            if (!file_exists( $targetPath ))
                @mkdir(str_replace('//', '/', $targetPath), 0755, true);

            move_uploaded_file($tempFile, $targetPath . $hash);

            // Generate Thumbnail
            if ($t_width || $t_height)
            {
                $this->image = Image::factory($targetPath . $hash);

                $this->image->resize($t_width, $t_height, Image::AUTO)
                        ->crop($t_width, $t_height);

                $this->image->save($targetPath . '/prev_' . $hash, 90);
            }
            // Resize Image
            if ($width || $height)
            {
                $this->image = Image::factory($targetPath . $hash);
                if( $this->image->width > $width  || $this->image->height > $height )
                {
                    $this->image->resize($width, $height, Image::AUTO);
                    if( isset($equalSize) )
                        $this->image->crop($width, $height);

                    if($watermark !== null)
                        $this->image->watermark(Image::factory( $watermark ), true, true);
                    
                    $this->image->save($targetPath . $hash, 90);
                    
                }else{
                    if($watermark !== null)
                        $this->image->watermark(Image::factory( $watermark ), true, true);
                }
            }


            echo "1";
            return array('filename' => $hash, 'album_id' => $album_id);
        }
        die();
    }

    /**/

    public function publ_images_uploadWd($folder, $id = 1, $album_id = 1, $hash='')
    {
        KOhana_log::instance()->add('d',Kohana::debug($folder, $id , $album_id , $hash));
        // поменять имя. собрать все имена в массив.
        if (!empty($_FILES))
        {
            $album_id = $album_id == 0 ? $album_id = 1 : $album_id;

            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = MEDIAPATH . "$folder/$id/";
            $targetFile = str_replace('//', '/', $targetPath) . $_FILES['Filedata']['name'];

            if (!file_exists(MEDIAPATH . "$folder/$id"))
                @mkdir(str_replace('//', '/', $targetPath), 0755, true);

            $hash = $hash ? $hash : uniqid() . '.jpg';
            move_uploaded_file($tempFile, $targetPath . $hash);

            return array('filename' => $hash, 'objects_id' => $id, 'album_id' => $album_id);
        }
    }

    public function publ_images_check($folder, $id = 1)
    {
        $fileArray = array();

        foreach ($_POST as $key => $value)
        {
            if ($key != 'folder' && $key != '_')
            {
                if (file_exists(MEDIAPATH . "$folder/$id/$value"))
                {
                    $fileArray[$key] = $value;
                }
            }
        }

        echo json_encode($fileArray);
        die();
    }

    public function saveImagesToDB($data)
    {
        $keys = array();
        $ins = array();
        foreach ($data as $key => $item)
        {
            $keys[] = $key;
            $ins[] = $item;
        }
        $keys[] = 'date';
        $ins[] = DB::expr('NOW()');
        
        $query = DB::insert('photogallery_photos',
                                $keys
                        )
                        ->values($ins)
//                        ->execute()
                ;
        Kohana_log::instance()->add('$query->__toString()', Kohana::debug($query->__toString()));
        $query->execute();

        return 1;
    }

    public function image_delete($folder, $id)
    {
        $query = DB::select()
                        ->from('photogallery_photos')
                        ->where('id', '=', $id);

        $result = $query->execute();

        if ($result->count() != 0)
        {
            $image = $result->current();

            @unlink(str_replace('//', '/', MEDIAPATH . '/' . $folder . '/' . $image['filename']));
            @unlink(str_replace('//', '/', MEDIAPATH . '/' . $folder . '/prev_' . $image['filename']));

            $query = DB::delete('photogallery_photos')
                            ->where('id', '=', $id)
                            ->execute()
            ;
            return 1;
        }else
            return false;
    }

    public function editPhotos($data, $id)
    {
        $query = DB::update('photogallery_photos')
                        ->set(
                                $data
                        )
                        ->where('id', '=', $id)
                        ->execute();
        return 1;
    }

    /**/

    public function updatePhoto()
    {
        $res = $this->editPhotos(array('comment' => $_REQUEST['titlePopup'], 'album_id' => $_REQUEST['albumPopup']), $_REQUEST['popupPhotoID']);
        return $res;
    }

    public function moveLeft()
    {
        $query = DB::select()
                        ->from(array('photogallery_photos', 'cnp'))
                        ->where('cnp.id', '=', $_GET['prevID'])
        ;

        $result = $query->execute(); //->cached(10)

        if ($result->count() == 0)
            return false;
        else
            $prev = $result->current();

        DB::delete('photogallery_photos')->where('id', '=', $_GET['prevID'])->execute();
        DB::update('photogallery_photos')->set(array('id' => $_GET['prevID']))->where('id', '=', $_GET['curID'])->execute();

        $keys = array();
        $ins = array();
        foreach ($prev as $key => $item)
        {
            $keys[] = $key;
            if ($key == 'id')
                $ins[] = $_GET['curID'];
            else
                $ins[] = $item;
        }

        DB::insert('photogallery_photos', $keys)->values($ins)->execute();

        return 1;
    }

    public function moveRight()
    {
        $query = DB::select()
                        ->from(array('photogallery_photos', 'cnp'))
                        ->where('cnp.id', '=', $_GET['prevID'])
        ;

        $result = $query->execute(); //->cached(10)

        if ($result->count() == 0)
            return false;
        else
            $prev = $result->current();

        DB::delete('photogallery_photos')->where('id', '=', $_GET['prevID'])->execute();
        DB::update('photogallery_photos')->set(array('id' => $_GET['prevID']))->where('id', '=', $_GET['curID'])->execute();

        $keys = array();
        $ins = array();
        foreach ($prev as $key => $item)
        {
            $keys[] = $key;
            if ($key == 'id')
                $ins[] = $_GET['curID'];
            else
                $ins[] = $item;
        }

        DB::insert('photogallery_photos', $keys)->values($ins)->execute();

        return 1;
    }

    public function deleteAlbum($album_id)
    {
        $albTitle = $this->getAlbums($album_id);
        $alb = $this->getPhotos($album_id);

        foreach ($alb as $photo)
        {
            foreach ($photo as $item)
                @unlink(MEDIAPATH . '/photogallery/' . $item['filename']);
            @unlink(MEDIAPATH . '/photogallery/prev_' . $item['filename']);
        }

        DB::delete('photogallery_photos')->where('album_id', '=', $album_id)->execute();
        DB::delete('photogallery_albums')->where('id', '=', $album_id)->execute();
        return 1;
    }

    public function add_album($id, $lang_id = 1)
    {
        if ($id)
        {
            DB::update('photogallery_albums')
                    ->set(array('title' => $_POST['title'], 'order_by' => $_POST['order_by']))
                    ->where('id', '=', $id)
                    ->execute();
        }
        else
        {
            DB::insert('photogallery_albums', array('title', 'language_id', 'order_by'))
                    ->values(array($_POST['title'], $lang_id, $_POST['order_by']))
                    ->execute();
            return 1;
        }
    }

    function getSearch($queryOrig)
    {
//        $table = array('pa.title', 'pp.comment');
        $table = array('comment');
        $query = explode(' ', $queryOrig);

        $found = array();

        $str = "SELECT * FROM photogallery_photos LEFT JOIN photogallery_albums ON photogallery_albums.id = photogallery_photos.album_id WHERE ";
        $like = '';
        foreach ($table as $item)
        {
            foreach ($query as $word)
            {
                $like .= " OR $item LIKE '%".mysql_escape_string($word)."%' ";
            }
        }

        $select = $str . ($like ? "0 $like" : '1');        
        $queryDB = DB::query(Database::SELECT, $select.' ORDER BY date DESC');
        $result = $queryDB->execute();

        if ($result->count() == 0)
            return array();

        $found = $result->as_array();
        $return = array();
        foreach ($found as $field)
        {
            $text = '';
            foreach ($table as $item)
            {                
                foreach($query as $word)
                {
                    if( strstr(mb_strtolower(strip_tags($field[$item]), 'UTF-8'), $word) ){ $text .= $field[$item].' '; break; }
                }
                
            }
            $return[] = array(
                'text' => $text,
                'url' => '/photogallery/' . $field['id'],
                'date' => $field['date'],
                'album_id' => $field['album_id'],
                'filename' => $field['filename'],
                );
        }
        
        return $return;
    }

    static function get_sitemap($all)
    {

        $url = Route::get('photogallery_front')->uri();
        return array(array(
                'url' => $url,
                'depth' => 0,
                'title_page' => I18n::get('Photogallery')
        ));
    }

    static public function sitemap()
    {
        $res = DB::select(
                                'id',
                                array(DB::expr('0'), 'parent_id'),
                                array('title', 'name'),
                                array(DB::expr("CONCAT('/photogallery', '/', id)"), 'page_url')
                        )
                        ->from('photogallery_albums')
                        ->execute()
                        ->as_array('id');

        $res = array_merge(array(0 => array(
                        "id" => 0,
                        "parent_id" => -1,
                        "name" => "Фотогалерея",
                        "page_url" => "/photogallery"
                    )
                        )
                        , $res
        );
//echo Kohana::debug($res);die();
        return $res;
    }

    public function saveOrder( $data = array() )
    {
        if( count( $data) > 0 )
        {
            foreach($data as $key => $item)
            {
                DB::update('photogallery_photos')
                    ->set(array('order_by'=>$key))
                    ->where('id','=',$item)
                    ->execute();
            }
        }
        return 1;
    }
}
