<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 19.02.2010 - 18:14:09 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        photogalleryfrontend.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
class Controller_PhotogalleryFrontend extends Controller_Content {
    public $template	= 'photogallery.tpl';

    public function action_list()
    {
        $albums = Model_Photogallery::getAlbums(NULL, CURRENT_LANG_ID);
        $photos = Model_PhotogalleryFrontend::getPhotos( $this->request->param('id'), CURRENT_LANG_ID );
        $this->view->assign('photos', $photos);
        $this->view->assign('albums', $albums);
        $this->template = 'photogallery.tpl';

        $this->page_info = array( 'page_title'=>I18n::get('Documents'),'page_name'=>I18n::get('Documents') );
    }
}
?>