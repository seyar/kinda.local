<?php

defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 09.02.2010 - 10:47:44 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        newbuildings.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_photogallery extends Controller_AdminModule
{

    public $template = 'photogallery';
    public $module_title = 'Photogallery';
    public $module_name = 'photogallery';
    public $module_desc_short = 'Photogallery Descr Short';
    public $module_desc_full = 'Photogallery Descr Full';
    public $images_folder = 'photogallery';
    public $image_width = 0;
    public $image_height = 0;
    public $image_prev_width = 0;
    public $image_prev_height = 0;

    public function before()
    {
        parent::before();
        $this->model = new Model_Photogallery();

        $this->image_width = Kohana::config('photogallery')->image_width;
        $this->image_height = Kohana::config('photogallery')->image_height;
        $this->image_prev_width = Kohana::config('photogallery')->image_prev_width;
        $this->image_prev_height = Kohana::config('photogallery')->image_prev_height;
    }

    public function action_index()
    {
        $this->template->albums = $this->model->getAlbums(NULL, $this->lang); // альбомы для фотографий. стадии
        $this->template->images_folder = $this->images_folder;
    }

    public function action_image_preview()
    {
        $this->show_image_preview(MEDIAPATH . $this->images_folder . '/' . $this->request->param('id') . '/' . basename($_GET['name'])
                , $this->image_prev_width, $this->image_prev_height);
    }

    /* image upload */

    public function action_images_upload()
    {
        if( Kohana::config('photogallery.watermark') ) $watermark = dirname(__FILE__).Kohana::config('photogallery')->watermark;

        $data = $this->model->publ_images_upload($this->images_folder, $_POST['album_id'], NULL, $this->image_width, $this->image_height, $this->image_prev_width, $this->image_prev_height, FALSE, $watermark );
        //die(Kohana::debug($data));
        $this->model->saveImagesToDB($data);
    }

    public function action_images_check()
    {
        $this->model->publ_images_check($this->images_folder, $this->request->param('id'));
    }

    public function action_images_show()
    {
        echo json_encode(
                $this->model->getPhotos((isset($_GET['album_id'])?$_GET['album_id']:NULL), $this->lang)
        );
        die();
    }

    public function action_images_default()
    {
        die(print($this->model->image_default($this->request->param('id'), $_GET['photo_id'])));
    }

    public function action_images_delete()
    {
        die(print($this->model->image_delete($this->images_folder, $_GET['photo_id'])));
    }

    /**/

    public function action_updatePhoto()
    {

        $this->model->publ_images_uploadWd($this->images_folder, $this->request->param('id'), $_REQUEST['albumPopup'], $_POST['filename']);
        $this->model->updatePhoto($this->request->param('id'));
        $this->redirect_to_controller($this->request->controller . '/' . ($_POST['actTab'] ? '/#' . $_POST['actTab'] : ''));
    }

    public function action_updatePhotoAjax()
    {
        $res = $this->model->updatePhoto($this->request->param('id'));
        die(print($res));
    }

    public function action_saveOrder()
    {
        die(print( $this->model->saveOrder($_GET['photos'])));
    }
//    public function action_moveLeft()
//    {
//        die(print( $this->model->moveLeft($this->request->param('id'))));
//    }
//
//    public function action_moveRight()
//    {
//        die(print( $this->model->moveRight($this->request->param('id'))));
//    }

    public function action_edit_albums()
    {
        $this->template = View::factory('edit_albums');

        $this->template->albums = $this->model->getAlbums(NULL, $this->lang);
    }

    public function action_delete_album()
    {
        die(print($this->model->deleteAlbum($this->request->param('id'))));
    }

    public function action_add_album()
    {
        $this->model->add_album( (isset($_POST['albumID'])?$_POST['albumID']:NULL), $this->lang);
        $this->redirect_to_controller(str_replace('//', '/', $this->request->controller . '/edit_albums'));
    }

    public function action_get_album()
    {
        $res = $this->model->getAlbums($_GET['id'], $this->lang);
        die(json_encode($res['0']));
    }

    static function get_sitemap($all = false)
    {
        return Model_Photogallery::get_sitemap($all);
    }

}
