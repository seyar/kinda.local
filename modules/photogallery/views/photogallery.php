<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 18.12.2010 - 13:58:15 Exp $
 *
 * Project:     kontext
 * File:        photogallery.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<? $_GET['postInit'] = 1; include 'js.php'; ?>
<!--floating block-->
<form method="post" action="" enctype="multipart/form-data">
    <input type="hidden" name="list_action" id="list_action" value="<?= $admin_path . $controller ?>upload/"/>
    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding10px">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <a href="#" class="albums act" onclick="setAlbum('0', this);return false;">Все</a>&nbsp;&nbsp;
                            <? foreach ($albums as $item): ?>
                                <a class="albums" href="#" id="album_<?= $item['id'] ?>" onclick="setAlbum('<?= $item['id'] ?>', this);return false;"><?= $item['title'] ?></a>&nbsp;&nbsp;
<? endforeach; ?>
                            <a href="<?= $admin_path . $controller ?>edit_albums" class="flRight"><?= i18n::get('Edit albums') ?></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel whiteBg padding20px">

            <? if ($errors): ?>
            <div class="errors">
            <? foreach ($errors as $message)
                echo $message; ?>
            </div>
        <? endif; ?>
        <!-- **** -->
<? //include MODPATH.'admin/views/system/uploadify.php' ?>



        <script type="text/javascript" src="/static/admin/js/images_upload.js.php?u=<?= cookie::get('user')?>&n=<?= $_COOKIE[session_name()]?>&path=<?= $admin_path . $controller . $id ?>&postInit=<?= $_GET['postInit']?>""></script>
        <script type="text/javascript" src="/vendor/uploadify/jquery.uploadify.js"></script>
        <link href="/vendor/uploadify/uploadify.css" rel="stylesheet" type="text/css" />

        <!--uploadify-->
        <script type="text/javascript" >
            function showImagesPreview(album_id)
            {
                $('#preloader').show();
                $.getJSON('<?= $admin_path . $controller ?><?= isset($id) ? $id . '/' : '' ?>images_show', {album_id:album_id},function(data)
                {
                    newHtml = ''; totalImages=0;
            
                    for(i in data)
                    {
                        if (album_id && album_id!='0') // Photo List
                        {
                            $('#controlButtons').show();
                    
                            newHtml += '<div class="clear"></div><p class="albumTitle">'+i+'</p><div class="clear"></div>';
                            for(j in data[i] )
                            {
                                newHtml += '<div file="'+data[i][j]['id']+'" class="rel tCenter albumID'+data[i][j]['album_id']+' imgPrev" rel="" orderby="'+data[i][j]['order_by']+'" style="width: <?= $image_prev_width ?>px;height: <?= $image_prev_height ?>px;">';
                        
                                newHtml += '<div class="clear"></div>\
                                        <div onclick="selectPhoto($(this).parent());" class="image" title="'+(data[i][j]['comment']+'')+'"'
                                    +'" rel="/static/media/<?= $images_folder ?>/'+data[i][j]['filename']+'" style="background: url(/static/media/<?= $images_folder ?>/prev_'+data[i][j]['filename']+') center center no-repeat;">\
                                        </div>\
                                        <!--<a href="javascript:void(0);" class="editPhoto" rel="'+data[i][j]['id']+'" onclick="showPopup(this,'+data[i][j]['album_id']+'); return false;"><img src="/static/admin/images/edi.png"/></a>-->\
                                        <!--<span class="comment">'+data[i][j]['comment']+'</span>-->\
                                        </div>';
                            }
                            newHtml += '<div class="clear"></div>';
                        }
                        else
                        {
                            $('#controlButtons').hide();
                    
                            newHtml += '<div class="flLeft padding10px overhid" style="width: <?= $image_prev_width ?>px; padding-top:20px;"><p class="albumTitle overhid" style="height:1.5em;">'+i+'</p><div class="clear"></div>';
                            for(j in data[i] ) // Albom List
                            {
                                newHtml += '<div file="'+data[i][j]['id']+'" class="rel tCenter albumID'+data[i][j]['album_id']+' imgPrev" rel="" orderby="'+data[i][j]['order_by']+'" style="width: <?= $image_prev_width ?>px; height: <?= $image_prev_height ?>px; margin-top:5px">';
                        
                                newHtml += '<div class="clear"></div>\
                                        <div onclick="setAlbum(\''+data[i][j]['album_id']+'\', $(\'#album_'+data[i][j]['album_id']+'\'));" class="image" '
                                    +'" rel="/static/media/<?= $images_folder ?>/'+data[i][j]['filename']+'" style="background: url(/static/media/<?= $images_folder ?>/prev_'+data[i][j]['filename']+') center center no-repeat">\
                                        </div>\
                                        </div>';
                            }
                            //                    newHtml += '</div>';
                            newHtml += '</div>';
                        }
                
                    }
            
                    $('#imagesPreview').html(newHtml);
                    $('#preloader').hide();
            
                    // show images on doubleclick
                    $('#imagesPreview div.imgPrev .image').dblclick(function()
                    {
                        window.open($(this).attr('rel'));
                    })
            
            
                });
            }
    
            function setDefault( thisObj )
            {
                $(thisObj).children('.smallPreloader').show();
                $.ajax({
                    type: 'GET',
                    url: '<?= $admin_path . $controller ?><?= $id ?>/images_default?name='+$(thisObj).attr('file'),
                    success: function(data){
                        if(data=='1')
                        {
                            $('#imagesPreview div.imgPrev.default').removeClass('default');
                            $(thisObj).addClass('default');
                            $(thisObj).children('.smallPreloader').hide();
                        }
                        else
                            alert('Server error!');
                    },
                    error: function(){ alert('Server error!'); }
                });
            }
    
            function deletePhotos()
            {
                ids = [];
                i=0;
                $('#imagesPreview .imgPrev.selected').each(function(){
                    ids[i] = $(this).attr('file');
                    i++;
                });
                if ( i == 0) { alert('Please choose photo, by clicking them');return false; }
        
        
                $.ajax({
                    type: 'GET',
                    data:{photo_id:ids},
                    url: '<?= $admin_path . $controller ?><?= $id ?>images_delete',
                    success: function(data){
                        if(data=='1')
                        {
                            $('#imagesPreview .imgPrev.selected').remove();
                            //                newMaxLeft++;
                            //                    $('#fileInput').uploadifySettings( 'queueSizeLimit', newMaxLeft );
                        }
                    }
                });
            }
    
            function selectPhoto(thisObj)
            {
                $(thisObj).toggleClass('selected');
            }
    
            $(document).ready(function(){
                showImagesPreview();
            });
        </script>
        <style type="text/css">

    div.imgPrev { background: center top no-repeat; border:1px solid #999999;float:left;height:126px;margin:18px 5px 3px 3px;width:120px;}
        div.imgPrev .image{ height: 100px; width: 100%; overflow: hidden; }
        div.imgPrev .image img{ height: 100%; text-align: center; }
    div.imgPrev.default { border: 3px solid yellow; margin:16px 3px 1px 1px; }
    div.imgPrev.selected { border: 3px solid #2A8EFF; margin:16px 3px 1px 1px;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px; }
        div.imgPrev .delPhoto{ position: absolute;top: 1px; right: 1px; z-index: 3;}
        div.imgPrev .editPhoto{ position: absolute;top: 1px; right: 1px; z-index: 3; width: 16px; height: 16px; }
        div.imgPrev .editPhoto img { width: 100%;height: 100%;}
        div.imgPrev .actPhoto{ position: absolute;top: 4px; right: 4px; z-index: 3; background: url(/static/admin/images/act_red.png);width: 16px; height: 16px;}
        div.imgPrev.default .actPhoto{ background-image: url(/static/admin/images/act_green.png); }
        div.imgPrev .comment{ position: absolute;bottom: 0;width: 99%; text-align: center; left:0; }

    
        .popup, .popupClone{ background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #000000; height: 100px; padding: 11px; text-align: left; width: 300px; z-index: 6; }
		
        .popupInput{ margin: 2px 0; }
        #popupClone{display: none;}

        .overlay{ position: absolute;width: 100%;height: 100%; top: 0;left: 0; }
        .smallPreloader{ position: absolute;top: 47px;left:0;width: 120px;height: 30px; text-align: center;}

    .albumTitle { overflow: hidden; padding-left:5px; color:grey; font-size:14px}
        </style>
        <?

        function albumOptions($val=array(), $key=NULL) {
            $GLOBALS['albums'][$val['id']] = $val['title'];
        }

        array_walk($albums, 'albumOptions');
        ?>
		<br />
        <p><? echo I18n::get('Photogallery (double click to view)') ?></p>
		<br />
        <div id="images_list">

            <div id="popupClone" class="popupClone">
                <input type="hidden" value="0" class="photoID"/>
				<span style="color:grey; font:italic 12px Arial">Описание фотогрфаии:</span>
				<br/>
				<br/>
                <input type="text" style="width: 290px;" name="titlePopup" value="" class="popupInput"/><br/><br/>
                <!-- <? echo Form::select('albumPopup', $GLOBALS['albums'], NULL, array('class'=>'popupInput')); ?><br/>
                <input type="file" name="Filedata" id="uploadPopup" class="popupInput"/><br/><br/> -->
                <button class="btn formSave popupInput flLeft" type="button" onclick="formSend(this);return false;"><span><span>Сохранить</span></span></button>
                <img src="/static/admin/images/preloader.gif" width="16" id="preloader2" style="display: none;"/>
                <a href="#" style="margin-top: 10px; " class="flRight" onclick="$(this).parent().remove();return false;"><?echo I18n::get('Close')?></a>
            </div>

            <div id="imagesPreview"><? echo I18n::get('Photos list') ?></div>
            <div class="clear"></div>
            <br>
            <div id="controlButtons">
                <div class="uploadifyClone" style="display:none;">
                    <input class="fileInputClone" name="" type="file" />
                </div>
                <div id="uploadify" class="flLeft">
                    <input id="fileInput" name="fileInput" type="file"/>
                </div>
                <button class="btn blue flRight" name="photosDelete" onclick="deletePhotos();return false;"><span><span>Удалить</span></span></button>
                <!--	    <a href="javascript:$('#fileInput').uploadifyUpload();">Upload Files</a> |
                    <a href="javascript:$('#fileInput').uploadifyClearQueue();">Clear Queue</a>-->
            </div>
			<div class="clear"></div>
			<br />
            <ul id="menu" class="contextMenu hidden">
                <li id="default"><? echo I18n::get('Set Default') ?></li>
                <li id="delete"><? echo I18n::get('Delete') ?></li>
            </ul>
        </div>


        <!-- **** -->
<? if ($pagination): ?>
    <?= $pagination ?>
<? endif; ?>
        <!-- **** -->

        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
