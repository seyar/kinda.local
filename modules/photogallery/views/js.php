<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 18.12.2010 - 15:54:54 Exp $
 *
 * Project:     kontext
 * File:        js.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript">
    function reinit( album_id )
    {
        $('#uploadify').html('');
        $('#uploadify').html( $('.uploadifyClone').html() );

        $('#uploadify .fileInputClone').attr({name:'fileInput', id:'fileInput'});

        if(album_id == 0)
            $('#uploadify').hide();
        else
		{
            $('#uploadify').show();

			uploadify(album_id);
		}	
    }

    function delPhoto(thisObj)
    {
        $('#preloader').show();
        $.ajax({
            type: 'GET',
            url: '<?= $admin_path . $controller ?><?= $id ?>/images_delete?photo_id='+$(thisObj).parent().attr('file'),
            success: function(data){
                if(data=='1')
                {
                    $(thisObj).parent().remove();
                    $('#preloader').hide();
                }
            }
        });
    }

    function saveOrder()
    {
        var photos = {};
        var i = 1;
        $('#imagesPreview div.imgPrev').each(function(){
            photos[i] = $(this).attr('file');
            i++;
        });

        $.getJSON(
        '<?= $admin_path . $controller ?><?= $id ?>/saveOrder',
        {photos:photos},
        function(data)
        {
            if(data != '1')
            {
                $('#preloader').hide();
                alert('Server error!');
            }
        }
    );
    }

    function setAlbum(id,thisObj)
    {
        $('.albums').removeClass('act');
        $(thisObj).addClass('act');

        showImagesPreview(id);
		
		$(document).ready(function(){
			reinit(id);
		});
    }

    function showPopup(thisObj, album_id)
    {
        width = $('.wrap').css('width');
        width = width.replace('px', '');

        $('.popup').remove();
        var popup = $('#popupClone').clone();

        $(thisObj).parent().append( $(popup).attr({'id':'', 'class':'popup'}) );
        pos = $('.popup').offset();

        //if(pos.left > (width - 600) )
        //    $('.popup').css({'left':'auto','right':'0'});
        
		$('.popup').css({'left':'36%', 'position':'fixed','top':'45%','-moz-border-radius':'5px'});

        $('.popup input.photoID').val( $(thisObj).attr('rel') );
        $('.popup input[name=titlePopup]').val( $(thisObj).parent().children('.comment').text() );
        $('.popup select[name=albumPopup]').val( album_id );

        //            $(document).bind('click',function(){ closePopup()});
    }

    function closePopup( thisObj )
    {
        $('.popup').remove();
        $(document).unbind('click');
    }

    function moveLeft( thisObj )
    {
        var index = $('#imagesPreview div.imgPrev').index(thisObj);
        var prevElm = $('#imagesPreview div.imgPrev').eq(index-1);
        var defaultClass = ( $(thisObj).hasClass('default') ? 1 : 0 );
        var prevDefaultClass = ( $(prevElm).hasClass('default') ? 1 : 0 );
        if(index != 0)
        {
            $(thisObj).children('.smallPreloader').removeClass('hidden');
            $('#preloader').show();
            $.getJSON(
            '<?= $admin_path . $controller ?><?= $id ?>/moveLeft{literal}',
            {curID:$(thisObj).attr('file'), prevID:$(prevElm).attr('file'), defaultClass:defaultClass, prevDefaultClass:prevDefaultClass },
            function(data)
            {
                if(data)
                {
                    reinit($('#cur_album_id').val());
                    showImagesPreview($('#cur_album_id').val());
                    $(thisObj).children('.smallPreloader').addClass('hidden');
                    $('#preloader').hide();
                    /*
                        $('#imagesPreview div.imgPrev').eq(index-1).remove();
                        $(thisObj).attr('file',data[0]);
                        $(thisObj).after( prevElm.attr('file',data[1]) );
                        $(thisObj).attr('id',data[0]);
                     */
                }else{
                    $('#preloader').hide();
                    alert('Server error!');
                }
            }
        );

        }
    }

    function moveRight( thisObj )
    {
        var amount = $('#imagesPreview div.imgPrev').length;
        var index = $('#imagesPreview div.imgPrev').index(thisObj);
        var prevElm = $('#imagesPreview div.imgPrev').eq(index+1);
        var defaultClass = ( $(thisObj).hasClass('default') ? 1 : 0 );
        var prevDefaultClass = ( $(prevElm).hasClass('default') ? 1 : 0 );

        if(index != amount-1)
        {
            $('#preloader').show();
            $(thisObj).children('.smallPreloader').removeClass('hidden');
            $.getJSON(
            '<?= $admin_path . $controller . $id ?>/moveRight{literal}',
            {curID:$(thisObj).attr('file'), prevID:$(prevElm).attr('file'), defaultClass:defaultClass, prevDefaultClass:prevDefaultClass},
            function(data)
            {
                if(data)
                {
                    reinit($('#cur_album_id').val());
                    showImagesPreview($('#cur_album_id').val());
                    $('#preloader').hide();
                    $(thisObj).children('.smallPreloader').addClass('hidden');
                    /*
                        $('#imagesPreview div.imgPrev').eq(index+1).remove();
                        $(thisObj).attr('file',data[0]);
                        $(thisObj).before(prevElm.attr('file',data[1]));
                        $(thisObj).attr('id',data[0]);
                     */
                }else{
                    $('#preloader').hide();
                    alert('Server error! '+data);
                }
            }
        );

        }
    }

    function formSend( thisObj )
    {
        if( $('.popup input[name=Filedata]').val() )
        {
            $('#list_action').val('<?= $admin_path . $controller?>'+$('.popup input.photoID').val()+'/updatePhoto');
            styleBg = $(thisObj).parent().parent().children('.image').attr('style');
            styleBg = styleBg.substr( (styleBg.indexOf('=')+1), 17 );
            $('#filename').val( styleBg );

            $(document).find('form')
                .attr( 'action', $('#list_action').val() )
                .attr( 'method', 'POST')
                .submit();
        }else{

            $('#preloader').show();
            $('#preloader2').show();
            $.getJSON(
                '<?= $admin_path . $controller . $id ?>/updatePhotoAjax?titlePopup='+$('.popup input[name=titlePopup]').val()+'&albumPopup='+$('.popup input[name=albumPopup]').val()+'&popupPhotoID='+$('.popup input.photoID').val() ,
            function(data){
                if(data=='1')
                {
                    $('#preloader').hide();
                    $('#preloader2').hide();
                    var obj = $(thisObj).parent().parent().children('.image');
                    var album_id = $('.popup input[name=albumPopup]').val();
                    $(thisObj).parent().parent().children('.comment').text( $('.popup input[name=titlePopup]').val() );
                    $(thisObj).parent().parent().children('.image').attr('onclick','');
                    $(thisObj).parent().parent().children('.image').bind('click',function(){
                        showPopup( obj, album_id);
                        return false;
                    });

                    var div = $(thisObj).parent().parent();

                    $(div).remove();

                    //$('.albumID'+album_id).each(function(){
                    //    if( $(div).attr('file') == $(this).attr('file') )
                    //        $(this).before(div);
                    //});

                    $('.albumID'+album_id+':last').after(div);
                    $('.popup').remove();
                }
                else
                    alert('Server error!');
            }
        );
        }
        return false;
    }

</script>