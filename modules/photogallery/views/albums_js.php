
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript">
    function editAlbum(id, thisObj)
    {
        $('#preloader').show();
        $('.addBox').slideUp();
        $('#albumID').remove();
        
        $.getJSON(
            '<?=$admin_path.$controller?>get_album?id='+id,
            function(data)
            {
                inputAlbumId = $( document.createElement('input') ).attr({id:'albumID',type:'hidden', name:'albumID',value:data.id});
                $('.addBox').append(inputAlbumId);

                $('#preloader').hide();
                $('#title').val(data.title);
                $('#order_by').val(data.order_by);

                $('.addBox').slideToggle();
            }
        );

    }

    function deleteAlbum(id, thisObj)
    {
        $('#preloader').show();
        $.get(
            '<?=$admin_path.$controller?>'+id+'/delete_album',
            function(data)
            {
                if(data == 1)
                    $(thisObj).parent().parent().remove();
                else
                    alert('Server error '+data);
            }
        );
        $('#preloader').hide();

    }
</script>
