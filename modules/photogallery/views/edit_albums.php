<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 18.12.2010 - 14:48:01 Exp $
 *
 * Project:     kontext
 * File:        edit_albums.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<?include 'albums_js.php'?>
<!--floating block-->
<form method="post" action="">
    <input type="hidden" name="list_action" id="list_action" value="<?=$admin_path.$controller?>upload/"/>
    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding10px">
                <table width="100%">
                    <tr>
                        <td>
                            <button class="btn blue" type="button" onclick="$('#albumID').remove();$('.addBox').slideToggle();return false;">
                                <span><span><?echo I18n::get('Add album');?></span></span></button>

                        </td>
                        <td align="right">

                            <a href="<?=$admin_path.$controller?>" class="flRight"><?=i18n::get('Edit photos');?></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel whiteBg">

            <? if ($errors): ?>
            <div class="errors">
                <? foreach ($errors as $message)
                    echo $message; ?>
            </div>
            <? endif; ?>
            <!-- **** -->
            <input type="hidden" name="popupPhotoID" id="popupPhotoID" value=""/>
            <input type="hidden" name="filename" id="filename" value=""/>

                <div class="addBox" style="display:none;">
                    <table class="albumTab">
                        <tr>
                            <td>Название</td>
                            <td><input type="text" name="title" id="title" value="<?=$_POST['album']?>"/></td>
                        </tr>
                        <tr>
                            <td>№ сорт.</td>
                            <td><input type="text" name="order_by" id="order_by" value="<?=Arr::get($_POST,'album',9999)?>"/></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button class="btn formSave" style="margin-right:20px" id="addAlb" type="button" onclick="$('#list_action').val('<?=$admin_path.$controller?>add_album/');return false;"><span><span>Сохранить</span></span></button>
                                <button class="btn" id="addAlb" type="button" onclick="$('#albumID').remove();$('.addBox').slideUp();return false;"><span><span>Отмена</span></span></button>
                            </td>
                        </tr>
                    </table>
                </div>


                <table cellspacing="0" cellpadding="0" width="100%" class="sortableContentTab">
                    <tr>
                        <th  class="tCenter" width="10%">№ сорт</th>
                        <th>Альбом</th>
                        <th width="5%">&nbsp;</th>
                    </tr>

                    <?foreach($albums as $item):?>
                    <tr>
                        <td class="tCenter">
                            <?=$item['order_by']?>
                        </td>
                        <td class="tLeft"><a href="#" onclick ="editAlbum('<?=$item['id']?>', this);return false;"><?=$item['title']?></a></td>
                        <td>
                            <a href="#" onclick="if(confirm('Вы уверены?')) deleteAlbum('<?=$item['id']?>', this);return false;" title="Удалить"><img src="/static/admin/images/del.gif" alt="delete"/></a>
                        </td>
                    </tr>
                    <?endforeach;?>
                </table>
            <!-- **** -->
            <? if ($pagination): ?>
            <?=$pagination?>
            <? endif; ?>
            <!-- **** -->

        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
