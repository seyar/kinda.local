<?php defined('SYSPATH') or die('No direct script access.');

return array
(
    'Photogallery'				=> 'Фотогалерея',
    'Photogallery Descr Short'	=> 'Photogallery Descr Short',
    'Photogallery Descr Full'	=> 'Photogallery Descr Full',

    'Create new' => 'Создать новый / Загрузить',
    'Choose saved obj' => 'Перейти к объекту',
    'Choose saved map' => 'Загрузить шаблон',
    'Choose img' => 'Выбрать картинку',

    'Documents' => 'Документы',
    'Close' => 'Закрыть',
	'Photogallery (double click to view)' => 'Для просмотра фотографии нажмите на нее два раза',
);

