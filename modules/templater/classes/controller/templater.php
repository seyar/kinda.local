<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 25.11.2010 - 12:11:33 Exp $
 *
 * Project:     kontext
 * File:        templater.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
class Controller_Templater extends Controller_Content
{
    public $template = 'index.tpl';

    
    
    public function action_index()
    {
        
        $this->template = $this->request->param('file').'.tpl';
    }
}
?>
