<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 25.11.2010 - 12:09:13 Exp $
 *
 * Project:     kontext
 * File:        init.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */

// Frontned Routes
//
// //
// vote
Route::set
        (
	    'templater',
	    '(<lang>/)t(/<file>)',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'templater',
		    'file'        => 'index',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
            )
	)
;

?>