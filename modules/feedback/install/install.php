<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
            'name'             => 'Feedback',
            'fs_name'          => 'feedback',
            'panel_serialized' => NULL,
            'useDatabase'      => true
        );