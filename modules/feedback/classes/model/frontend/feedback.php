<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Frontend_Feedback extends Model
{

    public static function save_to_db($message, $contacts = '')
    {
        Db::insert('feedback')
                ->columns(
                        array(
                            'contacts',
                            'message',
                            'languages_id',
                        )
                )
                ->values(
                        array(
                            $contacts,
                            $message,
                            CURRENT_LANG_ID,
                        )
                )
                ->execute();
    }

}