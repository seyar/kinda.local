<?php

defined('SYSPATH') OR die('No direct access allowed.');

/* @version $Id: v 0.1 21.04.2010 - 16:04:50 Exp $
 *
 * Project:     ChimeraCMS 2
 * File:        init.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Yuriy Klinkov <klinkov@gmail.com>
 */

class Model_Feedback extends Model
{

    static public function get_items($languages_id = NULL)
    {
        $rows_per_page = Kohana::config('feedback')->rows_per_page;

        $_POST['rows_num'] = $rows_per_page;

        $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
                        ->from('feedback')
                        ->where('languages_id', '=', $languages_id);

        $query = DB::select()
                        ->from('feedback')
                        ->where('languages_id', '=', $languages_id);

        $count = $query_c
                        ->execute()
                        ->get('mycount');

        if (isset($_POST['page']))
            $_GET['page'] = $_POST['page'];

        $pagination = Pagination::factory(array(
                    'total_items' => $count,
                    'items_per_page' => $rows_per_page,
                ));

        $result = $query
                        ->order_by('date', 'DESC')
                        ->limit($pagination->items_per_page)
                        ->offset($pagination->offset)
                        ->execute();

        $page_links_str = '';

//	    $page_links_str = $pagination->render();

        if ($result->count() == 0)
            return array(array(), '', 1);
        else
        {
            return array($result->as_array(), $page_links_str, $pagination->total_pages);
        }


        if ($result->count() == 0)
        {
            return false;
        }
        else
        {
            return $result->as_array('id');
        }
    }

    static public function delete($id = NULL)
    {
        $query = DB::delete('feedback');

        if (is_numeric($id))
	{
            $query->where('id', '=', $id); // ->limit(1)

	    $total_rows = $query->execute();
	}

	return true;
    }

    static public function delete_list($array)
    {
        $query = DB::delete('feedback');
        if (count($array))
	{
	    $list = array_flip($array);

            $query->where('id', 'in', $list ); // ->limit(1)

	    $total_rows = $query->execute();

	}
	return true;
    }

}