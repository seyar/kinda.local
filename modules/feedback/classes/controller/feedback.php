<?php defined('SYSPATH') or die('No direct script access.');

/* @version $Id: v 0.1 21.04.2010 - 16:04:50 Exp $
 *
 * Project:     ChimeraCMS 2
 * File:        init.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Yuriy Klinkov <klinkov@gmail.com>
 */

class Controller_Feedback extends Controller_AdminModule
{

    public $template = 'feedback';
    public $module_name = 'feedback';

    public $module_title = 'Feedback';
    public $module_desc_short = 'Feedback Descr Short';
    public $module_desc_full = 'Feedback Descr Full';

    public function action_index()
    {
        list($this->template->pagination, $this->template->rows) = Admin::model_pagination(NULL, 'feedback',
                array('id', 'date', 'contacts', 'message'), array(), 'date'
            );
    }


    public function action_delete()
    {
	if ($this->request->param('id'))
	    Model_Feedback::delete( $this->request->param('id') );

	elseif (!empty($_POST['chk']))
	    Model_Feedback::delete_list( $_POST['chk'] );

        $this->redirect_to_controller($this->request->controller);
    }

}