<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );

class Controller_Frontend_Feedback extends Controller_Content
{

    public $template;

    public function before()
    {
        parent::before();
        $this->template = Kohana::config( 'feedback' )->front_template;
    }

    public function action_index()
    {
        $this->view->assign( 'targetPage', Kohana::config( 'feedback' )->target_page );
        Model_Content::find_static_page( Request::instance()->uri );
    }

    public function action_send()
    {
        mb_internal_encoding( 'utf-8' );
        
        if( isset( $_POST ) && !empty( $_POST ) && !isset( $_POST['agree'] )/* && mb_strlen($_POST['action']) == 1 */ )
        {
            $message = include(Kohana::find_file( 'views', $_POST['mail_template'] ));

            if( Kohana::config( 'feedback' )->save_to_db )
            {
                list($contacts, $msg) = explode("\n\n", $message);

                Model_Frontend_Feedback::save_to_db( $msg, $contacts);
            }

            if( Email::send(
                            Kohana::config( 'feedback' )->send_to, Kohana::config( 'feedback' )->send_from, Kohana::config( 'feedback' )->subject, $message
                    )
            )
            {
                $this->request->redirect( $_POST['referer'] . '?success' );
            }else
            {
                $this->request->redirect( $_POST['referer'] . '?failure' );
            }
        }else
        {
            $this->request->redirect( $_POST['referer'] . '?failure' );
        }
    }

}
