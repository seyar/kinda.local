<?php defined('SYSPATH') OR die('No direct access allowed.');

/* @version $Id: v 0.1 21.04.2010 - 16:04:50 Exp $
 *
 * Project:     ChimeraCMS 2
 * File:        init.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Yuriy Klinkov <klinkov@gmail.com>
 */

return array
(
	'lang'		=> 'ru', // Default the  language.
	'path'		=> dirname(__FILE__) . '/../views/', // Admin templates folder.


	'front_template' => 'inner.tpl',
	'mail_template' => 'mail',
	'send_from' => array( 'noreply@' . $_SERVER['HTTP_HOST'], 'Cообщение с сайта '.$_SERVER['HTTP_HOST'] ),
	'send_to'   => 'kinda@home.cris.net',
	'subject'   => 'Сообщение сайта ' . $_SERVER['HTTP_HOST'],
	'charset'   => 'UTF-8',

	'target_page' => 'feedback',

	'save_to_db' => true,
	'rows_per_page' => 25,
    
    'title' => 'Контакты. Рекламодателям. О проекте. Портал крымских новостей. сontext.crimea.ua',
);
