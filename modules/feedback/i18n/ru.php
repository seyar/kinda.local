<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 08.04.2010 - 16:16:56 Exp $
 *
 * Project:     ChimeraCMS2
 * File:        ru.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 * @modified Yuriy Klinkov <klinkov@gmail.com>
 */
return array
(
    'Feedback'   		=> 'Сообщения',
    'Contact Info'   		=> 'Контакт. Информация',
    'Message'   		=> 'Сообщение',
);
?>