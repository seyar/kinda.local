<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        feedback.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Yuriy Klinkov <klinkov@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>

<form method="post" action="">
<!--floating block-->
    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <table width="100%">
                    <tr>

                        <td class="vMiddle tRight"><?echo I18n::get('With marked');?></td>
                        <td style="padding-top:2px"><select id="list_action"><option value="delete"><?echo I18n::get('Delete');?></option></select></td>
                        <td style="width:46%"><button class="btn formUpdate" type="button">
                                <span><span><?echo I18n::get('Apply');?></span></span></button>
                        </td>
                        <td class="vMiddle tRight"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>
                        <td><?=Form::select('rows_per_page', array(
                                '10' => '10 ' . I18n::get('lines'),
                                '20' => '20 ' . I18n::get('lines'),
                                '30' => '30 ' . I18n::get('lines'),
                        ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange'=>"formfilter(jQuery('#rows_per_page'))"))?>
                        </td>
                    </tr>
                </table>
            </div>
<!--            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>-->
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel whiteBg">
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>
            <!-- **** -->
                <table width="100%" cellpadding="0" cellspacing="0" class="sortableContentTab">
                    <thead>
                        <tr>
                            <th style="width:5%;padding-left:0;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll" /></th>
                            <th style="width:10%; padding: 10px;" class="tLeft <?=Admin::table_sort_header('date')?>" ><?= I18n::get('Date') ?></th>
                            <th style="width:20%; padding: 10px;" class="tLeft <?=Admin::table_sort_header('contacts')?>"><?= I18n::get('Contact Info') ?></th>
                            <th style="width:60%; padding: 10px;" class="tLeft <?=Admin::table_sort_header('message')?>"><?= I18n::get('Message') ?></th>
                            <th style="width:5%;"><?= I18n::get('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?if($rows):?>
                        <?foreach($rows as $key=>$item):?>
                        <tr class="<?if($key % 2 == 0):?>even<?else:?>odd<?endif;?>">
                            <td class="tCenter"><input type="checkbox" name="chk[<?=$item['id']?>]" value="<?=$item['id']?>"  class="listCheckbox checkBox" /></td>
                            <td><?= $item['date'];?></td>
                            <td><?= nl2br($item['contacts']);?></td>
                            <td><?= nl2br($item['message']);?></td>
                            <td>
                                <span class="pageicons" style="width: 22px; border:none; margin: 0 auto;">
                                <a href="<?=$admin_path.$controller?><?=$item['id']?>/delete"><img src="/static/admin/images/del.gif" alt="delete" title="<?echo I18n::get('Delete')?>"/></a>
                                </span>
                            </td>
                        </tr>
                        <?endforeach;?>
                        <?endif;?>
                    </tbody>
                </table>
            <? if ($pagination): ?>
            <?=$pagination?>
            <? endif; ?>
            <!-- **** -->
<?//echo Kohana::debug($kohana_view_data);?>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
