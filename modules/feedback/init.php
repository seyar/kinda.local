<?php

defined('SYSPATH') or die('No direct script access.');

/* @version $Id: v 0.1 21.04.2010 - 16:04:50 Exp $
 *
 * Project:     ChimeraCMS 2
 * File:        init.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Yuriy Klinkov <klinkov@gmail.com>
 */

Route::set
        (
                'feedback',
                'admin/feedback((/<id>)/<action>)',
                array('id' => '\d{1,10}',)
        )
        ->defaults
        (
                array(
                    'controller' => 'feedback',
                    'id' => NULL,
                    'action' => 'index',
                )
        )
;

$route_conf = Kohana::config('feedback')->target_page;

Route::set
        (
                'feedback_front',
                '(<lang>/)'.$route_conf.'(/<action>)',
                array('lang' => '(?:' . LANGUAGE_ROUTE . ')',)
        )
        ->defaults
        (
                array(
                    'controller' => 'frontend_feedback',
                    'action' => 'download',
                    'lang' => LANGUAGE_ROUTE_DEFAULT,
                )
        )
;