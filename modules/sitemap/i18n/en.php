<?php defined('SYSPATH') or die('No direct script access.');

return array
(
    'Sitemap Descr Short'	=> 'File Sitemap - this is a file with additional information on this site.',
    'Sitemap Descr Full'	=> 'File Sitemap - this is a file with additional information on this site.<br /> With a Sitemap you can tell search engines (Yandex, Google, etc), which pages on your site should be indexed as frequently updated information on the internet, as well as which pages indexed is most important. ',
);