<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        sitemap.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Yuriy Klinkov <klinkov@gmail.com>
 */
?>
<link rel="stylesheet" type="text/css" href="/static/admin/css/paginator.css" />
<script type="text/javascript" src="/static/admin/js/edit.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.paginator.js"></script>

<script type="text/javascript">
	function nextPage(page_id)
	{
	    current_page = parseInt($(page_id).val());

	    if(current_page < $('#total_pages').val())
		$(page_id).val( current_page+1 );

	    return current_page;
	}

	function prevPage(page_id)
	{
	    current_page = parseInt($(page_id).val());

	    if(current_page > 1)
		$(page_id).val( current_page-1 );

	    return current_page;
	}
</script>

<form method="post" action="">
<!--floating block-->
    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <table width="100%">
                    <tr>

                        <td class="vMiddle tRight"><?echo I18n::get('With marked');?></td>
                        <td style="padding-top:2px"><select id="list_action"><option value="delete"><?echo I18n::get('Delete');?></option></select></td>
                        <td style="width:46%"><button class="btn formUpdate" type="button">
                                <span><span><?echo I18n::get('Apply');?></span></span></button>
                        </td>
                        <td class="vMiddle tRight"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>
                        <td><?=Form::select('rows_per_page', array(
                                '10' => '10 ' . I18n::get('lines'),
                                '20' => '20 ' . I18n::get('lines'),
                                '30' => '30 ' . I18n::get('lines'),
                        ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange'=>"formfilter(jQuery('#rows_per_page'))"))?>
                        </td>
                    </tr>
                </table>
            </div>
<!--            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>-->
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel whiteBg">
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>
            <!-- **** -->
                <table width="100%" cellpadding="0" cellspacing="0" class="sortableContentTab sitemaptab">
                    <thead>
                        <tr>
                            <th style="width:5%;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll" /></th>
                            <th style="width:5%;" class="tLeft" >№</th>
                            <th style="width:80%;" class="tLeft"><?= I18n::get('File') ?></th>
                            <th style="width:10%;"; class="tLeft"><?= I18n::get('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="<?if($key % 2 == 0):?>even<?else:?>odd<?endif;?>">
                            <td class="tCenter"><input type="checkbox" name="chk[<?=$item['id']?>]" value="<?=$item['id']?>"  class="listCheckbox checkBox" /></td>
                            <td>1</td>
                            <td><a href="<?=$admin_path.$controller?>1/edit" >sitemap.xml</a></td>
                            <td>
                                <span class="pageicons" style="width: 22px; border:none">
                                <a href="<?=$admin_path.$controller?><?=$item['id']?>/delete" onclick="if( !confirm('<?echo I18n::get('Are you sure?')?>') )return false;"><img src="/static/admin/images/del.gif" alt="delete" title="<?echo I18n::get('Delete')?>"/></a>
                                </span>
                            </td>
                        </tr>
                        <tr class="<?if($key % 2 == 0):?>even<?else:?>odd<?endif;?>">
                            <td class="tCenter"><input type="checkbox" name="chk[<?=$item['id']?>]" value="<?=$item['id']?>"  class="listCheckbox checkBox" /></td>
                            <td>2</td>
                            <td><a href="<?=$admin_path.$controller?>2/edit" >robots.txt</a></td>
                            <td>
                                <span class="pageicons" style="width: 22px; border:none">
                                <a href="<?=$admin_path.$controller?><?=$item['id']?>/delete" onclick="if( !confirm('<?echo I18n::get('Are you sure?')?>') )return false;"><img src="/static/admin/images/del.gif" alt="delete" title="<?echo I18n::get('Delete')?>"/></a>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <? if ($pagination): ?>
            <?=$pagination?>
            <? endif; ?>
            <!-- **** -->
<?//echo Kohana::debug($kohana_view_data);?>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
