<?php defined('SYSPATH') or die('No direct script access.');

Route::set
        (
	    'sitemap',
	    'admin/sitemap((/<id>)/<action>)(/<path>)',
	    array( 'id' => '\d{1,10}', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'sitemap',
		    'id'	    => NULL,
		    'action'        => 'sitemap',
                )
	)
;

// frontend route
Route::set
        (
	    'sitemap_front',
	    '(<lang>/)sitemap',
	    array( 'lang' => '(?:'.LANGUAGE_ROUTE.')', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_sitemap',
		    'action'        => 'sitemap',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;