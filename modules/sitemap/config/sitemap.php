<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	'lang'		=> 'ru', // Default the  language.
	'path'		=> dirname(__FILE__) . '/../views/', // Admin templates folder.
);
