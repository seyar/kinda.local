<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Sitemap extends Model {

    static public $sitemap = array();

    public static function get_from_content($parent_id = 0)
    {

	$query = DB::select('id', 'page_name','page_url', 'parent_url', 'page_title')
			    ->from('content')
			    ->where('status', '=', 1)
			    ->and_where('languages_id', '=', CURRENT_LANG_ID)
			    ->and_where('visible_to', '=', 'all')
			    ->and_where('hide_from_map', '!=', '1')
			    ->and_where('parent_id', '=', $parent_id)
//			    ->order_by( DB::expr('parent_id ASC, page_order ASC') )
//			    ->cached(60)
			    ;

	$result = $query->execute();

	if ($result->count() == 0) return array();
	else
	{
            $return = array();
            $res = $result->as_array();
            foreach( $res as $item )
            {
                if( substr($item['parent_url'],-1) == '/' ) $item['parent_url'] = substr($item['parent_url'],0, -1);
                $item['depth'] = substr_count($item['parent_url'],'/');
                $item['page_name'] = $item['page_name'] ? $item['page_name'] : $item['page_title'];
//                $return[] = $item;
                Model_Sitemap::$sitemap[] = $item;
                if( Model_Sitemap::hasChilds($item['id']) ) //есть дети
                {
                   Model_Sitemap::get_from_content($item['id']);
                }
            }
	    return $return;
	}
    }

    static public function hasChilds($id)
    {
        $query = DB::select('id')
			    ->from('content')
			    ->where('status', '=', 1)
			    ->and_where('languages_id', '=', CURRENT_LANG_ID)
			    ->and_where('visible_to', '=', 'all')
			    ->and_where('hide_from_map', '!=', '1')
			    ->and_where('id', '=', $id)
			    ;

	$result = $query->execute();

	if ($result->count() == 0) return false;
	else
            return true;
    }

    public static function get_pages_from_modules()
    {
        $ret = Controller_Admin::get_sitemap();

        $contollers_list = Model_Sitemap::get_controllers_list();


        foreach( $contollers_list as $item )
        {
                $method = array('Controller_'.$item['fs_name'], 'get_sitemap');
                try
                {
                    $part_array = call_user_func( $method, false );
                    $ret = is_array($part_array) ? array_merge($part_array, $ret) : $ret;
                }
                catch(Exception $e){}
                
        }
        $return = array();
        foreach( $ret as $item )
        {
            $return[] = array(
                'page_url' => $item['url'],
                'depth' => ($item['depth'] ? $item['depth'] : 0),
                'page_name' => ( $item['title_page'] ? $item['title_page'] : $item['title_new'] )
            );
        }

        return $return;
    }

    static function get_controllers_list()
    {
        $controllers = array(
                            array('fs_name' => 'content')
                       );
        $result = DB::select()
            ->from('modules')
            ->execute();
        
        if( $result->count() == 0 ) return array();
        else return array_merge($controllers, $result->as_array());
    }

    static public function prepareArray($array)
    {
        $ret = array();
        foreach ($array as $value)
        {
            $ret[$value['parent_id']][$value['id']] =
                        array(
                                'id' => $value['id'],
                                'parent_id' => $value['parent_id'],
                                'name' => $value['name'],
                                'page_url' => str_replace("//", "/", $value['page_url']),
                            );
        }

        return $ret;
    }

    static function generate_sitemap_xml($lang_id = 1)
    {

        $siteMap = array();

        foreach (Model_Sitemap::get_controllers_list() as $controller){
            $htmlMethod = array('Controller_'.ucfirst($controller['fs_name']), 'sitemap');
            $xmlMethod = array('Controller_'.ucfirst($controller['fs_name']), 'sitemapXML');

            $htmlRes = call_user_func( $htmlMethod, false );
            if($htmlRes) $siteMap[] = $htmlRes;

            $xmlRes  = call_user_func( $xmlMethod, false );
            if($xmlRes) $siteMap[] = $xmlRes;

        }

//        return Model_Sitemap::generate_sitemap_xml( $siteMap );

        $weights = array(0.4, 0.8, 0.64, 0.5 );
        $lastmods = array(0=>30, 1=>7, 2=>14, 3=>21 );

        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <urlset
                      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
                      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
                <url>
                  <loc>http://'.$_SERVER['HTTP_HOST'].($lang_id != 1 ? '/en/':'/').'</loc>
                  <priority>'.number_format($weights['1'],2,'.','').'</priority>
                  <lastmod>'.date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d")-$lastmods['1'], date("Y"))).'</lastmod>
                </url>';

        foreach ($siteMap as $data)
        {
            foreach ($data as $item)
            {
                $xml .= '<url>
                          <loc>http://'.str_replace("//", "/", $_SERVER['HTTP_HOST'].'/'.$item['page_url']).'</loc>
                          <priority>'.number_format($item['priority'],2,'.','').'</priority>
                          <lastmod>'.date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d")-$lastmods['1'], date("Y"))).'</lastmod>
                        </url>';
            }
        }
        $xml .= '</urlset>';
        return $xml;
    }

    public function get_robots_txt()
    {
        $file = str_replace('//', '/', DOCROOT.'/robots.txt');
        if( file_exists($file) )
            return file_get_contents($file);
    }

    public function save($type_file)
    {
        $file = $type_file == 1 ? str_replace('//', '/', DOCROOT.'/sitemap.xml') : str_replace('//', '/', DOCROOT.'/robots.txt');

		$handle = fopen($file, 'w');

		if(!fwrite($handle, $_POST['sitemap_xml'])) return "writing error";

		fclose($handle);

        return 1;
    }

}
