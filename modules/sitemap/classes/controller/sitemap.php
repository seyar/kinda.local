<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Sitemap extends Controller_AdminModule {

    public $template = 'sitemap';

    public $module_name         = 'sitemap';
    public $module_title	= 'Sitemap';
    public $module_desc_short   = 'Sitemap Descr Short';
    public $module_desc_full	= 'Sitemap Descr Full';

    public function before()
    {
        parent::before();
	$this->model = new Model_Sitemap();
    }

    public function action_index()
    {
        $this->action_sitemap();

        $controllers = Model_Sitemap::get_controllers_list();
        $siteMap = array();

        foreach ($controllers as $controller){
            $method = array('Model_'.ucfirst($controller['fs_name']), 'sitemap');

            try{
                $res = call_user_func( $method, false );
                $siteMap[$controller['fs_name']] = Model_Sitemap::prepareArray($res);
            }catch(Exception $e){}
        }

        $this->page_info = array('page_title'=> I18n::get('Sitemap'), 'page_name'=> I18n::get('Sitemap')
				, 'page_content'=> Model_Sitemap::$sitemap );

    }

    public function action_sitemap()
    {
        $controllers = Model_Sitemap::get_controllers_list();
        $siteMap = array();

        foreach ($controllers as $controller){
            $method = array('Model_'.ucfirst($controller['fs_name']), 'sitemap');

            try{
                $res = call_user_func( $method, false );
                $siteMap[$controller['fs_name']] = Model_Sitemap::prepareArray($res);
            }catch(Exception $e){}
        }

        $this->page_info = array('page_title'=> I18n::get('Sitemap'), 'page_name'=> I18n::get('Sitemap')
				, 'page_content'=> Model_Sitemap::$sitemap );

        $this->template->siteMap = $siteMap;
    }

    public function action_edit()
    {
        $this->template = View::factory('sitemap_view');

        if( $this->request->param('id') == 1)
        {
            $xml = Model_Sitemap::generate_sitemap_xml();

            $this->template->sitemap_xml = $xml;
            if( file_exists( DOCROOT.'sitemap.xml' )) $file = '<a target="_blank" style="margin: 0 0 0 5px;" class="greenBtn flLeft" href="http://'.$_SERVER['HTTP_HOST'].'/sitemap.xml">файл sitemap.xml</a>';
        }else {
            $robots = Model_Sitemap::get_robots_txt();
            $this->template->sitemap_xml = $robots;
            if( file_exists( DOCROOT.'sitemap.xml' )) $file = '<a class="greenBtn flLeft" style="margin: 0 0 0 5px;" target="_blank" href="http://'.$_SERVER['HTTP_HOST'].'/robots.txt">файл robots.txt</a>';
        }
        if( isset($file) )
            $this->template->link_to_file = $file;

    }

    public function action_save()
    {
        $res = $this->model->save( $this->request->param('id') );
        if($res){
            $this->redirect_to_controller($this->request->controller );
        }else{
            $this->action_edit();
        }
    }

    public function action_update()
    {
        $res = $this->model->save( $this->request->param('id') );
        if($res){
            $this->redirect_to_controller($this->request->controller.'/'.$this->request->param('id').'/edit');
        }else{
            $this->action_edit();
        }
    }
}
