<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Sitemap extends Controller_Content {

    public $template = 'sitemap.tpl';

    public function action_sitemap()
    {
        $controllers = Model_Sitemap::get_controllers_list();
        $siteMap = array();

        foreach ($controllers as $controller){
            $method = array('Model_'.ucfirst($controller['fs_name']), 'sitemap');

            try{
                $res = call_user_func( $method, false );
                $siteMap[$controller['fs_name']] = Model_Sitemap::prepareArray($res);
            }catch(Exception $e){}
        }

        $this->page_info = array('page_title'=> I18n::get('Sitemap'), 'page_name'=> I18n::get('Sitemap')
				, 'page_content'=> Model_Sitemap::$sitemap );

        $this->view->assign('siteMap', $siteMap );
    }
}