<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );

class Controller_Frontend_Downloads extends Controller_Content
{
    
    public $template;

    public function before()
    {
        parent::before();
    }

    public function action_price()
    {
        $file = '';
        $authorized = Session::instance()->get('authorized');
        foreach( Model_Downloads::$exts as $ext )
        {
            if( file_exists(MEDIAPATH.Model_Downloads::$filename.$ext) )
            {
                $file = MEDIAPATH.Model_Downloads::$filename.$ext;
                break;
            }
        }
		
		
        if($file && is_file($file) && $authorized['id'] )
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;            
        }
        else
        {
            Messages::add('Рекомендуем зарегистрироваться для возможности скачать прайс', 'ok');
            $this->request->redirect( Route::get('registration_front')->uri(array('action'=>'','code'=>'','lang'=>'')) );
        }
            
    }


}
