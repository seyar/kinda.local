<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of downloads
 *
 * @author user1
 */
class Controller_Downloads extends Controller_AdminModule
{
    public $template = 'view';
    public $module_name = 'downloads';

    public $module_title = 'Downloads';
    public $module_desc_short = 'Feedback Descr Short';
    public $module_desc_full = 'Feedback Descr Full';
    
    public function action_index()
    {
        foreach( Model_Downloads::$exts as $ext )
        {
            if( file_exists(MEDIAPATH.Model_Downloads::$filename.$ext) )
            {
                $this->template->filename = pathinfo(MEDIAPATH.Model_Downloads::$filename.$ext);
                break;
            }
        }
        
    }
    
    public function action_upload()
    {
        Model_Downloads::download( $_FILES['price'] );
        $this->redirect_to_controller($this->request->controller);
    }
}

?>
