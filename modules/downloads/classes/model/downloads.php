<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of downloads
 *
 * @author user1
 */
class Model_Downloads extends Model
{
    public static $filename = 'price';
    public static $exts = array('.xls','.xlsx','.zip');
    
    static function download( $files = array() )
    {
        if( !$files['name'] || $files['error'] != 0 ){    Messages::add('Ошибка загрузки');return false;}
        
        $info = pathinfo($files['name']);
        
        if( !in_array( '.'.$info['extension'],  Model_Downloads::$exts) ){    Messages::add('Неверный формат файла');return false;}
        foreach( Model_Downloads::$exts as $ext )
        {
            $filepath = MEDIAPATH.Model_Downloads::$filename.$ext;
            if( file_exists($filepath) )
            {
                unlink($filepath);
            }
        }
        
        move_uploaded_file($files['tmp_name'], MEDIAPATH.self::$filename.'.'.$info['extension']);
        return true;
    }
}

?>
