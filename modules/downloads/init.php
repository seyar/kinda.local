<?php


Route::set
        (
                'downloads',
                'admin/downloads((/<id>)/<action>)',
                array('id' => '\d{1,10}',)
        )
        ->defaults
        (
                array(
                    'controller' => 'downloads',
                    'id' => NULL,
                    'action' => 'index',
                )
        )
;


Route::set
        (
                'downloads_front',
                '(<lang>/)download(/<action>)',
                array('lang' => '(?:' . LANGUAGE_ROUTE . ')',)
        )
        ->defaults
        (
                array(
                    'controller' => 'frontend_downloads',
                    'action' => 'download',
                    'lang' => LANGUAGE_ROUTE_DEFAULT,
                )
        )
;
?>