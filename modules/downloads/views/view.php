<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>

<form action="<?= $admin_path?><?=$controller?>upload" enctype="multipart/form-data" method="post">
<!--floating block-->
<!--    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <table width="100%">
                    <tr>

                        <td class="vMiddle tRight"><?echo I18n::get('With marked');?></td>
                        <td style="padding-top:2px"><select id="list_action"><option value="delete"><?echo I18n::get('Delete');?></option></select></td>
                        <td style="width:46%"><button class="btn formUpdate" type="button">
                                <span><span><?echo I18n::get('Apply');?></span></span></button>
                        </td>
                        <td class="vMiddle tRight"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>
                        <td><?=Form::select('rows_per_page', array(
                                '10' => '10 ' . I18n::get('lines'),
                                '20' => '20 ' . I18n::get('lines'),
                                '30' => '30 ' . I18n::get('lines'),
                        ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange'=>"formfilter(jQuery('#rows_per_page'))"))?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>-->

    <!--floating block-->

    <div class="rel whiteBg">
        <input type="hidden" name="list_action" id="list_action" value="upload"/>
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>
            <!-- **** -->
            <div class="padding20px">
                <table width="100%" cellpadding="0" cellspacing="0" class="sortableContentTab">
                    <thead>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input type="file" name="price" /></td>
                            <td><button class="btn formUpdate" type="button">
                                <span><span><?echo I18n::get('Upload');?></span></span></button></td>
                            <td width="80%">&nbsp;</td>
                        </tr>
                        <?if($filename):?>
                        <tr>
                            <td>&nbsp;</td>
                            <td><img src="/static/admin/images/<?=$filename['extension']?>.png" alt="" class="vMiddle"/> <a href="<?=MEDIAWEBPATH.$filename['filename'].'.'.$filename['extension']?>"><?=$filename['filename'].'.'.$filename['extension']?></a></td>
                            <td>&nbsp;</td>
                            <td width="80%">&nbsp;</td>
                        </tr>
                        <?endif;?>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Поддерживаемые форматы, xls, xlsx, zip</td>
                            <td>&nbsp;</td>
                            <td width="80%">&nbsp;</td>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- **** -->
<?//echo Kohana::debug($kohana_view_data);?>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>