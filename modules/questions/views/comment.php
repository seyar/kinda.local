<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 29.12.2010 - 21:20:17 Exp $
 *
 * Project:     kontext
 * File:        comment.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<form action="/admin/questions/add" method="post" id="commentForm">
<table width="100%" cellpadding="4" cellspacing="4">
    <tr>
        <td><b>Комментарий:</b></td>
        <td><textarea name="parent_comment" rows="5" cols="30"><?=$obj['comment']?></textarea></td>
    </tr>
    <tr>
        <td><b>Статус:</b></td>
        <td><input type="checkbox" name="status" checked="true" value="1"/></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td><b>Пользователь:</b></td>
        <td><input type="text" name="user" value="admin"/></td>
    </tr>
    <tr>
        <td><b>Ответ:</b></td>
        <td><textarea name="comment" rows="5" cols="30"><?=$parent_obj['comment']?></textarea></td>
    </tr>
    <tr><td colspan="2" class="tRight">            
            <button class="btn blue" type="button" onclick="formSave();return false;">
                                <span><span><?= I18n::get('Save') ?></span></span></button>
        </td></tr>
</table>
<input type="hidden" name="object_id" value="<?=$obj['object_id']?>" />
<input type="hidden" name="module_id" value="<?=$obj['module_id']?>" />
<input type="hidden" name="parent_id" value="<?=$obj['id']?>" />

</form>
