<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 29.12.2010 - 15:31:36 Exp $
 *
 * Project:     kontext
 * File:        comments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="rel whiteBg">
<table width="100%" class="contentTab" id="contentTab" cellpadding="0" cellspacing="0">
    <tr class="first">
        <th class="first">&nbsp;</th>
        <th width="2%"><input type="checkbox" name="" value="" style="width:auto;" class="listCheckboxAll"/></th>
        <th width="13%"><?php echo I18n::get('Module')?></th>
        <th ><?php echo I18n::get('Comment')?></th>
        <th width="15%"><?php echo I18n::get('Date')?></th>
<!--        <th width="15%"><?php echo I18n::get('Status')?></th>-->
        <th class="last" width="5%">&nbsp;</th>
    </tr>
<!--   <?/* <tr>
        <th colspan="4">&nbsp;</th>
        <th align="right">
            <?php echo I18n::get('From')?>
            <input type="text" name="dateFrom" class="date" value="<?=  htmlentities($filter['dateFrom'])?>" />
            <br />
            <?php echo I18n::get('To')?>
            <input type="text" name="dateTo" class="date" value="<?=  htmlentities($filter['dateTo'])?>" />
        </th>
        <th>
            <select name="status">
                <option value="-1">---</option>
                <option value="0"<?if($filter['status'] !== '' && $filter['status'] == 0):?> selected = "selected"<?endif;?>><?=i18n::get('Inactive')?></option>
                <option value="1"<?if($filter['status'] == 1):?> selected = "selected"<?endif;?>><?=i18n::get('Active')?></option>
            </select>
        </th>
        <th></th>
        <th class="last">
            <button class="btn formFilter" type="button">
                <span><span>
                        <?=i18n::get('Show')?>
                    </span></span>
            </button>
        </th>

    </tr>*/?>-->
    <?//foreach($rows as $comments):?>
    <?foreach($rows[0] as $key => $value):?>
            <tr class="{iteration is odd?'odd':'even'}">
                <td width="1%">&nbsp;</td>
                <td align="center"><input type="checkbox" name="chk[<?=$value['id']?>]" value="<?=$value['id']?>" class="listCheckbox"/></td>
                <td align="center">
                    <?= $installed_modules[$value['module_id']]['name']?>
                </td>
                <td><?= htmlentities($value['comment'])?></td>
                <td align="center" class="nowrap"><?=$value['created_at']?></td>
        <!--        <td align="center">
                         <?=$value['status']?>
                </td>-->
                <td width="5%" nowrap>
                    <a href="<?= $admin_path?><?=$controller?><?=$value['id']?>/active/" onclick="ajax_act(this);return false;"><img src="/static/admin/images/act_<?if($value['status']):?>green<?else:?>red<?endif;?>.png" alt="active" title="Active"/></a>&nbsp;
                    <a target="_blank" href="<?= $admin_path?><?=$controller?><?=$value['id']?>/load" onclick="ajax_load(this);return false;"><img src="/static/admin/images/ico_preview.gif" alt="to the site" title="<?php echo I18n::get('View on site')?>"/></a>&nbsp;
                    <a href="<?= $admin_path?><?=$controller?><?=$value['id']?>/delete/" onclick="ajax_del(this);return false;"><img src="/static/admin/images/del.png" alt="delete" title="<?php echo I18n::get('Delete')?>"/></a>&nbsp;
                </td>
            </tr>

        <?if( isset($rows[$value['id']]) ):?>
            <?foreach($rows[$value['id']] as $kkey => $vvalue):?>
                    <tr class="{iteration is odd?'odd':'even'}">
                        <td width="1%">&nbsp;</td>
                        <td align="center"><input type="checkbox" name="chk[<?=$vvalue['id']?>]" vvalue="<?=$vvalue['id']?>" class="listCheckbox"/></td>
                        <td align="center">
                            <?= $installed_modules[$vvalue['module_id']]['name']?>
                        </td>
                        <td><?= htmlentities($vvalue['comment'])?></td>
                        <td align="center" class="nowrap"><?=$vvalue['created_at']?></td>
                <!--        <td align="center">
                                 <?=$vvalue['status']?>
                        </td>-->
                        <td width="5%" nowrap>
                            <a href="<?= $admin_path?><?=$controller?><?=$vvalue['id']?>/active/" onclick="ajax_act(this);return false;"><img src="/static/admin/images/act_<?if($vvalue['status']):?>green<?else:?>red<?endif;?>.png" alt="active" title="Active"/></a>&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--                            <a target="_blank" href="<?= $admin_path?><?=$controller?><?=$vvalue['id']?>/load" onclick="ajax_load(this);return false;"><img src="/static/admin/images/ico_preview.gif" alt="to the site" title="<?php echo I18n::get('View on site')?>"/></a>&nbsp;-->
                            <a href="<?= $admin_path?><?=$controller?><?=$vvalue['id']?>/delete/" onclick="ajax_del(this);return false;"><img src="/static/admin/images/del.png" alt="delete" title="<?php echo I18n::get('Delete')?>"/></a>&nbsp;

                        </td>
                    </tr>

            <?endforeach;?>
            <tr><td colspan="6">&nbsp;</td></tr>
        <?endif;?>

    <?endforeach;?>
    <?//endforeach;?>
</table>
 </div>

<!--jqmodal-->
        <div class="hidden jqmWindow" id="jqmWindow">
          <p class="tCenter"><img src="/static/admin/images/preloader.gif" alt=""/></p>
        </div>
        <!--end jqmodal-->
<!--jqmodal-->
<link rel="stylesheet" href="/vendor/jquery/jqmodal/jqModal.css" />
<script type="text/javascript" src="/vendor/jquery/jqmodal/jqModal.js"></script>
<!--jqmodal-->
<script type="text/javascript">
    function load_all()
    {
        $.get(
                '<?= $admin_path?><?=$controller?>index/',
                function(data)
                {
                    $('#data').html(data);
                }
            );
    }

    function ajax_act(thisObj)
    {
        $.get(
            thisObj.href,
            function(data)
            {                
                if( $(thisObj).children('img').attr('src').indexOf('red') == -1)
                    $(thisObj).children('img').attr('src', '/static/admin/images/act_red.png')
                else
                    $(thisObj).children('img').attr('src', '/static/admin/images/act_green.png')
            }
        );
    }
    function ajax_del(thisObj)
    {
        $.get(
            thisObj.href,
            function(data)
            {
                if( data == 1)
                    $(thisObj).parents('tr').remove();
            }
        );
    }

    function ajax_load(thisObj)
    {
        var obj = $('#jqmWindow').remove();
        $('.container').after(obj);
        $('#jqmWindow').jqm();
        $.get(
            thisObj.href,
            function(data)
            {
                $('#jqmWindow').html(data).css({ marginLeft:-$('#jqmWindow').width()/2+'px'})
                $('#jqmWindow').jqmShow();
            }
        );
    }

    function formSave()
    {
        var form = $('#commentForm');
        var data = form.serialize();
        $.post(
            form.attr('action'),
            data,
            function(data)
            {
                $('#jqmWindow').jqmHide();
                if(data == 1)
                    load_all();
            }
        );
    }
</script>
<style>
    .jqmWindow{background: white; padding: 10px}
</style>