    <link href="/static/admin/css/jquery.date_input.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/vendor/jquery/jquery.date_input.min.js"></script>
    <script type="text/javascript" src="/static/admin/js/edit.js"></script>
    <link rel="stylesheet" type="text/css" href="/static/admin/css/paginator.css" />
    <script type="text/javascript" src="/static/admin/js/list.js"></script>
    <script type="text/javascript" src="/vendor/jquery/jquery.paginator.js"></script>
    
    <script type="text/javascript">
	function nextPage(page_id)
	{
	    current_page = parseInt($(page_id).val());

	    if(current_page < $('#total_pages').val())
		$(page_id).val( current_page+1 );

	    return current_page;
	}

	function prevPage(page_id)
	{
	    current_page = parseInt($(page_id).val());

	    if(current_page > 1)
		$(page_id).val( current_page-1 );

	    return current_page;
	}
    </script>
    
    <div class="wrap flRight">
          <div class="precontent"></div>
	  <form method="post" action="<?=$admin_path.$controller?>/{$module_name}/">

          <div class="rightCol ">
<!-- MODULE HEAD -->
              <div class="pageBlock {if !$show_module_desc} hidden{/if}" id="addBox">
                      <div class="topTitle rel">{$module.title}<a href="<?=$admin_path?>" class="close"><img src="/static/admin/images/close.gif" alt=""/></a></div>
                      <div class="topBlockBody">
                          <img src="/static/admin/images/moduleIco.jpg" alt="" class="flLeft" width="76"/>
                          <p class="module_descr_short">{$module.descr_short}<br/><a href="#" class="showDescrFull">{php}echo I18n::get('Show full'){/}</a> </p>
                          <p class="module_descr_full hidden">{$module.descr_full}<br/><a href="#" class="showDescrShort">{php}echo I18n::get('Show short'){/}</a> </p>
                          <div class="clear"></div>
                      </div>
              </div>
              <div class="breadcrumbs">
                  <p class="flLeft"><a href="<?=$admin_path?>">{php}echo I18n::get('Content'){/}</a> &gt; {$module.title}</p>
                  <p class="flRight"><a href="<?=$admin_path?>"
			class="showHelp {if $show_module_desc} hidden{/if}">{php}echo I18n::get('Show help'){/}</a>&nbsp;<a href="#"
			class="addShortcut">{php}echo I18n::get('Add to &quot;shortcuts&quot;'){/}</a></p>
                  <div class="clear"></div>
              </div>
<!-- /MODULE HEAD -->

              <div class="pageBlock noPadding">
                  <div class="topTitle cont rel">{$module.title}</div>
                  <div class="fancy rel">
                      <div class="contentNavBtns" id="contentNavBtns">
                          <div class="inner">
                              <div style="width:280px;margin:0 auto">
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  {php}echo I18n::get('With selected'){/}
                                  <select id="list_action">
                                      <option value="delete">{php}echo I18n::get('Delete'){/}</option>
                                  </select>
                                  <button class="btn formUpdate" type="button"><span><span>{php}echo I18n::get('Start'){/}</span></span></button>
			          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </div>
                              <div class="clear"></div>
                          </div>
                      </div>
                  </div>

                  <div class="innerGray">
		      <br/>
                      <table width="100%" class="contentTab" cellpadding="0" cellspacing="0">
			  <tr>
			      <td width="20"> </td>
			      <td class="nowrap">
				  <input type="hidden" id="total_pages" value="{$total_pages}" />
				{php}echo I18n::get('Page'){/} <button class="btn formFilter" type="button" onclick="prevPage('#page');;"><span><span>&lt;</span></span></button>
				<input type="text" id="page" name="page" value="{$quicky.request['page']|default:'1'}" style="width: 3em; text-align: center;" />
				<button class="btn formFilter" type="button" onclick="nextPage('#page');"><span><span>&gt;</span></span></button>
				{php}echo I18n::get('of'){/} {$total_pages|default:'1'}.
			      </td>
			      <td class="nowrap">
				{php}echo I18n::get('Show'){/}
                                {select id="rows_num" name="rows_num" style="width: 3em; text-align: left;" value=$itemsPerPage}
				    {option value=10 text='10'}
				    {option value=25 text='25'}
				    {option value=50 text='50'}
				    {option value=100 text='100'}
				{/select} {php}echo I18n::get('rows'){/}.
			      </td>
			      <td width="33%" align="right"  class="nowrap">
				&nbsp;
			      </td>
			      <td> </td>
			  </tr>
		      </table>
		      <br/>
                      <table width="100%" class="contentTab" id="contentTab" cellpadding="0" cellspacing="0">
                          <tr class="first">
                              <th class="first">&nbsp;</th>
                              <th width="2%"><input type="checkbox" name="" value="" style="width:auto;" class="listCheckboxAll"/></th>
                              <th width="13%">{php}echo I18n::get('Module'){/}</th>
                              <th >{php}echo I18n::get('Comment'){/}</th>
                              <th width="15%">{php}echo I18n::get('Date'){/}</th>
                              <th width="15%">{php}echo I18n::get('Status'){/}</th>
                              <th width="5%">{php}echo I18n::get('Rating'){/}</th>
                              <th class="last" width="5%">&nbsp;</th>
                          </tr>
                          <tr>
                              <th colspan="4">&nbsp;</th>
                              <th align="right">
                                      {php}echo I18n::get('From'){/}
                                      <input type="text" name="dateFrom" class="date" value="{$filter.dateFrom|escape:'html'}" />
                                  <br />
                                      {php}echo I18n::get('To'){/}
                                      <input type="text" name="dateTo" class="date" value="{$filter.dateTo|escape:'html'}" />
                              </th>
                              <th>
                                  <select name="status">
                                      <option value="-1">---</option>
                                      <option value="0"{if $filter.status !== '' && $filter.status == 0} selected = "selected"{/if}>{i18n('Inactive')}</option>
                                      <option value="1"{if $filter.status == 1} selected = "selected"{/if}>{i18n('Active')}</option>
                                  </select>
                              </th>
                              <th></th>
                              <th class="last">
                                  <button class="btn formFilter" type="button">
                                      <span><span>
                                          {i18n('Show')}
                                      </span></span>
                                  </button>
                              </th>
                              
                          </tr>
                        {foreach from=$rows value=$comments name="comments"}
                        {foreach from=$comments key="key" value=$value}
			  <tr class="{iteration is odd?'odd':'even'}">
                              <td width="1%">&nbsp;</td>
                              <td align="center"><input type="checkbox" name="chk[{$value.id}]" value="{$value.id}" class="listCheckbox"/></td>
                              <td align="center">
                                  {$installed_modules[$value.module_id]['name']}
                              </td>
                              <td>{$value.comment|html:'escape'}</td>
                              <td align="center" class="nowrap">{$value.created_at}</td>
                              <td align="center">
				 {$value.status}
			      </td>
                              <td align="center">{$value.rating}</td>
                              <td width="5%" nowrap>
				    <a href="<?=$admin_path?>{$controller}/{$module_name}/{$value.id}/active/" ><img src="/static/admin/images/{if $value.status}act{/else}inact{/}.gif" alt="active" title="Active"/></a>&nbsp;
				    <a target="_blank" href="{get_url_by_id($installed_modules[$value.module_id]['fs_name'], $value.object_id)}#comment{$value.id}" ><img src="/static/admin/images/tosite.png" alt="to the site" title="{php}echo I18n::get('View on site'){/}"/></a>&nbsp;
				    <a href="<?=$admin_path?>{$controller}/{$module_name}/{$value.id}/delete/" ><img src="/static/admin/images/del.png" alt="delete" title="{php}echo I18n::get('Delete'){/}"/></a>&nbsp;

			      </td>
                          </tr>
                        {/foreach}
                        {/foreach}
                      </table>
                  </div>


	<script language="javascript" type="text/javascript">
        {literal}
            $(document).bind('ready', function ()
            {
                var page = /page=([^#&]*)/.exec(window.location.href);
                page = page ? page[1] : 1;
                $('#paginator').paginator(
                {
                    pagesTotal: {/literal}{$total_pages}{literal}
                    ,pagesSpan: 10
                    ,pageCurrent: page
                    ,baseUrl: '?page='
                    ,lang: {
                {/literal}
                        next  : "{php}echo I18n::get('Next'){/}",
                        last  : "{php}echo I18n::get('Last'){/}",
                        prior : "{php}echo I18n::get('Prior'){/}",
                        first : "{php}echo I18n::get('First'){/}",
                        arrowRight : String.fromCharCode(8594),
                        arrowLeft  : String.fromCharCode(8592)
                  {literal}
                    }
                });
            });
        {/literal}
	</script>
		  {if $total_pages && $total_pages>1}
		    <div class="paginator" id="paginator"></div>
		  {/}

                  <div class="clear"></div>
                  
              </div>
	      <p class="tRight"><a href="<?=$admin_path?>">{php}echo I18n::get('Go to start page'){/}</a></p>
          </div>
	  </form>
          <div class="clear"></div>
      </div> <!-- content -->
