<?php defined('SYSPATH') or die('No direct script access.');

Route::set
        (
	    'questions',
	    'admin/questions(/<id>(/<action>)(/<path>))',
	    array(
            'id' => '[0-9]*',
            'action' => '[^/]*',
            )
        )
        ->defaults
        (
            array(
		    'controller'    => 'questions',
		    'id'	    => NULL,
		    'action'        => 'index',
            )
	)
;

// Frontned Routes
Route::set
        (
	    'questions_front',
	    '(<lang>/)questions/<moduleId>/<objectId>(/<action>)',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_questions',
		    'action'        => 'view',
		    'moduleId'	    => 0,
		    'objectId'	    => 0,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
            )
	)
;