<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Questions extends Controller_AdminModule {

    public $template		= 'comments_list';

    public $module_title	= 'Questions';
    public $module_name	= 'questions';
    public $module_desc_short   = 'Comments Descr Short';
    public $module_desc_full	= 'Comments Descr Full';

    public function before()
    {
//        die(Kohana::debug($this->request, Request::instance()));
//        if ($this->request == Request::instance()) {
//            throw new Exception('module could be started not from here');
//        }
        parent::before();
        $this->auto_wrapper = false;
        $this->_model = new Model_Frontend_Questions();        
    }

    public function action_index()
    {
        $where = array();        
        if(Request::$method == 'POST')
        {
            $itemsPerPage = intval($_POST['rows_num']) > 10 ? intval($_POST['rows_num']) : 10;
            setcookie('bcommentsRowsPerPage', $itemsPerPage);
            $filter = $this->validData($_POST);
            if($filter !== false)
            {
                setcookie('commentsFilterData', json_encode($filter), time()+604800);
            }
        }
        else
        {

            $filter = Arr::get($_COOKIE, 'commentsFilterData', '');
            $filter = json_decode($filter, true);

            if( !is_array($filter) || !$this->validData($filter))
            {
                $filter = array();
            }

            $itemsPerPage = intval( Arr::get( $_COOKIE, 'bcommentsRowsPerPage', ''));

        }

        $this->template->filter = $filter;

        $where = $this->_model->getFilterWhere($filter);        

        $totalRows = $this->_model->count($where);        
        $itemsPerPage = $itemsPerPage > 10 ? $itemsPerPage : 10;
        
        if(isset($_POST['page']) && intval($_POST['page']) > 0)
        {
            $currentPage = intval($_POST['page']);
        }else{
            if(isset($_GET['page']) && intval($_GET['page']) > 0)
                $currentPage = intval($_GET['page']);
            else $currentPage = 1;
        }


        $this->itemsPerPage = $itemsPerPage;
        $this->currentPage = $currentPage;

        $paginator = Pagination::factory(
                array(
                    'current_page'   => $currentPage,
                    'total_items'    => $totalRows,
		    'items_per_page' => $itemsPerPage
                ))
                ;

        $this->total_pages = $paginator->total_pages;
        $this->items_per_page = $paginator->items_per_page;

        $comments = $this->_model->find($where, $paginator->items_per_page, $paginator->offset);
        
        $this->template->rows = $comments;
    }

    public function action_active()
    {
        $id = intval($this->request->param('id'));
        if( !$id ) return;

        $data = array(
                'status' => DB::expr("!status")
            );
        
        $this->_model->update($data, $id);
        die(print($data['status']));
//        $this->redirect_to_controller($this->request->controller);
    }

    public function action_delete()
    {
        if(Request::$method == 'POST')
        {
            $id = $_POST['chk'];
            array_map('intval', $id);
        }else $id = intval($this->request->param('id'));

        if( !$id ) return;

        $this->_model->delete( $id );
        die(print(1));
//        $this->redirect_to_controller($this->request->controller);
    }

    public function action_load()
    {
        if( !$id = $this->request->param('id') ) return;

        $this->template = View::factory('comment');

        $obj = $this->_model->find( array(array('id','=',$id)) );
        reset($obj);
        $obj = current($obj);
        reset($obj);
        $obj = current($obj);
        
        $this->template->obj = $obj;
        $parent_obj = $this->_model->find( array(array('parent_id','=',$id)) );
        
        if(is_array($parent_obj) && count($parent_obj) > 0)
        {
            reset($parent_obj);
            $parent_obj = current($parent_obj);
            reset($parent_obj);
            $parent_obj = current($parent_obj);

            $this->template->parent_obj = $parent_obj;
        }

        $this->auto_wrapper = false;

    }

    public function validData($data)
    {
        $keys = array('dateFrom', 'dateTo', 'status');

        $data = Arr::extract($data, $keys);

        $validator = Validate::factory($data)
                            ->rule('dateFrom', 'date')
                            ->rule('dateTo', 'date')
                            ->rule('status', 'range', array(-1, 1));
        if( $validator->check() )
        {
            return $data;
        }
        else
        {
            return false;
        }
    }

    public function action_add()
    {
        $oQuestions = new Model_Questions();
        $oQuestions->delete(array('parent_id', '=', $_POST['parent_id']) );

        $parent_comment = $_POST['parent_comment'];
        unset($_POST['parent_comment']);
        $res = $this->_model->insert($_POST);
        $res = $this->_model->update(array('parents_path' => $_POST['parent_id'].','.$res['lastInsertId'].',' ), $res['lastInsertId'] );
        $res = $this->_model->update(array('status' => 1,'comment'=>$parent_comment), $_POST['parent_id']);
        die('1');
    }
}
