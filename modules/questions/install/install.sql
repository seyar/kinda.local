DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `module_id` int(10) unsigned NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(1) unsigned NOT NULL default '1',
  `rating` smallint(5) NOT NULL default '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `module_id` (`module_id`,`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
