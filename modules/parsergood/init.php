<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */

Route::set
        (
	    'parser',
	    'admin/parser((/<id>)/<action>)(/<path>)',
	    array( 'id' => '\d{1,10}',  )
        )
        ->defaults
        (
            array(
		    'controller'    => 'parser',
		    'id'	    => NULL,
		    'action'        => 'index',
                )
	)
;
?>