<?php
defined( 'SYSPATH' ) OR die( 'No direct access allowed.' );
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */

class Controller_Parser extends Controller_AdminModule
{
    public $template = 'parser';
    public $module_title = 'Parser';
    public $module_name = 'parser';
    public $module_desc_short = 'Shop Descr Short';
    public $module_desc_full = 'Shop Descr Full';
    public $parseFrom = '';
    public $parser;
    

    public function before()
    {
        parent::before();        
        $this->model = new Model_Parser();
        $this->parseFrom = Cookie::get( 'parseFrom_' . MODULE_NAME, 'yandex' );
        
        if( isset( $_REQUEST['parseFrom'] ) && !empty( $_REQUEST['parseFrom'] ) )
        {
            $this->parseFrom =  $_REQUEST['parseFrom'];
            cookie::set( 'parseFrom_' . MODULE_NAME, $_REQUEST['parseFrom'] );            
        }
        
        $class = 'Model_'. ucfirst($this->parseFrom) .'parser';
        $this->parser = New $class();
            
        $this->template->parseFrom = $this->parseFrom;
    }

    public function action_index()
    {
        $this->template->rows = Model_Categories::categories_folders( $this->lang, 1 );
    }

    public function action_parse( $page = NULL )
    {
        $url = Arr::get($_GET, 'url', Arr::get($_POST,'byurl',''));
        $_GET['query'] = $_REQUEST['query'];
        
        if( !$url )
        {
            if( !isset($_GET['query']) || empty($_GET['query']) ){ die( json_encode(array('errors'=>'1', 'ret'=>'Не задан критерий поиска')) ); }
            $url = $this->parser->getParseURL().urlencode( mb_convert_encoding($_GET['query'],'CP1251','UTF-8') );
        }
        
        $return = array();
        $return['errors'] = '1';
        $return['ret'] = 'Готово';
        $return['parseUrl'] = $url;
        
        if( $_GET['dontparse'] == 1 )
        {
            $return['ret'] = 'из списка';
            die( json_encode($return) );
        }
        
        $_GET['id'] = $_REQUEST['id'];
        //сначала сделаем все товары котрые будут парсится анверифайд, а потом их статус изменится
        Model_Parser::update( array('status' => 'unverified'), array(array('id','=', $_GET['id'])) );
        if( !isset($page) && empty($page) )
        {
            if( !strstr($url,'model-spec.xml') ) $url = str_replace( 'model.xml', 'model-spec.xml', $url );
            $page = Model_Parser::old_curl_get_contents( $url );
        }
        
        $goodparams = $this->parser->getParams( $page,$url );
        
        $return['parseUrl'] = Model_Content::arrGet($goodparams, 'url', $url);

        $params = FALSE;
        if( isset($goodparams['params']) )
            $params = $goodparams['params'];
        
        if( $params == FALSE && strstr($page,'captcha') )
        {
            // ban captcha           
            $return['ret'] = $this->parser->getCaptchaHtml( $page, $url );
            die( json_encode($return) );
        }
        elseif( $params == FALSE )
        {            
            $return['ret'] = !empty(Model_Parser::$errors) ? Model_Parser::$errors : 'Параметры не найдены';
            die( json_encode($return) );
        }

        if( $params != FALSE && is_array($params) )
        {
            $res = Model_Parser::saveParams( $params, $_GET['id'] );
        }

        $res = $this->parser->getPhotos( $page, $_GET['id'] );
        
        if( Model_Parser::$errors )
        {
            $return['ret'] = Model_Parser::$errors;
            die( json_encode($return) );
        }
        else
        {
            Model_Parser::updateGood( $_GET['id'] );
            DB::update('shop_goods')->set(array('parsed' => 1))->where('id','=',$_GET['id'])->execute();
            $return['errors'] = '1';
            die( json_encode($return) );
        }
    }

    public function action_getListGoods()
    {        
        return die( json_encode( Model_Parser::getGoods( $this->request->param( 'id' ), $_GET['reparse'], isset($_GET['byScu']) ) ) );
    }

    public function action_renameGood()
    {
        return die( json_encode( Model_Parser::renameGood( $this->request->param( 'id' ), $_POST['name'] ) ) );
    }

    function action_parseParams()
    {        
        $res = $this->parser->parseParams( $this->request->param( 'id' ), $_GET['parseUrl'], $_GET['overwrite'] );
        
        if( Model_Parser::$errors ) die(  json_encode( Model_Parser::$errors ));
        return die( json_encode( $res ) );
    }

    function action_sendCaptcha()
    {
//        Model_Parser::$errors = '';
        

        $page = $this->parser->sendCaptcha( $this->request->param('id') );
//      Kohana_log::instance()->add('d', print_r($page,1));
//      die(print_r($page,1));
//        $return = '1';
        $this->action_parse();
//        $params = $this->parser->getParams( $page );
//
//        if( $params == FALSE && strstr( $page, 'Ограничение доступа' ) )
//        {
//            //yandex ban
//            $return = $this->parser->getCaptchaHtml( $page );
//            return die( $return );
//        }
//
//        $this->parser->getPhotos( $page, $this->request->param( 'id' ) );
//
//        if( $params != FALSE && is_array( $params ) )
//            Model_Parser::saveParams( $params, $this->request->param( 'id' ) );


//        if( Model_Parser::$errors )
//            die( json_encode(Model_Parser::$errors ) );
//        else
            return ;
    }

    function action_sendCaptcha2()
    {
        $page = $this->parser->sendCaptcha( $this->request->param( 'id' ) );
        
        $this->action_parseParams();
        die();
    }

}

?>