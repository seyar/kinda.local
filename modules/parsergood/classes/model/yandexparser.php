<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */
class Model_Yandexparser extends Model_Parser
{
    function __construct()
    {
        $this->parseUrl = 'http://market.yandex.ua';
        include('Zend/Dom/Query.php');
    }
    
    public function getParseURL()
    {
        return $url = $this->parseUrl . '/search.xml?cvredirect=1&text=';
    }
    
    static function getParams( $page = NULL, $url = NULL )
    {
        if( !$page ){ Model_Parser::$errors .= 'page null'.__FUNCTION__; return false; }
        
        // get good params
//        Kohana_Log::instance()->add('d', $page);
        
        /* fix new yandex html */
        if( !strstr($url,'model-spec.xml') ) $url = str_replace( 'model.xml', 'model-spec.xml', $url );
        $dom = new Zend_Dom_Query($page);
        
        $params = array();
        $values = array();
                
        foreach( $dom->query('table.b-properties th') as $item ) // промежуточная страница поиска.
        {   
            if( !$item->getAttribute('colspan') )
                $params[] = trim( utf8_decode($item->nodeValue) );
        }
        
        foreach( $dom->query('table.b-properties td') as $item ) // промежуточная страница поиска.
        {   
                $values[] = trim( utf8_decode($item->nodeValue) );
        }
        
        if( !isset($params) || count($params) == 0 || (count($params) != count($values)) )
        {
            Model_Parser::$errors .= 'Параметры не найдены на яндексе';
            return false;
        }
        
        return array( 'url' => $url, 'params' => array_combine($params,$values) );
        
    }

    static function getPhotos($page = NULL, $good_id = NULL)
    {
        if( !isset($page) || empty($page) ){Model_Parser::$errors .= 'page null'.__FUNCTION__; return false; }
        if( !isset($good_id) || empty($good_id) ){Model_Parser::$errors .= 'good_id null'.__FUNCTION__; return false; }

        // get photos
        $photos = array();
        preg_match('|td[^<]+bigpic(.+)<\/td|Uis', $page, $match);
        if( isset($match) && count($match) )
        {
            $photos = self::copyImages( $match[1], $good_id, 'main' );
            self::setDefaultPhoto($photos, $good_id);
        }
            
        preg_match('|td[^<]+smallpic(.+)</td|Uis', $page, $match);
        if( isset($match) && count($match) )
        {
            $photos2 = self::copyImages( $match[1], $good_id );

            if( is_array($photos) && is_array($photos2) )
                $photos = array_merge($photos, $photos2);
        }

        return $photos;
    }

    static function getCaptchaHtml( $page = NULL )
    {
        if( !isset($page) || empty($page) ){Model_Parser::$errors .= 'page null'.__FUNCTION__; return false; }

        preg_match('|form[^>]+>(.+)response">|Uis', $page, $match);
        $t = str_replace('input', 'input onkeyup="if(event.keyCode == 13) captchaSend( $(this).next() );return false;" ', $match[1]);
        $t = '<div class="captchaForm">'.$t.'response"><button class="btn" type="button" onclick="captchaSend(this);return false;"><span><span>Отправить капчу</span></span></button></div><br />';

        return $t;
    }

    static function copyImages( $html = NULL, $good_id = NULL, $main = NULL )
    {
        $return = array();

        $destination = DOCROOT.'/static/media/shop/'.$good_id.'/';

        if( !is_string($html) /*|| $destination == NULL */) return false;
        preg_match_all('|href="([^<]+)"|Uis', $html, $match);
        if( count($match[1]) == 0 ) preg_match_all('|src="([^<]+)"|Uis', $html, $match);

        if( !file_exists($destination) ) mkdir($destination, 0755, true);
        $t_width = Kohana::config('shop')->image_prev_width;
        $t_height = Kohana::config('shop')->image_prev_height;
        foreach( $match[1] as $key => $item)
        {
//            for($i=0;$i<5;$i++)
//            {
//                $imageurl = $i == 0 ? $item : urldecode($item.'&size='.$i);
                $targetFile = $destination.$good_id.$key.$main.'.jpg';
                copy($item, $targetFile);
                $return[$key] = $targetFile;
//            }

            $image = Image::factory( $targetFile );
            $image->resize($t_width, $t_height, Image::AUTO)
                            ->crop($t_width, $t_height);
            $image->save(dirname($targetFile).'/prev_'.basename($targetFile), 90);
        }

        return $return;
    }

    static function parseParams( $toCatid, $url, $overwrite = 0,$page = NULL )
    {        
        $error = 0;
        //if( !$query ) $error = 'Нет запроса';        
        $page = (!$page) ? self::old_curl_get_contents( $url ) : $page;
        // get good params
        $dom = new Zend_Dom_Query($page);
        
        $params = array();
        $values = array();
                
        foreach( $dom->query('table.b-properties th') as $item ) // промежуточная страница поиска.
        {   
            if( !$item->getAttribute('colspan') )
                $params[] = trim( utf8_decode($item->nodeValue) );
        }
        
        foreach( $dom->query('table.b-properties td') as $item ) // промежуточная страница поиска.
        {   
                $values[] = trim( utf8_decode($item->nodeValue) );
        }
        
        if( count($params) == 0 && strstr($page, 'Ограничение доступа') )
        {
            preg_match('|form[^>]+>(.+)response">|Uis', $page, $match);
            $t = str_replace('input', 'input onkeyup="if(event.keyCode == 13) captchaSend2( $(this).next() );return false;" ', $match[1]);
            $t = '<div class="captchaForm">'.$t.'response"><button class="btn" type="button" onclick="captchaSend2(this);return false;"><span><span>Отправить капчу</span></span></button></div>';
            return $t.'<br />';
        }
        
        if( (count($values) > 0 && count($params) > 0) )
        {            
            $params = array_combine($params,$values);
            return self::copyParamsFrom($params,$toCatid, $overwrite);
        }
        else
        {
            Model_Parser::$errors .= 'А параметров нет.';
            return false;            
        }
        
        return self::copyParamsFrom($params,$toCatid, $overwrite);
    }

    function sendCaptcha( $id = NULL, $data = array() )
    {
        $retpath = (isset( $_REQUEST['url'] ) && !empty( $_REQUEST['url'] ) ) ? str_replace( $this->parseUrl, '', $_REQUEST['url'] ) : $_GET['retpath'];
        $data = array( 'key' => $_REQUEST['key'], 'response' => $_REQUEST['response'], 'retpath' => $retpath );
        
        $url = $this->parseUrl.'/captcha/check-captcha.xml?'.http_build_query($data);
        
        
//        $data['retpath'] = '';
        $page = self::old_curl_get_contents( $url );
        //Kohana_Log::instance()->add('parser',Kohana::debug( $url, http_build_query($_POST),$_POST, html_entity_decode($page) ));
        
        return $page;
    }
}
?>