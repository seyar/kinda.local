<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */
class Model_Hotlineparser extends Model_Parser
{
    function __construct()
    {
//        include('simple_html_dom.php');
        include('Zend/Dom/Query.php');
        $this->parseUrl = 'http://hotline.ua';
    }
    
    function getParseURL()
    {
        return $url = $this->parseUrl.'/sr/?table=193-3-314-315&q=';
    }
    
    function getParams( $page = NULL )
    {
        //if( !strstr($page, 'search_pre_results') ) return array();
        if( !isset($page) || empty($page) ){ Model_Parser::$errors .= 'page null'; return false; }
        $url = '';
        if( !strstr($page,'tx_full') )
        {
            if( !$goodpage = $this->_getGoodPage($page) ){ return false; }
            
            $url = $goodpage['url'];
            $page = $goodpage['page'];
        }
        else
        {            
            $page = mb_convert_encoding($page, 'UTF-8', 'CP1251');                        
        }
        
//        list($url, $page) = $this->_getGoodPage($page);
        
        $dom = new Zend_Dom_Query($page);
        $dom->setDocument($page);
//        Kohana_log::instance()->add('d',print_r(mb_detect_encoding($page),1));
        $params = array();
        $values = array();
        foreach( $dom->query('table.tx_full td') as $item ) // промежуточная страница поиска.
        {
            if( $item->getAttribute('class') == 'op' )
                $params[] = trim( utf8_decode( $item->nodeValue ));
            else
                $values[] = trim( utf8_decode( $item->nodeValue ));
        }
        
//        Kohana_Log::instance()->add('d',print_r(array_combine($params, $values), 1) );
        if( count($params) != 0 && count($values) != 0 && count($values) == count($params) )
        {            
            return array( 'url' => $url, 'params' => array_combine($params, $values) );
        }else{
            Model_Parser::$errors .= 'Параметры не найдены на '.$this->parseUrl;
            return false;
        }
    }

    function getPhotos($page = NULL, $good_id = NULL)
    {
        if( !isset($page) || empty($page) || !isset($good_id) || empty($good_id)){Model_Parser::$errors .= 'page null'; return false; }
        
        if( !strstr($page,'kadr') )
        {
            $goodpage = $this->_getGoodPage($page);
            $page = $goodpage['page'];
        }
        
        $dom = new Zend_Dom_Query($page);
        $links = '';
        
        foreach( $dom->query('.kadr a') as $item )
        {
            $links[] = $item->getAttribute('href');            
        }
        
        if( !is_array($links) || count($links) == 0 ) return array();
        // get photos
        ;
        $photos = self::copyImages( array(array_shift($links)), $good_id, 'main' );
        self::setDefaultPhoto($photos, $good_id);
        
        if( count($links) != 0 ) 
            $photos2 = self::copyImages( $links, $good_id );

        if( is_array($photos) && isset($photos2) && is_array($photos2) )
            $photos = array_merge($photos, $photos2);

        return $photos;
    }
    
    /**
     * Function get search page redirect to the good page and return
     * @param string $page 
     */
    protected function _getGoodPage( $page )
    {  
        $page = mb_convert_encoding($page, 'UTF-8','cp1251' );
        
        preg_match('|<p id="crumbs">(.*)<div class="fixed-right"></div>|Uis', $page, $match);
        if( count($match) == 0 ){ Model_Parser::$errors .= 'Не найдено товара на странице поиска'; return false; }
        $dom = new Zend_Dom_Query($match[0]);
        /**/
        $links = '';
        foreach ($dom->query('a') as $item)
        {
            $t = $item->getAttribute('href');
            if( $t != '/' )
            {
                $links = $t; // находим первую ссылку и переходим по ней. 
                break;
            }
        }
        if( empty($links) )
        {
            Model_Parser::$errors .= 'Не найдено товара на странице поиска';
            return false;
        }
        /**/
        $url = $links;
        $page = parent::old_curl_get_contents( $links );
        $dom->setDocument($page);

        $links = '';
        foreach( $dom->query('.link_info') as $item ) // промежуточная страница поиска.
        {
            if( $links = $item->getAttribute('href') ) break;
        }
        /**/
        
        $page = parent::old_curl_get_contents( $this->parseUrl.$links );        // страница с товаром        
        $page = mb_convert_encoding($page, 'UTF-8','cp1251' );
        return array( 'url' => $this->parseUrl.$links, 'page' => $page );
    }

    function getCaptchaHtml( $page = NULL, $url = NULL, $forGood = TRUE )
    {
        if( !isset($page) || empty($page) ){ Model_Parser::$errors .= 'page null'; return false; }
        
        $dom = new Zend_Dom_Query($page);
        
        $links = '';        
        
        foreach( $dom->query('table img') as $item )
            $links = trim(utf8_decode($item->getAttribute('src')));
        
        $return = '';
        $return .= "<img src='{$this->parseUrl}$links' alt='' class='vMiddle'/>&nbsp;";
        if( $forGood )
        {
            $return .= '<input style="width: 70px;" value="" name="number" onkeyup="if(event.keyCode == 13) captchaSend( $(this).next() );return false;">&nbsp;';
            $return .= '<button class="btn" type="button" onclick="captchaSend(this);return false;"><span><span>Отправить captcha</span></span></button>';
            $return .= '<input type="hidden" name="byurl" value="'.$url.'">';
        }
        else
        {
            $return .= '<input style="width: 70px;" value="" name="number" onkeyup="if(event.keyCode == 13) captchaSend2( $(this).next() );return false;">&nbsp;';
            $return .= '<button class="btn" type="button" onclick="captchaSend2(this);return false;"><span><span>Отправить captcha</span></span></button>';
        }
        
        return $return;
    }

    /**
     * Function save and crop images to file system
     * 
     * @param array $photos
     * @param int $good_id
     * @param string $main suffix file name
     * @return array of filenames 
     */
    static function copyImages( $photos = NULL, $good_id = NULL, $main = NULL )
    {
        $return = array();

        $destination = DOCROOT.'/static/media/shop/'.$good_id.'/';
        
        if( !file_exists($destination) ) mkdir($destination, 0755, true);
        $t_width = Kohana::config('shop')->image_prev_width;
        $t_height = Kohana::config('shop')->image_prev_height;
        
        foreach( $photos as $key => $item)
        {
            $name = substr( basename($item), 0, strpos( basename($item), '.' )); // режем по первой точке
            $targetFile = $destination.$good_id.$name.$main.'.jpg';
            copy($item, $targetFile);
            $return[$key] = $targetFile;

            $image = Image::factory( $targetFile );
            $image->resize($t_width, $t_height, Image::AUTO)
                            ->crop($t_width, $t_height);
            $image->save(dirname($targetFile).'/prev_'.basename($targetFile), 90);
        }

        return $return;
    }

    function parseParams( $toCatid, $url, $overwrite = 0,$page = NULL )
    {
        $error = 0;
        if( !$url ) $error = 'Нет запроса';
        $page = (!$page) ? self::old_curl_get_contents( $url ) : $page;        
        $page = mb_convert_encoding($page, 'UTF-8','cp1251' );
        $page = self::invalidCharacterFilter($page, TRUE);        
        
        // get good params
        if( strstr($page,'captcha'))
        {
            // ban captcha 
            $return = $this->getCaptchaHtml( $page, $url, FALSE );
            die( json_encode($return) );            
        }
        
        $dom = new Zend_Dom_Query($page);
//        Kohana_log::instance()->add('d',print_r($dom->getDocument(),1));
        $params = array();
        $values = array();
                
        foreach( $dom->query('table.tx_full td') as $item ) // промежуточная страница поиска.
        {            
            if( $item->getAttribute('class') == 'op' )
                $params[] = trim( utf8_decode($item->nodeValue) );            
            else
                $values[] = trim( utf8_decode($item->nodeValue) );
        }
        
        if( (count($values) > 0 && count($params) > 0) )
        {            
            $params = array_combine($params,$values);
            return self::copyParamsFrom($params,$toCatid, $overwrite);
        }
        else
        {
            Model_Parser::$errors .= 'А параметров нет.';
            return false;            
        }
    }

    function sendCaptcha( $id = NULL ) 
    {        
        $url = $this->parseUrl.'/sr/';

        $data['number'] = $_REQUEST['number'];
        $page = self::old_curl_get_contents( $url, $data );
        //Kohana_Log::instance()->add('parser', print_r($page,1) );
        
        return $page;
    }
    
    /**
     * Function delete invalid characters for Zend DOM Query
     * 
     * @param string $string 
     */
    static function invalidCharacterFilter($str, $cropTable = FALSE)
    {
        
        $str = preg_replace('|<img[^>]+/img/s/qmark4.gif[^>]+>|Uis', '', $str);
        if( $cropTable )
        {
            preg_match( '|<table[^>]+tx_full[^>]+>(.*)</table>|Uis',$str, $match);
            if(isset($match[0]))
                $str = $match[0];
        }
        $str = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
        return $str;
    }
    
}    
?>