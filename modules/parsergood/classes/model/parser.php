<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author ${fullName} ${email}
 */
class Model_Parser extends Model
{
    static public $errors = '';
    public $parseUrl = '';

    function old_getPage( $query = NULL, $good_id = NULL )
    {
        $error = 0;
        if( !$query ) $error = 'Нет запроса';
        $page = self::curl_get_contents(  );
        // get good params

        preg_match('|full-spec-cont(.+)</div></td>|Uis', $page, $match);
        preg_match_all('|td[^<]+label"><span>(.+)</span></td>|Uis', $match[0], $match);
        $params = $match[1];
        if( count($params) == 0 && strstr($page, 'Ограничение доступа ') )
        {
            
        }
        
        if( self::saveParams($params, $good_id) == -1) $error = 'Нет совпадений с базой ';


        if( $error === 0 )
            return 1;
        else
            die(print($error));
    }

    static function curl_get_contents( $url, $post = NULL )
    {
        $cookiefile = DOCROOT."static/media/cookiefile";
        
        $curl_config = get_object_vars(Kohana_Config::instance()->load('curl'));
        
        $curl = new Curl_Core( $curl_config );
        
        $curl->addOption(CURLOPT_COOKIEFILE, $cookiefile);
        $curl->addOption(CURLOPT_COOKIEJAR, $cookiefile);
        return $curl->get($url);
    }

    static function old_curl_get_contents( $url, $post=NULL )
    {
      if (!extension_loaded('curl'))
      {    $error  = 'Module php_curl.dll not connected.';
            Kohana_Log::instance()->add('curl error', die(Kohana::debug( $error )));
           return array(0, $error);
      }
      
      $session_id = (isset($session_id) && !empty($session_id) ) ? $session_id : session_id();
      $ch = @curl_init();
      
      curl_setopt($ch, CURLOPT_USERAGENT, self::fake_user_agent_http_get());
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
      curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
      curl_setopt($ch, CURLOPT_TIMEOUT, 300);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      
      curl_setopt($ch, CURLOPT_COOKIEFILE, DOCROOT."/static/media/cookiefile");
      curl_setopt($ch, CURLOPT_COOKIEJAR, DOCROOT."/static/media/cookiefile");
//      curl_setopt($ch, CURLOPT_COOKIE, session_name() . '=' . $session_id );
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

      if ($post)
      {    curl_setopt($ch, CURLOPT_POST, 1);
           curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      }

      curl_setopt($ch, CURLOPT_URL, $url);
      $result = curl_exec($ch);      
//      $result = mb_convert_encoding($result, 'UTF-8', 'CP1251' );
      
      if(curl_errno($ch)) die(Kohana::debug( curl_error($ch) ));

      curl_close($ch);
      
      preg_match('|<body(.*)</body>|Uis', $result, $match);
      
      $result = isset($match[0]) ? $match[0] : $result;
      
      return $result;
    }
    
    /**
     *
     * @return string browser useragent for curl 
     */
    static function fake_user_agent_http_get() 
    {
         $agents[] = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; Media Center PC 5.0)";
         $agents[] = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)";
         $agents[] = "Opera/9.63 (Windows NT 6.0; U; ru) Presto/2.1.1";
         $agents[] = "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5";
         $agents[] = "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.1.18) Gecko/20081203 Firefox/2.0.0.18";
         $agents[] = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.16) Gecko/20080702 Firefox/2.0.0.16";
         $agents[] = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-us) AppleWebKit/525.27.1 (KHTML, like Gecko) Version/3.2.1 Safari/525.27.1";

         return $agents[rand(0,(count($agents)-1))];
    }

    static function getGoods( $catid = 0, $reparse = FALSE, $byScu = FALSE )
    {
        $query = DB::select('g.*', array('g.scu','scu'), array('g.name','name') )
                ->from(array('shop_goods', 'g'))
                ->join(array('shop_categories','c'))
                    ->on('g.categories_id','=','c.id')
                ->join(array('shop_prices', 'prices'), 'LEFT')
                    ->on('g.id', '=', 'prices.good_id')
//                ->where('g.status','!=','out_of_store')
                ->where('prices.price_categories_id', '=', Kohana_Config::instance()->load('shop')->default_category)
//                ->and_where('g.status','!=','unverified')
                ->and_where('g.status','!=','archive')
                ->and_where('c.status','!=','unverified')
                ;
        if( !IN_PRODUCTION)
            $query->limit(1);
        
        if( $catid != 0 )
                $query->and_where('c.id','=',$catid);
        if( $byScu != FALSE )
                $query->and_where('g.scu','!=', '');

        if( !$reparse )
                $query->and_where('g.parsed','!=', '1');

        $result = $query->execute();
        if( $result->count() == 0) return array();
        else
        {
            if( $catid != 0 )
                DB::update('shop_categories')->set(array('last_parse' => DB::expr('NOW()')) )->where('id','=',$catid)->execute();
            else
                DB::update('shop_categories')->set(array('last_parse' => DB::expr('NOW()')) )->execute();
            
            return $result->as_array('id');
        }

    }

    static function saveParams( $params = NULL, $good_id = NULL )
    {
        if(!isset($good_id) && empty($good_id)){Model_Parser::$errors .='Не задан айди товара'; return false;}
        if( count($params) == 0 || !isset($params) || empty($params) ){ Model_Parser::$errors .='Параметры заданы'; return false; }
        
        $result = DB::select( array('gp.name','name'), array('gp.alias','alias'), array('gp.id','id') )
            ->from(array('shop_goods','g'))
            ->join(array('shop_goods_properties', 'gp'))
                ->on('gp.category_id','=','g.categories_id')
            ->and_where('g.id','=', $good_id)
            ->execute()
        ;
        
//        Kohana_log::instance()->add('d', print_r($result->as_array(),1));
//        die(Kohana::debug( $result->__toString() ));
        if( $result->count() == 0 ){ Model_Parser::$errors .= 'Нет совпадений с базой '; return false;}
        else
        {
            $update = array();
            $properties_category_id = array();
//            Kohana_log::instance()->add('d', print_r($params,1) );
//            Kohana_log::instance()->add('d2', print_r($result->as_array(),1) );
            foreach($params as $key => $item)
            {
                foreach( $result->as_array() as $iitem)
                {                    
                    if( $iitem['alias'] == Controller_admin::to_url($key) )
                    {
                        // нашелся параметр
                        if( trim($key) == ':' ) continue;
                        $update[$iitem['alias']] = array('data' => $item, 'id' => $iitem['id']);
                        break;
                    }
                }
            }
            
            unset($item);
            unset($key);
            $return = 0;
//            Kohana_log::instance()->add('d', print_r($update,1));
//            Kohana_log::instance()->add('$good_id', print_r($good_id,1));
            // обновим те параметры которые нашли
            foreach($update as $key => $item)
            {

                DB::delete('shop_goods_properties_values')
                        ->where('good_id', '=', $good_id)
                        ->and_where('properties_category_id', '=', $item['id'])
                        ->execute()
                        ;
                DB::insert('shop_goods_properties_values', array(
                    'good_id','properties_category_id', 'value_text') )
                        ->values(array(
                            $good_id,
                            $item['id'],
                            $item['data']
                            ))
                        ->execute()
                        ;

                $return++;
            }
            
            return $return;
        }
    }

    static function setDefaultPhoto($photos = array(), $good_id = NULL)
    {
//        Kohana_log::instance()->add('pgotos', Kohana::debug( $photos ));
        if( count($photos) == 0 || !isset($good_id) || empty($good_id) ) return false;
        if( trim(basename($photos[0])) != '' )
        {
            DB::update('shop_goods')
                ->set( array('default_photo' => basename($photos[0])) )
                ->where('id','=',$good_id)
                ->execute()
                ;
        }
        return true;
    }

    static function renameGood( $id = NULL,$name = NULL )
    {
        if( !isset($id) || empty($id) || !isset($name) || empty($name) ) return false;
        DB::update('shop_goods')->set(array('name'=>$name))->where('id','=',$id)->execute();
        return 1;
    }

    static function copyParamsFrom( $newParams, $toId,$overwrite = 0 )
    {           
        $result = DB::select()
            ->from('shop_goods_properties')
            ->where( 'category_id','=',$toId )
            ->order_by('alias')
            ->execute()
            ;

        $currentParams = $result->as_array('alias');

        $insertquery = DB::insert('shop_goods_properties', array(
            'category_id',
            'alias',
            'name',
            'type',
            ) )
        ;
        $flag = 0;
        if( count($newParams) > 0 )
        {
            foreach( $newParams as $key => $item )
            {
                $alias = Controller_admin::to_url($key);
                $name = $key;

                if($overwrite == 1)
                {
                    /* полностью старые удаляются а новые добавляются*/
                    $q = DB::delete('shop_goods_properties')
                        ->where('category_id','=',$toId)
    //                    ->and_where('alias','=',$alias)
                        ->execute()
                        ;
                }

                if( !isset($currentParams[$alias]['alias']) || $overwrite == 1 )
                {
                    $flag = 1;
                    $insertquery->values(array(
                            $toId,
                            $alias,
                            $name,
                            'text',
                        ));
                }
    //            $alias .= isset($currentParams[$alias]['alias']) ? '_copy' : ''; // если такой уже есть
    //            $name .= isset($currentParams[$alias]['name']) ? '_copy' : ''; // если такой уже есть
            }
        }
        
        if( $flag == 1 )
            $insertquery->execute();

        return $newParams;
    }

    function old_sendCaptcha( $id = NULL )
    {
        die('Hello world');
        $url = 'http://market.yandex.ru/captcha/check-captcha.xml?'.http_build_query($_POST);

        $_POST['retpath'] = '';
        $page = self::curl_get_contents($url );
        //Kohana_Log::instance()->add('parser',Kohana::debug( $url, http_build_query($_POST),$_POST, html_entity_decode($page) ));
        return 1;
    }
    
    static public function updateGood( $id = NULL )
    {
        if( !$id ) return false;
            
        $result = DB::select('id','quantity')
            ->from('shop_goods')
            ->where('id','=', $id)
            ->limit(1)
            ->execute()
        ;
        
        if( $result->count() == 0 ) return false;
        $result = $result->current();        
        if( $result['quantity'] == 0 ) self::update( array('status' => 'out_of_store'), array(array('id','=',$id)) );
        else self::update( array('status' => 'active'), array(array('id','=',$id)) );
    }
    
    static public function update( $data = array(), $where = array() )
    {
        if( count($where) == 0 || count($data) == 0 ) return false;
        
        $query = DB::update('shop_goods')
            ->set($data)
        ;
        
        foreach( $where as $item )
            $query->where( $item[0],$item[1],$item[2] );
        
        $query->execute();
        
        return true;
    }
    
    public function getParseURL()
    {
        // чето не получилось с абстрактным классом
    }
    
    /**
     * Function return inner html from DOM object
     * 
     * @param DOM object $element
     * @return string HTML 
     */
    static function DOMinnerHTML($element)
    { 
        $innerHTML = ""; 
        $children = $element->childNodes; 
        foreach ($children as $child) 
        { 
            $tmp_dom = new DOMDocument(); 
            $tmp_dom->appendChild($tmp_dom->importNode($child, true)); 
            $innerHTML.=trim($tmp_dom->saveHTML()); 
        } 
        return utf8_decode($innerHTML); 
    } 
}
?>