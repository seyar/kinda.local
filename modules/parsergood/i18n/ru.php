<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 ${date} - ${time} Exp $
 *
 * Project:     ${project.name}
 * File:        ${nameAndExt} * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author ${fullName} ${email}
 */
return array(
  'Yandex Parser'  => 'Парсер',
  'Select the source site'  => 'Выберите сайт-источник',
  'Parse the category parameters'  => 'Запарсить параметры категории',
  'Category'  => 'Категория',
  'From all'  => 'Все',
  'Overwrite all options'  => 'Перезатереть все параметры',
  'Parse goods from category'  => 'Запарсить товары категории',
  'To parse'  => 'Парсить',
  'Overwrite all goods'  => 'Перепарсить все товары',
  'Last parsing'  => 'Последний парсинг',
  'undefined'  => 'неопределен',
  'Rename'  => 'Переименовать',
  'Done'  => 'Готово',
  'View on Yandex'  => 'Просмотр на яндексе',
  'View on '  => 'Просмотр на ',
  'Parse by URL'  => 'Пaрсить по URL',
  'Parse by scu'  => 'Пaрсить по артикулу',
);
?>
