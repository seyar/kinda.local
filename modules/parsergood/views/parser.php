<form method="post" action="">
<!--floating block-->
    <div class="rel whiteBg floatingOuter" id="contentNavBtns" style="height:60px">
        <div class="whiteblueBg absBlocks floatingInner" style="height:60px">
            <div class="padding20px">
				<span class="greyitalic"><? echo I18n::get('Select the source site'); ?>:</span>
				 <ul style="list-style-type:none; margin-left:0px;">
                    <li><label><input type="radio" name="parseFrom" value="yandex" url="http://market.yandex.ua" <?if(trim($parseFrom) == 'yandex' || !$parseFrom ):?>checked="checked"<?endif;?> /> Yandex.ru</label></li>
                    <li><label><input type="radio" name="parseFrom" value="hotline" url="http://hotline.ua" <?if($parseFrom == 'hotline'):?>checked="checked"<?endif;?> /> Hotline.ua</label></li>
                </ul>   
			</div>

        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="padding20px rel whiteBg">
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>
            <!-- **** -->
            
              <div style="padding:5px;">
                  <span class="greyitalic"><? echo I18n::get('Parse the category parameters'); ?>:</span><br /><br />
                  URL: <input type="text" name="parseUrl" id="parseUrl" value="" style="width:99%;"/>
					<br /> <br />
					<? echo I18n::get('Category'); ?>:<br />
                  <select name="category_id_forcat" id="category_id_forcat" class="uncustomized" style="width: 100%;">
                      
                      <option value="0" rel=""><? echo I18n::get('From all'); ?></option>
                      <?foreach($rows as $item):?>
                        <?if($item['depth'] <=1 ) $style = "font-weight: bold;"; else $style='';?>
                        <option value="<?= $item['id']?>" rel="<?= date('d.m.Y',$item['last_parse'])?>" style="<?= $style?>"><?=$item['name']?></option>
                      <?  endforeach;?>
                  </select>
				  <br/>
                  <br/>
                  <button class="btn" type="button" onclick="parserStart_forcat();return false;"><span><span><? echo I18n::get('To parse'); ?></span></span></button>&nbsp;&nbsp;&nbsp;
                  <label><input style="margin-right:5px" type="checkbox" name="reparse_forcat" id="reparse_forcat" value="1"/>
				  <? echo I18n::get('Overwrite all options'); ?></label>

              </div>

              <div style="padding:5px;">
                  <br/>
				<span class="greyitalic"><? echo I18n::get('Parse goods from category'); ?>:</span><br /><br />
                  <select name="category_id" class="uncustomized" id="category_id" onchange="$('#last_parse').html('<br /><? echo I18n::get('Last parsing'); ?>: '+$(this).children('option:selected').attr('rel'));" style="width: 100%;">

                      <option text="Все" value=0 rel=""></option>
                      <? foreach($rows as $item):?>
                      <? if($item['depth'] <=1 ) $style="font-weight: bold;"; else $style='';?>
                        <option text="<?= $item['name']?>" value="<?= $item['id']?>" rel="<?= date('d.m.Y',$item['last_parse'])?>" style="<?= $style?>"><?=$item['name']?></option>
                      <? endforeach;?>
                  </select>
				  <br/>
                <label><input style="margin:5px 5px 5px 0" type="checkbox" name="byScu" value="1"/>
				  <? echo I18n::get('Parse by scu'); ?></label>
                  <br/>
                  <button class="btn" type="button" onclick="parserStart();return false;"><span><span><? echo I18n::get('To parse'); ?></span></span></button>&nbsp;&nbsp;&nbsp;<label><input style="margin-right:5px" type="checkbox" name="reparse" id="reparse" value="1"/><? echo I18n::get('Overwrite all goods'); ?></label>
                  <br/>
                  <span id="last_parse"></span>
                  <br/>
                  <div id="results"></div>
              
              </div>
            <!-- **** -->
<?//echo Kohana::debug($kohana_view_data);?>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
<? include "parser_js.php"?>