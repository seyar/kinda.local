<style type="text/css">
    .row{ padding: 0 0 15px 0; }
    .row .title{width: 45%; text-align: right; float: left; padding-right: 15px;}
    .row .res{ float: left; color: green;  width: 52%; padding: 0 0 10px 10px; border-bottom: 1px dotted grey;}
    .row .res a{display:block; margin:3px 0 5px 0}
</style>
<script type="text/javascript">
    
    function getParseDomain()
    {
        var parseUrl = '';
        $('input[name=parseFrom]').each(function(){ 
            if( $(this).attr('checked') == true )
            {
                //parseFrom = $(this).val();
                parseUrl = $(this).attr('url');
            }
        });
        return parseUrl;
    }
    
    function parserStart_forcat()
    {
        var resulthtml = '';
        var catid = $('#category_id_forcat option:selected').val();
        var reparse = $('#reparse_forcat').attr('checked') == true ? 1 : 0;
        if( $('#parseUrl').val() == '' ){alert('enter url');return false;}
        if( catid == 0 ){alert('enter cat');return false;}
        
        var parseUrl = getParseDomain();
        var parseFrom = $('input[name=parseFrom]:checked').val();
        
        
        $('#results').html('<p class="tCenter"><img src="/static/admin/images/preloader.gif" /></p>');
        $.getJSON(
            '<?= $admin_path?><?= $controller?>'+catid+'/parseParams',
            { parseUrl:$('#parseUrl').val(), overwrite:reparse, parseFrom: parseFrom  },
            function(data)
            {
                if( typeof(data) == 'object')
                {
                    for(i in data)
                    {
                        resulthtml += '<div class="row">\
                                <div class="title">' + i + '</div>\
                                <div class="res"><? echo I18n::get('Done'); ?></div>\
                                <div class="clear"></div>\
                            </div>';
                    }
                   
                }else{
                    resulthtml += data;
//                    alert(data);
                }
                $('#results').html(resulthtml);
            }
        );
    }
    function parserStart()
    {
        var resulthtml = '';
        var goodname = '';
        var catid = $('#category_id option:selected').val();
//        var catid = $('#category_id').val();
        var reparse = $('#reparse').attr('checked') == true ? 1 : 0;
        var parseFrom = $('input[name=parseFrom]:checked').val();
        var byScu = $('input[name=byScu]:checked').val();
        
        $('#results').html('<p class="tCenter"><img src="/static/admin/images/preloader.gif" /></p>');
        $.getJSON(
            '<?= $admin_path?><?= $controller?>'+catid+'/getListGoods',
            {reparse:reparse, parseFrom: parseFrom, byScu:byScu },
            function(data)
            {                
                if( typeof(data) == 'object')
                {                    
                    for(i in data)
                    {
                        goodname = data[i]['name'].length != 0 ? data[i]['name'] : data[i]['alter_name'];

                        resulthtml += '<div class="row" id="id_'+data[i]['id']+'">\
                                <div class="title">' + data[i]['id']+'. '+goodname +'. ('+ data[i]['scu'] +')</div>\
                                <div class="res"><img src="/static/admin/images/preloader.gif" style="width:10px;"/></div>\
                                <div class="clear"></div>\
                            </div>';
                    }
                }
                else
                {
                    alert(data);
                }
                
                $('#results').html(resulthtml);
                
                for(j in data)
                {
                    goodname = data[j]['name'].length != 0 ? data[j]['name'] : data[j]['alter_name'];
                    goodname = byScu == 1 ? data[j]['scu'] : goodname;   
                    
					/*
                    if(goodname.indexOf(',') != -1)
                    {
                        pos = goodname.search(/,/i);
                        goodname = goodname.substr(0,pos);
                    }
					*/
                    
                    if( parseGood(goodname, data[j]['id'],'', parseFrom, 1) == -1) break;                        
//                    setTimeout(function(){ parseGood(goodname, data[j]['id']); }, 100);
                }
                
                
            }
        );
    }

    function parseGood(name, id, url, parseDomain, dontparse)
    {
        parseDomain = parseDomain || 'yandex';
        dontparse   = dontparse || '0';
        url = url || '';
        name = name.replace('"','&quot;');
        
        var parseUrl = getParseDomain();
        var byScu = $('input[name=byScu]:checked').val();
        
        
        $('#id_'+id+' .res').html('<p class="tCenter"><img src="/static/admin/images/preloader.gif" /></p>');
        $.ajax({
            type:'get',
            dataType:'json',
            url:'<?= $admin_path?><?= $controller?>parse',
            data:{'query':name, 'id':id, url:url, parseDomain:parseDomain, byScu:byScu, dontparse:dontparse },
            success:function(data){
                if( data.errors == 0)
                {
                    $('#id_'+id+' .res').html('Готово');
                }else{
                    $('#id_'+id+' .res').html('<span style="color:red;display: block;margin-bottom: 5px;">'+data.ret+'</span>\
                            <input type="text" style="width:71%;" name="goodname['+id+']" rel="'+id+'" value="'+name+'" onkeyup="if(event.keyCode == 13) renameGood( $(this).next() );return false;" />\
							<button class="btn" type="button" onclick="renameGood(this);return false;"><span><span><? echo I18n::get('Rename'); ?></span></span></button>\
							<a target="_blank" href="'+data.parseUrl+'"><? echo I18n::get('View on '); ?>'+ $('input[name=parseFrom]:checked').parent().text() +'</a>\
							<input style="width:71%;" type="text" name="" class="byurl" value="'+url+'" onkeyup="if(event.keyCode == 13) parseGood( \''+name+'\', '+id+',$(this).val(), \''+$('input[name=parseFrom]:checked').val()+'\' );"/>\
							<button class="btn" type="button" onclick="parseGood( \''+name+'\', '+id+',$(this).prev().val(), \''+$('input[name=parseFrom]:checked').val()+'\' );return false;"><span><span><? echo I18n::get('Parse by URL'); ?></span></span></button>\
                    ');
                }
            },
            async:false
        });
    }

    function renameGood(thisObj)
    {
        var name = $(thisObj).prev().val();
        var id = $(thisObj).prev().attr('rel');
        var parseDomain = $('input[name=parseFrom]:checked').val();

        $.post(
            '<?= $admin_path?><?= $controller?>/'+id+'/renameGood',
            {name:name},
            function(data)
            {
                if( data == 1)
                {
                    $('#'+id+' .res').html('<p class="tCenter"><img src="/static/admin/images/preloader.gif" /></p>');
                    parseGood(name, id,'',parseDomain);
                }
            }
        );

//        alert("Здравствуйте!\r\nВы позвонили в центр поддержки службы хэзболат по номеру\r\nСифир, Сифир, Вахат, Вахат\r\nс именем = "+name+"\r\nфамилия = "+id);
    }

    function captchaSend( thisObj )
    {
        var response = $(thisObj).prev().val();
        var id = $(thisObj).parents('.row').attr('id');        
        id = id.replace('id_','');
        var name = $('#id_'+id+' .res').find('input[rel='+id+']').val();
        var url = $('#id_'+id+' .res').find('input.byurl').val();
        var parseUrl = getParseDomain();
//        var src = $(thisObj).parent().parent().find('img').attr('src');
                
        var fields = new Object();
        $(thisObj).parent().parent().parent().find('input').each(
            function(){
                var key = $(this).attr('name');
                fields[key] = $(this).val();
            }
        );
        fields['url'] = url;
        fields['id'] = id;
        fields['query'] = name;
    
        $('#id_'+id+' .res').html('<p class="tCenter"><img src="/static/admin/images/preloader.gif" /></p>');
        $.ajax({
            type:'post',
            dataType:'json',
            url:'<?= $admin_path?><?= $controller?>'+id+'/sendCaptcha',
            data:fields,
            success:function(data)
            {
                $('#id_'+id+' .res').html('<p class="tCenter"><img src="/static/admin/images/preloader.gif" /></p>');

                if( data.errors == 0)
                {
                    $('#id_'+id+' .res').html('Готово');
                }else{
                    $('#id_'+id+' .res').html('<span style="color:red;display: block;margin-bottom: 5px;">'+data.ret+'</span>\
                            <input type="text" style="width:71%;" name="goodname['+id+']" rel="'+id+'" value="'+name+'" onkeyup="if(event.keyCode == 13) renameGood( $(this).next() );return false;" />\
							<button class="btn" type="button" onclick="renameGood(this);return false;"><span><span><? echo I18n::get('Rename'); ?></span></span></button>\
							<a target="_blank" href="'+data.parseUrl+'"><? echo I18n::get('View on '); ?>'+ $('input[name=parseFrom]:checked').parent().text() +'</a>\
							<input style="width:71%;" type="text" name="" class="byurl" value="'+url+'" onkeyup="if(event.keyCode == 13) parseGood( \''+name+'\', '+id+',$(this).val(), \''+$('input[name=parseFrom]:checked').val()+'\' );"/>\
							<button class="btn" type="button" onclick="parseGood( \''+name+'\', '+id+',$(this).prev().val(), \''+$('input[name=parseFrom]:checked').val()+'\' );return false;"><span><span><? echo I18n::get('Parse by URL'); ?></span></span></button>\
                    ');
                }
            }
        });
    }
    
    function captchaSend2( thisObj )
    {
        var catid = $('#category_id_forcat option:selected').val();
        var resulthtml = '';
        
        var fields = new Object();
        $(thisObj).parent().parent().parent().find('input').each(
            function(){
                var key = $(this).attr('name');
                fields[key] = $(this).val();
            }
        );
            
        fields.overwrite = $('#reparse_forcat').attr('checked') == true ? 1 : 0;
        fields.parseUrl = $('#parseUrl').val();


        $.getJSON(
            '<?= $admin_path?><?= $controller?>'+catid+'/sendCaptcha2',
            fields,
            function(data)
            {
                if( typeof(data) == 'object')
                {
                    for(i in data)
                    {
                        resulthtml += '<div class="row">\
                                <div class="title">' + data[i] + '</div>\
                                <div class="res"><? echo I18n::get('Done'); ?></div>\
                                <div class="clear"></div>\
                            </div>';
                    }

                }else{
                    resulthtml += data;
//                    alert(data);
                }
                $('#results').html(resulthtml);
            }
        );
    }
</script>
