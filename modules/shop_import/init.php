<?php defined('SYSPATH') or die('No direct script access.');

// Import Info
Route::set
        (
	    'shop_front_import',
	    '(<lang>/)shopimport(/<type>(/<action>))',
	    array(
		      'action' => '(?:index|view|repairCategories|repairParentsIds)'
		    , 'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'type' => '(?:1c|csv)'
		    , 'id' => '.+'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_shopimport',
		    'action'        => 'import',
		    'type'        => '1c',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;