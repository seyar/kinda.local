<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    'Goods' => array
    (
        'shop_categories' =>  array
                            (
                                'fields' => array
                                (
                                    'id'                => 'Код',
                                    'parent_id'         => 'КодРодителя',
                                    'name'              => 'Наименование',
                                ),
                                'rules' => array
                                (
                                    'ГруппаТоваров'     => 'Да',
                                ),
                            ),
        'shop_goods' =>  array
                            (
                                'fields' => array
                                (
                                    'id'                => 'Код',
                                    'categories_id'     => 'КодРодителя',
                                    'alter_name'        => 'Наименование',
                                    'guaranties_period' => 'Гарантия',
                                    'scu'               => 'Артикул',
                                ),
                                'rules' => array
                                (
                                    'ГруппаТоваров'     => 'Нет',
                                ),
                                'status' => 'unverified',
                                'start_date' => true,
                            ),
        'shop_prices' =>  array
                            (
                                'rows' => array
                                (
                                    '1'                 => 'ЦенаРозничная',
                                    '2'                 => 'ЦенаСпец',
                                ),
                                'rows_link'             => 'price_categories_id',
                                'rows_fields'           => array
                                (
                                    'good_id'           => 'Код',
                                ),
                                'rows_fields' => array
                                (
                                    'good_id'           => 'Код',
                                ),
                                'rules' => array
                                (
                                    'ГруппаТоваров'     => 'Нет',
                                ),
                            ),
    ),

    'GoodsCount' => array
    (
        'shop_goods' =>  array
                            (
                                'fields_update' => array
                                (
                                    'quantity'              => 'Колво',
                                ),
                                'fields_link'               => array( 'id' => 'Код' ),
                            ),
    ),

    'Users' => array
    (
        'shop_customers' =>  array
                            (
                                'fields' => array
                                (
                                    'contractor_id'         => 'Код',
                                    'name'                  => 'ОфицНаим',
                                    'price_category_id'     => 'КатегорияЦены',
//                                    'is_taxpayer'              => 'НДС',
                                ),
                                'fields_replace' => array
                                (
                                    'КатегорияЦены'         => array( 'shop_price_categories' , 'id'),

                                ),
                                'fields_rule' => array (
                                    'КатегорияЦены'
                                )

                            ),
    ),
);
