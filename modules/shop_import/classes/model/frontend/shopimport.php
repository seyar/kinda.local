<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Frontend_ShopImport extends Model
{
    static protected $_parentString = '1,';
    
    static function drop_quantity()
    {
        Db::update('shop_goods')
            ->set( array('quantity' => 0) )
            ->execute()
        ;
    }

    /**
     * Function check difference from xml and categories. check not existed ids and parents. check having goods and folders with goods photos
     * 
     * @param string $import_type 1c
     * @param string $file Goods
     * @param string $action Goods
     * @param array $config config shopimport with rules
     */
    static function repairCategories($import_type, $file, $action, $config)
    {
        $import_config = Kohana::config('import_'.$import_type);
        $rules = Kohana::config('import_'.$import_type.'.'.$action.'.shop_categories');

        $xml_file = file_get_contents($file);
        $xml = simplexml_load_string($xml_file, "SimpleXMLElement", LIBXML_NOBLANKS); // & LIBXML_NOXMLDECL
        
        $categories = self::getCategories();
        
        $xmlarr = array();
        $toDelete = array();
        if(count($xml))
        {
            foreach( $xml as $node )
            {
                if(
                        ($rules['rules'] && Model_Frontend_ShopImport::process_rules($action, $rules['rules'], $rules ,$node))
                            || !$rules['rules'] 
                )
                {                    
                    $id = $node['Код']->__toString();
                    $parentid = $node['КодРодителя']->__toString();
                    
                    if( !isset($categories[$id]) || !isset($categories[$id]) )
                        $toDelete[] = $id;
                        
                    $xmlarr[$id] = $parentid;
                }
            }
        }
        $difference = array_diff( array_keys($categories), array_keys($xmlarr) );
        
        $folders = array();
        if( count($difference) > 0 )
        {
            $goods = DB::select('id')
            ->from('shop_goods')
                    ->where('categories_id','IN',DB::expr('('.implode(',',$difference).')') )
                    ->execute()
                    ->as_array('id','id')
                    ;
            
            foreach($goods as $item)
            {
                if( file_exists( MEDIAWEBPATH.'shop/'.$item.'/' ) ) $folders[] = $item;
            }

            DB::delete('shop_categories')
                ->where( 'id', 'IN', DB::expr('('.implode(',',$difference).')') )
                ->execute()
            ;
        }
        
        echo(Kohana::debug($folders,$difference, $toDelete));
//        echo(Kohana::debug($xmlarr,$categories));
        die('Hello world');
    }
    
    static function import_data($import_type, $file, $action, $config)
    {
        $import_config = Kohana::config('import_'.$import_type);

        $xml_file = file_get_contents($file);
        $xml = simplexml_load_string($xml_file, "SimpleXMLElement", LIBXML_NOBLANKS); // & LIBXML_NOXMLDECL

        if (count($xml))
        {
            foreach ($xml as $node)
            {                
                foreach ($import_config[$action] as $key=>$value)
                {
                    if ( ($value['rules'] && Model_Frontend_ShopImport::process_rules($action, $value['rules'], $value ,$node))
                            || !$value['rules'] )
                    {
                     
                        if ( $value['fields'] ) // if columns mode (categories, prices)
                        {
                            $query = Db::select()
                                        ->from($key)
                                        ->where('id', '=', $node[$value['fields']['id']])
                                        ;

                            $result = $query->execute();

                            $sql_cols = $sql_values = array();
                            
                            foreach ($value['fields'] as $sql_key=>$sql_value)
                            {
                                $sql_cols[] = $sql_key;
                                $sql_values[$sql_key] = (string)$node[$sql_value];
                            }

                            if ($result->count() == 0) // insert
                            {
                                if ($value['start_date'])
                                {
                                    $sql_cols[] = 'start_date';
                                    $sql_values['start_date'] = date('Y-m-d H:i:s');
                                }

                                if ($value['status'])
                                {
                                    $sql_cols[] = 'status';
                                    $sql_values['status'] = $value['status'];
                                }
                                $query = Db::insert($key, $sql_cols)->values($sql_values);
                            }
                            else // update
                            {
                                $query = Db::update($key)->set($sql_values)->where('id', '=', $node[$value['fields']['id']]);
                            }

                            $query->execute();
                            
                        }
                        elseif ( $value['fields_update'] ) // if update mode (quantities)
                        {
                            $sql_values = array();

                            foreach ($value['fields_update'] as $sql_key=>$sql_value)
                            {
                                $sql_values[$sql_key] = Db::expr( "`$sql_key` + " .(string)$node[$sql_value]);
                            }

                            $query = Db::update($key)->set($sql_values);

                            foreach ($value['fields_link'] as $sql_key=>$sql_value)
                            {
                                $query->where($sql_key, '=', $node[$sql_value]);
                            }

                            $query->execute();
                        }
                        elseif ( $value['rows'] ) // if rows mode
                        {
                            // process default currency

                            foreach ($value['rows'] as $rw_key=>$rw_value)
                            {
                                $query = Db::select()
                                            ->from($key)
                                            ->where((string)$value['rows_link'], '=', $rw_key)
                                            ->and_where('currencies_id', '=', Controller_Frontend_ShopImport::$default_currency)
                                    ;

                                foreach ($value['rows_fields'] as $r_key=>$r_value)
                                {
                                    $query->and_where($r_key, '=', $node[$r_value]);
                                }

                                $result = $query->execute();

                                $sql_cols = $sql_values = $sql_update_cols = $sql_update_values = array();

                                foreach ($value['rows_fields'] as $sql_key=>$sql_value)
                                {
                                    $sql_cols[] = $sql_key;
                                    $sql_values[$sql_key] = (string)$node[$sql_value];

                                    $sql_update_cols[] = $sql_key;
                                    $sql_update_values[$sql_key] = (string)$node[$sql_value];
                                }

                                $sql_cols[] = 'price';
                                $sql_values['price'] = str_replace(',', '.', $node[$rw_value]);

                                if ($result->count() == 0) // Insert
                                {
                                    $sql_cols[] = 'currencies_id';
                                    $sql_values['currencies_id'] = Controller_Frontend_ShopImport::$default_currency;
                                    $sql_cols[] = (string)$value['rows_link'];
                                    $sql_values[(string)$value['rows_link']] = $rw_key;

                                    $query = Db::insert($key)->columns(array_keys($sql_values))->values($sql_values);
                                }
                                else // Update
                                {
                                    $current = $result->current();
                                    $query = Db::update($key)->set($sql_values)->where('id', '=', $current['id']);
                                }
                                
                                $query->execute();


                                // Process active currencies
/*
                                foreach (Controller_Frontend_Shop::$active_currencies as $c_value)
                                {
                                    $sql_values['currencies_id'] = $c_value;

                                    if ($result->count() == 0) // Insert
                                    {
                                        $query = Db::insert($key)->columns(array_keys($sql_values))->values($sql_values);
                                    }
                                    else // Update
                                    {
                                        $query = Db::select(array(Db::expr('COUNT(*)'),'cnt'),'id')->from($key)->where('currencies_id', '=', $c_value);

                                        foreach ($sql_update_cols as $u_value)
                                        {
                                            $query->and_where($u_value, '=', $sql_update_values[$u_value]);
                                        }

                                        $search_result = $query->execute()->current();

                                        $sql_values['currencies_id'] = $c_value;

                                        if ($search_result['cnt']) // there is row
                                        {
                                            $query = Db::update($key)->set($sql_values)->where('id', '=', $search_result['id']);
                                        }
                                        else // there isn't row
                                        {
                                            $sql_values[(string)$value['rows_link']] = $rw_key;
                                            $query = Db::insert($key)->columns(array_keys($sql_values))->values($sql_values);
                                        }
                                    }

                                    $query->execute();
                                }
*/
                            }

                        }
                    }
                }
            }
        }
    }

    static function import_customers($import_type, $file, $action, $config)
    {
        $import_config = Kohana::config('import_'.$import_type);

        $xml_file = file_get_contents($file);
        $xml = simplexml_load_string($xml_file, "SimpleXMLElement", LIBXML_NOBLANKS); // & LIBXML_NOXMLDECL

        if (count($xml))
        {
            foreach ($xml as $node)
            {
                foreach ($import_config[$action] as $key=>$value)
                {
                    if ( ($value['rules'] && Model_Frontend_ShopImport::process_rules($action, $value['rules'], $value ,$node))
                            || !$value['rules'] )
                    {

                        if ( $value['fields'] ) // if columns mode (categories, prices)
                        {
                            $query = Db::select()
                                        ->from($key)
                                        ->where('id', '=', $node[$value['fields']['contractor_id']])
                                        ;
                            $result = $query->execute();
                            

                            $sql_values = array();

                            foreach ($value['fields_replace'] as $sql_key=>$sql_value)
                            {

                                $query = Db::select($sql_value[1])
                                            ->from($sql_value[0])
                                            ->where('id', '=', $node[$value['fields'][$sql_key]])
                                            ;
                                $replace = $query->execute()->current();
                                
                                $node[$sql_key] = $replace;
                            }
echo Kohana::debug($node);

                            foreach ($value['fields'] as $sql_key=>$sql_value)
                            {
                                $sql_values[$sql_key] = (string)$node[$sql_value];
                            }

                            foreach ($value['fields_rule'] as $sql_key=>$sql_value)
                            {
                                $sql_values[$sql_key] = (string)$node[$sql_value];
                            }

echo Kohana::debug($sql_values);
echo Kohana::debug($result);
die();
                            if ($result->count() == 0) // insert
                            {
                                if ($value['start_date'])
                                {
                                    $sql_cols[] = 'start_date';
                                    $sql_values['start_date'] = date('Y-m-d H:i:s');
                                }

                                if ($value['status'])
                                {
                                    $sql_cols[] = 'status';
                                    $sql_values['status'] = $value['status'];
                                }
                                $query = Db::insert($key, $sql_cols)->values($sql_values);
                            }
                            else // update
                            {
                                $query = Db::update($key)->set($sql_values)->where('id', '=', $node[$value['fields']['id']]);
                            }

                            $query->execute();

                        }

                    }
                }
            }
        }
    }

    static function process_rules($action, $rules, $value ,$node)
    {
        foreach ($rules as $r_key => $r_value)
        {
            if ($node[$r_key] != $r_value)
            {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Function get category(ies)
     * 
     * @param array $where = array( $column, $op, $cond )
     * @return array array(id => parent_id) 
     */
    static function getCategories( $where = array(), $limit = NULL )
    {
        $cats = DB::select( 'id', 'parent_id' )
        ->from('shop_categories')
        ;
        
        if( isset($where) )
        {
            foreach( $where as $item )
                $cats->and_where( $item[0],$item[1],$item[2] );
        }
        if($limit)
            $cats->limit($limit);
        
        $cats = $cats->execute()
        ->as_array( 'id', 'parent_id' )
        ;
            
        return $cats;
    }
    
    /**
     * function repair parent_path in categories and save to db
     */
    static function repairParentsIds()
    {
        $cats = self::getCategories(NULL);
        unset($cats[1]);
//    die(Kohana::debug($p));
//    die('Hello world');
        foreach( $cats as $key => $item )
        {
            $p = self::getParentString( $key );
            DB::update('shop_categories')
                ->set(array(
                    'parents_path' => $p
                ))
                ->where('id','=',$key)
                ->execute()
            ;
            self::$_parentString = '1,';
        }
        die('Hello world');
    }
    
    static function getParentString( $item )
    {   
        $root = Kohana::config('shop.root_category_id');
        $cat = self::getCategories( array(array('id','=',$item)), 1 );
        
        if( (int)$cat[$item] != $root )
        {            
            self::$_parentString = self::$_parentString.$cat[$item].',';
            self::getParentString( $cat[$item] );
//            exit();
        }
        
        $temp = self::$_parentString;

        return $temp.$item.',';
        
    }
}