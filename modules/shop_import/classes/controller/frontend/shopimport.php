<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Frontend_ShopImport extends Controller_Content
{

    public $template;
    
    static public $default_currency;
    static public $active_currencies;

    const images_folder		= 'shop';
    const root_url		= 'shop';

    public function before()
    {
        parent::before();        
        
        $this->template = Kohana::config('shop')->start_template;
        Controller_Frontend_ShopImport::$default_currency = Kohana::config('shop')->default_currency;
        Controller_Frontend_ShopImport::$active_currencies = Kohana::config('shop')->active_currencies;

	$this->model->images_folder = MEDIAPATH.$this->images_folder.'/';

//	$this->view->assign('web_root_url',  Controller_Frontend_Shop::root_url);
//	$this->view->assign('web_images_folder', MEDIAWEBPATH.Controller_Frontend_Shop::images_folder."/" );

    }

    public function action_index()
    {

    }

    public function action_import()
    {
       Model_Frontend_ShopImport::import_data($this->request->param('type'), DOCROOT . 'Goods.xml', 'Goods', Kohana::config('shop'));
       Model_Frontend_ShopImport::drop_quantity();
       Model_Frontend_ShopImport::import_data($this->request->param('type'), DOCROOT . 'GoodsCount.xml', 'GoodsCount', Kohana::config('shop'));
//        Model_Frontend_ShopImport::import_customers($this->request->param('type'), DOCROOT . 'Users.xml', 'Users', Kohana::config('shop'));
    }

    public function action_repairCategories()
    {
        Model_Frontend_ShopImport::repairCategories($this->request->param('type'), DOCROOT . 'Goods.xml', 'Goods', Kohana::config('shop'));
    }
    
    public function action_repairParentsIds()
    {
        Model_Frontend_ShopImport::repairParentsIds();
        die('Hello world');
    }

}
