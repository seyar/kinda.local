<?php defined('SYSPATH') or die('No direct script access.');

// Frontend
Route::set
        (
	    'search_front',
	    '(<lang>/)search(/<query>(/<action>))',
	    array(
		    'lang' => '(?:'.LANGUAGE_ROUTE.')'
		)
        )
        ->defaults
        (
           array(
		    'controller'    => 'searchFrontend',
		    'action'        => 'index',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;