<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 02.03.2010 - 16:11:48 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        SearchFrontend.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_SearchFrontend extends Controller_Content
{
    public $template = 'search.tpl';
    public $per_page = 10;

//    public function before()
//    {
//        $this->model = new Model_SearchFrontend();
//    }

    public function action_index()
    {
        Model_SearchFrontend::deleteOldSearches();
        
        $page = $_GET['page'] != 0 ? $_GET['page'] - 1 : $_GET['page'];
        $start = $page * $this->per_page + 1;

        if( mb_strlen(trim($_GET['q']), 'UTF-8') < 4 ) return false;

        list($search_result,$page_links,$total_rows) = Model_SearchFrontend::getSearch(trim($_GET['q']),$start, $this->per_page);

        $this->page_info = array('page_title'=>I18n::get('Search'));
        $this->view->assign('search_result', $search_result );
        $this->view->assign('total_rows', $total_rows );
        $this->view->assign('page_links', $page_links );
//        $this->view->assign('page_line', $this->page_line($search_result, $total_rows) );
    }

    public function action_deleteOldSearches()
    {
        Model_SearchFrontend::deleteOldSearches();
        die();
    }

    static function get_sitemap()
    {
        return Model_SearchFrontend::get_sitemap();
    }
}

?>