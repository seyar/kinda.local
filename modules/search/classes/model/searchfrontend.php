<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 02.03.2010 - 15:19:05 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        searchFrontend.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_SearchFrontend extends Model
{
    function getSearch( $query,$start = 0, $limit )
    {
//        if( !$item = Model_SearchFrontend::getItems($query, CURRENT_LANG_ID) )
//        {
            Model_SearchFrontend::getSearchOnFly($query,$limit);
//        }

        $classes_for_search = array(
            array('Model_AdminPages','getSearch'),
            array('Model_Publications','getSearch'),
            array('Model_Photogallery','getSearch'),
        );
        $returnMedia = array();$foundMedia = array();
        $foundMedia = array_merge($foundMedia, call_user_func($classes_for_search[1],$query, Kohana::config('publications')->mediaIDs ));
        $foundMedia = array_merge($foundMedia, call_user_func($classes_for_search[2],$query) );
        foreach( $foundMedia as $item)
        {
            if( strip_tags($item['text']) )
            {
                $item['text'] = strip_tags($item['text']);
//                $item['text'] = Text::limit_chars(str_replace($query, Kohana::config('search')->select_before.$query.Kohana::config('search')->select_after, $item['text']), 500);
                $item['text'] = Text::limit_chars(preg_replace("/$query/", Kohana::config('search')->select_before.$query.Kohana::config('search')->select_after, $item['text'], 1), 500);
                $mykey = strstr($item['url'],'audio') ? 'audio' : (strstr($item['url'],'video') ? 'video' : 'photo');
                $returnMedia['media'][$mykey][] = $item;
            }
        }

        list($return,$page_links,$total_rows) = Model_SearchFrontend::getSearchFromDb($query, $start, $limit);
        if(is_array($return) && count($return) > 0 )
            $return = array_merge($return,$returnMedia);
        else
            $return = $returnMedia;
        
        return array($return,$page_links,$total_rows);
    }

    function getSearchOnFly($query,$limit)
    {
        // пройтись по всем моделькам бекенда и вызвать там функцию гет сеч.
        // или контроллерам
        
        $classes_for_search = array(
            array('Model_AdminPages','getSearch'),
            array('Model_Publications','getSearch'),
            array('Model_Photogallery','getSearch'),
        );
        
        $found = array();
        $foundMedia = array();
        /* for normal sites */
//        foreach($classes_for_search as $item)
//        {
//            $found = array_merge($found, call_user_func($item,$query));
//        }

        /* for fucking kontext */
        $found = array_merge($found, call_user_func($classes_for_search[0],$query));
        $found = array_merge($found, call_user_func($classes_for_search[1],$query, NULL, Kohana::config('publications')->mediaIDs ));


        $return = array();

        foreach( $found as $item)
        {
            if( strip_tags($item['text']) )
            {
                $item['text'] = strip_tags($item['text']);
//                $item['text'] = Text::limit_chars(str_replace($query, '<span style="color:#6e8362; background-color:#efcba7;">'.$query.'</span>', $item['text']), 500);
                $item['text'] = Text::limit_chars(preg_replace("/$query/", Kohana::config('search')->select_before.$query.Kohana::config('search')->select_after, $item['text'], 1), 500);
                $return[] = $item;
            }
        }
        
        if(count($return))
            Model_SearchFrontend::saveSearch(trim($query), $return);
        
        return $return;
    }

    function saveSearch($query, $result)
    {
        DB::query(Database::UPDATE, "REPLACE search_saved SET
            query = '".mysql_escape_string($query)."', 
            result = '".mysql_escape_string(serialize($result))."',
            language_id = '".CURRENT_LANG_ID."',
            date = NOW()
        ")->execute();

        return 1;
    }

    function deleteOldSearches()
    {
        DB::query(Database::DELETE, 'DELETE FROM search_saved WHERE TO_DAYS(NOW())-TO_DAYS(date) > 2')->execute();
        return 1;
    }

    function getSearchFromDb($query, $start = 0, $length = 10)
    {
        $item = Model_SearchFrontend::getItems($query, CURRENT_LANG_ID);

        if($item)
        {
            $full_result = unserialize($item['result']);
            $total_rows = ceil(count($full_result) / $length);

            $pagination = Pagination::factory(array(
                    'total_items'    => count($full_result),
                    'items_per_page' => $length,
            ));

            $page_links = $pagination->render('pagination/digg');
            
            $res = count(unserialize($item['result'])) > $length ?
				array_slice(unserialize($item['result']), $start-1, $length)
			: 
				unserialize($item['result']);

            $return = array();
            $key = $start;          
			
            if(!$res ) return false;
			
            foreach($res as $r)
            {
                $return[$key] = $r;
                $key++;
            }
            
            return array( $return,$page_links, $total_rows );
            
        }else{
            return false;
        }
        
    }

    function getItems($id = NULL, $lang_id = 1)
    {
        // $query = DB::query(Database::SELECT, "SELECT * FROM search_saved WHERE TO_DAYS(NOW())-TO_DAYS(date) < 2")
        
        $query = DB::select()
            ->from('search_saved')
            ->where('language_id','=',$lang_id)
            ->order_by('date','desc')
        ;

        if($id && is_numeric($id))          $query->and_where('id','=', $id);
        if($id && !is_numeric($id))         $query->and_where('query','=', $id);
        
        $result = $query->execute();
        if( $result->count() == 0 ) return false;
        
        if($id)
            $return = $result->current();
        else
            $return = $result->as_array();

        return $return;
    }
    static public function get_sitemap()
    {

    }
}
?>