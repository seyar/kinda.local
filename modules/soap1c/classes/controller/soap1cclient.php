<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 04.06.2010 - 15:37:53 Exp $
 *
 * Project:     microline.shade
 * File:        transport.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
class Controller_Soap1cClient extends Controller
{
    // слушаем 1с принимаем запросы
    // посылаем запросы 1с. о наших заказах

    private $client;
    private $model;

    function before()
    {
        parent::before();
//        $this->model = new Model_Soap1c();
        // disable wsdl cache
        ini_set('soap.wsdl_cache_enabled', '0');
        
        /* zend module for soap*/
//        require ('Zend/Soap/AutoDiscover.php' );
        
    }

    public function action_index()
    {
        try {
            $this->client = new SoapClient(
                   'http://' . $_SERVER['HTTP_HOST'] . '/soap1c-server/wsdl',
//                   'http://' . $_SERVER['HTTP_HOST'] . '/e_1.wsdl',
                    array(
                        "trace"      => 1,
                        "exceptions" => 0
                    )
            );
            $data = array(
              'code' => 'euro',
              'name' => 'EUR',
              'iso_name' =>840,
              'rate' => 0.125,
              'vat' => 0.2,
            );
            $result = $this->client->saveCurrency($data);
            
        } catch (SoapFault $exception) {
            echo $exception;
            die('Hello world');
        }
        

        echo (Kohana::debug( $result ));
//        echo (Kohana::debug( $this->client ));
        
        return ;
    }

    public function action_savegood()
    {
        try {
            // инициализация клиента
            $this->client = new SoapClient(
                   'http://' . $_SERVER['HTTP_HOST'] . '/soap1c-server/wsdl',
                    array(
                        "trace"      => 1,
                        "exceptions" => 0
                    )
            );            
        }catch ( SoapFault $exception){
            echo $exception;
            die();
        }

//        $ap_param = array(
//            'id' => '6',
//            'name' => 'd',
//            'description' => '@$post[]',
//            'parent_id' => 628,
//            'status' => 1 // categs
//        );
        $ap_param = array(
            "id" => 298,
            "exported1c" =>  "1",
            "status" =>  3,
            "goods_list" => array(108661 => 1),
        );

        $ap_param = array(
            'id' => '2968',
            'categories_id' => "111168",
//            'scu' => '@$post[',
//            'name' => '@$post[name]',
//            'full_description' => '@$posdasf asdf asdf[full_description]',
//            'cost' => '@$post[cost]',
            'quantity' => '20',
//            'price' => '2000',
//            'price_uah' => '3000',
//            'guaranties_period' => '@$post[guaranties_period]',
//            'status' => 'active',
        ); // goods

        /* {"id":"372","status":"1","exported1c":"1","goods_list":{109417:1}} edit order*/
//        {"id":"108643","categories_id":"761","name":"OEM Windows XP Professional Rus (для сборщиков)"}

//        $ap_param = array(
//                        'id' => 3,
//                        'name' => '$post[dasdsa]',
//                        'code' => '$post[code]',
//                        'iso_code' => '$post[iso_code]',
//                        'rate' => 0.12,
//                        'rate_w_vat' => 0.13
//            );
        
        
        $result = $this->client->saveGood( json_encode( $ap_param ) ); // вызов удаленного метода
        

        echo (print_r($result,1) );
//        file_put_contents(DOCROOT.'/file.txt', $result."\r\n");
//        echo (Kohana::debug( $this->client ));
        return ;
    }
    
}

?>