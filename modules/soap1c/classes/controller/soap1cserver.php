<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 04.06.2010 - 15:37:53 Exp $
 *
 * Project:     microline.shade
 * File:        transport.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
//require dirname(__FILE__).'/../model/soap1c.php';

class Controller_Soap1cServer extends Controller
{
    // слушаем 1с принимаем запросы
    // посылаем запросы 1с. о наших заказах

    private $server;
    private $client;
    private $model;

    function before()
    {        
        parent::before();
//        $this->model = new Model_Soap1c();
        // disable wsdl cache
        ini_set('soap.wsdl_cache_enabled', '0');
        
        /* zend module for soap*/
//        require ('Zend/Soap/AutoDiscover.php' );
        
    }

    public function action_service()
    {
        // disable wsdl cache        
        ini_set('soap.wsdl_cache_enabled', '0');
        // set auth settings if needed
//        if( !defined( SOAP_AUTHENTICATION_BASIC ))  define('SOAP_AUTHENTICATION_BASIC', 1);
//        if( !defined( SOAP_1_2 ))                   define('SOAP_1_2', 2);
//        if( !defined( WSDL_CACHE_NONE ))            define('WSDL_CACHE_NONE', 0);
        $settings   = array(
                          'login'           => 'user',
                          'password'        => 'password',
                          'authentication'  => SOAP_AUTHENTICATION_BASIC,
                          'soap_version'    => SOAP_1_2,
                          'encoding'        => 'UTF-8',
                          'cache_wsdl'      => WSDL_CACHE_NONE
                          );

        // include user:password if needed
        $this->server = new SoapServer('http://' . $_SERVER['HTTP_HOST'] . '/soap1c-server/wsdl', $settings);
//        $this->server = new SoapServer(DOCROOT.'/e_1.wsdl', $settings);
        
        $this->server->setClass('Model_Soap1c');
        $this->server->handle();        
        die();
    }
        
    /**
     * wsdl to call
     *
     * @param void
     * @return void
     */
    public function action_wsdl()
    {
//        $wsdl = new Zend_Soap_AutoDiscover();
//
//        $wsdl->setUri('http://' . $_SERVER['HTTP_HOST'] .'/soap1c-server' );
//        $wsdl->setClass('Model_Soap1c');
////        $wsdl->handle();
//        $wsdl->dump(DOCROOT.'soap.wsdl');        
//        include DOCROOT.'/wsdl/soap_4.php';
        echo file_get_contents(DOCROOT.'/wsdl/soap_4.wsdl');
        die();
    }
}
?>