<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 04.06.2010 - 15:38:00 Exp $
 *
 * Project:     microline.shade
 * File:        transport.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_Soap1c extends Model
{
    function deleteCategory($id)
    {

        if (is_numeric($id))
        {
            if( Model_Categories::categories_delete($id) )
                return 1;            
        }
        
        return 0;
    }

    function saveCategory($data)
    {
        if (!$post = json_decode($data, TRUE))
        {
            return new SoapFault("error", "Ошибка разбора soap");
        }

        $id = (int) $post['id'];
        if (!$id)
        {
            return new SoapFault("error", "на задан ай-да");
        }
        $lang = 1;
        if (!$post['parent_id'])
        {
            return new SoapFault("error", "на задан парент ай-да");
        }
Kohana_Log::instance()->add('soap save category', print_r($post,1));
        $res = Model_Categories::categories_load($id);
        $parent = Model_Categories::categories_load($post['parent_id']);
        if( !isset($parent) || count($parent) == 0 )
        {
            return new SoapFault("error", "Парент ай-да не существует");
        }

        $post['description'] = isset($post['description']) ? $post['description'] : DB::expr('description');
        $post['status'] = isset($post['status']) ? $post['status'] : DB::expr('status');
        $post['name'] = isset($post['name']) ? $post['name'] : DB::expr('name');
        $post['seo_url'] = isset($post['seo_url']) ? $post['seo_url'] : ( isset($post['name']) ? Controller_Admin::to_url($post['name']) : DB::expr('seo_url') );
        
        if ((int) @$res['id'])
        { // update
            $id = (int) $id;

            // Load current page info
//            $result = DB::select()->from('shop_categories')
//                    ->limit(1)
//                    ->where('id', '=', $id)
//                    ->execute()
//                    ->current()
//                    ;
            DB::update('shop_categories')
                    ->set(
                            array(
//                                    'name' => $post['name'],
                                'seo_url' => $post['seo_url'],
//                                    'seo_keywords' => $post['seo_keywords'],
//                                    'seo_description' => $post['seo_description'],
                                'description' => $post['description'],
                                'parent_id' => $post['parent_id'],
//                                    'order_id' => $post['order_id'],
                                'status' => $post['status'],
                            )
                    )
                    ->where('id', '=', $id)
                    ->execute()
            ;
        }
        else
        { // create
            $id = 0;

            list($id, $total_rows) = DB::insert('shop_categories',
                                    array(
                                        'id',
                                        'languages_id',
                                        'name',
                                        'seo_url',
//                                    'seo_keywords',
//                                    'seo_description',
                                        'description',
                                        'parent_id',
//                                    'order_id',
                                        'status'
                                    )
                            )
                            ->values(
                                    array(
                                        $post['id'],
                                        $lang,
                                        $post['name'],
                                        $post['seo_url'],
//                                        $post['seo_keywords'],
//                                        $post['seo_description'],
                                        $post['description'],
                                        $post['parent_id'],
//                                        $post['order_id'],
                                        $post['status'],
                                    )
                            )
                            ->execute();
        }
        
        // new parents_path from new id
        $q1 = DB::select('parents_path')->from('shop_categories')
                ->where('id', '=', $post['parent_id'])
                ->execute();

        $q1 = $q1->as_array();

        $new_parent = $q1[0]['parents_path'].$id.',';

        DB::update('shop_categories')
            ->set(array('parents_path' => $new_parent))
            ->where('id', '=', $id)
            ->execute();

//        Model_Categories::save_add_filelds($lang, $id, $post);
//        return $id;
        return 1;
    }

    function saveGood($data)
    {
        if( substr( trim($data), 0,1 ) != '[' ) $data = "[$data]";
        $search = array('\\', '""""');
        $replace = array('_', '""');
        $data = str_replace($search, $replace, $data);
        $data = preg_replace('|\t|', '', $data);
        $data = preg_replace('|\r|', '', $data);
        $data = preg_replace('|\n|', '', $data);
        
        Kohana_log::instance()->add('soap save good json', print_r($data,1));
        
        $post = array();

        if( !$post = json_decode($data, TRUE) )
        {
            Kohana_log::instance()->add('$post good errored', print_r($data,1));
            return new SoapFault("error", "Ошибка разбора soap");
        }
        
        if( count($post) > 0 )
        {
            $res = array();
            foreach( $post as $item )
            {
                $to = array(Kohana::config('shop.send_to'),Kohana::config('shop.EmailDeveloper'));
                $from = array(Kohana::config('shop.send_from'), $_SERVER['HTTP_HOST']);
                $subject = array(Kohana::config('shop.subject'));                
                if( self::saveGoodSingle($item) != 1)
                {
                    Email::send($to,$from, $subject, 'не прошло сохранение товара с 1с '.$item['id'] );
                    Kohana_log::instance()->add('soap error save good', print_r($item,1) );
                }
            }
        }

        return 1;
    }
    
    static function saveGoodSingle($post)
    {
        Kohana_log::instance()->add('soap save good', print_r($post,1));
        /*
          array(
          id
          categories_id
          brand_id
          alter_name
          scu
          name
          short_description
          full_description
          is_bestseller
          is_new
          rating
          show_on_startpage
          default_photo
          start_date
          edit_date
          cost
          weight
          quantity
          guaranties_period
          store_approximate_date
          seo_keywords
          seo_description
          seo_url
          status
          );
         */

//        if (!$post = json_decode($data, TRUE))
//        {
//            return new SoapFault("error", "Ошибка разбора soap");
//        }
//
        $post['currencies_id'] = @$post['currencies_id'] ? $post['currencies_id'] : 1;
        $id = (int) $post['id'];
        if (!$id)
        {
            return new SoapFault("error", "на задан ай-ди");
        }
        
        /* проверяем есть ли такой товар. */
        $res = Model_Goods::load($id);
        $cat = Model_Categories::categories_load($post['categories_id']);
        if(count($cat) == 0)
        {
            return new SoapFault("error", "Категории не существует");
        }

        $post['status'] = Model_Content::arrGet($post, 'status', 'from1c');
        $goodstatuses = array('active','archive','out_of_store','unverified', 'allow_to_order');        
        $post['status'] = in_array(trim($post['status']),$goodstatuses) ? $post['status'] : 'from1c';
        
        
        $post['seo_url'] = isset($post['seo_url']) ? str_replace( Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, urlencode($post['seo_url']) ) : $res['seo_url'];
        $post['seo_url'] = str_replace( '__', '_', $post['seo_url'] );
        $post['seo_url'] = strtolower( str_replace( '__', '_', $post['seo_url'] ) );
        
        $post['scu'] = isset($post['scu']) ? $post['scu'] : DB::expr('scu');
        $post['full_description'] = isset($post['full_description']) ? $post['full_description'] : DB::expr('full_description');
        $post['cost'] = isset($post['cost']) ? $post['cost'] : DB::expr('cost');
        $post['quantity'] = isset($post['quantity']) ? $post['quantity'] : DB::expr('quantity');
        $post['guaranties_period'] = isset($post['guaranties_period']) ? $post['guaranties_period'] : DB::expr('guaranties_period');
        $post['alter_name'] = isset($post['alter_name']) ? $post['alter_name'] : ( isset($post['name']) ? $post['name'] : DB::expr('alter_name') );
        $post['store_approximate_date'] = (isset($post['store_approximate_date']) && strtotime($post['store_approximate_date']) ? $post['store_approximate_date'] : DB::expr('store_approximate_date') );
        $post['name'] = isset($post['name']) ? $post['name'] : Model_Content::arrGet($post, 'alter_name', 'new good from1c');
        
        if( isset($res) && count($res) > 0 )
        { // update
            DB::update('shop_goods')
                    ->set(
                            array(
                                'categories_id' => $post['categories_id'],
                                'scu' => $post['scu'],
                                'name' => (isset($res) && !empty($res['name']) && $res['name'] != 0 ) ? $res['name'] : $post['name'],
//                                    'brand_id' => $post['brand_id'],
                                'alter_name' => $post['alter_name'],
                                'short_description' => $post['full_description'],
//                                'full_description' => @$post['full_description'],
//                                    'is_bestseller' => $post['is_bestseller'],
//                                    'is_new' => $post['is_new'],
//                                    'rating' => $post['rating'],
//                                    'show_on_startpage' => $post['show_on_startpage'],
                                'cost' => $post['cost'],
//                                    'weight' => $post['weight'],
                                'quantity' => $post['quantity'],
                                'guaranties_period' => $post['guaranties_period'],
                                'store_approximate_date' => $post['store_approximate_date'],
//                                    'seo_keywords' => $post['seo_keywords'],
//                                    'seo_description' => $post['seo_description'],
                                'seo_url' => $post['seo_url'],
                                'status' => $post['status'],
                            )
                    )
                    ->where('id', '=', $id)
                    ->execute()
            ;
        }
        else
        { // create
            $id = $total_rows = 0;
            
            $post['status'] = $post['status'] == 'archive' ? 'archive' : 'from1c';

            list($id, $total_rows) = DB::insert('shop_goods',
                                    array(
                                        'id',
                                        'categories_id',
                                        'scu',
                                        'name',
//                                    'brand_id',
                                        'alter_name',
//                                    'short_description',
                                        'full_description',
//                                    'is_bestseller',
//                                    'is_new',
//                                    'rating',
//                                    'show_on_startpage',
                                        'cost',
//                                    'weight',
                                        'quantity',
                                        'guaranties_period',
                                        'store_approximate_date',
//                                    'seo_keywords',
//                                    'seo_description',
                                        'seo_url',
                                        'status',
                                    )
                            )
                            ->values(
                                    array(
                                        $post['id'],
                                        $post['categories_id'],
                                        $post['scu'],
                                        $post['name'],
//                                        $post['brand_id'],
                                        $post['alter_name'],
//                                        $post['short_description'],
                                        $post['full_description'],
//                                        $post['is_bestseller'],
//                                        $post['is_new'],
//                                        $post['rating'],
//                                        $post['show_on_startpage'],
                                        $post['cost'],
//                                        $post['weight'],
                                        $post['quantity'],
                                        $post['guaranties_period'],
                                        $post['store_approximate_date'],
//                                        $post['seo_keywords'],
//                                        $post['seo_description'],
                                        $post['seo_url'],
                                        $post['status'],
                                    )
                            )
                            ->execute();
        }

        // Prices update
        $post['currencies_id'] = Model_Content::arrGet($post, 'currencies_id', 1);
        $post['price'] = Arr::get($post, 'price', DB::expr('price') );
        $post['price_uah'] = Arr::get($post, 'price_uah', DB::expr('price') );
        
        self::pricesUpdate( $post['id'],$post['currencies_id'],$post['price'],$post['price_uah'] );
        
        

        return 1;
    }
    
    function pricesUpdate( $id = NULL, $currencies_id = NULL, $price = NULL, $price_uah = NULL )
    {
        $priceCats = Db::select()
                        ->from('shop_price_categories')
                        ->execute()
        ;
        
        if( $priceCats->count() > 0 )
        {            
            foreach( $priceCats->as_array() as $item )
            {
                $priceRow = Db::select()
                        ->from('shop_prices')
                        ->where( 'currencies_id', '=', $currencies_id )
                        ->and_where('good_id', '=', $id)
                        ->and_where('price_categories_id', '=', $item['id'])
                        ->execute()
                ;
                
                if( $priceRow->count() == 0 )
                {
                    // insert new price
                    $q = Db::insert('shop_prices', array('price', 'price_categories_id', 'currencies_id', 'good_id'));
                    $q->values(array(
                        $price,
                        $item['id'],
                        $currencies_id,
                        $id
                    ));
                    
                    if( Kohana::config('shop.rateMode') == 'manual' )
                    {
                        $q->values(array(
                            $price_uah,
                            Kohana::config('shop.rate_uah_id'),
                            $currencies_id,
                            $id
                        ));
                    }
                    $q->execute();
                }
                else
                {
                    // update current price
                    $query = Db::update('shop_prices')
                            ->set( array('price' => $price) )
                            ->where('currencies_id', '=', $currencies_id )
                            ->where('price_categories_id', '=', $item['id'] )
                            ->and_where('good_id', '=', $id)
                            ->execute()
                    ;
                    
                }
            }
        }
        
    }

    function deleteGood($id)
    {
        $query = DB::delete('shop_goods');

        if (is_numeric($id))
        {
            $query->where('id', '=', $id); // ->limit(1)
            $total_rows = $query->execute();

            return 1;
        }else
            return 0;
    }

    function deleteCurrency($id = NULL)
    {
        $query = DB::delete('shop_currencies');

        if (is_numeric($id))
        {
            $query->where('id', '=', $id); // ->limit(1)
            $total_rows = $query->execute();
            return 1;
        }else
            return 0;
    }

    function saveCurrency($data)
    {
        Kohana_Log::instance()->add('soap save currency', Kohana::dump($data));
        if (!$post = json_decode($data, TRUE))
        {
            return new SoapFault("error", "Ошибка разбора soap");
        }
        if (!$id = $post['id'])
        {
            return new SoapFault("error", "не задан ай-ди");
        }
        /* может есть уже? */
        $res = Model_Currencies::load($post['id']);

        if ((int) @$res['id'])
        { // update
            DB::update('shop_currencies')
                    ->set(
                            array(
                                'name' => $post['name'],
                                'code' => $post['code'],
                                'iso_code' => $post['iso_code'],
                                'rate' => str_replace(',', '.', $post['rate']),
                                'rate_w_vat' => str_replace(',', '.', $post['rate_w_vat']),
                            )
                    )
                    ->where('id', '=', $post['id'])
                    ->execute();
        }
        else
        { // create
            DB::insert('shop_currencies',
                            array(
                                'name', 'code', 'iso_code', 'rate', 'rate_w_vat',
                            )
                    )
                    ->values(
                            array(
                                $post['name'],
                                $post['code'],
                                $post['iso_code'],
                                str_replace(',', '.', $post['rate']),
                                str_replace(',', '.', $post['rate_w_vat']),
                            )
                    )
                    ->execute();
        }

        return 1;
    }

    function editOrder($data)
    {
        /*
          array(
          'id',
          'status',
          'goods_list',
          );
         */
Kohana_Log::instance()->add('editOrder', Kohana::dump($data));
        if (!$post = json_decode($data, TRUE))
            return new SoapFault("error", "Ошибка разбора soap");
        if (!@$post['id'])
            return new SoapFault("error", "Ошибка разбора id");

        /* get cur order */
        $order = Model_Orders::load($post['id']);
        /**/

        $total_sum = 0;

        //Kohana_Log::instance()->add('d', Kohana::debug($post));
        foreach ($post['goods_list'] as $key => $val)
        {
            $info = Model_Goods::load($key);
            //$info['scu'] = isset($info['scu']) ? $info['scu'] : '0';
            $goods_info[] = array(
                'id' => $info['id'],
                'scu' => $info['scu'],
                'name' => $info['name'] ? $info['name'] : $info['alter_name'],
                'count' => $val,
                'price' => $info['prices'][Kohana::config('shop')->default_category][Kohana::config('shop')->frontend_currency],
                'discount' => 0,
                'sum' => $val * $info['prices'][Kohana::config('shop')->default_category][Kohana::config('shop')->frontend_currency],
            );

            $total_sum += $val * $info['prices'][Kohana::config('shop')->default_category][Kohana::config('shop')->frontend_currency];
        }
        $post['exported1c'] = isset($post['exported1c']) ? $post['exported1c'] : 1;
        $update = array(
            'status' => $post['status'],
            'exported1c' => $post['exported1c'],
            'subtotal' => $total_sum,
            'sum' => ($total_sum + $order['shipment_cost']),
//                    'discount' => ,
//                    'tax' => ,
//                    'total_cost' => ,
        );

        if ($goods_info)
            $update['ser_details'] = serialize($goods_info);

        DB::update('shop_orders')
                ->set($update)
                ->where('id', '=', $post['id'])
                ->execute()
        ;

//        return $post['id'];
        return 1;
    }

    function deleteOrder($id)
    {
        /*
          'id',
         */
        $query = DB::delete('shop_orders');

        if (is_numeric($id))
        {
            $query->where('id', '=', $id); // ->limit(1)
            $total_rows = $query->execute();
            return 1;
        }else
            return 0;

        return 1;
    }

    function getOrders()
    {
		Kohana_Log::instance()->add('soap запрос заказов', '1');
        $query = DB::select('*')
                        ->from(array('shop_orders', 'orders'))
                        ->where('exported1c', '=', 0)
        ;

        $result = $query
                        ->order_by('id', 'DESC')
                        ->execute();

//        if ($result->count() == 0)  return new SoapFault("error", "а заказов то новых нет");
        if ($result->count() == 0)
            return 0;
        else
            $res = $result->as_array();

        $shipment_methods = Model_Shop::shipment_methods();
//        DB::update('shop_orders')
//            ->set(array('exported1c' => 1))
//            ->execute()
//            ;

        $return = '';
//        Kohana_Log::instance()->add('ret', print_r($res,1));
        foreach ($res as $key => $item)
        {

            if (isset($shipment_methods[$item['shipment_method_id']]['name']))
            {
                $item = array_merge(
                                array('shipment_method_name' => $shipment_methods[$item['shipment_method_id']]['name']),
                                $item
                );
            }

            $return .= "{\"";
            foreach ($item as $kkey => $iitem)
            {
                if ($kkey == 'ser_details' || $kkey == 'ser_status_history')
                {
                    $str = '';
                    $ser = unserialize($iitem);
                    if ($ser):
                        $str .= "{";
                        foreach ($ser as $keygood => $itemGood)
                        {
                            foreach ($itemGood as $kkeygood => $iitemGood)
                            {
                                $str .= "\"$kkeygood\":\"$iitemGood\", ";
                            }
                        }
                        $str = rtrim($str,', ') . "}, ";
                    endif;
                    $return .= $kkey.'":'.str_replace('&quot;','',$str).' "';
                }
                else
                {
                    $return .= ''.$kkey.'":"'.str_replace('&quot;','',$iitem).'", "';
                }
            }
            $return = rtrim($return,', "') . "}\r\n";
        }

        //$e = new SoapFault("error", "error message");
//        file_put_contents(DOCROOT . '/file.txt', $return . "\r\n");
//Kohana_Log::instance()->add('orders', Kohana::dump(json_encode($res)));
        //return $e;
//        $res = json_encode($res);
//        $res = mb_convert_encoding($res, 'cp1251', 'utf-8');
        return $return;
    }
    
    public function getCount()
    {        
		Kohana_Log::instance()->add('soap запрос количества товаров', '1');
        $priceCategoryId = 1;
        $prices = DB::select( array('sp.good_id', 'good_id'), array('sp.price', 'price')  )
                ->from( array('shop_prices', 'sp') )
                ->cached(240)
                ->execute()                
                ->as_array('good_id', 'price')
        ;
        
        
        $result = DB::select( array('sg.id','id'), array('sg.quantity', 'quantity')  )
                ->from( array('shop_goods', 'sg') )
                ->where('status','!=','unverified')
                ->where('status','!=','out_of_store')
                ->cached(240)
                ->execute()                
        ;
        
        if( $result->count() == 0 ) return array();
        $return = array();        
        foreach( $result->as_array() as $item )
        {   
            $return[$item['id']] = array('quantity'=>$item['quantity'],'price'=> Arr::get($prices, $item['id'], "-1") );
        }
        return json_encode($return);
    }

}

?>
