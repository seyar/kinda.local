<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 04.06.2010 - 15:36:32 Exp $
 *
 * Project:     microline.shade
 * File:        init.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

// Frontned Routes

Route::set
        (
	    'soap1c_front_view_client',
	    '(<lang>/)soap1c-client(/<action>)',
	    array( 'lang' => '(?:'.LANGUAGE_ROUTE.')' )
        )
        ->defaults
        (
            array(
		    'controller'    => 'soap1cclient',
		    'action'        => 'index',
                    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

Route::set
        (
	    'soap1c_front_view_server',
	    '(<lang>/)soap1c-server(/<action>)',
	    array( 'lang' => '(?:'.LANGUAGE_ROUTE.')' )
        )
        ->defaults
        (
            array(
		    'controller'    => 'soap1cserver',
		    'action'        => 'service',
                    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;
?>