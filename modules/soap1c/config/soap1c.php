<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 04.06.2010 - 15:37:28 Exp $
 *
 * Project:     microline.shade
 * File:        1c.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
return array
(
	'lang'		=> 'ru', // Default the  language.
	'path'		=> dirname(__FILE__) . '/../views/', // Admin templates folder.
	'module_root'		=> dirname(__FILE__) . '/../', // Admin templates folder.
);

?>