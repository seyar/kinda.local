<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminModulesControl extends Model
{

    protected $_moduleDirectory = "modules";
    static protected $_installedModules = array();

    static public function get_installed()
    {
        if (!empty(self::$_installedModules))
            return self::$_installedModules;

        $query = DB::select('id', 'name', 'fs_name', 'selectedwidget')
                        ->from('modules')
                        ->order_by('fs_name')
                        ->cached(120)
                        ->as_assoc()
        ;

        $result = $query->execute();
        if ($result->count() == 0)
            return array();
        else
        {
            $return = array();
            foreach ($result->as_array() as $value)
            {
                $return[$value['id']] = array(
                    'name' => I18n::get($value['name']),
                    'fs_name' => $value['fs_name'],
                    'selectedwidget' => $value['selectedwidget'],
                    'id' => $value['id']
                );
            }

            self::$_installedModules = $return;
            return $return;
        }
    }

    public function get_inactive()
    {

    }

    static function modulesPanel()
    {
        $query = DB::select('id', 'panel_serialized')
                        ->from('modules')
                        ->order_by('id')
                        ->cached(120)
                        ->as_assoc()
        ;

        $result = $query->execute();
        if ($result->count() == 0)
            return array();
        else
        {
            $result = $result->as_array('id');

            $return = array();

            foreach ($result as $key => $value)
            {
                $options = unserialize($value['panel_serialized']);

                if (is_array($options))
                {
                    $options['info']['name'] = I18n::get($options['info']['name']);

                    foreach ($options['links'] as $key => $value)
                        $options['links'][$key]['name'] = I18n::get($options['links'][$key]['name']);

                    $return[] = $options;
                }
            }

            return $return;
        }
    }

    static public function find($name)
    {
        $res = DB::select('id')
                        ->from('modules')
                        ->where('fs_name', '=', $name)
                        ->cached(30)
                        ->execute()
                        ->as_array()
        ;
        return $res[0]['id'];
    }

    public function installModule($moduleName)
    {
        $moduleParams = $this->getModuleParams($moduleName);

        if ($moduleParams === false)
            return false;

        DB::insert(
                        'modules',
                        array(
                            'name',
                            'fs_name',
                            'panel_serialized'
                        )
                )
                ->values(
                        array(
                            $moduleParams['name'],
                            $moduleParams['fs_name'],
                            $moduleParams['panel_serialized']
                        )
                )->execute();

        //updating userModules file
        $сonfigFiles = Kohana::find_file('config', 'userModules');
        $userModules = require $сonfigFiles[0];

        $userModules[$moduleName] = "{{MODPATH}}" . $moduleName;

        $content = "<?php defined('SYSPATH') OR die('No direct access allowed.');\n\nreturn "
                . var_export($userModules, true) . ';';
        $content = str_replace("'{{MODPATH}}", "MODPATH.'", $content);
        file_put_contents($сonfigFiles[0], $content);


        if ($moduleParams['useDatabase'])
        {
            $sql = file_get_contents($this->_getFile($moduleName, 'install.sql'));
            $this->execSQL($sql);
        }
    }

    public function getInstallableModules()
    {
        $directory = new DirectoryIterator(DOCROOT . '/' . $this->_moduleDirectory);

        $modules = array();
        foreach ($directory as $dir)
        {
            if ($dir->isDir() && substr($dir->getFilename(), 0, 1) !== '.')
            {
                $moduleName = $dir->getFilename();
                $moduleParams = $this->getModuleParams($moduleName);


                if ($moduleParams !== false)
                {
                    $moduleParams['name'] = I18n::get($moduleParams['name']);
                    $modules[] = $moduleParams;
                }//
            }
        }
        return $modules;
    }

    public function getModuleParams($moduleName)
    {
        $installFile = $this->_getFile($moduleName);
        if (!array_key_exists($moduleName, Kohana::modules()) && file_exists($installFile))
        {
            return require $installFile;
        }
        else
        {
            return false;
        }
    }

    private function _getFile($moduleName, $fileName = 'install.php')
    {
        return DOCROOT . '/' . $this->_moduleDirectory . '/' . $moduleName . "/install/" . $fileName;
    }

    public function execSQL($sql)
    {
        $sql = preg_replace("#\/\*.*?\*\/;?#m", '', $sql);
        $sql = preg_replace("/#.*$/m", '', $sql);
        $sql = preg_replace("/\s{2,}/m", '', $sql);
        preg_match_all("#.+?\;(?=\s*(?:INSERT|DELETE|CREATE|DROP|ALTER|$))#ims", $sql, $matches);
        foreach ($matches[0] as $sql)
        {
            mysql_query($sql) or die(mysql_error());
        }
    }

    public function uninstallModule($id)
    {
        $moduleName = DB::select('fs_name')
                        ->from('modules')
                        ->where('id', '=', $id)
                        ->execute()
                        ->as_array();
        $moduleName = $moduleName[0]['fs_name'];

        $сonfigFiles = Kohana::find_file('config', 'userModules');
        $userModules = require $сonfigFiles[0];
        unset($userModules[$moduleName]);
        $content = "<?php defined('SYSPATH') OR die('No direct access allowed.');\n\nreturn "
                . var_export($userModules, true) . ';';
        file_put_contents($сonfigFiles[0], $content);

        DB::delete('modules')
                ->where('id', '=', $id)
                ->execute();

        $moduleParams = $this->_getFile($moduleName);
        if ($moduleParams['useDatabase'])
        {
            $sql = file_get_contents($this->_getFile($moduleName, 'uninstall.sql'));
            $this->execSQL($sql);
        }
    }

}