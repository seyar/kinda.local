<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminRoleRules extends Model {

    static public function role_load()
    {
	$query = DB::select()->order_by('name', 'DESC');
	$roles = Sprig::factory('role')->load($query, FALSE);

	$return =array();

	foreach($roles->as_array() as $key=>$role)
	{
	    $return[$role->id] = array(
				    'id'=>$role->id,
				    'name'=>$role->name,
				);
	}
	return $return;
    }


    static public function getRules()
    {
        $res = DB::select()
                ->from('aacl_rules')
                ->execute()
                ->as_array();

        foreach($res as $rule)
        {
            if($rule['action'] === null) $rule['action'] = "*";
            $ret[$rule['role_id']][$rule['resource']][$rule['action']] = $res;
        }
        return $ret;
    }

    static public function getResources()
    {
        $files = Kohana::find_file('config', 'resources');

        $resources = array();
        foreach($files as $file)
        {
            $_resources = require($file);
            foreach($_resources as &$resource)
            {
                $resource['name'] = I18n::get($resource['name']);
                foreach($resource['actions'] as &$action)
                {
                    $action['name'] = I18n::get($action['name']);
                }
            }

            $resources = array_merge($resources, $_resources);
        }
        return $resources;
    }

//    public function role_rule_add($id = NULL)
//    {
//        if( is_numeric($id) && is_numeric($_GET['role']) )
//        {
//            $query = Db::insert('roles_users', array(
//                                                        'user_id',
//                                                        'role_id'
//                                                    ))
//                        ->values(
//                                array(
//                                        $id,
//                                        (int)$_GET['role']
//                                    )
//                                )
//        ;
//            if( $query->execute() )
//                echo '1';
//        }
//        else
//        {
//            return false;
//        }
//
//    }
//
//    public function role_rule_rem($id = NULL)
//    {
//        if( is_numeric($id) && is_numeric($_GET['role']) )
//        {
//            $query = Db::delete('roles_users')
//                        ->where('user_id', '=', $id)
//                        ->and_where('role_id', '=', (int)$_GET['role'])
//        ;
//            if( $query->execute() )
//                echo '1';
//        }
//        else
//        {
//            return false;
//        }
//
//    }

}