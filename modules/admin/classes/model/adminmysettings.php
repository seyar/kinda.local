<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminMySettings extends Model
{

    static public function settings_load($id = NULL)
    {
        if (is_numeric($id))
	{
	    $user = Sprig::factory('user');
	    $user->values(array('id'=>$id))->load(NULL, 1);
            
	    return $user->as_array();
	}
	return array();
    }

    public function settings_save($id = NULL)
    {
        if( $post = Model_AdminUser::user_validate() )
        {
            $user = Sprig::factory('user');
            $user->values(array('id'=>$id))->load(NULL, 1);
            if ($post['password'] && $user->password == sha1($post['oldpassword'])  && $post['password'] == $post['password_confirm'])
            {
                $user->password = $user->password_confirm = sha1($post['password']);
            }

            $user->language = $post['language'];
            $user->email = $post['email'];
            $user->fullname = $post['fullname'];
//            $user->status = $post['status'];

            $user->update();

            return true;
        }
        else {
            return false;
        }

    }

    public function user_validate()
    {
        $keys = array ('username', 'password', 'oldpassword', 'password_confirm', 'email', 'language', 'fullname', );
        $params = Arr::extract($_POST, $keys, NULL);

        $post = Validate::factory($params)

                ->rule('username', 'not_empty')
                ->rule('username', 'min_length', array(4) )
                ->rule('username', 'max_length', array(64) )

                ->rule('email', 'not_empty' )
                ->rule('email', 'email' )

                ->rule('language', 'not_empty' )
                ->rule('language', 'min_length', array(2) )

                ->rule('fullname', 'not_empty' )
                ->rule('fullname', 'max_length', array(255) )
        ;

        if ($params['password_confirm'] != $params['password'])
            $post->rule('password_confirm', 'matches', array('password'));
        
        if ($post->check()) {
            return $params;
        }
        else {
            $this->errors = $post->errors('validate');

        }
    }

}