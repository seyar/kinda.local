<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminGlobalBlocks extends Model
{

    public function gb_show($languages_id = NULL, $where = NULL)
    {
        $query = DB::select()->from('globalblocks');

        if (is_numeric($languages_id))
            $query->where('languages_id', '=', $languages_id);

        if ($where)
        {
            foreach ($where as $key => $item)
                $query->and_where($key, '=', $item);
        }

        $result = $query->cached(60)->execute();

        if ($result->count() == 0)
            return array();
        else
            return $result->as_array();
    }

    static function gb_pagination($languages_id = NULL)
    {
        $return = array('', array(), 0);

        // Get and save to cookes current page and sort column / order

        $limit = Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10);

        $page = Arr::get($_GET, 'page', Cookie::get(Request::instance()->controller . '_page'), 1);
        $order_by = Arr::get($_GET, 'order_by', Cookie::get(Request::instance()->controller . '_order_by'));
        $order_direction = Arr::get($_GET, 'order_direction', Cookie::get(Request::instance()->controller . '_order_direction'));

        $_GET['page'] = $page;

        Cookie::set('rows_per_page', $limit);

        Cookie::set(Request::instance()->controller . '_page', $page);
        Cookie::set(Request::instance()->controller . '_order_by', $order_by);
        Cookie::set(Request::instance()->controller . '_order_direction', $order_direction);


        $query_c = DB::select(Db::expr('COUNT(*) AS count'))
                        ->from('globalblocks');
        $query = DB::select('id', 'name', 'description')
                        ->from('globalblocks');

        if (!empty($order_by))
        {
            if (!empty($order_direction))
            {
                $query->order_by($order_by, $order_direction);
            }
            else
                $query->order_by($order_by, 'ASC');
        }

        if (is_numeric($languages_id))
        {
            $query_c->where('languages_id', '=', $languages_id);
            $query->where('languages_id', '=', $languages_id);
        }

        $result_c = $query_c->cached(30)
                        ->execute()
                        ->current();

        // if there is no page with number $page
        if (($page-1)*$limit>$result_c['count'])
        {
            $_GET['page']  = $page = ceil($result_c['count'] / $limit);
        }

        $result = $query
                        ->limit($limit)
                        ->offset(($page-1)*$limit)
                        ->cached(30)
                        ->execute();

        if ($result->count())
        {
            $return = array(Pagination::factory(
                        array(
                            'total_items' => $result_c['count'],
                            'items_per_page' => $limit,
                            'view' => 'pagination/modern',
                            'auto_hide' => TRUE,
                        )
                )->render()
                , $result->as_array('id')
            );
        }
        return $return;
    }

    static public function gb_frontend_load($languages_id = NULL)
    {
        $query = DB::select()->from('globalblocks');

        if (is_numeric($languages_id))
            $query->where('languages_id', '=', $languages_id);

        $result = $query->cached(120)->execute();
        if ($result->count() == 0)
            return array();
        else
            return $result->as_array('name', 'content');
    }

    public function gb_load($id = NULL)
    {
        $query = DB::select()->from('globalblocks')->limit(1);

        if ((int) $id)
        {
            $query->where('id', '=', $id);

            $result = $query->execute();
            if ($result->count() == 0)
                return array();
            else
                return $result->current();
        }
        else
        {
            return array();
        }
    }

    public function gb_save($lang, $id = NULL)
    {
        if ($post = Model_AdminGlobalBlocks::gb_validate())
        {
            if ((int) $id)
            { // update
                DB::update('globalblocks')
                        ->set(
                                array(
                                    'name' => Controller_Admin::to_url($post['block_name']),
                                    'content' => $post['block_content'],
                                    'description' => $post['block_description'],
                                    'languages_id' => $lang,
                                )
                        )
                        ->where('id', '=', $id)
                        ->execute();
            }
            else
            { // create
                list($id, $total_rows) = DB::insert('globalblocks', array('name', 'content', 'description', 'languages_id',))
                                ->values(array(Controller_Admin::to_url($post['block_name']), $post['block_content'], $post['block_description'], $lang,))
                                ->execute();
            }

            return $id;
        }
        else
        {
            return false;
        }
    }

    public function gb_validate()
    {
        $keys = array('block_name', 'block_content', 'block_description');
        $params = Arr::extract($_POST, $keys, NULL);

        $post = Validate::factory($params)
                        ->rule('block_name', 'not_empty')
                        ->rule('block_name', 'min_length', array(2))
                        ->rule('block_name', 'max_length', array(255))
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            Messages::add($post->errors('validate'));
        }
    }

    public function gb_copy($id = NULL)
    {

        if (is_numeric($id))
        {
            $resut = DB::select('name')->from('globalblocks')->limit(1)->where('id', '=', $id)->execute()->as_array();
            $query = DB::query(Database::INSERT, ' INSERT INTO globalblocks (name, content, description, languages_id) ' .
                                    '  SELECT CONCAT_WS("_", t2.name, t3.s_sum ) AS name, t2.content, t2. description, t2.languages_id ' .
                                    '  FROM globalblocks AS t2 ' .
                                    '  LEFT JOIN ( ' .
                                    '     SELECT COUNT(*)+1 AS "s_sum", :id AS id FROM globalblocks AS tc WHERE name LIKE "' . $resut[0]['name'] . '%" ' .
                                    '  ) AS t3 ON (t3.id = t2.id) ' .
                                    '  WHERE t2.id=:id')
                            ->bind(':id', $id);

            $result = $query->execute();
        }
    }

    public function gb_delete($id = NULL)
    {
        $query = DB::delete('globalblocks');

        if (is_numeric($id))
        {
            $query->where('id', '=', $id); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function gb_delete_list($array)
    {
        $query = DB::delete('globalblocks');

        if (count($array))
        {
            $query->where('id', 'in', array_flip($array)); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function copy2otherlang($forLang = NULL, $id, $lang_id = 1)
    {
        $block = $this->gb_load($id);
        unset($block['id']);
        if ($forLang)
        {
            foreach ($forLang as $key => $item)
            {
                $block['languages_id'] = $key;
                $gb_block = $this->gb_show(NULL, array('name' => $block['name'], 'languages_id' => $key));
                if (count($gb_block) === 0 && $item == 1)
                {
                    DB::insert('globalblocks', array(
                                'name', 'content', 'description', 'languages_id',
                            ))
                            ->values(array(
                                $block['name'], $block['content'], $block['description'], $key,
                            ))
                            ->execute()
                    ;
                }
            }
        }
        return 1;
    }

}