<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_Admin extends Model
{
    public $_table;
    
    public function get_role_id()
    {
        // Authlite instance
        $this->authlite = Authlite::instance('authlite');

        // login check
        if( !$this->authlite->logged_in() && Router::$method != 'login' )
        {
            url::redirect(Router::$controller . '/login');
        }
        else
        {
            // assigns the user object
            $this->user = $this->authlite->get_user();
        }
    }

    
    /**
     *
     * @param array $columns    - selectable columns array('id','name')
     * @param array $where      - and where condition array(array('id','=','5'))
     * @param int $cache        - 0 or int number of seconds cache
     * @param bool $as_array    - if TRUE retrurn array, FALSE - return current, 'as_object' - return Database_Result object
     * @param string,int $key   - return array keys 
     * @param string,int $value   - return array value 
     * @param array $order_by   - order by Array( column, desc )
     * @return array 
     */
    public function getItems( $columns = array(), $where = array(), $cache = 0, $as_array = TRUE, $key = NULL, $value = NULL, $order_by = NULL, $limit = NULL )
    {
        $return = array();
        
        $columns = !empty($columns) ? DB::expr(mysql_escape_string(implode(',',$columns))) : DB::expr('*');
        $query = DB::select( $columns )
                ->from($this->_table)
                ;
        
        if( !empty($order_by) )
        {
            $query->order_by($order_by[0],$order_by[1]);
        }
        
        if( !empty($where) )
        {
            foreach( $where as $item )
                $query->and_where( $item[0], $item[1], $item[2] );
        }
        
        if( !empty($limit) )
        {
            $query->limit($limit);
        }
        
        $query = $query->cached($cache);
        
        if( $as_array === 'as_object' ) return $query;
        $query = $query->execute();
        
        if( $query->count() == 0 ) return $return;
        if( $as_array === TRUE )
            $return = $query->as_array($key, $value);
        else
            $return = $query->current();
        
        return $return;
    }
    
    /**
     * insert data into table $_table
     * 
     * @param array $data
     * @return array($insert_id, $total_rows) 
     */
    public function insertItem( $data = array() )
    {
        if( empty($data) ) return array(0,0);
        $query = DB::insert( $this->_table, array_keys($data) )
                ->values($data)
                ->execute()
                ;
        return $query;
    }
    
    /**
     * insert data into table $_table
     * 
     * @param array $data
     * @param array $keys
     * @return array($insert_id, $total_rows) 
     */
    public function insertItems( $data = array(),$keys = array() )
    {
        if( empty($data) ) return array(0,0);
        
        $query = DB::insert( $this->_table, $keys );
        
        foreach( $data as $item )
        {
            $query->values($item);
        }
        $query->execute();
        
        return $query;
    }
    
    /**
     * update data into table $_table
     * 
     * @param array $data
     * @param array $where and where condition array(array('id','=','5'))
     * @return $total_rows
     */
    public function updateItem( $data = array(), $where = array() )
    {
        if( empty($data) ) return 0;
        if( empty($where) ) return false;
        
        $query = DB::update( $this->_table )
                ->set($data)
                ;
        
                if( !empty($where) )
                {
                    foreach( $where as $item )
                        $query->and_where( $item[0], $item[1], $item[2] );
                }
                
        $query->execute();
        return $query;
    }
    
    /**
     * delete item(s) from table
     * 
     * @param array $where and where condition array(array('id','=','5'))
     * @return int affected rows
     */
    public function deleteItem( $where = array() )
    {
        $return = 0;
        if( empty($where) && count($where) == 0 ) return $return;
        
        $query = DB::delete( $this->_table );
        
        foreach( $where as $item )
            $query->and_where( $item[0], $item[1], $item[2] );
        
        $return = $query->execute();
        
        return $return;
    }
    
    static public function pagination(Database_Query_Builder_Select $table, $limit = 10, $page = NULL, $cached = 0, $paginationTemplate = 'modern' )
    {
        $return = array('', array(), 0);
        $paginationTemplate = 'pagination/'.$paginationTemplate;
        // Get and save to cookes current page and sort column / order
        $page = $page ? $page : Arr::get($_GET, 'page', 1);

//        $_GET['page'] = $page;
        
        $selectExt = new Kohana_SelectExt();
        $selectExt->extend( $table );
        $selectExt->setSelect( array(Db::expr('COUNT(*) AS count')) );
        $query_c = $selectExt;
        $query = $table;

        $result_c = $query_c
//                        ->group_by('id')
                        ->execute()
                ;
        
        $countAllRecords = $result_c->get('count');
        
        // if there is no page with number $page                
        if( ($page-1)*$limit > $countAllRecords )
        {
            $page = ceil($countAllRecords / $limit);
        }
        
        $page = $page <=0 ? 1 : $page;
        
        $result = $query
                        ->limit($limit)
                        ->offset(($page-1)*$limit)
                        ->cached($cached)
                        ->execute()
                        ;

        if ($result->count() > 0)
        {
            $pagination = Pagination::factory(
                        array(
                            'previous_page' => '‹',
                            'next_page' => '›',
                            'total_items' => $countAllRecords,
                            'items_per_page' => $limit,
                            'view' => $paginationTemplate,
                            'auto_hide' => TRUE,
                        )
                );
            
            $return = array(
                $result->as_array('id')
                , $pagination->render()
                , $pagination->total_pages
                , $countAllRecords
            );
        }else
        {
			if( $page != 1 )
			{
//				 Cookie::set(Request::instance()->controller . '_page', 1);
//				 Request::instance()->redirect( ADMIN_PATH . Request::instance()->controller );
			}
		}

        return $return;
    }
}