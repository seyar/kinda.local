<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminShortcut extends Model
{

    static function shortcuts_list()
    {        
        $result = Db::select()->from('users_shortcuts')
                        ->order_by('id', 'asc')
                        ->where('user_id', '=', $_SESSION['user_id'])
                        ->execute()
        ;

        if ($result->count() == 0)
            return array();
        else
            return $result->as_array();
    }

    static function shortcuts_delete($id)
    {
        if (is_numeric($id))
        {
            $result = Db::delete('users_shortcuts')
                            ->where('id', '=', $id)
                            ->execute()
            ;

            echo '1';
        }
        else
            echo '0';
    }

    static function shortcuts_add($name=NULL, $link=NULL)
    {
        if ($name && $link)
        {
            $name1 = $name2 = '';
            list($name1, $name2) = explode(':', $name);
            $name = trim($name2);

            $res = DB::select()
                            ->from('users_shortcuts')
                            ->where('link', '=', $link)
                            ->execute();

            if ($res->count())
            {
                echo json_encode(array('result' => 2));
                return;
            }

            $result = Db::insert('users_shortcuts',
                                    array
                                        (
                                        'user_id',
                                        'name',
                                        'link',
                                    )
                            )
                            ->values(
                                    array
                                        (
                                        $_SESSION['user_id'],
                                        $name,
                                        $link,
                                    )
                            )
                            ->execute()
            ;

            echo json_encode(array('result' => 1, 'name' => $name));
        }
        else
            echo json_encode(array('result' => 1));
    }

}
