<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminTemplates extends Model
{

    static public function get_list()
    {
        $info = array();
        $files = scandir(TEMPLATEPATH);

        foreach ($files as $key => $value)
        {
            if ($value != '.' && $value != '..' && $value != '.svn' && is_dir(TEMPLATEPATH . $value) && $value != 'errors')
            {
                $template = (array) Model_AdminTemplates::get_info($value);
                if($template)
                    $info[$value] = array('name' => $template['name'], 'creationDate' => $template['creationDate'], 'author' => $template['author'],
                        'authorEmail' => $template['authorEmail'], 'authorUrl' => $template['authorUrl'], 'version' => $template['version'],);
            }
        }

        return $info;
    }

    static public function get_info($template_name)
    {
        if(!is_readable(TEMPLATEPATH . $template_name . '/info.xml')) return array();
        $info = simplexml_load_file(TEMPLATEPATH . $template_name . '/info.xml');
        return $info;
    }

    /*
     * Usage :
     * 	    Model_AdminTemplates::set_info($template_name, 'default', 'new_default.tpl' )
     */

    static public function set_info($template_name, $key, $value)
    {
        $info = @simplexml_load_file(TEMPLATEPATH . $template_name . '/info.xml');

        foreach ($info->xpath($key) as $item)
        {
            $item[0] = $value;
        }
        if (file_put_contents(TEMPLATEPATH . $template_name . '/info.xml', $info->asXML()))
            return true;
        else
            return false;
    }

    /*
     * Usage :
     * 	    Model_AdminTemplates::add_info($template_name, 'css', 'filename', 'new.css' )
     */

    static public function add_info($template_name, $key, $children, $value)
    {
        $info = @simplexml_load_file(TEMPLATEPATH . $template_name . '/info.xml');

        foreach ($info->xpath($key) as $item)
        {
            $item->addChild($children, $value);
        }

        if (file_put_contents(TEMPLATEPATH . $template_name . '/info.xml', $info->asXML()))
            return true;
        else
            return false;
    }

    /*
     * Usage :
     * 	    Model_AdminTemplates::del_info($template_name, 'css/*, 'new.css' )
     */

    static public function del_info($template_name, $key, $value)
    {
        $info = @simplexml_load_file(TEMPLATEPATH . $template_name . '/info.xml');

        foreach ($info->xpath($key) as $seg)
        {
            if ((string) $seg == $value)
            {

                $dom = dom_import_simplexml($seg);
                $dom->parentNode->removeChild($dom);

                unset($seg);
            }
        }

        if (file_put_contents(TEMPLATEPATH . $template_name . '/info.xml', $info->asXML()))
            return true;
        else
            return false;
    }

}