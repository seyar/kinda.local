<?php
defined( 'SYSPATH' ) OR die( 'No direct access allowed.' );

class Model_AdminUser extends Model
{

    static public function login()
    {
        $user = Sprig::factory( 'user' );

        // Load rules defined in sprig model into validation factory
        $post = Validate::factory( $_POST )
                        ->rules( 'username', $user->field( 'username' )->rules )
                        ->rules( 'password', $user->field( 'password' )->rules );

        if( IN_PRODUCTION )
        {
 //           $post->rule( 'captcha', 'not_empty' )
 //                ->rule( 'captcha', 'Captcha::valid' )
            ;
        }

        // Validate the post
        if( $post->check() )
        {
            // Load the user by username and password
            $user->values( $post->as_array() )->load( NULL, 1 );
            
            Auth::instance()->login( $_POST['username'], $_POST['password'], false );
            
            if( !Auth::instance()->logged_in() )
            {
                $post->error( 'username', 'Invalid Login or Password' );
                // throw new Exception('error');
                return $post;
            }

            $user = Auth::instance()->get_user();            
            Cookie::set( 'user', $user->id );
            Session::instance()->set( 'user_id', $user->id );
            $user->last_login = time();
            $user->update();

        }

        return $post;
    }

    static public function logout()
    {
        // Delete the authorization
        Cookie::delete( 'user', 'user_last_url' );
        Session::instance()->destroy();

        // Redirect to the login page
        Request::instance()->redirect( substr( URL::base( TRUE, TRUE ), 0, -1 ) . ADMIN_PATH . '/login/' );
    }

    static public function user_roles( $id = NULL )
    {
        $query = DB::select()
                        ->from( 'roles' )
                        ->cached( 30 );
        if( is_numeric( $id ) )
        {
            $query->where( 'id', '=', $id );
        }

        $result = $query->execute()->as_array();

        if( count( $result ) == 0 )
            return array( );
        else
        {
            return $result;
        }
    }

    public function restore( $email )
    {
        $res = DB::select()
                        ->from( 'users' )
                        ->where( 'email', '=', $email )
                        ->execute();

        if( empty( $res ) )
        {
            return false;
        }

        $code = md5( 'aasdf' . rand( 0, 932993 ) . 'sss' . rand( 0, 9329923 ) );
        $res = DB::update( 'users' )
                        ->set(
                                array( 'lost_code' => $code )
                        )
                        ->where( 'id', '=', $res[0]['id'] )
                        ->execute();

        // отправляем письмецо
        $search = array( '%site%', '%email%', '%pass%', );
        $replace = array( $_SERVER['HTTP_HOST'], $email, 'http://' . $_SERVER['HTTP_HOST'] . '/admin/login/' . $code . '/sp/' );
        $filename = Kohana::config( 'admin' )->path . 'email_lost_code.tpl';
        $message = str_replace( $search, $replace, file_get_contents( $filename ) );

        if( !$res = Model_Feedback::send_mail( Kohana::config( 'feedback' )->send_from
                        , $email
                        , Kohana::config( 'admin' )->forgot_subject
                        , $message
        ) )
        {
            $this->errors = i18n::get( 'send mail error' );
            return false;
        }

        return true;
    }

    public function send_pass( $code )
    {
        $res = DB::select()
                        ->from( 'users' )
                        ->where( 'lost_code', '=', $code )
                        ->execute();
        if( empty( $res ) )
        {
            return false;
        }
        $res = $res[0];
        $pass = substr( uniqid( '', true ), 0, 6 );
        DB::update( 'users' )
                ->set(
                        array( 'password' => Auth::instance()->hash_password( $pass ) )
                )
                ->where( 'id', '=', $res['id'] )
                ->execute()
        ;

        // отправляем письмецо
        $search = array( '%site%', '%email%', '%pass%', );
        $replace = array( $_SERVER['HTTP_HOST'], $res['email'], $pass );
        $filename = Kohana::config( 'admin' )->path . 'email_new_pass.tpl';
        $message = str_replace( $search, $replace, file_get_contents( $filename ) );

        if( !$res = Model_Feedback::send_mail( Kohana::config( 'feedback' )->send_from
                        , $res['email']
                        , Kohana::config( 'admin' )->forgot_subject
                        , $message
        ) )
        {
            $this->errors = i18n::get( 'send mail error' );
            return false;
        }

        return true;
    }

    public function user_show()
    {
        $query = DB::select()->order_by( 'username', 'DESC' );
        $users = Sprig::factory( 'user' )->load( $query, FALSE );

        $return = array( );

        foreach( $users->as_array() as $key => $user )
        {
            $return[$user->id] = array(
                'id' => $user->id,
                'username' => $user->username,
                'email' => $user->email,
                'fullname' => $user->fullname,
                'logins' => $user->logins,
                'status' => $user->status,
            );

            if( count( $user->roles ) )
                foreach( $user->roles as $rk => $rv )
                    $return[$user->id]['roles'][$rk] = $rv->name;
        }
        return $return;
    }

    public function user_active( $id = NULL )
    {
        if( is_numeric( $id ) )
        {
            $status = $id == 1 ? "1" : "!status";

            DB::query( Database::UPDATE, "UPDATE users SET status = $status WHERE id = $id LIMIT 1" )
                    ->execute();
        }
        return true;
    }

    static public function user_load( $id = NULL )
    {
        if( $id )
        {
            $user = Sprig::factory( 'user' );
            $user->values( array( 'id' => $id ) )->load( NULL, 1 );

            $roles = array( );

            if( count( $user->roles ) )
                foreach( $user->roles as $rk => $rv )
                    $roles[$rv->id] = 1;

            $user = $user->as_array();
            $user['roles'] = $roles;

            return $user;
        }
        return array( );
    }

    public function user_save( $id = NULL )
    {
        if( $post = Model_AdminUser::user_validate() )
        {
            $user = Sprig::factory( 'user' );
            if( (int) $id )
            {
                $user->values( array( 'id' => $id ) )->load( NULL, 1 );
            }
            if( $post['password'] && $post['password'] == $post['password_confirm'] )
            {
//                $user->password = $user->password_confirm = sha1( $post['password'] );
                $user->password = $user->password_confirm = Auth::instance()->hash_password( $post['password'] );                
            }

            $user->language = $post['language'];
            $user->email = $post['email'];
            $user->fullname = $post['fullname'];
            $user->username = $post['username'];

            if( $id == 1 )
                $post['status'] = 1;

            $user->status = $post['status'];
            $user->logins = 0;
            $user->show_desc = ' ';
            $user->last_login = '';
            $user->roles = $_POST['roles'];
//            die;
            if( (int) $id )
                $user->update();
            else
                $user->create();

            return true;
        }
        else
        {
            return false;
        }
    }

    public function user_delete( $id = NULL )
    {
        $query = DB::delete( 'users' );

        if( is_numeric( $id ) && $id != 1 )
        {
            $query->where( 'id', '=', $id ); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function user_delete_list( $array = array( ) )
    {
        if( isset( $array[1] ) )
            unset( $array[1] );
        if( count( $array ) )
        {
            $query = DB::delete( 'users' );
            $query->where( 'id', 'in', array_flip( $array ) ); // ->limit(1)

            $total_rows = $query->execute();
        }
    }

    public function user_validate()
    {
        $keys = array( 'username', 'password', 'oldpassword', 'password_confirm', 'email', 'language', 'fullname', 'roles', 'status' );
        $params = Arr::extract( $_POST, $keys, NULL );

        $post = Validate::factory( $params )
                        ->rule( 'username', 'not_empty' )
                        ->rule( 'username', 'min_length', array( 4 ) )
                        ->rule( 'username', 'max_length', array( 64 ) )
                        ->rule( 'email', 'not_empty' )
                        ->rule( 'email', 'email' )
                        ->rule( 'language', 'not_empty' )
                        ->rule( 'language', 'min_length', array( 2 ) )
                        ->rule( 'fullname', 'max_length', array( 255 ) )
        ;

        if( $params['oldpassword'] !== null && !empty( $params['password_confirm'] ) )
            $post->rule( 'oldpassword', 'not_empty' );
        if( $params['password_confirm'] != $params['password'] )
            $post->rule( 'password_confirm', 'matches', array( 'password' ) );

        if( $post->check() )
        {
            return $params;
        }else
        {
            $this->errors = $post->errors( 'validate' );
        }
    }

}
