<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminRoleUsers extends Model {

    static public function role_user_load()
    {
	$query = DB::select()->order_by('username', 'DESC');
	$users = Sprig::factory('user')->load($query, FALSE);

	$return =array();

	foreach($users->as_array() as $key=>$user)
	{
	    $return[$user->id] = array(
				    'id'=>$user->id,
				    'username'=>$user->username,
				);

	    if (count($user->roles))
		    foreach($user->roles as $rk=>$rv)
			$return[$user->id]['roles'][$rv->id] = $rv->id;

	}
	return $return;
    }

    static public function role_user_add($id = NULL)
    {
        if( is_numeric($id) && is_numeric($_GET['role']) )
        {
            $query = Db::insert('roles_users', array(
                                                        'user_id',
                                                        'role_id'
                                                    ))
                        ->values(
                                array(
                                        $id,
                                        (int)$_GET['role']
                                    )
                                )
        ;
            if( $query->execute() )
                echo '1';
        }
        else
        {
            return false;
        }

    }

    static public function role_user_rem($id = NULL)
    {
        if( is_numeric($id) && is_numeric($_GET['role']) )
        {
            $query = Db::delete('roles_users')
                        ->where('user_id', '=', $id)
                        ->and_where('role_id', '=', (int)$_GET['role'])
        ;
            if( $query->execute() )
                echo '1';
        }
        else
        {
            return false;
        }

    }

}