<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminSysinfo extends Model
{

    public function info()
    {
        ob_start();
        phpinfo();
        $phpinfo = ob_get_contents();
        ob_end_clean();

        return str_replace('<hr', '<br',
                    str_replace('width="600"', 'width="100%"', str_replace(
                        "module_Zend Optimizer"
                        , "module_Zend_Optimizer"
                        , preg_replace('%^.*<div class="center">(.*)</div>.*$%ms', '$1', $phpinfo)
                    )
                )
            );
    }

}