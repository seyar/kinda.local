<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminRole extends Model {

    public function roles_show()
    {
	$query = DB::select()
		    ->from('roles')
//		    ->cached(5)
		    ;
        if ( isset($id) && is_numeric($id))
        {
            $query->where('id','=',$id);
        }
	
	$result = $query->execute()->as_array();
	
	if (count($result) == 0) return array();
	else {
            $return = array();
            foreach($result as $role)
            {
                $return[$role['id']] = array(
                                        'name'=>$role['name'],
                                        'description'=>$role['description'],
                                        'status'=>$role['status'],
                                    );
            }
            return $return;
	}

    }

    public function role_active($id = NULL)
    {
        if (is_numeric($id))
	{
            $status = $id == 1 ? "1" : "!status";
            
	    DB::query(Database::UPDATE, "UPDATE roles SET status = $status WHERE id = $id LIMIT 1")
		    ->execute();
	}
	return true;

    }

    static public function role_load($id = NULL)
    {
        if ($id)
        {
            $role = Sprig::factory('role');
            $role->values(array('id'=>$id))->load(NULL, 1);

            $role = $role->as_array();
            return $role;
        }
        return array();

    }

    public function role_save($id = NULL)
    {        
        if( $post = Model_AdminRole::role_validate() )
        {            
            $role = Sprig::factory('role');
            if( (int)$id )
            {
                $role->values(array('id'=>$id))->load(NULL, 1);
                if ($id == 1) $post['status'] = 1;
                $role->values($post);
                $role->update();
            }
            else
            {
                $role->values($post);
                $role->create();
            }
            
            return true;
        }
        else {
            return false;
        }

    }

    public function role_delete($id = NULL)
    {
        if (is_numeric($id) && $id != 1)
	{
            $query = DB::delete('roles');
            $query->where('id', '=', $id); // ->limit(1)

	    $total_rows = $query->execute();
	}
    }

    public function role_delete_list($array = array())
    {
        if(isset($array[1])) unset($array[1]);
        if (count($array))
	{
            $query = DB::delete('roles');
            $query->where('id', 'in', array_flip($array) ); // ->limit(1)
	    $total_rows = $query->execute();
	}
    }

    public function role_validate()
    {
        $keys = array ('name', 'description', 'status');
        $params = Arr::extract($_POST, $keys, NULL);

        $post = Validate::factory($params)

                ->rule('name', 'not_empty')
                ->rule('name', 'min_length', array(4) )
                ->rule('name', 'max_length', array(64) )

                ->rule('description', 'max_length', array(255) )
        ;
        
        if ($post->check()) {
            return $params;
        }
        else
        {
            Messages::add($post->errors('validate'));
            $this->errors = $post->errors('validate');

        }
    }

}