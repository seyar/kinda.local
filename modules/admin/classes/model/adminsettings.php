<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminSettings extends Model
{

    public function settings_save()
    {
        if ($post = $this->settings_validate())
        {
            $cache_objects = array();
            foreach (array('Images', 'JavaScript', 'CSS', 'HTML', 'SQL',) as $value)
            {
                $cache_objects[$value] = array_search($value, $post['cache_objects']) !== false ? true : false;
            }
            $post['cached_objects'] = $cache_objects;

            $settings = Kohana::$config->load('settings');

            $settings->set('site_name', $post['site_name'])
                    ->set('site_owner', $post['site_owner'])
                    ->set('admin_email', $post['admin_email'])
                    ->set('url_autofix', ($post['url_autofix'] ? TRUE : FALSE))
                    ->set('session_timeout', (int) $post['session_timeout'])
                    ->set('session_ip_lock', ($post['session_ip_lock'] ? TRUE : FALSE))
                    ->set('mail_from_name', $post['mail_from_name'])
                    ->set('mail_from_address', $post['mail_from_address'])
                    ->set('cache_lifetime', (int) $post['cache_lifetime'])
                    ->set('site_stop', ($post['site_stop'] ? TRUE : FALSE))
                    ->set('site_stop_message', $post['site_stop_message'])
                    ->set('cached_objects', $post['cached_objects'])
            ;


            if ($files = Kohana::find_file('config', 'settings'))
            {
                if ($file = $files[0])
                {
                    file_put_contents($file, "<?php defined('SYSPATH') OR die('No direct access allowed.');\n\nreturn "
                            . var_export($settings->as_array(), true) . ';');
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public function settings_validate()
    {
        $keys = array('site_name', 'site_owner', 'admin_email', 'url_autofix', 'session_timeout', 'session_ip_lock'
            , 'mail_from_name', 'mail_from_address', 'cache_lifetime', 'cache_objects'
            , 'site_stop', 'site_stop_message');

        $params = Arr::extract($_POST, $keys, NULL);

        $post = Validate::factory($params)
                        ->rule('site_name', 'not_empty')
                        ->rule('admin_email', 'not_empty')
                        ->rule('admin_email', 'email')
                        ->rule('site_name', 'not_empty')
                        ->rule('site_name', 'max_length', array(255))
                        ->rule('session_timeout', 'not_empty')
                        ->rule('session_timeout', 'digit')
                        ->rule('mail_from_name', 'not_empty')
                        ->rule('mail_from_name', 'max_length', array(255))
                        ->rule('mail_from_address', 'not_empty')
                        ->rule('mail_from_address', 'email')
                        ->rule('cache_lifetime', 'not_empty')
                        ->rule('cache_lifetime', 'digit')
                        ->rule('site_stop_message', 'not_empty')
                        ->rule('cache_lifetime', 'digit')

        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            $this->errors = $post->errors('validate');
        }
    }

    static function domains_list()
    {
        $result = Db::select('d.*', 'l.name')->from(array('domains', 'd'))
                        ->join(array('languages', 'l'))
                        ->on('l.id', '=', 'd.languages_id')
                        ->order_by('d.id', 'asc')
                        ->execute()
        ;

        if ($result->count() == 0)
            return array();
        else
            return $result->as_array();
    }

    static function domains_load($id)
    {
        if (is_numeric($id))
        {
            $result = Db::select()->from('domains')
                            ->where('id', '=', $id)
                            ->execute()
            ;

            if ($result->count() == 0)
                return array();
            else
                return $result->as_array();
        }
    }

    static function domains_delete($id)
    {
        if (is_numeric($id))
        {
            $result = Db::delete('domains')
                            ->where('id', '=', $id)
                            ->execute()
            ;

            echo '1';
        }
        else
            echo '0';
    }

    static function domains_save()
    {
        if ($post = Model_AdminSettings::domain_validate())
        {
            if ($post['dom_id'])
            {
                Db::update('domains')
                        ->set(
                                array(
                                    'http_host' => $post['dom_http_host'],
                                    'languages_id' => $post['dom_language'],
                                    'domain_title' => $post['dom_title'],
                                    'domain_keywords' => $post['dom_keywords'],
                                    'domain_description' => $post['dom_description'],
                                    'domain_template' => $post['dom_template'],
                                )
                        )
                        ->where('id', '=', $post['dom_id'])
                        ->execute();
            }
            else
            {
                Db::insert('domains', array('http_host', 'languages_id', 'domain_title', 'domain_keywords',
                            'domain_description', 'domain_template'))
                        ->values(
                                array(
                                    $post['dom_http_host'],
                                    $post['dom_language'],
                                    $post['dom_title'],
                                    $post['dom_keywords'],
                                    $post['dom_description'],
                                    $post['dom_template'],
                                )
                        )
                        ->execute();
            }
            echo '1';
        }
        else
            echo '0';
    }

    static public function domain_validate()
    {
        $keys = array('dom_id', 'dom_http_host', 'dom_language', 'dom_title', 'dom_keywords', 'dom_description', 'dom_template');
        $params = Arr::extract($_POST, $keys, NULL);

        $post = Validate::factory($params)
                        ->rule('dom_id', 'digit')
                        ->rule('dom_http_host', 'not_empty')
                        ->rule('dom_http_host', 'max_length', array(255))
                        ->rule('dom_language', 'not_empty')
                        ->rule('dom_language', 'digit')
                        ->rule('dom_title', 'not_empty')
                        ->rule('dom_title', 'max_length', array(255))
                        ->rule('dom_keywords', 'max_length', array(255))
                        ->rule('dom_description', 'max_length', array(255))
                        ->rule('dom_template', 'not_empty')
                        ->rule('dom_template', 'max_length', array(255))
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            echo '0';
            exit();
        }
    }

    static function languages_list()
    {
        $result = Db::select()->from('languages')
                        ->order_by('id', 'asc')
                        ->cached(5)
                        ->execute()
        ;

        if ($result->count() == 0)
            return array();
        else
            return $result->as_array();
    }

    static function languages_load($id)
    {
        if (is_numeric($id))
        {
            $result = Db::select()->from('languages')
                            ->where('id', '=', $id)
                            ->execute()
            ;

            if ($result->count() == 0)
                return array();
            else
                return $result->as_array();
        }
    }

    static function languages_delete($id)
    {
        if (is_numeric($id))
        {
            $result = Db::delete('languages')
                            ->where('id', '=', $id)
                            ->execute()
            ;

            echo '1';
        }
        else
            echo '0';
    }

    static function languages_save()
    {
        if ($post = Model_AdminSettings::lang_validate())
        {
            if ($post['lang_id'])
            {
                Db::update('languages')
                        ->set(
                                array(
                                    'name' => $post['lang_name'],
                                    'shortname' => $post['lang_shortname'],
                                    'prefix' => $post['lang_prefix'],
                                )
                        )
                        ->where('id', '=', $post['lang_id'])
                        ->execute();
            }
            else
            {
                Db::insert('languages', array('name', 'shortname', 'prefix'))
                        ->values(
                                array(
                                    $post['lang_name'],
                                    $post['lang_shortname'],
                                    $post['lang_prefix'],
                                )
                        )
                        ->execute();
            }
            echo '1';
        }
        else
            echo '0';
    }

    static public function lang_validate()
    {
        $keys = array('lang_id', 'lang_name', 'lang_prefix', 'lang_shortname');
        $params = Arr::extract($_POST, $keys, NULL);

        $post = Validate::factory($params)
                        ->rule('lang_id', 'digit')
                        ->rule('lang_name', 'not_empty')
                        ->rule('lang_name', 'max_length', array(127))
                        ->rule('lang_prefix', 'not_empty')
                        ->rule('lang_prefix', 'max_length', array(3))
                        ->rule('lang_shortname', 'not_empty')
                        ->rule('lang_shortname', 'max_length', array(3))
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            echo '0';
            exit();
        }
    }

}