<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminLayout extends Model {

    public function layout_show($template_name = NULL)
    {
	$layout_list =array();

	$template_info = Model_AdminTemplates::get_info(
		$template_name ? $template_name : Model_AdminDomains::domain_template($_SERVER['HTTP_HOST'])
	);

	foreach ($template_info->templates->filename as $key=>$item)
	{
	    $item = (string)$item;
	    $layout_list[] = $item;
	}

	return $layout_list;
    }

    public function layout_load($id = NULL)
    {
	if (is_numeric($id))
	{ // update

	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $layout_list = Model_AdminLayout::layout_show($template_name);

	    $content = file_get_contents(TEMPLATEPATH . $template_name . '/' .$layout_list[$id]);

	    return array('id'=>$id,
			'content'=>mb_convert_encoding(
				$content, 'UTF-8', mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true)
				),
			'filename' => $layout_list[$id],
			);
	}
	else
	{
	    return array('id'=>$id, 'content'=>'{include file="header.tpl"}', 'filename'=>'');
	}
    }

    public function layout_save($lang, $id = NULL)
    {
	if( $post = Model_AdminLayout::layout_validate() )
	{
	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $layout_list = Model_AdminLayout::layout_show($template_name);

	    if( is_numeric($id))
	    {
		$layout_filename = $layout_list[$id];
		@copy(TEMPLATEPATH . $template_name . '/' . $layout_filename
			, TEMPLATEPATH . $template_name . '/' . $layout_filename . '.bak' );
	    }
	    else
	    {
		$layout_filename = Controller_Admin::transliterate($_POST['layout_filename']);
		Model_AdminTemplates::add_info($template_name, 'templates', 'filename', $layout_filename );
	    }
	    
	    file_put_contents(TEMPLATEPATH . $template_name . '/' .$layout_filename, $post['content']);

	    return true;
	}
	else
	{
	    return false;
	}
    }

    public function layout_validate()
    {
	$keys = array ('layout_filename', 'content');
	$params = Arr::extract($_POST, $keys, NULL);

	$post = Validate::factory($params)
		    ->rule('layout_filename', 'not_empty')
		    ->rule('content', 'not_empty')
	;

        if ($post->check())
	{
	    return $params;
	}
	else
	{
	    $this->errors = $post->errors('validate');
	}

    }

    public function layout_copy($id = NULL)
    {

        if (is_numeric($id))
	{
	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $layout_list = Model_AdminLayout::layout_show($template_name);
	    $layout_filename = $layout_list[$id];
	    $layout_new_filename = 'copy_' . basename($layout_filename);

	    @copy(TEMPLATEPATH . $template_name . '/' . $layout_filename
			, TEMPLATEPATH . $template_name . '/' . $layout_new_filename );

	    Model_AdminTemplates::add_info($template_name, 'templates', 'filename', $layout_new_filename );
	}
    }

    public function layout_delete($id = NULL)
    {
        if (is_numeric($id))
	{
	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $layout_list = Model_AdminLayout::layout_show($template_name);
	    $layout_filename = $layout_list[$id];
	    @unlink(TEMPLATEPATH . $template_name . '/' .$layout_filename);

	    Model_AdminTemplates::del_info($template_name, 'templates/*', $layout_filename );
	}
    }

    public function layout_delete_list($array)
    {
        if (count($array))
	{
	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $layout_list = Model_AdminLayout::layout_show($template_name);

	    $array = array_flip($array);
	    
	    foreach($array as $value)
	    {
		$layout_filename = $layout_list[$value];
		@unlink(TEMPLATEPATH . $template_name . '/' .$layout_filename);

		Model_AdminTemplates::del_info($template_name, 'templates/*', $layout_filename );
	    }
	}
    }

}