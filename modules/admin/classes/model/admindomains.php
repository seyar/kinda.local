<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminDomains extends Model
{

    static public function domain_info($name)
    {
        $query = DB::select()
                        ->from('domains')
                        ->where('http_host', '=', $name)
                        ->limit(1)
        ;

        $result = $query->execute();
        if ($result->count() == 0)
            return array();
        else
            return $result->current();
    }

    static public function domain_template($name)
    {
        $domain_info = Model_AdminDomains::domain_info($name);

        return arr::get($domain_info, 'domain_template', '');
    }

}