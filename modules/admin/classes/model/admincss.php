<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_AdminCSS extends Model {

    public function css_show($template_name = NULL)
    {
	$css_list =array();

	$template_info = Model_AdminTemplates::get_info(
		$template_name ? $template_name : Model_AdminDomains::domain_template($_SERVER['HTTP_HOST'])
	);

	foreach ($template_info->css->filename as $key=>$item)
	{
	    $item = (string)$item;
	    $css_list[] = $item;
	}

	return $css_list;
    }

    public function css_load($id = NULL)
    {
	if (is_numeric($id))
	{ // update

	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $css_list = Model_AdminCSS::css_show($template_name);

	    $content = file_get_contents(TEMPLATEPATH . $template_name . '/' .$css_list[$id]);

	    return array('id'=>$id,
			'content'=>mb_convert_encoding(
				$content, 'UTF-8', mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true)
				),
			'filename' => $css_list[$id],
			);
	}
	else
	{
	    return array('id'=>$id, 'content'=>"@charset 'UTF-8';\n", 'filename'=>'');
	}
    }

    public function css_save($lang, $id = NULL)
    {
	if( $post = Model_AdminCSS::css_validate() )
	{
	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $css_list = Model_AdminCSS::css_show($template_name);

	    if( is_numeric($id))
	    {
		$css_filename = $css_list[$id];
		@copy(TEMPLATEPATH . $template_name . '/' . $css_filename
			, TEMPLATEPATH . $template_name . '/' . $css_filename . '.bak' );
	    }
	    else
	    {
		$css_filename = Controller_Admin::transliterate($_POST['css_filename']);
		Model_AdminTemplates::add_info($template_name, 'css', 'filename', $css_filename );
	    }
	    file_put_contents(TEMPLATEPATH . $template_name . '/' .$css_filename, $post['content']);

	    return true;
	}
	else
	{
            Messages::add('Incorrect post');
	    return false;
	}
    }

    public function css_validate()
    {
	$keys = array ('css_filename', 'content');
	$params = Arr::extract($_POST, $keys, NULL);

	$post = Validate::factory($params)
		    ->rule('css_filename', 'not_empty')
		    ->rule('content', 'not_empty')
	;

        if ($post->check())
	{
	    return $params;
	}
	else
	{
	    $this->errors = $post->errors('validate');
	}

    }

    public function css_copy($id = NULL)
    {

        if (is_numeric($id))
	{
	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $css_list = Model_AdminCSS::css_show($template_name);
	    $css_filename = $css_list[$id];
	    $css_new_filename = dirname($css_filename) . '/copy_' . basename($css_filename);

	    @copy(TEMPLATEPATH . $template_name . '/' . $css_filename
			, TEMPLATEPATH . $template_name . '/' . $css_new_filename );

	    Model_AdminTemplates::add_info($template_name, 'css', 'filename', $css_new_filename );
	}
    }

    public function css_delete($id = NULL)
    {
        if (is_numeric($id))
	{
	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $css_list = Model_AdminCSS::css_show($template_name);
	    $css_filename = $css_list[$id];
	    @unlink(TEMPLATEPATH . $template_name . '/' .$css_filename);

	    Model_AdminTemplates::del_info($template_name, 'css/*', $css_filename );
	}
    }

    public function css_delete_list($array)
    {
        if (count($array))
	{
	    $template_name = Model_AdminDomains::domain_template($_SERVER['HTTP_HOST']);
	    $css_list = Model_AdminCSS::css_show($template_name);

	    $array = array_flip($array);

	    foreach($array as $value)
	    {
		$css_filename = $css_list[$value];
		@unlink(TEMPLATEPATH . $template_name . '/' .$css_filename);

		Model_AdminTemplates::del_info($template_name, 'css/*', $css_filename );
	    }
	}
    }

}