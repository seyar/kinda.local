<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_WidgetManager extends Model {

    public function getWidgets($installed = false)
    {
        $modulesControl = new Model_AdminModulesControl();
        $installedModules = $modulesControl->get_installed();

        $widgetsData = array();
        foreach ($installedModules as $module)
        {
            if ( $installed && !$module['selectedwidget'] ) continue;
            try
            {
                $className = "Model_".ucfirst($module['fs_name']);
                $class = new ReflectionClass($className);

                $model = $class->newInstance();
                $widgetsData[$module['id']] = $class->getMethod('getWidgetData')->invoke($model);
            }
            catch(Exception $e){}
        }
        
        return $widgetsData;
    }

    public function update($id, $status)
    {
        DB::update('modules')
                ->set(array('selectedwidget' => $status))
                ->where('id', '=', $id)
                ->execute();
    }
}