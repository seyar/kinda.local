<?php defined('SYSPATH') OR die('No direct access allowed.');

class Admin
{
    static function model_pagination($languages_id, $table, Array $columns=array(), $where = array(), $prim_order_by = NULL )
    {
        $return = array('', array(), 0);

        // Get and save to cookes current page and sort column / order
        $limit = Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10);
        $page = Arr::get($_GET, 'page', Cookie::get(Request::instance()->controller . '_page'), 1);
        $order_by = Arr::get($_GET, 'order_by', Cookie::get(Request::instance()->controller . '_order_by'));
        $order_direction = Arr::get($_GET, 'order_direction', Cookie::get(Request::instance()->controller . '_order_direction'));

        $limit = $limit ? $limit : 10;
        $page = $page ? $page : 1;

        $_GET['page'] = $page;
        
        if (!in_array($order_by, $columns))
                $order_by = '';

        Cookie::set('rows_per_page', $limit);

        Cookie::set(Request::instance()->controller . '_page', $page);
        Cookie::set(Request::instance()->controller . '_order_by', $order_by);
        Cookie::set(Request::instance()->controller . '_order_direction', $order_direction);

        $query_c = DB::select(Db::expr('COUNT(*) AS count'))
                        ->from($table);

        $query = DB::select(Db::expr( implode(', ', $columns) ))
                        ->from($table);


        if( !empty($order_by) )
        {
            if (!empty($order_direction))
            {
                $query->order_by($order_by, $order_direction);
            }
            else
                $query->order_by($order_by, 'ASC');
        }

        if( !empty($prim_order_by) && !is_array( $prim_order_by ) )
                $query->order_by($prim_order_by, 'DESC');
        elseif( !empty($prim_order_by) && is_array( $prim_order_by ) )
                $query->order_by($prim_order_by[0], $prim_order_by[1]);

        if (is_numeric($languages_id))
        {
            $query_c->where('languages_id', '=', $languages_id);
            $query->where('languages_id', '=', $languages_id);
        }

        if( is_array($where) && count($where) > 0 )
        {
            foreach($where as $item)
            {
                $query_c->where($item[0], $item[1], $item[2]);
                $query->where($item[0], $item[1], $item[2]);
            }
        }

        $result_c = $query_c
                        ->execute()
                        ->current();

        // if there is no page with number $page                
        if (($page-1)*$limit>$result_c['count'])
        {
            $_GET['page']  = $page = ceil($result_c['count'] / $limit);
        }
        
        $page = $page <=0 ? 1 : $page;        
        $result = $query
                        ->limit($limit)
                        ->offset(($page-1)*$limit)
//                        ->cached(30)
                        ->execute()
                        ;

        if ($result->count() > 0)
        {
            $return = array(Pagination::factory(
                        array(
                            'total_items' => $result_c['count'],
                            'items_per_page' => $limit,
                            'view' => 'pagination/modern',
                            'auto_hide' => TRUE,
                        )
                )->render()
                , $result->as_array('id')
            );
        }else
        {
			if( $page != 1 )
			{
				 Cookie::set(Request::instance()->controller . '_page', 1);
				 Request::instance()->redirect( ADMIN_PATH . Request::instance()->controller );
			}
		}

        return $return;
    }
    
    static function highload_model_pagination($languages_id, Database_Query_Builder_Select $table, $where = array(), $prim_order_by = NULL )
    {
        $return = array('', array(), 0);

        // Get and save to cookes current page and sort column / order
        $limit = Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10);
        $page = Arr::get($_GET, 'page', Cookie::get(Request::instance()->controller . '_page'), 1);
        $order_by = Arr::get($_GET, 'order_by', Cookie::get(Request::instance()->controller . '_order_by'));
        $order_direction = Arr::get($_GET, 'order_direction', Cookie::get(Request::instance()->controller . '_order_direction'));

        $limit = $limit ? $limit : 10;
        $page = $page ? $page : 1;

        $_GET['page'] = $page;
        
//        if (!in_array($order_by, $columns))
                $order_by = '';

        Cookie::set('rows_per_page', $limit);

        Cookie::set(Request::instance()->controller . '_page', $page);
        Cookie::set(Request::instance()->controller . '_order_by', $order_by);
        Cookie::set(Request::instance()->controller . '_order_direction', $order_direction);

//        $query_c = DB::select(Db::expr('COUNT(*) AS count'))
//                        ->from($table);

        $query_c = $table;
        $query = $table;
//        $query = DB::select(Db::expr( implode(', ', $columns) ))
//                        ->from($table);


        if( !empty($order_by) )
        {
            if (!empty($order_direction))
            {
                $query->order_by($order_by, $order_direction);
            }
            else
                $query->order_by($order_by, 'ASC');
        }

        if( !empty($prim_order_by) && !is_array( $prim_order_by ) )
                $query->order_by($prim_order_by, 'DESC');
        elseif( !empty($prim_order_by) && is_array( $prim_order_by ) )
                $query->order_by($prim_order_by[0], $prim_order_by[1]);

        if (is_numeric($languages_id))
        {
            $query_c->where('languages_id', '=', $languages_id);
            $query->where('languages_id', '=', $languages_id);
        }

        if( is_array($where) && count($where) > 0 )
        {
            foreach($where as $item)
            {
                $query_c->where($item[0], $item[1], $item[2]);
                $query->where($item[0], $item[1], $item[2]);
            }
        }

        $result_c = $query_c
                        ->group_by('id')
                        ->execute()
//                        ->current()
                ;
        
//        $countAllRecords = $result_c->count();
        $countAllRecords = count($result_c->as_array('id'));
        
        // if there is no page with number $page                
        if( ($page-1)*$limit > $countAllRecords )
        {
            $_GET['page']  = $page = ceil($countAllRecords / $limit);
        }
        
        $page = $page <=0 ? 1 : $page;
        $result = $query
                        ->limit($limit)
                        ->offset(($page-1)*$limit)
//                        ->cached(30)
                        ->execute()
                        ;

        if ($result->count() > 0)
        {
            $return = array(Pagination::factory(
                        array(
                            'total_items' => $countAllRecords,
                            'items_per_page' => $limit,
                            'view' => 'pagination/modern',
                            'auto_hide' => TRUE,
                        )
                )->render()
                , $result->as_array('id')
            );
        }else
        {
			if( $page != 1 )
			{
				 Cookie::set(Request::instance()->controller . '_page', 1);
				 Request::instance()->redirect( ADMIN_PATH . Request::instance()->controller );
			}
		}

        return $return;
    }

    static function table_sort_header($column='')
    {
        if (!$column)
            return '';

        $page = Arr::get($_GET, 'page', 1);
        $order_by = Arr::get($_GET, 'order_by', '');
        $direction = Arr::get($_GET, 'order_direction', '');

        $return = "$direction ";
        $current_direction = 'asc';

        if ($order_by==$column)
            if (empty($direction) || strtoupper($direction)=='ASC')
            {
                $return .= "sorting_asc";
                $current_direction = 'desc';
            }
            else
            {
                $return .= "sorting_desc";
            }
        else
                $return .= "sorting";

        $return .= "\" order_url=\"?page=$page&order_by=$column&order_direction=$current_direction";

        return $return;
    }

}