<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Groups extends Controller_Admin
{

    public $template = 'groups_list';
    public $module_title = 'Groups';
    public $module_desc_short = 'Groups Descr Short';
    public $module_desc_full = 'Groups Descr Full';

    public function before()
    {
        parent::before();
        $this->model = new Model_AdminRole();
    }

    public function action_index()
    {
        $this->template->rows = $this->model->roles_show();
    }

    public function action_active()
    {
        $this->model->role_active($this->request->param('id'));
        $this->redirect_to_controller('groups');
    }

    public function action_edit()
    {
        $this->template = View::factory('groups_edit');

        $this->template->obj = $this->model->role_load($this->request->param('id'));

    }

    public function action_save()
    {
        if ($this->model->role_save(intval($this->request->param('id'))))
        {
            $this->redirect_to_controller($this->request->controller);
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            $this->model->role_delete($this->request->param('id'));

        elseif (count($_POST['chk']))
            $this->model->role_delete_list($_POST['chk']);

        $this->redirect_to_controller('groups');
    }

    public function action_update()
    {
        if ($this->model->role_save($this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }
    public function action_apply()
    {
        if ($this->model->role_save($this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

}