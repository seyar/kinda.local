<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_ModulesControl extends Controller_Admin {

    public $template    = 'modules_list';

    public $module_title	= 'Modules';
    public $module_desc_short   = 'Modules Descr Short';
    public $module_desc_full	= 'Modules Descr Full';

    public function before()
    {
        parent::before();
	$this->model = new Model_AdminModulesControl();
    }

    public function action_index()
    {
	//$this->view->assign('rows', $this->model->page_show( $this->lang ) );
    }

    public function action_edit()
    {
        $this->template = 'modules_edit';
        
        if(Request::$method == 'POST')
        {
            $moduleName = Arr::get($_POST, 'moduleName');
            $this->model->install($moduleName);
            $this->request->redirect('admin/modulescontrol/');
        }
    }

    public function action_install()
    {
        if(Request::$method == 'POST')
        {
            $moduleName = $_POST['fs_name'];
            $this->model->installModule($moduleName);
            $this->request->redirect('admin/modulescontrol/');
        }
        
        $this->template = 'modules_install.tpl';
        $modules = $this->model->getInstallableModules();
        $this->view->assign('installModules', $modules);
    }

    public function action_uninstall()
    {
        $id = intval($this->request->param('id'));
        if ( $id )
        {
            $this->model->uninstallModule($id);
            $this->request->redirect('admin/modulescontrol/');
        }
    }
}

