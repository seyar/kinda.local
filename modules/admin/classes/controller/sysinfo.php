<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_SysInfo extends Controller_Admin {

    public $template    = 'sysinfo';

    public $module_title	= 'System information';
    public $module_desc_short   = 'Sysinfo Descr Short';
    public $module_desc_full	= 'Sysinfo Descr Full';

    public function before()
    {
        parent::before();
        $this->model = new Model_AdminSysinfo();
    }

    public function action_index()
    {
        $this->template->content = $this->model->info();
    }

}

