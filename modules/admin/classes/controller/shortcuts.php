<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Shortcuts extends Controller_Admin {

    public $template		= 'shortcuts_list';

    public $module_title	= 'Shortcuts';
    public $module_desc_short   = 'Shortcuts Descr Short';
    public $module_desc_full	= 'Shortcuts Descr Full';

    public $user;

    public function before()
    {
        parent::before();
        $this->model = new Model_AdminShortcut();
        $this->template->current_lang = $this->lang;
    }

    public function action_index()
    {
        $this->view->assign('rows', $this->model->shortcuts_list());
    }

    public function action_add()
    {
        if (isset($_POST['name']) && isset($_POST['name']))
        {
            $this->model->shortcuts_add( $_POST['name'], $_POST['link'] );
        }
        die();
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            $this->model->shortcuts_delete( $this->request->param('id') );

        $this->redirect_to_controller('shortcuts');
    }

}