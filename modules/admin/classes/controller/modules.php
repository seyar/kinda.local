<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Modules extends Controller_Admin
{

    public $template = '';
    public $route_uri;

    public function action_index()
    {
        if (strpos($this->request->param('module_name'), ':'))
        {
            $parts = explode(':', $this->request->param('module_name'));
            define('MODULE_NAME', $parts[0]);
            define('MODULE_SUBCONTROLLER', $parts[1]);
        }
        else
            define('MODULE_NAME', $this->request->param('module_name'));

            define('MODULE_ACTION', $this->request->param('module_action'));
            define('MODULE_ID', $this->request->param('id'));
//	define('ADMIN_TEMPLATES', Kohana::config('admin')->path );

        if (!array_key_exists(MODULE_NAME, Kohana::modules()))
        {
            $this->template = 'error.tpl';
            $responce = Request::factory(ADMIN_PATH)->execute()->response;
            $this->view->assign('error', 'Module <b>"' . MODULE_NAME . '"</b> is not installed.');
            return true;
        }

        if (!defined("MODULE_SUBCONTROLLER"))
        {
            $this->route_uri = Route::get(MODULE_NAME)
                            ->uri(
                                    array('action' => MODULE_ACTION, 'id' => MODULE_ID)
            );
        }
        else
        {
            $this->route_uri = Route::get(MODULE_NAME)
                            ->uri(
                                    array('controller' => MODULE_SUBCONTROLLER, 'action' => MODULE_ACTION, 'id' => MODULE_ID)
            );
        }


        if ($this->route_uri)
        {
            $responce = Request::factory($this->route_uri)->execute()->response;

            $this->module_title = $responce['title'];
            $this->module_desc_short = $responce['desc_short'];
            $this->module_desc_full = $responce['desc_full'];
            $this->output_source = $responce['content'];
        }
    }

}
