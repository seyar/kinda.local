<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Layout extends Controller_Admin
{

    public $template = 'layout_list';
    public $module_title = 'Layout';
    public $module_desc_short = 'Layout Descr Short';
    public $module_desc_full = 'Layout Descr Full';

    public function before()
    {
        parent::before();
        $this->model = new Model_AdminLayout();
    }

    public function action_index()
    {
        $this->template->rows = $this->model->layout_show();
    }

    public function action_edit()
    {
        $this->template = View::factory('layout_edit');
        $this->template->obj = $this->model->layout_load($this->request->param('id'));

    }

    public function action_copy()
    {
        $this->model->layout_copy($this->request->param('id'));

        $this->redirect_to_controller('layout');
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            $this->model->layout_delete($this->request->param('id'));
        elseif (count($_POST['chk']))
            $this->model->layout_delete_list($_POST['chk']);

        $this->redirect_to_controller('layout');
    }

    public function action_save()
    {
        if ($this->model->layout_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller);
        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_update()
    {
        if ($this->model->layout_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->action_edit();
        }
    }

}

