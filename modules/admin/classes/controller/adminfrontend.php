<?php
defined( 'SYSPATH' ) or die( 'No direct script access.' );

class Controller_AdminFrontend extends Controller
{

    static function init()
    {
        $query = DB::select( 'id', 'prefix', 'is_default' )
                        ->from( 'languages' )
                        ->order_by( 'id' )
                        ->cached( 7200 )
        ;

        $result = $query->execute()->as_array();

        if( count( $result ) )
        {
            $langs_prefix = array( );

            foreach( $result as $value )
            {
                $langs_prefix[] = $value['prefix'];
                if( $value['is_default'] )
                    define( 'LANGUAGE_ROUTE_DEFAULT', $value['prefix'] );
            }

            define( 'LANGUAGE_ROUTE', implode( '|', $langs_prefix ) );
        }
    }

    static function ready()
    {

        $request = Request::instance( $_SERVER['PATH_INFO'] );

        $query = DB::select( 'id', 'prefix' )
                        ->from( 'languages' )
                        ->order_by( 'id' )
                        ->where( 'prefix', '=', $request->param( 'lang' ) )
                        ->cached( 3600 )
        ;

        $result = $query->execute();

        if( $result->count() )
        {
            $current_lang = $result->current();

            define( 'CURRENT_LANG_ID', $current_lang['id'] );

            i18n::lang( $request->param( 'lang' ) );

            Kohana_Controller_Quicky::$intermediate_vars['request_lang_id'] = $current_lang['id'];
            Kohana_Controller_Quicky::$intermediate_vars['request_lang_prefix'] = $current_lang['prefix'];
        }
    }

}