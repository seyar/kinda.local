<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_GroupAssigments extends Controller_Admin {

    public $template		= 'groupassigments_list';

    public $module_title	= 'Group Assigments';
    public $module_desc_short   = 'Group Assigments Descr Short';
    public $module_desc_full	= 'Group Assigments Descr Full';


    public function before()
    {
	parent::before();
	$this->model = new Model_AdminRoleUsers();
    }

    public function action_index()
    {
        $this->template->rows = Model_AdminRoleUsers::role_user_load();
        
        $this->template->user_roles = Model_AdminUser::user_roles();

    }

    public function action_add()
    {
        Model_AdminRoleUsers::role_user_add($this->request->param('id'));
        exit();
    }

    public function action_rem()
    {
        Model_AdminRoleUsers::role_user_rem($this->request->param('id'));
        exit();
    }
}