<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Main extends Controller_Admin {

    public $template    = 'index';

    public function action_index()
    {
        $this->template->intro = 'Index Page Here';
    }

}
