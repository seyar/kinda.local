<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Domains extends Controller_Admin {

    public $template    = 'domains_list.tpl';

    public $module_title	= 'Pages';
    public $module_desc_short   = 'Pages Descr Short';
    public $module_desc_full	= 'Pages Descr Full';

    public function before()
    {
	parent::before();

	$this->model = new Model_AdminDomains();

    }

    public function action_index() {

    }

}