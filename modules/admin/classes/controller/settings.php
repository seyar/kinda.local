<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Settings extends Controller_Admin {

    public $template		= 'settings_list';

    public $module_title	= 'Settings';
    public $module_desc_short   = 'Settings Descr Short';
    public $module_desc_full	= 'Settings Descr Full';

    public $user;

    public function action_index()
    {
        $this->template->languages = Model_AdminSettings::languages_list();
        $this->template->templates = Model_AdminTemplates::get_list();

        $this->template->obj = (array)Kohana::config('settings');
    }

    public function action_clearcache()
    {
        $this->redirect_to_controller($this->request->controller);
    }

    public function action_update()
    {
        $this->model = new Model_AdminSettings();
        
	if( $this->model->settings_save() )
	{
	    $this->redirect_to_controller($this->request->controller.'#'.$_POST['actTab']);
	}
	else
	{
	    $this->template->errors = $this->model->errors;
	    $this->action_index();
	}
    }

    public function action_ajax()
    {
        switch ( arr::get($_POST, 'action', ''))
        {
            case 'domains_list':
                echo json_encode(Model_AdminSettings::domains_list());
                break;

            case 'domains_edit':
                echo json_encode(Model_AdminSettings::domains_load(arr::get($_POST, 'id', NULL)));
                break;

            case 'domains_save':
                Model_AdminSettings::domains_save();
                break;

            case 'domains_delete':
                Model_AdminSettings::domains_delete(arr::get($_POST, 'id', NULL));
                break;


            case 'languages_list':
                echo json_encode(Model_AdminSettings::languages_list());
                break;

            case 'languages_edit':
                echo json_encode(Model_AdminSettings::languages_load(arr::get($_POST, 'id', NULL)));
                break;

            case 'languages_save':
                Model_AdminSettings::languages_save();
                break;

            case 'languages_delete':
                Model_AdminSettings::languages_delete(arr::get($_POST, 'id', NULL));
                break;
            

            // Hack !?
            default:
                echo 'Ooops! :p';
                break;
        }
        exit;
    }

}