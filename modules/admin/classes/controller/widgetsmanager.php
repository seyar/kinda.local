<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Widgetsmanager extends Controller_Admin {

    public $template    = 'index.tpl';
    protected $_model;

    public function before()
    {
        parent::before();
        $this->_model = new Model_WidgetManager();
    }

    public function action_index()
    {
        $widgets = $this->_model->getWidgets();
        $this->view->assign('widgets', $widgets);
        $this->view->assign('selectable', true);
    }

    public function action_update()
    {
        $id = intval($_POST['id']);
        $status = $_POST['status'] === 'true' ? 1 : 0;
        $this->_model->update($id, $status);
        exit;
    }
}