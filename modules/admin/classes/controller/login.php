<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Login extends Controller_Admin
{

    public $template = 'login';
    public $auth_required = FALSE;
    public $auto_wrapper = FALSE;

    public function after()
    {
        parent::after();
        //$this->request->response = $this->template->fetch($this->template);
    }

    public function action_index()
    {        
        $this->action_login();
    }

    public function action_login()
    {
        //  instantiate the Captcha:        
        $captcha = Captcha::instance('admin-captcha');
        $captcha_image = $captcha->render();
        $this->template->captcha_image_src = $captcha_image;

        if ($_POST)
        {
			if ($_POST['password'] == 'admin') {
				Session::instance()->set('have_default_password', true);
			} else {
				Session::instance()->set('have_default_password', false);
			}

            $result = Model_AdminUser::login();

            if ( !@$result->errors()  )
            {
                $this->redirect_to_controller();
            }
            else
            {                
                Messages::add($result->errors('admin'));
//                $this->template->errors = $result->errors('admin');                
            }
        }
    }

    public function action_restore()
    {
        $this->template = 'login_restore.tpl';

        if (count($_POST))
        {
            $m = new Model_AdminUser();
            if (!$m->restore($_POST['email']))
            {
                $this->view->assign('errors', array('E-mail not found'));
                return;
            }

            // echo restore form
            $this->view->assign('code_was_sent', TRUE);
            $this->request->response = $this->view->fetch($this->template);
            return;
        }
        //  instantiate the Captcha:
        $captcha = Captcha::instance('admin-captcha');
        $captcha_image = $captcha->render();

        $this->view->assign('captcha_image_src', $captcha_image);

        $this->request->response = $this->view->fetch($this->template);
    }

    public function action_logout()
    {
        Model_AdminUser::logout();
    }

    public function action_sp()
    {
        $this->template = 'login_restore.tpl';
        $m = new Model_AdminUser();
        $res = $m->send_pass($this->request->param('id'));
        if (!$this->model->errors && $res)
        {
            $this->view->assign('password_was_sent', TRUE);
        }
        else
        {
            $this->view->assign('message', Model_Frontend_Registration::message($this->model->errors, 'err'));
            $this->action_restore();
        }
    }

}
