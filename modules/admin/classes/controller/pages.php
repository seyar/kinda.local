<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Pages extends Controller_Admin
{

    public $template = 'pages_list';
    public $module_title = 'Pages';
    public $module_desc_short = 'Pages Descr Short';
    public $module_desc_full = 'Pages Descr Full';



    public function before()
    {
        parent::before();
        $this->model = new Model_AdminPages();
        $this->template->current_lang = $this->lang;
    }

    public function action_index()
    {
        $res = $this->model->page_show($this->lang);
        $this->template->rows = $res;
    }

    public function action_ajax_childrens()
    {
        $this->auto_wrapper = FALSE;
        $this->template = View::factory('pages_list_ajax');


        if ((int) $this->request->param('id'))
        {
            $this->template->depth = 1;
            $this->template->rows = $this->model->page_show($this->lang, (int) $this->request->param('id'));
//            echo Kohana::debug($this->template);
//            $this->request->response = $this->template->render($this->template);
        }
    }

    public function action_ajax_reorder()
    {
        $this->auto_wrapper = FALSE;
//        $this->model->page_move((int) $this->request->param('id'), (int) $_POST['new_parent_id'], (int) $_POST['new_order']);
        $this->model->page_move_new_diz($_POST['data']);
        exit;
    }

    public function action_edit()
    {
        $this->template = View::factory('pages_edit');

        $this->template->obj = $this->model->page_load($this->request->param('id'));
        $this->template->page_types = $this->model->page_types();
        $this->template->site_folders = $this->model->page_folders($this->lang);
        $this->template->siteFoldersOtherLang = $this->model->siteFoldersOtherLang($this->lang);
        $this->template->visible_to = $this->model->page_visible_to();

        $this->template->langs = $this->get_languages_array();
        $this->template->page_template_list = $this->model->get_page_templates();
    }

    public function action_copy()
    {
        parse_str(substr($_POST['data'], 0, -1), $data);
        $this->model->page_copy($data['dragged_id']);
        $this->model->page_move_new_diz($_POST['data']);

//        $this->redirect_to_controller('pages');
    }

    public function action_active()
    {
        $this->model->page_active($this->request->param('id'));
        die('OK');
//        $this->redirect_to_controller('pages');
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            $this->model->page_delete($this->request->param('id'));

        elseif (!empty($_POST['chk']))
            $this->model->page_delete_list($_POST['chk']);

        $this->redirect_to_controller('pages');
    }

    public function action_save()
    {
        if ($this->model->page_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller);
        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_apply()
    {
        if (($id = $this->model->page_save($this->lang)))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $id . '/edit/#' . $_POST['actTab']);
        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_follow()
    {
        $page_info = $this->model->page_load($this->request->param('id'));
        Request::instance()->redirect(str_replace('//', '/', $page_info['parent_url'] . '/' . $page_info['page_url'])
                . ($page_info['pagetypes_id'] == 2 ? '/' : '')  // For Folder fix
        );
        die();
    }

    public function action_update()
    {

        if ($this->model->page_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit/#' . $_POST['actTab']);
        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_copy2otherlang()
    {
        if ($this->model->copy2otherlang($_POST['copy_for_lang'], $this->request->param('id'), $this->lang))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->action_edit();
        }
    }

}