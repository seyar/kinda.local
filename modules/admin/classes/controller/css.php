<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_CSS extends Controller_Admin {

    public $template    = 'css_list';

    public $module_title	= 'Style sheets';
    public $module_desc_short   = 'CSS Descr Short';
    public $module_desc_full	= 'CSS Descr Full';

    public function before()
    {
    	parent::before();
        $this->model = new Model_AdminCSS();
    }

    public function action_index()
    {
        $this->template->rows = $this->model->css_show();
    }

    public function action_edit()
    {
        $this->template = View::factory('css_edit');
	$this->template->obj = $this->model->css_load( $this->request->param('id') );
    }

    public function action_copy()
    {
	$this->model->css_copy( $this->request->param('id') );

        $this->redirect_to_controller('css');
    }

    public function action_delete()
    {
	if ($this->request->param('id'))
	    $this->model->css_delete( $this->request->param('id') );
	elseif (count($_POST['chk']))
	    $this->model->css_delete_list($_POST['chk']);


        $this->redirect_to_controller('css');
    }

    public function action_save()
    {
	if( $this->model->css_save( $this->lang, $this->request->param('id') ) )
	{
	    $this->redirect_to_controller($this->request->controller);
	}
	else
	{
	    
	    $this->action_edit();
	}
    }

    public function action_update()
    {
	if( $this->model->css_save( $this->lang, $this->request->param('id') ) )
	{
	    $this->redirect_to_controller($this->request->controller.'/'.$this->request->param('id').'/edit');
	}
	else
	{
	    $this->action_edit();
	}
    }
}

