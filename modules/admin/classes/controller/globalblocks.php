<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Controller_GlobalBlocks extends Controller_Admin
{

    public $template = 'block_list';
    public $module_title = 'Global Blocks';
    public $module_desc_short = 'Global Blocks Descr Short';
    public $module_desc_full = 'Global Blocks Descr Full';

    public function before()
    {
        parent::before();
        $this->model = new Model_AdminGlobalBlocks();
        $this->template->current_lang = $this->lang;
    }

    public function action_index()
    {

        list($this->template->pagination, $this->template->rows) = Admin::model_pagination($this->lang, 'globalblocks',
                array('id', 'name', 'description')
            );
    }

    public function action_edit()
    {

        $this->template = View::factory('block_edit');
        $this->template->obj = $this->model->gb_load($this->request->param('id'));
        $this->template->langs = $this->get_languages_array();
    }

    public function action_copy()
    {
        $this->model->gb_copy($this->request->param('id'));

        $this->redirect_to_controller('globalblocks');
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            $this->model->gb_delete($this->request->param('id'));
        elseif (!empty($_POST['chk']))
            $this->model->gb_delete_list($_POST['chk']);

        $this->redirect_to_controller('globalblocks');
    }

    public function action_save()
    {
        if ($this->model->gb_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller);
        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_apply()
    {
        if ($id = $this->model->gb_save($this->lang, 0))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $id . '/edit');
        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_update()
    {
        if ($this->model->gb_save($this->lang, $this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_copy2otherlang()
    {
        if ($this->model->copy2otherlang($_POST['copy_for_lang'], $this->request->param('id'), $this->lang))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->action_edit();
        }
    }

}