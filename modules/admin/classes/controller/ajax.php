<?php

class Controller_Ajax extends Controller_Quicky {

    public $auto_render = FALSE;

    public function action_test()
    {

        die();
    }

    public function action_switch_desc()
    {
	$user = Sprig::factory('user')
  	    		->values(array('id' => Session::instance()->get('user_id') ))
  	    		->load();

	$user->__set('show_desc', !$user->__get('show_desc'));
	$user->update();
	
	Cookie::set('show_desc', !$user->__get('show_desc'));

	echo !$user->__get('show_desc');
	
	die();
    }

}
