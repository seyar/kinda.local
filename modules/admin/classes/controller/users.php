<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Users extends Controller_Admin
{

    public $template = 'users_list';
    public $module_title = 'Users';
    public $module_desc_short = 'Users Descr Short';
    public $module_desc_full = 'Users Descr Full';
    public $user;

    public function before()
    {
        parent::before();
        $this->model = new Model_AdminUser();
    }

    public function action_index()
    {
        $this->template->rows = $this->model->user_show();
        $this->template->user_roles = $this->model->user_roles();
    }

    public function action_active()
    {
        $this->model->user_active($this->request->param('id'));
        $this->redirect_to_controller('users');
    }

    public function action_edit()
    {
        $this->template = View::factory('users_edit');
        $this->template->obj = $this->model->user_load($this->request->param('id'));
        $this->template->user_roles = $this->model->user_roles();

    }

    public function action_save()
    {
        if ($this->model->user_save($this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller);
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            $this->model->user_delete($this->request->param('id'));

        elseif (count($_POST['chk']))
            $this->model->user_delete_list($_POST['chk']);

        $this->redirect_to_controller('users');
    }

    public function action_update()
    {
        if ($this->model->user_save($this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->template->errors = $this->model->errors;
            $this->action_edit();
        }
    }

}