<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_FrontendUsers extends Controller_Admin {

    public $template		= 'frontendusers_list.tpl';

    public $module_title	= 'Frontend Users';
    public $module_desc_short   = 'Frontend Users Descr Short';
    public $module_desc_full	= 'Frontend Users Descr Full';

    public function action_index() {
        $users = Model_Registration::get_items(null,null,null,null,null,null);
        $this->view->assign('users', $users);
    }

    public function action_activate()
    {
        $id = intval($this->request->param('id'));
        if(!$id) return;
        $reg = new Model_Registration();
        $reg->changeStatus($id);
        $this->redirect_to_controller($this->request->controller);
    }

    public function action_delete()
    {
        $id = intval($this->request->param('id'));
        if(!$id) return;
        $reg = new Model_Registration();
        $reg->deleteItem($id);
        $this->redirect_to_controller($this->request->controller);
    }


}