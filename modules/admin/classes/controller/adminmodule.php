<?php

defined('SYSPATH') OR die('No direct access allowed.');

abstract class Controller_AdminModule extends Controller_Admin
{
    public $auto_wrapper = TRUE;

    public function before()
    {        
        parent::before();
        define('MODULE_NAME',$this->module_name);
//        $this->request->controller = Request::instance()->controller . '/';
    }

    public function ajax_echo()
    {
        $this->after();
        echo $this->request->response['content'];
        exit;
    }

    public function after()
    {        
        parent::after();        
        
        $this->template->admin_templates = Kohana::config('admin')->path;
        $this->template->template_dir = Kohana::config( strtolower(MODULE_NAME))->path;

    }

}