<?php defined('SYSPATH') OR die('No direct access allowed.');
/*
 * Usage:
 * echo Html::image('image/my_image.jpg');
 */

class Controller_Images extends Controller {

      protected $directory = 'static/images/';

      public function action_index()
      {
          // Create the filename
          $file = $this->directory.$this->request->param('file');

          if ( ! is_file($file))
          {
              throw new Kohana_Exception('Image does not exist');
          }

          //
          // Check your permissions here
          //

          // Set the mime type
          $this->request->headers['Content-Type'] = File::mime($file);
          $this->request->headers['Content-length'] = filesize($file);

          // Send the set headers to the browser
          $this->request->send_headers();

          // Send the file
          $img = @ fopen($file, 'rb');
          if ($img) {
              fpassthru($img);
              exit;
          }
      }

  } // End Images
