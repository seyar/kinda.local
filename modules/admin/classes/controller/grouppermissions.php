<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_GroupPermissions extends Controller_Admin {

    public $template		= 'grouppermissions_list';

    public $module_title	= 'Group Permissions';
    public $module_desc_short   = 'Group Permissions Descr Short';
    public $module_desc_full	= 'Group Permissions Descr Full';

    public function before()
    {
	parent::before();
	$this->model = new Model_AdminRoleRules();
    }

    public function action_index()
    {
//	$this->template->rows', $this->model->role_load());
        $this->template->user_roles = Model_AdminUser::user_roles();
        $this->template->user_rules = Model_AdminRoleRules::getRules();
        $this->template->resources = Model_AdminRoleRules::getResources();
    }


    public function action_aacl()
    {
        $role = $_POST['role'];
        $resource = Arr::get($_POST, 'resource', "*");
        $action = Arr::get($_POST, 'action', null);
        if($action == "*") $action = null;
        if($_POST['checked'] == 'true')
        {
            AACL::grant($role, $resource);
        }
        else
        {
            AACL::revoke($role, $resource, $action);
        }
        exit;
    }

    public function action_add()
    {
        Model_AdminRoleRules::role_rule_add($this->request->param('id'));
        exit();
    }

    public function action_rem()
    {
        Model_AdminRoleRules::role_rule_rem($this->request->param('id'));
        exit();
    }

}