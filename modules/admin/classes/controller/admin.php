<?php defined('SYSPATH') OR die('No direct access allowed.');

abstract class Controller_Admin extends Controller_Template implements AACL_Resource
{

    public $auto_render = TRUE;
    public $auto_wrapper = TRUE;
    // admin param
    public $admin_path = '/admin/';
    public $admin_controller = 'main';
    public $lang = 1;  // Current Language
    public $interface_lang = 1; // admin interface Language
    public $session;  // Session info
    public $admin_model; // AdminUser model will be here
    public $model;  // Module model will be here
    // Using TRUE means requests will require authentication by default
    public $auth_required = TRUE;
    // Public variable for the user model
    public $user;
    public $module_title = 'Main';
    public $module_name = 'main';
    public $module_desc_short;
    public $module_desc_full;
    public $template;
    /**
     *
     * @var int     allow to hide breadcrums in template
     */
    public $breadcrumbs = TRUE;
    /**
     *
     * @var int     allow to hide float block width btns in template
     */
    public $floatingblock = FALSE;

    public $output_source;
    public $request;
    public $cache;
    static public $intermediate_vars = array();

    public function __construct(Request $request)
    {
        ini_set('session.gc_maxlifetime', 12*60*60);
        
        $this->admin_path = ADMIN_PATH;

        parent::__construct($request);

        $this->session = Session::instance();

        // Fix to restore session for "ajax" session via Flash. Require $_POST['user'] and $_POST['session_id']
        if (isset($_POST['session_id']) AND isset($_POST['user']))
        {
            Cookie::set('user', $_POST['user']);
            $this->session = Session::instance('native', $_POST['session_id']);
            $this->session->set('user_id', $_POST['user']);
        }


        $this->admin_model = new Model_AdminUser();

        // If a user id cookie is found attempt to load user
        if ($id = $this->session->get('user_id'))
        {
            $user = Sprig::factory('user')
                            ->values(array('id' => $id), array('status' => 1))
                            ->load();
            if ($user->loaded())
            {
                // User is logged in
                $this->user = $user;
            }
        }

        // If user is not logged in and login is required
        if ($this->auth_required AND (!$this->user
                OR
                ($this->session->get('user_ip') AND $this->session->get('user_ip') != $_SERVER['REMOTE_ADDR']) )
        )
        {
            // Redirect to the login page
            $request->redirect(url::site('/admin/login/'));
            die();
        }

        try
        {
            $defaultAccessModules = array('login', 'main', 'modules', 'photogallery', 'goods', 'publications');
            if (!in_array($this->request->controller, $defaultAccessModules))
                AACL::check($this);
        }
        catch (Exception $e)
        {
            $request->redirect(url::site('/admin/login/'));
            die();
        }

    }

    public function before()
    {
        parent::before();

        i18n::$lang = (isset($this->user->language)) ? $this->user->language : ''; //Kohana::config('admin.lang');
        // content languages
        $default_lang = $this->get_default_lang();
        $this->lang = Cookie::get('admin_lang') ? Cookie::get('admin_lang') : $default_lang;

//        $this->view->template_dir = Kohana::config('admin')->path;
        if ($this->auth_required)
        {
            if ($id = Cookie::get('user'))
            {
                $this->user->values(array('id' => $id));
            }
        }
        
        if (!defined("MODULE_SUBCONTROLLER"))
        {
            $this->template->module_name = @MODULE_NAME;
            define('MODULE_SUBCONTROLLER', $this->template->module_name);
        }
        else
        {
            $this->template->module_group = MODULE_NAME;
            $this->template->module_name = MODULE_NAME . ':' . MODULE_SUBCONTROLLER;
            $this->template->module_subname = MODULE_SUBCONTROLLER;
        }

        $this->template->module_id = @MODULE_ID;
    }

    public function after()
    {
        parent::after();

        if ($this->user)
        {
            $this->template->show_module_desc = $this->user->__get('show_desc');

            $this->template->installed_modules = Model_AdminModulesControl::get_installed();
//            $this->template->shortcuts = Model_AdminShortcut::shortcuts_list();
            $this->template->modules = Model_AdminModulesControl::modulesPanel();
            $this->template->admin_default_lang = $this->lang;
            $this->template->admin_lang = $this->get_languages_array();

            // module info
            $this->template->module = array(
                'title' => I18n::get($this->module_title),
                'descr_short' => I18n::get($this->module_desc_short),
                'descr_full' => I18n::get($this->module_desc_full),
            );

            // set admin vars for link building
            $this->template->admin_path = $this->admin_path;
            $this->template->admin_controller = $this->admin_controller;

            // set route vars for link building
            $this->template->controller = Request::instance()->controller.'/';
            $this->template->action = $this->request->action;
            $this->template->id = $this->request->param('id');

            
        }
        /* interface lang */
        $user_info = $this->get_user_info(1);
        $this->template->user_info = $user_info;

        $this->interface_lang = $user_info['language'];
        I18n::lang($this->interface_lang);
        /* interface lang */

//        if ($this->auto_wrapper)
//        {
            // get month name from i18n
            $this->template->title = I18n::get($this->module_title);

            $this->template->date_month = I18n::get(strtolower(date('M')));
            $old_errRep = error_reporting();
            error_reporting( E_COMPILE_ERROR|E_RECOVERABLE_ERROR|E_ERROR|E_CORE_ERROR|E_PARSE );
            // print body
            $body = $this->template ? $this->template->render() : $this->output_source;

            // print header
            $this->request->response = '';
            $this->request->response .= $this->template->render('system/header');
            //bread crumb
            $this->request->response .= ($this->breadcrumbs === TRUE ? $this->template->render('system/breadcrumbs') : '');

            if($this->auto_wrapper)
                $this->request->response .= $body;
            else
                $this->request->response = $body;

            // print footer
            $this->request->response .= ($this->auto_wrapper === TRUE ? $this->template->render('system/footer') : '' );
            error_reporting( $old_errRep );
//        }
    }

    final public function action_skip()
    {
        // Do nothing
    }

    final public function redirect_to_controller($controller='main')
    {
        $this->request->redirect(substr(URL::base(TRUE, TRUE), 0, -1)
                . $this->admin_path . ($controller ? $controller . '/' : ''));
    }

    public function get_languages_array()
    {
        $query = DB::select()
                        ->from('languages')
                        ->order_by('name')
                        ->cached(15);

        $result = $query->execute();

        if ($result->count() == 0)
            return array();
        else
        {
            $rows = $result->as_array();
            return $rows;
        }
    }

    public function get_default_lang()
    {
        $return = 0;
        $return = DB::select()->from('languages')->where('is_default', '=', 1)->execute()->get('id');
        return $return;
    }

    public function action_set_lang()
    {
        Cookie::set('admin_lang', Request::instance()->param('id'));
        Request::instance()->redirect($_SERVER['HTTP_REFERER']);
    }

    public function show_image_preview($filename, $width=120, $height=100, $lifetime = 60)
    {
        $this->cache = Cache_Sqlite::instance();

        $cached_image = $this->cache->get('test_image' . $filename);

        if (!$cached_image)
        {

            if (!is_file($filename))
            {
                throw new Kohana_Exception404('Image does not exist');
            }

            $this->image = Image::factory($filename);

            $cached_image = array
                (
                'mime' => $this->image->mime
                , 'content' => $this->image
                        ->resize($width, $height, Image::NONE)
//                                                    ->crop($width, $height)
                        ->render()
                    )
            ;

            $this->cache->set('test_image' . $filename
                    , array
                (
                'mime' => $cached_image['mime']
                , 'content' => $cached_image['content']
                    )
                    , $lifetime
            )
            ;
        }

        header("Content-type: " . $cached_image['mime']);
        echo $cached_image['content'];
        die();
    }

    static public function delete_directory($dir)
    {
        if (!file_exists($dir))
            return true;

        if (!is_dir($dir) || is_link($dir))
            return unlink($dir);
        foreach (scandir($dir) as $item)
        {
            if ($item == '.' || $item == '..')
                continue;
            if (!Controller_Admin::delete_directory($dir . "/" . $item))
            {
                chmod($dir . "/" . $item, 0777);
                if (!Controller_Admin::delete_directory($dir . "/" . $item))
                    return false;
            };
        }
        return rmdir($dir);
    }

    public static function transliterate($str)
    {

        $tr = array(
            "Ґ" => "G", "Ё" => "YO", "Є" => "E", "Ї" => "YI", "І" => "I",
            "і" => "i", "ґ" => "g", "ё" => "yo", "№" => "#", "є" => "e",
            "ї" => "yi", "А" => "A", "Б" => "B", "В" => "V", "Г" => "G",
            "Д" => "D", "Е" => "E", "Ж" => "ZH", "З" => "Z", "И" => "I",
            "Й" => "Y", "К" => "K", "Л" => "L", "М" => "M", "Н" => "N",
            "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T",
            "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "TS", "Ч" => "CH",
            "Ш" => "SH", "Щ" => "SCH", "Ъ" => "'", "Ы" => "Y", "Ь" => "",
            "Э" => "E", "Ю" => "YU", "Я" => "YA", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ж" => "zh",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "'",
            "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya"
        );
        return strtr($str, $tr);
    }

    public static function to_url($str)
    {
        return htmlspecialchars(strtr(
                        UTF8::str_ireplace(' ', '_', Controller_Admin::transliterate(UTF8::transliterate_to_ascii($str)))
                        , array("\"" => '', "'" => '')
                )
        );
    }

    static public function sitemap()
    {
        return array();
    }

    static public function sitemapXML()
    {
        return array();
    }

    static public function get_user_info($id = 1)
    {
        $user = Sprig::factory('user');
        $user->values(array('id' => $id))->load(NULL, 1);
        return $user->as_array();
    }

    public function acl_id()
    {
        // Controller namespace, controller name
        return 'c:' . strtolower($this->request->controller);
    }

    /**
     * AACL_Resource::acl_actions() implementation
     *
     * @param	bool	$return_current [optional]
     * @return	mixed
     */
    public function acl_actions($return_current = FALSE)
    {
        if ($return_current)
        {
            return $this->request->action;
        }

        // Find all actions in this class
        $reflection = new ReflectionClass($this);

        $actions = array();

        // Add all public methods that start with 'action_'
        foreach ($reflection->getMethods(ReflectionMethod::IS_PUBLIC) as $method)
        {
            if (substr($method->name, 0, 7) === 'action_')
            {
                $actions[] = substr($method->name, 7);
            }
        }

        return $actions;
    }

    /**
     * AACL_Resource::acl_conditions() implementation
     *
     * @param	Model_User	$user [optional] logged in user model
     * @param	object    	$condition [optional] condition to test
     * @return	mixed
     */
    public function acl_conditions(Model_User $user = NULL, $condition = NULL)
    {
        if (is_null($user) AND is_null($condition))
        {
            // We have no conditions
            return array();
        }
        else
        {
            // We have no conditions so this test should fail!
            return FALSE;
        }
    }

    /**
     * AACL_Resource::acl_instance() implementation
     *
     * Note that the object instance returned should not be used for anything except querying the acl_* methods
     *
     * @param	string	Class name of object required
     * @return	Object
     */
    public static function acl_instance($class_name)
    {
        // Return controller instance populated with manipulated request details
        $instance = new $class_name(Request::instance());

        $controller_name = strtolower(substr($class_name, 11));

        if ($controller_name !== Request::instance()->controller)
        {
            // Manually override controller name and action
            $instance->request->controller = strtolower(substr(get_class($this), 11));

            $instance->request->action = NULL;
        }

        return $instance;
    }

}
