<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_MySettings extends Controller_Admin {

    public $template		= 'mysettings_list.tpl';

    public $module_title	= 'My Settings';
    public $module_desc_short   = 'My Settings Descr Short';
    public $module_desc_full	= 'My Settings Descr Full';

    public $user;

    public function action_index()
    {
        $this->view->assign('obj', Model_AdminMySettings::settings_load($_SESSION['user_id']));
    }

    public function action_update()
    {
        $_POST['username'] = 'username';
        
        $this->model = new Model_AdminMySettings();
	if( $this->model->settings_save($_SESSION['user_id']) )
	{
	    $this->redirect_to_controller($this->request->controller);
	}
	else
	{
	    $this->view->assign('errors', $this->model->errors);
	    $this->action_index();
	}
    }

}