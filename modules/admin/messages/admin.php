<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'translations' => array(
		'en-us' => 'English',
		'ru-ru' => 'Русский',
	),
    
    'username' => array(
        'Invalid Login or Password' => 'Invalid Login or Password',
        ),
    'captcha' => array(
        'Captcha::valid' => 'Invalid captcha value',
    ),
);
