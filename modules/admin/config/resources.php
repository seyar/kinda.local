<?php

    return array(
                array(
                    'name' => 'Domains',
                    'resource' => 'c:domains',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),
                                    
                                 )
                ),
                array(
                    'name' => 'File Manager',
                    'resource' => 'c:filemanager',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Users',
                    'resource' => 'c:users',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),
                                 )
                ),
                array(
                    'name' => 'Pages',
                    'resource' => 'c:pages',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Global Blocks',
                    'resource' => 'c:globalblocks',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),
                                 )
                ),
                array(
                    'name' => 'Groups',
                    'resource' => 'c:groups',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),
                                 )
                ),
                array(
                    'name' => 'Group Assigments',
                    'resource' => 'c:groupassigments',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Group Permissions',
                    'resource' => 'c:grouppermissions',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Frontend Users',
                    'resource' => 'c:frontendusers',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Style sheets',
                    'resource' => 'c:css',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Templates',
                    'resource' => 'c:layout',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Modules Control',
                    'resource' => 'c:modulescontrol',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Settings',
                    'resource' => 'c:settings',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'System Info',
                    'resource' => 'c:sysinfo',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Updates',
                    'resource' => 'c:updates',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'System Logs',
                    'resource' => 'c:syslogs',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'My Settings',
                    'resource' => 'c:mysettings',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Shortcuts',
                    'resource' => 'c:shortcuts',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
                array(
                    'name' => 'Widgets Managment',
                    'resource' => 'c:widgetsmanager',
                    'actions' => array(
                                    array(
                                        'name' => 'All',
                                        'action' => '*'
                                    ),

                                 )
                ),
            );
