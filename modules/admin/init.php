<?php defined('SYSPATH') or die('No direct script access.');

Route::set
	(
	    'admin_ajax',
	    'admin/ajax/(/<module_action>(/<id>))',
	    array( 'id' => '\d{1,10}', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'ajax',
		    'action'        => 'test',
		    'id'            => NULL,
                )
	)
;

Route::set
	(
	    'admin_captcha',
	    'admin/captcha/<id>',
	    array( 'id' => '.+', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'captcha',
		    'action'        => 'show',
		    'id'            => NULL,
                )
	)
;
Route::set
	(
	    'admin_sp',
	    'admin/login/<id>/sp',
	    array( 'id' => '.+', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'login',
		    'action'        => 'sp',
		    'id'            => NULL,
                )
	)
;

//Route::set
//        (
//	    'admin_modules',
//	    'admin/modules/<module_name>((/<id>)/<module_action>)',
//	    array( 'id' => '\d{1,10}', )
//        )
//        ->defaults
//        (
//            array(
//		    'controller'	=> 'modules',
//		    'module_name'	=> NULL,
//		    'id'		=> NULL,
//		    'module_action'	=> 'index',
//                )
//	)
//;


// Final Admin Route
Route::set
        (
	    'admin',
	    'admin(/<controller>)((/<id>)/<action>)',
	    array( 'id' => '\d{1,10}', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'main',
		    'id'	    => NULL,
		    'action'        => 'index',
                )
	)
;

// Load languages into LANGUAGE_ROUTE
Controller_AdminFrontend::init();

// 
Event::add('system.routing', array('Controller_AdminFrontend', 'ready'));
