<?php defined('SYSPATH') or die('No direct script access.');

return array
(
        'mainwelcome' => '<h3>Welcome to the administrative panel control system Chimera CMS!</h3>
                    <p style="padding-top: 10px;">The control system Chimera CMS is designed so that it is convenient to work with even novice computer user. 
					<br />The main advantage is that Chimera is a easy to remember system with high-quality and original design, and, most importantly, allows you to edit and update the contents of the site, without the help of designers, webmasters and programmers.</p>',
        'Pages Descr Short' => 'Module Pages site is intended for the management of the site pages, edit them, move and structuring. This module allows you to edit and work with the page\'s content. ',

        'Pages Descr Full'	=>
                                'To view or create the page, select "Content-> Pages", click "Create".<br />
                                On this page, you can create the following: <br />
                                1. Page. This item allows you to create the desired page, for further posting on the site. <br />
                                2. Folder. This folder will allow you to group and store groups of pages. <br />
                                3. Link. At the entrance to a particular page, the item will automatically move the user specified in the link to another page. <br /> <br />
                                After selecting the type of item, you can start filling the fields. <br />
                                The drop-down list of "Path to the page, select the desired section, and in the box to the right enter the URL of the page in a format *. html, where *- a short page name in Latin letters (for example, news_priglashenie.html). Note: The title page of your website should not coincide . <br />In the "Page Title" you enter a title page, which will be displayed at the top of the browser, through which the created page will open. <br />In the "Name of the page in the menu" you enter a page title, which will be displayed directly in the menu. <br />In the main field is filled with substantive content of the page. <br />Following the establishment of all transactions, click the "Save and exit" to return to the menu "Pages" or "Apply" to remain on the edited page. These same points apply in the case of creating a folder or Links.<br />
                                ',
    
	
        'Global Blocks Descr Short' => 'Global blocks this is static information that will be displayed on the site, regardless on which page the user is located. ',
        'Global Blocks Descr Full'	=> 'Global blocks allow you to combine ongoing information and store it in one place. For example, telephone company or copyrights site. <br />
		If you use the block structure of your site, then editing the text in one block, you change it and the whole site. This is convenient because you do not need to change it in each template or on each page separately. Properly breaking site on the blocks, you can simplify your work and save time.',
    
	
        'File Manager Descr Short' => 'File Manager lets you manage files and directories on the server. ',
        'File Manager Descr Full'	=> 'File Manager lets you manage files and directories on the server.
To add a file, click "Upload" in the window that appears, click "Choose" and select which files to download. For the structuring of files in the file manager for folders, click New Folder. To upload files to a new folder is enough going to come in and repeat the process of injection. Also, you can easily, by dragging the mouse, move files in a folder or basket. The features available in the upper panel file manager will allow you to quickly and easily manage your files. ',
//

        'Layout Descr Short' => 'In this block are all available on the website page templates. There is an opportunity to view, copy, edit and delete. ',
        'Layout Descr Full' => '

In this block, are all available on the website page templates. There is an opportunity to view, copy, edit and delete. Wild is one of the main tools control the appearance of pages on the site. The same document can be displayed using several templates. Templates can be divided into two groups conditional:<br />
1. Those templates that are used to create the submission web page. Such as: menu template, the template home page, etc.<br />
2. Templates for displaying documents website. For each type of documents on the website, you can specify a default template is displayed.<br /><br />
The main menu displays immediately templates to display the site interface, and when editing can be controlled by templates that serve to display the documents website.
',

    'CSS Descr Short'   => 'In this block are all available on the site stylesheet. There is an opportunity to view, copy, edit and delete. ',
    'CSS Descr Full'    => 'In this block, are all available on the site stylesheet. There is an opportunity to view, copy, edit and delete. <br />Style sheets are a good tool for improving and optimizing web-pages, which can easily extend the opportunities for effective improvement of the form pages. ',
//
    
    'Users Descr Short'     => 'This section allows you to manage registered users of the site, organizing their personal study. ',
    'Users Descr Full' => '
                    This section allows you to manage registered users of the site, organizing their personal study.
                    Also in this section, "Community" contains a full database of registered users. The administrator can quickly delete, modify data or to block access to specific users.<br /><br />
                    Registration on the site may be used for several purposes:<br />
                    &bull; a certain group of users can be given the right to edit any of the sections through access to the administrative section; <br />
                    &bull; Office e-mail-mailing users of the site and subscribed to it; <br />
                    &bull; endowed with a certain group of users viewing rights of private partitions <br />
                    &bull; etc.',

    'Groups Descr Short' => 'This section is intended to view, create, edit and delete groups.',
    'Groups Descr Full' => 'This section is intended to view, create, edit and delete groups.',

    'Group Assigments Descr Short' => 'In this section, there is the possibility to refer any or group of participants to a particular group.',
    'Group Assigments Descr Full' => 'In this section, there is the possibility to refer any or group of participants to a particular group.',
	
    'Group Permissions Descr Short' => 'This section is intended for installation of certain human groups.',
    'Group Permissions Descr Full' => 'This section is intended for installation of certain human groups. You can specify which groups will have access to certain pages and sections, and what does not. ',

    'Frontend Users Descr Short' => 'This section displays all registered users of the site. ',
    'Frontend Users Descr Full' => 'This section displays all registered users of the site. <br />You can view, edit, activate, and delete user profiles. It is possible to use a function to certain users by identifying their checkmarks to the left of the username. ',
//
    
    'Settings Descr Short' => 'This section allows you to adjust and edit the basic settings of the site. ',
    'Settings Descr Full' => 'This section allows you to make adjustments and edit the basic settings of the site. <br />
	On the General tab is possible to change the information about the main title of the site, change the e-mail administrator, etc. <br />
	Tab "Domains" to add the new domain, or edit existing ones. <br />
	Tab "Languages" is an opportunity to add new languages to manage linguistic versions of the site, as well as edit existing ones. <br />
	Tab "Mail" allows you to set the name and e-mail, from which will be sent notification (Notification, etc.). <br />
	Tab "Performance" allows you to manage cache settings, setting time, as well as select objects for the cache. <br />
	Tab "Stop Site" allows pause site, as well as edit the message to be displayed to users when trying to access the site. ',


    'Updates Descr Short' => 'This section allows you to view all available updates for the site, as well as install them.',
    'Updates Descr Full'  => 'This section allows you to view all available updates for the site, as well as install them.',
	
    'System Logs Descr Short' => 'System logs - a module allowing you to view all system actions (logs) by users and administrators.',
    'System Logs Descr Full' => 'System logs - a module allowing you to view all system actions (logs) by users and administrators.',
//
    
    'Modules Descr Short' => 'This section controls the modular architecture of the system.',
    'Modules Descr Full' => 'Functional programming modules perform many functions on the site. From the organization of electronic online stores before the polls. The control system module allows administrators to configure, manage, and use of functional modules that were developed and connected specifically for this project. ',

//



    'My Settings Descr Short' => 'This section is intended to edit the settings for the user in the administrative part.',
    'My Settings Descr Full'  => 'This section is intended to edit the settings for the user in the administrative part. <br />
	Here you can change the current password to a new, changed e-mail, change the language interface convenient to you, and as noted, will be displayed "Labels" or not. <br />
	After entering all the data, click "Apply" to stay on the village of editing or "Save and exit" to return to the section "My Profile". ',
//


    

    'Shortcuts Descr Short'=>'This section allows you quick access to frequently-used functions of the system of governance.',
    'Shortcuts Descr Full'=>'This section allows you quick access to frequently-used functions of the system of governance.',


	'have_default_password' => '<b>Warning!</b> You use a password by default. Do not do this. It is very important to use secure passwords. <br/>
		You can easily change yours one by clicking <a href="/admin/users/">this link</a>',

);

