<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:27:18 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<script type="text/javascript" src="/static/admin/js/settings.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.serializeanything.js"></script>

<!--floating block-->
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
    <form method="post" action="">
    <input type="text" name="actTab" id="actTab" value="common" />
    <div class="absBlocks floatingInner">
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td style="width:37%">
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?>update">
                                        <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
        </div>
        <div class="absBlocks floatingPlaCorner L"></div>
        <div class="absBlocks floatingPlaCorner R"></div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

        <!--floating block-->

        <div class="rel" >

            <div class="whiteblueBg padding20px font14px">
                <ul class="tabs">
                    <li class="active" id="common"><a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Common')?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="domains">
                        <a href="#" onclick="loadDomains();">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Domains'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="languages">
                        <a href="#" onclick="langLoad();">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Languages'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="mail">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Mail'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="cache">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Cache'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="sitestop">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Site Stop'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                </ul>
                <div class="clear"></div>
                <br />

        <? if ($errors): ?><div class="errors"><?foreach ($errors as $message) echo $message;?></div><? endif; ?>
    </div>
    <div class="whiteblueBg">
        <!-- **** -->
        <div id="" class="tabContent common padding20px"><? include 'settings_common.php'; ?></div>
        <div id="" class="tabContent domains  openedTab"><? include 'settings_domains.php'; ?></div>
        <div id="" class="tabContent languages "><? include 'settings_languages.php'; ?></div>
        <div id="" class="tabContent mail "><? include 'settings_mail.php'; ?></div>
        <div id="" class="tabContent cache "><? include 'settings_cache.php'; ?></div>
        <div id="" class="tabContent sitestop "><? include 'settings_sitestop.php'; ?></div>
        <!-- **** -->
    </div>
</form>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>
</div>
