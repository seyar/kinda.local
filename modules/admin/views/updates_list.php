<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 18.11.2010 - 17:43:22 Exp $
 *
 * Project:     Chimera2.local
 * File:        updates_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="rel whiteBg padding20px">
    <br />
    <?= I18n::get('Update has not yet released') ?>. 
    <br />
                        <div class="absBlocks side L"></div>
                        <div class="absBlocks side R"></div>
                        <div class="absBlocks corner L"></div>
                        <div class="absBlocks corner R"></div>
                    </div>