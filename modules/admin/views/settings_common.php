<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 17:59:38 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_common.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>

<div class="flLeft" style="width: 50%;">
<table width="100%" class="italic grayText2 borderNone">
    <tr><td>
			<div class="blocktitles"><?php echo I18n::get('Site name')?>: </div>
            <div class="winput"><input type="text" name="site_name" value="<?=htmlentities((isset($obj['site_name'])?$obj['site_name']:$_POST['site_name']),ENT_COMPAT,'UTF-8');?>" style="width: 90%;;"/></div></td>
    </tr>
    <tr><td>
			<div class="blocktitles"><?php echo I18n::get('Site owner')?>: </div>
            <div class="winput"><input type="text" name="site_owner" value="<?=  htmlentities((isset($obj['site_owner'])?$obj['site_owner']:$_POST['site_owner']),ENT_COMPAT,'UTF-8');?>" style="width: 90%;;"/></div></td>
    </tr>
    <tr><td>
			<div class="blocktitles"><?php echo I18n::get('Admin email')?>: </div>
			<div class="winput"><input type="text" name="admin_email" value="<?=(isset($obj['admin_email'])?$obj['admin_email']:$_POST['admin_email'])?>" style="width: 90%;;"/></div></td>
    </tr>
    
    <tr><td><div class="blocktitles"><?php echo I18n::get('Session timeout')?>: </div>
        <div class="winput"><input type="text" name="session_timeout" value="<?=(isset($obj['session_timeout'])?$obj['session_timeout']:$_POST['session_timeout']);?>" style="width: 90%;;"/></div></td>
    </tr>
    
</table>
</div>
<div class="flLeft commonset"  style="width: 50%;">
<table width="100%" class="italic grayText2 borderNone">
    <tr><td class="checklabel">
            <label><input type="checkbox" name="url_autofix" value="1" class="flLeft"
                   <?if($obj['url_autofix'] || $_POST['url_autofix']):?> checked="checked" <?endif;?> /><?php echo I18n::get('Address autofix')?></label>
        </td>
    </tr>
    <tr><td  class="checklabel">
            <label><input type="checkbox" name="session_ip_lock" value="1" class="flLeft"
                   <?if($obj['session_ip_lock'] || $_POST['session_ip_lock']):?> checked="checked" <?endif;?> /><?php echo I18n::get('Lock session to IP')?></label></td>
    </tr>
</table>
</div>
<div class="clear"></div>