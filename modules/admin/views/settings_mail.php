<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 18:03:26 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_mail.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<br />
<div class="padding20px">
    <div class="dottedBottomBorder" ></div>
    <table cellspacing="5" width="100%" class="italic grayText2 borderNone" >
        <tr>
            <td width="47%" nowrap><?php echo I18n::get('Send email from name')?>:&nbsp; 
                <input type="text" name="mail_from_name" value="<?= htmlentities((isset($obj['mail_from_name'])?$obj['mail_from_name']:$_POST['mail_from_name']) ,ENT_COMPAT,'UTF-8');?>" style="width:58%;"/>
            </td>
            <td class="dottedLeftBorder" style="padding-left: 10px; width:48%"><?php echo I18n::get('Send email from address')?>: &nbsp;
                <input type="text" name="mail_from_address" value="<?= htmlentities((isset($obj['mail_from_address'])?$obj['mail_from_address']:$_POST['mail_from_addresss']) ,ENT_COMPAT,'UTF-8');?>"  style="width:58%;"/>
            </td>
        </tr>
    </table>
</div>