<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 18.11.2010 - 17:22:29 Exp $
 *
 * Project:     Chimera2.local
 * File:        sysinfo.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<style type="text/css">
td.e {
    width: 30%;
}
</style>

<div class="rel">
    <div class="whiteBg padding20px pre-wrapie">
            <?=$content?>

        <!-- *************** /content END HERE ***************** -->

    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>