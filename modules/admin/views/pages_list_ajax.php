<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 15:39:58 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages_list_ajax.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<? if (is_array($rows) && count($rows) > 0): ?>
    <ul class="rel" style="background-color: #FFF;">
<? foreach ($rows as $key => $item): ?>
        <li id="page_<?=$item['id']?>" class="node level-<?echo count(explode('/',$item['parent_url']))-1;?> children-visible <?=($key % 2 == 0?'odd':'even');?>">
            <div class="page">
                <input type="checkbox" name="" value="" class="absBlocks checkBox listCheckbox"/>
                <a href="/admin/pages/<?=$item['id']?>/edit" class="pageTitle"><img src="/static/admin/images/ico_<?if($item['is_default']==1 || $item['pagetypes_id'] == 2):?>main<?else:?>content<?endif;?>page.gif" alt="" class="vMiddle"/> <?=$item['page_title']?></a>
                <a href="/admin/pages/<?=$item['id']?>/edit" class="absBlocks pageUrl"><?=$item['page_url']?></a>

                <img alt="Drag and Drop" src="/static/admin/images/drag_to_sort.gif" class="handle_reorder" />
                <img alt="Drag to Copy" src="/static/admin/images/drag_to_copy.gif" class="handle_copy" />
                <img title="" style="display: none;" src="/static/admin/images/spinner.gif" id="busy-3" class="busy" alt="" />

                <span class="absBlocks pageicons">

                    <a href="/admin/pages/<?=$item['id']?>/active" class="ajaxSwitchColumn">
                        <img src="/static/admin/images/act_green.png" alt="active" <? if($item['status']!=1):?>class="hidden"<?endif;?>/>
                        <img src="/static/admin/images/act_red.png" alt="inactive"  <? if($item['status']==1):?>class="hidden"<?endif;?>/>
                    </a>
                    <a href="/admin/pages/<?=$item['id']?>/delete" class=""><img src="/static/admin/images/del.gif" alt="settings"/></a>

                </span>
            </div>
            <?if($item['pagetypes_id'] == 2):?>
                <img align="middle" title="" src="/static/admin/images/expand.png" class="absBlocks expander" alt="toggle children" />
            <?endif;?>

        </li>
<? endforeach; ?>
    </ul>
<? endif; ?>

<? /*

        <li class="close" id="{$value.id}" value="{$value.parent_id}">
            <a href="/admin/pages/{$value.id}/delete" class="pageLink l5"><img src="/static/admin/images/del.png" alt="delete" onclick="return confirm(Chimera.lang.are_you_sure);" title="{php}echo I18n::get('Delete'){/}"/></a>
            <a href="/admin/pages/{$value.id}/follow" class="pageLink l2"><img src="/static/admin/images/tosite.png" alt="to the site" title="{php}echo I18n::get('View on site'){/}"/></a>
            <a href="/admin/pages/{$value.id}/edit"   class="pageLink l4"><img src="/static/admin/images/edi.png" alt="edit" title="{php}echo I18n::get('Edit'){/}"/></a>
            <a href="/admin/pages/{$value.id}/copy"   class="pageLink l3"><img src="/static/admin/images/cop.png" alt="copy" title="{php}echo I18n::get('Copy'){/}"/></a>
            <a href="/admin/pages/{$value.id}/active" class="pageLink l1"><img src="/static/admin/images/{if $value.status}act{/else}inact{/}.png" alt="active" title="{php}echo I18n::get('Active'){/}"/></a>
            <input type="checkbox" name="chk[{$value.id}]" value="{$value.id}"
                   class="listCheckbox" style="margin:-15px {$value.depth*18+39}px 0 -{$value.depth*18+50}px; "/><span  title="{$value.page_title}">{$value.page_url|truncate:80:'...':false} (<i>{$value.page_title|truncate:80:'...':false}</i>)</span>

          {if $value.pagetypes_id == 2} {* Folder *}
          <ul>
          {include file="pages_list_ajax.tpl" rows=$value.childs}
          </ul>
          {/if}
         *

</li> <!-- Page {$value.page_title} -->
 */ ?>