<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 16.11.2010 - 17:38:31 Exp $
 *
 * Project:     Chimera2.local
 * File:        page_edit_options.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<table class="borderNone italic grayText2" width="100%">
    <tr>
        <td style="width: 25%;">
            <?=I18n::get('Template');?>
            <div style="width:125px;"><?echo Form::select('page_template', $page_template_list, (isset($obj['page_template'])?$obj['page_template']:$_POST['page_template']));?></div>

        </td>
        <td style="width: 25%;" class="checklabel">
            <label><input type="checkbox" name="is_default" value="1" <?if($obj['is_default'] || $_POST['is_default']):?>checked="checked"<?endif;?>/><?=  I18n::get('Home');?></label>
            <label><input type="checkbox" name="status" value="1" <?if($obj['status'] || $_POST['status'] || !$obj['id']):?>checked="checked"<?endif;?>/><?=  I18n::get('Page active');?></label>
        </td>
        <td  class="checklabel" style="width: 25%;">
            <label><input type="checkbox" name="is_cached" value="1" <?if($obj['is_cached'] || $_POST['is_cached'] || !$obj['id']):?>checked="checked"<?endif;?>/><?=  I18n::get('To cache');?></label>
            <label><input type="checkbox" name="is_on_map" value="1" <?if($obj['hide_from_map'] || $_POST['hide_from_map']):?>checked="checked"<?endif;?>/><?=  I18n::get('Hide from the map');?></label>
        </td>
        <td style="width: 25%;" class="tRight">
            <div style="width:73px; margin-left: 10px;" class="flRight"><?echo Form::select('visible_to', $visible_to, (isset($obj['visible_to'])?$obj['visible_to']:$_POST['visible_to']));?></div>
            <span class="vMiddle"><?echo I18n::get('The page is visible to');?>&nbsp;</span>
            <div class="clear"></div>

        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="blocktitles"><?echo I18n::get('Description');?> (description):</div>
            <textarea style="width:97%" rows="6" cols="1" name="page_description"><?=(isset($obj['page_description'])?$obj['page_description']:$_POST['page_description'])?></textarea>
        </td>
        <td colspan="2">
            <div class="blocktitles"><?echo I18n::get('Keywords, with the comma');?>:</div>
            <textarea style="width:97%" rows="6" cols="1" name="page_keywords"><?=(isset($obj['page_keywords'])?$obj['page_keywords']:$_POST['page_keywords'])?></textarea>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="blocktitles"><?echo I18n::get('Associate words');?>:</div>
            <textarea style="width:97%" rows="6" cols="1" name="page_associated_words"><?=(isset($obj['page_associated_words'])?$obj['page_associated_words']:$_POST['page_associated_words'])?></textarea>
        </td>
        <td colspan="2">
            <div class="blocktitles"><?echo I18n::get('Forbidden Words');?>:</div>
            <textarea  style="width:97%" rows="6" cols="1" name="page_disabled_words"><?=(isset($obj['page_disabled_words'])?$obj['page_disabled_words']:$_POST['page_disabled_words'])?></textarea>
        </td>
    </tr>
</table>