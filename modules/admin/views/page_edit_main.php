<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 16.11.2010 - 17:35:13 Exp $
 *
 * Project:     Chimera2.local
 * File:        page_edit_main.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>



<table class="borderNone italic grayText2" width="100%">
    <tr>
        <td><?= I18n::get('Edit content') ?>:</td>
    </tr>
    <tr>
        <td colspan="4">
            <textarea name="page_content" rows="5" cols="50" id="ckeditor"><?echo (isset($obj['page_content']) ? $obj['page_content'] : $_POST['page_content']) ?></textarea>
        </td>
    </tr>
</table>
