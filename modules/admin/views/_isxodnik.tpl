<!DOCTYPE html PUBLIC  "-//W3C//DTD XHTML 1.0 Strict//EN" "www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>ChimeraCMS : {$title}</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta http-equiv="X-UA-Compatible"content="IE=EmulateIE7"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="author" content="BestItSolutions"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/styleLib.css"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/googleBtn.css"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/paginator.css"/>
        <!--[if ie]><link rel="stylesheet" type="text/css" href="/static/admin/css/styleIe.css"/><![endif]-->
        <script type="text/javascript" src="/vendor/jquery/jquery.min.js"></script>
        <!-- additional css & js here -->

        <script type="text/javascript" src="/static/admin/js/main.js"></script>
        <!--        custom select-->
        <link rel="stylesheet" href="/vendor/jquery/customselect/styles.css" type="text/css"/>
        <script type="text/javascript" src="/vendor/jquery/customselect/jquery.customselect.js"></script>

        <script type="text/javascript" src="/vendor/jquery/jquery.paginator"></script>
        
        <!--        custom select-->
        <!--        <script type="text/javascript" src="/static/admin/js/clock.js"></script>-->
        <script type="text/javascript" src="/static/admin/js/shortcuts.js"></script>
        <script type="text/javascript" src="/static/admin/js/lang/{php}echo I18n::$lang{/php}.js"></script>

    </head>
    <body>
        111
        <div class="container">
            <div class="wrap rel">
                <div class="precontent"></div>
                <!--            end header-->
                <!-- main part-->
                <div class="mainContent">
                    <!--                breadcrumbs-->
                    <div class="breadcrumbs rel">
                        <h1>Страницы</h1>
                        <ul>
                            <li><a href="#">Главная</a> &gt; </li>
                            <li><a href="#">Контент</a> &gt; </li>
                            <li class="last">Страницы</li>
                        </ul>
                        <div class="absBlocks breadcr L"></div>
                        <div class="absBlocks breadcr R"></div>
                    </div>
                    <!--                breadcrumbs -->
                    <div class="rel">
                        <!--floating block-->
                        <div class="rel whiteBg floatingOuter" id="contentNavBtns">
                            <div class="whiteblueBg absBlocks floatingInner">
                                <div class="padding20px">
                                    <table width="100%">
                                        <tr>
                                            <td class="vMiddle">C отмеченными </td>
                                            <td><select name="sel0"><option>Удалить</option></select></td>
                                            <td style="width:58%"><button class="btn formUpdate" type="button">
                                                    <span><span>{php}echo I18n::get('Apply'){/}</span></span></button>
                                            </td>
                                            <td class="vMiddle"><div class="dottedLeftBorder">На старнице </div></td>
                                            <td>
                                                <select name="sel1">
                                                    <option>10 строк</option>
                                                    <option>20 строк</option>
                                                    <option>30 строк</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <!--floating block-->
                        <div class="whiteBg">

                            <!--                        content module-->
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th style="width:5%;padding-left:0;" class="tCenter"><input type="checkbox" name="" value=""/></th>
                                        <th style="width:5%" class="tCenter">№</th>
                                        <th style="width:15%" class="tRight">Дата</th>
                                        <th style="width:25%">Контактные данные <a href="#">up</a><a href="#">down</a></th>
                                        <th>Сообщение</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="tCenter"><input type="checkbox" name="" value=""/></td>
                                        <td><p>10</p></td>
                                        <td><p class="tRight">2007.10.16<br /><span class="grayText">18:08</span></p></td>
                                        <td><p>dd</p></td>
                                        <td><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris urna, dignissim ac sodales ut, vestibulum suscipit augue. Nullam ut orci nec dolor dignissim pharetra. Proin consectetur convallis purus. Cras volutpat neque non diam convallis aliquam. Morbi tristique ipsum at nibh elementum eu rhoncus odio aliquet. Vestibulum nulla felis, suscipit a varius non, bibendum non tortor. Ut metus mauris, feugiat eu euismod at, tincidunt vel tellus. Pellentesque venenatis suscipit nibh et auctor. Curabitur vel orci lorem, et semper sem. Phasellus et dui arcu. Vestibulum in ullamcorper felis. Suspendisse consequat erat vitae orci ullamcorper vel aliquet augue tempus. Proin varius ligula eget nulla dictum ut consectetur massa pulvinar. Curabitur mollis metus vel nisi imperdiet at tincidunt nisl congue.
                                                In rhoncus lobortis sem id mattis. Cum sociis natoque pena</p>
                                        </td>
                                    </tr>
                                    <tr class="even">
                                        <td class="tCenter"><input type="checkbox" name="" value=""/></td>
                                        <td><p>10</p></td>
                                        <td><p class="tRight">2007.10.16<br /><span class="grayText">18:08</span></p></td>
                                        <td><p>dd</p></td>
                                        <td><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris urna, dignissim ac sodales ut, vestibulum suscipit augue. Nullam ut orci nec dolor dignissim pharetra. Proin consectetur convallis purus. Cras volutpat neque non diam convallis aliquam. Morbi tristique ipsum at nibh elementum eu rhoncus odio aliquet. Vestibulum nulla felis, suscipit a varius non, bibendum non tortor. Ut metus mauris, feugiat eu euismod at, tincidunt vel tellus. Pellentesque venenatis suscipit nibh et auctor. Curabitur vel orci lorem, et semper sem. Phasellus et dui arcu. Vestibulum in ullamcorper felis. Suspendisse consequat erat vitae orci ullamcorper vel aliquet augue tempus. Proin varius ligula eget nulla dictum ut consectetur massa pulvinar. Curabitur mollis metus vel nisi imperdiet at tincidunt nisl congue.
                                                In rhoncus lobortis sem id mattis. Cum sociis natoque pena</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tCenter"><input type="checkbox" name="" value=""/></td>
                                        <td><p>10</p></td>
                                        <td><p class="tRight">2007.10.16<br /><span class="grayText">18:08</span></p></td>
                                        <td><p>dd</p></td>
                                        <td><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris urna, dignissim ac sodales ut, vestibulum suscipit augue. Nullam ut orci nec dolor dignissim pharetra. Proin consectetur convallis purus. Cras volutpat neque non diam convallis aliquam. Morbi tristique ipsum at nibh elementum eu rhoncus odio aliquet. Vestibulum nulla felis, suscipit a varius non, bibendum non tortor. Ut metus mauris, feugiat eu euismod at, tincidunt vel tellus. Pellentesque venenatis suscipit nibh et auctor. Curabitur vel orci lorem, et semper sem. Phasellus et dui arcu. Vestibulum in ullamcorper felis. Suspendisse consequat erat vitae orci ullamcorper vel aliquet augue tempus. Proin varius ligula eget nulla dictum ut consectetur massa pulvinar. Curabitur mollis metus vel nisi imperdiet at tincidunt nisl congue.
                                                In rhoncus lobortis sem id mattis. Cum sociis natoque pena</p>
                                        </td>
                                    </tr>
                                    <tr class="even">
                                        <td class="tCenter"><input type="checkbox" name="" value=""/></td>
                                        <td><p>10</p></td>
                                        <td><p class="tRight">2007.10.16<br /><span class="grayText">18:08</span></p></td>
                                        <td><p>dd</p></td>
                                        <td><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris urna, dignissim ac sodales ut, vestibulum suscipit augue. Nullam ut orci nec dolor dignissim pharetra. Proin consectetur convallis purus. Cras volutpat neque non diam convallis aliquam. Morbi tristique ipsum at nibh elementum eu rhoncus odio aliquet. Vestibulum nulla felis, suscipit a varius non, bibendum non tortor. Ut metus mauris, feugiat eu euismod at, tincidunt vel tellus. Pellentesque venenatis suscipit nibh et auctor. Curabitur vel orci lorem, et semper sem. Phasellus et dui arcu. Vestibulum in ullamcorper felis. Suspendisse consequat erat vitae orci ullamcorper vel aliquet augue tempus. Proin varius ligula eget nulla dictum ut consectetur massa pulvinar. Curabitur mollis metus vel nisi imperdiet at tincidunt nisl congue.
                                                In rhoncus lobortis sem id mattis. Cum sociis natoque pena</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- content module-->

                            <!-- paginator-->
                            <script language="javascript" type="text/javascript">
                                {literal}
                                $(document).bind('ready', function ()
                                {
                                    var page = /page=([^#&]*)/.exec(window.location.href);
                                    page = page ? page[1] : 1;

                                    $('#paginator').paginator(
                                    {
                                        pagesTotal: {/literal}10{$total_pages}{literal}
                                        ,pagesSpan: 10
                                        ,pageCurrent: page
                                        ,baseUrl: '?page='
                                        ,lang: {
                                            {/literal}
                                            next  : "<span>{php}echo I18n::get('Next'){/}</span>",
                                            last  : "{php}echo I18n::get('Last'){/}",
                                            prior : "<span>{php}echo I18n::get('Prior'){/}</span>",
                                            first : "<span>{php}echo I18n::get('First'){/}</span>",
                                            arrowRight : String.fromCharCode(8594),
                                            arrowLeft  : String.fromCharCode(8592)
                                            {literal}
                                        }
                                    });
                                });
                                {/literal}
                            </script>
                            <div class="paginator" id="paginator"></div>

                            <!-- paginator-->
                            <br />
                            <br />
                        </div>
                        <div class="absBlocks side L"></div>
                        <div class="absBlocks side R"></div>
                        <div class="absBlocks corner L"></div>
                        <div class="absBlocks corner R"></div>
                    </div>
                    <div class="bottomshadow"></div>
                </div>
                <!--main part-->
                <div class="absBlocks topMenu">
                    <div class="topMenuInner">

                    </div>
                    <!--                header-->
                    <div class="absBlocks topMenu_side"></div>
                    <div class="absBlocks topMenu_side R"></div>
                    <a href="/admin/" class="absBlocks logo"><img src="/static/admin/images/logo.png" alt=""/></a>

                    <!--                menu-->
                    <ul class="absBlocks mainMenu">
                        <li class="ico_content active"><a href="#">Контент</a></li>
                        <li class="divider"></li>
                        <li class="ico_shop "><a href="#">Магазин</a>
                            <span class="submenuAct"></span>
                            <span class="submenu">
                                <a href="#">Товары</a>
                                <span class="divider">|</span>
                                <a href="#" class="active">Категории</a>
                                <span class="divider">|</span>
                                <a href="#">Категории</a>
                                <span class="absBlocks submenu_side L"></span>
                                <span class="absBlocks submenu_side R"></span>

                                <span class="subsubmenu">
                                    <a href="#" class="active">подТовары</a>
                                    <span class="divider">|</span>
                                    <a href="#">подКатегории</a>
                                    <span class="divider">|</span>
                                    <a href="#">Категории</a>
                                    <span class="absBlocks subsubmenu_side L"></span>
                                    <span class="absBlocks subsubmenu_side R"></span>
                                </span>
                            </span>
                        </li>
                        <li class="divider"></li>
                        <li class="ico_publications "><a href="#">Публикации</a></li>
                        <li class="divider"></li>
                        <li class="ico_votes "><a href="#">Опрос</a></li>
                        <li class="divider"></li>
                        <li class="ico_faq "><a href="#">F.A.Q</a></li>
                        <li class="divider"></li>
                        <li class="ico_feedback"><a href="#">Связь</a></li>
                        <li class="divider"></li>
                        <li class="ico_seo "><a href="#">SEO</a></li>
                        <li class="divider"></li>
                        <li class="ico_settings "><a href="#">Настройки</a></li>
                    </ul>
                    <div class="clear"></div>
                    <ul class="absBlocks mainMenu rightIco">
                        <li class="divider"></li>
                        <li class="ico_recycler"><a href="#">Корзина</a></li>
                    </ul>
                    <!--                menu -->
                    <div class="absBlocks helpLinks">
                        <a href="#" class="showHelp">Показать справку</a>
                        <a href="#" class="addToShortcats">Добавить в ярлыки</a>
                    </div>

                    <div class="absBlocks topAccInfo tRight">
                        <span class="tBold">Admin,</span>
                        <ul class="accLinks">
                            <li><a href="#">Выход</a></li>
                            <li><a href="/" onclick="window.open(this.href);return false;">На сайт</a></li>
                        </ul>
                        <ul class="langs">
                            <li>Рус</li>
                            <li class="divider">|</li>
                            <li><a href="#">Eng</a></li>
                        </ul>
                    </div>
                    <!--                header-->
                </div>
                <div class="prefooter"></div>
            </div>
        </div>
        <div class="footer rel tCenter">
            Best IT Solutions Limited, 2007-{$quicky.now|date_format:"%Y"} &copy; All rights reserved.
        </div>
    </body>
</html>