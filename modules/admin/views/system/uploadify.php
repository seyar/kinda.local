<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 09.12.2010 - 17:56:28 Exp $
 *
 * Project:     kontext
 * File:        uploadify.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 *
 * @param $withEditLink param for edit photo link
 * @param $_GET['postInit'] param for post initialize uploadify
 */
?>
<!--uploadify-->
<script type="text/javascript" src="/static/admin/js/images_upload.js.php?u=<?= Session::instance()->get('user_id') ?>&n=<?= $_COOKIE[session_name()] ?>&path=<?= $admin_path . $controller . $id ?>&postInit=<?= $_GET['postInit'] ?>"></script>

<script type="text/javascript" src="/vendor/uploadify/jquery.uploadify.js"></script>
<link href="/vendor/uploadify/uploadify.css" rel="stylesheet" type="text/css" />

<!--<script type="text/javascript" src="/vendor/jquery/jquery.jeegoocontext.min.js"></script>-->
<!--<link href="/static/admin/css/contextmenu.css" rel="stylesheet" type="text/css" />-->
<!--uploadify-->
<script type="text/javascript" >
    function showImagesPreview()
    {
        $.getJSON('<?= $admin_path . $controller ?><?= isset($id) ? $id . '/' : '' ?>images_show', function(data)
        {
            newHtml = ''; totalImages=0;
            var len = data.files.length;
            for(var i=0; i<len; i++)
            {
                /*
                        newHtml += '<div class="imgPrev '+(data.files[i]==data.default_img ? 'default' : '')+'" file="'+data.files[i]
                                +'" rel="'+data.webpath+'/'+data.files[i]+'" '+' style="background-image: url('
                                +data.path+'/image_preview?name='+data.files[i]+')"> </div>';
                 */
                newHtml += '<div class="rel tCenter imgPrev '+(data.default_img == data.files[i] ? 'default' : '')+'" id="image_'+i+'" file="'+data.files[i]+'" rel="'+data['webpath']+data['files'][i]+'">\
                                <div class="image" onclick="selectPhoto($(this).parent());"><img src="'+data['webpath']+'prev_'+data['files'][i]+'" alt=""/></div>\
                                <a href="#" class="actPhoto" onclick="setDefault($(this).parent());return false;" title="Set Default Photo"></a>\
                                <? if (isset($withEditLink)): ?><a href="#" class="editPhoto" onclick="showPopup(this); return false;"><img src="/static/admin/images/gallery-del.png"/></a><? endif; ?><span class="comment">';
                if(data['comments'] && data['comments'][ data['files'][i] ])
                {
                    newHtml +=(data['comments'][ data['files'][i] ]['comment'].replace(/\\/g, ''));
                }

                newHtml +='</span><div class="smallPreloader hidden"><img src="/static/admin/images/preloader.gif" alt="preloader"/></div></div>';
                totalImages++;
            }

            $('#imagesPreview').html(newHtml);

            // show images on doubleclick
            $('#imagesPreview div.imgPrev').dblclick(function()
            {
                window.open($(this).attr('rel'));
            });

        });
    } // function

    function setDefault( thisObj )
    {
        $(thisObj).children('.smallPreloader').show();
        if( $(thisObj).hasClass('default') )
        {
            $.ajax({
                type: 'GET',
                url: '<?= $admin_path . $controller ?><?= $id ?>/cancel_default_image',
                success: function(data){
                    if(data=='1')
                    {
                        $('#imagesPreview div.imgPrev.default').removeClass('default');
//                        $(thisObj).addClass('default');
                        $(thisObj).children('.smallPreloader').hide();
                    }
                    else
                        alert('Server error!');
                },
                error: function(){ alert('Server error!'); }
            });
        }else
        {
            $.ajax({
                type: 'GET',
                url: '<?= $admin_path . $controller ?><?= $id ?>/images_default?name='+$(thisObj).attr('file'),
                success: function(data){
                    if(data=='1')
                    {
                        $('#imagesPreview div.imgPrev.default').removeClass('default');
                        $(thisObj).addClass('default');
                        $(thisObj).children('.smallPreloader').hide();
                    }
                    else
                        alert('Server error!');
                },
                error: function(){ alert('Server error!'); }
            });
        }
    }

    function deletePhotos()
    {
        filenames = [];
        i=0;
        $('#imagesPreview .imgPrev.selected').each(function(){
            filenames[i] = $(this).attr('file');
            i++;
        });

        if ( i == 0) { alert('Please choose photo, by clicking them');return false; }

        $.ajax({
            type: 'GET',
            data:{name:filenames},
            url: '<?= $admin_path . $controller ?><?= $id ?>/images_delete',
            success: function(data){
                if(data=='1')
                {
                    $('#imagesPreview .imgPrev.selected').remove();
//                    newMaxLeft++;
//                    $('#fileInput').uploadifySettings( 'queueSizeLimit', newMaxLeft );
                }
            }
        });
    }

    function selectPhoto(thisObj)
    {
        $(thisObj).toggleClass('selected');
    }
    
    $(document).ready(function(){
                showImagesPreview();
            });
</script>

<style type="text/css">
div.imgPrev { background: center top no-repeat; border:1px solid #999999;float:left; height:<?= (isset($image_prev_height) ? $image_prev_height : 126) ?>px;margin:28px 5px 3px 3px; width:<?= (isset($image_prev_width) ? $image_prev_width : 120) ?>px; overflow: hidden;}
div.imgPrev .image{ /*height: 100px; width: 100%;*/ overflow: hidden; }
div.imgPrev .image img{ height: 100%; text-align: center; }
div.imgPrev.default { border: 3px solid yellow; margin:26px 3px 1px 1px; }
div.imgPrev.selected { border: 3px solid #2A8EFF; margin:26px 3px 1px 1px;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px; }
div.imgPrev .delPhoto{ position: absolute;top: 0; right: 0; z-index: 3;}
div.imgPrev .editPhoto{ position: absolute;top: 4px; right: 4px; z-index: 3; }
div.imgPrev .editPhoto img { width: 100%;height: 100%;}
div.imgPrev .actPhoto{ position: absolute;top: 4px; right: 4px; z-index: 3; background: url(/static/admin/images/act_red.png);width: 16px; height: 16px;}
div.imgPrev.default .actPhoto{ background-image: url(/static/admin/images/act_green.png); }
div.imgPrev .comment{ position: absolute;bottom: 0;width: 99%; text-align: center; left:0; }

.popup, .popupClone{ width: 600px; position: absolute; top: 0px; border: 1px solid #000;background: #fff; z-index: 6;text-align: left; padding: 5px; }
.popupInput{ margin: 2px 0; }
#popupClone{display: none;}

.overlay{ position: absolute;width: 100%;height: 100%; top: 0;left: 0; }
.smallPreloader{ position: absolute;top: 47px;left:0;width: 120px;height: 30px; text-align: center;}
</style>

<p><? echo I18n::get('Photogallery (double click to view)') ?></p>
<div id="images_list">
    <div id="imagesPreview"><? echo I18n::get('Photos list') ?></div>
    <div class="clear"></div>
    <br>
    <div>
<!--        <div class="uploadifyClone" style="display:none;">
            <input id="fileInputClone" name="" type="file" />
        </div>-->
        <div id="uploadify" class="flLeft">
            <input id="fileInput" name="fileInput" type="file"/>
        </div>
        <button class="btn blue flRight" name="photosDelete" onclick="deletePhotos();return false;"><span><span>Удалить</span></span></button>
        <!--	    <a href="javascript:$('#fileInput').uploadifyUpload();">Upload Files</a> |
            <a href="javascript:$('#fileInput').uploadifyClearQueue();">Clear Queue</a>-->
    </div>
	<div class="clear"></div>
    <ul id="menu" class="contextMenu hidden">
        <li id="default"><? echo I18n::get('Set Default') ?></li>
        <li id="delete"><? echo I18n::get('Delete') ?></li>
    </ul>
</div>
