<?php
defined('SYSPATH') or die('No direct script access.');
/* @version $Id: v 0.1 12.11.2010 - 12:29:54 Exp $
 *
 * Project:     Chimera2.local
 * File:        footer.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="bottomshadow" style="<?= $style_content; ?>"></div>
</div>
<!--main part-->
<div class="absBlocks topMenu">
    <div class="topMenuInner">

    </div>
    <!--                header-->
    <div class="absBlocks topMenu_side"></div>
    <div class="absBlocks topMenu_side R"></div>
    <a href="/admin/" class="absBlocks logo"><img src="/static/admin/images/logo.png" alt=""/></a>

    <!--                menu-->
    <ul class="absBlocks mainMenu" id="mainMenu">
        <li class="ico_content<? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'pages')): ?> active<? endif; ?>">
			<a href="<?= $admin_path ?>pages/"><?= I18n::get('Content') ?></a>
			<span class="submenuAct"></span>
			<span class="submenu" style="width: 315px;">
				<span class="submenuItem"><a href="<?= $admin_path ?>pages/"  <? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'pages')): ?>class="active"<? endif; ?>><?= I18n::get('Pages') ?></a></span>
				<span class="divider">|</span>
				<span class="submenuItem"><a href="<?= $admin_path ?>globalblocks/" <? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'globalblocks')): ?>class="active"<? endif; ?>><?= I18n::get('Global Blocks') ?></a></span>
				<span class="divider">|</span>
				<span class="submenuItem"><a href="/vendor/elfinder/elfinder.html" target="_blank"><?= I18n::get('File Manager') ?></a></span>
				<span class="absBlocks submenu_side L"></span>
				<span class="absBlocks submenu_side R"></span>
			</span>
		</li>
		<!--                <li class="divider"></li>
						<li class="ico_publications<? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'publications')): ?> active<? endif; ?>"><a href="<?= $admin_path ?>modules/publications/">Публикации</a></li>-->
		<? foreach ($installed_modules as $obj): ?>
			<li class="divider"></li>
			<li class="ico_<?= $obj['fs_name'] ?><? if (strstr($_SERVER['REQUEST_URI'], $admin_path . $obj['fs_name'])): ?> active<? endif; ?>"><a href="<?= $admin_path ?><?= $obj['fs_name'] ?>/"><?= $obj['name'] ?></a></li>
		<? endforeach; ?>

		<!--                    моя вставка-->
		<li class="divider"></li>
		<li class="has_submenu ico_shop<? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'shop')): ?> active<? endif; ?>">
			<a href="<?= $admin_path ?>goods/"><?= I18n::get('shop') ?></a>
			<span class="submenuAct" style="left: 33px;"></span>
			<span class="submenu" style="left:0px;width: 209px;">

				<span class="submenuItem"><a href="<?= $admin_path ?>goods/"  <? if (strstr($_SERVER['REQUEST_URI'], 'goods')): ?>class="active"<? endif; ?>><?= I18n::get('goods'); ?></a></span>

				<span class="divider">|</span>

				<span class="submenuItem"><a href="<?= $admin_path ?>orders/"  <? if (strstr($_SERVER['REQUEST_URI'], 'orders')): ?>class="active"<? endif; ?>><?= I18n::get('orders'); ?></a></span>

				<span class="divider">|</span>

				<span class="submenuItem"><a href="<?= $admin_path ?>customers/"  <? if (strstr($_SERVER['REQUEST_URI'], 'customers')): ?>class="active"<? endif; ?>><?= I18n::get('customers') ?></a></span>

<!--				<span class="divider">|</span>-->
<!--                            <span class="submenuItem"><a href="<?= $admin_path ?>updates/"  <? if (strstr($_SERVER['REQUEST_URI'], 'updates')): ?>class="active"<? endif; ?>><?= I18n::get('Updates') ?></a></span>
				<span class="divider">|</span>
				<span class="submenuItem"><a href="<?= $admin_path ?>syslogs/"  <? if (strstr($_SERVER['REQUEST_URI'], 'syslogs')): ?>class="active"<? endif; ?>><?= I18n::get('System Logs') ?></a></span>
				<span class="divider">|</span>-->
<!--				<span class="submenuItem"><a href="<?= $admin_path ?>paymethods/"  <? if (strstr($_SERVER['REQUEST_URI'], 'paymethods')): ?>class="active"<? endif; ?>><?= I18n::get('paymethods') ?></a></span>

				<span class="divider">|</span>
				<span class="submenuItem"><a href="<?= $admin_path ?>shipmethods/"  <? if (strstr($_SERVER['REQUEST_URI'], 'shipmethods')): ?>class="active"<? endif; ?>><?= I18n::get('Shipment options') ?></a></span>
				<span class="divider">|</span>
				<span class="submenuItem"><a href="<?= $admin_path ?>brands/"  <? if (strstr($_SERVER['REQUEST_URI'], 'brands')): ?>class="active"<? endif; ?>><?= I18n::get('brands') ?></a></span>-->
				<span class="divider">|</span>
				<span class="submenuItem"><a href="<?= $admin_path ?>currencies/"  <? if (strstr($_SERVER['REQUEST_URI'], 'currencies')): ?>class="active"<? endif; ?>><?= I18n::get('currencies'); ?></a></span>
				<span class="absBlocks submenu_side L"></span>
				<span class="absBlocks submenu_side R"></span>
			</span>
		</li>

		<!--                    моя вставка-->
		<!--                <li class="divider"></li>
						<li class="ico_votes<? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'votes')): ?> active<? endif; ?>"><a href="#">Опрос</a></li>
						<li class="divider"></li>
						<li class="ico_faq<? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'faq')): ?> active<? endif; ?>"><a href="#">F.A.Q</a></li>
						<li class="divider"></li>
						<li class="ico_feedback<? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'feedback')): ?> active<? endif; ?>"><a href="#">Связь</a></li>
						<li class="divider"></li>
						<li class="ico_seo<? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'seo')): ?> active<? endif; ?>"><a href="#">SEO</a></li>-->
		<li class="divider"></li>
		<li class="has_submenu ico_settings<? if (strstr($_SERVER['REQUEST_URI'], $admin_path . 'settings')): ?> active<? endif; ?>">
			<a href="<?= $admin_path ?>settings/"><?= I18n::get('Settings') ?></a>
			<span class="submenuAct"></span>
			<span class="submenu" style="left:-310px;width: 395px;">
				<span class="submenuItem"><a href="<?= $admin_path ?>settings/"  <? if (strstr($_SERVER['REQUEST_URI'], 'settings')): ?>class="active"<? endif; ?>><?= I18n::get('Settings') ?></a></span>
				<span class="divider">|</span>
				<span class="submenuItem"><a href="<?= $admin_path ?>sysinfo/"  <? if (strstr($_SERVER['REQUEST_URI'], 'sysinfo')): ?>class="active"<? endif; ?>><?= I18n::get('System Info') ?></a></span>
				<span class="divider">|</span>
<!--                            <span class="submenuItem"><a href="<?= $admin_path ?>updates/"  <? if (strstr($_SERVER['REQUEST_URI'], 'updates')): ?>class="active"<? endif; ?>><?= I18n::get('Updates') ?></a></span>
				<span class="divider">|</span>
				<span class="submenuItem"><a href="<?= $admin_path ?>syslogs/"  <? if (strstr($_SERVER['REQUEST_URI'], 'syslogs')): ?>class="active"<? endif; ?>><?= I18n::get('System Logs') ?></a></span>
				<span class="divider">|</span>-->
				<span class="submenuItem">
					<a href="<?= $admin_path ?>layout/"  <? if (strstr($_SERVER['REQUEST_URI'], 'layout')): ?>class="active"<? endif; ?>><?= I18n::get('Layout') ?></a>
					<span class="subsubmenu">
						<a href="<?= $admin_path ?>layout/" <? if (strstr($_SERVER['REQUEST_URI'], 'layout')): ?>class="active"<? endif; ?>><?= I18n::get('Templates') ?></a>
						<span class="divider">|</span>
						<a href="<?= $admin_path ?>css/" <? if (strstr($_SERVER['REQUEST_URI'], 'css')): ?>class="active"<? endif; ?>><?= I18n::get('Style sheets') ?></a>
						<span class="absBlocks subsubmenu_side L"></span>
						<span class="absBlocks subsubmenu_side R"></span>
					</span>
				</span>
				<span class="divider">|</span>
				<span class="submenuItem">
					<a href="<?= $admin_path ?>users/"  <? if (strstr($_SERVER['REQUEST_URI'], 'users')): ?>class="active"<? endif; ?>><?= I18n::get('Users'); ?></a>
					<span class="subsubmenu">
						<a href="<?= $admin_path ?>users/" <? if (strstr($_SERVER['REQUEST_URI'], 'users')): ?>class="active"<? endif; ?>><?= I18n::get('Users') ?></a>
						<span class="divider">|</span>
						<a href="<?= $admin_path ?>groups/" <? if (strstr($_SERVER['REQUEST_URI'], 'groups')): ?>class="active"<? endif; ?>><?= I18n::get('Groups') ?></a>
						<span class="divider">|</span>
						<a href="<?= $admin_path ?>groupassigments/" <? if (strstr($_SERVER['REQUEST_URI'], 'groupassigments')): ?>class="active"<? endif; ?>><?= I18n::get('Group Assigments') ?></a>
						<span class="divider">|</span>
						<a href="<?= $admin_path ?>grouppermissions/" <? if (strstr($_SERVER['REQUEST_URI'], 'grouppermissions')): ?>class="active"<? endif; ?>><?= I18n::get('Group Permissions') ?></a>
						<span class="absBlocks subsubmenu_side L"></span>
						<span class="absBlocks subsubmenu_side R"></span>
					</span>
				</span>

				<span class="absBlocks submenu_side L"></span>
				<span class="absBlocks submenu_side R"></span>
			</span>
		</li>
	</ul>
	<!--
	<div class="clear"></div>
	<ul class="absBlocks mainMenu rightIco">
		<li class="divider"></li>
		<li class="ico_recycler"><a href="#"><?= I18n::get('Recycler') ?></a></li>
	</ul>
	-->
	<!--  menu -->
	<div class="absBlocks helpLinks">
		<a href="#" class="showHelp <? if ($show_module_desc): ?>hidden<? endif; ?>"><?= I18n::get('Show help') ?></a>
		<!-- <a href="#" class="addToShortcats"><?= I18n::get('Add to &quot;shortcuts&quot;') ?></a> -->
	</div>
	<div class="absBlocks topAccInfo tRight">
		<span class="tBold"><?= $user_info['fullname'] ?> </span>
		<ul class="accLinks">
			<li><a href="<?= $admin_path ?>login/logout/"><?= I18n::get('Logout') ?></a></li>
			<li><a href="/" onclick="window.open(this.href);return false;"><?= I18n::get('View site') ?></a></li>
		</ul>
		<? if (count($admin_lang) > 0): ?>
			<ul class="langs">
				<? foreach ($admin_lang as $item): ?>
					<? if ($item['id'] == $admin_default_lang): ?>
						<li><?= $item['shortname'] ?></li>
					<? else: ?>
						<li><a href="/admin/main/<?= $item['id'] ?>/set_lang"><?= $item['shortname'] ?></a></li>
					<? endif; ?>
					<? if ($item != end($admin_lang)): ?>
						<li class="divider">|</li>
					<? endif; ?>

				<? endforeach; ?>

			</ul>
		<? endif; ?>
		<!--                        <ul class="langs">
									<li>Рус</li>
									<li class="divider">|</li>
									<li><a href="#">Eng</a></li>
								</ul>-->
	</div>
	<!--                header-->
</div>
<div class="prefooter"></div>
</div>
</div>
<div class="jqmodal messageWindow jqmWindow"></div>
<div class="footer rel tCenter">
	Best IT Solutions Limited, 2007-<?= date('Y'); ?> &copy; All rights reserved.
</div>
</body>
</html>