<?php  defined('SYSPATH') or die('No direct script access.');
/* @version $Id: v 0.1 12.11.2010 - 17:46:52 Exp $
 *
 * Project:     Chimera2.local
 * File:        breadcrums.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
 ?>
<!--                breadcrumbs-->
<div class="breadcrumbs rel">
    <h1><?= $module['title'] ?></h1>
	<ul>
        <li><a href="<?=ADMIN_PATH?>"><?= I18n::get('Main') ?></a>
        <? if ('/'.Request::instance()->uri.'/' != ADMIN_PATH): ?>
             &gt; </li>          
        
        <li><a href="<?= $admin_path?><?=$controller?>"><?= $module['title'] ?></a>
        <? endif; ?>
        </li>
    </ul>
    <!--<ul>
        <li><a href="/">Главная</a> &gt; </li>
        <li><a href="#">Контент</a> &gt; </li>
        <li class="last">Страницы</li>
    </ul> -->
    <div class="absBlocks breadcr L"></div>
    <div class="absBlocks breadcr R"></div>
</div>
<!--breadcrumbs-->