<?php  defined('SYSPATH') or die('No direct script access.');
/* @version $Id: v 0.1 12.11.2010 - 12:22:19 Exp $
 *
 * Project:     Chimera2.local
 * File:        header.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<!DOCTYPE html PUBLIC  "-//W3C//DTD XHTML 1.0 Strict//EN" "www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>ChimeraCMS : <?=$title?></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta http-equiv="X-UA-Compatible"content="IE=EmulateIE8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="author" content="BestItSolutions"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/styleLib.css"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/googleBtn.css"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/paginator.css"/>
        <!--[if ie]><link rel="stylesheet" type="text/css" href="/static/admin/css/styleIe.css"/><![endif]-->        
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<!--		<script src="/vendor/jquery/jquery.min.js"></script>-->
        <!-- additional css & js here -->

        <script type="text/javascript" src="/static/admin/js/main.js"></script>        
        <!--        custom select-->
        <link rel="stylesheet" href="/vendor/jquery/customselect/styles.css" type="text/css"/>
        <script type="text/javascript" src="/vendor/jquery/customselect/jquery.customselect.js"></script>

        <script type="text/javascript" src="/vendor/jquery/jquery.paginator.js"></script>

        <!--        custom select-->
        <!--        <script type="text/javascript" src="/static/admin/js/clock.js"></script>-->
<!--        <script type="text/javascript" src="/vendor/jquery/jquery.address-1.3.min.js"></script>-->
        <script type="text/javascript" src="/static/admin/js/lang/<?=I18n::$lang?>.js"></script>
        <script type="text/javascript" src="/vendor/jquery/datatable/jquery.dataTables.min.js"></script>

<!--        date input -->
        <script type="text/javascript" src="/vendor/jquery/jquery.date_input.min.js"></script>
        <script type="text/javascript" src="/static/admin/js/dateinput_<?=I18n::$lang?>.js"></script>
        <link rel="stylesheet" href="/static/admin/css/jquery.date_input.css" type="text/css" />
<!--        date input -->

    </head>
    <body>
        <div class="container">
            <div class="wrap rel">
                <div class="precontent"></div>
                <!--            end header-->
                <!-- main part-->
                <!--help-->
                <div id="addBox" class="rel showHelpBox <?if(!$show_module_desc):?>hidden<?endif;?>">
					<div class="blockhelp"><?echo I18n::get('Help')?></div>
					<div class="dottedBottomBorder2"></div>
					<a href="javascript:void(0);" class="absBlocks close tItalic"><?echo I18n::get('Close')?></a>
                    <div class="module_descr_short"><div class="dashedbackgr"><?=$module['descr_short']?></div><div class="dottedBottomBorder2"></div><a href="#" class="showDescrFull"><?echo I18n::get('Details')?></a></div>
                    <div class="module_descr_full hidden"><div class="dashedbackgr"><?=$module['descr_short']?></div><br /><?=$module['descr_full']?><div class="dottedBottomBorder2"></div><a href="#" class="showDescrShort"><?echo I18n::get('Show short')?></a></div>
                </div>
                <? if ( Session::instance()->get('have_default_password') ): ?>
                <div class="rel showHelpBox errorHelpBox">
					<? echo I18n::get('have_default_password'); ?>
				</div>
				<? endif; ?>
                <!--help-->
                
                <div class="mainContent">
                    
