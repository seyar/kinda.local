<script type="text/javascript" src="/vendor/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/vendor/ckeditor/adapters/jquery.js"></script>

<script type="text/javascript">
		jQuery( function($) 
		{
			// $('select').addClass('uncustomized');
		});
		
		var config;
        jQuery(document).ready(function()
        {
                config = {
                        toolbar:
                        [
                            ['Styles','Format','Font','FontSize'],
                            ['TextColor','BGColor'],
                            ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
                            ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
                            '/',
                            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
                            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
                            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                            ['Link','Unlink','Anchor'],
                            ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],
                            ['Templates','Maximize','ShowBlocks','-','Source']
                        ]
                        , defaultLanguage: '<?php echo I18n::$lang;?>'
                        , language: '<?php echo I18n::$lang;?>'
                        , filebrowserBrowseUrl : '/vendor/elfinder/elfinder.html'
        
                };

            <?if( isset($textareaID) && is_array($textareaID) ):?>
                <? foreach($textareaID as $key => $item):?>
                    var ckeditor_<?= $key?> = CKEDITOR.replace("<?= $item?>", config);
                <?endforeach;?>
            <?elseif( isset($textareaID) && !empty($textareaID) && !is_array($textareaID) ):?>
                var ckeditor = CKEDITOR.replace("<?= $textareaID?>", config);
            <?else:?>                
                var ckeditor = CKEDITOR.replace("ckeditor", config);
            <?endif;?>
            
            
        });
</script>
<? unset($textareaID);?>
<!-- [wysiwyg] -->
