<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 18:03:13 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_domains.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div id="DomainsTab">
    <input type="hidden" name="dom_id" value="0"/>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <th style="width: 2%;">&nbsp;</th>
            <th style="width: 24%;"><?php echo I18n::get('Domain') ?></th>
            <th style="width: 24%;"><?php echo I18n::get('Language') ?></th>
            <th style="width: 24%;"><?php echo I18n::get('Title') ?></th>
            <th style="width: 16%;"><?php echo I18n::get('Template') ?></th>
            <th style="width: 10%;"><?php echo I18n::get('Action') ?></th>
        </tr>
    </table>



    <div class="padding20px">
        <br />
        <div>
            <div class="flLeft" style="width: 25%;"><input style="width: 90%;" type="text" name="dom_http_host" value="" ></div>
            <div class="flLeft" style="width: 25%;">
<? echo Model_Content::customSelect('dom_language', $languages, NULL, NULL, 'id', 'name'); ?>
            </div>
            <div class="flLeft" style="width: 25%;text-align: center;"><input style="width: 90%;" type="text" name="dom_title" value="" maxlength="255"></div>
            <div class="flLeft" style="width: 25%;">
<? echo Model_Content::customSelect('dom_template', $templates, NULL, NULL, NULL, 'name'); ?>
            </div>
            <div class="clear"></div>
        </div>

        <br />
        <div class="flLeft" style="width: 50%;">
			<div class="greytext blocktitles"><?php echo I18n::get('Keywords') ?>: </div>
            <textarea name="dom_keywords" rows="4" cols="5" style="width:98%"></textarea></div>
        <div class="flRight" style="width: 50%;">
            <div style="padding: 0 0 0 10px;">
				<div class="greytext blocktitles"><?php echo I18n::get('Description') ?>: </div>
                <textarea name="dom_description" rows="4" cols="5" style="width:98%"></textarea></div>
        </div>


        <div class="clear"></div>
        <br />
        <button class="btn" type="button" onclick="return DomainSave();"><span><span><?= I18n::get('Save') ?></span></span></button>&nbsp;
        <button class="btn" type="button" onclick="return DomainCancel();"><span><span><?= I18n::get('Cancel') ?></span></span></button>
        <br />
        <br />
    </div>
</div>
