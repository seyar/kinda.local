<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 18:04:06 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_sitestop.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div class="padding20px">
    <div class="dottedBottomBorder"></div>
    <table style="margin: 1em auto;" cellspacing="5" width="100%" class="borderNone">
      <tr><td class="checklabel" style="width:25%">
              <label><input type="checkbox" name="site_stop" value="1" class="vMiddle"
                     <? if($obj['site_stop'] || $_POST['site_stop']):?> checked="checked" <?endif;?> />
					 <span class="greytext"><?php echo I18n::get('Stop site now')?></span>
					 </label>
          </td>

            <td style="width: 25%;text-align:right; vertical-align:top;padding-left: 10px;" class="dottedLeftBorder">
			<span class="greytext"><?php echo I18n::get('Site stop message')?>: </span></td>
            <td>
                <textarea name="site_stop_message" rows="5" cols="50" style="width:100%;"><?=(isset($obj['site_stop_message'])?$obj['site_stop_message']:$_POST['site_stop_message'])?></textarea>
            </td>
      </tr>
    </table>
    <div class="dottedBottomBorder"></div>
    <br/>
    <br/>
</div>