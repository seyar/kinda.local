<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 19.11.2010 - 10:32:48 Exp $
 *
 * Project:     Chimera2.local
 * File:        layout_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>

<!--floating block-->
<form method="post" action="">
	<input type="hidden" name="list_action" id="list_action" value="<?= $admin_path?><?= $controller?>delete" />
<div class="rel whiteBg floatingOuter" id="contentNavBtns">
    <div class="whiteblueBg absBlocks floatingInner">
        <div class="padding20px">
            <table width="100%">
                <tr>
                    <td style="width:48%"><button class="btn blue" type="button" onclick="document.location.href='/admin/pages/edit';">
                            <span><span><?= I18n::get('Create') ?></span></span></button></td>
                    <td class="vMiddle"><?= I18n::get('With marked') ?> </td>
                    <td style="padding-top:1px"><select name="sel0" onchange="$('#list_action').val(this.value);"><option selected="selected"><?= I18n::get('Delete') ?></option></select></td>
                    <td><button class="btn formUpdate" type="button">
                            <span><span><?= I18n::get('Apply') ?></span></span></button>
                    </td>
                    <td class="vMiddle"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>
                    <td>
                        <select name="sel1">
                            <option>10 <?= I18n::get('lines') ?></option>
                            <option>20 <?= I18n::get('lines') ?></option>
                            <option>30 <?= I18n::get('lines') ?></option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

<!--floating block-->

<div class="rel">
    <div class="whiteBg">
        <!--                        content module-->
        <table width="100%" cellpadding="0" cellspacing="0" class="sortableContentTab">
            <thead>
                <tr>
                    <th style="width:5%;padding-left:0;" class="tCenter"><input type="checkbox" name="" value=""/></th>
                    <th style="padding-left:0px;"><?= I18n::get('Templates Styles') ?></th>
                    <th style="width:5%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?foreach($rows as $key=>$value):?>
                <tr class="<? if($key % 2 == 0): ?>even<?else:?>odd<?endif;?>">
                    <td class="tCenter"><input type="checkbox" name="chk[<?=$key?>]" value="<?=$value?>"/></td>
                    <td><a href="<?=$admin_path.$controller?><?=$key?>/edit" ><?=$value?></a></td>
                    <td><a href="<?=$admin_path.$controller?><?=$key?>/copy" ><img src="/static/admin/images/cop.gif" alt="copy" title="<?echo I18n::get('Copy');?>"/></a></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>
</form>
