<?php
defined('SYSPATH') or die('No direct script access.');
/**
 *  @version $Id: v 0.1 12.11.2010 - 12:11:22 Exp $
 *
 * Project:     Chimera2.local
 * File:        index.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<!--floating block-->
<!--    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <table width="100%">
                    <tr>
                        <td class="vMiddle">C отмеченными </td>
                        <td><select name="sel0"><option>Удалить</option></select></td>
                        <td style="width:58%"><button class="btn formUpdate" type="button">
                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>
                        <td class="vMiddle"><div class="dottedLeftBorder">На стрaнице </div></td>
                        <td>
                            <select name="sel1">
                                <option>10 строк</option>
                                <option>20 строк</option>
                                <option>30 строк</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>-->

<!--floating block-->
<div class="rel">
    <div class="whiteBg padding20px">



        <!-- ********************** content START HERE module ********************* -->

        <br />
        <br />

        <div class="widgetsContainer">
            <?php if (!isset($selectable)): ?>
            <?php $selectable = false ?>

                <div id="cmsInfo"<?php if (isset($widgets) && count($widgets) !== 0): ?> style="display:none;" <? endif; ?>>
                         <!--<h3>Добро пожаловать в административную панель системы управления Chimera CMS!</h3>-->
                         <!--<p style="padding-top: 10px;">Система управления Chimera CMS разработана таким образом,
                         что работать с ней удобно даже неопытному пользователю ПК.
                         Основные преимущества в том, что Chimera представляет собой
                         легкозапоминаемую систему с качественным и оригинальным дизайном, и,
                         что главное, позволяет редактировать и обновлять содержимое сайта, не прибегая к помощи дизайнеров,
                         веб-мастеров или программистов.
                         </p>-->
                 <?php echo I18n::get('mainwelcome'); ?>
           </div>
            <?php endif; ?>

            <?php if (isset($widgets) && count($widgets) !== 0): ?>
            <?php foreach ($widgets as $key => $widget): ?>
            <? /* widget=$widget selectable=$selectable widgetId=$widgetId */ ?>
            <?php include 'widget.tpl'; ?>
            <?php endforeach; ?>
            <?php endif; ?>

                                <div class="clear"></div>

            <?php if (!$selectable): ?>
                                    <div class="flRight">
                                        <a href="/admin/widgetsmanager"><?php echo I18n::get('Customize this page') ?></a>
                                    </div>
                                    <div class="clear"></div>
            <?php endif; ?>
            <br />
            <br />
        </div>
        <br />
        <br />

        <!-- *************** /content END HERE ***************** -->

        </div>
                <div class="absBlocks side L"></div>
                <div class="absBlocks side R"></div>
                <div class="absBlocks corner L"></div>
                <div class="absBlocks corner R"></div>
            </div>