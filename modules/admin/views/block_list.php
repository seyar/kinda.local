<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>

<form method="post" action="">
<!--floating block-->
    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <table width="100%">
                    <tr>
                        <td><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>edit'">
                                <span><span><?echo I18n::get('Create');?></span></span></button></td>
                        <td class="vMiddle"><?echo I18n::get('With marked');?></td>
                        <td style="padding-top:1px;"><select id="list_action"><option value="delete"><?echo I18n::get('Delete');?></option></select></td>
                        <td style="width:46%"><button class="btn formUpdate" type="button">
                                <span><span><?echo I18n::get('Apply');?></span></span></button>
                        </td>
                        <td class="vMiddle"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>
                        <td><?=Form::select('rows_per_page', array(
                                '10' => '10 ' . I18n::get('lines'),
                                '20' => '20 ' . I18n::get('lines'),
                                '30' => '30 ' . I18n::get('lines'),
                        ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange'=>"formfilter(jQuery('#rows_per_page'))"))?>
                        </td>
                    </tr>
                </table>
            </div>
<!--            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>-->
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel whiteBg">
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>
            <!-- **** -->
                <table width="100%" cellpadding="0" cellspacing="0" class="sortableContentTab">
                    <thead>
                        <tr>
                            <th style="width:5%;padding-left:0;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll" /></th>
                            <th style="width:25%; padding-left: 10px;" class="tLeft <?=Admin::table_sort_header('name')?>" ><?= I18n::get('Name') ?></th>
                            <th style="width:65%; padding-left: 10px;" class="tLeft <?=Admin::table_sort_header('description')?>"><?= I18n::get('Description') ?></th>
                            <th style="width:5%;">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?if($rows):?>
                        <?foreach($rows as $key=>$item):?>
                        <tr class="<?if($key % 2 == 0):?>even<?else:?>odd<?endif;?>">
                            <td class="tCenter"><input type="checkbox" name="chk[<?=$item['id']?>]" value="<?=$item['id']?>"  class="listCheckbox checkBox" /></td>
                            <td><a href="<?=$admin_path.$controller.$item['id']?>/edit"><?=$item['name']?></a></td>
                            <td><?= strip_tags(Text::limit_words($item['description'],7));?></td>
                            <td><a href="<?=$admin_path.$controller.$item['id']?>/copy"><img src="/static/admin/images/cop.gif" alt="copy" title="<?=I18n::get('Copy')?>"/></a></td>
                        </tr>
                        <?endforeach;?>
                        <?endif;?>
                    </tbody>
                </table>
            <? if ($pagination): ?>
            <?=$pagination?>
            <? endif; ?>
            <!-- **** -->

        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
