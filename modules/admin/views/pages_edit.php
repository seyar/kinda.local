<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 15.11.2010 - 14:41:51 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<?
/**
 * @param String    $textareaID may be defined. ID or name Textarea.
 */
include 'system/wysiwyg.php';
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/pages.js"></script>

<!--floating block-->
<form method="post" action="">
<div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
    <input type="hidden" name="actTab" id="actTab" value="main" />
    <div class="absBlocks floatingInner">
        <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td><button class="btn blue formSave" type="button" rel="<?= $admin_path . $controller . (isset($id) ? $id : 0) ?>/save">
                                <span><span><?= I18n::get('Save') ?></span></span></button></td>
                        <td style="width:35%">
                            <? if ($obj['id']): ?>
                                <button class="btn blue formUpdate" type="button" rel="<?= $admin_path . $controller . (isset($id) ? $id : 0) ?>/update">
                                    <span><span><?= I18n::get('Apply') ?></span></span></button>
                            <? else: ?>
                                    <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller . (isset($id) ? $id : 0) ?>/apply">
                                        <span><span><?= I18n::get('Apply') ?></span></span></button>
                            <? endif; ?>
                                </td>

                                <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                        <span><span><?= I18n::get('Cancel') ?></span></span></button>
                                </td>
                            </tr>
                        </table>
                </div>
                <div class="absBlocks floatingPlaCorner L"></div>
                <div class="absBlocks floatingPlaCorner R"></div>
            </div>
            <div class="absBlocks side L"></div>
            <div class="absBlocks side R"></div>
        </div>

        <!--floating block-->

        <div class="rel" >

            <div class="whiteblueBg padding20px">
                <ul class="tabs">
                    <li class="active" id="main"><a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C font14px"><?= I18n::get('Main') ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="options">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C font14px"><?= I18n::get('Options'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <? if (isset($id)):?>
                    <li class="divider">&nbsp;</li>
                    <li class=""  id="blocks">
                        <a href="#">
                            <span class="linkSide L"></span>
                            <span class="linkSide C"><?= I18n::get('Blocks'); ?></span>
                            <span class="linkSide R"></span>
                        </a>
                    </li>
                    <? endif;?>
                </ul>
                <div class="clear"></div>
                <br />
                <div class="dottedBottomBorder"></div>
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>

        <!-- **** -->
        <table class="borderNone italic grayText2" width="100%">
            <tr>
                <td style="width: 25%;"><?= I18n::get('Page title') ?> (title):</td>
                <td style="width: 25%;"><?= I18n::get('The name of the pages in the menu') ?>:</td>
                <td><?= I18n::get('Type'); ?>:</td>
                <td style="width: 32%;"><?= I18n::get('Path to the page'); ?>:</td>
            </tr>
            <tr>
                <td><input class="page_edit_input" type="text" name="page_title" value="<?= (isset($obj['page_title']) ? $obj['page_title'] : $_POST['page_title']) ?>" /></td>
                <td><input class="page_edit_input" type="text" name="page_name" value="<?= (isset($obj['page_name']) ? $obj['page_name'] : $_POST['page_name']) ?>" /></td>
                <td>

        <? if ($page_types): ?>
		
                    <? echo Model_Content::customSelect('pagetypes_id',$page_types,(isset($obj['pagetypes_id'])?$obj['pagetypes_id']:$_POST['pagetypes_id']),array('onchange'=>'if( this.value == 3) showAddSelect();else{ copyPageFrom(this.value);hideAddSelect();}'),'id','name' );?>

        <? endif; ?>
                </td>
                <td style="padding-right:4px;">

                    <input type="hidden" name="parent_url" value="<?= (isset($obj['parent_url']) ? $obj['parent_url'] : $_POST['parent_url']) ?>" />


            <? $page_url = (isset($obj['page_url']) ? $obj['page_url'] : $_POST['page_url']) ?>

                    <span class="hidden">
                        {if $all_pages}
                        {select name=copy_from onchange="copyPageFrom(this.value);"}
                        {foreach from=$all_pages item=item key=key}
                        {assign var='opttext' value="`$item['parent_url']|rtrim:'/'`/`$item['page_url']`"}
                        {option value=$item.id text=$opttext}
                        {/foreach}
                        {/select}
                        {/}
                    </span>

                    <div class="input" style="width:98%; height:19px">
                        <div id="sample2" class="flLeft" style="width: 48%;">
                        <? if ($site_folders): ?>
                            <? echo Model_Content::customSelect('parent_id',$site_folders,(isset($obj['parent_id'])?$obj['parent_id']:$_POST['parent_id']),array('onchange'=>'changeParent()'),NULL,'name' );?>
                        <? endif; ?>
                        </div>
                        | <input type="text" name="page_url" value="<?= isset($obj['page_url'])?$obj['page_url']:str_replace($page_url, $obj['parent_url']) ?>" style="width:46%;"/>
                        <div class="clear"></div>
                    </div>
                </td>
            </tr>
        </table>
        <!-- **** -->

        <div id="pageEditMain" class="tabContent main openedTab"><? include 'page_edit_main.php'; ?></div>
        <div id="pageEditOptions" class="tabContent options "><? include 'page_edit_options.php'; ?></div>
        <div id="pageEditBlocks" class="tabContent blocks "><? include 'page_edit_blocks.php'; ?></div>

    </div>
<div class="absBlocks side L"></div>
<div class="absBlocks side R"></div>
<div class="absBlocks corner L"></div>
<div class="absBlocks corner R"></div>
</div>
</form>
