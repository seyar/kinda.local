<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 18:03:43 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_cache.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>

<div class="padding20px">
<div class="dottedBottomBorder"></div>
<table style="" cellspacing="5" class="borderNone italic grayText2" width="100%">
    <tr>
        <td style="width:30%;">
			<div class="blocktitles"><?php echo I18n::get('Cache lifetime (sec)')?>:</div>
            <input type="text" name="cache_lifetime" value="<?=(isset($obj['cache_lifetime'])?$obj['cache_lifetime']:$_POST['cache_lifetime'])?>" style="width: 60px" />
        </td>
        <td class="dottedLeftBorder" style="padding-left:20px; width: 20%;"><?php echo I18n::get('Cache next objects')?>:</td>
        <td class="checklabel">
            <ul class="ulToCols">

                <?foreach(array(1=>'Images', 2=>'JavaScript', 3=>'CSS', 4=>'HTML', 5=>'SQL') as $key => $item):?>
                    <li><label><input type="checkbox" name="cache_objects[]" value="<?=$item?>" <?if($obj['cached_objects'][$item] == TRUE):?>checked="true"<?endif;?>/><?=$item?></label></li>
                <?endforeach;?>
            </ul>

            <div class="clear"></div>
<!--            {select name="cache_objects[]" style="width: 202px;" multiple="true" size="5" value=$obj.cache_objects|default:$quicky.post['cache_objects']}
            {foreach from=array(1=>'Images', 2=>'JavaScript', 3=>'CSS', 4=>'HTML', 5=>'SQL') value="value" key="key"}
            {if $obj.cached_objects[$value]}{option value=$value text=$value selected=true}{else}{option value=$value text=$value}{/}
            {/foreach}
            {/select}-->
        </td>
    </tr>

</table>
<div class="dottedBottomBorder"></div>
<br />
<button class="btn" type="button" onclick="document.location.href='<?=$admin_path.$controller?>clearcache'"><span><span><?php echo I18n::get('Clear Cache')?></span></span></button>
<br />
<br />
</div>