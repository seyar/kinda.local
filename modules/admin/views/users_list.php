<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 19.11.2010 - 11:57:07 Exp $
 *
 * Project:     Chimera2.local
 * File:        users_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<!--floating block-->
<form method="post" action="">
    <input type="hidden" name="list_action" value="<?= $admin_path?><?= $controller?>delete" id="list_action"/>
<div class="rel whiteBg floatingOuter" id="contentNavBtns">
    <div class="whiteblueBg absBlocks floatingInner">
        <div class="padding20px">
            <table width="100%">
                <tr>
                    <td style="width:46%"><button class="btn blue" type="button" onclick="document.location.href='<?= $admin_path?><?= $controller?>edit';">
                                <span><span><?= I18n::get('Create') ?></span></span></button></td>
                    <td class="vMiddle"><?= I18n::get('With marked') ?> </td>
                    <td style="padding-top:1px;"><div style=" width: 100px;">
<select name="sel0" onchange="$('#list_action').val( $('[name=sel0]').val() );">
                            <option value="<?= $admin_path?><?= $controller?>delete" selected="selected"><?echo I18n::get('Delete')?></option>
<!--                            <option value="<?= $admin_path?><?= $controller?>copy"><?echo I18n::get('Copy')?></option>-->
                        </select></div>
</td>
                    <td><button class="btn formUpdate" type="button">
                            <span><span><?= I18n::get('Apply') ?></span></span></button>
                    </td>
                    <td class="vMiddle"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>
                    <td>
                        <select name="sel1">
                            <option>10 <?= I18n::get('lines') ?></option>
                            <option>20 <?= I18n::get('lines') ?></option>
                            <option>30 <?= I18n::get('lines') ?></option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

<!--floating block-->

<div class="rel">
    <div class="whiteBg">
        <!--                        content module-->
        <table width="100%" cellpadding="0" cellspacing="0" class="sortableContentTab tabpad0">
            <thead>
                <tr>
                    <th style="width:5%;padding-left:0;" class="tCenter"><input type="checkbox" name="" value=""/></th>
                    <th style="width:17%"><?= I18n::get('Login')?></th>
                    <th ><?= I18n::get('Role')?></th>
                    <th ><?= I18n::get('Full Name')?></th>
                    <th ><?= I18n::get('Email')?></th>
                    <th ><?= I18n::get('Logins')?></th>
                    <th style="width:75px; text-align:center;"><?=I18n::get('Action')?></th>
                </tr>
            </thead>
            <tbody>
                <?foreach($rows as $key=>$value):?>
                <tr class="<?if($key % 2 == 0):?>even<?else:?>odd<?endif;?>">
                    <td class="tCenter"><input type="checkbox" name="chk[<?=$key?>]" value="<?=$value?>" class="listCheckbox"/></td>
                    <td><a href="<?=$admin_path.$controller?><?=$key?>/edit"><?=$value['username']?></a></td>
                    <td><?=implode(',',$value['roles'])?></td>
                    <td><?=$value['fullname']?></td>
                    <td><?=$value['email']?></td>
                    <td><?=$value['logins']?></td>
                    <td class="tCenter">
                        <a href="<?=$admin_path.$controller?><?=$key?>/active" ><img src="/static/admin/images/act_<?if($value['status'] == 1):?>green<?else:?>red<?endif;?>.png" alt="copy" title="<?echo I18n::get('Active');?>"/></a>
                    </td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>
</form>