<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 16.11.2010 - 17:35:13 Exp $
 *
 * Project:     Chimera2.local
 * File:        page_edit_main.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 */
?>
<table class="borderNone italic grayText2" width="100%">
    <tr>
        <td><?= I18n::get('Name') ?></td>
    <tr>
        <td >
            <input type="text" value="<? echo (isset($block['page_block_name']) ? $block['page_block_name'] : $_POST['page_block_name']) ?>" name="page_block_name"/>
        </td>
    </tr>
    <tr>
        <td><?= I18n::get('Content') ?></td>
    </tr>
    <tr>
        <td >
            <textarea name="page_block_text" rows="5" cols="50" id="ckeditorBlock"><? echo (isset($block['page_block_text']) ? $block['page_block_text'] : $_POST['page_block_text']) ?></textarea>
        </td>
    </tr>
</table>
<div class="clear"></div>
<button onclick="return pageBlockSave();" type="button" class="btn"><span><span><?= I18n::get('Save') ?></span></span></button>
<button onclick="return pageBlockCancel();" type="button" class="btn"><span><span><?= I18n::get('Cancel') ?></span></span></button>
<div class="clear"></div>
<p><br></p>
<script type="text/javascript">

    var ckeditor = CKEDITOR.replace("ckeditorBlock", config);

    function pageBlockCancel() {
        if (CKEDITOR.instances.ckeditorBlock)
            CKEDITOR.instances.ckeditorBlock.destroy();

        $("#pageBlocksEdit").hide().html('');
        $("#pageBlocksList").show();

        return false;
    }

    function pageBlockSave() {
        if (CKEDITOR.instances.ckeditorBlock)
            CKEDITOR.instances.ckeditorBlock.destroy();

        $.post('<?= $admin_path . $controller ?><?= $block['id'] ?>/save_page_block', {
            'page_id':<?=$block['page_id']?>,
            'page_block_name': $('#pageBlocksEdit input[name=page_block_name]').val (),
            'page_block_text': $('#pageBlocksEdit textarea[name=page_block_text]').val ()
        }, function (data){
            if (data=='1')
            {

                $('#pageBlockName_<?= $block['id'] ?>').text($('#pageBlocksEdit input[name=page_block_name]').val ());

                $("#pageBlocksEdit").hide();
                $("#pageBlocksList").show();

                if ('<?= $block['id'] ?>'=='0') { document.location.reload(); return false;}
            }
            else
                ckeditor = CKEDITOR.replace("ckeditorBlock", config);
        });

        return false;
    }
</script>
