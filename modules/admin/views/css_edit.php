<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 19.11.2010 - 11:14:57 Exp $
 *
 * Project:     Chimera2.local
 * File:        layout_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script src="/vendor/codepress/codepress.js" type="text/javascript"></script>
<form method="post" action="">
    <!--floating block-->
    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner layoutheader">
            <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td><button class="btn blue formSave" type="button" rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                <span><span><?= I18n::get('Save') ?></span></span></button></td>
                        <td style="width:37%">
                            <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">
                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?= $admin_path . $controller ?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->
    <div class="rel">
        <div class="whiteblueBg italic grayText2">
            <? include MODPATH . ADMIN_PATH . '/views/system/messages.php'; ?>
            <div class="innerGray" style="padding:10px 25px">
                <div class="blocktitles"><? echo I18n::get('Filename') ?>: </div>
                <input type="text" name="css_filename" value="<?= (isset($obj['filename']) ? $obj['filename'] : $_POST['layout_filename']) ?>"
                       style="width: 18em;" <? if ($obj['filename']): ?> readonly="readonly"<? endif; ?>/>
                <br>
                <br><!--class="codepress css linenumbers-off"-->
				<div class="blocktitles"><? echo I18n::get('Content') ?>: </div>
                <textarea name="content" rows="25" cols="55" id="css_content" style="width: 100%;"
                          ><?= htmlentities((isset($obj['filename']) ? $obj['content'] : $_POST['content']), ENT_COMPAT, 'UTF-8'); ?></textarea>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>