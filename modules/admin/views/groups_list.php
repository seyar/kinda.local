<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 19.11.2010 - 15:38:31 Exp $
 *
 * Project:     Chimera2.local
 * File:        groups_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<form method="post" action="">
<!--floating block-->
<div class="rel whiteBg floatingOuter" id="contentNavBtns">
    <div class="whiteblueBg absBlocks floatingInner">
        <div class="padding20px">
            <table width="100%">
                <tr>
                    <td style="width:46%"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path?><?=$controller?>edit';">
                                <span><span><?= I18n::get('Create') ?></span></span></button></td>
                    <td class="vMiddle"><?= I18n::get('With marked') ?> </td>
                    <td style="padding-top:1px;">
					<div style=" width: 100px;">
                        <select name="list_action" id="list_action">
                            <option value="<?=$admin_path?><?=$controller?>delete" selected="selected"><?echo I18n::get('Delete')?></option>
<!--                            <option><?echo I18n::get('Copy')?></option>-->
                        </select></div></td>
                    <td><button class="btn formUpdate" type="button">
                            <span><span><?= I18n::get('Apply') ?></span></span></button>
                    </td>
                    <td class="vMiddle"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>
                    <td>
                        <select name="sel1">
                            <option>10 строк</option>
                            <option>20 строк</option>
                            <option>30 строк</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
</div>

<!--floating block-->

<div class="rel">
    <div class="whiteBg">
        <!--                        content module-->
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>
        <table width="100%" cellpadding="0" cellspacing="0" class="sortableContentTab tabpad0">
            <thead>
                <tr>
                    <th style="width:5%;padding-left:0;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll"/></th>
                    <th width="30%"><? echo I18n::get('Group Name')?></th>
                    <th width=""><? echo I18n::get('Description')?></th>
                    <th style="width:75px; text-align:center;"><?=I18n::get('Action')?></th>
                </tr>
            </thead>
            <tbody>
                <?foreach($rows as $key=>$value):?>
                <tr class="<?if($key % 2 == 0):?>even<?else:?>odd<?endif;?>">
                    <td class="tCenter"><input type="checkbox" name="chk[<?=$key?>]" value="<?=$value?>" class="listCheckbox"/></td>
                    <td><a href="<?=$admin_path.$controller?><?=$key?>/edit"><?=$value['name']?></a></td>
                    <td><?=$value['description']?></td>

                    <td class="tCenter">
                        <a href="<?=$admin_path.$controller?><?=$key?>/active" ><img src="/static/admin/images/act_<?if($value['status'] == 1):?>green<?else:?>red<?endif;?>.png" alt="copy" title="<?echo I18n::get('Active');?>"/></a>
                    </td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>
</form>