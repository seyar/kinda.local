<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 12.11.2010 - 16:55:31 Exp $
 *
 * Project:     Chimera2.local
 * File:        pages.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8">jQuery.noConflict();</script>
<script type="text/javascript" charset="utf-8" src="/vendor/wcm/prototype.js"></script>
<script type="text/javascript" charset="utf-8" src="/vendor/wcm/effects.js"></script>
<script type="text/javascript" charset="utf-8" src="/vendor/wcm/dragdrop.js"></script>
<script type="text/javascript" charset="utf-8" src="/vendor/wcm/wolf.js"></script>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>

<!--floating block-->
<form method="post" action="">
    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <table width="100%">
                    <tr>
                        <td><button class="btn blue" type="button" onclick="document.location.href='/admin/pages/edit';">
                                <span><span><?= I18n::get('Create') ?></span></span></button></td>
                        <td style="width:50%">
                            <a href="#" id="toggle_reorder" onclick="toggle_reorder = !toggle_reorder; toggle_copy = false; $$('.handle_reorder').each(function(e) { e.style.display = toggle_reorder ? 'inline': 'none'; }); $$('.handle_copy').each(function(e) { e.style.display = toggle_copy ? 'inline': 'none'; }); return false;"><?= I18n::get('reorder') ?></a>,
                            <a id="toggle_copy" href="#" onclick="toggle_copy = !toggle_copy; toggle_reorder = false; $$('.handle_copy').each(function(e) { e.style.display = toggle_copy ? 'inline': 'none'; }); $$('.handle_reorder').each(function(e) { e.style.display = toggle_reorder ? 'inline': 'none'; }); return false;"><?= I18n::get('copy') ?></a>
                        </td>
                        <td style="width: 12%;" class="vMiddle tRight"><?= I18n::get('With selected') ?></td>
                        <td style="padding-top:1px"><select id="list_action"><option value="delete"><?= I18n::get('Delete') ?></option></select></td>
                        <td><button class="btn formUpdate" type="button">
                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

<!--floating block-->
<div class="rel">
    <div class="whiteBg">

        <!-- ********************** content START HERR module ********************* -->
        <table width="100%" cellpadding="0" cellspacing="0" class="dottedBottomBorder">
            <tr>
                <th style="width:5%;" class="tCenter"><input type="checkbox" name="" value="" class="listCheckboxAll"/></th>
                <th style="width:530px; padding-left:0px;"><?=I18n::get('Page name')?></th>
                <th><?=I18n::get('Page URL')?></th>
                <th style="width:93px;"><?=I18n::get('Action')?></th>
            </tr>
        </table>
        <ul class="pages" id="site-map">
        <?foreach($rows as $key => $item):?>
            <li id="page_<?=$item['id']?>" class="node level-<?echo count(explode('/',$item['parent_url']))-1;?> <?if($item['pagetypes_id'] == 2):?>children-hidden<?else:?>no-children<?endif;?> <?if($key % 2 == 0):?>even<?endif;?>">
                <div class="page">
                    <input type="checkbox" name="chk[<?=$item['id']?>]" value="<?=$item['id']?>" class="absBlocks checkBox listCheckbox"/>
                    <a href="/admin/pages/<?=$item['id']?>/edit" class="pageTitle"><img src="/static/admin/images/ico_<?if($item['is_default']==1 || $item['pagetypes_id'] == 2):?>main<?else:?>content<?endif;?>page.gif" alt="" class="vMiddle"/> <?=$item['page_title']?></a>
                    <a href="/admin/pages/<?=$item['id']?>/follow" class="absBlocks pageUrl"><?=$item['page_url']?></a>

                    <img alt="Drag and Drop" src="/static/admin/images/drag_to_sort.gif" class="handle_reorder" />
                    <img alt="Drag to Copy" src="/static/admin/images/drag_to_copy.gif" class="handle_copy" />
                    <img title="" style="display: none;" src="/static/admin/images/spinner.gif" id="busy-3" class="busy" alt="" />

                    <span class="absBlocks pageicons">
                        <a href="/admin/pages/<?=$item['id']?>/active" class="ajaxSwitchColumn">
                            <img src="/static/admin/images/act_green.png" alt="active" <? if($item['status']!=1):?>class="hidden"<?endif;?>/>
                            <img src="/static/admin/images/act_red.png" alt="inactive"  <? if($item['status']==1):?>class="hidden"<?endif;?>/>
                        </a>
                        <a href="/admin/pages/<?=$item['id']?>/delete" class=""><img src="/static/admin/images/del.gif" alt="settings"/></a>
                    </span>
                </div>
                <?if($item['pagetypes_id'] == 2):?>
                    <img align="middle" title="" src="/static/admin/images/expand.png" class="absBlocks expander" alt="toggle children">
                <?endif;?>

            </li>
        <?endforeach;?>
        </ul>
        <br />
        <!-- *************** /content END HERE ***************** -->

    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>
</form>
