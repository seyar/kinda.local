<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 19.11.2010 - 16:12:59 Exp $
 *
 * Project:     Chimera2.local
 * File:        groups_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<form method="post" action="">
<!--floating block-->
    <div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td><button class="btn blue formSave" type="button" rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                <span><span><?= I18n::get('Save') ?></span></span></button></td>
                        <td style="width:37%">
                    <? if ($obj['id']): ?>
                        <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">
                            <span><span><?= I18n::get('Apply') ?></span></span></button>
                    <? else: ?>
                            <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/apply">
                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                    <? endif; ?>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

<!--floating block-->
<div class="rel">
    <div class="whiteblueBg padding20px">
        <br />
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>
        <div class="flLeft dottedRightBorder" style="width:45%;">
            
        <table cellspacing="5" class="italic borderNone grayText2">
              <tr>
                  <td>
				  <div class="blocktitles"><?php echo I18n::get('Name');?>: </div>
			      <input type="text" name="name" value="<?=(isset($obj['name'])?$obj['name']:$_POST['name'])?>" style="width:406px;"/></td>
			  </tr>
              <tr>
                  <td valign="top">
				  <div class="blocktitles"><?php echo I18n::get('Description');?>: </div>
                  <textarea name="description" rows="5" cols="40" style="width:406px;"><?=(isset($obj['description'])?$obj['description']:$_POST['description'])?></textarea></td>
			  </tr>
			  <tr>
                  <td><label><input class="vMiddle" type="checkbox" name="status" value="1" <?if($obj['status'] || !$obj['id'] || $_POST['status']):?> checked="checked"<?endif;?>/> <?php echo I18n::get('Group active')?></label></td>
			  </tr>
        </table>
        </div>
        <div class="flLeft italic grayText2" style="width:45%; padding: 0 0 0 40px;">
        </div>
        <div class="clear"></div>
        <br />
        <br />
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>
</form>