<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 19.11.2010 - 14:26:27 Exp $
 *
 * Project:     Chimera2.local
 * File:        users_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<form method="post" action="">
<!--floating block-->
    <div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td><button class="btn blue formSave" type="button" rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                <span><span><?= I18n::get('Save') ?></span></span></button></td>
                        <td style="width:37%">
                    <? if ($obj['id']): ?>
                        <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">
                            <span><span><?= I18n::get('Apply') ?></span></span></button>
                    <? else: ?>
                            <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/apply">
                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                    <? endif; ?>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?=$admin_path.$controller?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

<!--floating block-->
<div class="rel">
    <div class="whiteblueBg padding20px">
        <br />
        <br />
        <div class="flLeft dottedRightBorder" style="width:45%;">
        <table cellspacing="5" class="italic borderNone grayText2">
          <tr><td class="tRight"><?php echo I18n::get('Login')?> </td><td rowspan="8">&nbsp;</td>
              <td><input type="text" name="username" value="<?=(isset($obj['username'])?$obj['username']:$_POST['username'])?>" style="width:200px;"/></td>
          </tr>
          <tr><td class="tRight"><?php echo I18n::get('New password')?></td>
              <td><input type="password" name="password" value="" style="width:200px;"/></td>
          </tr>
          <tr><td class="tRight"><?php echo I18n::get('New password (repeat)')?></td>
              <td><input type="password" name="password_confirm" value="" style="width:200px;"/></td>
          </tr>
          <tr><td class="tRight"><?php echo I18n::get('Email')?></td>
              <td><input type="text" name="email" value="<?=(isset($obj['email'])?$obj['email']:$_POST['email'])?>" style="width:200px;"/></td>
          </tr>
          <tr><td class="tRight"><?php echo I18n::get('Language')?></td>
              <td>
                  <?echo Form::select('language', array('ru'=>'Русский', 'en'=>'English'),(isset($obj['language'])?$obj['language']:$_POST['language']), NULL)?>
              </td>
          </tr>
          <tr><td class="tRight"><?php echo I18n::get('Name')?></td>
                  <td><input type="text" name="fullname" value="<?=(isset($obj['fullname'])?$obj['fullname']:$_POST['fullname'])?>" style="width:200px;"/></td>
              </tr>
        </table>
        </div>
        <div class="flLeft italic grayText2" style="width:45%; padding: 5px 0 0 40px;">
            <p class="tBold" style="color:#000;"><?php echo I18n::get('Roles')?></p><br />
              <?foreach($user_roles as $key => $value):?>
              <label><input class="vMiddle" type="checkbox" name="roles[]" value="<?=$value['id']?>" <?if($obj['roles'][$value['id']]):?>checked="true"<?endif;?>/> <?=$value['name']?></label>&nbsp;&nbsp;&nbsp;
              <?endforeach;?>
              <br />
              <br />
              <br />

              <p class="tBold" style="color:#000;"><?php echo I18n::get('User Status')?></p><br />
              <label><input  class="vMiddle" type="radio" name="status" value="1" <?if($obj['status'] || !$obj['id'] || $_POST['status']):?> checked="checked"<?endif;?>/>&nbsp;<?php echo I18n::get('Active')?></label>&nbsp;&nbsp;&nbsp;
                  <label><input type="radio"  class="vMiddle" name="status" value="0" <?if($obj['status'] || !$obj['id'] || $_POST['status']):?><?else:?> checked="checked"<?endif;?>/>&nbsp;<?php echo I18n::get('Noactive')?></label>
        </div>
        <div class="clear"></div>
        <br />
        <br />
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>
</form>