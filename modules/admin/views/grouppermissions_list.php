<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 19.11.2010 - 16:59:21 Exp $
 *
 * Project:     Chimera2.local
 * File:        grouppermissions_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript">
    $().ready(function()
    {

    });

    function changeRule(role, resource, action, checked)
    {
        url = '/admin/grouppermissions/aacl';
        $.post(url, {checked: checked, role:role, resource: resource, action: action});
    }
</script>
<div class="rel">
    <div class="whiteBg">
        <!--                        content module-->
        <table width="100%" cellpadding="0" cellspacing="0" class="tabpad0">
            <thead>
            <tr class="first">
                <th width="1%">&nbsp;</th>
                <th width=""><? echo I18n::get('Access Page') ?></th>
                <? foreach ($user_roles as $key => $value): ?>
                    <th class="tCenter"><?= $value['name'] ?></th>
                <? endforeach; ?>
                </tr>
            </thead>
            <tbody>
            <? foreach ($resources as $key => $resource): ?>
                    <tr class="<? if ($key % 2 == 0): ?>even<? else: ?>odd<? endif; ?>">
                                <td width="1%">&nbsp;</td>
                                <td><?= $resource['name'] ?></td>

                <? if (count($resource['actions']) > 1): ?>
                    <? foreach ($user_roles as $rvalue): ?>
                                    <td></td>
                    <? endforeach; ?>
                                </tr>

                    <? foreach ($resource['actions'] as $kkey => $action): ?>
                                            <tr class="<? if ($kkey % 2 == 0): ?>even<? else: ?>odd<? endif; ?>">
                                                        <td style="padding-left: 15px;"><?= $action['name'] ?></td>
                        <? foreach ($user_roles as $rvalue): ?>

                                                        <td><input type="checkbox" name="user_rule[<?= $rvalue['id'] ?>][<?= $resource['resource'] ?>][<?= $action['action'] ?>]" value="1"
                                                                                  onclick ="changeRule('<?= $rvalue['name'] ?>','<?= $resource['resource'] ?>', '<?= $action['action'] ?>', this.checked)"  class="ajaxChangeRole user_<?= $value['id'] ?>" <? if ($user_rules[$rvalue['id']][$resource['resource']][$action['action']] || $rvalue['name'] == 'admin'): ?>checked="checked"<? endif; ?> <? if ($rvalue . name == 'admin'): ?>disabled="disabled"<? endif; ?> /></td>

                        <? endforeach; ?>
                                                        </tr>
                    <? endforeach; ?>
                <? else: ?>
                    <? foreach ($user_roles as $rvalue): ?>
                                                                <td class="tCenter"><input type="checkbox" name="user_rule[<?= $rvalue['id'] ?>][<?= $resource . resource ?>][*]" value="1"
                                                                                          onclick ="changeRule('<?= $rvalue['name'] ?>','<?= $resource['resource'] ?>', '*', this.checked)"  class="ajaxChangeRole user_<?= $value['id'] ?>" <? if ($user_rules[$rvalue['id']][$resource['resource']]['*'] || $rvalue['name'] == 'admin'): ?>checked="checked"<? endif; ?> <? if ($rvalue['name'] == 'admin'): ?>disabled="disabled"<? endif; ?> /></td>
                    <? endforeach ?>
                                                                    </tr>
                <? endif; ?>
            <? endforeach; ?>
                        </tbody>
        </table>
        <!--                        content module-->
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>
