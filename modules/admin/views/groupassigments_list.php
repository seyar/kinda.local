<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 19.11.2010 - 16:30:16 Exp $
 *
 * Project:     Chimera2.local
 * File:        groupassigments_list.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript">
    $().ready(function()
    {
        $('.ajaxChangeRole').click(function(){
            obj = $(this);
            url = document.location.href + obj.attr('rel_user') + '/' + (obj.attr('checked')?'add':'rem') + '?role=' + obj.attr('rel_role');
            $.get(url, function(data){
                if(data != '1')
                {
                    alert('Opps! Error was happen!\n:(');
                    obj.attr('checked', !obj.attr('checked'));
                }
            });
        });
    });
</script>
<!--floating block-->

<!--floating block-->

<div class="rel">
    <div class="whiteBg">
        <!--                        content module-->
        <table width="100%" cellpadding="0" cellspacing="0" class="sortableContentTab tablepaddingleft20">
            <thead>
                  <tr class="first">
                        <th class="first" width="1%"><input type="checkbox" name="chk[]" class="listCheckboxAll" /></th>
                        <th width=""><?php echo I18n::get('User Name')?></th>                                         
                        <?foreach($user_roles as $key => $value):?>
                        <th <?if(end($user_roles) == $item):?>class="last"<?endif;?>><?=$value['name']?></th>
                        <?endforeach;?>
                  </tr>
            </thead>
            <tbody>
        
        <?foreach($rows as $key => $value):?>
                  <tr class="{iteration is odd?'odd':'even'}">
                    <td class="tCenter"><input type="checkbox" name="chk[]" class="listCheckbox" /></td>
                    <td><?=$value['username']?></td>
                    <?foreach($user_roles as $rvalue):?>
                    <td><input type="checkbox" name="user_role_<?=$value['id']?>[<?=$rvalue['id']?>]" rel_user="<?=$value['id']?>" rel_role="<?=$rvalue['id']?>"
                                              class="ajaxChangeRole user_<?=$value['id']?>" <?if($value['roles'][$rvalue['id']]):?>checked="checked"<?endif;?> /></td>
                    
                    <?endforeach;?>
                  </tr>
        <?endforeach;?>
            </tbody>
                  </table>
    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>
