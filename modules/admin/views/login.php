<!DOCTYPE html PUBLIC  "-//W3C//DTD XHTML 1.0 Strict//EN" "www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>ChimeraCMS : <?=i18n::get('Вход в систему');?></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta http-equiv="X-UA-Compatible"content="IE=EmulateIE7"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="author" content="BestItSolutions"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/styleLib.css"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/googleBtn.css"/>
        <link rel="stylesheet" type="text/css" href="/static/admin/css/paginator.css"/>
        <!--[if ie]><link rel="stylesheet" type="text/css" href="/static/admin/css/styleIe.css"/><![endif]-->
        <script type="text/javascript" src="/vendor/jquery/jquery.min.js"></script>

        <!-- additional css & js here -->
        <script type="text/javascript" src="/static/admin/js/login.js"></script>
        <script type="text/javascript" src="/static/admin/js/main.js"></script>
        <!--        custom select -->
        <link rel="stylesheet" href="/vendor/jquery/customselect/styles.css" type="text/css" />
        <script type="text/javascript" src="/vendor/jquery/customselect/jquery.customselect.js"></script>

        <script type="text/javascript" src="/vendor/jquery/jquery.paginator"></script>

        <!-- custom select -->
        <!-- <script type="text/javascript" src="/static/admin/js/clock.js"></script> -->
        <!-- <script type="text/javascript" src="/static/admin/js/shortcuts.js"></script> -->
        <script type="text/javascript" src="/static/admin/js/lang/<?=I18n::$lang?>.js"></script>

    </head>
  <body>
      <div class="container" style="background: none;">
            <div class="loginWrap">
              <div class="loginHead tCenter">Система управления контентом</div>
              <div class="loginBody">
                  <form action="" method="POST" autocomplete="off">
                    
                  <table cellpadding="0" cellspacing="0">
                      <tr class="inputs">
                          <td><div><input type="text" name="username" value="<?=isset($_POST['username'])?$_POST['username']:''?>" placeholder="Login"/></div></td>
                          <td><div><input type="password" name="password" value="" placeholder="Password"/></div></td>
                          <td><input type="submit" value="Войти" class="enter"/></td>
                      </tr>
<!--                      <tr class="inputs">
                          <td><?= I18n::get('Code to enter') ?></td>
                          <td>
                              <div class="flLeft"><input type="text" name="captcha" value="" style="width: 130px;"/></div>
                              <?=$captcha_image_src?>
                              <span class="clear"></span>
                              <img src="{$captcha_image_src}" alt="captcha code" class="flRight"/>
                          </td>
                      </tr>-->
                      <tr>
                          <td>&nbsp;</td>
                          <td colspan="2" class="tRight">
                              <!--<label><input type="checkbox" name="link_to_ip" value="1" class="vMiddle"/>
                                  <a href="javascript:void(0);" ><?= I18n::get('Bind session to the IP') ?></a>
                              </label>-->
                              <a href="{$admin_path}{$controller}/restore/"><?= I18n::get('Forgot your password') ?>?</a>
                          </td>
                          <td>&nbsp;</td>
                      </tr>
                  </table>
                      <?php
                    if ( ! empty($errors))
                    {
                      echo '<ul class="errors">';
                      foreach ($errors as $field => $error){
                            if($error == 'error.password.invalid')
                                echo '<li rel="'.$field.'">Invalid Password</li>';
                            else
                                echo '<li rel="'.$field.'">'.ucfirst($error).'</li>';
                      }
                      echo '</ul>';
                    }?>
                  </form>
              </div>
              <br/>
<!--              <p class="blueText">Информация</p>
              <p style="font-size: 14px;color:#999999;padding: 5px 0;">Для консоли администратора для правильной работы:</p>

              <ul>
                  <li>Cookies должны быть включены в Вашем броузере.</li>
                  <li>Javascript должeн быть включен в броузере.</li>
                  <li>Всплывающие окна должны быть разрешены по следующему адресу: (<?=$_SERVER['SERVER_NAME']?>).</li>
              </ul>-->
          </div>
          <div class="prefooter"></div>
          <div class="clear"></div>
      </div>
      <div class="footer tCenter">Best IT Solutions Limited, 2009-<?=date('Y')?> &copy;. All rights reserved.</div>

  </body>
</html>