<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 18:04:18 Exp $
 *
 * Project:     Chimera2.local
 * File:        settings_languages.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<div id="LanguagesTab">
<table width="100%" style="margin: 1em auto;" cellspacing="0" cellpadding="0">
    <tr class="first">
        <th width="2%" >&nbsp;</th>
        <th width="30%" class="first"><?php echo I18n::get('Language')?></th>
        <th width="29%"><?php echo I18n::get('Prefix')?></th>
        <th width="29%"><?php echo I18n::get('Shortname')?></th>
        <th width="10%"><?php echo I18n::get('Action')?></th>
    </tr>
</table>


<div class="padding20px">
    <button class="btn" type="button" onclick="return LangShow();"><span><span><?php echo I18n::get('Add Language')?></span></span></button>

<br />
<br />
    <div id="langFormDiv" class="grayText2 italic hidden">
        <div class="dottedBottomBorder" style=""></div>
        <br /><input type="hidden" name="lang_id" value="0"/>
        <div class="flLeft tLeft" style="width: 31%;padding: 0 20px 0 0;">
            <div class="blocktitles"><?php echo I18n::get('Language')?>: </div>
            <div class="winput"><input type="text" name="lang_name" value="" maxlength="127"></div>
        </div>
        <div class="flLeft tLeft" style="width: 31%;padding: 0 20px 0 0;">
            <div class="blocktitles"><?php echo I18n::get('Prefix')?>: </div>
            <div class="winput"><input type="text" name="lang_prefix" value="" maxlength="3"></div>
        </div>
        <div class="flLeft tLeft" style="width: 31%;padding: 0 20px 0 0;">
            <div class="blocktitles"><?php echo I18n::get('Short Name')?>: </div>
            <div class="winput"><input type="text" name="lang_shortname" value="" maxlength="3"></div>
        </div>
        <div class="clear"></div>
        <br />
        <button class="btn" type="button" onclick="return LangSave();"><span><span><?php echo I18n::get('Save')?></span></span></button>
        <button class="btn" type="button" onclick="return LangCancel();"><span><span><?php echo I18n::get('Cancel')?></span></span></button>
    </div>
<br />
</div>
</div>