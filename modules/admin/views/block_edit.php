<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 16:47:54 Exp $
 *
 * Project:     Chimera2.local
 * File:        block_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */

?>
<?
/**
 * @param String    $textareaID may be defined. ID or name Textarea.
 */
include 'system/wysiwyg.php';
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>

<!--floating block-->
<form method="post" action="">
    <div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
        <div class="absBlocks floatingInner">
            <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td><button class="btn blue formSave" type="button" rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                <span><span><?= I18n::get('Save') ?></span></span></button></td>
                        <td style="width:35%">
                            <? if ($obj['id']): ?>
                                <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">
                                    <span><span><?= I18n::get('Apply') ?></span></span></button>
                            <? else: ?>
                                <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/apply">
                                    <span><span><?= I18n::get('Apply') ?></span></span></button>
                            <? endif; ?>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?= $admin_path . $controller ?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel" >

        <div class="whiteblueBg padding20px">
            <? if ($errors): ?><div class="errors"><? foreach ($errors as $message)
                echo $message; ?></div><? endif; ?>

            <!-- **** -->
            <table class="borderNone italic grayText2" width="100%">
                <tr>
                    <td>
						<div class="blocktitles"><?= I18n::get('Block Title') ?>:</div>
                        <input type="text" style="width: 95%" name="block_name" value="<?= (isset($obj['name']) ? $obj['name'] : $_POST['name']) ?>"/>
                    </td>
                    <td style="">
						<div class="blocktitles"><?= I18n::get('Block Description') ?>:</div>
                        <input type="text" style="width: 95%" name="block_description" value="<?= (isset($obj['description']) ? $obj['description'] : $_POST['description']) ?>"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
						<div class="blocktitles"><?= I18n::get('Contents') ?>:</div>
                        <textarea rows="50" cols="5" id="ckeditor" name="block_content"><?= (isset($obj['content']) ? $obj['content'] : $_POST['block_content']) ?></textarea>
                    </td>
                </tr>
            </table>
            <!-- **** -->
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
