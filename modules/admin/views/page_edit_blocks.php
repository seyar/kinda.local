<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 16.11.2010 - 17:35:13 Exp $
 *
 * Project:     Chimera2.local
 * File:        page_edit_main.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 */
?>
<table style="width: 100%;" id="pageBlocksList">
    <tr>
        <th><?=I18n::get('Name')?></th>
        <th width="15%"><?=I18n::get('Edit time')?></th>
        <th width="5%">&nbsp;</th>
    </tr>

<?
foreach ($page_blocks as $block_info):
?>
    <tr id="pageBlockRow_<?=$block_info['id']?>">
        <td>
            <a id="pageBlockName_<?=$block_info['id']?>" class="pageBlocksLink" href="<?=$admin_path.$controller?><?=$block_info['id']?>/load_page_block"><?=$block_info['page_block_name'];?></a>
        </td>
        <td>
            <?=$block_info['date'];?>
        </td>
        <td style="text-align: center">
            <a class="ajax" rel="status" href="<?=$admin_path.$controller?><?=$block_info['id']?>/active_page_block" ><img
                src="/static/admin/images/act_<?if($block_info['status'] == 1):?>green<?else:?>red<?endif;?>.png"
                alt="<?echo I18n::get('Active');?>" title="<?echo I18n::get('Active');?>"/></a>&nbsp;
            <a class="ajax" rel="delete" block_id="<?=$block_info['id']?>" href="<?=$admin_path.$controller?><?=$block_info['id']?>/delete_page_block" ><img
                src="/static/admin/images/del.png"
                alt="<?echo I18n::get('Delete');?>" title="<?echo I18n::get('Delete');?>"/></a>
        </td>
    </tr>
<?endforeach;?>
        <tr>
            <td colspan="3">
                <a class="pageBlocksLink" href="<?=$admin_path.$controller?>0/load_page_block"><?=i18n::get('Create new block')?></a>
            </td>
        </tr>
</table>
<div id="pageBlocksEdit" class="hidden"></div>
<script type="text/javascript">
    $('a.pageBlocksLink').click(function(){
        $.post($(this).attr('href'), {page_id:<?=$id?>}, function(html) {
            if (html)
            {
                    $("#pageBlocksList").hide();
                    $("#pageBlocksEdit").show().html(html);
            }
        });
    return false;
    })

    $('#pageBlocksList a.ajax').click(function(){
        var obj = this;
        $.get($(this).attr('href'), function(data){
            if(data=='1' || data=='0')
            {
                switch($(obj).attr('rel')) {
                    case 'status':
                        if (data=='1')
                            $(obj).children('img').attr('src', $(obj).children('img').attr('src').replace('red', 'green')).stop(true,true).hide().fadeIn();
                        else
                            $(obj).children('img').attr('src', $(obj).children('img').attr('src').replace('green', 'red')).stop(true,true).hide().fadeIn();
                        break;
                    case 'delete':
                        $('#pageBlockRow_'+$(obj).attr('block_id')).remove();
                        break;
                }
            }
            return false;
        })
        return false;
    })
</script>
