<?php defined('SYSPATH') OR die('No direct access allowed.');
 
return array
(
        'version' => '0.5.0.0',
        'cache' => false,
        'debug' => false,
        'security' => false,
        'force_compile' => false,
        'error_reporting' => E_ALL ^ E_NOTICE , //E_COMPILE_ERROR|E_RECOVERABLE_ERROR|E_ERROR|E_CORE_ERROR|E_PARSE|E_USER_ERROR,
        'php_handling' => 0, //a number between 0 and 3, chechk quicky for SMARTY_PHP_* constants
 
        'template_dir' => DOCROOT. 'templates/',
        'compile_dir' => DOCROOT . 'cache/quicky_compile/',
        'plugin_dir' => array(), //you can put in multiple paths for quicky to load plugins from
        'cache_dir' => DOCROOT . 'cache/quicky/',
        'include_before' => array(),
        'include_after' => array(),
        'ldelim' => '{',
        'rdelim' => '}',
);