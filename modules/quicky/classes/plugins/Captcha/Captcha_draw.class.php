<?php
/**************************************************************************/
/* (c)oded 2006 by white phoenix
/* http://whitephoenix.ru
/*
/* CAPTCHA_draw.class.php
/**************************************************************************/
class CAPTCHA_draw {
    public $text;
    public $heigth = 33;
    public $width = 116;
    public $quantity;
    public $linecount = 1;
    public $linecolor = 0xC6C6C6;
    public $textcolor = 0xC6C6C6;
    public $bgcolor = 0xA1A1A1;
    public $noise1 = FALSE;
    public $noise2 = FALSE;
    public $noise3 = FALSE;
    public $noise_wave_n = 0;
    public $noise_skew_n = 1;
    public $fonts_dir;
    
    function __construct() {
        $this->fonts_dir = dirname(__FILE__).'/fonts/';
    }
    function generate_text() {
        return $this->text = chr_gen('ABCDEFGHKLMNPQRSTUVWXYZ23456789abcdefghkmnpqrstuvwxyz23456789', 5);
    }
    function show() {
        require_once dirname(__FILE__).'/imagedraw.class.php';
        ob_start();
        $text = $this->text;
        $draw = new imagedraw;
        $draw->type = 'png';
        $draw->W = $this->width;
        $draw->H = $this->heigth;
        $draw->init();
        $draw->createtruecolor();
        $draw->antialias(TRUE);
        $draw->setbgcolor($this->bgcolor);
        //$draw->colortransparent(0xFFFFFF);
        $fonts = array();
        $fonts = array(
                'Verdana',
                'Times',
                'Tempsitc',
        );

        // draw text
        for($i=0;$i<strlen($text);$i++) {
            $size = rand(15,20);
            $angle = rand(-10,10);
            $x = 10+rand(-5,5)+$i*20;
            $y = 20+rand(-5,5)+$size/5;
            $font = $fonts[array_rand($fonts)].'.ttf';
            $font_p = $this->fonts_dir.$font;
            $draw->ttftext($text{$i}, $this->textcolor, $font_p, $size, $x, $y, $angle);
        }

        // draw line
        for ($j = 0; $j < $this->linecount; $j++) {
            $lastX = -1;
            $lastY = -1;
            $N = rand(5, 15);
            for ($i = 1; $i < $draw->W+1; $i+=5) {
                $X = $i;
                $Y = $draw->H/2-sin($X/$N)*10+$j*10;
                if ($lastX > -1) {
                    $draw->line1($lastX,$lastY,$X,$Y,$this->textcolor,2);
                }
                $lastX = $X;
                $lastY = $Y;
            }
        }

//        for ($i = 0; $i < $this->noise_skew_n; $i++) {
//            $draw->skew_waves();
//        }
//
//        $draw->colortransparent(0x000000);
//        $draw->setbgcolor($this->bgcolor);
        
        if (strlen(ob_get_contents()) == 0) {
            header('Content-type: image/png');
        }
        $draw->out();
    }
}

if (!function_exists('chr_gen')) {
    function chr_gen($chars,$len) {
        if (!strlen($chars) or !is_numeric($len)) {
            return FALSE;
        }
        $result = '';
        $l = strlen($chars)-1;
        for($i = 0; $i < $len; $i++) {
            $result .= $chars{mt_rand(0,$l)};
        }
        return $result;
    }
}