<?php
function quicky_function_get_url_by_id($params,$quicky)
{
    $module = $params[0];
    $id = $params[1];
    $class = "Controller_".ucfirst($module);
    return call_user_func(array($class,'getUrlById'), $id);
}
