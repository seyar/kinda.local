{include file="../../admin/views/system/wysiwyg.tpl"}
<link rel="stylesheet" type="text/css" href="/static/admin/css/paginator.css" />
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.paginator.js"></script>
{include file="emailsender_edit_js.tpl"}

<div class="wrap flRight">
      <div class="precontent"></div>
      <form method="post" action="{$admin_path}{$controller}/{$module_name}/">

      <div class="rightCol ">
<!-- MODULE HEAD -->
          <div class="pageBlock {if !$show_module_desc} hidden{/if}" id="addBox">
                  <div class="topTitle rel">{$module.title}<a href="{$admin_path}" class="close"><img src="/static/admin/images/close.gif" alt=""/></a></div>
                  <div class="topBlockBody">
                      <img src="/static/admin/images/moduleIco.jpg" alt="" class="flLeft" width="76"/>
                      <p class="module_descr_short">{$module.descr_short}<br/><a href="#" class="showDescrFull">{php}echo I18n::get('Show full'){/}</a> </p>
                      <p class="module_descr_full hidden">{$module.descr_full}<br/><a href="#" class="showDescrShort">{php}echo I18n::get('Show short'){/}</a> </p>
                      <div class="clear"></div>
                  </div>
          </div>
          <div class="breadcrumbs">
              <p class="flLeft"><a href="{$admin_path}">{php}echo I18n::get('Content'){/}</a> &gt; {$module.title}</p>
              <p class="flRight"><a href="{$admin_path}"
                    class="showHelp {if $show_module_desc} hidden{/if}">{php}echo I18n::get('Show help'){/}</a>&nbsp;<a href="#"
                    class="addShortcut">{php}echo I18n::get('Add to &quot;shortcuts&quot;'){/}</a></p>
              <div class="clear"></div>
          </div>
<!-- /MODULE HEAD -->
          <div class="pageBlock noPadding">
              <div class="topTitle cont rel">{$module.title}</div>
              <div class="fancy rel">
                  <div class="contentNavBtns" id="contentNavBtns">
                      <div class="inner">
                          <a href="{$admin_path}{$controller}/{$module_name}/edit" class="greenBtn flLeft">{php}echo I18n::get('add'){/}</a>
                          <p class="flLeft">
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              {php}echo I18n::get('With selected'){/}
                              <select id="list_action" onchange="">
                                  <option value="{$admin_path}{$controller}/{$module_name}/send">{php}echo I18n::get('Send'){/}</option>
                                  <option value="{$admin_path}{$controller}/{$module_name}/delete">{php}echo I18n::get('Delete'){/}</option>
                              </select>
                              <button class="btn formUpdate" onclick="{literal}if( !checkChks()){alert('Выберите хоть один email'); return false;}{/literal}" type="button"><span><span>{php}echo I18n::get('Start'){/}</span></span></button>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          </p>                          
                          <div class="clear"></div>
                      </div>
                  </div>
              </div>

              <div class="innerGray">
                  <br/>
                  <table>
                      <tr><td>Текст письма</td></tr>
                      <tr><td><textarea name="text" rows="4" cols="74" id="ckeditor"  class="jquery_ckeditor"></textarea></td></tr>
                  </table>
                  <br/>
                  <table width="100%" cellspacing="0" cellpadding="0" id="contentTab" class="contentTab">
                      <tr>
                          <th width="2%" class="tLeft"><input type="checkbox" class="listCheckboxAll" style="width: auto;" value="" name=""></th>
                          <th width="2%">№</th>
                          <th class="tLeft">Email</th>
                          <th width="9%"></th>
                      </tr>
{foreach name="rows" from=$rows key="key" value="value"}
                    <tr>
                        <td><input type="checkbox" class="listCheckbox" value="{$value.id}" name="chk[{$value.id}]"></td>
                        <td>{$key+1}</td>
                        <td><a href="{$admin_path}{$controller}/{$module_name}/{$value.id}/edit" >{$value.email|truncate:100}</a></td>
                        <td>
                            {*<a href="{$admin_path}{$controller}/{$module_name}/{$value.id}/changeStatus" onclick="changeStatus(this);return false;"><img src="{if $value.status == 1}/static/admin/images/status-on.png{else}/static/admin/images/status-off.png{/if}" alt="status" title="{php}echo I18n::get('status'){/}"/></a>&nbsp;&nbsp;&nbsp;*}
                            <a href="{$admin_path}{$controller}/{$module_name}/{$value.id}/edit" ><img src="/static/admin/images/edi.gif" alt="edit" title="{php}echo I18n::get('Edit'){/}"/></a>&nbsp;&nbsp;&nbsp;
                            <a href="{$admin_path}{$controller}/{$module_name}/{$value.id}/delete" onclick="if( confirm('Вы уверены?') ) deleteItem(this);return false;"><img src="/static/admin/images/del.gif" alt="delete" title="{php}echo I18n::get('Delete'){/}"/></a>
                        </td>
                    </tr>
{/foreach}
                  </table>
                  {if $total_pages && $total_pages>1}
                  {*$pages_list*}
	<script language="javascript" type="text/javascript">
{literal}
		    $(document).bind('ready', function ()
		    {
			var page = /page=([^#&]*)/.exec(window.location.href);
			page = page ? page[1] : 1;

			$('#paginator').paginator(
			{
			    pagesTotal: {/literal}{$total_pages}{literal}
			    ,pagesSpan: 10
			    ,pageCurrent: page
			    ,baseUrl: '?page='
			    ,lang: {
			{/literal}
				next  : "{php}echo I18n::get('Next'){/}",
				last  : "{php}echo I18n::get('Last'){/}",
				prior : "{php}echo I18n::get('Prior'){/}",
				first : "{php}echo I18n::get('First'){/}",
				arrowRight : String.fromCharCode(8594),
				arrowLeft  : String.fromCharCode(8592)
			  {literal}
			    }
			});
		    });
{/literal}
	</script>
		    <div class="paginator" id="paginator"></div>
		  {/}
              </div>
              <div class="clear"></div>
          </div>
          <p class="tRight"><a href="{$admin_path}">{php}echo I18n::get('Go to start page'){/}</a></p>
      </div>
      </form>
      <div class="clear"></div>
  </div> <!-- content -->