    <script type="text/javascript" src="/vendor/jquery/jquery.paginator.js"></script>

    {literal}
    <script type="text/javascript">
	function nextPage(page_id)
	{
	    current_page = parseInt($(page_id).val());

	    if(current_page < $('#total_pages').val())
		$(page_id).val( current_page+1 );

	    return current_page;
	}

	function prevPage(page_id)
	{
	    current_page = parseInt($(page_id).val());

	    if(current_page > 1)
		$(page_id).val( current_page-1 );

	    return current_page;
	}
    </script>

    <script type="text/javascript">

    function deleteItem( thisObj )
    {
        $.get(
            $(thisObj).attr('href'),
            function(data)
            {
                if(data == 1)
                    $(thisObj).parent().parent().remove();
                else
                    alert('Server error!');
            }
        );
    }

    function changeStatus( thisObj )
    {
        $.get(
            $(thisObj).attr('href'),
            function(data)
            {
                source = $(thisObj).children('img').attr('src');

                if(data == 1)
                    if( source.indexOf('status-on') != -1 )
                        $(thisObj).children('img').attr('src', '/static/admin/images/status-off.png');
                    else
                        $(thisObj).children('img').attr('src', '/static/admin/images/status-on.png')
                else
                    alert('Server error!');
            }
        );
    }

    function checkChks()
    {
        var err = 1;
        $('.listCheckbox').each(function(){
            if( $(this).attr('checked') == true) err = 0
        });
        if( err == 0) return true;
        return false;
    }

    $(document).ready(function(){
        // main
            var config = {
                        toolbar:
                        [
                            ['Styles','Format','Font','FontSize'],
                            ['TextColor','BGColor'],
                            ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
                            ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
                            '/',
                            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
                            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
                            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                            ['Link','Unlink','Anchor'],
                            ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],
                            ['Templates','Maximize','ShowBlocks','-','Source']
                        ]
                        , defaultLanguage: 'ru'
                        , filebrowserBrowseUrl : '/vendor/elfinder/elfinder.html'
        //		, stylesCombo_stylesSet: 'my_styles:/static/css/styles.js'
                };

            var ckeditor = CKEDITOR.replace('ckeditor', config);
    });
    </script>

    <style type="text/css">
        
    </style>
{/literal}
