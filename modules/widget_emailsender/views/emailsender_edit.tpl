{ include file="wysiwyg.tpl" }
<script type="text/javascript" src="/static/admin/js/edit.js"></script>

{include file="emailsender_edit_js.tpl"}

<div class="wrap flRight">
      <div class="precontent"></div>
      <form method="post" action="" enctype="multipart/form-data">

      <div class="rightCol ">
<!-- MODULE HEAD -->
          <div class="pageBlock {if !$show_module_desc} hidden{/if}" id="addBox">
                  <div class="topTitle rel">{$module.title}<a href="{$admin_path}" class="close"><img src="/static/admin/images/close.gif" alt=""/></a></div>
                  <div class="topBlockBody">
                      <img src="/static/admin/images/moduleIco.jpg" alt="" class="flLeft" width="76"/>
                      <p class="module_descr_short">{$module.descr_short}<br/><a href="#" class="showDescrFull">{php}echo I18n::get('Show full'){/}</a> </p>
                      <p class="module_descr_full hidden">{$module.descr_full}<br/><a href="#" class="showDescrShort">{php}echo I18n::get('Show short'){/}</a> </p>
                      <div class="clear"></div>
                  </div>
          </div>
          <div class="breadcrumbs">
              <p class="flLeft"><a href="{$admin_path}">{php}echo I18n::get('Content'){/}</a> &gt; {$module.title}</p>
              <p class="flRight"><a href="{$admin_path}"
                    class="showHelp {if $show_module_desc} hidden{/if}">{php}echo I18n::get('Show help'){/}</a>&nbsp;<a href="#"
                    class="addShortcut">{php}echo I18n::get('Add to &quot;shortcuts&quot;'){/}</a></p>
              <div class="clear"></div>
          </div>
<!-- /MODULE HEAD -->
          <div class="pageBlock noPadding">
              <div class="topTitle cont rel">{$module.title}</div>
                {*<div class="topTabs">
                    <a href="#" class="active flLeft showMain"><span>{php}echo I18n::get('Main'){/}</span></a>
                    

                    <div class="clear"></div>
                </div>*}
              <div class="fancy rel">
                  <div class="contentNavBtns" id="contentNavBtns">
                      <div class="inner">
                          <a href="{$admin_path}{$controller}/{$module_name}/{$id|default:0}/save" class="greenBtn flLeft formSave"
                             style="margin: 0 5px 0 0;">{php}echo I18n::get('Save and exit'){/}</a>
                          {if $row.id}<a href="{$admin_path}{$controller}/{$module_name}/{$id}/update" class="greenBtn flLeft formUpdate">{php}echo I18n::get('Apply'){/}</a>{/if}
                          <p class="flRight">
                              <a href="{$admin_path}{$controller}/{$module_name}/" class="greenBtn flLeft">{php}echo I18n::get('Cancel'){/}</a>
                          </p>
                          <div class="clear"></div>
                      </div>
                  </div>
              </div>
              {if $errors}<div class="errors">{foreach from=$errors item=message}{$message}{/}</div>{/if}
              
              <div class="innerGray" style="padding:0 25px">
                    <div id="mainTabInfo" class="contentTab">
                        {include file="emailsender_content.tpl"}
                    </div>
                                       
              </div>
              
              <div class="clear"></div>
          </div>
      </div>

      </form>
      <div class="clear"></div>
  </div> <!-- content -->
