<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 01.03.2010 - 11:36:42 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        consol_emailsender.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_EmailSender extends Model {

    static public function getItems($id = NULL, $per_page = 50, $code = NULL)
    {
        $rows_per_page = ((int)$per_page)
				? ((int)$per_page)
				: (int)cookie::get('rows_per_page', 50);

        cookie::set('rows_per_page', $rows_per_page); 

        $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
            ->from('emailsender')
        ;
        
        $count = $query_c
                ->execute()
                ->get('mycount');

        $query = DB::select()
            ->from('emailsender')
            ->order_by('date','desc')
            ->order_by('id','desc')
        ;

        if($id && is_numeric($id))          $query->and_where('id','=', $id);
        elseif($id && !is_numeric($id))     $query->and_where('email','=', $id);
        if( !$id && $code )                 $query->where('unsubscribe','=', $code);

        if (isset($_POST['page'])) $_GET['page'] = $_POST['page'];
        
        $pagination = Pagination::factory(array(
		    'total_items'    => $count,
		    'items_per_page' => $rows_per_page,
	    ));

        $result = $query
//                ->limit($pagination->items_per_page)
//                ->offset($pagination->offset)
                ->execute();
        $page_links_str = '';
	    $page_links_str = $pagination->render('pagination/digg');

        if($id || $code)
            return $return = $result->current();
        else
        {
            return array( $result->as_array(), $page_links_str, $pagination->total_pages );
        }        
    }

    public function validate_post()
    {
        $keys = array (
			'email'
		    );

	$params = Arr::extract($_POST, $keys, '');

	$post = Validate::factory($params)

					->rule('email', 'not_empty')
					->rule('email', 'email')
					
//					->rule('full_text', 'not_empty')
	;

        if ($post->check())
	{
	    return $params;
	}
	else
	{
	    $this->errors = $post->errors('validate');
	}
    }

    function saveItem( $id = NULL, $code = NULL )
    {
        if( !$post = Model_emailsender::validate_post()){ die(Kohana::debug( $post ));return false;}
        $email = Model_EmailSender::getItems($post['email']);

        if( $email ) return $email['id'];
        if( !$id )
        {
            $keys = array( 'email');
            $ins = array( $post['email']);
            if($code)
            {
                $keys[] = 'unsubscribe';
                $ins[] = $code;
            }
            $keys[] = 'date';
            $ins[] = DB::expr('NOW()');

            DB::insert('emailsender', $keys )
                ->values( $ins )
                ->execute()
            ;
            $id = mysql_insert_id();

        }else{
            DB::update('emailsender')
                ->set(array(
                    'email'=>$post['email'],
                ))
                ->where('id','=',$id)
                ->execute()
            ;
        }
        return $id;
        
    }

    

    static function deleteItem($id)
    {
        if( !$id ) return false;
        DB::delete('emailsender')->where('id', '=', $id)->execute();
        return 1;
    }

    function deleteItems( $array )
    {
        $query = DB::delete('emailsender');
        if (count($array))
	{
	    $list = array_flip($array);
            $query->where('id', 'in', $list ); // ->limit(1)
	    $total_rows = $query->execute();
	}
	return true;
    }

    function send( $array )
    {
        $list = array_flip($array);
            
        foreach($list as $item)
        {
            $text = '<div style="background: #D8E5DC; height: 62px; width: 800px; padding: 5px;"><img src="http://consol.ua/templates/consol/images/email_logo.jpg" alt=""/></div>'.$_POST['text'];
            $row = Model_EmailSender::getItems($item);
            if( $row['unsubscribe'] ) 
                $text .= '<br/><br/>...................................<br/>Для того чтоб отписаться от получения новостей перейдите по ссылке <a href="http://'.$_SERVER['HTTP_HOST'].'/emailsender/un/'.$row['unsubscribe'].'">http://'.$_SERVER['HTTP_HOST'].'/emailsender/un/'.$row['unsubscribe'].'</a>';
            if(! Model_EmailSender::send_mail(
                    Kohana::config('emailsender')->send_from,
                    $row['email'],
                    Kohana::config('emailsender')->subject,
                    $text
                )){ continue;}

        }
        
	return true;
    }

    public static function send_mail($from, $to, $subject, $message, $charset = 'UTF-8')
    {        
	$headers = 'MIME-Version: 1.0' . "\r\n" .
			'Content-type: text/html; charset='.$charset . "\r\n" .
			'From: ' . $from . "\r\n" .
		    'X-Mailer: ChimeraCMS_2';
        $subject = "=?UTF-8?B?".base64_encode($subject)."?=\n";
	if ( mail($to, $subject, $message, $headers) )
	    return 1;
        return false;
    }

//    function changeStatus($id)
//    {
//        DB::query(Database::UPDATE, "UPDATE emailsender SET status = !status WHERE id = $id")
//                ->execute()
//            ;
//        return 1;
//    }

    static function unsubscribe( $code )
    {        
        $obj = Model_EmailSender::getItems(NULL, NULL, $code);
        return Model_EmailSender::deleteItem($obj['id']);
    }
}
?>