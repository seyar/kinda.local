<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 04.03.2010 - 11:12:57 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        emailsenderFrontend.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_EmailsenderFrontend extends Controller_Content
{
    public function action_save()
    {
        $code = md5( uniqid() );
        $res = Model_EmailSender::saveItem(NULL,$code);
            
        if($res)
        {
            Cookie::set('yousigned', 'test');            
            $search = array('%HTTP_HOST%', '%code%');
            $replace = array( $_SERVER['HTTP_HOST'], $code );
            $message = str_replace($search, $replace, file_get_contents( Kohana::config('emailsender')->path.'/email_yousigned.tpl'));

            Model_EmailSender::send_mail(
                    Kohana::config('emailsender')->send_from,
                    $_POST['email'],
                    Kohana::config('emailsender')->subject,
                    $message
                );
            $this->request->redirect( $_SERVER['HTTP_REFERER'] );
        }
        else
        {            
            $this->view->assign('errors', $this->model->errors);
            $this->request->redirect( '/' );
        }
    }

    public function action_un()
    {
        $res = Model_EmailSender::unsubscribe( $this->request->param('code') );
        $this->page_info = array(
            'page_title' => i18n::get('You have succesfully unsubscribed'),
            'page_keywords' => i18n::get('You have succesfully unsubscribed'),
            'page_description' => i18n::get('You have succesfully unsubscribed'),
            'page_content' => i18n::get('You have succesfully unsubscribed'),
        );
        $this->template = 'inner.tpl';
    }
}

?>