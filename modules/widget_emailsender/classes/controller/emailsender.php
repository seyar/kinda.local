<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 01.03.2010 - 11:36:32 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        votes.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_EmailSender extends Controller_AdminModule {

    public $template		= 'emailsender.tpl';

    public $module_title	= 'emailSender';
    public $module_desc_short   = 'emailSender Descr Short';
    public $module_desc_full	= 'emailSender Descr Full';

    public function before()
    {
        parent::before();
	$this->model = new Model_EmailSender();
    }
    
    public function action_index()
    {
        $res = $this->model->getItems();
        
        $this->view->assign( 'rows', $res[0] );
        $this->view->assign( 'pages_list', '' );
	$this->view->assign( 'total_pages', '' );
    }

    public function action_edit()
    {
        $res = $this->model->getItems( $this->request->param('id') );
        $this->view->assign( 'row',$res );
        $this->template = 'emailsender_edit.tpl';
    }

    public function action_save()
    {
        if($this->model->saveItem( $this->request->param('id') ))
        {
            $this->redirect_to_controller( $this->request->controller );
        }else
	{
	    $this->view->assign('errors', $this->model->errors);
	    $this->action_edit();
	}
    }

    public function action_update()
    {
        if($this->model->saveItem( $this->request->param('id') ))
        {
            $this->redirect_to_controller( $this->request->controller.'/'.$this->request->param('id').'/edit' );
        }else
	{
	    $this->view->assign('errors', $this->model->errors);
	    $this->action_edit();
	}
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            die(print($this->model->deleteItem( $this->request->param('id') )));

        elseif (count($_POST['chk']))
            $this->model->deleteItems( $_POST['chk'] ) ;
        $this->redirect_to_controller($this->request->controller);
    }

    public function action_send()
    {
        
        if(count($_POST['chk']))
            $this->model->send( $_POST['chk'] );
        
        $this->redirect_to_controller($this->request->controller);
    }
}

?>