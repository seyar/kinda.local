CREATE TABLE `emailsender` (
  `id` int(10) NOT NULL auto_increment,
  `firstname` varchar(255) default NULL,
  `surname` varchar(255) default NULL,
  `phone` varchar(255) default NULL,
  `email` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `unsubscribe` varchar(32) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `Index 2` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
