<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
            'name'             => 'E-mail Sender',
            'fs_name'          => 'emailsender',
            'panel_serialized' => NULL,
            'useDatabase'      => true
        );