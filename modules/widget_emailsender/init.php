<?php defined('SYSPATH') or die('No direct script access.');

Route::set
        (
	    'emailsender',
	    'admin/emailsender((/<id>)/<action>)',
	    array( 'id' => '\d{1,10}', )
        )
        ->defaults
        (
           array(
		    'controller'    => 'emailsender',
		    'action'        => 'index',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

// Frontend

Route::set
        (
	    'emailsender_front',
            '(<lang>/)emailsender(/<action>(/<code>))',
	    array( 'lang' => '(?:'.LANGUAGE_ROUTE.')', )
        )
        ->defaults
        (
           array(
		    'controller'    => 'emailsenderfrontend',
		    'action'        => 'save',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;