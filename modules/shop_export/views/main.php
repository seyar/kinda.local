<?php

defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 May 16, 2011 - 10:59:41 AM Exp $
 *
 * Project:     microshop.local
 * File:        index.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh seyarchapuh@gmail.com
 */
?>

<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>

<form method="post" action="<?=$admin_path?><?=$controller?>generate">
<?= Form::input('list_action', '', array('id' => 'list_action', 'type'=>'hidden') )?>
<!--floating block-->
    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <table width="100%">
                    <tr>
                        <td>
                            <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/generate">
                                        <span><span><?= I18n::get('Generate') ?></span></span></button>&nbsp;&nbsp;
                        </td>
                        <td width="10%"><?echo I18n::get('Exoprt to:');?></td>
                        <td>
                            <?= Form::select('exportMethod', array('yandex' => I18n::get('Yandex')), 'yandex', array('id' => 'exportMethod') )?>
                        </td>
                        <td width="50%">&nbsp;</td>
                        <? /*
                        <td class="vMiddle"><?echo I18n::get('With marked');?></td>
                        <td style="padding-top:1px; width:15%"><select id="list_action"><option value="delete"><?echo I18n::get('Delete');?></option></select></td>
                        <td style="width:51%"><button class="btn formUpdate" type="button">
                                <span><span><?echo I18n::get('Apply');?></span></span></button>
                        </td>
                        <td class="vMiddle"><div class="dottedLeftBorder"><?= I18n::get('On a page') ?> </div></td>
                        <td><?=Form::select('rows_per_page', array(
                                '10' => '10 ' . I18n::get('lines'),
                                '20' => '20 ' . I18n::get('lines'),
                                '30' => '30 ' . I18n::get('lines'),
                        ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange'=>"formfilter(jQuery('#rows_per_page'))"))?>
                        </td>
                         * 
                         */
                        ?>
                    </tr>
                </table>
            </div>
<!--            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>-->
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel whiteBg">
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>
        <div class="padding20px">
            <br />
            <? require dirname(__FILE__)."/$target/list.php";?>
            <br />
        </div>
        
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>