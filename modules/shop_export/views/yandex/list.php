<?php

defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 May 16, 2011 - 10:59:41 AM Exp $
 *
 * Project:     microshop.local
 * File:        list.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh seyarchapuh@gmail.com
 */
?>

<form action="" method="post">
Дата создания файла.<br />
<?= Form::input('date', date('Y-m-d H:i') , array('style' => 'width:10em;', 'class' => 'date') )?>
<br />
<br />

<p>
<?if($url):?>
<a href="<?= $url?>"><?= $filename?>.xml</a> (<?= $filedate?>)
<?else:?>
<?= i18n::get( 'File is not exists' )?>
<?endif;?>
</p>
</form>