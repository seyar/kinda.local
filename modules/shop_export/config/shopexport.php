<?php
defined( 'SYSPATH' ) OR die( 'No direct access allowed.' );

return array
    (
    'lang' => 'ru', // Default the  language.
    'path' => dirname( __FILE__ ) . '/../views/', // Admin templates folder.

    'exportFolder' => 'export',
    'localDeliveryCost' => 100, //указывается общая стоимость доставки для своего региона.
    'localDeliveryCostForGood' => 35, //указывается общая стоимость доставки для своего региона.
    
    'deprecatedCategories' => array(
        100294
    ), 
);
