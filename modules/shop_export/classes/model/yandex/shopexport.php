<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 May 16, 2011 - 11:48:07 AM Exp $
 *
 * Project:     microshop.local
 * File:        shopexport.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh seyarchapuh@gmail.com
 */

class Model_Yandex_ShopExport extends Model_Shopexport
{
    static $search = array('&', '<','>');
    static $replace = array('','','');
    
    public function generate( $goods = array(), $categories = array() )
    {
        $categoriesXml = self::generateCategorySection($categories);        
        $currenciesXml = self::generateCurrenciesSection();        
        $goods = self::generateGoodsSection($goods, $categories, self::getCurrencies(), $this->domainInfo);
        
        $headerSection = 
            '<name>Интернет магазин Микролайн</name>'."\r\n".
            '<company>'.$this->domainInfo['domain_title'].'</company>'."\r\n".
            '<url>http://'.$this->domainInfo['http_host'].'/</url>'."\r\n".
            '<platform>Chimera CMS</platform>'."\r\n".
            '<agency>BestArtDesign.com</agency>'."\r\n"
            ;
        
        
        $deliveryCost = '<local_delivery_cost>'.Kohana::config('shopexport.localDeliveryCost').'</local_delivery_cost>'."\r\n";
        
        $date = ( isset($_POST['date']) && strlen($_POST['date'] < 11) ) ? $_POST['date'].date( ' H:i', time()): date( 'Y-m-d H:i', time());
        
        $return = "<yml_catalog date=\"{$date}\">\r\n<shop>\r\n";
        $return .= $headerSection;
        $return .= $currenciesXml;
        $return .= $categoriesXml;        
        $return .= $deliveryCost;
        $return .= $goods;
        $return .= "</shop>\r\n</yml_catalog>";
//        $return = str_replace(self::$search, self::$replace, $return);
        return $return;
    }
    
    /**
     * Function generate category part xml 
     * 
     * @param array $categories
     * @return string 
     */
    static function generateCategorySection( $categories = array() )
    {
        $return = '<categories>'."\r\n";
        if( count($categories) )
        {
            foreach($categories as $item)
            {
                $return .= '<category id="'.$item['id'].'" parentId="'.$item['parent_id'].'">'.str_replace(self::$search, self::$replace, html_entity_decode($item['name'], ENT_COMPAT,'UTF-8')).'</category>'."\r\n";
            }
        }
        $return .= '</categories>'."\r\n";
        return $return;
    }
    
    /**
     * Function generate currencies part yandex xml 
     * 
     * @param array $currencies
     * @return string 
     */
    static function generateCurrenciesSection( $currency = array() )
    {
        if( !count($currency) ) $currency = self::getCurrencies();
        
        $return = 
            '<currencies>'."\r\n".'
            <currency id="'.$currency['name'].'" rate="'.$currency['rate'].'" plus="0"/>'."\r\n".'
            </currencies>'."\r\n";
        
        return $return;
    }
    
    /**
     * Function return current currencies
     * 
     * @return array currencies 
     */
    static function getCurrencies()
    {
        $oCurrencies = new Model_Frontend_Currencies();
        $currencies = $oCurrencies->getCurrencies();
        $currency = $currencies[Kohana::config( 'shop' )->frontend_currency];
        
        return $currency;
    }
    
    /**
     * Function generate category part xml 
     * 
     * @param array $categories
     * @return string 
     */
    static function generateGoodsSection( $goods = array(), $categories = array(), $currencies = array(), $domainInfo = array() )
    {
        if( count($goods) === FALSE ) return false;
        $imagesFolder = Kohana::config('shop.images_folder');
        $return = '';
        
        foreach($goods as $item)
        {
            $return .= "<offer id=\"{$item['id']}\" available=\"true\" bid=\"21\">\r\n";
            $catSeoUrl = str_replace( Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, urlencode( $categories[$item['categories_id']]['seo_url'] ) );
            $goodSeoUrl = str_replace( Model_Frontend_Categories::$url_search, Model_Frontend_Categories::$url_replace, urlencode( $item['seo_url'] ) );
            
            $return .=    "<url>http://{$domainInfo['http_host']}/shop/{$catSeoUrl}_{$item['categories_id']}/{$goodSeoUrl}_{$item['id']}.html</url>\r\n";
            $return .=    "<price>{$item['price']}</price>\r\n";
            $return .=    '<currencyId>UAH</currencyId>'."\r\n";
            $return .=    '<categoryId>'.$item['categories_id'].'</categoryId>'."\r\n";
            $return .=    (!empty($item['default_photo']) ? "<picture>http://{$domainInfo['http_host']}".MEDIAWEBPATH."{$imagesFolder}/{$item['id']}/{$item['default_photo']}</picture>\r\n" : '');
//            $return .=    '<store>true</store>'."\r\n";
//            $return .=    '<pickup>true</pickup>'."\r\n";
            $return .=    '<delivery>true</delivery>'."\r\n";
            $return .=    '<local_delivery_cost>'.Kohana::config('shopexport.localDeliveryCostForGood').'</local_delivery_cost>'."\r\n";
            $return .=    '<name>'.str_replace(self::$search,self::$replace, html_entity_decode(Arr::get($item, 'name',$item['alter_name']), ENT_COMPAT,'UTF-8')).'</name>'."\r\n";
//            $return .=    '<vendor>Longines</vendor>';
            $return .=    '<vendorCode>'.$item['scu'].'</vendorCode>'."\r\n";
            $return .=    (!empty($item['description']) ? '<description>'.html_entity_decode( $item['description'], ENT_COMPAT, 'UTF-8').'</description>'."\r\n" : '');
//            $return .=    '<sales_notes>Часы можно упаковать в подарочную упаковку.</sales_notes>';
//          <barcode>0123456789012</barcode>
//            $return .=    '<country_of_origin>Швейцария</country_of_origin>';
            $return .= '</offer>'."\r\n";
            
        }
        return "<offers>\r\n$return</offers>\r\n";
    }
    
    /**
     * function parse xml file, return entered date create.
     * 
     * @param string $target path to file
     * @return string date 
     */
    function getFileCreateDate( $target = NULL )
    {
        if( !file_exists($this->fileFolderUrl.$target.self::EXT) ) return false;
        $xml = file_get_contents( $this->fileFolderUrl.$target.self::EXT );
        
        preg_match('|<[^>]+date="(.+)">|Uis', $xml, $matches);
        if( isset($matches[1]) ) return $matches[1];
        return false;
    }
}