<?php

defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 May 16, 2011 - 10:44:48 AM Exp $
 *
 * Project:     microshop.local
 * File:        shopexport.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh seyarchapuh@gmail.com
 */

class Model_Shopexport extends Model
{
    const EXT = '.xml';
    protected $domainInfo;
    protected $fileFolderUrl;

    function __construct()
    {
        $this->domainInfo = Model_Content::load_domain_info();
        $this->fileFolderUrl = DOCROOT.Kohana::config('shopexport.exportFolder').'/';
    }
    
    /**
     * Function return categories id => Array('id','parent_id','name')
     * 
     * @return array 
     */
    static function getListCategories()
    {
        $deprecated = Kohana::config('shopexport.deprecatedCategories');
        $query = DB::select('id','parent_id','name', 'seo_url')
                ->from('shop_categories')
                ->where('status' ,'=','verified')
                ->and_where('id', 'NOT IN', DB::expr('('.implode(',',$deprecated).')') )
                ;
        
        $result = $query->execute();
               
        if( $result->count() == 0) return array();
        else
            return $result->as_array('id');
    }
    
    /**
     * get list  available goods
     * 
     * @param array $categories
     * @return array goods 
     */
    static function getListGoods( $categories = NULL )
    {
        if( !isset($categories) || empty($categories) ) $categories = self::getListCategories();
        $price_category = 1; // розница
        
        $query = DB::select('id','name','alter_name', 'seo_url', 'categories_id', 'scu', 'short_description','default_photo')
                ->from('shop_goods')
                ->where('categories_id','IN', DB::expr('('.implode(',',array_keys($categories)).')') )
                ;
        
//        if( !IN_PRODUCTION )
//            $query->limit(20);
        
        $query->and_where_open();
        foreach( Kohana::config('shop.frontendStatuses') as $item )
        {
            $query->or_where( "status", '=', $item );
        }
        $query->or_where( "status", '=', 'out_of_store' );
        $query->and_where_close();
        
        $result = $query->execute();
        if( $result->count() == 0 )        return array();
        else
        {
            $return = array();
            foreach( $result->as_array('id') as $key => $item )
            {
                $query = Db::select('p.price_categories_id', 'p.currencies_id', 'p.price')
                        ->from(array('shop_prices', 'p'))
                        ->where('p.good_id' , '=',  $item['id'])
                        ->order_by('p.currencies_id')
                ;

                if (is_numeric($price_category))
                    $query->where('p.price_categories_id', '=', (int)$price_category);

                $prices = array();
                $rows = $query->execute()->as_array();
                
                $oPrices = new Model_Frontend_Currencies();
                $item['price'] = $oPrices->getPrice($rows);
                $item['price'] = number_format($item['price'][$price_category][Kohana::config( 'shop' )->frontend_currency],2,'.','');
                $return[$key] = $item;
            }
            return $return;
        }
    }
    
    /**
     * Function return string url to the export file /export/yandex.xml
     * 
     * @param string $target target method
     * @return string url
     */
    function getExportFileURL( $target )
    {
        if( !file_exists($this->fileFolderUrl.$target.self::EXT) )
            return false;
        
        return '/'.Kohana::config('shopexport.exportFolder').'/'.$target.self::EXT;
    }
    
    /**
     * Function save data to file
     * 
     * @param string $target file path to save
     * @param string $data date to save
     * @return bool 
     */
    function saveExportFile( $target,$data )
    {
        if( !file_exists($this->fileFolderUrl) )
            mkdir( $this->fileFolderUrl, 0755, TRUE );
        
        $file = $this->fileFolderUrl.$target.self::EXT;
        
        if(!$handle = fopen($file, 'w')) return "some error";
        if(!fwrite($handle, $data."\r\n")) return "writing error";
        fclose($handle);
        
        return;
    }
}