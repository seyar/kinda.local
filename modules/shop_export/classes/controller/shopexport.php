<?php

defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 May 16, 2011 - 10:45:20 AM Exp $
 *
 * Project:     microshop.local
 * File:        shopexport.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh seyarchapuh@gmail.com
 */

class Controller_shopexport extends Controller_AdminModule
{
    public $template = 'main';
    public $module_title = 'ShopExport';
    public $module_name = 'ShopExport';
    
    public $target = 'yandex';
    
    public function before()
    {
//        $this->template =  $this->target.'/'.$this->template;
        parent::before();
        $this->template->target = $this->target;
        $this->model = new Model_Shopexport();
        
        $exportMethod = "Model_{$this->target}_ShopExport";
        $this->exportMethod = new $exportMethod;
    }
    
    public function action_index()
    {
        $this->template->url = $this->model->getExportFileURL( $this->target );
        $this->template->filename = $this->target;
        $this->template->filedate = $this->exportMethod->getFileCreateDate( $this->target );
    }
    
    public function action_generate()
    {
        $categories = Model_Shopexport::getListCategories();
        $res = Model_Shopexport::getListGoods( $categories );
        
        $res = $this->exportMethod->generate($res, $categories);
        $this->model->saveExportFile( $this->target, $res );
        
        $this->redirect_to_controller($this->request->controller);
        
    }
    
}