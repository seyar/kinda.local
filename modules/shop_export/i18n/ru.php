<?php

defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 May 16, 2011 - 11:05:50 AM Exp $
 *
 * Project:     microshop.local
 * File:        ru.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh seyarchapuh@gmail.com
 */

return array(
    
    'Exoprt to:' => 'Экспорт в:',
    'File is not exists' => 'Файл не существует',
    'Generate' => 'Сгенерировать',
    
);