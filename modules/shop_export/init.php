<?php defined('SYSPATH') or die('No direct script access.');

// Import Info
Route::set
        (
	    'shop_front_export',
	    '(<lang>/)shopexport((/<id>)/<action>)',
	    array(
		     'lang' => '(?:'.LANGUAGE_ROUTE.')'
		    , 'id' => '.+'
		)
        )
        ->defaults
        (
            array(
		    'controller'    => 'shopexport',
		    'action'        => 'index',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
           )
	)
;