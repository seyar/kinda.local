<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
            'name'             => 'Shop Export',
            'fs_name'          => 'shopexport',
            'panel_serialized' => '',
            'useDatabase'      => true
        );