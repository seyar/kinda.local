<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 23.04.2010 - 16:57:44 Exp $
 *
 * Project:     golden
 * File:        ru.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
return array(
//    'pagination.previous' => 'Пред.',
//    'pagination.next' => 'След.'
    'pagination.previous' => '&lt;',
    'pagination.next' => '&gt;'
);
?>