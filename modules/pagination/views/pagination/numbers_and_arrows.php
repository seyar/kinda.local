
	<a href="<?php echo $page->url($previous_page) ?>">&lt;</a>

	<?php for ($i = 1; $i <= $total_pages; $i++): ?>

		<?php if ($i == $current_page): ?>
			<a href="<?php echo $page->url($i) ?>" id="check"><?php echo $i ?></a>
		<?php else: ?>
			<a href="<?php echo $page->url($i) ?>"><?php echo $i ?></a>
		<?php endif ?>

	<?php endfor ?>
	
	<?php if ($next_page !== FALSE): ?>
		<a href="<?php echo $page->url($next_page) ?>">&gt;</a>
	<?php else: ?>
		<a href="<?php echo $page->url($total_pages) ?>">&gt;</a>
	<?php endif ?>
	
	
	
<!-- .pagination -->