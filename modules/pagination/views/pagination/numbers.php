					<div class="paginator">	

						<?php if ($first_page !== FALSE): ?>
						<a href="<?php echo $page->url($first_page) ?>">1</a>
						<?php else: ?>
							<?php echo '<a class="active">1</a>' ?>
						<?php endif ?>

						<?php for ($i = 2; $i < $total_pages; $i++): ?>

							<?php if ($i == $current_page): ?>
								<?php echo '<a class="active">'.$i.'</a>'  ?>
							<?php else: ?>
								<a href="<?php echo $page->url($i) ?>"><?php echo $i ?></a>
							<?php endif ?>

						<?php endfor ?>

						<?php if ($last_page !== FALSE): ?>
							<a href="<?php echo $page->url($last_page) ?>"><?php echo $total_pages ?></a>
						<?php else: ?>
							<?php echo '<a class="active">'.$total_pages.'</a>'  ?>
						<?php endif ?>

					</div><!-- .pagination -->