<?
    $length = 9;

    $start = $end = 0;

//echo Kohana::debug($current_page);
//echo Kohana::debug($kohana_view_data);
//echo Kohana::debug($current_page, $total_pages);

    if ($total_pages <= 9) // 1...X...9
    {
        $start = 1;
        $end = $total_pages;
    }
    elseif ($current_page >= $total_pages && $total_pages > 9) // Bug
    {
        $start = $total_pages - $length;
        $end = $current_page = $total_pages;
        $last_page = FALSE;
    }
    elseif ($current_page > 5 && ($current_page + 5) < $total_pages && $total_pages > 9) // 1...X...999
    {
        $start = $current_page - 5;
        $end = $start + $length;
    }
    elseif ($current_page > 5 && ($current_page + 5) >= $total_pages && $total_pages > 9) // 900...X..999
    {
        $start = $total_pages - $length;
        $end = $total_pages;
    }
    elseif ($current_page < 5 && $total_pages > 9)  // 1...X..10...
    {
        $start = 1;
        $end = $length;
    }
?>
<div class="dataTables_paginate paging_full_numbers">
    <a class="first paginate_button" href="<?=$page->url($first_page)?>"><?=i18n::get('First')?></a>
    <? if($previous_page): ?><a class="previous paginate_button" href="<?=$page->url($previous_page)?>">&nbsp;</a><? endif;?>
    <span>
        <?php for ($i = $start; $i <= $end; $i++): ?>
        <a class="paginate_button <?if ($i == $current_page):?>paginate_active<?endif;?>" href="<?=$page->url($i)?>"><?=$i?></a>
        <?php endfor ?>
    </span>
    <? if($next_page): ?><a class="next paginate_button" href="<?=$page->url($next_page)?>">&nbsp;</a><? endif;?>
    <a class="last paginate_button" href="<?=$page->url($total_pages)?>"><?=i18n::get('Last')?></a>
</div>