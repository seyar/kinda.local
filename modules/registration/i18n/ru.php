<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    'Registration' => 'Регистрация',

    'email' => 'Email',
    'name' => 'ФИО',
    'Name' => 'Имя',
    'password_confirm' => 'Подтверждение пароля',
    'password confirm' => 'Подтверждение пароля',
    'password' => 'Пароль',
    'mobphone' => 'Мобильный телефон',
    'address' => 'Адрес',
    'comments' => 'Комментарии',

    'Last login' => 'Последний логин',

    'You have registered succesfully. Check your email for the letter with a password' => 'Вы успешно зарегистрированы.<br/> на ваш email было отправлено письмо с паролем',
    'Check your email for the letter with a password resotre link' => 'На ваш email было отправлено письмо с ссылкой для восстановления пароля',
    'Check your email for the letter with a new password' => 'На ваш email было отправлено письмо с новым паролем',
    'The email address already exists' => 'Такой email уже существует в системе',
    'User already exists' => 'Пользователь уже существует в системе',
    'User not found' => 'Пользователь не найден',
    'send mail error' => 'ошибка отправки письма',
    'Invalid email' => 'Неверный почтовый адрес',

    'Password restore' => 'Восстановление пароля',
    'Password was changed' => 'Пароль изменен',
    'Name was changed' => 'Имя изменено',
    'Email was changed' => 'Почта изменена',
    'Password was changed' => 'Пароль изменен',
    'Profile' => 'Редактирование личных данных',
    'Add new address' => 'Добавление нового адреса',
    'User Profile' => 'Профиль пользователя',
    'Empty pass' => 'Введите пароль',
    'Old password is incorrect' => 'Старый пароль неверный',
    'Password and confirm not match' => 'Пароль и копия пароля не совпадает',

    'Registration Descr Short' => 'Данный раздел позволяет управлять зарегистрированными пользователями сайта, организуя их личный кабинет.',
    'Registration Descr Full' => 'Данный раздел позволяет управлять зарегистрированными пользователями сайта, организуя их личный кабинет.
                    <br />Так же в данном разделе «Пользователи» содержится полная база зарегистрированных пользователей. Администратор может быстро удалить, изменить данные или закрыть доступ определенным пользователям.<br />
                    <br />Регистрация пользователей на сайте может использоваться для нескольких целей:
                    <span style="display:block;padding-left: 10px">&bull; определенная группа пользователей может быть наделена правами для редактирования каких-либо разделов через доступ к административному разделу сайта;</span>
                    <span style="display:block;padding-left: 10px">&bull; управление e-mail-рассылкой пользователям сайта и подпиской на нее;</span>
                    <span style="display:block;padding-left: 10px">&bull; наделение определенной группы пользователей правами просмотра закрытых разделов</span>
                    <span style="display:block;padding-left: 10px">&bull; и т.п.</span>',
    
    'username' => 'Логин',
    'fullname' => 'Имя',
    'Last Login' => 'Последний логин',
    
    'Your account succesfully activated' => 'Ваш аккаунт успешно активирован',
    'Code is incorrect' => 'Пароль восстановления неверный',
    
    'Genarate Invite' => 'Сгенерировать код',
    'Account not activated' => 'Аккаунт не активирован',
);
?> 