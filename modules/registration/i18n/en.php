<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    'Registration Descr Short' => 'This section allows you to manage registered users of the site, organizing their personal study. ',
    'Registration Descr Full' => 'This section allows you to manage registered users of the site, organizing their personal study.
                        <br />Also in this section, "Community" contains a full database of registered users. The administrator can quickly delete, modify data or to block access to specific users.<br />
                        <br />Registration on the site may be used for several purposes:<br />
                        &bull; a certain group of users can be given the right to edit any of the sections through access to the administrative section;<br />
                        &bull; Office e-mail-mailing users of the site and subscribed to it;<br />
                        &bull; endowed with a certain group of users viewing rights of private partitions<br />
                        &bull; etc.</span>',
);
?>