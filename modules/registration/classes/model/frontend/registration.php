<?php

defined('SYSPATH') OR die('No direct access allowed.');
/** 
 * @version $Id: v 0.1 22.04.2010 - 10:31:11 Exp $
 *
 * Project:     golden
 * File:        registrationfrontend.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_Frontend_Registration extends Model
{

    static public function check_status()
    {        
        if (!Session::instance()->get('authorized'))
        {
//            Request::instance()->redirect('/registration/error');
            Request::instance()->redirect('/registration/');
        }
    }

    static function init()
    {
        $user_info = Session::instance()->get('authorized', array());
        
        if (Session::instance()->get('user_id', NULL))
        {
            Kohana_Controller_Quicky::$intermediate_vars['user_info'] = $user_info;
        }
        else
        {
            Kohana_Controller_Quicky::$intermediate_vars['user_info'] = array();
        }
    }

    public static function update1($data, $id)
    {
        DB::update('site_users')->set($data)->where('id', '=', $id)->execute();
    }

    public function update()
    {
        $post = array();
        $post['current_pass'] = Arr::get($_POST, 'current_pass', NULL);
        $post['new_pass'] = Arr::get($_POST, 'new_pass', NULL);
        $post['new_pass2'] = Arr::get($_POST, 'new_pass2', NULL);

        $post['name'] = Arr::get($_POST, 'name', NULL);

        $authorized = Session::instance()->get('authorized');
        $email = $authorized['email'];

        if ($post['new_pass'] == '')
        {
            $this->view->assign('message', Model_Frontend_Registration::message(I18n::get('Empty pass'), 'err'));
            return FALSE;
        }

        // if user exists
        if ($post['new_pass'] == $post['new_pass']
                && $user = Model_Registration::get_items(NULL, $email, $post['current_pass']))
        {
            $query = DB::update('site_users')
                            ->set(
                                    array(
                                        'pass' => ($post['new_pass'] ? sha1($post['new_pass']) : Db::expr('`pass`')),
                                        'name' => ($post['name'] ? $post['name'] : Db::expr('`name`')),
                                    )
                            )
                            ->where('id', '=', Session::instance()->get('user_id'))
            ;

            $query->execute();

            $this->view->assign('message', Model_Frontend_Registration::message(I18n::get('Password was changed'), 'ok'));

            if ($post['name'])
            {
                $authorized['name'] = $post['name'];
                Session::instance()->set('authorized', $authorized);
            }

            return TRUE;
        }
        else
        {
            $this->view->assign('message', Model_Frontend_Registration::message(I18n::get('User not found'), 'err'));
            return FALSE;
        }
    }

    public static function passupdate()
    {
        $post = array();
        $post['current_pass'] = Arr::get($_POST, 'current_pass', NULL);
        $post['new_pass'] = Arr::get($_POST, 'new_pass', NULL);
        $post['new_pass2'] = Arr::get($_POST, 'new_pass2', NULL);

        $authorized = Session::instance()->get('authorized');
        $email = $authorized['email'];

        if ($post['new_pass']!=$post['new_pass2'])
        {
            Messages::add(I18n::get('Password and confirm not match'));
            return;
        }
        
        // if user exists
        if ($user = Model_Registration::get_items(NULL, $email, $post['current_pass']))
        {
            $query = DB::update('site_users')
                            ->set(
                                    array(
                                        'pass' => ($post['new_pass'] ? sha1($post['new_pass']) : Db::expr('`pass`')),
                                    )
                            )
                            ->where('id', '=', Session::instance()->get('user_id'))
            ;

            $query->execute();
            Messages::add(I18n::get('Password was changed'),'ok');

            Session::instance()->set('authorized', $authorized);
        }
        else
        {
            Messages::add(I18n::get('Old password is incorrect'),'err');
        }
    }

    public static function nameupdate()
    {

        $post = array();
        $post['name'] = Arr::get($_POST, 'name', NULL);

        $authorized = Session::instance()->get('authorized');        
        $email = $authorized['email'];
        // if user exists        
        if ($user = Model_Registration::get_items(NULL, $email))
        {
            $query = DB::update('site_users')
                            ->set(
                                    array(
                                        'name' => ($post['name'] ? $post['name'] : Db::expr('`name`')),
                                    )
                            )
                            ->where('id', '=', Session::instance()->get('user_id'))
            ;

            $query->execute();
            Messages::add(I18n::get('Name was changed'),'ok');


            $authorized['name'] = $post['name'];            
            Session::instance()->set('authorized', $authorized);
        }
        else
        {
            Messages::add(I18n::get('User not found'),'err');
        }
    }

    public static function emailupdate()
    {
        $post = array();
        $post['email'] = Arr::get($_POST, 'email', NULL);

        $authorized = Session::instance()->get('authorized');
        $email = $authorized['email'];

        // if user exists
        if ($user = Model_Registration::get_items(NULL, $email))
        {
            $query = DB::update('site_users')
                            ->set(
                                    array(
                                        'email' => ($post['email'] ? $post['email'] : Db::expr('`name`')),
                                    )
                            )
                            ->where('id', '=', Session::instance()->get('user_id'))
            ;

            $query->execute();
            Messages::add(I18n::get('Email was changed'),'ok');


            $authorized['email'] = $post['email'];
            Session::instance()->set('authorized', $authorized);
        }
        else
        {
            Messages::add(I18n::get('User not found'),'err');
        }
    }

    public static function save_item()
    {
        if( !$post = Model_Frontend_Registration::validate_post_save() )
            return false;

        // Generate Unique Password
        $post['password'] = !isset($post['password']) || empty($post['password']) ? substr(md5(rand(0,9999).'sdfasdfas'), 0, 6) : $post['password'];
        if(!IN_PRODUCTION)
            Kohana_Log::instance()->add('pass', print_r($post['password'],1) );
        // if email exists
        if ($user = Model_Registration::get_items(NULL, $post['email']))
        {
            Messages::add(I18n::get('User already exists'),'err');
            return false;
        }
        
        list($id, $total_rows) = DB::insert('site_users', array('email', 'name', 'pass', 'user_last_login', 'profile','status'))
                        ->values(
                            array(
                                $post['email']
                                ,$post['name']
                                ,sha1($post['password'])
                                ,DB::expr('NOW()')
                                ,Kohana::config('profile')->default_profile
                                ,0
                            )
                        )
                        ->execute()
        ;
        // логиним
//        if (!Model_Frontend_Registration::login($post['email'], $post['password']))
//        {
//            Messages::add(I18n::get('Login error'));
//            return false;
//        }

        // обновляем сешн айди
        if (!Model_Registration::edit_item($id, array('user_last_session_id' => session_id())))
        {
            Messages::add(I18n::get('Session update error'),'err');
            return false;
        }

        // отправляем письмецо
        $search = array('%site%', '%email%', '%pass%',);
        $replace = array('http://' . $_SERVER['HTTP_HOST'], $post['email'], $post['password']);
        $filename = Kohana::config('registration')->path . 'email_registration.tpl';
        $message = str_replace($search, $replace, file_get_contents($filename));

        if(!Email::send(
                        $post['email'], array(Kohana::config('registration')->send_from, $_SERVER['HTTP_HOST']), Kohana::config('registration')->registration_subject, $message, TRUE
        ))
        {
            Kohana_log::instance()->add('email send error', Kohana::debug(Kohana::config('feedback')->send_from
                            , $post['email']
                            , Kohana::config('registration')->registration_subject
                            , $message));
        }

        return $id;
    }

    static function info($user_id = NULL)
    {
        $return = DB::select('id', 'name', 'email', 'user_last_login', 'user_last_session_id', 'status', 'avatar', 'profile')
                        ->from('site_users')
                        ->where('id', '=', $user_id)
                        ->cached()
                        ->execute()
                        ->current()
        ;

        if ($return)
            return $return;
        else
            return array();
    }

    static function get_customer_id()
    {
        $return = DB::select(DB::expr('COUNT(*) as mycount'))->from('site_users')->execute()->get('mycount');
        if ($return)
            return ($return + 1);
        else
            return false;
    }

    static function validate_post_save()
    {
        $keys = array(
            'email', 'name', 'phone','country', 'district', 'city', 'address', 'comment'
        );

        $params = Arr::extract($_POST, $keys, '');
        $post = Validate::factory($params)

            ->rule('email', 'not_empty')
            ->rule('email', 'email')
            ->rule('name', 'not_empty')
            ->rule('phone', 'not_empty')
            ->rule('country', 'not_empty')
            ->rule('district', 'not_empty')
            ->rule('city', 'not_empty')
//            ->rule('password', 'not_empty')
//            ->rule('password_confirm', 'matches', array('password'))
//            ->rule('password_confirm', 'not_empty')
            ->filter('username', 'trim')
            ->filter('email', 'trim')
//            ->filter('password', 'trim')
//            ->filter('password_confirm', 'trim')
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            Messages::add($post->errors('validate'),'err');
        }
    }

    static function validate_post_login()
    {
        $keys = array(
            'email_login', 'pass_login',
        );

        $params = Arr::extract($_POST, $keys, '');

        $post = Validate::factory($params)
                        ->rule('email_login', 'not_empty')
                        ->rule('pass_login', 'not_empty')
                        ->rule('pass_login', 'min_length', array(4))
        ;

        if ($post->check())
        {
            return $params;
        }
        else
        {
            Messages::add($post->errors('validate'),'err');
        }
    }

    function login($email, $pass)
    {
        $user = false;

        if (!empty($email) &&  !empty($pass))
            $user = Model_Registration::get_items(NULL, $email, $pass, NULL, 1);
        
        if ($user)
        {
            Session::instance()->set('authorized', array('email' => $email, 'name' => $user['name'], 'id' => $user['id'], 'profile' => $user['profile']));
            Session::instance()->set('user_id', $user['id']);
            return true;
        }
        
        if (!empty($email) &&  !empty($pass))
            $userNotActivated = Model_Registration::get_items(NULL, $email, $pass);
        
        if ($userNotActivated)
        {
            Messages::add(I18n::get('Account not activated'),'err');
            return false;
        }
        
        Messages::add(I18n::get('User not found'),'err');
        return false;
    }

    public function send_code()
    {
        if (!$user = Model_Registration::get_items(NULL, $_POST['email']))
        {
            Messages::add(i18n::get('User not found'),'err');
            return false;
        };

        $code = md5('asdf' . rand(0, 932993) . 'sss' . rand(0, 9329923));
        Model_Registration::edit_item($user['id'], array('lost_code' => $code));
        
        // отправляем письмецо
        $search = array('%site%', '%email%', '%pass%',);
        $replace = array($_SERVER['HTTP_HOST'], $_POST['email'], 'http://' . $_SERVER['HTTP_HOST'] . '/registration/sp/' . $code);
        $filename = Kohana::config('registration')->path . 'email_lost_code.tpl';
        $message = str_replace($search, $replace, file_get_contents($filename));
        
        if (!$res = Email::send($_POST['email']
                        , array(Kohana::config('registration')->send_from, $_SERVER['HTTP_HOST'])
                        , Kohana::config('registration')->forgot_subject
                        , $message
                        , TRUE
        ))
        {
            Messages::add(i18n::get('send mail error'),'err');
            return false;
        }

        return 1;
    }

    public function send_pass($code)
    {
        // находим юзера по коду
        if (!$user = Model_Registration::get_items(NULL, NULL, NULL, $code))
        {
            Messages::add(i18n::get('User not found'),"err");
            return false;
        };
        // обновляем его пароль
        $pass = substr(md5(rand(0,2222).'dddd'), 0, 6);
        Model_Registration::edit_item($user['id'], array('pass' => sha1($pass)));

        // шлем письмецо с паролем на емеил
        $search = array('%site%', '%email%', '%pass%',);
        $replace = array($_SERVER['HTTP_HOST'], $user['email'], $pass);
        $filename = Kohana::config('registration')->path . 'email_new_pass.tpl';
        $message = str_replace($search, $replace, file_get_contents($filename));

        if (!$res = Email::send($user['email']
                        , array(Kohana::config('registration')->send_from,$_SERVER['HTTP_HOST'])
                        , Kohana::config('registration')->forgot_subject
                        , $message
                        , TRUE
        ))
        {
            Messages::add(i18n::get('send mail error'),'err');
            return false;
        }

        // сотрем код чтоб в другой раз не перетерли пароль
        Model_Registration::edit_item($user['id'], array('lost_code' => ''));

        return 1;
    }

}

?>