<?php

defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 22.04.2010 - 10:31:11 Exp $
 *
 * Project:     golden
 * File:        registrationfrontend.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_Frontend_Profile_Regular extends Model
{

    static public function add_bookmark($user_id=NULL)
    {
        Db::insert('site_users_bookmarks')
                ->columns(array('user_id', 'title', 'url'))
                ->values(array($user_id, Arr::get($_GET, 'title', 'Bookmark without Title'), Arr::get($_GET, 'url', '#')))
                ->execute();
    }

    static public function get_bookmarks($user_id=NULL)
    {
        return Db::select()->from('site_users_bookmarks')
                ->where('user_id', '=', $user_id)
                ->order_by('created', 'DESC')
                ->execute()
                ->as_array();
    }

    static public function delete_bookmarks($user_id=NULL)
    {
        Db::delete('site_users_bookmarks')
                ->where('user_id', '=', $user_id)
                ->and_where('id', 'IN', Db::expr("(".implode(',', array_keys($_POST['bkmrk'])).")"))
                ->execute()
        ;
    }

    static public function get_comments($user_id=NULL)
    {
        return Db::select()->from('comments')
                ->where('user_id', '=', $user_id)
                ->order_by('created_at', 'DESC')
                ->execute()
                ->as_array();
    }

}

?>