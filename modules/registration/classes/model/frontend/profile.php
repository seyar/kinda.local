<?php

defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 22.04.2010 - 10:31:11 Exp $
 *
 * Project:     golden
 * File:        registrationfrontend.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_Frontend_Profile extends Model
{
    static public function avatar_url($user_id=NULL)
    {
        return MEDIAWEBPATH.Controller_Frontend_Profile::AVATARDIR.$user_id.'.jpg';
    }

    static public function avatar_upload($user_id)
    {
        $validate = Validate::factory($_FILES);

        $validate->rules('picture', array('Upload::valid' => array(),
            'Upload::not_empty' => array(),
            'Upload::type' => array('Upload::type' => array('jpeg', 'jpg', 'png', 'gif', 'bmp')),
            'Upload::size' => array('1M'))
        );

        if ($validate->check())
        {

            if (!file_exists(MEDIAPATH.Controller_Frontend_Profile::AVATARDIR))
                mkdir(MEDIAPATH.Controller_Frontend_Profile::AVATARDIR);


            $filename = Upload::save($_FILES['picture'], $user_id.'_orig.png'
                    , MEDIAPATH.Controller_Frontend_Profile::AVATARDIR, 0777);

            Image::factory($filename)
                    ->resize(Kohana::config('profile')->avatar_size['height'], Kohana::config('profile')->avatar_size['width'], Image::AUTO)
                    ->save(MEDIAPATH.Controller_Frontend_Profile::AVATARDIR . $user_id.'.jpg', 100);

        }
        else
        {
            Messages::add($validate->errors('upload'));
        }

    }

}

?>