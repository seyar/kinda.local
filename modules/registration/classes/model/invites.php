<?php

defined('SYSPATH') OR die('No direct access allowed.');

/**
 * @version $Id: v 0.1 Jul 27, 2011 - 12:08:02 AM Exp $
 *
 * Project:     kinda
 * File:        invites.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://seyar.org.ua * 
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
class Model_Invites extends Model_Registration
{
    /**
     * generate code XdXDdxX
     * 
     * @return string 
     */
    public function generateInvite()
    {
        return Text::random();
    }
    
    /**
     * check if invite already used
     * 
     * @return bool 
     */
    public function checkInvite()
    {
        return TRUE;
    }
    
    /**
     * save invite code for use in future
     * @return bool
     */
    public function saveInvite()
    {
       return 1; 
    }
}

?>