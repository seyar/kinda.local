<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 21.04.2010 - 16:11:01 Exp $
 *
 * Project:     golden
 * File:        registration.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_Registration extends Model
{

    static public function get_items($id = NULL, $email = NULL, $pass = NULL, $code = NULL, $status = NULL)
    {
        $query = DB::select()
                        ->from('site_users')
        ;

        if (is_numeric($id))
        {
            $query->where('id', '=', $id);
        }
        elseif ($email && $pass)
        {
            $query->where('email', '=', trim($email));
            $query->and_where('pass', '=', sha1($pass));
        }
        elseif ($email)
        {
            $query->where('email', '=', trim($email));
        }
        elseif ($code)
        {
            $query->where('lost_code', '=', $code);
        }

        if( isset($status) )
        {
            $query->where('status', '=', $status);
        }

        $result = $query->execute();

        if ($result->count() == 0)
            return false;
        else
        {
            if ($id || $email || $pass || $code)
                return $result->current();
            else
                return $result->as_array('id');
        }
    }

    static public function edit_item($id, $data = array())
    {
//        Controller_Registration::$fields;
        //$post = Model_Registration::validate_post_save();

        if (is_numeric($id))
        {
            DB::update('site_users')
                    ->set($data)
                    ->where('id', '=', $id)
                    ->execute()
            ;
            return true;
        }
        return false;
    }

    static public function save($id)
    {
        if ($post = Model_Registration::validate())
        {
            if (is_numeric($id))
            {
                DB::update('site_users')
                        ->set(
                                $post
                            )
                        ->where('id', '=', $id)
                        ->execute()
                ;


                /* Extended Profile method */

                if (get_class() . '_Profile_' . $post['profile']
                        && method_exists(get_class() . '_Profile_' . $post['profile'], __FUNCTION__)
                )
                {
                    $ext_class = get_class() . '_Profile_' . $post['profile'];
                    $ext_method =  __FUNCTION__;
                    call_user_func(array($ext_class, $ext_method), $id, $post);
                }
                /* /Extended Profile method */

                return true;
            }
        }
        return false;
    }

    static public function validate()
    {
        $keys = array(
            'name', 'email',
            'profile', 'status',
        );

        $params = Arr::extract($_POST, $keys, '');

        $post = Validate::factory($params)
                        ->rule('name', 'not_empty')
                        ->rule('email', 'not_empty')
                        ->rule('email', 'email')
                        ->rule('profile', 'not_empty')
                        ->rule('status', 'not_empty')
        ;
        if ($post->check())
        {
            return $params;
        }
        else
        {
            Messages::add($post->errors('validate'));
        }
    }

    static public function changeStatus($id)
    {
        DB::update('site_users')
                ->set(array('status' => DB::expr('!status')))
                ->where('id', '=', $id)
                ->execute()
        ;
        return TRUE;
    }

    static public function delete($id=NULL)
    {
        if (!$id)
            return;

        if (is_array($id))
        {
            foreach($id as $user_id)
            {
                Model_Registration::delete($user_id);
            }

            return;
        }

        /* Extended Profile method */

        $user_info = Model_Registration::get_items($id);

        if (get_class() . '_Profile_' . $user_info['profile']
                && method_exists(get_class() . '_Profile_' . $user_info['profile'], __FUNCTION__)
        )
        {
            $ext_class = get_class() . '_Profile_' . $user_info['profile'];
            $ext_method =  __FUNCTION__;

            //$ext_class::$ext_method($id, $user_info);
            call_user_func(array($ext_class, $ext_method), $id, $user_info);
        }
        /* /Extended Profile method */

        DB::delete('site_users')
                ->where('id', '=', $id)
                ->execute()
        ;

        return TRUE;
    }
    
    static public function factory($name, $db = NULL)
    {
        return parent::factory($name, $db);
    }
}

?>
