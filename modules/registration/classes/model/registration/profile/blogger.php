<?php

defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 21.04.2010 - 16:11:01 Exp $
 *
 * Project:     golden
 * File:        registration.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_Registration_Profile_Blogger extends Model
{

    static function save($id = NULL, Array $post = array())
    {

        $user_info = Db::select('add_info')->from('site_users')->where('id', '=', $id)->execute()->current();

        if ($user_info['add_info'])
        {
            $user_info=(array)json_decode($user_info['add_info']);

            if ($user_info['publicationstypes_id'])
            {
                // ok, pub_type exist
                return;
            }
        }
        // create publ_type

        $publication_type_info = Model_Publications::publ_types_load(Kohana::config('profile')->blog_publicationstypes_id);

        list($insert_id, $total_rows) = Db::insert('publicationstypes')->columns(
                array('name', 'parent_id', 'parent_url', 'url', 'status', 'template', 'ser_priFields', 'have_addFields', 'ser_addFields')
            )
            ->values(
                    array(
                        str_replace('%USER_NAME%', $post['name'], Kohana::config('profile')->blog_name_template),
                        Kohana::config('profile')->blog_publicationstypes_id,
                        $publication_type_info['parent_url'] . $publication_type_info['url'].'/',
                        urlencode(Controller_Admin::to_url($post['name'])),
                        1,
                        Kohana::config('profile')->blog_publicationstypes_template,
                        Kohana::config('profile')->blog_publicationstypes_ser_priFields,
                        1,
                        'a:1:{i:110;a:3:{s:4:"name";s:7:"user_id";s:4:"type";s:3:"int";s:4:"desc";s:2:"ID";}}', // SER ARRAY
                    )
            )->execute()
        ;

        Db::update('site_users')
            ->set(
                    array('add_info'=> json_encode(array("publicationstypes_id"=>$insert_id)) )
                )
            ->where('id', '=', $id)
            ->execute();

    }

    static function delete($id = NULL, $user_info = array())
    {
        if (!$user_info)
            $user_info = Db::select('add_info')->from('site_users')->where('id', '=', $id)->execute()->current();

        if ($user_info['add_info'])
        {
            $user_info=(array)json_decode($user_info['add_info']);

            if ($user_info['publicationstypes_id'])
            {
                // pub_type exist, so delete them

                Db::delete('publicationstypes')->where('id', '=', $user_info['publicationstypes_id'])->execute();
            }
        }
    }
}
