<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 21.04.2010 - 16:10:55 Exp $
 *
 * Project:     golden
 * File:        registration.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_Registration extends Controller_AdminModule
{
    public $template = 'registration';
    public $module_name = 'registration';
    public $module_title = 'Registration';
    public $module_desc_short = 'Registration Descr Short';
    public $module_desc_full = 'Registration Descr Full';

    public function before()
    {
        parent::before();
        $this->model = new Model_Registration();
        $this->template->current_lang = $this->lang;
    }

    public function action_index()
    {
        list($this->template->pagination, $this->template->rows) = Admin::model_pagination(NULL, 'site_users',
                array('id', 'name', 'email', 'status', 'user_last_login', 'profile')
            );
    }

    public function action_edit()
    {
        $this->template = View::factory('registration_edit');

        $this->template->obj = Model_Registration::get_items($this->request->param('id'));
        $this->template->profile_variants = Kohana::config('profile')->profile_variants;

        $this->template->messages = Messages::get();
//        $this->template->langs = $this->get_languages_array();
    }

    public function action_update()
    {
        if ($id = Model_Registration::save($this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller . '/' . $this->request->param('id') . '/edit');
        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_save()
    {

        if ($id = Model_Registration::save($this->request->param('id')))
        {
            $this->redirect_to_controller($this->request->controller);

        }
        else
        {
            $this->action_edit();
        }
    }

    public function action_active()
    {
        Model_Registration::changeStatus($this->request->param('id'));
        
        // Childs modules proccessing (Shop, ...etc)
        if( Kohana::config('registration.child_class') && class_exists( Kohana::config('registration.child_class') ) )
        {
            call_user_func( array(Kohana::config('registration.child_class'), 'changeStatus'), $this->request->param('id') );
        }
        $this->redirect_to_controller($this->request->controller);
    }

    public function action_delete()
    {
        if( $this->request->param('id') )
            Model_Registration::delete($this->request->param('id'));
        elseif( isset($_POST['chk']) )
            Model_Registration::delete($_POST['chk']);
        
        /* delete from child module */
        $shopDelete = Kohana::config('registration')->get('child_module_delete');
        $shopDeleteList = Kohana::config('registration')->get('child_module_delete_list');
        if( $shopDelete && is_callable( $shopDelete ) )
        {
            if( $this->request->param('id') )
                call_user_func($shopDelete, $this->request->param('id') );
            elseif( isset($_POST['chk']) )
                call_user_func($shopDeleteList, $_POST['chk']);
        }
        /**/
        $this->redirect_to_controller($this->request->controller);
    }
    
    public function action_generateInvite()
    {
        $res = $this->model->factory('Invites')->generateInvite();
        die(print($res));
    }

}