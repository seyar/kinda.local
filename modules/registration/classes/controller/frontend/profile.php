<?php

defined('SYSPATH') OR die('No direct access allowed.');
/** @version $Id: v 0.1 22.04.2010 - 10:31:05 Exp $
 *
 * Project:     golden
 * File:        registrationfrontend.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Yuriy KLinkov <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_Frontend_Profile extends Controller_Content
{

    public  $template   = 'registration_profile.tpl';
    const   AVATARDIR   = 'avatars/';
    static public $user_info  = array();

    public function before()
    {
        $obj = 'Model_Frontend_Registration';
        $method = 'check_status';
        if( method_exists( $obj, $method ) )
        {
            call_user_func( array($obj, $method) );
        }

        self::$user_info = Session::instance()->get('authorized');
        parent::before();
    }

    public function action_ext()
    {
        $user = Session::instance()->get('authorized');

        if (class_exists(get_class($this) . '_' . $user['profile'])
                && method_exists(get_class($this) . '_' . $user['profile'], 'action_' . Request::instance()->param('param'))
        )
        {
            echo Request::factory('/profile_ext/' . Request::instance()->controller . '_' . $user['profile']
                    . '/'. Request::instance()->param('param'))->execute();
            die;
        }
        
    }

    public function action_index()
    {        
        $this->view->assign('user_info', Model_Frontend_Registration::info(self::$user_info['id']));
        $this->page_info = array( 'page_title'=>I18n::get('User Profile') );

        if (class_exists(get_class($this) . '_' . self::$user_info['profile'])
                && method_exists(get_class($this) . '_' . self::$user_info['profile'], __FUNCTION__)
            )
        {

            $ext_class = get_class() . '_' . self::$user_info['profile'];
            $ext_method =  __FUNCTION__;
            
            $uai = array();

            try
            {
                @$uai = call_user_func(array($ext_class, $ext_method), $id, $post);
            }
            catch (ErrorException $e)
            {
                throw new Exception('add func '.$ext_class.'::'.$ext_method);
            }

            $this->view->assign('user_add_info', $uai);
        }
        return;
    }

    public function action_logout()
    {
        Session::instance()->delete('authorized');
        Session::instance()->delete('user_id');
        $this->request->redirect('/');
    }

    public function action_passupdate()
    {
//        $this->template = 'profile_change_password.tpl';
        if (empty($_POST)) return;

        Model_Frontend_Registration::passupdate();
        
        if( !Messages::has() ) die('ok');
        else
        {
//            $this->request->redirect( Route::get( 'profile_front' )->uri( array('action' => '', 'lang' => '') ) );
            $this->request->redirect( '/shop/order/address' );
        }

    }

    public function action_emailupdate()
    {
        $this->template = 'profile_change_email.tpl';
        if (empty($_POST)) return;


        Model_Frontend_Registration::emailupdate();

        die('ok');
    }

    public function action_nameupdate()
    {
        $this->template = 'profile_change_name.tpl';

        if (empty($_POST)) return;

        Model_Frontend_Registration::nameupdate();

        if( Request::$is_ajax )
            die('ok');
        else
            $this->request->redirect( Route::get('profile_front')->uri(array('lang'=>'','action'=>'')) );
    }

    public function action_avatarupdate()
    {
        if (!Kohana::config('profile')->members_avatar)
            Request::instance()->redirect('/profile');


        $this->template = 'profile.tpl';

        if (!empty($_FILES))

            Model_Frontend_Profile::avatar_upload(self::$user_info['id']);

        Request::instance()->redirect('/profile');
    }

}
