<?php

defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 22.04.2010 - 10:31:05 Exp $
 *
 * Project:     golden
 * File:        registrationfrontend.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Yuriy KLinkov <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_Frontend_Profile_Regular extends Controller_Content
{
    public $template = 'profile.tpl';
    static public $user_info  = array();

    public function before()
    {
        self::$user_info = Session::instance()->get('authorized');
        parent::before();
    }

    public function action_index()
    {
        
    }
    public function action_addbookmark()
    {
        if (!empty($_GET) && !empty($_SERVER['HTTP_REFERER']))
            Model_Frontend_Profile_Regular::add_bookmark(self::$user_info['id']);
        Request::instance()->redirect($_SERVER['HTTP_REFERER']);
    }

    public function action_bookmarks()
    {
        $this->view->assign('user_name', $user_info['name']);
        $this->view->assign('user_bookmarks', Model_Frontend_Profile_Regular::get_bookmarks(self::$user_info['id']));
        $this->page_info = array( 'page_title'=>I18n::get('Bookmarks') );

        $this->template = 'profile_bookmarks.tpl';
    }

    public function action_delbookmarks()
    {
        Model_Frontend_Profile_Regular::delete_bookmarks(self::$user_info['id']);
        Request::instance()->redirect('profile/ext/bookmarks');
    }

    public function action_comments()
    {
        $this->view->assign('user_name', $user_info['name']);
        $this->view->assign('user_comments', Model_Frontend_Profile_Regular::get_comments(self::$user_info['id']));
        $this->page_info = array( 'page_title'=>I18n::get('Comments') );

        $this->template = 'profile_comments.tpl';
    }

}