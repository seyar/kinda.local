<?php

defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 22.04.2010 - 10:31:05 Exp $
 *
 * Project:     golden
 * File:        registrationfrontend.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_Frontend_Registration extends Controller_Content
{

    public $template = 'registration.tpl';

    public function before()
    {
        parent::before();
        $this->model = new Model_Frontend_Registration();
    }

    public function action_index()
    {
        $this->page_info = array('page_title' => i18n::get('Registration'));
    }

    public function action_add()
    {
		Kohana_log::instance()->add('register user', "*********************" );
        $user_id = Model_Frontend_Registration::save_item();

        if (!$user_id && Messages::has())
        {
            return false;
        }

        $data = array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'date_create' => DB::expr('NOW()'),
            'status' => 0,
            'id' => $user_id,
            'price_category_id' => 1,
        );

        // Childs modules proccessing (Shop, ...etc)
        if(Kohana::config('registration')->get('child_module'))
        {
            $res = call_user_func(Kohana::config('registration')->get('child_module'), $data);
            if( $res != 1 )
            {
                Model_Registration::delete($user_id);
            }
        }

        if ( !Messages::has() )
        {
            Messages::add(i18n::get('You have registered succesfully. Check your email for the letter with a password'),'ok');
            Kohana_log::instance()->add('register user', "POST: ".var_export($_POST, 1) );
            Kohana_log::instance()->add('register user', "*********************" );
            $this->request->redirect('/');
        }
        else
        {
            $this->action_index();
        }
        
    }

    public function action_login_form()
    {
//        $this->template = 'registration_login.tpl';
    }

    public function action_login()
    {   
        $this->page_info = array('page_title' => i18n::get('Registration'));
        
        $result = Model_Frontend_Registration::login( trim($_POST['email_login']), trim($_POST['pass']));
        
        if ( /*!Messages::has() && */ $result)
        {
            if (!Request::$is_ajax)
            {
                if (!empty($_POST['referer']) && $_POST['referer']!='/registration/error')
                    $this->request->redirect($_POST['referer']);
                else
                    $this->request->redirect('/');
            }
            else
            {
                die('ok');
            }
        }else
        {
            $this->action_login_form();
        }
    }

    public function action_forgot()
    {
        $this->page_info = array('page_title' => I18n::get('Password restore'));
        $this->template = 'registration_forgot_password.tpl';
    }

    public function action_error()
    {
        $this->page_info = array('page_title' => I18n::get('Ooops!'));
        $this->template = 'registration_error.tpl';
    }

    public function action_send_code()
    {
        $res = $this->model->send_code();

        if (!Messages::has() && $res)
        {
//            $this->view->assign('success', I18n::get('Check your email for the letter with a password resotre link'));
            Messages::add(I18n::get('Check your email for the letter with a password resotre link'),'ok');
            $this->request->redirect('/');
//            $this->page_info = array('page_title' => I18n::get('Password restore'));
//            $this->template = 'registration_forgot_password.tpl';
        }
        else
        {
            if ($message)
            {
                $this->action_forgot();
            }
            else
                $this->request->redirect('/');
        }
    }

    public function action_sp()
    {
        $res = $this->model->send_pass($this->request->param('code'));

        if (!Messages::has() && $res)
        {
//            $this->view->assign('success', I18n::get('Check your email for the letter with a new password'), 'ok', TRUE);
            Messages::add(I18n::get('Check your email for the letter with a new password'),'ok');
            $this->request->redirect('/');

//            $this->page_info = array('page_title' => I18n::get('Password restore'));
//            $this->template = 'registration_forgot_password.tpl';
        }
        else
        {
            if ($message)
            {
                $this->action_forgot();
            }
            else
                $this->request->redirect('/');
        }
    }

}

?>
