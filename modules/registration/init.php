<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 21.04.2010 - 16:04:50 Exp $
 *
 * Project:     golden
 * File:        init.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

Route::set
        (
	    'registration',
	    'admin/registration((/<id>)/<action>)',
	    array( 'id' => '\d{1,10}', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'registration',
		    'id'	    => NULL,
		    'action'        => 'index',
                )
	)
;

// Frontend Routes
Route::set
        (
	    'registration_front',
	    '(<lang>/)registration(/<action>(/<code>))',
	    array( 'lang' => '(?:'.LANGUAGE_ROUTE.')', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_registration',
		    'action'        => 'index',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	);

Route::set
        (
	    'profile_front',
	    '(<lang>/)profile(/<action>(/<param>)(/<id>))',
	    array( 'lang' => '(?:'.LANGUAGE_ROUTE.')', 'action' => '(?:index|logout|passupdate|nameupdate|emailupdate|avatarupdate|ext)', )
        )
        ->defaults
        (
            array(
		    'controller'    => 'frontend_profile',
		    'action'        => 'index',
		    'param'         => NULL,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	);

Route::set
        (
	    'profile_front_ext',
	    '(<lang>/)profile_ext/<controller>/<action>(/<id>)',
	    array( 'lang' => '(?:'.LANGUAGE_ROUTE.')', 'id' => '\d{1,10}', )
        )
        ->defaults
        (
            array(
		    'controller'    => NULL,
		    'action'        => 'index',
		    'id'            => NULL,
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	);


if( strpos( $_SERVER["PATH_INFO"], ADMIN_PATH ) !== 0 )
    Event::add( 'system.ready', array( 'Model_Frontend_Registration', 'init' ) );
?>