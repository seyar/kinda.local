<?php

defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 21.12.2010 - 16:11:06 Exp $
 *
 * Project:     ChimeraCMS2
 * File:        profile.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Yuriy Klinkov <klinkov@gmail.com>
 */
return array
    (
    'avatar_size' => array(
        'width' => '25', 'height' => 25
    ),

    'default_profile' => 'regular',
    'profile_variants' => array('regular', 'blogger'),

    'members_avatar' => TRUE,

    'blog_publicationstypes_id' => 33,
    'blog_publicationstypes_template' => 'pubopninions',
    'blog_publicationstypes_ser_priFields' => 'a:1:{s:9:"pub_image";s:9:"pub_image";}',

    'blog_name_template' => 'Мнение %USER_NAME%',

    'blogger_image_width' => 800,
    'blogger_image_height' => 600,

    'blogger_image_prev_width' => 120,
    'blogger_image_prev_height' => 105,

);
?>