<?php

defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 21.04.2010 - 16:11:06 Exp $
 *
 * Project:     golden
 * File:        registration.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
return array
    (
    'lang' => 'ru', // Default the  language.
    'path' => dirname(__FILE__) . '/../views/', // Admin templates folder.

    'send_from' => 'noreply@' . $_SERVER['HTTP_HOST'],
    'registration_subject' => 'Регистрация на сайте ' . $_SERVER['HTTP_HOST'],
    'forgot_subject' => 'Восстановление пароля на сайте ' . $_SERVER['HTTP_HOST'],

//    'fields' => array(
//                        'email',
//                        'name',
//                        'mobphone',
//                        'address',
//                        'comments'
//                    )
    'child_class' => 'Model_Customers',
    'child_module' => array('Model_Customers', 'save'),
    'child_module_delete' => array('Model_Customers', 'delete'),
    'child_module_delete_list' => array('Model_Customers', 'delete_list'),

    'fields' => array(),    
);
?>