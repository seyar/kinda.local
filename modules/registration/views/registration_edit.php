<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 17.11.2010 - 16:47:54 Exp $
 *
 * Project:   Chimera2.local
 * File:    block_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" src="/static/admin/js/list.js"></script>

<!--floating block-->
<form method="post" action="">
    <div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
        <div class="absBlocks floatingInner">
            <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td>
                            <button class="btn blue formSave" type="button" rel="<?= $admin_path . $controller ?><?= (isset($obj['id']) ? $obj['id'] : 0) ?>/save">
                                <span><span><?= I18n::get('Save') ?></span></span></button></td>
                        <td style="width:35%">
                            <button class="btn blue formUpdate" type="button" rel="<?= $admin_path . $controller ?><?= (isset($obj['id']) ? $obj['id'] : 0) ?>/update">
                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?= $admin_path . $controller ?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

    <!--floating block-->

    <div class="rel" >

        <div class="whiteblueBg padding20px">
            <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>

            <!-- **** -->
            <table class="borderNone italic grayText2" width="100%">
                <tr>
                    <td style="width: 20%;">
						<div class="blocktitles"><?= I18n::get('Email') ?>:</div>
                        <input type="text" style="width: 90%" name="email" value="<?= (isset($obj['email']) ? $obj['email'] : $_POST['email']) ?>"/>
                    </td>
                    <td style="width: 35%;">
						<div class="blocktitles"><?= I18n::get('Name') ?>:</div>
                        <input type="text" style="width: 90%" name="name" value="<?= (isset($obj['name']) ? $obj['name'] : $_POST['name']) ?>"/>
                    </td>
                    <td style="width: 20%; padding-right: 3%;">
                        <div class="blocktitles"><?= I18n::get('Type') ?>:</div>
                        <?= Form::select('profile', array_combine($profile_variants, $profile_variants), isset($obj['profile']) ? $obj['profile'] : $_POST['profile']) ?>
                    </td>
                    <td  style="width: 15%;">
                        <div class="blocktitles"><?= I18n::get('User Status') ?>:</div>
                        <?= Form::select('status', array(0 => I18n::get('Off'), 1 => I18n::get('On')), isset($obj['status']) ? $obj['status'] : $_POST['status']) ?>
                    </td>
                </tr>
            </table>
            <!-- **** -->
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>
