<?php

defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 Jul 27, 2011 - 1:01:24 AM Exp $
 *
 * Project:     kinda
 * File:        registration_js.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://seyar.org.ua * 
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<link rel="stylesheet" type="text/css" href="/vendor/jquery/jqmodal/jqModal.css"/>
<script type="text/javascript" src="/vendor/jquery/jqmodal/jqModal.js"></script>
    
<script type="text/javascript">
    
    $(document).ready( function(){
        $('.jqmodal').jqm({overlay:0.01});
    });
    
    function generateInvite()
    {
        $.get(
            '/admin/registration/generateInvite',
            function(data)
            {
                $('.jqmodal').html(data).jqmShow();                
            }
        );
    }
    
</script>
<style type="text/css">
    .messageWindow{width: 200px;margin: 0 0 0 -100px; text-align: center;font-size: 16px; background: #fff; padding: 20px; border: 1px solid #333; -moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px; }
</style>