# --------------------------------------------------------
# Host:                         192.168.1.5
# Database:                     golden
# Server version:               5.0.27-log
# Server OS:                    redhat-linux-gnu
# HeidiSQL version:             5.0.0.3272
# Date/time:                    2010-04-27 09:04:57
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table golden.site_users
DROP TABLE IF EXISTS `site_users`;
CREATE TABLE `site_users` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`email` VARCHAR(255) NOT NULL,
	`pass` VARCHAR(42) NOT NULL,
	`user_last_login` DATETIME NOT NULL,
	`user_last_session_id` VARCHAR(255) NULL DEFAULT NULL,
	`status` SET('1','0') NOT NULL DEFAULT '1',
	`lost_code` VARCHAR(32) NULL DEFAULT NULL,
	`profile` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT;

# Data exporting was unselected.
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
