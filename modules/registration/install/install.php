<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
            'name'             => 'Registration',
            'fs_name'          => 'registration',
            'panel_serialized' => NULL,
            'useDatabase'      => true
        );
