<?php defined('SYSPATH') or die('No direct script access.');

// Catch-all route for Captcha classes to run
Route::set('captcha', '(<lang>/)captcha(/<group>)'
            , array('lang' => '(?:'.LANGUAGE_ROUTE.')',)
        )
	->defaults(array(
		'controller' => 'captcha',
		'action' => 'index',
		'group' => 'default',
                'lang'	    => LANGUAGE_ROUTE_DEFAULT,
            )
        );
