CREATE TABLE `votes_own_var` (
  `id` int(10) NOT NULL auto_increment,
  `question_id` int(10) NOT NULL,
  `text` varchar(255) default NULL,
  `points` int(10) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `FK1` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `votes_questions` (
  `id` int(10) NOT NULL auto_increment,
  `question` varchar(255) default NULL,
  `status` enum('1','0') NOT NULL default '1',
  `total_sum` int(10) NOT NULL default '0',
  `total_votes` int(10) NOT NULL default '0',
  `date` datetime NOT NULL,
  `language_id` int(11) NOT NULL default '1',
  `withOwnVar` enum('1','0') default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `votes_question_vars` (
  `id` int(11) NOT NULL auto_increment,
  `text` varchar(255) default NULL,
  `points` int(11) NOT NULL default '1',
  `sum_points` int(11) NOT NULL default '0',
  `num_votes` int(11) NOT NULL default '0',
  `questions_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_votes_question_vars_votes_questions1` (`questions_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `votes_question_vars` ADD CONSTRAINT `FK1` FOREIGN KEY (`questions_id`) REFERENCES `votes_questions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
