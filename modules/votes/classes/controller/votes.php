<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 01.03.2010 - 11:36:32 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        votes.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_Votes extends Controller_AdminModule {

    public $template		= 'votes';

    public $module_title	= 'votes';
    public $module_name	= 'votes';
    public $module_desc_short   = 'votes Descr Short';
    public $module_desc_full	= 'votes Descr Full';

    public function before()
    {
        parent::before();
	$this->model = new Model_Votes();
    }

    public function action_index()
    {

        list($rows, $page_links, $total_pages) = $this->model->getItems(NULL, $this->lang);

        list($this->template->pagination, $this->template->rows) = Admin::model_pagination(NULL, 'votes_questions',
                array('id', 'question', 'status', 'date')
            );

//        $this->template->rows = $rows;
//        $this->template->pages_list = $page_links;
//        $this->template->total_pages = $total_pages;
    }

    public function action_edit()
    {
        $this->template = View::factory('votes_edit');
        $res = $this->model->getItem( $this->request->param('id'), $this->lang );
        $this->template->row = $res;
    }

    public function action_save()
    {
        $this->model->saveItem( $this->request->param('id'), $this->lang );
        $this->redirect_to_controller( $this->request->controller );
    }

    public function action_update()
    {
        $this->model->saveItem( $this->request->param('id'), $this->lang );
        $this->redirect_to_controller($this->request->controller.'/'.$this->request->param('id').'/edit');
    }

    public function action_delete()
    {
        if ($this->request->param('id'))
            die(print($this->model->deleteItem( $this->request->param('id') )));

        elseif (count($_POST['chk']))
            $this->model->deleteItems( $_POST['chk'] ) ;
        $this->redirect_to_controller($this->request->controller);
    }

    public function action_changeStatus()
    {
        if ($this->request->param('id'))
            $this->model->changeStatus( $this->request->param('id') );

        $this->redirect_to_controller( $this->request->controller );
    }

    public function action_getOwns()
    {
        if ($this->request->param('id'))
        {
            $res = $this->model->getOwnVars( $this->request->param('id'));
            //$res = explode('||', $res['text']);
            die(print( json_encode($res) ));
        }
    }

    public function action_deleteOwnVar()
    {
        if ($this->request->param('id'))
            die(print($this->model->deleteOwnVar( $this->request->param('id') )));
    }

    public function action_panel()
    {
        Db::update('modules')->set(
                array(
                    'panel_serialized' => serialize(
                        array(
                            'info'  =>  array(  'name'  => 'Votes',
                                                'link'  => ADMIN_PATH.'modules/votes',
                                                'image' => ADMIN_PATH.'modules/votes/logo'
                                            ),
                            'links' =>  array(
                                                array( 'name'=>'Create',         'value' => '/0/edit'),
                                                array( 'name'=>'List',     'value' => '/'),
                                            ),
                        )
                    ), // serialize
                ) // array
            )
                ->where('fs_name', '=', MODULE_NAME)
                ->execute();
        exit();
    }

    public function action_logo()
    {
          // Create the filename
          $file = Kohana::find_file('media', 'votes', 'png');

          if ( ! is_file($file))
          {
              throw new Kohana_Exception('Image does not exist');
          }

          // Set the mime type
          $this->request->headers['Content-Type'] = File::mime($file);
          $this->request->headers['Content-length'] = filesize($file);

          // Send the set headers to the browser
          $this->request->send_headers();

          // Send the file
          $img = @ fopen($file, 'rb');
          if ($img) {
              fpassthru($img);
              exit;
          }
    }
}

?>