<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @version $Id: v 0.1 02.03.2010 - 16:11:48 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        votesFrontend.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_Frontend_Votes extends Controller_Content
{
    public $template = 'votes.tpl';

    public function before()
    {
        parent::before();
        $this->model = new Model_Frontend_Votes();
    }

    public function action_index()
    {
        $this->view->assign('res', Model_Frontend_Votes::init());
        
//        return $this->view->fetch( $this->view->template_root.$this->template);
    }

    public function action_showVoteResults()
    {
        $res['votes_results'] = $this->model->showVoteResults( $this->request->param('id'), CURRENT_LANG_ID );
        $this->view->assign('res', $res);
        die(print( $this->view->fetch( $this->view->template_root.'votes_ajax.tpl') ));
    }

    public function action_showVoteQuestions()
    {
        $res =  $this->model->showVoteQuestions( $this->request->param('id'), CURRENT_LANG_ID );
        die(print( $this->view->fetch( $this->view->template_root.'votes_ajax.tpl') ));
    }

    public function action_makeVote()
    {        
        die(print( $this->model->makeVote( $this->request->param('id') ) ));
    }
}

?>