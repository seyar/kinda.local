<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 01.03.2010 - 11:36:42 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        votes.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Model_Votes extends Model {

    static public function getItems( $status = NULL, $lang_id = 1)
    {
        // Set and save rows_per_page value
        $rows_per_page = (isset($_POST['rows_num'])) ? ((int) $_POST['rows_num']) : (int) cookie::get('rows_per_page', 10);

        cookie::set('rows_per_page', $rows_per_page);
        $_POST['rows_num'] = $rows_per_page;

        $query_c = DB::select(DB::expr('COUNT(*) AS mycount'))
                        ->from(array('votes_questions', 'vq'))
                        ->join(array('votes_question_vars', 'vqv'), 'left outer')
                        ->on('vq.id', '=', 'vqv.questions_id')
                        ->where('language_id', '=', $lang_id)
                        ->group_by('vq.id')
        ;

        $query = DB::select('vq.*', array('vqv.text', 'text'), array('vqv.points', 'points'), array('vqv.questions_id', 'questions_id'))
                        ->from(array('votes_questions', 'vq'))
                        ->join(array('votes_question_vars', 'vqv'), 'left outer')
                        ->on('vq.id', '=', 'vqv.questions_id')
                        ->where('language_id', '=', $lang_id)
                        ->order_by('date', 'desc')
                        ->group_by('vq.id')
        ;

        if ($status)
            $query->and_where('status', '=', $status);

        // Set and save current datefrom
        $date_from = (isset($_POST['search_date_from']) ? $_POST['search_date_from'] : Session::instance()->get('search_date_from', '') );

        Session::instance()->set('search_date_from', $date_from);
        $_POST['search_date_from'] = $date_from;

        if ($date_from)
        {
            $query_c->and_where(DB::expr('TO_DAYS(vq.date)'), '>=', DB::expr('TO_DAYS("' . date('Y-m-d', strtotime($date_from)) . '")'));
            $query->and_where(DB::expr('TO_DAYS(vq.date)'), '>=', DB::expr('TO_DAYS("' . date('Y-m-d', strtotime($date_from)) . '")'));
        }

        // Set and save current dateto
        $date_to = (isset($_POST['search_date_to']) ? $_POST['search_date_to'] : Session::instance()->get('search_date_to', '') );

        Session::instance()->set('search_date_to', $date_from);
        $_POST['search_date_to'] = $date_to;

        if ($date_to)
        {
            $query_c->and_where(DB::expr('TO_DAYS(vq.date)'), '<=', DB::expr('TO_DAYS("' . date('Y-m-d', strtotime($date_to)) . '")'));
            $query->and_where(DB::expr('TO_DAYS(vq.date)'), '<=', DB::expr('TO_DAYS("' . date('Y-m-d', strtotime($date_to)) . '")'));
        }

        $count = $query_c
                        ->execute()
                        ->get('mycount')
        ;

        if (isset($_POST['page']))
            $_GET['page'] = $_POST['page'];

        $pagination = Pagination::factory(array(
                    'total_items' => $count,
                    'items_per_page' => $rows_per_page,
                ));

        $query->limit($pagination->items_per_page);

        $page_links_str = $pagination->render();

        $result = $query
                ->offset($pagination->offset)
                ->execute();

        if ($result->count() == 0) 
			return array(array(), '', 0);
        else
            return array($result->as_array(), $page_links_str, $pagination->total_pages);
    }

    static public function getItem($id, $lang_id = 1)
    {
        $query = DB::select('vq.*')
                        ->from(array('votes_questions', 'vq'))
                        ->where('vq.id', '=', $id)
//            ->and_where('vq.language_id','=',$lang_id)
        ;

        $result = $query->execute();
        $return = $result->current();

        $query = DB::select('vqv.*')
                        ->from(array('votes_question_vars', 'vqv'))
                        ->where('vqv.questions_id', '=', $id)
        ;

        $result = $query->execute();
        $res = $result->as_array();
        $vars = array();
		$num_points = 0;
        foreach($res as $item)
        {
            $num_points += $item['num_votes'];
            $vars[$item['id']] = $item;
        }
        /*calculate num_points*/
        $query = DB::select('vov.id', array(DB::expr('SUM(points)'), 'num_points') )
            ->from(array('votes_own_var','vov'))
            ->where( 'vov.question_id','=',$id )
			->group_by('vov.question_id')
			->execute()
			->get('num_points')
        ;
        $num_points += $query;
        /**/
        $return['total_sum'] = $return['total_votes'] = $num_points;
        $return['vars'] = $vars;

        return $return;
    }

    function saveItem( $id = NULL, $lang_id = 1 )
    {
        if( !$id )
        {
            $date = $_POST['date'] ? $_POST['date'] : DB::expr('NOW()');
            DB::insert('votes_questions', array('question', 'date', 'withOwnVar', 'language_id')
                )
                ->values(array($_POST['question'], $date,$_POST['withOwnVar'], $lang_id) )
                ->execute()
            ;
            $id = mysql_insert_id();
            $this->save_question_vars($_POST['text'], $id);
        }
        else
        {
            DB::update('votes_questions')
                    ->set(array('question' => $_POST['question'], 'date' => $_POST['date']
                        , 'withOwnVar' => (isset($_POST['withOwnVar'])?$_POST['withOwnVar']:0), 'language_id' => $lang_id))
                ->where('id','=',$id)
                ->execute()
            ;
            $this->edit_question_vars($lang_id, $_POST['text'], $id);
        }


        return $id;
    }

    function save_question_vars($data, $question_id)
    {
        DB::delete('votes_question_vars')->where('questions_id', '=', $question_id)->execute();
        $query = DB::insert('votes_question_vars', array('text','questions_id') );

        foreach($data as $item)
        {
            if($item)
                $query->values(array($item,$question_id));
        }
        $query->execute();
        return 1;
    }

    function edit_question_vars($lang_id, $data, $question_id)
    {
        $question = Model_Votes::getItem($question_id, $lang_id);
        $i = 0;
        foreach($question['vars'] as $key => $item)
        {
            if($data[$i])
            {

                DB::update('votes_question_vars')
                    ->set(array('text'=>$data[$i]))
                    ->where('id','=',$key)
                    ->execute()
                ;
            }
            $i++;
        }

        return 1;
    }

    function deleteItem($id)
    {
        if (!$id)
            return false;
        DB::delete('votes_questions')->where('id', '=', $id)->execute();
        return 1;
    }

    function deleteItems( $array )
    {
        $query = DB::delete('votes_questions');
        if (count($array))
	{
	    $list = array_flip($array);
            $query->where('id', 'in', $list ); // ->limit(1)
	    $total_rows = $query->execute();
	}
	return true;
    }

    function changeStatus($id)
    {
        DB::query(Database::UPDATE, "UPDATE votes_questions SET status = IF(status='1', 0, 1) WHERE id = $id")
                ->execute()
            ;
        return 1;
    }

    static public function getOwnVars($id)
    {
//        $query = DB::select('vov.*', array(DB::expr('SUM(points)'),'sum_points'), array(DB::expr('GROUP_CONCAT(text SEPARATOR "||")'), 'text') )
        $query = DB::select('vov.*' )
            ->from(array('votes_own_var','vov'))
            ->where('vov.question_id','=',$id)
        ;

        $result = $query->execute();
        $return = $result->as_array();

        return $return;
    }

    public function deleteOwnVar( $id )
    {
        if (!$id)
            return false;

        $question_id = DB::select('vov.*' )
            ->from(array('votes_own_var','vov'))
            ->where('vov.id','=',$id)
            ->execute()
            ->get('question_id')
        ;

        DB::delete('votes_own_var')->where('id', '=', $id)->execute();
        DB::update('votes_questions')
            ->set(array('total_sum'=> DB::expr('total_sum-1'), 'total_votes'=> DB::expr('total_votes-1')))
            ->where('id','=',$question_id)
            ->execute();
        return 1;
    }
}
?>