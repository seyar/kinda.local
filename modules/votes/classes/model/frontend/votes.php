<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 02.03.2010 - 15:19:05 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        votesFrontend.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
class Model_Frontend_Votes extends Model
{
    static function init()
    {
        $cookie = Cookie::get('voted') ? json_decode(Cookie::get('voted')) : array();
        
        $rows = Model_Frontend_Votes::getItems( 1, $cookie, CURRENT_LANG_ID );
        $return = array();
        if( $rows)
        {
            $id = rand(0,(count($rows)-1) );
            $rows[$id]['vars'] = Model_Frontend_Votes::getVars( $rows[$id]['id'], CURRENT_LANG_ID );
            $return['votes'] = $rows[$id];
            //Kohana_Controller_Quicky::$intermediate_vars['votes'] = $rows[$id];
        }
        else
        {
            $rows = Model_Frontend_Votes::getResults( 1, $cookie, CURRENT_LANG_ID );
            $id = rand(0,(count($rows)-1) );
            if( isset($rows[$id]) ) $return['votes_results'] = $rows[$id];
//            Kohana_Controller_Quicky::$intermediate_vars['votes_results'] = $rows[$id];
        }

        return $return;
        
    }

    static public function getItems( $status = NULL, $blocked_ids = array(), $lang_id = 1 )
    {
//        $languages_id = $languages_id ? $languages_id : 1;

        $query = DB::select('vq.*', array('vqv.text','text'),array('vqv.points','points'),array('vqv.questions_id','questions_id') )
            ->from(array('votes_questions','vq'))
            ->join(array('votes_question_vars','vqv'), 'left outer')
                ->on('vq.id','=','vqv.questions_id')
            ->order_by('date', 'desc')
            ->where('vq.language_id','=',$lang_id)
            ->group_by('vq.id')
        ;

        if($status) $query->where('status','=', $status);
        if( count($blocked_ids) )
        {
            foreach($blocked_ids as $item)
            {
                $query->and_where('vq.id','!=', $item);
            }
        }

        $result = $query
                ->execute();

        if ($result->count() == 0) return array();
        else
            return $result->as_array();
    }

    static public function getResults( $status = NULL, $blocked_ids, $lang_id = 1 )
    {
        $query = DB::select('vq.*', array('vqv.text','text'),array('vqv.points','points'),array('vqv.questions_id','questions_id') )
            ->from(array('votes_questions','vq'))
            ->join(array('votes_question_vars','vqv'), 'left outer')
                ->on('vq.id','=','vqv.questions_id')
            ->order_by('date', 'desc')
            ->and_where_open()
            ->where('vq.language_id','=',$lang_id)
            ->group_by('vq.id')
        ;

        foreach( $blocked_ids as $item )
        {
            $query->or_where('vq.id','=', $item);
        }
        
        if($status)
            $query->and_where_close()->and_where('status','=', $status);
        else
            $query->and_where_close();

        $result = $query->execute();
        
        if ($result->count() == 0) return array();
        else
        {
            $res = $result->as_array();            
            foreach($res as $item)
            {
                $item['vars'] = Model_Frontend_Votes::getVars($item['id']);
                if( $item['withOwnVar'] == 1 )
                {
                    $owns = Model_Votes::getOwnVars($item['id']);                    
                    $item['vars']['-1'] = array(
                            'id' => -1,
                            'text' => I18n::get('Your answer`s variant'),
                            'points' => count($owns),
                            'sum_points' => count($owns) * 1,
                            'num_votes' => count($owns),
                            'questions_id'=>$item['id']
                        );                    
                }
                $return[] = $item;
            }
            
            return $return;
        }
    }
    
    public static function getVars($id, $lang_id = 1)
    {
        $query = DB::select('vqv.*')
            ->from(array('votes_question_vars','vqv'))
            ->where('vqv.questions_id','=',$id)
        ;

        $result = $query->execute();
        $return = $result->as_array();

        return $return;
    }

    function showVoteResults($id, $lang_id = 1)
    {        
        $res = Model_Votes::getItem($id, $lang_id );
        if( $res['withOwnVar'] == 1 )
        {
            $owns = Model_Votes::getOwnVars( $res['id'] );
            $res['vars']['-1'] = array(
                'id' => -1,
                'text' => I18n::get('Your answer`s variant'),
                'points' => count($owns),
                'sum_points' => count($owns) * 1,
                'num_votes' => count($owns),
                'questions_id'=>$id
            );
        }

        return $res;
    }

    function showVoteQuestions($id)
    {
        $res = Model_Votes::getItem($id, $lang_id );

        return json_encode($res);
    }

    function makeVote( $id )
    {
        $res = Model_Votes::getItem($id, CURRENT_LANG_ID);
        
        $cookie = Cookie::get('voted') ? json_decode(Cookie::get('voted')) : array();
        
        if( array_search($id, $cookie) || array_search($id, $cookie) === 0)
        {
            die('youvoted');
        }
        
        if( $_GET['var_id'] != -1 )
        {
            DB::update('votes_questions')
                ->set(array(
                    'total_votes' => ($res['total_votes']+1),
//                    'total_sum' => ($res['total_sum']+$res['vars'][$_GET['var_id']]['points'])
                    'total_sum' => ($res['total_sum']+1)
                ))
                ->where('id','=',$id)
                ->execute()
            ;

            DB::update('votes_question_vars')
                ->set(array(
                    'num_votes' => ($res['vars'][$_GET['var_id']]['num_votes']+1),
//                    'sum_points' => ($res['vars'][$_GET['var_id']]['sum_points']+$res['vars'][$_GET['var_id']]['points'])
                    'sum_points' => ($res['vars'][$_GET['var_id']]['sum_points']+1)
                ))
                ->where('id', '=', (int)$_GET['var_id'])
                ->execute()
            ;
        }else{
            DB::update('votes_questions')
                ->set(array(
                    'total_votes' => ($res['total_votes']+1),
                    'total_sum' => ($res['total_sum']+1)
                ))
                ->where('id','=',$id)
                ->execute()
            ;

            DB::insert('votes_own_var', array('question_id','text'))
                ->values(array(
                    $id,
                    $_GET['text']
                ))
                ->execute()
            ;
        }
        
        array_push($cookie, $id);
        
        Cookie::set('voted', json_encode($cookie) );
        return 1;
    }
}
?>