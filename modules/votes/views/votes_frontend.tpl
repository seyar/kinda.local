<br/>
<div class="blockTop"><img src="{$template_root}images/quiz.jpg" alt=""/></div>
<div class="blockBody">
    <div class="blockBodyInner">
        <p class="tCenter">Какую площадь кваритиры вы хотите купить в период с 2010 - 2011 год?</p>
        <div class="leftline"></div>
        <p><input type="hidden" name="rate" value="" id="rate"/></p>
        <div class="inner">
            <table width="100%">
                <tr>
                    <td style="width:1%;"><div class="ratecheckboxImg"  rel="1"></div></td>
                    <td><p class="ratecheckbox tCenter" rel="1">22-30м<sup>2</sup></p></td>
                </tr>
                <tr>
                    <td style="width:1%;"><div class="ratecheckboxImg"  rel="2"></div></td>
                    <td><p class="ratecheckbox tCenter"  rel="2">22-30м<sup>2</sup></p></td>
                </tr>
                <tr>
                    <td style="width:1%;"><div class="ratecheckboxImg" rel="3"></div></td>
                    <td><p class="ratecheckbox tCenter" rel="3">22-30м<sup>2</sup></p></td>
                </tr>
                <tr>
                    <td style="width:1%;"><div class="ratecheckboxImg checked" rel="4"></div></td>
                    <td><p class="ratecheckbox tCenter"  rel="4">22-30м<sup>2</sup></p></td>
                </tr>
            </table>
            <textarea class="quizTxt" cols="1" rows="1"></textarea>
            <a href="#" class="btn" style="margin:10px auto;">Проголосовать</a>
            <a href="#" class="flLeft">Результаты</a>
            <a href="#" class="flRight">Все голосования</a>
            <div class="clear"></div>
        </div>
    </div>
</div>