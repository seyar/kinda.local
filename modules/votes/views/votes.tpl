<link rel="stylesheet" type="text/css" href="/static/admin/css/paginator.css" />
<script type="text/javascript" src="/static/admin/js/list.js"></script>
<script type="text/javascript" src="/static/admin/js/edit.js"></script>
<script type="text/javascript" src="/vendor/jquery/jquery.paginator.js"></script>
<!--date-->
    <script type="text/javascript" src="/vendor/jquery/jquery.date_input.min.js"></script>
    <link href="/static/admin/css/jquery.date_input.css" rel="stylesheet" type="text/css" />
<!--end date-->

{include file="votes_edit_js.tpl"}

<div class="wrap flRight">
      <div class="precontent"></div>
      <form method="post" action="{$admin_path}{$controller}/{$module_name}/">

      <div class="rightCol ">
<!-- MODULE HEAD -->
          <div class="pageBlock {if !$show_module_desc} hidden{/if}" id="addBox">
                  <div class="topTitle rel">{$module.title}<a href="{$admin_path}" class="close"><img src="/static/admin/images/close.gif" alt=""/></a></div>
                  <div class="topBlockBody">
                      <img src="/static/admin/images/moduleIco.jpg" alt="" class="flLeft" width="76"/>
                      <p class="module_descr_short">{$module.descr_short}<br/><a href="#" class="showDescrFull">{php}echo I18n::get('Show full'){/}</a> </p>
                      <p class="module_descr_full hidden">{$module.descr_full}<br/><a href="#" class="showDescrShort">{php}echo I18n::get('Show short'){/}</a> </p>
                      <div class="clear"></div>
                  </div>
          </div>
          <div class="breadcrumbs">
              <p class="flLeft"><a href="{$admin_path}">{php}echo I18n::get('Content'){/}</a> &gt; {$module.title}</p>
              <p class="flRight"><a href="{$admin_path}"
                    class="showHelp {if $show_module_desc} hidden{/if}">{php}echo I18n::get('Show help'){/}</a>&nbsp;<a href="#"
                    class="addShortcut">{php}echo I18n::get('Add to &quot;shortcuts&quot;'){/}</a></p>
              <div class="clear"></div>
          </div>
<!-- /MODULE HEAD -->
          <div class="pageBlock noPadding">
              <div class="topTitle cont rel">{$module.title}</div>
              <div class="fancy rel">
                  <div class="contentNavBtns" id="contentNavBtns">
                      <div class="inner">
                          <a href="{$admin_path}{$controller}/{$module_name}/edit" class="greenBtn flLeft">{php}echo I18n::get('Create'){/}</a>
                          <p class="flLeft">
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              {php}echo I18n::get('With selected'){/}
                              <select id="list_action">
                                  <option value="delete">{php}echo I18n::get('Delete'){/}</option>
                              </select>
                              <button class="btn formUpdate" type="button"><span><span>{php}echo I18n::get('Start'){/}</span></span></button>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          </p>                          
                          <div class="clear"></div>
                      </div>
                  </div>
              </div>

              <div class="innerGray">
                  <table width="100%" class="contentTab" cellpadding="0" cellspacing="0">
			  <tr>
			      <td width="20"> </td>
			      <td class="nowrap">
				  <input type="hidden" id="total_pages" value="{$total_pages}" />
				Страница <button class="btn formFilter" type="button" onclick="prevPage('#page');;"><span><span>&lt;</span></span></button>
				<input type="text" id="page" name="page" value="{$quicky.request['page']|default:'1'}" style="width: 3em; text-align: center;" />
				<button class="btn formFilter" type="button" onclick="nextPage('#page');"><span><span>&gt;</span></span></button>
				из {$total_pages|default:1}.
			      </td>
			      <td class="nowrap">
				Показывать по {select name="rows_num" style="width: 3em; text-align: left;" value=$quicky.post['rows_num']}
				    {option value=10 text='10'}
				    {option value=25 text='25'}
				    {option value=50 text='50'}
				    {option value=100 text='100'}
				{/select} строк.
			      </td>
			      <td align="right">
                                &nbsp;<button class="btn" type="button" onclick="$('.date').val('')"><span><span>Очистить</span></span></button>&nbsp;
				Дата с <input type="text" class="date" name="search_date_from" value="{$quicky.post['search_date_from']}" style="width: 9em;" />
                                по <input type="text" class="date" name="search_date_to" value="{$quicky.post['search_date_to']}" style="width: 9em;" />
			      </td>
			      <td>&nbsp;<button class="btn formFilter" type="button"><span><span>Показать</span></span></button>&nbsp;</td>
			  </tr>
		      </table>
                  <br/>
                  <table width="100%" cellspacing="0" cellpadding="0" id="contentTab" class="contentTab">
                      <tr>
                          <th width="2%" class="tLeft"><input type="checkbox" class="listCheckboxAll" style="width: auto;" value="" name=""></th>
                          <th width="2%">№</th>
                          <th>Вопрос</th>
                          <th width="9%">Дата</th>
                          <th width="10%"></th>
                      </tr>
{foreach name="rows" from=$rows key="key" value="value"}
                    <tr class="{if $quicky.foreach.rows.iteration % 2 == 0}even{else}odd{/if}">
                        <td><input type="checkbox" class="listCheckbox" value="{$value.id}" name="chk[{$value.id}]"></td>
                        <td>{$key+1}</td>
                        <td><a href="{$admin_path}{$controller}/{$module_name}/{$value.id}/edit" >{$value.question|truncate:200}</a></td>
                        <td>{$value.date|date_format:'%d.%m.%Y'}</td>
                        <td>
                            <a href="{$admin_path}{$controller}/{$module_name}/{$value.id}/changeStatus" onclick="changeStatus(this);return false;"><img src="/static/admin/images/{if $value.status != 1}un{/if}verified.gif" alt="status" title="{php}echo I18n::get('status'){/}"/></a>&nbsp;&nbsp;&nbsp;
                            <a href="{$admin_path}{$controller}/{$module_name}/{$value.id}/edit" ><img src="/static/admin/images/edi.gif" alt="edit" title="{php}echo I18n::get('Edit'){/}"/></a>&nbsp;&nbsp;&nbsp;
                            <a href="{$admin_path}{$controller}/{$module_name}/{$value.id}/delete" onclick="if( confirm('Вы уверены?') ) deleteItem(this);return false;"><img src="/static/admin/images/del.gif" alt="delete" title="{php}echo I18n::get('Delete'){/}"/></a>
                        </td>
                    </tr>
{/foreach}
                  </table>
                  {if $total_pages>1}
                    <div class="paginator" id="paginator"></div>
                  {/}
              </div>
              <div class="clear"></div>
          </div>
          <p class="tRight"><a href="{$admin_path}">{php}echo I18n::get('Go to start page'){/}</a></p>
      </div>
      </form>
      <div class="clear"></div>
  </div> <!-- content -->