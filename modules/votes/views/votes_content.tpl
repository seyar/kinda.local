Дата: <input type="text" name="date" class="date" value="{$quicky.post.date|default:$row.date|date_format:'%Y-%m-%d'}" style="width: 120px;"/>
<table width="100%" id="varTab">
    <tr>
        <td width="50%">
            Вопрос:<br/>
            <textarea name="question" rows="5" cols="50">{$quicky.post.question}{$row.question}</textarea>
        </td>
        <td class="varsBlock">
            <span id="checkpoint">Варианты ответов:<br/>Всего {$row.total_sum}</span>
            {if $row.vars}
            {foreach from=$row.vars item=item}
            <div>
                <input type="text" value="{$item.text}" name="text[]" class="new_input">
                <span class="question_results">{($item.sum_points/$row.total_sum * 100)|number_format:2:',':''}%</span>
                <span class="question_results_num">{$item.sum_points|number_format:2:',':''}</span>
            </div>
            {/foreach}
            {/if}
            <div class="orig">
                <input type="text" class="var_input" name="text[]" value=""/>
                <a href="#" title="Добавить" id="plus" onclick="plus();return false;"><img src="/static/admin/images/plus.png" class="vMiddle" alt="+"/></a>
                <a href="#" title="Удалить" id="minus" style="{if !$row.vars}display: none;{/}" onclick="minus();return false;"><img src="/static/admin/images/minus.png" class="vMiddle" alt="-"/></a>
            </div>
        </td>
    </tr>
    <tr>
        <td><br/><label><input type="checkbox" name="withOwnVar" value="1" {if $row.withOwnVar == 1}checked="checked"{/}/> Разрешить свой вариант ответа.</label></td>
        <td><a href="{$admin_path}{$controller}/{$module_name}/{$id|default:0}/getOwns" onclick="showOwns(this);return false;">Результаты пользователей.</a></td>
    </tr>
</table>