
    <script type="text/javascript">

    function plus()
    {
        block = $( document.createElement('div') ).append(
            $('.var_input').clone().removeClass('var_input').addClass('new_input')
        )
            //.append($('#minus').clone())
        ;

        $('.orig').before( block );
        // change input values
        $('.orig').children('input').val('');

        checkShowingMinus();
    }

    function minus()
    {
        $('.orig').prev().remove();
        checkShowingMinus();
    }

    function checkShowingMinus()
    {
        if( $('.new_input').length == 0)
            $('#minus').css('display','none');
        else
            $('#minus').css('display','inline');
    }

    function deleteItem( thisObj )
    {
        $.get(
            $(thisObj).attr('href'),
            function(data)
            {
                if(data == 1)
                    $(thisObj).parent().parent().remove();
                else
                    alert('Server error!');
            }
        );
    }

    function changeStatus( thisObj )
    {
        $.get(
            $(thisObj).attr('href'),
            function(data)
            {
                source = $(thisObj).children('img').attr('src');

                if(data == 1)
                    if( source.indexOf('un') != -1 )
                        $(thisObj).children('img').attr('src', '/static/admin/images/verified.gif');
                    else
                        $(thisObj).children('img').attr('src', '/static/admin/images/unverified.gif')
                else
                    alert('Server error!');
            }
        );
    }

    function showOwns(thisObj)
    {
        $('#jqmWindow').jqm();
        $('#jqmWindow').jqmShow();
        $.getJSON(
            $(thisObj).attr('href'),
            function(data)
            {
                if(data)
                {
                    var results = '';                    
                    for(i in data)
                    {
                        results += '<div class="ownVote rel">'+data[i]['text']+'<a href="#" class="deleteOwnVar" onclick="deleteOwnVar(\''+data[i]['id']+'\', this);return false;"><img src="/static/admin/images/del.gif" alt=""/></a></div>';

                    }
                    $('#jqmWindow').html(results);                    
                }else{
                    alert('Server error!'); return false;
                }
            }
        );
    }

    function deleteOwnVar(id, thisObj)
    {
        $.getJSON(
            '{/literal}{$admin_path}{$controller}/{$module_name}/'+id+'/deleteOwnVar{literal}',
            function(data)
            {
                if(data)
                {
                    $(thisObj).parent().remove();
                }else{
                    alert('Server error!'); return false;
                }
            }
        );
    }

    $(document).ready(function()
    {
        <?if($total_pages):?>
        var page = /page=([^#&]*)/.exec(window.location.href);
        page = page ? page[1] : 1;

        $('#paginator').paginator(
        {
            pagesTotal: <?=$total_pages?>
            ,pagesSpan: 10
            ,pageCurrent: page
            ,baseUrl: '?page='
            ,lang: {
        
                next  : "<?php echo I18n::get('Next')?>",
                last  : "<?php echo I18n::get('Last')?>",
                prior : "<?php echo I18n::get('Prior')?>",
                first : "<?php echo I18n::get('First')?>",
                arrowRight : String.fromCharCode(8594),
                arrowLeft  : String.fromCharCode(8592)          
            }
        });
        <?endif;?>
/*
	    var config = {
		    toolbar:
		    [
			['Styles','Format','Font','FontSize'],
			['TextColor','BGColor'],
			['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
			['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
			'/',
			['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
			['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
			['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
			['Link','Unlink','Anchor'],
			['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],
			['Templates','Maximize','ShowBlocks','-','Source']
		    ]
		    , extraPlugins: 'cyberim'
		    , defaultLanguage: 'ru'
    //		, stylesCombo_stylesSet: 'my_styles:/static/css/styles.js'
	    };

	    $('.jquery_ckeditor').ckeditor(config);
*/
    });

    </script>

    <style type="text/css">
        #varTab tr td{ vertical-align: top;}
        .varsBlock .var_input{ width:200px; }
        .varsBlock .new_input{ width:200px; margin: 0 0 5px 0px;}
        .varsBlock .question_results{ margin: 0 0 5px 50px;}
        .varsBlock .question_results_num{ margin: 0 0 5px 50px;}

        #jqmWindow{ max-height: 600px;overflow-y: auto; }
        #jqmWindow .ownVote{ border-bottom:1px dashed #ccc; margin: 0 0 10px 0; }
        .deleteOwnVar{ position: absolute;top: 0; right: 0; }
    </style>
