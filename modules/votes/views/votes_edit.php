<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 30.11.2010 - 16:00:51 Exp $
 *
 * Project:     kontext
 * File:        votes_edit.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
include 'system/wysiwyg.php';
?>
<!--<script type="text/javascript" src="/static/admin/js/list.js"></script>-->
<script type="text/javascript" src="/static/admin/js/pages.js"></script>
<!--date-->
<link href="/static/admin/css/jquery.date_input.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/vendor/jquery/jquery.date_input.min.js"></script>
<!--end date-->

<!--jqmodal-->
<link href="/vendor/jquery/jqmodal/jqModal.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/vendor/jquery/jqmodal/jqModal.js"></script>
<!--end jqmodal-->
<?include 'votes_edit_js.php'?>
<!--floating block-->
<form method="post" action="">
    <div class="rel whiteBg floatingOuter innerPage" id="contentNavBtns">
        <input type="hidden" name="actTab" id="actTab" value="main" />
        <div class="absBlocks floatingInner">
            <div class="padding20px">
                <input type="hidden" name="list_action" id="list_action" value=""/>
                <table width="100%">
                    <tr>
                        <td><button class="btn blue formSave" type="button" rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/save">
                                <span><span><?= I18n::get('Save') ?></span></span></button></td>
                        <td style="width:37%">
                                <button class="btn blue formUpdate" type="button"  rel="<?= $admin_path . $controller ?><?= (isset($id) ? $id : 0) ?>/update">
                                    <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>

                        <td style="width:80%; text-align: right;"><button class="btn blue" type="button" onclick="document.location.href='<?= $admin_path . $controller ?>';">
                                <span><span><?= I18n::get('Cancel') ?></span></span></button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="absBlocks floatingPlaCorner L"></div>
            <div class="absBlocks floatingPlaCorner R"></div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>
    <!--floating block-->

    <div class="rel" >

        <div class="whiteblueBg padding20px">
            <br />
        <? include MODPATH.ADMIN_PATH.'/views/system/messages.php';?>

            Дата: <input type="text" name="date" class="date" value="<?= (isset($_POST['date']) ? $_POST['date'] : (isset($row['date']) ? date('Y-m-d', strtotime($row['date'])) : date('Y-m-d'))) ?>" style="width: 120px;"/>
            <table width="100%" id="varTab">
                <tr>
                    <td width="50%">
                        Вопрос:<br/>
                        <textarea name="question" rows="5" cols="50"><?= (isset($_POST['question']) ? $_POST['question'] : $row['question']) ?></textarea>
                    </td>
                    <td class="varsBlock">
                        <span id="checkpoint">Варианты ответов:<br/>Всего <?= $row['total_sum'] ?></span>
<? if ($row['vars']): ?>
    <? foreach ($row['vars'] as $item): ?>
                                <div>
                                    <input type="text" value="<?= $item['text'] ?>" name="text[]" class="new_input">
                                    <span class="question_results"><?= number_format(($item['sum_points'] / $row['total_sum'] * 100), 2, ',', '') ?>%</span>
                                    <span class="question_results_num"><?= number_format($item['sum_points'], 2, ',', '') ?></span>
                                </div>
    <? endforeach; ?>
<? endif; ?>
                        <div class="orig">
                            <input type="text" class="var_input" name="text[]" value=""/>
                            <a href="#" title="Добавить" id="plus" onclick="plus();return false;"><img src="/static/admin/images/plus.jpg" class="vMiddle" alt="+"/></a>
                            <a href="#" title="Удалить" id="minus" style="<? if (!$row['vars']): ?>display: none;<? endif; ?>" onclick="minus();return false;"><img src="/static/admin/images/minus.jpg" class="vMiddle" alt="-"/></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><br/><label><input type="checkbox" name="withOwnVar" value="1" <? if ($row['withOwnVar'] == 1): ?>checked="checked"<? endif; ?>/> Разрешить свой вариант ответа.</label></td>
                    <td><a href="<?= $admin_path . $controller ?>/<?= isset($id) ? $id : 0 ?>/getOwns" onclick="showOwns(this);return false;">Результаты пользователей.</a></td>
                </tr>
            </table>

        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
        <div class="absBlocks corner L"></div>
        <div class="absBlocks corner R"></div>
    </div>
</form>  <!--jqmodal-->
<div class="jqmWindow hidden" id="jqmWindow">
    <p class="tCenter"><img src="/static/admin/images/preloader.gif" alt=""/></p>
</div>
<!--end jqmodal-->