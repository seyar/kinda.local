<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 30.11.2010 - 14:09:59 Exp $
 *
 * Project:     kontext
 * File:        votes.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */
?>
<script type="text/javascript" charset="utf-8" src="/static/admin/js/list.js"></script>
<link rel="stylesheet" href="/static/admin/css/list_pagination.css" type="text/css"/>

<form method="post" action="">
<!--floating block-->

    <div class="rel whiteBg floatingOuter" id="contentNavBtns">
        <div class="whiteblueBg absBlocks floatingInner">
            <div class="padding20px">
                <table width="100%">
                    <tr>
                        <td><button class="btn blue" type="button" onclick="document.location.href='/admin/votes/edit';">
                                <span><span><?= I18n::get('Create') ?></span></span></button></td>

                        <td class="vMiddle">C отмеченными </td>
                        <td><select name="sel0"><option>Удалить</option></select></td>
                        <td><button class="btn formUpdate" type="button">
                                <span><span><?= I18n::get('Apply') ?></span></span></button>
                        </td>
                        <td class="vMiddle"><div class="dottedLeftBorder">На стрaнице </div></td>
                        <td><?=Form::select('rows_per_page', array(
                                '10' => '10 ' . I18n::get('lines'),
                                '20' => '20 ' . I18n::get('lines'),
                                '30' => '30 ' . I18n::get('lines'),
                        ), Arr::get($_POST, 'rows_per_page', Cookie::get('rows_per_page'), 10), array('onchange'=>"formfilter(jQuery('#rows_per_page'));"))?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="absBlocks side L"></div>
        <div class="absBlocks side R"></div>
    </div>

<!--floating block-->
<div class="rel">
    <div class="whiteBg">

        <!-- ********************** content START HERR module ********************* -->
        <table width="100%" cellspacing="0" cellpadding="0" id="contentTab" class="sortableContentTab">
              <tr>
                  <th style="width: 5%; padding-left: 0pt;" class="tCenter"><input type="checkbox" class="listCheckboxAll" style="width: auto;" value="" name=""></th>
                  <th width="2%">№</th>
                  <th class="tLeft <?=Admin::table_sort_header('question')?>" ><?= I18n::get('Question') ?></th>
                  <th width="10" class="tLeft <?=Admin::table_sort_header('date')?>" ><?= I18n::get('Date') ?></th>
                  <th width="5%"></th>
              </tr>
<?foreach($rows as $key => $value):?>
            <tr class="<?if($key % 2 == 0):?>even<?else:?>odd<?endif;?>">
                <td class="tCenter"><input type="checkbox" class="listCheckbox checkBox" value="<?=$value['id']?>" name="chk[<?=$value['id']?>"></td>
                <td class="tCenter"><?=($key+1)?></td>
                <td><a href="<?=$admin_path.$controller?><?=$value['id']?>/edit" ><?=Text::limit_chars($value['question'], 200, '...')?></a></td>
                <td><?=date('d.m.Y',strtotime($value['date']));?></td>
                <td>
                    <span class="pageicons" style="width: 55px;">
                        <a href="<?=$admin_path.$controller?><?=$value['id']?>/changeStatus" ><img src="/static/admin/images/act_<?if($value['status'] == 1):?>green<?else:?>red<?endif;?>.png" alt="Activate" title="<?echo I18n::get('Active');?>"/></a>&nbsp;&nbsp;&nbsp;
                        <a href="<?=$admin_path.$controller?><?=$value['id']?>/delete" onclick="if( !confirm('<?echo I18n::get('Are you sure?')?>') ) return false;"><img src="/static/admin/images/del.gif" alt="delete" title="<?echo I18n::get('Delete')?>"/></a>
                    </span>
                </td>
            </tr>
<?endforeach;?>
          </table>
        <? if ($pagination): ?>
        <?=$pagination?>
        <? endif; ?>

<? //echo Kohana::debug($kohana_view_data);?>

        <br />
        <!-- *************** /content END HERE ***************** -->

    </div>
    <div class="absBlocks side L"></div>
    <div class="absBlocks side R"></div>
    <div class="absBlocks corner L"></div>
    <div class="absBlocks corner R"></div>
</div>
</form>