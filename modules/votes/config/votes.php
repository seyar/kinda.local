<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 01.03.2010 - 11:48:48 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        votes.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
return array
(
	'lang'			=> 'ru', // Default the  language.
	'path'			=> dirname(__FILE__) . '/../views/', // Admin templates folder.

);
?>