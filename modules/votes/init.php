<?php defined('SYSPATH') or die('No direct script access.');

Route::set
        (
	    'votes',
	    'admin/votes((/<id>)/<action>)',
	    array( 'id' => '\d{1,10}', )
        )
        ->defaults
        (
           array(
		    'controller'    => 'votes',
		    'action'        => 'index',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

// Frontend
Route::set
        (
	    'votes_front',
	    '(<lang>/)ajax/votes((/<id>)/<action>)',
	    array( 'id' => '\d{1,10}', 'lang' => '(?:'.LANGUAGE_ROUTE.')')
        )
        ->defaults
        (
           array(
		    'controller'    => 'frontend_votes',
		    'action'        => 'index',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;

//if ( strpos($_SERVER["PATH_INFO"], ADMIN_PATH) !== 0 )
//    Event::add('system.routing', array('Model_VotesFrontend', 'init'));