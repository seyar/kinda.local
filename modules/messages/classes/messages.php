<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Messages
{

    public static $session_key = 'messages';
    public static $session_type_key = 'messages_type';

    public static function add($messages, $type = 'err')
    {
        if (is_string($messages))
        {
            $messages = array($messages);
        }

        $status = FALSE;

        if (is_array($messages))
        {
            $status = Session::instance()->set(Messages::$session_key, array_merge(Messages::get(), $messages));
        }
        $status = Session::instance()->set(Messages::$session_type_key, $type);
        return $status;
    }
    
    public static function get()
    {
        return Session::instance()->get(Messages::$session_key, array());
    }
    
    public static function getType()
    {
        return Session::instance()->get(Messages::$session_type_key, array());
    }

    public static function get_once()
    {
        return Session::instance()->get_once(Messages::$session_key);
    }

    public static function has()
    {
        return Messages::get() !== array();
    }

    public static function delete()
    {
        return Session::instance()->delete(Messages::$session_key);
    }

}