<?php

defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 02.04.2010 - 17:58:08 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        rss.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */

class Controller_rss extends Controller_Content
{

    public $template = 'rss.tpl';
    public $module_title = 'RSS';

    public function before()
    {
        parent::before();
        $this->model = new Model_rss();
    }

    public function action_index()
    {
        $xml = $this->model->create_rss();
        header("content-type: text/xml");
        echo $xml;
        die();
    }

}

?>