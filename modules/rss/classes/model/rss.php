<?php defined('SYSPATH') OR die('No direct access allowed.');
/* @version $Id: v 0.1 02.04.2010 - 17:58:16 Exp $
 *
 * Project:     chimera2.local_webservers
 * File:        rss.php * 
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 * 
 * @author Seyar Chapuh <seyarchapuh@gmail.com>, <sc@bestitsolutions.biz>
 */
class Model_rss extends Model
{
    public function create_rss()
    {
        $langs = Model_Content::get_languages_array();
        $lang = $langs[CURRENT_LANG_ID]['prefix'] == 'ru' ? '' : '/'.$langs[CURRENT_LANG_ID]['prefix'];
        $info = array(
              'title' => Kohana::config('news')->feed_title,
              'link' => 'http://'.$_SERVER['HTTP_HOST'].$lang.Kohana::config('news')->feed_link,
              'pubDate' => date(DATE_RFC822), //'Mon, 12 Sep 2005 18:37:00 GMT',
              'description' => Kohana::config('news')->feed_description,
          );
        $xml = '';
//        $items = Model_News::news_show(CURRENT_LANG_ID, NULL, 10, 1);
        $items = Model_Frontend_Publications::publication_index(CURRENT_LANG_ID, NULL, 10, array('publications.status' => array('op'=>'=','value'=>1)) );
                
        $return = array();
        foreach( $items[0] as $item)
        {
            $url = str_replace('//','/',$item['pubtype_parent_url'].$item['pubtype_url'].'/'.$item['pub_url']);
            $return[] =
                  array(
                      'title' => $item['pub_title'],
                      'link' => 'http://'.$_SERVER['HTTP_HOST'].$url,
                      'pubDate' => date(DATE_RFC822, strtotime($item['pub_date'])),
                      'description' => $item['pub_shorttext'],
                  );

        }
        
        $xml = Feed::create($info, $return, 'rss2', 'UTF-8');
        return $xml;
    }
}
?>