<?php  defined('SYSPATH') or die('No direct script access.');
/**
 * @version $Id: v 0.1 04.01.2011 - 14:28:28 Exp $
 *
 * Project:     kontext
 * File:        init.php *
 *
 * This library is commercial distributed software; you can't
 * redistribute it and/or modify it without owner (or author) approval.
 *
 * @link http://bestartdesign.com
 * @Best IT Solutions (C) 2010
 *
 * @author Seyar Chapuh <seyarchapuh@gmail.com>
 */

Route::set
        (
	    'rss',
	    '(<lang>/)rss(/<action>)',
	    array( 'lang' => '(?:'.LANGUAGE_ROUTE.')', 'action' => '(?:index|rss)',)
        )
        ->defaults
        (
            array(
		    'controller'    => 'rss',
		    'action'        => 'index',
		    'lang'	    => LANGUAGE_ROUTE_DEFAULT,
                )
	)
;
?>