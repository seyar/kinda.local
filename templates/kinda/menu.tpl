{if $quicky.server.REQUEST_URI == '/'}
<!--        <div class="slider-wrapper theme-default">
            <div class="ribbon"></div>
            <div id="slider" class="nivoSlider">
                <img src="images/toystory.jpg" alt="" />
                <a href="http://dev7studios.com"><img src="images/up.jpg" alt="" title="This is an example of a caption" /></a>

                <img src="images/walle.jpg" alt="" />
                <img src="images/nemo.jpg" alt="" title="#htmlcaption" />
            </div>
        </div>-->

        <div class="absBlocks teaser slider-wrapper theme-default">
            <div class="ribbon"></div>
            <div class="teaserInner nivoSlider" id="slider">
                {$blocks.teaser|strip_tags:'<img>'}
            </div>
        </div>
        <div class="topTeaserLeft absBlocks"></div>
        <div class="topTeaserRight absBlocks"></div>
        
        <link rel="stylesheet" href="/vendor/jquery/nivo/themes/default/default.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/vendor/jquery/nivo/themes/pascal/pascal.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/vendor/jquery/nivo/themes/orman/orman.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/vendor/jquery/nivo/nivo-slider.css"/>
        <script type="text/javascript" src="/vendor/jquery/nivo/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="{$template_root}js/nyro.js"></script>
{else}
    <div class="absBlocks teaserShort"></div>
{/}
{php}$this->assign('first', Model_Frontend_Categories::getFirst(1));{/}
				
<ul class="topMenu absBlocks{if $quicky.server.REQUEST_URI != '/'} short{/}">
    <li><a href="/about.html" class="mainlink_company {if $quicky.server.REQUEST_URI == '/about.html'}active{/}">О Компании</a></li>
    <li class="divider"><img src="{$template_root}images/divider.png" alt=""/></li>
    <li><a href="/" class="mainlink_cat {if $quicky.server.REQUEST_URI == '/'}active{/}">Каталог</a></li>
    <li class="divider"><img src="{$template_root}images/divider.png" alt=""/></li>
    <li><a href="/shipment.html" class="mainlink_shipment {if $quicky.server.REQUEST_URI == '/shipment.html'}active{/}">Доставка</a></li>
    <li class="divider"><img src="{$template_root}images/divider.png" alt=""/></li>
    <li><a href="/payment.html" class="mainlink_payment {if $quicky.server.REQUEST_URI == '/payment.html'}active{/}">Оплата</a></li>
    <li class="divider"><img src="{$template_root}images/divider.png" alt=""/></li>
    <li><a href="/news/" class="mainlink_newsA {if $quicky.server.REQUEST_URI|strstr:'/news'}active{/}">Новости</a></li>
    <li class="divider"><img src="{$template_root}images/divider.png" alt=""/></li>
    <li><a href="/partners.html" class="mainlink_partners {if $quicky.server.REQUEST_URI == '/partners.html'}active{/}">Партнеры</a></li>
    <li class="divider"><img src="{$template_root}images/divider.png" alt=""/></li>
    <li><a href="/contacts.html" class="mainlink_feedback {if $quicky.server.REQUEST_URI == '/contacts.html'}active{/}">Связь с нами</a></li>
</ul>
<div class="clear"></div>
<div class="absBlocks leftMenuCorner{if $quicky.server.REQUEST_URI != '/'} short{/} {if $quicky.server.REQUEST_URI == '/about.html'}active{/}"></div>
<div class="absBlocks rightMenuCorner{if $quicky.server.REQUEST_URI != '/'} short{/} {if $quicky.server.REQUEST_URI == '/contacts.html'}active{/}"></div>