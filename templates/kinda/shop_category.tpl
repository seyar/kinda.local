{include file="header.tpl"}
                
                <!-- header -->
                
                <div class="centerColInner">
                    <div class="rightColInner rel">
                        {if count($category_info.name)}
                        <p class="colTitle">{$category_info.name}</p>
                        {else}
                        <p class="colTitle">Результаты поиска &laquo;<span>{$quicky.get.browser}</span>&raquo;</p>
                        {/}
                        <div class="emptyBlock"></div>
                        <div class="breadcrumbs">
                            
                            {if $category_info['parents_serialized']}                            
                                {if $category_info['parents_serialized']|count > 1}<a href="/">Главная</a> <img src="{$template_root}images/arrow.png" class="vMiddle" alt=""/> {/}
                                {foreach from=$category_info['parents_serialized'] item=item name="bla2"}
                                    {if $category_info['id'] != $item['id']}                                    
                                        <a href="/shop/{$item['seo_url']}_{$item['id']}">{$item['name']}</a> {if !$quicky.foreach.bla2.last}<!--<img src="{$template_root}images/arrow.png" class="vMiddle" alt=""/>-->{/}
                                    {/}
                                {/}
                            {/}
                        </div>
                        <div class="topSortPanel">
                            {if $sortby == 'price' && $sortasc != 'desc'}
                                {? $priceAsc = 'desc'}
                            {else}
                                {? $priceAsc = 'asc'}
                            {/}
                            
                            {if $sortby == 'name' && $sortasc != 'desc'}
                                {? $nameAsc = 'desc'}
                            {else}
                                {? $nameAsc = 'asc'}
                            {/}
                            
                            {if $sortby == 'scu' && $sortasc != 'desc'}
                                {? $scuAsc = 'desc'}
                            {else}
                                {? $scuAsc = 'asc'}
                            {/}
                            Cортировать продукцию: 
                            <img src="{$template_root}images/sort_by_price.png" alt="" class="vMiddle"/> <a href="javascript:void(0)" onclick="goodsSortBy('price', '{$priceAsc}');"{if $sortby == 'price'} class="active"{/}>по цене</a>&nbsp;
                            <img src="{$template_root}images/sort_by_name1.png" alt="" class="vMiddle"/> <a href="javascript:void(0)" onclick="goodsSortBy('name','{$nameAsc}');"{if $sortby == 'name'} class="active"{/}>по названию</a>&nbsp;
                            <img src="{$template_root}images/sort_by_scu.png" alt="" class="vMiddle"/> <a href="javascript:void(0)" onclick="goodsSortBy('scu','{$scuAsc}');"{if $sortby == 'scu'} class="active"{/}>по артикулу</a>&nbsp;
                            <div class="verticalBorder absBlocks"></div>
                            {if $authorized}
                            <p class="flRight priceSortSide">Цены:
                                <img src="{$template_root}images/pices_uah.png" alt="" class="vMiddle"/> <a href="#" onclick="setCurrency();return false;">гривна</a>
                                <span>или</span>
                                <img src="{$template_root}images/price_by_usd.png" alt="" class="vMiddle"/> <a href="#" onclick="setCurrency();return false;">доллар США</a>
                            </p>
                            {/}
                        </div>
                        <div class="goods">
                            {if $goods}
                                {foreach name="goods_foreach" from=$goods value=value }
                                <div class="carouselItem {if $quicky.foreach.goods_foreach.iteration % 3 == 0}last{/}">
                                    <p><a href="/shop/{$value.category_url}_{$value.categories_id}/{$value.seo_url}_{$value.id}.html">
                                        <img src="{if $value.default_photo}/static/media/shop/{$value.id}/prev_{$value.default_photo}{else}/static/media/no_photo.jpg{/if}" />
                                    </a></p>
                                    <div class="goodTitleRow"><a href="/shop/{$value.category_url}_{$value.categories_id}/{$value.seo_url}_{$value.id}.html">{$value.name|default:$value.alter_name|truncate:300:'...':false} </a>
                                        <div class="scuBlock">Артикул: {$value.scu}</div>
                                    </div>

                                    <div class="priceRow">
                                        <a href="{$value.id}" class="addToCart flLeft"><img src="{$template_root}images/plustocart.png" alt=""/> В корзину</a>
                                        {if $authorized &&$value.oldprices[1][1] > 0 }
                                            <div class="priceMain flRight uah haveoldprice"><span>{$value.oldprices[1][1]|string_format:"%.2f"}</span> грн</div>
                                            <div class="priceMain flRight hidden usd haveoldprice"><span>${$value.oldprices[1][2]|string_format:"%.2f"}</span></div>
                                        {/}

                                        {if $authorized }
                                            <div class="priceMain flRight uah newprice"><span>{$value.prices[1][1]|string_format:"%.2f"}</span> грн</div>
                                            <div class="priceMain flRight hidden usd newprice"><span>${$value.prices[1][2]|string_format:"%.2f"}</span></div>
                                        {/}
                                        <div class="clear"></div>
                                    </div>

                                    <br/>
                                </div>
                                {/}
                            {/}
                            <div class="clear"></div>
                        </div>
                        
                        <div class="category-description">{$category_info.description}</div>
                        <br />
                        {if $pages_list}
                        <div class="paginator">
                            {$pages_list}
<!--                            <a href="#" class="prev"><img src="{$template_root}images/pagination_prev.png" alt=""/></a>
                            <a href="#">1</a>
                            <a href="#" class="active">2</a>
                            <a href="#">3</a>
                            <a href="#">...</a>
                            <a href="#">1</a>
                            <a href="#" class="next"><img src="{$template_root}images/pagination_next.png" alt=""/></a>-->
                        </div>
                        {/}                        
                    </div>                    
                </div>
                
                <div class="leftColInner">
                    {include file="shop_menu.tpl"}
                </div>
                <div class="clear"></div>
                
				<br />
				
{include file="menu.tpl"}
				
<!-- footer -->
<script type="text/javascript" src="{$template_root}js/categories.js"></script>                
{include file="footer.tpl"}
