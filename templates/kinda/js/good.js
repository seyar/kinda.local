function reloadGood(id) {
    $.ajax({
        type:'get',
        url:window.location.href + '/loadGood',
        data:{id:id},
        dataType:'json',
        success:function (data) {
            if (data) {
                var price = parseFloat(data['price']);
                $('.priceMain span').text(price.toFixed(2));
                $('.addBtn').attr('href', data.good_id );
            }
        }
    });
}

function setParam(id) {
    var cookie = $.cookie('goodAddParams');
    var data = {};

    if (cookie == null) {
        data[id] = $('input[type=radio]:checked').val().trim();
    }
    else {
        data = JSON.parse(cookie);
        data[id] = $('input[type=radio]:checked').val().trim();
    }
    $.cookie('goodAddParams', JSON.stringify(data), {path:'/', expires:7});
}

var showGood = function(event) {
    event.preventDefault();
    var scrollVal = $(window).scrollTop() + $(window).height() / 2 - $('.jqLogin').height() / 2;

    $('#jqmWindowGood').jqm();
    $.get(
        document.location.href.replace('#','')+'/loadGoodData'
        , {id:$(this).attr('href')}
        , function(data){
            $('#jqmWindowGood').html(data).css('top', scrollVal).jqmShow();
        }
    );


}

$(document).ready(function () {
    $('.formats select option:first').attr('selected', 'selected');

    $('#addBtn').click(showGood);

//    var leftpartwidth = $('.cloud-zoom').offset().left-10;
    var leftpartwidth = parseInt($('.container').width()) - ($('.mainImage').offset().left + $('.mainImage').width());
    var imgheight = $('.cloud-zoom').children('img').height();

    imgheight = imgheight || 524;
    $('.cloud-zoom').CloudZoom({
        'showTitle':false
//        ,'position':'left'
        ,'zoomWidth':leftpartwidth
//        ,'zoomHeight':imgheight-4
    });
});