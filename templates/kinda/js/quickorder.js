$('document').ready(function()
{
    $('.quickorder input, .quickorder textarea').focus(function()
    {
        if( $(this).attr('default') == $(this).val() || !$(this).val() )
            $(this).val('');
            
        $(this).css('color', '#000000');
    });
        
    $('.quickorder input, .quickorder textarea').blur(function(){
        if( $(this).attr('default') )
        {
            if( $(this).val() == '' )
                $(this).val( $(this).attr('default') );

            $(this).css('color', '#b2b2b2');
            
        }
    });
    
}
);
    
function showTab( thisObj )
{
    $('#addressTab').toggle();
        
    var text = $('#addressTab').is(':visible') ? 'Скрыть адрес доставки' : 'Показать адрес доставки';
        
    $(thisObj).text( text );
        
}
    
function checkForm()
{
    var err = '';

    if( !checkemail($('.required.email').val()) ) err += 'Введите e-mail корректно<br />';
        
    $('.required').each(function()
    {            
        if( $(this).val() == $(this).attr('default') ) $(this).val('');
        if( !$(this).val() ) err += $(this).attr('rel')+'<br />';
    });
        
    if( err )
    {
        myAlert(err);
        return false;
    }
    return true;
}

function clearValues( block )
{
        
    $(block).find('input,select,textarea').each(function()
    {            
        if( $(this).attr('default') != undefined )
        {
            if( $(this).val() == $(this).attr('default') ) $(this).val('');
        }
    });

    return true;
}
    
function checkemail( str )
{
    var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    if (filter.test(str))
        testresults=true
    else{
        //alert("Введите правильный адрес email!")
        testresults=false
    }
    return (testresults)
}

