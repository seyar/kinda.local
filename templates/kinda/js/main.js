$(document).ready(
    function () {
        $('.topMenu li').eq(0).children('a').hover(
            function () {
                $('.leftMenuCorner').addClass('hover');
            }, function () {
                $('.leftMenuCorner').removeClass('hover');
            }
        );

        $('.topMenu li:last-child a').hover(
            function () {
                $('.rightMenuCorner').addClass('hover');
            }, function () {
                $('.rightMenuCorner').removeClass('hover');
            }
        );

//    $('.switcher').click(function(){
//        alert('d');
//    });

        $('.jqmodal').jqm();


        $('a#loginTopLink').click(
            function () {
                var scrollVal = $(window).scrollTop() + $(window).height() / 2 - $('.jqLogin').height() / 2;
                $('.jqLogin').jqm();
                $('.jqLogin').css('top', scrollVal).jqmShow();
            }
        );

        $('a#forgotPassLink').click(function () {
            $('.jqLogin').jqmHide();
            $.ajax({
                type:'get',
                url:'/registration/forgot',
                success:function (data) {
                    showPopup(data);
                }
            });
        });

        $('#enterLink').click(function () {
            if (checkLogin($('#loginForm'))) $('#loginForm').submit();

            return false;
        });
    });

function showPopup(data) {
    $('.jqmodal').html(data);
    var scrollVal = $(window).scrollTop() + $(window).height() / 2 - $('.jqmodal').height() / 2;
    $('.jqmodal').css('top', scrollVal).jqmShow();
}

function myAlert(text, type) {
//	scrollTo(0,0);
    type = type || 'err';
    $('#message').show();
    $('#message').html('<div class="message ' + type + '">' + text + '</div>').slideDown('fast');

    setTimeout(function () {
        $('#message').slideUp('fast',
            function () {
                $('#message').hide();
            }
        );
    }, 10000);
}

function checkLogin(div) {
    err = '';
    if ($(div).find('.email').html() != null && !checkemail($(div).find('.email').val())) err += 'Введите правильный формат Е-mail, ';

    $(div).find('.required').each(
        function () {
            if (!$(this).val()) err += $(this).attr('rel') + ', ';
        }
    );

    if (err) {
        myAlert(err.substr(0, (err.length - 2)), 'err');
        return false;
    } else
        return true;
}

function checkemail(str) {
    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    if (filter.test(str))
        testresults = true
    else {
        //alert("Введите правильный адрес email!")
        testresults = false
    }
    return (testresults)
}