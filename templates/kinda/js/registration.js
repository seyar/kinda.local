/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(
    function(){
    
        $('#register').click(function(){
            if( check( $('#regForm') ) ) $('#regForm').submit();
            
            return false;
        });
    
});

function check(div)
{
    err = '';
    
    if( $(div).find('.email').html() != null && !checkemail( $(div).find('.email').val() ) ) err += 'Введите правильный формат Е-mail, ';

    $(div).find('.required').each(
        function()
        {
            if( !$(this).val() ) err += $(this).attr('rel')+', ';
        }
    );

    if( err )
    {
        myAlert( err.substr(0, (err.length - 2) ) ,'err');
        return false;
    }else
        return true;
}

