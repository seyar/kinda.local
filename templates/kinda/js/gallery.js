var step = '249';
var speed = 300;
var animated = 0;

$(document).ready(function(){
    
});

function prev( thisObj )
{
    if(!checkPrev(thisObj)) return false;
    if( animated != 1  )
    {
        animated = 1;
        thisObj.children('.carouselOuter').children('.carouselInner').animate(
            {"marginLeft": "+="+step},
            speed,
            function(){animated = 0;}
        );
    }
}

function next( thisObj )
{
    if(!checkNext(thisObj)) return false;
    if( animated != 1 )
    {
        animated = 1;
        thisObj.children('.carouselOuter').children('.carouselInner').animate(
            {"marginLeft": "-="+step},
            speed,
            function(){animated = 0;}
        );
    }
}

function checkPrev(thisObj)
{
    
    if( parseInt(thisObj.children('.carouselOuter').children('.carouselInner').css('marginLeft')) == 0 ) 
        return false;
    return true;
}

function checkNext(thisObj)
{
    var count = thisObj.children('.carouselOuter').children('.carouselInner').children('.carouselItem').length;
    var carouselWidth = step * (count - 2);
    
    if( parseInt(thisObj.children('.carouselOuter').children('.carouselInner').css('marginLeft')) == carouselWidth * -1 ) return false;
    return true;
}