/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function goodsSortBy( sort_by, sort_asc )
{
    
    jQuery.cookie('shop_f_sortby', sort_by); 
    jQuery.cookie('shop_f_sortasc', sort_asc); 
    document.location.reload();
    return false;
}

function setCurrency( )
{
    $('.priceMain.usd, .priceMain.uah').toggleClass('hidden');
}

var showGood = function(event) {
    event.preventDefault();
    var scrollVal = $(window).scrollTop() + $(window).height() / 2 - $('.jqLogin').height() / 2;

    $('#jqmWindowGood').jqm();
    $.get(
        document.location.href.replace('#','')+'/some_'+$(this).attr('href')+'.html/loadGoodData'
        , {id:$(this).attr('href')}
        , function(data){
            $('#jqmWindowGood').html(data).css('top', scrollVal).jqmShow();
        }
    );


}

$(document).ready(function () {

    $('.addToCart').click(showGood);
});