{include file="header.tpl"}
                
<!-- header -->

<div class="centerColInner">
    <div class="rightColInner rel newContent">
        <h1 class="colTitle tLeft">{$page.page_name}</h1>
        <br />
        <div class="text">
            {$page.page_content}
        </div>                    
    </div>                    
</div>

<div class="leftColInner">
    {include file="shop_menu.tpl"}
</div>
<div class="clear"></div>

<br />

{include file="menu.tpl"}

<!-- footer -->
				
{include file="footer.tpl"}