{include file="header.tpl"}
<link rel="stylesheet" href="/vendor/jquery/cloudzoom/cloud-zoom.css" />
                
                <!-- header -->
                
                <div class="centerColInner">
                    <div class="rightColInner rel">
                        <p class="colTitle">{$category_info.page_title}</p>
                        <div class="breadcrumbs">
                            {if $category_info['parents_serialized']}
                                <a href="/">Главная</a> <img src="{$template_root}images/arrow.png" class="vMiddle" alt=""/> 
                                {if $category_info['parents_serialized']|count > 1}
                                {/}
                                    {foreach from=$category_info['parents_serialized'] item=item name="bla"}
                                    {*if $category_info['id'] != $item['id']*}
                                        <a href="/shop/{$item['seo_url']}_{$item['id']}">{$item['name']}</a> {if !$quicky.foreach.bla.last}<img src="{$template_root}images/arrow.png" class="vMiddle" alt=""/>{/}
                                    {*/*}
                                {/}
                            {/}
                            {*$category_info.name*}
<!--                            <a href="#">Главная</a>
                            <img src="{$template_root}images/arrow.png" class="vMiddle" alt=""/>
                            <a href="#">Пригласительные открытки</a>-->
                        </div>
                        <div class="emptyBlock"></div>
                        
			            <span style="color:gray;font-size:10px;display: block;padding: 0 0 3px 0;">Для увеличения изображения наведите курсор</span>
                        <div class="zoom-small-image mainImage">
                            <a href="/static/media/{if $obj_images.default_img}shop/{$obj.id}/{$obj_images.default_img}{else}no_photo.jpg{/}" class='cloud-zoom' id='zoom1' rel="adjustX: -10, adjustY:0">
                                <img src="/static/media/{if $obj_images.default_img}shop/{$obj.id}/{$obj_images.default_img}{else}no_photo.jpg{/}" alt="" />
                            </a>
                        </div>


                        {foreach from=$obj_images.files value=value name="bla"}
                            {if $value != $obj_images.default_img}
                                <div class="goodvert"><a href="/static/media/shop/{$obj.id}/{$value}" class="gallery fancy imgsmall" rel="2"><img src="/static/media/shop/{$obj.id}/prev_{$value}" alt="" /></a></div>
                            {/if}
                        {/foreach}			 
                        <div class="clear"></div>

                        {if $authorized }<div class="priceMain goodPage flRight oldpricegood"><span>{$obj.prices[1][1]|string_format:"%.2F"}</span> грн</div>{/}
                        {if $authorized && $obj.oldprices[1][1] > 0}<div class="priceMain goodPage flRight haveoldpricegood"><span>{$obj.oldprices[1][1]|string_format:"%.2F"}</span> грн</div>{/}
                        <p class="goodTitle">{$obj.name|default:$obj.alter_name} </p>
                        <div class="clear"></div>
                        <div class="goodDescription">
                            <p class="descriptionTitle">Характеристики</p>
                            <table>
                                <tr>
                                    <td>Артикул: </td>
                                    <td>{$obj.scu}{foreach from=$obj.groups item=item} / {$item['scu']}{/}</td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle;">Формат:</td>
                                    <td>
                                        <div class="borderedBorder flLeft formats">
                                            <select class="invizSelect" onchange="reloadGood(this.value);">
                                                <option value="{$obj.id}">{$obj.format}</option>
                                                {foreach from=$obj.groups item=item}
                                                    <option value="{$item.id}">{$item.format}</option>
                                                {/}
                                            </select>
                                        </div>
                                        <div class="clear"></div>
                                    </td>
                                </tr>
                                
                                {foreach from=$obj.add_fields item=iitem name="bla"}
                                    {if $iitem.f_group}
                                        <tr>
                                            <td colspan="2">{$iitem.name}:</td>
                                        </tr>                                        
                                        {foreach from=$iitem.childs item=citem}
                                            {if (($citem.value_int == 0 && $citem.value_int|isset) || $citem.value_int || $citem.value_float || $citem.value_varchar || $citem.value_text || $citem.value_date)}
                                                <tr>
                                                    <td>{$citem.name}:</td>
                                                    <td>
                                                        {if $citem['type'] == 'boolean' && $citem.value_text|strstr:'|' }
                                                            {? $razmers = explode('|',$citem['value_text']);}
                                                            {foreach from=$razmers item=it name="bla"}
                                                                <div class="radio-size"><label><input name="{$citem['name']}" onclick="setParam({$obj.id});" type="radio" value="{$it}" {if $quicky.foreach.bla.first}checked="checked"{/}/> <span>{$it}</span></label> </div>
                                                            {/}
                                                        {else}
                                                            {if ( (!$citem.value_int|isset) && !$citem.value_float && !$citem.value_varchar && !$citem.value_date)}
                                                                {$citem.value_text}
                                                            {else}    
                                                                {if $citem.value_int == 1 && $citem.type == 'logic'}+{elseif $citem.value_int == 0 && $citem.type == 'logic'}-{else}{$citem.value_int}{/}{$citem.value_float}{$citem.value_varchar}
                                                            {/}
                                                        {/}
                                                    </td>
                                                </tr>
                                            {/}
                                        {/foreach}
                                    {else}
                                    
                                        {if (($iitem.value_int == 0 && $iitem.value_int|isset) || $iitem.value_int || $iitem.value_float || $iitem.value_varchar || $iitem.value_text || $iitem.value_date)}
                                            <tr>
                                                <td>{$iitem.name}:</td>
                                                <td>
                                                    {if $iitem['type'] == 'boolean' && $iitem.value_text|strstr:'|' }
                                                        {? $razmers = explode('|',$iitem['value_text']);}
                                                        {foreach from=$razmers item=it name="bla"}
                                                        <div class="radio-size"><label><input name="{$iitem['name']}" onclick="setParam({$obj.id});" type="radio" value="{$it}" {if $quicky.foreach.bla.first}checked="checked"{/} /> <span>{$it}</span></label> </div>
                                                        {/}
                                                    {else}
                                                    
                                                        {if ( (!$iitem.value_int|isset) && !$iitem.value_float && !$iitem.value_varchar && !$iitem.value_date)}
                                                            {$iitem.value_text }
                                                        {else}
                                                            {if $iitem.value_int == 1 && $iitem.type == 'logic'}+{elseif $iitem.value_int == 0 && $iitem.type == 'logic'}-{else}{$iitem.value_int}{/}{$iitem.value_float}{$iitem.value_varchar}
                                                        {/}
                                                    {/}
                                                </td>
                                            </tr>
                                        {/}
                                    {/}
                                {/}
                                
                            </table>
                            <p class="descriptionTitle">Описание</p>
                            {$obj.full_description}
                        </div>
                        <br />
                        <div class="bottomTaxiBorder"><div></div></div>
                        <br />
                        <div class="borderedBorder flLeft quantityStatus">
                            Наличие на складе: 
                                {if $obj.quantity==0}
                                    {$obj['store_approximate_date']}
                                {else}
                                        <span>есть в наличии</span>
                                {/}
                        </div>
                        <a href="{$obj.id}" class="addBtn" id="addBtn"><img src="{$template_root}images/plustocart.png" alt=""/> Добавить в корзину</a>
                        <br />
                    </div>                    
                </div>
                
                <div class="leftColInner">
                    {include file="shop_menu.tpl"}
                </div>
                <div class="clear"></div>
                
				<br />
				
{include file="menu.tpl"}

                <!-- footer -->
                <script src="/vendor/jquery/cloudzoom/cloud-zoom.1.0.2.min.js"></script>
                <script src="{$template_root}js/good.js"></script>


{include file="footer.tpl"}
