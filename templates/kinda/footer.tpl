                <a class="absBlocks logo" href="/"><img src="{$template_root}images/logo.png" alt="kinda"/></a>
                <div class="verticalLine first"></div>
                <a href="/download/price" class="topLinks downloadLinks downloadPrice">Скачать прайс<br/>лист</a>
                <a href="/custom-order" class="topLinks downloadLinks orderPacket">Заказать имиджевый пакет</a>
                <div class="verticalLine second"></div>
                {if $authorized['id']}
                    <a href="/shop/order/address/" class="absBlocks loginLinks profile">{$authorized.name|truncate:35:'...':true}</a>
                    <a href="/profile/logout" class="absBlocks loginLinks logout">Выход</a>
                    <a href="/shop/cart" style="top:87px;" class="absBlocks loginLinks cart">Корзина</a>
                {else}
                    <a href="#" class="absBlocks loginLinks login" id="loginTopLink">Войти</a>
                    <a href="/registration" class="absBlocks loginLinks bepartner">Стать<br />партнером</a>
                    <a href="/shop/cart" class="absBlocks loginLinks cart">Корзина</a>
                {/}
                
                <div class="verticalLine third"></div>
                <div class="absBlocks phones tRight">
                    <p><span class="code">{$blocks.gorod_kod}</span><span class="phoneDigits">{$blocks.tel_gorod}</span></p>
					<p class="phoneRow man">Главный офис в Симферополе</p>
                    <p><span class="code">{$blocks.mob_kod}</span><span class="phoneDigits">{$blocks.tel_mob}</span></p>
					<p class="phoneRow"> Менеджер по продажам</p>
                </div>
                <div class="fancy"></div>
            </div>
            <footer class="rel">
                <div class="absBlocks circleLine"><p></p></div>
                
                <p class="cities absBlocks simf">Симферополь</p>
                <p class="cities absBlocks kyi">Киев</p>
                {$blocks.address_simf}
                {$blocks.address_kyiv}
                <div class="clear"></div>
                <a href="/" class="absBlocks footerLogo"><img src="{$template_root}images/kinda_bottom_logo.png" alt="kinda"/></a>
                <form action="/shop/search" method="get" id="bottomSearchForm">
                    <input type="text" name="browser" value="{$quicky.get.browser|default:''}" placeholder="Поиск по каталогу" class="absBlocks footerSearchInput"/>
                    <a href="#" class="absBlocks zoomIco" onclick="if( $('#bottomSearchForm input').val() ) $('#bottomSearchForm').submit();return false;"></a>
                </form>
		<div class="counter">{$blocks.counter}</div>
            </footer>
        </div>
        {include file="messages.tpl"}
        
        <div class="jqmodal"></div>
        <div class="jqLogin">
            <div class="loginPopup">
                <a href="javascript:void(0);" title="закрыть" class="closeLoginForm" onclick="$('.jqLogin').jqmHide();"></a>
                <form action="/registration/login" method="post" id="loginForm" onsubmit="if( !checkLogin( $('#loginForm') ) ) return false;">
                    <h2 class="tCenter"><p><span><span>Вход на сайт</span></span></p></h2>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td class="tCenter">Логин</td>
                        </tr>
                        <tr>
                            <td><input class="tCenter required email" type="text" name="email_login" value="{$quicky.post.email_login}" rel="Введите Email"/></td>
                        </tr>
                        <tr>
                            <td class="tCenter"><br/>Пароль</td>
                        </tr>
                        <tr>
                            <td><input class="tCenter required" type="password" name="pass" value="" rel="Введите пароль"/></td>
                        </tr>
                        <tr>
                            <td class="">
                                <br />
                                <a href="#" class="enterBtn" id="enterLink">Войти</a>
                                <p class="forgotP"><img class="vMiddle" src="{$template_root}images/forgot.png" alt=""/> <a href="#" id="forgotPassLink">Забыли пароль?</a></p>
                            </td>
                        </tr>
                    </table>
                    <input type="submit" style="position: absolute; left: -10000px;"/>
                </form>
            </div>
        </div>
        <div class="jqmWindowGood" id="jqmWindowGood"></div>
    </body>
</html>
