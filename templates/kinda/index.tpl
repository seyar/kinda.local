                {include file="header.tpl"}
                <!-- header -->
                
                <h1><p><span><span>Наша продукция</span></span></p></h1>
				
                <div class="mainCats">
<!--                    ---- -->
                {php}
                    $oCat = new Model_Frontend_Categories();
                    $cats = $oCat->getFirstSecondDepth();
                    $this->assign('cats', $cats);
                {/}
                    {if $cats}
                        <div class="row">
                        {foreach from=$cats item=item name="bla"}
                                <div class="mainCat">
                                    {php} if( file_exists(MEDIAPATH.'shop/categories/thumb_'.$this->_tpl_vars['item']['id'].'.jpg') ): {/}
                                    <div><img src="/static/media/shop/categories/thumb_{$item['id']}.jpg" alt=""/></div>
                                    {php}endif;{/}
                                    
                                    <p><a href="/shop/{$item['seo_url']}_{$item['id']}" class="catTitle">{$item['name']}</a></p>
                                    {if $item['children']}
                                    <ul>
                                        {foreach from=$item['children'] item=iitem}
                                        <li><a href="/shop/{$iitem['seo_url']}_{$iitem['id']}">{$iitem['name']}</a></li>
                                        {/}
                                    </ul>                        
                                    {/}
                                </div>
                        {if $quicky.foreach.bla.iteration % 4 == 0}<div class="clear"></div></div><div class="row">{/}
                        {/}
                    {else}
                    Категории не найдены.
                    {/}
                    {if $cats|count % 4 == 0}</div>{/}
<!--                    ---- -->
                </div>
                <div class="leftCol flLeft">
                    <h2><span>О нас</span></h2>
                    <div class="leftText">
						{$blocks.onas}                                        
                    </div>
                </div>
                <div class="rightCol flRight">
                    <h2 class="tCenter"><p><span><span>Спецпредложения</span></span></p></h2>
                    <br />
                    {php}$newgoods = Model_Frontend_Categories::show_new(9, 1); $this->assign('newgoods', $newgoods);{/}
                    <div class="rel">
                        <a href="#" class="carouselPrev" onclick="prev( $(this).parent() );return false;"></a>
                        <a href="#" class="carouselNext" onclick="next( $(this).parent() );return false;"></a>
                        <div class="carouselOuter">
                            {if $newgoods}
                            <div class="carouselInner">
                                {foreach from=$newgoods item=item}
                                <div class="carouselItem hothit">
                                    <div class="hothitLabel"></div>
                                    <p><a href="/shop/{$item.category_url|escape:'url'}_{$item.categories_id}/{$item.seo_url|escape:'url'}_{$item.id}.html">
                                        <img src="{if $item.default_photo}/static/media/shop/{$item.id}/prev_{$item.default_photo}{else}/static/media/no_photo.jpg{/if}" alt=""/>
                                    </a></p>
                                    <div class="mainTitleGood flLeft">
                                        <a href="/shop/{$item.category_url|escape:'url'}_{$item.categories_id}/{$item.seo_url|escape:'url'}_{$item.id}.html">{$item.name|default:$item.alter_name}</a>
                                    </div>
                                    {if $authorized}
                                    <div class="priceMain flRight"><span>{$item.prices[1][1]|string_format:"%.2f"}</span> грн</div>
                                    {/}
                                    <div class="clear"></div>
                                    <br />
<!--                                    <div class="bottomTaxiBorder"><div></div></div>-->
                                </div>
                                {/}
                            </div>
                            {else}
                            <p class="tCenter">Спецпредложения не найдены.</p>
                            {/}
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <br />
                <div class="leftCol flLeft">
                    {php}$this->assign('lastNews',Model_Frontend_Publications::publication_top( CURRENT_LANG_ID,1,3 ));{/}
                    <h2><a href="/news/"><span>Новости</span></a></h2>
                    <div class="leftText">
                        {if $lastNews}
                        <div class="mainNews">
                            {foreach from=$lastNews item=value}
                            <div class="mainNew">
                                {if $value.pub_image}
                                    <div class="mainNewImage flLeft"><p><a href="/news/{$value['pub_url']}"><img src="/static/media/publications/{$value.id}/prev_{$value.pub_image}" alt="{$value.pub_name|escape:'html'}" /></a></p></div>
                                {/}
                                <div class="mainNewText flLeft">
                                    <a href="/news/{$value['pub_url']}">{$value.pub_name|truncate:200:'...':true}</a>
                                    <p class="date">&mdash;{$value.pub_date|date_format:'%d. %m. %Y'}</p>
                                </div>
                                <div class="clear"></div>
                                <br />
                                <div class="bottomTaxiBorder"><div></div></div>
                            </div>
                            {/}
<!--                            <div class="mainNew">
                                <div class="mainNewImage flLeft"><a href="#"><img src="{$template_root}images/image1.jpg" alt=""/></a></div>
                                <div class="mainNewText flLeft">
                                    <a href="#">Появились металлические стойки для открыток. Ваши открытки достойны лучшего.</a>
                                    <p class="date">&mdash;12.12.09</p>
                                </div>
                                <div class="clear"></div>
                                <br />
                                <div class="bottomTaxiBorder"><div></div></div>
                            </div>-->
                        </div>
                        {else}
                        <p class="tCenter">Новости не найдены.</p>
                        {/}
                    </div>
                </div>
                <div class="rightCol flRight">
                    {php}$bestsellers = Model_Frontend_Categories::show_bestseller(9, 1); $this->assign('bestsellers', $bestsellers);{/}
                    <h2 class="tCenter"><p><span><span>Хиты продаж</span></span></p></h2>
                    <br />
                    <div class="rel">
                        <a href="#" class="carouselPrev" onclick="prev( $(this).parent() );return false;"></a>
                        <a href="#" class="carouselNext" onclick="next( $(this).parent() );return false;"></a>
                        <div class="carouselOuter">
                            {if $bestsellers}
                            <div class="carouselInner">
                                {foreach from=$bestsellers item=item}
                                <div class="carouselItem">                                    
                                    <p><a href="/shop/{$item.category_url|escape:'url'}_{$item.categories_id}/{$item.seo_url|escape:'url'}_{$item.id}.html">
                                        <img src="{if $item.default_photo}/static/media/shop/{$item.id}/prev_{$item.default_photo}{else}/static/media/no_photo.jpg{/if}" alt=""/>
                                    </a></p>
                                    <div class="mainTitleGood flLeft">
                                        <a href="/shop/{$item.category_url|escape:'url'}_{$item.categories_id}/{$item.seo_url|escape:'url'}_{$item.id}.html">{$item.name|default:$item.alter_name}</a>
                                    </div>
                                    {if $authorized}
                                    <div class="priceMain flRight"><span>{$item.prices[1][1]|string_format:"%.2f"}</span> грн</div>
                                    {/}
                                    <div class="clear"></div>
                                    <br />
<!--                                    <div class="bottomTaxiBorder"><div></div></div>-->
                                </div>
                                {/}
                            </div>
                            {else}
                            <p class="tCenter">Хиты не найдены.</p>
                            {/}
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
				<br />
                {include file="menu.tpl"}
                <!-- footer -->
                <script type="text/javascript" src="{$template_root}js/gallery.js"></script>
				{include file="footer.tpl"}
