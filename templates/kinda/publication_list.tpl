{include file="header.tpl"}
                
                <!-- header -->
                
                <div class="centerColInner">
                    <div class="rightColInner rel">
                        <p class="colTitle">Новости</p>
                        <div class="emptyBlock"></div>
                        <div class="breadcrumbs">
                            <a href="/">Главная</a>
                            <img src="{$template_root}images/arrow.png" class="vMiddle" alt=""/>
                            <span>Новости</span>
                        </div>
                        {if $publ_id}
                        <div class="news">
                            {foreach from=$rows key="key" value="value"}
                                {? $pub_url=$root_folder .$inter_info.publications_types[$value.publicationstypes_id]['parent_url'].$inter_info.publications_types[$value.publicationstypes_id].url .'/'.$value['pub_url']}
                                <div class="new">
                                    {if $value.pub_image}
                                    <div class="newImg flLeft">
                                        <p><a href="/news/{$value['pub_url']}"><img src="{$images_folder}{$value.id}/prev_{$value.pub_image}" alt="{$value.pub_name|escape:'html'}" /></a></p>
                                    </div>
                                    {/}
                                    <div class="newText flLeft">
                                        <a href="/news/{$value['pub_url']}">{$value.pub_name} </a> <span class="date">&mdash; {$value.pub_date|date_format:'%d. %m. %Y'}</span>
                                        <p>{$value.pub_shorttext}</p>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="bottomTaxiBorder"></div>
                                </div>
                            {/}
                        </div>
                        {/}
                        
                        <br />
                        {if $page_line}
                            <div class="paginator">
                                    {$page_line}
                            </div>
                        {/if}
<!--                        <div class="paginator">
                            <a href="#" class="prev"><img src="{$template_root}images/pagination_prev.png" alt=""/></a>
                            <a href="#">1</a>
                            <a href="#" class="active">2</a>
                            <a href="#">3</a>
                            <a href="#">...</a>
                            <a href="#">21</a>
                            <a href="#" class="next"><img src="{$template_root}images/pagination_next.png" alt=""/></a>
                        </div>-->
                    </div>                    
                </div>
                
                <div class="leftColInner">
                    <p class="colTitle">Каталог</p>
                    <div class="emptyBlock"></div>
                    <div class="rel searchInput"><input type="text" name="" value="" placeholder="поиск по каталогу"/><a href="#"><img src="{$template_root}images/zoom.png" alt=""/></a></div>
                    
                    <ul class="leftMenu">
                        <li class="plus minus">
                            <a href="#" class="switcher"></a>
                            <a href="#">Торжественная открытка "RAIWAI"</a>
                            <ul class="submenu">
                                <li class="plus minus"><a href="#">Dream Cards</a></li>
                                <li class="plus minus"><a href="#">Dream Cards</a></li>
                                <li class="plus minus"><a href="#">Dream Cards</a></li>
                            </ul>
                        </li>
                        <li class="plus">
                            <a href="#" class="switcher"></a>
                            <a href="#">Торжественная открытка "RAIWAI"</a>
                            <ul class="submenu">
                                <li class="plus minus"><a href="#">Dream Cards</a></li>
                                <li class="plus minus"><a href="#">Dream Cards</a></li>
                                <li class="plus minus"><a href="#">Dream Cards</a></li>
                            </ul>
                        </li>
                        <li class="plus">
                            <a href="#">Торжественная открытка "RAIWAI"</a>
                            <ul class="submenu">
                                <li class="plus minus"><a href="#">Dream Cards</a></li>
                                <li class="plus minus"><a href="#">Dream Cards</a></li>
                                <li class="plus minus"><a href="#">Dream Cards</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                
				<br />
				
{include file="menu.tpl"}
				
                <!-- footer -->
				
{include file="footer.tpl"}