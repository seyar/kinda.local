{php}
$cats = Model_Frontend_Categories::child_categories(NULL,1);
$this->assign('cats',$cats);
{/}
<p class="colTitle">Каталог</p>
<div class="emptyBlock"></div>
<form action="/shop/search" method="get" id="topSearchForm">
    <div class="rel searchInput"><input type="text" name="browser" value="{$quicky.get.browser|default:''}" placeholder="поиск по каталогу"/><a href="#" onclick="if( $('#topSearchForm input').val() ) $('#topSearchForm').submit();return false;"><img src="{$template_root}images/zoom.png" alt=""/></a></div>    
</form>

{if $cats}
<ul class="leftMenu" id="leftMenu">
    {foreach from=$cats item=item}
    <li class="plus {if ($item['id'] == $category_info['id'] || $item['id'] == $category_info['parent_id']) }minus{/}" {if !$item['childs']}style="background: none;"{/}>
        {if $item['childs']}
        <a href="#" class="switcher"></a>
        {/}
        <a href="/shop/{$item['seo_url']}_{$item['id']}">{$item['name']|truncate:50:'...':true}</a>
        <ul class="submenu">
        {if $item['childs']}
            {foreach from=$item['childs'] item=iitem}
                <li class="plus minus"><a href="/shop/{$iitem['seo_url']}_{$iitem['id']}">{$iitem['name']|truncate:50:'...':true}</a></li>
            {/}
        {/}
        </ul>
    </li>
    {/}
</ul>
{else}
Категории не найдены.
{/}
<script type="text/javascript" src="{$template_root}js/leftmenu.js"></script>