{include file="header.tpl"}
                
<!-- header -->

<div class="centerColInner">
    <div class="rightColInner rel newContent">
        <h1 class="colTitle tLeft">&nbsp;</h1>
        <br />
        <div class="text">
            <p><b>К сожалению, Ваш заказ не был обработан</b></p>
            <br/>
            <p>Вернитесь в <a href="/shop/cart/">Корзину</a> и попробуйте отправить данные еще раз</p>                      
        </div>
    </div>
</div>

<div class="leftColInner">
    {include file="shop_menu.tpl"}
</div>
<div class="clear"></div>

<br />

{include file="menu.tpl"}

<!-- footer -->
				
{include file="footer.tpl"}