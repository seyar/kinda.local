{php}$this->assign('authorized',Session::instance()->get('authorized'));{/php}<!DOCTYPE html>
<html>
    <head>
        <title>{$page.page_title}</title>
        <meta name="keywords" content="{$page.page_keywords}" />
        <meta name="description" content="{$page.page_description}" />
        <meta charset="UTF-8" />
        <meta name="author" content="Seyar Chapuh" />

        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

        <link rel="stylesheet" type="text/css" href="{$template_root}css/styleLib.css" />
        <link rel="stylesheet" type="text/css" href="{$template_root}css/fonts.css" />
        <link rel="stylesheet" type="text/css" href="{$template_root}css/style.css" />


        <!--[if IE 6]>
        <script src="{$template_root}js/DD_belatedPNG.js" type="text/javascript"></script> 
		<script type="text/javascript" src="{$template_root}js/pngfix.js"></script>
        <![endif]-->
        <link rel="icon" type="image/png" href="{$template_root}images/favicon.png" />
        <link rel="apple-touch-icon" href="{$template_root}images/ifavicon.png"/>

        <!--[if lt ie 8]><link rel="stylesheet" type="text/css" href="{$template_root}css/styleIe.css"><![endif]-->
        
        <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700|Open+Sans:400italic,600italic&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
        
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
        <!--jqmodal-->
            <link rel="stylesheet" href="/vendor/jquery/jqmodal/jqModal.css" />
            <script type="text/javascript" src="/vendor/jquery/jqmodal/jqModal.js"></script>
        <!-- jqmodal -->
        <!--fancybox-->
        <link rel="stylesheet" type="text/css" href="/vendor/jquery/fancybox/jquery.fancybox.css" />

        <script type="text/javascript" src="/vendor/jquery/fancybox/jquery.fancybox-1.2.1.js"></script>
        <script type="text/javascript" src="{$template_root}js/fancy.js"></script>
        
        <!--fancybox-->
        
        <script type="text/javascript" src="/vendor/jquery/jquery.cookie.js"></script>
        <script type="text/javascript" src="{$template_root}js/main.js"></script>
        <script type="text/javascript" src="/vendor/json2.js"></script>
    </head>
    <body>
        <div class="container rel">
            <div class="wrap rel">
				<div class="preheader{if $quicky.server.REQUEST_URI != '/'} short{/}"></div>
