﻿{include file="header.tpl"}

               <div id="middle">
                {include file="sideLeft.tpl"}

                <div id="container2" class="contentpad contentIe">
				
				<h1>{php}echo i18n::get('The page'){/} "{$url}" {php}echo i18n::get('does not exist.'){/}</h1>
                <p>{php}echo i18n::get('Check to make sure you typed the address correctly or use the menu to navigate the site.'){/}</p>

                    
                </div><!-- #container-->



            </div><!-- #middle-->

        </div><!-- #wrapper -->
		<div style="clear: both"></div>
{include file="footer.tpl"}