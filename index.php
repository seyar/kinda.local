<?php

/** Important patch for deployment on CGI/FastCGI servers * */
if (!isset($_SERVER["PATH_INFO"]))
    $_SERVER["PATH_INFO"] = (isset($_SERVER['ORIG_PATH_INFO']) && !empty($_SERVER['ORIG_PATH_INFO'])) ?
            $_SERVER['ORIG_PATH_INFO'] : $_SERVER["REQUEST_URI"];

if ($_SERVER["PATH_INFO"] == '/index.php')
    $_SERVER["PATH_INFO"] = '/';
    
if ($_SERVER["PATH_INFO"] == '/403.shtml')
    $_SERVER["PATH_INFO"] = '/';
    

define('ADMIN_PATH',	'/admin/');

/**
 * The directory in which your application specific resources are located.
 * The application directory must contain the config/kohana.php file.
 *
 * @see  http://docs.kohanaphp.com/install#application
 */
$application = 'application';

/**
 * The directory in which your modules are located.
 *
 * @see  http://docs.kohanaphp.com/install#modules
 */
$modules = 'modules';

/**
 * The directory in which the Kohana resources are located. The system
 * directory must contain the classes/kohana.php file.
 *
 * @see  http://docs.kohanaphp.com/install#system
 */
$system = 'system';

/**
 * The default extension of resource files. If you change this, all resources
 * must be renamed to use the new extension.
 *
 * @see  http://docs.kohanaphp.com/install#ext
 */
define('EXT', '.php');

/**
 * Test to make sure that Kohana is running on PHP 5.2 or newer. Once you are
 * sure that your environment is compatible with Kohana, you can comment this
 * line out. When running an application on a new server, uncomment this line
 * to check the PHP version quickly.
 */
//version_compare(PHP_VERSION, '5.2', '<') and exit('Kohana requires PHP 5.2 or newer.');

/**
 * Set the PHP error reporting level. If you set this in php.ini, you remove this.
 * @see  http://php.net/error_reporting
 *
 * When developing your application, it is highly recommended to enable notices
 * and strict warnings. Enable them by using: E_ALL | E_STRICT
 *
 * In a production environment, it is safe to ignore notices and strict warnings.
 * Disable them by using: E_ALL ^ E_NOTICE
 *
 * When using a legacy application with PHP >= 5.3, it is recommended to disable
 * deprecated notices. Disable with: E_ALL & ~E_DEPRECATED
 */
error_reporting(E_ALL);

/**
 * End of standard configuration! Changing any of the code below should only be
 * attempted by those with a working knowledge of Kohana internals.
 *
 * @see  http://docs.kohanaphp.com/bootstrap
 */

// Set the full path to the docroot
define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);

//set_include_path( 'lib' );

// Make the application relative to the docroot
if ( ! is_dir($application) AND is_dir(DOCROOT.$application))
    $application = DOCROOT.$application;

// Make the modules relative to the docroot
if ( ! is_dir($modules) AND is_dir(DOCROOT.$modules))
    $modules = DOCROOT.$modules;

// Make the system relative to the docroot
if ( ! is_dir($system) AND is_dir(DOCROOT.$system))
    $system = DOCROOT.$system;

// Define the absolute paths for configured directories
define('APPPATH', realpath($application).DIRECTORY_SEPARATOR);
define('MODPATH', realpath($modules).DIRECTORY_SEPARATOR);
define('SYSPATH', realpath($system).DIRECTORY_SEPARATOR);
define('TEMPLATEPATH', DOCROOT.'templates'.DIRECTORY_SEPARATOR);

define('STATICPATH',    DOCROOT.'static'.DIRECTORY_SEPARATOR);
define('MEDIAPATH',     STATICPATH.'media'.DIRECTORY_SEPARATOR);
define('IMAGEPATH',     STATICPATH.'images'.DIRECTORY_SEPARATOR);

// Define the web paths for user directories
define('MEDIAWEBPATH',	'/static/media/');
define('IMAGEWEBPATH',	'/static/images/');

// Clean up the configuration vars
unset($application, $modules, $system);

if (file_exists('install'.EXT)) {
    // Load the installation check
    return include 'install'.EXT;
}

// Load the base, low-level functions
require SYSPATH.'base'.EXT;

// Load the core Kohana class
require SYSPATH.'classes/kohana/core'.EXT;

if (is_file(APPPATH.'classes/kohana'.EXT)) {
    // Application extends the core
    require APPPATH.'classes/kohana'.EXT;
}
else {
    // Load empty core extension
    require SYSPATH.'classes/kohana'.EXT;
}

// Bootstrap the application
require APPPATH.'bootstrap'.EXT;
