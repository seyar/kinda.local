jQuery.fn.SelectCustomizer = function(){
    // Select Customizer jQuery plug-in
	// based on customselect by Ace Web Design http://www.adelaidewebdesigns.com/2008/08/01/adelaide-web-designs-releases-customselect-with-icons/
	// modified by David Vian http://www.ildavid.com/dblog
	// and then modified AGAIN be Dean Collins http://www.dblog.com.au
    return this.each(function(){
        var obj = jQuery(this);
        var name = obj.attr('id');
        var id_slc_options = name+'_options';
        var id_icn_select = name+'_iconselect';
        var id_holder = name+'_holder';
        var custom_select = name ? name : '_customselect';
        var selected_value = '';
        var selectedClass = '';
        var selectedTitle = '';
        var ttt = '';
        obj.after("<div id=\""+id_slc_options+"\" class=\"optionswrapper\"> </div>");
        obj.find('option').each(function(i){
            selectedClass='';
            if( jQuery(this).attr('selected') == true ){selectedClass = ' selectedclass'; ttt = jQuery(this).text(); selected_value = jQuery(this).attr("value"); selectedTitle = jQuery(this).attr("title");if(!selectedTitle) selectedTitle = '';}
            jQuery("#"+id_slc_options).append("<div alt=\"" + jQuery(this).attr("title") + "\" title=\"" + jQuery(this).attr("value") + "\" class=\"selectitems"+selectedClass+"\"><span>" + jQuery(this).html().replace(/&amp;/g,'&').replace(/&lt;/g,'<').replace(/&gt;/g,'>') + "</span></div>");
            
        });

        obj.before("<input title=\"" + selectedTitle + "\" type=\"hidden\" value =\"" + selected_value + "\" name=\"" + obj.attr('name') + "\" id=\""+custom_select+"\" /><div class=\"customSelectWrap\"><div id=\""+id_icn_select+"\" class=\"iconselect\">" + ttt +  "</div><div id=\""+id_holder+"\" class=\"selectwrapper\"> </div></div>").remove();
        var width = parseFloat(obj.css('width'));
        if(width)
        {
            widthStr = (width - 32)+'px';
            widthStr1 = (width - 0)+'px';
            jQuery("#"+id_icn_select).css('width', widthStr);
            if(width > 152)
                jQuery("#"+id_holder).css('width', widthStr1);
        }
        
        jQuery("#"+id_icn_select).click(function(a){
			if(jQuery("#"+id_holder).css('display') == 'none') {
				jQuery("#"+id_holder).fadeIn(200);
				jQuery("#"+id_holder).focus();
				a.stopPropagation();
				jQuery(document).keypress(function(e) {
					if(!e) var e = window.event;
					e.cancelBubble = true;
					e.returnValue = false;
					if (e.stopPropagation) {
						e.stopPropagation();
						e.preventDefault();
					}
				});
				jQuery(document).keyup(function(e) {

					if(e.which == 40) {
						var lastSelected = jQuery("#"+id_holder+" .selectedclass");
						if(lastSelected.size() == 0) {
							var nextSelected =  jQuery("#"+id_slc_options+" div:first:");
						} else {
							var nextSelected = lastSelected.next(".selectitems");
						}
						if(nextSelected.size() == 1) {
							lastSelected.removeClass("selectedclass");
							nextSelected.addClass("selectedclass");
							jQuery("#"+custom_select).val(nextSelected.title);
                                                        jQuery("#"+custom_select).attr('title', jQuery(nextSelected).attr('alt'));
                                                        jQuery("#"+id_icn_select).html(nextSelected.html());
							var rowOffset = (nextSelected.offset().top - jQuery("#"+id_holder).offset().top);
							if(rowOffset > 130) {
								jQuery("#"+id_slc_options).scrollTo((jQuery("#"+id_slc_options).scrollTop() + 27) +  "px");
							}
						}

					} else if(e.which == 38) {
						var lastSelected = jQuery("#"+id_holder+" .selectedclass");
						var nextSelected = lastSelected.prev(".selectitems");
						if(nextSelected.size() == 1) {
							lastSelected.removeClass("selectedclass");
							nextSelected.addClass("selectedclass");
							jQuery("#"+custom_select).val(nextSelected.title);

                                                        jQuery("#"+custom_select).attr('title', nextSelected.attr('alt'));
           					jQuery("#"+id_icn_select).html(nextSelected.html());
							var rowOffset = (nextSelected.offset().top - jQuery("#"+id_holder).offset().top);
							if(rowOffset > 0) {
								jQuery("#"+id_slc_options).scrollTo((jQuery("#"+id_slc_options).scrollTop() - 27) +  "px");
							}
						}
					} else if(e.which == 13) {
						jQuery("#"+id_holder).fadeOut(250);
						jQuery(document).unbind('keyup');
						jQuery(document).unbind('keypress');
						jQuery('body').unbind('click');
					}

				});
				jQuery('body').click(function(){
					jQuery("#"+id_holder).fadeOut(200);
					jQuery('body').unbind('click');
					jQuery(document).unbind('keyup');
					jQuery(document).unbind('keypress');
				});
			} else {
				jQuery("#"+id_holder).fadeOut(200);
				jQuery('body').unbind('click');
				jQuery(document).unbind('keyup');
				jQuery(document).unbind('keypress');
			}


        });


        jQuery("#"+id_holder).append(jQuery("#"+id_slc_options)[0]);
		jQuery("#"+id_holder).append("<div class=\"selectfooter\"></div>");
		jQuery("#"+id_slc_options+" > div:last").addClass("last");

//        if(!jQuery("#"+id_holder+" .selectedclass").length)
//            jQuery("#"+id_holder).find(".selectitems:first").addClass('selectedclass');
//        
        jQuery("#"+id_holder+ " .selectitems").mouseover(function(){

            jQuery(this).addClass("hoverclass");
        });
        jQuery("#"+id_holder+" .selectitems").mouseout(function(){
            jQuery(this).removeClass("hoverclass");
        });
        jQuery("#"+id_holder+" .selectitems").click(function(){
            var changeEvent;
            var doOnChange = false;
            if( (changeEvent = obj.attr('onchange')) )
            {
                if(!jQuery(this).hasClass('selectedclass'))                    
                    doOnChange = true;
            }
            jQuery("#"+id_holder+" .selectedclass").removeClass("selectedclass");
            jQuery(this).addClass("selectedclass");
            var thisselection = jQuery(this).html();

            jQuery("#"+custom_select).attr('title', jQuery(this).attr('alt'));
            jQuery("#"+custom_select).val(jQuery(this).attr('title'));
            
            jQuery("#"+id_icn_select).html(thisselection);
            jQuery("#"+id_holder).fadeOut(250);
            jQuery(document).unbind('keyup');
            jQuery(document).unbind('keypress');
            jQuery('body').unbind('click');

            if( doOnChange ) //jQuery("#custom_select").bind('click', onchange);//
                setTimeout(changeEvent, 0);


        });

        jQuery("#"+id_holder).hover(function(){},function(){jQuery(this).fadeOut(200)});


    });
}